package com.hp.printosmobilelib.ui.widgets;

import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;

/**
 * Created by Anwar Asbah on 12/19/2016.
 */
public class CustomSlidingAnimation extends Animation {

    private View view;
    private int targetHeight;
    private int currentHeight;
    private int heightChange;

    CustomAnimationCallback callback;

    public CustomSlidingAnimation(View view) {
        this.view = view;
        targetHeight = 0;
        currentHeight = 0;
        heightChange = 0;
    }

    public void addAnimationCallback(CustomAnimationCallback callback) {
        this.callback = callback;
    }

    @Override
    protected void applyTransformation(float interpolatedTime, Transformation transformation) {
        if (interpolatedTime >= 1) {
            view.getLayoutParams().height = targetHeight;
        } else {
            int stepHeight = (int) (heightChange * interpolatedTime);
            view.getLayoutParams().height = currentHeight + stepHeight;
        }

        view.requestLayout();
        if(callback != null){
            callback.onAnimationFrame();
        }
    }

    @Override
    public boolean willChangeBounds() {
        return true;
    }

    public void init(int targetHeight, int currentHeight) {
        this.targetHeight = targetHeight;
        this.currentHeight = currentHeight;
        this.heightChange = targetHeight - currentHeight;
    }

    public interface CustomAnimationCallback {
        void onAnimationFrame();
    }
}
