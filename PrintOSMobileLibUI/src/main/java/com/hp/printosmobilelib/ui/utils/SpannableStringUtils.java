package com.hp.printosmobilelib.ui.utils;

import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.TypefaceSpan;

/**
 * Created by Minerva on 3/5/2018.
 */

public class SpannableStringUtils {

    private SpannableString spannableString;

    public SpannableStringUtils(String spannable) {
        spannableString = new SpannableString(spannable);
    }

    public void setSpannable(TypefaceSpan customTypefaceSpan, int fontSize, int color, int index, int length) {
        spannableString.setSpan(customTypefaceSpan, index, length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(new ForegroundColorSpan(color),
                index, length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(new AbsoluteSizeSpan(fontSize),
                index, length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
    }

    public SpannableString getSpannableString() {
        return spannableString;
    }
}
