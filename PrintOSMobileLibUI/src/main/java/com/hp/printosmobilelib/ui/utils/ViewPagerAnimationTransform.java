package com.hp.printosmobilelib.ui.utils;

import android.support.v4.view.ViewPager;
import android.view.View;

/**
 * Created by Anwar Asbah on 4/25/2016.
 */
public class ViewPagerAnimationTransform implements ViewPager.PageTransformer {

    private int viewId;

    public ViewPagerAnimationTransform(int viewId) {
        this.viewId = viewId;
    }

    public void transformPage(View view, float position) {

        int pageWidth = view.getWidth();


        if (position < -1) { // [-Infinity,-1)
            //This page is way off-screen to the left.
            view.setAlpha(1);

        } else if (position <= 1) { // [-1,1]

            View image = view.findViewById(viewId);
            if(image != null){
                image.setTranslationX(-position * pageWidth / 2); //Half the normal speed
            }

        } else { // (1,+Infinity]
            // This page is way off-screen to the right.
            view.setAlpha(1);
        }


    }
}
