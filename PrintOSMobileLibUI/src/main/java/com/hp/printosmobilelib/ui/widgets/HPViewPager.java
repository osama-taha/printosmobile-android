package com.hp.printosmobilelib.ui.widgets;

import android.content.Context;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;

/**
 * A custom view pager for HP adding custom properties to enable/disable the scrolling.
 *
 * @author Osama Taha
 *         Created by on 3/29/16.
 */
public class HPViewPager extends ViewPager implements Animation.AnimationListener {

    static final long ANIMATION_DURATION = 200;
    private static final int DEFAULT_MAX_HEIGHT = Integer.MAX_VALUE;

    private int maxHeight;
    private Handler handler;
    private Runnable onAnimationEndAction;

    /**
     * True if the scrolling is enabled, false otherwise.
     */
    private boolean scrollingEnabled = true;
    private View mCurrentView;

    Boolean mAnimStarted = false;
    CustomSlidingAnimation animation;

    public HPViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    void init(){
        maxHeight = DEFAULT_MAX_HEIGHT;
        animation = new CustomSlidingAnimation(this);
        animation.setAnimationListener(this);
        handler = new Handler();
    }

    public void addCustomAnimationCallback (CustomSlidingAnimation.CustomAnimationCallback callback) {
        animation.addAnimationCallback(callback);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return isScrollingEnabled() ? super.onTouchEvent(event) : false;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        return isScrollingEnabled() ? super.onInterceptTouchEvent(event) : false;
    }

    /**
     * Set the enabled state of this view.
     *
     * @param scrollingEnabled True if the scrolling is enabled, false otherwise.
     */
    public void setScrollingEnabled(boolean scrollingEnabled) {
        this.scrollingEnabled = scrollingEnabled;
    }

    /**
     * Returns the enabled status for scrolling.
     *
     * @return True if the scrolling is enabled, false otherwise.
     */
    public boolean isScrollingEnabled() {
        return scrollingEnabled;
    }

    protected  int getNewHeight (int widthMeasureSpec){
        mCurrentView.measure(widthMeasureSpec, MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED));
        int h = mCurrentView.getMeasuredHeight();
        return Math.min(maxHeight, h);
    }

    public void setMaxHeight(int maxHeight){
        this.maxHeight = maxHeight;
    }

    public void measureCurrentView(View currentView) {
        mCurrentView = currentView;
        requestLayout();
    }

    boolean itemsInstantiated () {
        if (mCurrentView == null) {
            return false;
        }
        return true;
    }

    public void addAnimatinListener (final Animation.AnimationListener listener){
        onAnimationEndAction = new Runnable() {
            @Override
            public void run() {
                listener.onAnimationEnd(null);
            }
        };
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        if (!mAnimStarted && itemsInstantiated()) {
            int height = getNewHeight(widthMeasureSpec);
            int newHeight = MeasureSpec.makeMeasureSpec(height, MeasureSpec.EXACTLY);

            if (getLayoutParams().height != 0 && heightMeasureSpec != newHeight) {
                animation.init(height, getLayoutParams().height);
                animation.setDuration(ANIMATION_DURATION);
                startAnimation(animation);
                if(onAnimationEndAction != null) {
                    handler.postDelayed(onAnimationEndAction, ANIMATION_DURATION);
                }
                mAnimStarted = true;
            } else {
                heightMeasureSpec = newHeight;
            }
        }

        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    @Override
    public void onAnimationStart(Animation animation) {
        mAnimStarted = true;
    }

    @Override
    public void onAnimationEnd(Animation animation) {
        mAnimStarted = false;
    }

    @Override
    public void onAnimationRepeat(Animation animation) {
    }
}