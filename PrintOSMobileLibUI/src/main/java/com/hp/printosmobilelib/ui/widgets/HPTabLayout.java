package com.hp.printosmobilelib.ui.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.support.design.widget.TabLayout;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hp.printosmobilelib.ui.R;

/** created by Anwar Asbah on 10/18/2016 **/
public class HPTabLayout extends TabLayout {


    private Typeface typeface;

    public HPTabLayout(Context context) {
        this(context, null);
        init(null);
    }

    public HPTabLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public HPTabLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs);
    }

    @Override
    public void addTab(Tab tab) {
        super.addTab(tab);

        ViewGroup mainView = (ViewGroup) getChildAt(0);
        ViewGroup tabView = (ViewGroup) mainView.getChildAt(tab.getPosition());

        int tabChildCount = tabView.getChildCount();
        for (int i = 0; i < tabChildCount; i++) {
            View tabViewChild = tabView.getChildAt(i);
            if (tabViewChild instanceof TextView) {
                if (typeface != null) {
                    ((TextView) tabViewChild).setTypeface(typeface, Typeface.NORMAL);
                }
            }
        }
    }

    private void init(AttributeSet attrs) {
        if (attrs == null) {
            return;
        }

        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.HPTabLayout);
        if (typedArray.hasValue(R.styleable.HPTabLayout_typeface)) {
            int typefaceValue = typedArray.getInt(R.styleable.HPTabLayout_typeface, TypefaceManager.HPTypeface.HP_REGULAR);
            typeface = TypefaceManager.getTypeface(getContext(), typefaceValue);
        }

        typedArray.recycle();
    }
}
