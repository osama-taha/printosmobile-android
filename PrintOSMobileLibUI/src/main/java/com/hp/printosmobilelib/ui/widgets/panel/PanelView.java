package com.hp.printosmobilelib.ui.widgets.panel;

import android.content.Context;
import android.os.Handler;
import android.support.v7.widget.CardView;
import android.text.Spannable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.hp.printosmobilelib.ui.R;
import com.hp.printosmobilelib.ui.common.HPView;

import butterknife.ButterKnife;

/**
 * Created by Anwar Asbah on 4/12/2016.
 */
public abstract class PanelView<T> extends FrameLayout implements HPView<T> {

    private static final String TAG = PanelView.class.getSimpleName();

    private View headerView;
    private TextView headerTitle;
    private View headerSeparatorView;
    private LinearLayout contentLayout;
    private ProgressBar loadingView;
    private LinearLayout headerLeftViewContainer;
    private LinearLayout headerLeftViewContainer2;
    private View contentView;
    private TextView noInformationTextView;
    protected CardView containerView;

    private T viewModel;
    protected View shareButton;

    public static final String PANEL_SCREEN_SHOT_FILE_NAME = "PrintOS";

    public PanelView(Context context) {
        super(context);
        init();
    }

    public PanelView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public PanelView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        inflate(getContext(), R.layout.view_panel, this);
        containerView = findViewById(R.id.container);
        contentLayout = findViewById(R.id.layout_content);
        loadingView = findViewById(R.id.loading_view);
        headerView = findViewById(R.id.header_container);
        headerTitle = findViewById(R.id.text_title);
        headerLeftViewContainer = findViewById(R.id.left_view_container);
        headerLeftViewContainer2 = findViewById(R.id.left_view_container1);
        noInformationTextView = findViewById(R.id.text_view_no_information);
        headerSeparatorView = findViewById(R.id.separator);
        setTitle(getTitleSpannable());
        contentView = inflate(getContext(), getContentView(), contentLayout);
        ButterKnife.bind(this, this);

        headerView.setVisibility(showPanelHeader() ? VISIBLE : GONE);
        headerSeparatorView.setVisibility(showPanelHeader() ? VISIBLE : GONE);

        if (hasSharingButton()) {
            View headerLeftView = inflate(getContext(), R.layout.panel_share_view, headerLeftViewContainer2);
            shareButton = headerLeftView.findViewById(R.id.share_button);
            shareButton.setVisibility(GONE);
            shareButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    onShareClicked();
                }
            });
        }

        if (isPanelRounded()) {
            containerView.setRadius(getContext().getResources().getDimension(R.dimen.panel_view_max_corner_radius));
        }

        bindViews();
    }

    public void setTitleNumberOfLines(int numberOfLines) {
        headerTitle.setMaxLines(numberOfLines);
    }

    public void onRefresh() {
        //do nothing
    }

    public abstract Spannable getTitleSpannable();

    public abstract int getContentView();

    public abstract void updateViewModel(T viewModel);

    public LinearLayout getHeaderLeftView() {
        return headerLeftViewContainer;
    }

    public abstract void bindViews();

    @Override
    public void showLoading() {
        loadingView.setVisibility(VISIBLE);
        noInformationTextView.setVisibility(INVISIBLE);
        if (shareButton != null) {
            shareButton.setEnabled(false);
        }
    }

    @Override
    public void hideLoading() {
        loadingView.setVisibility(GONE);
        if (shareButton != null) {
            shareButton.setEnabled(true);
        }
    }

    public boolean isLoading() {
        return loadingView.getVisibility() == VISIBLE;
    }

    @Override
    public void setViewModel(T viewModel) {
        this.viewModel = viewModel;
    }

    public T getViewModel() {
        return viewModel;
    }

    public View getView() {
        return contentView;
    }

    public View getCurrentView() {
        return getView();
    }

    public void setTitle(Spannable title) {
        if (showPanelHeader()) {
            headerTitle.setText(title, TextView.BufferType.SPANNABLE);
        }
    }

    public void sharePanel() {
        //Overriding this method is not required..
        //Concrete classes may hide the implementation of it with their custom implementation.
    }

    public void lockShareButton(boolean lock) {
        //Concrete classes may hide the implementation of it with their custom implementation.
        if (shareButton != null) {
            shareButton.setEnabled(!lock);
            shareButton.setClickable(!lock);
        }
    }

    public void setShareButtonVisibility(Boolean isVisible) {
        if (shareButton != null) {
            shareButton.setVisibility(isVisible ? VISIBLE : GONE);
        }
    }

    public void onDestroy() {

    }

    public boolean showEmptyCard() {

        final boolean isValid = isValidViewModel();

        new Handler().post(new Runnable() {
            @Override
            public void run() {

                if (shareButton != null) {
                    shareButton.setVisibility(hasSharingButton() && isValid ? VISIBLE : GONE);
                }

                if (isValid) {
                    noInformationTextView.setVisibility(GONE);
                    contentLayout.setVisibility(VISIBLE);
                    headerLeftViewContainer.setVisibility(VISIBLE);
                } else {
                    noInformationTextView.setText(getEmptyCardText());
                    contentLayout.setVisibility(GONE);
                    noInformationTextView.setVisibility(VISIBLE);
                    headerLeftViewContainer.setVisibility(GONE);
                }

                hideLoading();

            }
        });

        return !isValid;
    }

    protected boolean isValidViewModel() {
        return true;
    }

    protected abstract String getEmptyCardText();

    protected boolean hasSharingButton() {
        return false;
    }

    protected boolean showPanelHeader() {
        return true;
    }

    protected boolean isPanelRounded() {
        return false;
    }

    protected void onShareClicked() {
        //panel related
    }

    public String getPanelTag() {
        return TAG;
    }

}
