package com.hp.printosmobilelib.ui.common;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import butterknife.ButterKnife;

/**
 * A base activity for HP created to be extended by every activity to provide some configurations
 * and some methods common to every activity.
 *
 * @author Osama Taha
 */
public abstract class HPActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResource());
        ButterKnife.bind(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    protected abstract int getLayoutResource();
}
