package com.hp.printosmobilelib.ui.common;

/**
 * An interface for HP custom views.
 * Created by Osama Taha on 3/28/16.
 */
public interface HPView<T> {

    void showLoading();
    void hideLoading();
    void setViewModel(T viewModel);
}
