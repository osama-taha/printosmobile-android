package com.hp.printosmobilelib.core.logging;

import java.util.List;

/**
 * Created by Osama Taha
 */
public interface HPLogListener {

    void flushLogs(List<HPLogEvent> logEvents);
}