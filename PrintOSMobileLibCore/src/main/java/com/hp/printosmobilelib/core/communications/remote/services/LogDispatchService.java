package com.hp.printosmobilelib.core.communications.remote.services;

import com.hp.printosmobilelib.core.logging.HPLogEvent;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by OsamaTaha on 3/30/17.
 */

public interface LogDispatchService {

    @POST("api/PrintbeatService/mobile/log")
    Call<ResponseBody> sendLogsToServer(@Body List<HPLogEvent> logEvents);

}
