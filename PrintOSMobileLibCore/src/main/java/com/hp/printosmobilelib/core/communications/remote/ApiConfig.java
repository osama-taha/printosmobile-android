package com.hp.printosmobilelib.core.communications.remote;

/**
 * A configuration class for API client.
 *
 * @author Osama Taha
 */
public class ApiConfig {

    /*
   * The base host URL.
   */
    private final String[] baseHostUrl;

    public ApiConfig(String... baseHostUrl) {
        if (baseHostUrl == null) {
            throw new IllegalArgumentException(this.getClass().getName() + ", baseHostUrl == null");
        }
        this.baseHostUrl = baseHostUrl;
    }

    public String[] getBaseHostsUrl() {
        return baseHostUrl;
    }

}
