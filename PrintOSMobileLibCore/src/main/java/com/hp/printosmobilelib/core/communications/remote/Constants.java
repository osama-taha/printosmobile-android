package com.hp.printosmobilelib.core.communications.remote;

/**
 * Created by Osama on 11/19/17.
 */

public class Constants {

    private static final String USERS_API = "api/aaa/v4/users/";

    public static final String CONTEXT_API = USERS_API + "context";
    public static final String LOGIN_API = USERS_API + "login";
    public static final String LOGOUT_API = USERS_API + "logout";
    public static final String VALIDATE_API = USERS_API + "validate";

    private Constants() {
        //Prevent initiating this class.
    }
}
