package com.hp.printosmobilelib.core.logging;

import android.content.Context;
import android.os.AsyncTask;

import com.hp.printosmobilelib.core.communications.remote.SessionManager;

import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.AsyncAppender;
import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.android.LogcatAppender;
import ch.qos.logback.classic.encoder.PatternLayoutEncoder;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.rolling.FixedWindowRollingPolicy;
import ch.qos.logback.core.rolling.RollingFileAppender;
import ch.qos.logback.core.rolling.SizeBasedTriggeringPolicy;
import ch.qos.logback.core.util.StatusPrinter;

/**
 * Created by Osama Taha on 11/2/16.
 */
public class HPLogger {

    private static final String LOG_PATTERN = "%d{yyyy-MM-dd_HH-mm-ss} [%thread] %-5level %logger{36} - %msg%n";
    private static final String MAX_LOG_FILE_SIZE = "2MB";

    private static final String ERROR = "e";
    private static final String WARN = "w";
    private static final String VERBOSE = "v";
    private static final String INFO = "i";
    private static final String DEBUG = "d";

    private static HPLogger sharedInstance;

    private static Context sContext;
    private String filePath;

    public static HPLogger getInstance(Context context) {
        if (sharedInstance == null) {
            sharedInstance = new HPLogger(context);
        }
        return sharedInstance;
    }

    private static boolean logToServer() {
        return HPLogConfig.getInstance(sContext).isEnabled();
    }

    private HPLogger(Context context) {
        sContext = context;
    }

    public void init(String directoryPath, String fileName, String fileNameFormat) {

        this.filePath = directoryPath + fileName;

        // reset the default sContext (which may already have been initialized)
        // since we want to reconfigure it
        LoggerContext context = (LoggerContext) LoggerFactory.getILoggerFactory();
        context.reset();

        // setup FileAppender
        PatternLayoutEncoder encoder1 = new PatternLayoutEncoder();
        encoder1.setContext(context);
        encoder1.setPattern(LOG_PATTERN);
        encoder1.start();

        RollingFileAppender<ILoggingEvent> rollingFileAppender = new RollingFileAppender<>();
        rollingFileAppender.setFile(this.filePath);
        rollingFileAppender.setEncoder(encoder1);
        rollingFileAppender.setAppend(true);
        rollingFileAppender.setContext(context);

        SizeBasedTriggeringPolicy<ILoggingEvent> triggeringPolicy = new SizeBasedTriggeringPolicy<>();
        triggeringPolicy.setMaxFileSize(MAX_LOG_FILE_SIZE);
        triggeringPolicy.setContext(context);
        triggeringPolicy.start();

        FixedWindowRollingPolicy rollingPolicy = new FixedWindowRollingPolicy();
        rollingPolicy.setFileNamePattern(directoryPath + fileNameFormat);
        rollingPolicy.setMaxIndex(1);
        rollingPolicy.setParent(rollingFileAppender);
        rollingPolicy.setContext(context);
        rollingPolicy.start();


        rollingFileAppender.setTriggeringPolicy(triggeringPolicy);
        rollingFileAppender.setRollingPolicy(rollingPolicy);
        rollingFileAppender.start();

        PatternLayoutEncoder encoder2 = new PatternLayoutEncoder();
        encoder2.setContext(context);
        encoder2.setPattern(LOG_PATTERN);
        encoder2.start();

        LogcatAppender logcatAppender = new LogcatAppender();
        logcatAppender.setContext(context);
        logcatAppender.setEncoder(encoder2);
        logcatAppender.start();

        AsyncAppender asyncAppender = new AsyncAppender();
        asyncAppender.setContext(context);
        asyncAppender.setName("ASYNC");
        asyncAppender.addAppender(rollingFileAppender);
        asyncAppender.start();

        // Add the newly created appenders to the root logger;
        // qualify Logger to disambiguate from org.slf4j.Logger
        ch.qos.logback.classic.Logger root = (ch.qos.logback.classic.Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
        root.setLevel(Level.ALL);
        root.addAppender(asyncAppender);
        root.addAppender(logcatAppender);

        //Print any status messages (warnings, etc) encountered in logback config
        StatusPrinter.print(context);

    }

    public String getFilePath() {
        return filePath;
    }

    private static boolean logToFileEnabled() {
        return HPLogConfig.getInstance(sContext).isLogToFileEnabled();
    }

    public static org.slf4j.Logger getLogger(String tag) {
        org.slf4j.Logger log = LoggerFactory.getLogger(tag);
        return log;
    }


    public static void v(String tag, String message) {
        new AsyncDebLoggerTask().execute(tag, message, VERBOSE);
    }

    public static void e(final String tag, final String message) {
        new AsyncDebLoggerTask().execute(tag, message, ERROR);
    }

    public static void i(final String tag, final String message) {
        new AsyncDebLoggerTask().execute(tag, message, INFO);
    }

    public static void d(final String tag, final String message) {
        new AsyncDebLoggerTask().execute(tag, message, DEBUG);
    }

    public static void e(final String tag, final String message, final Throwable ex) {
        new AsyncDebLoggerTask().execute(tag, message + " " + ex, ERROR);
    }

    public static void w(final String tag, final String message) {
        new AsyncDebLoggerTask().execute(tag, message, WARN);
    }

    /**
     * A method to be called to Log locally only.
     */
    public static void logD(String tag, String message) {
        if (logToFileEnabled()) {
            org.slf4j.Logger log = getLogger(tag);
            log.debug(message);
        }
    }

    /**
     * A method to be called to Log locally only.
     */
    public static void logE(String tag, String message, Throwable ex) {
        if (logToFileEnabled()) {
            org.slf4j.Logger log = getLogger(tag);
            log.error(message, ex);
        }
    }

    public static void enableFileLogging(boolean enable) {
        if (enable) {
            ((LoggerContext) LoggerFactory.getILoggerFactory()).start();
        } else {
            ((LoggerContext) LoggerFactory.getILoggerFactory()).stop();
        }
    }


    private static void addToLogsQueue(String tag, Level level, final String message) {

        if (logToServer()) {

            HPLogQueue.LogType logType = SessionManager.getInstance(sContext).hasCookie() ?
                    HPLogQueue.LogType.AUTHENTICATED_LOG : HPLogQueue.LogType.PRE_LOGIN;

            HPLogQueue.getInstance(sContext).enqueueLog(logType, level, tag,
                    message);
        }
    }


    static class AsyncDebLoggerTask extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... strings) {

            if (strings.length != 3) {
                return null;
            }

            String tag = strings[0];
            String message = strings[1];
            String type = strings[2];

            if (logToFileEnabled()) {
                org.slf4j.Logger log = getLogger(tag);
                if(type.equals(ERROR)) {
                    log.error(message);
                } else if (type.equals(INFO)) {
                    log.info(message);
                } else if (type.equals(WARN)) {
                    log.warn(message);
                } else {
                    log.debug(message);
                }

            }

            if(type.equals(ERROR)) {
                addToLogsQueue(tag, Level.ERROR, message);
            } else if (type.equals(INFO)) {
                addToLogsQueue(tag, Level.INFO, message);
            } else if (type.equals(WARN)) {
                addToLogsQueue(tag, Level.WARN, message);
            } else if (type.equals(VERBOSE)) {
                addToLogsQueue(tag, Level.ALL, message);
            } else {
                addToLogsQueue(tag, Level.DEBUG, message);
            }

            return null;
        }
    }
}
