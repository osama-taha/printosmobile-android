package com.hp.printosmobilelib.core.communications.remote.services;


import com.hp.printosmobilelib.core.communications.remote.Constants;
import com.hp.printosmobilelib.core.communications.remote.models.OrganizationJsonBody;
import com.hp.printosmobilelib.core.communications.remote.models.UserCredentials;
import com.hp.printosmobilelib.core.communications.remote.models.UserData;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import rx.Observable;

/**
 * Retrofit REST interface definition for Login Endpoint
 * <p/>
 * Created by Osama Taha on 3/27/16.
 */
public interface LoginService {

    /**
     * Login the user to the APP and and returns a data relevant the logged in user.
     *
     * @param userCredentials - represents the required data to authenticate the user.
     */
    @POST(Constants.LOGIN_API)
    Observable<Response<UserData>> login(@Header("mobile-app") String header, @Body UserCredentials userCredentials);

    @POST(Constants.LOGIN_API)
    Call<UserData> loginSync(@Header("mobile-app") String header, @Body UserCredentials userCredentials);

    @PUT(Constants.CONTEXT_API)
    Call<ResponseBody> changeOrganizationContext(@Body OrganizationJsonBody organization);

    @DELETE(Constants.LOGOUT_API)
    Call<ResponseBody> logout();

    @GET(Constants.VALIDATE_API)
    Call<ResponseBody> checkCookie();

    @GET(Constants.VALIDATE_API)
    Observable<Response<UserData>> checkCookieAsync();

    @PUT(Constants.CONTEXT_API)
    Observable<ResponseBody> changeOrganizationContextAsync(@Body OrganizationJsonBody organization);
}
