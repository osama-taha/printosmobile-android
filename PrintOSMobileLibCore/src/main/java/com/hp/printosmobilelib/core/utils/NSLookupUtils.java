package com.hp.printosmobilelib.core.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.LinkProperties;
import android.os.AsyncTask;
import android.os.Build;

import org.xbill.DNS.ExtendedResolver;
import org.xbill.DNS.Lookup;
import org.xbill.DNS.Record;
import org.xbill.DNS.SimpleResolver;
import org.xbill.DNS.Type;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Osama on 3/8/18.
 */

public class NSLookupUtils {

    /**
     * @throws Exception
     */
    @SuppressWarnings("unchecked")
    public static String resolveAPITXT(Context context, String lookupFile) throws Exception {

        Lookup lookup = new Lookup(lookupFile, Type.TXT);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            /* Since Android 8, the system properties net.dns1, net.dns2, ... are not available anymore.
               The current version of dnsjava relies on these properties to find the default name servers,
               so we have to add the servers explicitly (fortunately, there's an Android API to
               get the active DNS servers). */
            ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            LinkProperties activeLink = connectivity.getLinkProperties(connectivity.getActiveNetwork());

            List<String> simpleResolvers = new ArrayList<>();
            for (InetAddress inetAddress : activeLink.getDnsServers()) {
                SimpleResolver simpleResolver = new SimpleResolver();
                simpleResolver.setAddress(inetAddress);
                simpleResolvers.add(inetAddress.getHostAddress());
            }

            ExtendedResolver extendedResolver = new ExtendedResolver(simpleResolvers.toArray(new String[simpleResolvers.size()]));
            lookup.setResolver(extendedResolver);
        }

        Record recs[] = lookup.run();
        if (recs == null) {
            throw new RuntimeException("Could not lookup domain.");
        }

        String result = "";

        for (Record rec : recs) {
            String rData = rec.rdataToString().replaceAll("\"", "");
            result += rData;
        }

        return result;
    }

}
