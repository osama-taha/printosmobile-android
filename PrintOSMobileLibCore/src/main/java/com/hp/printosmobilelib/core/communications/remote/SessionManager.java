package com.hp.printosmobilelib.core.communications.remote;

import android.content.Context;

import com.hp.printosmobilelib.core.communications.remote.models.UserCredentials;
import com.hp.printosmobilelib.core.utils.EncryptionUtils;

/**
 * This class will manage saving/retrieving the user cookie/credentials in secure way.
 *
 * @author Osama Taha
 */
public class SessionManager {

    private final Preferences storage;

    private static SessionManager Instance;

    private SessionManager(Context context) {
        this.storage = Preferences.getInstance(context);
    }

    public static SessionManager getInstance(Context context) {
        if (Instance == null) {
            Instance = new SessionManager(context);
        }
        return Instance;
    }


    public boolean hasCookie() {
        return !getCookie().isEmpty();
    }

    public String getCookie() {
        return storage.getString(Key.PREF_COOKIE, "");
    }

    public void saveCookie(String cookie) {
        storage.put(Key.PREF_COOKIE, cookie);
        storage.commit();
    }

    public void saveUserCredentials(UserCredentials user) {

        storage.put(Key.PREF_USERNAME, EncryptionUtils.encrypt(user.getLogin()));
        storage.put(Key.PREF_PASS, EncryptionUtils.encrypt(user.getPassword()));
        storage.put(Key.PREF_EULA_VERSION, user.getEulaVersion());
        storage.commit();

    }

    public synchronized UserCredentials getUserCredentials() {
        UserCredentials userCredentials = new UserCredentials(EncryptionUtils.decrypt(storage.getString(Key.PREF_USERNAME, EncryptionUtils.encrypt("")))
                , EncryptionUtils.decrypt(storage.getString(Key.PREF_PASS, "")), storage.getString(Key.PREF_EULA_VERSION, null));
        return userCredentials;
    }

    public synchronized void clearUserCredentials() {
        storage.remove(Key.PREF_USERNAME, Key.PREF_PASS);
        storage.commit();
    }

    public synchronized void saveTempCredentials() {
        storage.put(Key.PREF_TEMP_USERNAME, storage.getString(Key.PREF_USERNAME, ""));
        storage.put(Key.PREF_TEMP_PASS, storage.getString(Key.PREF_PASS, ""));
        storage.commit();
    }

    public synchronized UserCredentials getTempCredentials() {
        String userName = storage.getString(Key.PREF_TEMP_USERNAME, null);
        String password = storage.getString(Key.PREF_TEMP_PASS, null);
        UserCredentials userCredentials = new UserCredentials(
                userName == null ? null : EncryptionUtils.decrypt(userName),
                password == null ? null : EncryptionUtils.decrypt(password),
                null);
        return userCredentials;
    }

    public synchronized void clearTempCredentials() {
        storage.remove(Key.PREF_TEMP_USERNAME, Key.PREF_TEMP_PASS);
        storage.commit();
    }

    public void clearCookie() {
        storage.remove(Key.PREF_COOKIE);
        storage.commit();
    }

    /**
     * Class for keeping all the keys used for shared preferences in one place.
     */
    private static final class Key {
        public static final String PREF_USERNAME = "key_auto_login_username";
        public static final String PREF_PASS = "key_auto_login_password";
        public static final String PREF_TEMP_USERNAME = "temp_username";
        public static final String PREF_TEMP_PASS = "temp_password";
        public static final String PREF_COOKIE = "cookie";
        public static final String PREF_EULA_VERSION = "EULA_VERSION";
    }
}
