package com.hp.printosmobilelib.core.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.hp.printosmobilelib.core.logging.HPLogger;

/**
 * Check device's network connectivity and speed
 */
public class NetworkUtils {

    private static final String TAG = NetworkUtils.class.getSimpleName();
    
    private static final String MOBILE_NETWORK = "Mobile";
    private static final String WIFI = "WiFi";
    private static final String UNKNOWN = "Unknown";

    /**
     * Get the network info
     */
    public static NetworkInfo getNetworkInfo(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo();
    }

    /**
     * Check if there is any connectivity to a Wifi network
     */
    public static boolean isConnectedWifi(Context context) {
        NetworkInfo info = NetworkUtils.getNetworkInfo(context);
        return (info != null && info.isConnected() && info.getType() == ConnectivityManager.TYPE_WIFI);
    }

    /**
     * Check if there is any connectivity to a mobile network
     */
    public static boolean isConnectedMobile(Context context) {
        NetworkInfo info = NetworkUtils.getNetworkInfo(context);
        return (info != null && info.isConnected() && info.getType() == ConnectivityManager.TYPE_MOBILE);
    }

    /**
     * Get network type
     */
    public static String getNetworkType(Context context) {

        try {

            if (isConnectedMobile(context)) {
                return MOBILE_NETWORK;
            } else if (isConnectedWifi(context)) {
                return WIFI;
            } else {
                return UNKNOWN;
            }

        } catch (Exception exception) {

            HPLogger.d(TAG, "unable to detect network type: " + exception);

            return UNKNOWN;
        }
    }

}