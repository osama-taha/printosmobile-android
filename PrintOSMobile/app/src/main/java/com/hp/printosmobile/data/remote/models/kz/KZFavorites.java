package com.hp.printosmobile.data.remote.models.kz;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Minerva on 3/15/18.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class KZFavorites {

    @JsonProperty("total")
    private Integer totalAssetsReturned;
    @JsonProperty("count")
    private Integer assetsCount;
    @JsonProperty("members")
    private List<KZItem> favoriteAssetResponses = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<>();

    @JsonProperty("total")
    public Integer getTotalAssetsReturned() {
        return totalAssetsReturned;
    }

    @JsonProperty("total")
    public void setTotalAssetsReturned(Integer totalAssetsReturned) {
        this.totalAssetsReturned = totalAssetsReturned;
    }

    @JsonProperty("count")
    public Integer getAssetsCount() {
        return assetsCount;
    }

    @JsonProperty("count")
    public void setAssetsCount(Integer assetsCount) {
        this.assetsCount = assetsCount;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    @JsonProperty("members")
    public List<KZItem> getFavoriteAssetResponses() {
        return favoriteAssetResponses;
    }

    @JsonProperty("members")
    public void setFavoriteAssetResponses(List<KZItem> favoriteAssetResponses) {
        this.favoriteAssetResponses = favoriteAssetResponses;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}