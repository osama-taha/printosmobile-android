package com.hp.printosmobile.presentation.modules.eula;

import java.io.Serializable;

/**
 * Created by Osama Taha on 9/19/16.
 */
public class EULAViewModel implements Serializable {

    private String eulaText;
    private String eulaVersion;
    private String privacyVersion;

    public String getEulaText() {
        return eulaText;
    }

    public void setEulaText(String eulaText) {
        this.eulaText = eulaText;
    }

    public String getEulaVersion() {
        return eulaVersion;
    }

    public void setEulaVersion(String eulaVersion) {
        this.eulaVersion = eulaVersion;
    }

    public String getPrivacyVersion() {
        return privacyVersion;
    }

    public void setPrivacyVersion(String privacyVersion) {
        this.privacyVersion = privacyVersion;
    }

    @Override
    public String toString() {
        return "EULAViewModel{" +
                "eulaText='" + eulaText + '\'' +
                ", eulaVersion='" + eulaVersion + '\'' +
                ", privacyVersion='" + privacyVersion + '\'' +
                '}';
    }
}
