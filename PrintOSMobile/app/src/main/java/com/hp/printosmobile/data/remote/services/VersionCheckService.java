package com.hp.printosmobile.data.remote.services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hp.printosmobile.BuildConfig;
import com.hp.printosmobile.Constants;
import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.data.remote.ApiServicesProvider;
import com.hp.printosmobile.data.remote.models.VersionUpdateResult;
import com.hp.printosmobile.data.remote.models.VersionsData;
import com.hp.printosmobile.presentation.BaseActivity;
import com.hp.printosmobile.presentation.modules.npspopup.NPSNotificationManager;
import com.hp.printosmobile.utils.HPStringUtils;

import java.io.IOException;

import retrofit2.Response;

/**
 * Created by Osama Taha on 6/27/16.
 */
public class VersionCheckService extends Service {

    private static final String DEBUG_VERSION_SPLIT_REGEX = "-";
    private static final int REALTIME_REFRESH_DEF_RATE = 30;

    public VersionCheckService() {
    }

    @Override
    public int onStartCommand(Intent intent, final int flags, int startId) {
        new Thread(new Runnable() {
            @Override
            public void run() {

                ApiServicesProvider serviceProvider = PrintOSApplication.getApiServicesProvider();
                SupportedVersionsService supportedVersionsService = serviceProvider.getSupportedVersionsService();

                PrintOSPreferences preferences = PrintOSPreferences.getInstance(VersionCheckService.this);

                Response<VersionsData> response;
                try {
                    response = supportedVersionsService.getVersionsData().execute();
                } catch (IOException e) {
                    response = null;
                }

                VersionsData versionsData = response != null ?
                        response.body() : null;

                if (versionsData == null) {

                    stopSelf();

                } else {

                    if (versionsData.getConfiguration() != null) {

                        VersionsData.Configuration.Validation validation = versionsData.getConfiguration().getValidation();
                        if (validation != null) {
                            preferences.saveValidationParams(
                                    validation.getCheckCookieExpiryHrs(), validation.getMoveToHomeMins(), validation.isShowToastMessage()
                            );
                        }

                        preferences.enableCompletedJobsFeature(versionsData.getConfiguration().isCompletedJobsEnabled());

                        VersionsData.Configuration.Insights insights = versionsData.getConfiguration().getInsights();
                        if (insights != null) {
                            preferences.setInsightsJamsEnabled(insights.isJamsEnabled());
                            preferences.setInsightsFailureEnabled(insights.isFailuresEnabled());
                            preferences.setInsightsFirstOccuringVersion(insights.getBadgeVersion() == null ? "" : insights.getBadgeVersion());
                        }

                        Boolean nps = versionsData.getConfiguration().isNpsPopupEnabled();
                        preferences.setNPSFeatureEnabled(
                                nps == null || nps);

                        preferences.setClosedServiceCallsDays(versionsData.getConfiguration().getClosedServiceCallsDays());

                        if (!preferences.isNPSFeatureEnabled()) {
                            NPSNotificationManager.enableNotifications(false);
                        } else {
                            if (!preferences.hasNPSNotificationPreference()) {
                                NPSNotificationManager.enableNotifications(true);
                            }
                        }

                        Boolean kpiExplanation = versionsData.getConfiguration().getKpiExplanationEnabled();
                        preferences.setKpiExplanationEnabled(
                                kpiExplanation == null || kpiExplanation);

                        Boolean beatCoin = versionsData.getConfiguration().isBeatCoinsPopupEnabled();
                        preferences.setBeatCoinFeatureEnabled(
                                beatCoin == null || beatCoin);

                        VersionsData.Configuration.WeeklyPrintbeat weeklyPrintbeat = versionsData.getConfiguration().getWeeklyPrintbeat();
                        if (weeklyPrintbeat != null) {
                            Boolean weekIncrementalEnabled = weeklyPrintbeat.isKpiDataIncrementallyEnabled();
                            preferences.setWeekIncrementalFeatureEnabled(
                                    weekIncrementalEnabled == null || weekIncrementalEnabled);

                            if (weeklyPrintbeat.getLowPrintVolumeImp() != null) {
                                preferences.setLowImpressionPrintVolume(weeklyPrintbeat.getLowPrintVolumeImp());
                            }

                            preferences.setRankingLeaderboardFeatureFlag(weeklyPrintbeat.leaderboardRankingEnabled() != null ? weeklyPrintbeat.leaderboardRankingEnabled() : VersionsData.FeatureFlag.OFF);

                        }

                        VersionsData.Configuration.ReferFriend referFriend = versionsData.getConfiguration().getReferFriend();
                        if (referFriend != null) {
                            preferences.setReferToAFriendEnabled(
                                    referFriend.isEnabled() == null || referFriend.isEnabled());
                            preferences.setReferToAFriendUrl(
                                    referFriend.getUrl());
                        }

                        Integer filterSearchBoxCount = versionsData.getConfiguration().getFilterSearchBoxDeviceCount();
                        if (filterSearchBoxCount != null) {
                            preferences.setFilterSearchBoxDeviceCount(filterSearchBoxCount);
                        }

                        Integer npsDisplayTimeInterval = versionsData.getConfiguration().getNpsMinDisplayTimeInterval();
                        if (npsDisplayTimeInterval != null) {
                            preferences.setNPSMinDisplayTimeInterval(
                                    npsDisplayTimeInterval);
                        }

                        if (versionsData.getConfiguration().isTodayGraphEnabled() != null) {
                            PrintOSPreferences.getInstance(VersionCheckService.this).setTodayGraphEnabled(
                                    versionsData.getConfiguration().isTodayGraphEnabled());
                        }

                        VersionsData.Configuration.RealtimeDataRefresh realtimeDataRefresh = versionsData.getConfiguration().getRealtimeDataRefresh();
                        if (realtimeDataRefresh != null) {
                            preferences.setRealtimeRefreshEnabled(realtimeDataRefresh.isEnabled());
                            preferences.setRealtimeRefreshRate(realtimeDataRefresh.getRepeatIntervalInSecs() == 0 ?
                                    REALTIME_REFRESH_DEF_RATE : realtimeDataRefresh.getRepeatIntervalInSecs());
                        }

                        String todayGraphSupportedEquipments = "";
                        VersionsData.Configuration.TodayGraphFeature todayGraphFeature = versionsData.getConfiguration().getTodayGraphFeature();
                        if (todayGraphFeature != null && todayGraphFeature.getSupportedEquipments() != null) {
                            for (String bu : todayGraphFeature.getSupportedEquipments()) {
                                todayGraphSupportedEquipments += bu;
                            }
                        }
                        preferences.setTodayGraphSupportedEquipments(todayGraphSupportedEquipments);
                        preferences.setAppVersionForAutoSubscribeToIntraDailyTarget(
                                versionsData.getConfiguration().getIntraDailyUpdatesAutoSubscribeVersion());

                        VersionsData.FeatureFlag correctiveActionsFlag = versionsData.getConfiguration().getCorrectiveActionsEnabled();
                        if (correctiveActionsFlag != null) {
                            preferences.setCorrectiveActionsFeatureFlag(correctiveActionsFlag);
                        }

                        VersionsData.Configuration.DaysShiftsGraph daysShiftsGraph = versionsData.getConfiguration().getDaysShiftsGraph();
                        if (daysShiftsGraph != null) {
                            preferences.setHistogramDayChartType(daysShiftsGraph.getLastDaysChartType() == null ?
                                    "" : daysShiftsGraph.getLastDaysChartType());
                            preferences.setHistogramShifChartType(daysShiftsGraph.getLastShiftsChartType() == null ?
                                    "" : daysShiftsGraph.getLastShiftsChartType());
                            VersionsData.FeatureFlag histogramFeatureFlag = daysShiftsGraph.getFeatureEnabled();
                            if (histogramFeatureFlag != null) {
                                preferences.setHistogramChartFeatureFlag(histogramFeatureFlag);

                            }
                            preferences.setHistogramNumberOfShifts(daysShiftsGraph.getNumberOfShifts());
                        }

                        VersionsData.Configuration.InviteToPrintOS inviteToPrintos = versionsData.getConfiguration().getInviteToPrintOS();
                        if (inviteToPrintos != null) {
                            preferences.setInvitesWithCoinsEnabled(inviteToPrintos.isIncludeCoins());
                            preferences.setInvitesFeatureFlag(inviteToPrintos.getState());
                            preferences.setBeatCoinsPerInvite(inviteToPrintos.getBeatCoinsPerInvite());
                            preferences.setInviteAllPopupDisplayCounter(inviteToPrintos.getInviteAllPopupDisplayCounter());
                            preferences.setSendSmsEnabled(inviteToPrintos.isSendSmsEnabled());

                        }

                        Boolean forgotPasswordEnabled = versionsData.getConfiguration().getForgotPasswordEnabled();
                        if (forgotPasswordEnabled != null) {
                            preferences.setForgotPasswordEnabled(forgotPasswordEnabled);
                        }

                        VersionsData.FeatureFlag kpiBreakdownFlag = versionsData.getConfiguration().getKpiBreakdownState();
                        if (kpiBreakdownFlag != null) {
                            preferences.setKpiBreakdownReportFeatureFlag(kpiBreakdownFlag);
                        }

                        VersionsData.FeatureFlag stateDistributionFlag = versionsData.getConfiguration().getStateDistributionState();
                        if (stateDistributionFlag != null) {
                            preferences.setStateDistributionFeatureFlag(stateDistributionFlag);
                        }

                        VersionsData.FeatureFlag chinaLoggingFlag = versionsData.getConfiguration().getChinaLoggingState();
                        if (chinaLoggingFlag != null) {
                            preferences.setChinaLoggingFeatureFlag(chinaLoggingFlag);
                        }

                        VersionsData.FeatureFlag hpidFlag = versionsData.getConfiguration().getHpidState();
                        if (hpidFlag != null) {
                            preferences.setHPIDFeatureFlag(hpidFlag);
                        } else {
                            preferences.setHPIDFeatureFlag(VersionsData.FeatureFlag.OFF);
                        }

                        if(!TextUtils.isEmpty(versionsData.getConfiguration().getDnsTextFile())){
                            preferences.setNsLookupTextFileName(versionsData.getConfiguration().getDnsTextFile());
                        }

                        VersionsData.Configuration.AppRating appRating = versionsData.getConfiguration().getAppRating();
                        if(appRating != null) {
                            if(appRating.getRateRecurringPeriod() > 0) {
                                preferences.setRateDialogRecurringPeriod(appRating.getRateRecurringPeriod());
                            }
                        }
                    }

                    if (versionsData.getVersions() == null
                            || versionsData.getVersions().getPrintbeat() == null ||
                            versionsData.getVersions().getPrintbeat().getAndroid() == null) {

                        //No need to do anything.
                        stopSelf();

                    } else {

                        VersionUpdateResult result = getVersionUpdateResult(versionsData, VersionCheckService.this);

                        if (result == null) {
                            stopSelf();
                        }

                        String rawResponse = "";
                        ObjectMapper mapper = new ObjectMapper();
                        try {
                            rawResponse = mapper.writeValueAsString(versionsData);
                        } catch (JsonProcessingException e) {
                        }

                        preferences.setSupportedVersions(rawResponse);
                        preferences.setVersionCheckingTime(System.currentTimeMillis());
                        preferences.enableWarningUpdateDialog(true);

                        //Send version force-update/warning message to the running activity (if needed).
                        Intent notifyIntent = new Intent();
                        notifyIntent.setAction(Constants.IntentExtras.VERSION_UPDATE_INTENT_ACTION);
                        notifyIntent.putExtra(Constants.IntentExtras.VERSION_UPDATE_INTENT_EXTRA_KEY, result);
                        VersionCheckService.this.sendBroadcast(notifyIntent);


                    }
                }
                setServiceStarted();

                //Stop service once it finishes its task
                stopSelf();

            }
        }).start();

        return Service.START_STICKY;

    }

    private static synchronized void setServiceStarted() {
        BaseActivity.setIsServiceStarted(false);
    }

    public static VersionUpdateResult getVersionUpdateResult(VersionsData versionsData, Context context) {
        VersionUpdateResult versionUpdateResult = new VersionUpdateResult();

        String languageCode = PrintOSPreferences.getInstance(context).getLanguageCode();

        String version = getVersionName();

        VersionsData.Platform android = versionsData.getVersions().getPrintbeat().getAndroid();

        if (android.getValid() != null && android.getValid().contains(version)) {
            //No need to do anything.
            return null;
        } else if (android.getWarning() != null && android.getWarning().contains(version)) {
            versionUpdateResult.setMessageType(VersionUpdateResult.MessageType.WARNING);
            versionUpdateResult.setUpdateMessage(versionsData.getMessages().getMessageWarning().get(languageCode));
        } else if (android.getUpdate() != null && android.getUpdate().contains(version)) {
            versionUpdateResult.setUpdateMessage(versionsData.getMessages().getMessageUpdate().get(languageCode));
            versionUpdateResult.setMessageType(VersionUpdateResult.MessageType.FORCE_UPDATE);
        } else {
            versionUpdateResult.setMessageType(VersionUpdateResult.MessageType.OK);
        }

        versionUpdateResult.setLinkInPlayStore(android.getLinkInStore());

        return versionUpdateResult;
    }

    public static String getVersionName() {
        String version = BuildConfig.VERSION_NAME.trim();
        if (BuildConfig.DEBUG) {
            version = HPStringUtils.getStringBeforeRegex(version, DEBUG_VERSION_SPLIT_REGEX);
        }
        return version;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

}