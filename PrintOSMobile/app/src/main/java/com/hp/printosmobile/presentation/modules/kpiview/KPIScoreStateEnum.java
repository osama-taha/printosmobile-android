package com.hp.printosmobile.presentation.modules.kpiview;

/**
 * An enum represents the kpi score state.
 * <p/>
 * Created by Osama Taha on 1/17/16.
 */
public enum KPIScoreStateEnum {

    EMPTY(""), BELOW_AVERAGE("BELOW_AVERAGE"), AVERAGE("AVERAGE"), GOOD("GOOD"), GREAT("GREAT");

    private String state;

    KPIScoreStateEnum(String state) {
        this.state = state;
    }

    public String getState() {
        return state;
    }

    public static KPIScoreStateEnum from(String state) {
        for (KPIScoreStateEnum kpiState : KPIScoreStateEnum.values()) {
            if (kpiState.getState().equals(state))
                return kpiState;
        }
        return KPIScoreStateEnum.EMPTY;
    }

}
