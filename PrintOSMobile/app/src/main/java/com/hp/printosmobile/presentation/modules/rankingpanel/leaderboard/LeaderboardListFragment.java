package com.hp.printosmobile.presentation.modules.rankingpanel.leaderboard;

import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.hp.printosmobile.Analytics;
import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;
import com.hp.printosmobile.presentation.BaseFragment;
import com.hp.printosmobile.presentation.modules.home.Panel;
import com.hp.printosmobile.presentation.modules.insights.kz.kzfooter.VerticalSpaceItemDecoration;
import com.hp.printosmobile.presentation.modules.shared.PrintOSBaseAdapter;
import com.hp.printosmobile.presentation.modules.week.RankingViewModel;
import com.hp.printosmobile.utils.HPUIUtils;
import com.hp.printosmobilelib.core.communications.remote.APIException;
import com.hp.printosmobilelib.ui.widgets.panel.SharableObject;

import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * Created by Osama Taha on 7/29/2018.
 */
public class LeaderboardListFragment extends BaseFragment implements LeaderboardListView, LeaderboardListAdapter.LeaderboardAdapterCallbacks {

    private static final String RANK_TYPE_ARG = "rank_type";
    private static final long HIDE_WEEK_LABEL_DELLY_IN_MILLIS = 5000L;
    private static final long WEEK_LABEL_FADE_OUT_DURATION = 1000L;

    @Bind(R.id.leaderboard_recycler_view)
    RecyclerView recyclerView;
    @Bind(R.id.loading_indicator)
    ProgressBar loadingView;
    @Bind(R.id.week_number_label)
    TextView weekNumberTextView;
    @Bind(R.id.error_layout)
    LinearLayout errorLayout;

    private LeaderboardListPresenter presenter;
    private LeaderboardListAdapter adapter;
    private LinearLayoutManager layoutManager;

    private boolean isLoading;
    private boolean scrollEventSent;
    private boolean reachTopEventSent;

    private RecyclerView.OnScrollListener recyclerViewOnScrollListener = new RecyclerView.OnScrollListener() {

        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            switch (newState) {
                case RecyclerView.SCROLL_STATE_IDLE:

                    if (!reachTopEventSent) {

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if (!reachTopEventSent && layoutManager != null) {

                                    int pastVisibleItems = layoutManager.findFirstVisibleItemPosition();
                                    if (pastVisibleItems == 0) {
                                        Analytics.sendEvent(Analytics.RANKING_LEADERBOARD_REACH_TOP);
                                        reachTopEventSent = true;
                                    }
                                }
                            }
                        }, 500);
                    }

                    break;
                case RecyclerView.SCROLL_STATE_DRAGGING:

                    if (!scrollEventSent) {
                        Analytics.sendEvent(Analytics.RANKING_LEADERBOARD_SCROLL_LIST);
                        scrollEventSent = true;
                    }

                    break;
                case RecyclerView.SCROLL_STATE_SETTLING:
                    break;

            }

        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);

            if (dy > 0) {

                //isListGoingUp = true;

                int visibleItemCount = layoutManager.getChildCount();
                int totalItemCount = layoutManager.getItemCount();
                int firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();

                if (!isLoading && !presenter.isLastPage()) {
                    if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount && firstVisibleItemPosition >= 0) {
                        isLoading = presenter.getNextPage(adapter.getItem(adapter.getItemCount() - 1).getActualRank());
                    }
                }

            } else {

                //isListGoingUp = false;

                int firstVisiblePosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstCompletelyVisibleItemPosition();

                int currentTopRankIndex = presenter.getCurrentTopRank();

                if (!isLoading && (firstVisiblePosition > Math.min(LeaderboardListPresenter.PAGE_SIZE, currentTopRankIndex))) {
                    isLoading = presenter.getPreviousPage();
                }

            }

        }
    };


    public static Fragment newInstance(RankingViewModel.SiteRankViewModel siteRankViewModel) {

        LeaderboardListFragment fragment = new LeaderboardListFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(RANK_TYPE_ARG, siteRankViewModel);
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initPresenter();
    }

    private void initPresenter() {
        presenter = new LeaderboardListPresenter();
        presenter.attachView(this);
        presenter.setRankTypeViewModel((RankingViewModel.SiteRankViewModel) getArguments().getSerializable(RANK_TYPE_ARG));
        presenter.initLeaderboardList();
    }

    private void setupRecyclerView(List<RankingViewModel.SiteRankViewModel> items) {

        adapter = new LeaderboardListAdapter(getContext(), this);

        adapter.setOnReloadClickListener(new PrintOSBaseAdapter.OnReloadClickListener() {
            @Override
            public void onReloadClick() {
                isLoading = presenter.getNextPage(adapter.getItem(adapter.getItemCount() - 2).getActualRank());
            }
        });

        layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addOnScrollListener(recyclerViewOnScrollListener);
        recyclerView.setAdapter(adapter);
        recyclerView.addItemDecoration(new VerticalSpaceItemDecoration(VerticalSpaceItemDecoration.VERTICAL_ITEM_SPACE));

        adapter.addAll(items);

        setFocusOnMySiteCell();

        boolean hasRanks = items != null && items.size() > 0;
        if (hasRanks) {
            int weekNumber = presenter.getRankTypeViewModel().getWeek();
            weekNumberTextView.setText(PrintOSApplication.getAppContext().getString(R.string.ranking_leaderboard_week_number, weekNumber));
        }

        showCalculatedAtWeekLabel(hasRanks);

        if (getActivity() != null) {
            //Hide the "calculated for week" label after X seconds.
            new Handler(getActivity().getMainLooper()).postDelayed(new Runnable() {
                @Override
                public void run() {
                    showCalculatedAtWeekLabel(false);
                }
            }, HIDE_WEEK_LABEL_DELLY_IN_MILLIS);
        }

    }

    private void showCalculatedAtWeekLabel(boolean visible) {

        if (visible) {

            HPUIUtils.setVisibility(visible, weekNumberTextView);

        } else {

            Animation fadeOut = new AlphaAnimation(1, 0);
            fadeOut.setInterpolator(new AccelerateInterpolator());
            fadeOut.setDuration(WEEK_LABEL_FADE_OUT_DURATION);

            fadeOut.setAnimationListener(new Animation.AnimationListener() {
                public void onAnimationEnd(Animation animation) {
                    HPUIUtils.setVisibility(false, weekNumberTextView);
                }

                public void onAnimationRepeat(Animation animation) {
                }

                public void onAnimationStart(Animation animation) {
                }
            });

            weekNumberTextView.startAnimation(fadeOut);
        }
    }

    public void setFocusOnMySiteCell() {

        if (presenter == null) {
            return;
        }

        if (presenter.getRankTypeViewModel().isLowerThird()) {

            int index = presenter.getRankTypeViewModel().getLowerThirdValue() == 0 ?
                    adapter.getItemCount() : presenter.getRankTypeViewModel().getLowerThirdValue();

            if (index > 0) {
                recyclerView.scrollToPosition(index - 1);
            }

        } else {

            if (presenter.getRankTypeViewModel().getActualRank() == 1) {
                //scroll to site rank
                recyclerView.scrollToPosition(0);
            } else {
                //scroll to site rank - 1
                recyclerView.scrollToPosition(presenter.getRankTypeViewModel().getActualRank() - 2);
            }
        }


    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_leaderboard_list;
    }

    @Override
    public boolean isFragmentNameToBeDisplayed() {
        return false;
    }

    @Override
    public boolean isFilteringAvailable() {
        return false;
    }

    @Override
    public int getToolbarDisplayName() {
        return 0;
    }

    @Override
    public void onCancel() {

    }

    @Override
    public void onError(APIException exception, String tag) {

    }

    @Override
    public void onInitLeaderboardListCompleted(List<RankingViewModel.SiteRankViewModel> list) {
        setupRecyclerView(list);
    }

    @Override
    public void onNextPage(List<RankingViewModel.SiteRankViewModel> siteRankViewModels) {
        isLoading = false;
        adapter.addAll(siteRankViewModels);
    }

    @Override
    public void showErrorView() {
        errorLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideErrorView() {
        errorLayout.setVisibility(View.GONE);
    }

    @Override
    public void addFooterView() {
        adapter.addFooterView();
    }

    @Override
    public void removeFooterView() {
        adapter.removeFooterView();
        isLoading = false;
    }

    @Override
    public void showLoadingFooterView() {
        isLoading = true;
        adapter.updateFooterView(PrintOSBaseAdapter.FooterTypeEnum.LOAD_MORE);
    }

    @Override
    public void showErrorFooterView() {
        adapter.updateFooterView(PrintOSBaseAdapter.FooterTypeEnum.ERROR);
    }

    @Override
    public void showNoResultsFooterView() {
        adapter.updateFooterView(PrintOSBaseAdapter.FooterTypeEnum.NO_RESULTS);
    }

    @Override
    public void onPrevPageLoaded(List<RankingViewModel.SiteRankViewModel> siteRankViewModels) {
        adapter.insertItems(siteRankViewModels);
        isLoading = false;
    }

    @OnClick(R.id.retry_btn)
    public void onReloadButtonClicked() {
        if (presenter != null) {
            presenter.initLeaderboardList();
        }
    }

    @Override
    public void showLoadingView() {
        loadingView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoadingView() {
        loadingView.setVisibility(View.GONE);
    }

    @Override
    public void onScreenshotCreated(Panel panel, Uri screenshotUri, String title, String body, SharableObject sharableObject) {
        shareImage(screenshotUri, title, body, sharableObject);
    }
}
