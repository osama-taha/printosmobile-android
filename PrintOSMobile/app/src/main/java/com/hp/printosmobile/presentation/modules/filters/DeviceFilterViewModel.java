package com.hp.printosmobile.presentation.modules.filters;

import com.hp.printosmobile.presentation.modules.shared.MeasureTypeEnum;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Osama Taha on 10/9/16.
 */
public class DeviceFilterViewModel implements FilterItem, Serializable {

    public static final DeviceFiltering RT_SUPPORTED = new DeviceFiltering() {
        @Override
        public boolean isIncluded(DeviceFilterViewModel device) {
            return device != null && device.isRtSupported();
        }
    };

    private String deviceId;
    private String serialNumber;
    private String serialNumberDisplay;
    private String pressDescription;
    private String pressModel;
    private String organizationID;
    private String deviceName;
    private String deviceNickName;
    private String businessUnit;
    private String impressionType;
    private int sortPosition;
    private List<String> groups;
    private boolean isRtSupported;
    private boolean isSelected;
    private boolean isAllDevices;
    private SiteViewModel site;
    private String endUserId;
    private MeasureTypeEnum measureTypeEnum;

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getPressDescription() {
        return pressDescription;
    }

    public void setPressDescription(String pressDescription) {
        this.pressDescription = pressDescription;
    }

    public String getPressModel() {
        return pressModel;
    }

    public void setPressModel(String pressModel) {
        this.pressModel = pressModel;
    }

    public String getOrganizationID() {
        return organizationID;
    }

    public void setOrganizationID(String organizationID) {
        this.organizationID = organizationID;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public String getDeviceNickName() {
        return deviceNickName;
    }

    public void setDeviceNickName(String deviceNickName) {
        this.deviceNickName = deviceNickName;
    }

    public String getBusinessUnit() {
        return businessUnit;
    }

    public void setBusinessUnit(String businessUnit) {
        this.businessUnit = businessUnit;
    }

    public String getImpressionType() {
        return impressionType;
    }

    public void setImpressionType(String impressionType) {
        this.impressionType = impressionType;
    }

    public int getSortPosition() {
        return sortPosition;
    }

    public void setSortPosition(int sortPosition) {
        this.sortPosition = sortPosition;
    }

    public List<String> getGroups() {
        return groups;
    }

    public void setGroups(List<String> groups) {
        this.groups = groups;
    }

    public boolean isRtSupported() {
        return isRtSupported;
    }

    public void setRtSupported(boolean rtSupported) {
        isRtSupported = rtSupported;
    }

    public SiteViewModel getSite() {
        return site;
    }

    public void setSite(SiteViewModel site) {
        this.site = site;
    }

    @Override
    public String getId() {
        return deviceId;
    }

    @Override
    public String getName() {
        return deviceName;
    }

    @Override
    public List<FilterItem> getItems() {
        return null;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    @Override
    public boolean isSelected() {
        return false;
    }

    public void setAllDevices(boolean allDevices) {
        isAllDevices = allDevices;
    }

    public boolean isAllDevices() {
        return isAllDevices;
    }

    public String getSerialNumberDisplay() {
        return serialNumberDisplay;
    }

    public void setSerialNumberDisplay(String serialNumberDisplay) {
        this.serialNumberDisplay = serialNumberDisplay;
    }

    public MeasureTypeEnum getMeasureTypeEnum() {
        return measureTypeEnum;
    }

    public void setMeasureTypeEnum(MeasureTypeEnum measureTypeEnum) {
        this.measureTypeEnum = measureTypeEnum;
    }

    @Override
    public int compareTo(FilterItem another) {

        if (another == null) return -1;

        DeviceFilterViewModel deviceFilterViewModel = (DeviceFilterViewModel) another;

        if (sortPosition < 0 && deviceFilterViewModel.sortPosition < 0) return 0;
        if (sortPosition >= 0 && deviceFilterViewModel.sortPosition < 0) return -1;
        if (sortPosition < 0 && deviceFilterViewModel.sortPosition >= 0) return 1;

        return sortPosition - deviceFilterViewModel.sortPosition;

    }

    @Override
    public String toString() {
        return "DeviceFilterViewModel{" +
                "deviceId='" + deviceId + '\'' +
                ", serialNumber='" + serialNumber + '\'' +
                ", pressDescription='" + pressDescription + '\'' +
                ", pressModel='" + pressModel + '\'' +
                ", organizationID='" + organizationID + '\'' +
                ", deviceName='" + deviceName + '\'' +
                ", deviceNickName='" + deviceNickName + '\'' +
                ", businessUnit='" + businessUnit + '\'' +
                ", impressionType='" + impressionType + '\'' +
                ", sortPosition=" + sortPosition +
                ", groups=" + groups +
                ", isRtSupported=" + isRtSupported +
                ", site=" + site +
                '}';
    }

    public String getEndUserId() {
        return endUserId;
    }

    public void setEndUserId(String endUserId) {
        this.endUserId = endUserId;
    }


    public interface DeviceFiltering {
        boolean isIncluded(DeviceFilterViewModel device);
    }
}