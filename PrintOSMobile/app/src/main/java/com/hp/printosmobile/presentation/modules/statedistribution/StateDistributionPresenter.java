package com.hp.printosmobile.presentation.modules.statedistribution;

import com.hp.printosmobile.presentation.Presenter;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitEnum;
import com.hp.printosmobilelib.core.communications.remote.APIException;
import com.hp.printosmobilelib.core.logging.HPLogger;

import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Anwar Asbah on 2/18/2018.
 */
public class StateDistributionPresenter extends Presenter<StateDistributionView> {

    private static final String TAG = StateDistributionPresenter.class.getSimpleName();

    public void getPressStateDistribution(BusinessUnitEnum businessUnitEnum, String deviceSerialNumber, boolean isShiftSupport) {
        if (deviceSerialNumber == null || businessUnitEnum == null) {
            onFailure(null);
            return;
        }

        Subscription subscription = StateDistributionDataManager.getPressStateDistribution(
                deviceSerialNumber, businessUnitEnum, isShiftSupport)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<StateDistributionViewModel>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        HPLogger.d(TAG, "error fetching state distibution data: " + e.getMessage());
                        onFailure(e);
                    }

                    @Override
                    public void onNext(StateDistributionViewModel viewModel) {
                        if (mView != null) {
                            mView.onPressStateDistributionRetrieved(viewModel);
                        }
                    }
                });
        addSubscriber(subscription);
    }

    private void onFailure(Throwable e) {
        APIException exception = APIException.create(APIException.Kind.UNEXPECTED);
        if (e instanceof APIException) {
            exception = (APIException) e;
        }
        if (mView != null) {
            mView.onError(exception, TAG);
        }
    }

    public void onRefresh() {
        stopSubscribers();
    }

    @Override
    public void detachView() {
        stopSubscribers();
        mView = null;
    }
}
