package com.hp.printosmobile.presentation.modules.maintenance;

import android.os.Bundle;
import android.view.View;

import com.hp.printosmobile.R;
import com.hp.printosmobile.presentation.BaseActivity;
import com.hp.printosmobilelib.core.communications.remote.models.UserData;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * Created by Osama on 13/9/2017.
 */
public class MaintenanceActivity extends BaseActivity {

    @Bind(R.id.intercepting_layout)
    View interceptingLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setReturningFromBackgroundValue(true);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_maintenance;
    }

    @OnClick(R.id.button_try_again)
    public void onTryAgainButtonClicked() {
        validate();
    }

    @Override
    public boolean isBackwardCompatible() {
        return true;
    }

    @Override
    public boolean shouldValidateSession() {
        return true;
    }

    @Override
    public void onPreValidation() {
        super.onPreValidation();
        showInterceptingLayout(true);
    }

    @Override
    public void onValidationCompleted(UserData.User user) {
        super.onValidationCompleted(user);
        resetMainActivity();
    }

    @Override
    public void onValidationError() {
        super.onValidationError();
        showInterceptingLayout(false);
    }

    @Override
    public void onValidationUnauthorized() {
        super.onValidationUnauthorized();
        showInterceptingLayout(false);
        resetMainActivity();
    }

    @Override
    public void onValidationCompletedWithoutContextChange(UserData.User user) {
        super.onValidationCompletedWithoutContextChange(user);
        resetMainActivity();
    }

    @Override
    public void onServerDown() {
        showInterceptingLayout(false);
    }

    void showInterceptingLayout(boolean visible) {
        interceptingLayout.setVisibility(visible ? View.VISIBLE : View.GONE);
    }

    private static synchronized void setReturningFromBackgroundValue(boolean value) {
        setReturningFromBackground(value);
    }

}
