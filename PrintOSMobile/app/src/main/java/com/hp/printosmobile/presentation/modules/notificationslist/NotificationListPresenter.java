package com.hp.printosmobile.presentation.modules.notificationslist;

import android.content.Context;

import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.presentation.Presenter;
import com.hp.printosmobile.presentation.modules.settings.NotificationsManager;
import com.hp.printosmobilelib.core.communications.remote.APIException;
import com.hp.printosmobilelib.core.logging.HPLogger;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.FuncN;
import rx.schedulers.Schedulers;

/**
 * Created by Anwar Asbah on 1/23/2017.
 */
public class NotificationListPresenter extends Presenter<NotificationListView> {

    private static final String TAG = NotificationListPresenter.class.getName();
    private static final String AND_USER_ID = " and userID: ";

    public void getUserNotifications() {

        Context context = PrintOSApplication.getAppContext();
        final String languageCode = PrintOSPreferences.getInstance(context).getLanguageCode();

        NotificationsManager.getAvailableEvents()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<List<String>>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        HPLogger.d(TAG, "unable to fetch the notifications " + e);
                        if (mView != null) {
                            if (e instanceof APIException) {
                                mView.onNotificationsError((APIException) e);
                            } else {
                                mView.onNotificationsError(APIException.create(APIException.Kind.UNEXPECTED));
                            }
                        }
                    }

                    @Override
                    public void onNext(List<String> events) {
                        getUserSupportedNotifications(events, languageCode);
                    }
                });
    }

    private void getUserSupportedNotifications(List<String> supportedEvents, String local) {

        Subscription subscription = NotificationListDataManager.getUserNotifications(supportedEvents, local)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<List<NotificationViewModel>>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (mView != null) {
                            if (e instanceof APIException) {
                                mView.onNotificationsError((APIException) e);
                            } else {
                                mView.onNotificationsError(APIException.create(APIException.Kind.UNEXPECTED));
                            }
                        }
                    }

                    @Override
                    public void onNext(List<NotificationViewModel> notificationViewModels) {
                        if (mView != null) {
                            mView.updateNotificationListView(notificationViewModels);
                        }
                    }
                });
        addSubscriber(subscription);
    }

    public void markNotificationAsRead(final int notificationID, final int userID) {
        Subscription subscription = NotificationListDataManager
                .markNotificationAsRead(notificationID, userID)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<ResponseBody>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        HPLogger.d(TAG, "unable to mark the notification as read with id:"
                                + notificationID + AND_USER_ID + userID + " as read due to " + e);
                    }

                    @Override
                    public void onNext(ResponseBody responseBody) {
                        HPLogger.d(TAG, "Successfully marked notifications with id:"
                                + notificationID + AND_USER_ID + userID + " as read");
                    }
                });
        addSubscriber(subscription);
    }

    public void deleteNotification(final int notificationID, final int userID) {
        Subscription subscription = NotificationListDataManager
                .deleteNotification(notificationID, userID)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<ResponseBody>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        HPLogger.d(TAG, "unable to delete a notification with id:"
                                + notificationID + AND_USER_ID + userID + " due to " + e);
                    }

                    @Override
                    public void onNext(ResponseBody responseBody) {
                        HPLogger.d(TAG, "Successfully deleted notifications with id:"
                                + notificationID + AND_USER_ID + userID);
                    }
                });
        addSubscriber(subscription);
    }

    public void refresh() {
        stopSubscribers();
    }

    @Override
    public void detachView() {
        stopSubscribers();
        mView = null;
    }

    public void deleteNotifications(List<NotificationViewModel> models) {

        List<Observable<ResponseBody>> observables = new ArrayList<>();
        for (NotificationViewModel viewModel : models) {
            observables.add(NotificationListDataManager.deleteNotification(viewModel.getNotificationID(), viewModel.getUserEventID()));
        }

        Subscription subscription = Observable.combineLatest(observables, new FuncN<Object>() {
            @Override
            public Object call(Object... args) {
                return args;
            }
        }).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<Object>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (mView != null) {
                            if (e instanceof APIException) {
                                mView.onDeleteAllNotificationsFailed((APIException) e);
                            } else {
                                mView.onDeleteAllNotificationsFailed(APIException.create(APIException.Kind.UNEXPECTED));
                            }
                        }
                    }

                    @Override
                    public void onNext(Object object) {
                        HPLogger.d(TAG, "Successfully deleted notifications");
                        if (mView != null) {
                            mView.onAllNotificationsDeleted();
                        }
                    }
                });

        addSubscriber(subscription);

    }
}