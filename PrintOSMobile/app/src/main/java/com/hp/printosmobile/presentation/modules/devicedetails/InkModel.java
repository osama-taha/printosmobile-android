package com.hp.printosmobile.presentation.modules.devicedetails;

import android.os.Build;

import com.hp.printosmobile.R;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Objects;

/**
 * Created by Anwar Asbah on 8/16/16.
 */
public class InkModel implements Comparable<InkModel>, Serializable {

    public static final Comparator<InkModel> COMPARATOR = new Comparator<InkModel>() {
        @Override
        public int compare(InkModel lhs, InkModel rhs) {
            if (lhs == null && rhs == null) return 0;
            if (lhs == null) return 1;
            if (rhs == null) return -1;
            return lhs.compareTo(rhs);
        }
    };

    public enum InkEnum {
        C("C", R.string.c_color_code, R.string.c_color, R.color.c_color, 0),
        LC("LC", R.string.lc_color_code, R.string.lc_color, R.color.lc_color, 1),
        M("M", R.string.m_color_code, R.string.m_color, R.color.m_color, 2),
        LM("LM", R.string.lm_color_code, R.string.lm_color, R.color.lm_color, 3),
        Y("Y", R.string.y_color_code, R.string.y_color, R.color.y_color, 4),
        RR("R", R.string.r_color_code, R.string.r_color, R.color.r_color, 5),
        GN("GN", R.string.gn_color_code, R.string.gn_color, R.color.gn_color, 6),
        B("B", R.string.b_color_code, R.string.b_color, R.color.b_color, 7),
        CR("CR", R.string.cr_color_code, R.string.cr_color, R.color.cr_color, 8),
        CG("CG", R.string.cg_color_code, R.string.cg_color, R.color.cg_color, 9),
        CB("CB", R.string.cb_color_code, R.string.cb_color, R.color.cb_color, 10),
        G("G", R.string.g_color_code, R.string.g_color, R.color.g_color, 11),
        K("K", R.string.k_color_code, R.string.k_color, R.color.k_color, 12),
        MK("MK", R.string.mk_color_code, R.string.mk_color, R.color.mk_color, 13),
        PK("PK", R.string.pk_color_code, R.string.pk_color, R.color.pk_color, 14),
        W("W", R.string.w_color_code, R.string.w_color, R.color.w_color, 15),
        OP("OP", R.string.op_color_code, R.string.op_color, R.color.op_color, 16),
        OC("OC", R.string.oc_color_code, R.string.oc_color, R.color.oc_color, 17),
        E("E", R.string.e_color_code, R.string.e_color, R.color.oc_color, 18),
        E2("GLOSS ENHANCER", R.string.e_color_code, R.string.e_color, R.color.oc_color, 19),
        UNKNOWN("UNKNOWN", R.string.unknown_color_code, R.string.unknown_color, R.color.unknown_color, 20);

        private String name;
        private int localizedColorCodeResId;
        private int colorID;
        private int localizedNameId;
        private int sortOrder;

        InkEnum(String colorCode, int localizedColorCodeResId, int localizedNameId, int colorID, int sortOrder) {
            this.name = colorCode;
            this.localizedColorCodeResId = localizedColorCodeResId;
            this.localizedNameId = localizedNameId;
            this.colorID = colorID;
            this.sortOrder = sortOrder;
        }

        public String getName() {
            return name;
        }

        private void setName(String name) {
            this.name = name;
        }

        public int getColor() {
            return colorID;
        }

        public int getSortOrder() {
            return sortOrder;
        }

        public int getLocalizedNameId() {
            return localizedNameId;
        }

        public int getLocalizedColorCodeResId() {
            return localizedColorCodeResId;
        }

        public static InkEnum from(String state) {
            for (InkEnum ink : InkEnum.values()) {
                if (ink.getName().equalsIgnoreCase(state.toLowerCase()))
                    return ink;
            }
            return UNKNOWN;
        }
    }

    public enum InkState {

        LOW("Low", R.string.ink_state_low),
        OK("OK", R.string.ink_state_ok),
        EXPIRED("Expired", R.string.ink_state_expired),
        UNKNOWN("UNKNOWN", R.string.ink_state_unknown);

        private String name;
        private int localizedNameId;

        InkState(String name, int localizedNameId) {
            this.name = name;
            this.localizedNameId = localizedNameId;
        }

        public String getName() {
            return name;
        }

        public int getLocalizedNameId() {
            return localizedNameId;
        }

        public static InkState from(String state) {
            if (state == null) {
                return InkState.UNKNOWN;
            }
            for (InkState ink : InkState.values()) {
                if (ink.getName().equalsIgnoreCase(state.toLowerCase()))
                    return ink;
            }
            return InkState.UNKNOWN;
        }
    }

    private InkEnum inkEnum;
    private int level;
    private int capacity;
    private InkState inkState;
    private String expiryDate;
    private String inkApiName;

    public InkModel() {
        level = 0;
        capacity = 1;
        inkEnum = InkEnum.UNKNOWN;
    }

    public InkEnum getInkEnum() {
        return inkEnum;
    }

    public void setInkEnum(InkEnum inkEnum) {
        this.inkEnum = inkEnum;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public InkState getInkState() {
        return inkState;
    }

    public void setInkState(InkState inkState) {
        this.inkState = inkState;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getInkApiName() {
        return inkApiName;
    }

    public void setInkApiName(String inkApiName) {
        this.inkApiName = inkApiName;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("InkModel{");
        sb.append("inkEnum=").append(inkEnum);
        sb.append(", level=").append(level);
        sb.append(", capacity=").append(capacity);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public int compareTo(InkModel another) {
        if (another == null) return 1;
        return inkEnum.getSortOrder() - another.getInkEnum().getSortOrder();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @Override
    public int hashCode() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            return Objects.hash(inkEnum, level, capacity, inkState, expiryDate, inkApiName);
        }
        return 0;
    }
}
