package com.hp.printosmobile.presentation.modules.filters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.hp.printosmobile.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Osama Taha on 10/9/16.
 */
public class FilterItemsAdapter extends RecyclerView.Adapter<FilterItemsAdapter.FilterItemViewHolder> implements
        Filterable {

    List<SelectableItem> originalItems;
    List<SelectableItem> filteredItems;
    private final FilterItemsAdapterCallbacks mListener;
    private Context context;
    private Filter filter;

    public FilterItemsAdapter(Context context, List<FilterItem> mItems, FilterItemsAdapterCallbacks filterItemsAdapterCallbacks) {
        this.context = context;
        this.mListener = filterItemsAdapterCallbacks;

        addItems(mItems);
        getFilter().filter("");
    }

    @Override
    public FilterItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.filter_item_layout, parent, false);
        return new FilterItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final FilterItemViewHolder holder, final int position) {

        SelectableItem item = filteredItems.get(position);

        holder.mItem = item.filterItem;
        holder.itemNameTextView.setText(holder.mItem.getName());

        if (!(holder.mItem instanceof DeviceFilterViewModel)) {
            holder.itemView.setBackgroundResource(R.drawable.list_item_bg_color);
            holder.itemNameTextView.setTextColor(ContextCompat.getColorStateList(context, R.color.filter_list_group_site_item_text_color));
        } else {
            holder.itemNameTextView.setTextColor(ContextCompat.getColorStateList(context, R.color.filter_device_item_text_color_selector));
        }

        holder.setSelected(item.isSelected);

        holder.itemNameTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    holder.setSelected(!holder.getSelected());
                    mListener.onItemSelected(holder.getSelected(), holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return filteredItems == null ? 0 : filteredItems.size();
    }

    public void addItems(List<FilterItem> items) {
        if (originalItems == null) {
            originalItems = new ArrayList<>();
        }

        originalItems.clear();
        if (items != null) {
            for (int i = 0; i < items.size(); i++) {
                FilterItem item = items.get(i);
                SelectableItem selectableItem = new SelectableItem();
                selectableItem.filterItem = item;
                selectableItem.isSelected = item instanceof DeviceFilterViewModel && ((DeviceFilterViewModel) item).isAllDevices();
                originalItems.add(selectableItem);
            }
        }

        getFilter().filter("");

        notifyDataSetChanged();
    }

    public void clearSelections() {
        if (originalItems != null) {
            for (SelectableItem item : originalItems) {
                item.isSelected = item.filterItem instanceof DeviceFilterViewModel &&
                        ((DeviceFilterViewModel) item.filterItem).isAllDevices();
            }
        }
        notifyDataSetChanged();
    }

    public void selectItem(FilterItem filterItem, boolean select) {

        boolean isAll = filterItem instanceof DeviceFilterViewModel && ((DeviceFilterViewModel) filterItem).isAllDevices();

        if (originalItems != null && filterItem != null) {
            for (SelectableItem item : originalItems) {

                if (item == null) {
                    continue;
                }

                if ((item.getFilterItem() instanceof DeviceFilterViewModel &&
                        ((DeviceFilterViewModel) item.filterItem).isAllDevices() && isAll) ||
                        (item.filterItem != null && item.filterItem.getId() != null &&
                                item.filterItem.getId().equals(filterItem.getId()))) {
                    item.isSelected = select;
                    break;
                }
            }
            notifyDataSetChanged();
        }
    }

    public void selectRange(List<FilterItem> items) {

        if (items != null && originalItems != null) {
            for (SelectableItem item : originalItems) {
                item.isSelected = false;
                for (int i = 0; i < items.size(); i++) {
                    FilterItem filterItem = items.get(i);
                    if (filterItem != null && item.filterItem != null && item.filterItem.getId() != null && item.filterItem.getId().equals(filterItem.getId())) {
                        item.isSelected = true;
                    }
                }
            }
        }
        notifyDataSetChanged();
    }

    public List<FilterItem> getSelectedItems() {

        List<FilterItem> items = new ArrayList();
        for (int i = 0; i < originalItems.size(); i++) {
            SelectableItem item = originalItems.get(i);
            if (item.isSelected) {
                items.add(item.filterItem);
            }
        }

        return items;
    }

    @Override
    public Filter getFilter() {
        if (filter == null) {
            filter = new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    String filterString = constraint.toString().toLowerCase();

                    FilterResults results = new FilterResults();

                    final List<SelectableItem> list = originalItems;

                    int count = list.size();
                    final ArrayList<SelectableItem> filteredList = new ArrayList<>();

                    String filterableString;

                    for (int i = 0; i < count; i++) {
                        FilterItem item = list.get(i).filterItem;
                        filterableString = item == null ? "" : item.getName();
                        if (filterableString.toLowerCase().contains(filterString)) {
                            filteredList.add(list.get(i));
                        }
                    }

                    results.values = filteredList;
                    results.count = filteredList.size();

                    return results;
                }

                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    filteredItems = (List<SelectableItem>) results.values;
                    notifyDataSetChanged();
                }
            };
        }
        return filter;
    }

    public class SelectableItem {
        private FilterItem filterItem;
        private boolean isSelected;

        public void setFilterItem(FilterItem filterItem) {
            this.filterItem = filterItem;
        }

        public FilterItem getFilterItem() {
            return filterItem;
        }

        public void setSelected(boolean selected) {
            isSelected = selected;
        }

        public boolean isSelected() {
            return isSelected;
        }
    }

    public class FilterItemViewHolder extends RecyclerView.ViewHolder implements IFilterViewHolder {

        public final View mView;
        public final TextView itemNameTextView;
        private final ImageView checkMarkImageView;
        private FilterItem mItem;

        public FilterItemViewHolder(View view) {
            super(view);
            mView = view;
            itemNameTextView = view.findViewById(R.id.item_name_text_view);
            checkMarkImageView = view.findViewById(R.id.checkmark);
        }

        @Override
        public void setText(String v) {
            itemNameTextView.setText(v);
        }

        @Override
        public void setSelected(boolean selected) {

            mView.setSelected(selected);
            itemNameTextView.setSelected(selected);
            if (mItem instanceof DeviceFilterViewModel) {
                checkMarkImageView.setVisibility(selected ? View.VISIBLE : View.INVISIBLE);
            } else {
                checkMarkImageView.setVisibility(View.INVISIBLE);
            }

        }

        @Override
        public boolean getSelected() {
            return itemNameTextView.isSelected();
        }

        @Override
        public void updateView(int position) {
            setSelected(filteredItems.get(position).isSelected);
        }

    }

    public interface FilterItemsAdapterCallbacks {

        void onItemSelected(boolean selected, FilterItem mItem);
    }


    public interface IFilterViewHolder {

        void setText(String text);

        void setSelected(boolean selected);

        boolean getSelected();

        void updateView(int position);
    }

}
