package com.hp.printosmobile.presentation.modules.invites;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.hp.printosmobile.Analytics;
import com.hp.printosmobile.AppseeSdk;
import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.utils.HPUIUtils;
import com.hp.printosmobilelib.ui.common.HPFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * create by anwar asbah 11/5/2017
 */
public class InvitesListFragment extends HPFragment implements InviteUtils.InviteObservable, InviteAllPopup.InviteAllPopupCallback {

    public enum InvitesType {
        EMAIL(Analytics.INVITE_VIA_EMAIL_ACTION, AppseeSdk.EVENT_INVITE_VIA_EMAIL_ACTION),
        CONTACTS(Analytics.INVITE_VIA_CONTACT_ACTION, AppseeSdk.EVENT_INVITE_VIA_CONTACT_ACTION),
        SUGGESTED(Analytics.INVITE_VIA_SUGGESTED_ACTION, AppseeSdk.EVENT_INVITE_VIA_SUGGESTED_ACTION);

        private String analyticsAction;
        private String appseeEvent;

        InvitesType(String analyticsAction, String appseeEvent) {
            this.analyticsAction = analyticsAction;
            this.appseeEvent = appseeEvent;
        }

        public String getAnalyticsAction() {
            return analyticsAction;
        }

        public String getAppseeEvent() {
            return appseeEvent;
        }
    }

    private static final String INVITE_TYPE_ARG = "invite_type_arg";
    private static final long HIDE_DELAY = 200;

    @Bind(R.id.contacts_list)
    RecyclerView contactsList;
    @Bind(R.id.invite_button)
    TextView inviteButton;
    @Bind(R.id.input_field)
    EditText inputField;
    @Bind(R.id.invite_all_button)
    View inviteAllButton;

    private InvitesType invitesType;
    InviteAdapter inviteAdapter;
    List<InviteContactViewModel> inviteContactViewModels;

    public static InvitesListFragment newInstance(InvitesType invitesType) {
        InvitesListFragment fragment = new InvitesListFragment();

        Bundle bundle = new Bundle();
        bundle.putInt(INVITE_TYPE_ARG, invitesType.ordinal());
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();
        if (args != null && args.containsKey(INVITE_TYPE_ARG)) {
            int ordinal = args.getInt(INVITE_TYPE_ARG);
            if (ordinal >= 0 && ordinal < InvitesType.values().length) {
                invitesType = InvitesType.values()[ordinal];
            }
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        InviteUtils.addObserver(this);
        //Should be after add observer
        init();
    }

    private void init() {
        inviteButton.setVisibility(invitesType == InvitesType.EMAIL ? View.VISIBLE : View.GONE);

        inviteAllButton.setVisibility(View.GONE);
        if (inviteContactViewModels != null && invitesType == InvitesType.SUGGESTED) {
            boolean hasContactsToBeInvited = false;
            for (InviteContactViewModel viewModel : inviteContactViewModels) {
                hasContactsToBeInvited = viewModel.getInviteStatus() == InviteContactViewModel.InviteStatus.TO_BE_INVITED;
                if (hasContactsToBeInvited) {
                    break;
                }
            }

            inviteAllButton.setVisibility(hasContactsToBeInvited ? View.VISIBLE : View.GONE);
        }
    }

    @Override
    public void updateInviteObserver() {
        inviteContactViewModels = InviteUtils.getContacts(invitesType);
        if (inviteContactViewModels == null) {
            return;
        }
        displayContacts(inviteContactViewModels);

        if (invitesType == InvitesType.SUGGESTED) {
            int toInviteCount = 0;
            for (InviteContactViewModel viewModel : inviteContactViewModels) {
                if (viewModel.getInviteStatus() == InviteContactViewModel.InviteStatus.TO_BE_INVITED) {
                    toInviteCount += 1;
                }
            }
            inviteAllButton.setVisibility(toInviteCount > 0 ? View.VISIBLE : View.GONE);

            //to prevent dialog from poping out upon revoking an invitation
            int minNumberOfEntries = PrintOSPreferences.getInstance(PrintOSApplication.getAppContext())
                    .getInviteAllPopupDisplayCounter();
            if (toInviteCount == 0) {
                InvitesFragment.setNumberOfEntries(Math.min(minNumberOfEntries - 1,
                        InvitesFragment.getNumberOfEntries()));
            }

            if (InvitesFragment.getNumberOfEntries() >= minNumberOfEntries &&
                    PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).isInvitesCoinEnabled() &&
                    !PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).isInviteAllPopupSuppressed() &&
                    toInviteCount > 0) {
                InvitesFragment.setNumberOfEntries(0);
                if (!PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).isLatexOnly()) {
                    InviteAllPopup.getInstance(this, toInviteCount).show(getActivity().getFragmentManager(), InviteAllPopup.TAG);
                }
            }
        }
    }

    private void displayContacts(List<InviteContactViewModel> inviteContactViewModels) {
        if (inviteContactViewModels == null) {
            inviteContactViewModels = new ArrayList<>();
        }

        if (inviteAdapter != null) {
            inviteAdapter.setContactList(inviteContactViewModels, invitesType == InvitesType.EMAIL ?
                    "" : inputField.getText().toString());
            return;
        }

        inviteAdapter = new InviteAdapter(getActivity(), inviteContactViewModels, invitesType);
        contactsList.setAdapter(inviteAdapter);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        contactsList.setLayoutManager(layoutManager);

        inviteButton.setEnabled(false);
        inputField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (invitesType == InvitesType.EMAIL) {
                    boolean emailValid = InviteUtils.isEmailValid(s.toString());
                    inviteButton.setTextColor(ResourcesCompat.getColor(getResources(),
                            emailValid ? R.color.c62 : R.color.c201, null));
                    inviteButton.setBackground(ResourcesCompat.getDrawable(getResources(),
                            emailValid ? R.drawable.rounded_white_with_blue_stroke :
                                    R.drawable.rounded_white_with_grey_stroke, null));
                    inviteButton.setEnabled(emailValid);
                } else {
                    inviteAdapter.getFilter().filter(s);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        if (getContext() != null && isAdded()) {
            inputField.setHint(getString(invitesType == InvitesType.EMAIL ? R.string.invites_fragment_insert_email_hint
                    : R.string.invites_fragment_insert_name_hint));
        }

        inviteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideSoftKeyboardWithDelay();
                if (invitesType == InvitesType.EMAIL) {
                    InviteUtils.addInvitedEmail(inputField.getText().toString());
                    inputField.setText("");
                    inviteAdapter.setContactList(InviteUtils.getContacts(invitesType));
                }
            }
        });

        contactsList.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            @Override
            public boolean onInterceptTouchEvent(RecyclerView recyclerView, MotionEvent motionEvent) {
                hideSoftKeyboard();
                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView recyclerView, MotionEvent motionEvent) {
            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean b) {
            }
        });
    }

    @OnClick(R.id.invite_all_button)
    public void inviteAllContacts() {
        inviteButton.setVisibility(View.GONE);

        Analytics.sendEvent(Analytics.INVITE_ALL_VIA_BUTTON);

        for (InviteContactViewModel contactViewModel : inviteContactViewModels) {
            if (contactViewModel.getInviteStatus() == InviteContactViewModel.InviteStatus.TO_BE_INVITED) {
                InviteUtils.inviteContact(contactViewModel, invitesType, false, true);
            }
        }
    }

    public void hideSoftKeyboard() {
        HPUIUtils.hideSoftKeyboard(PrintOSApplication.getAppContext(), inputField);
    }

    private void hideSoftKeyboardWithDelay() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                hideSoftKeyboard();
            }
        }, HIDE_DELAY);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_invites_list;
    }

    @Override
    public String getToolbarDisplayNameExtra() {
        return "";
    }

    @Override
    public boolean isFragmentNameToBeDisplayed() {
        return true;
    }

    @Override
    public boolean isFilteringAvailable() {
        return false;
    }

    @Override
    public int getToolbarDisplayName() {
        return R.string.invites_fragment_name;
    }

    @Override
    public boolean onBackPressed() {
        return false;
    }

    @Override
    public boolean isIntercomAccessible() {
        return false;
    }

    @Override
    public void onInviteAllClicked() {
        inviteAllContacts();
    }

}
