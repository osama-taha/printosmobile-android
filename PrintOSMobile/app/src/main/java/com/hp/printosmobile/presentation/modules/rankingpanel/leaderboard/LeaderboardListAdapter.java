package com.hp.printosmobile.presentation.modules.rankingpanel.leaderboard;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hp.printosmobile.R;
import com.hp.printosmobile.presentation.modules.home.Panel;
import com.hp.printosmobile.presentation.modules.rankingpanel.RankingBreakdownPanel;
import com.hp.printosmobile.presentation.modules.shared.PrintOSBaseAdapter;
import com.hp.printosmobile.presentation.modules.week.RankingViewModel;
import com.hp.printosmobile.utils.HPUIUtils;
import com.hp.printosmobilelib.ui.widgets.panel.PanelView;
import com.hp.printosmobilelib.ui.widgets.panel.SharableObject;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Osama Taha
 */
public class LeaderboardListAdapter extends PrintOSBaseAdapter<RankingViewModel.SiteRankViewModel> implements RankingBreakdownPanel.RankBreakdownPanelCallback {

    private static final int NOT_MY_SITE_CELL_ID = 2;
    private static final int MY_SITE_CELL_ID = 3;

    private final Context context;
    private final LeaderboardAdapterCallbacks callbacks;
    private FooterViewHolder footerViewHolder;

    public LeaderboardListAdapter(Context context, LeaderboardAdapterCallbacks callbacks) {
        this.context = context;
        this.callbacks = callbacks;
    }

    @Override
    public int getItemViewType(int position) {
        return (isLastPosition(position) && isFooterAdded) ? FOOTER : getSiteItemViewType(position);
    }

    private int getSiteItemViewType(int position) {
        return items.get(position).isMySiteRank() ? MY_SITE_CELL_ID : NOT_MY_SITE_CELL_ID;
    }

    @Override
    protected RecyclerView.ViewHolder createHeaderViewHolder(ViewGroup parent) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.search_adapter_footer, parent, false);

        final HeaderViewHolder holder = new HeaderViewHolder(view);

        holder.reloadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (onReloadClickListener != null) {
                    onReloadClickListener.onReloadClick();
                }
            }
        });

        return holder;
    }

    @Override
    protected RecyclerView.ViewHolder createItemViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        if (viewType == MY_SITE_CELL_ID) {
            return new MySiteViewHolder(layoutInflater.inflate(R.layout.leaderboard_cell_my_site, parent, false));
        } else {
            return new SiteViewHolder(layoutInflater.inflate(R.layout.leaderboard_cell, parent, false));
        }

    }

    @Override
    protected RecyclerView.ViewHolder createFooterViewHolder(ViewGroup parent) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.search_adapter_footer, parent, false);

        final FooterViewHolder holder = new FooterViewHolder(view);

        holder.reloadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (onReloadClickListener != null) {
                    onReloadClickListener.onReloadClick();
                }
            }
        });

        return holder;
    }

    @Override
    protected void bindHeaderViewHolder(RecyclerView.ViewHolder viewHolder) {

    }

    @Override
    protected void bindItemViewHolder(RecyclerView.ViewHolder viewHolder, int position) {

        RankingViewModel.SiteRankViewModel siteRankViewModel = getItem(position);

        SiteViewHolder siteViewHolder = (SiteViewHolder) viewHolder;
        siteViewHolder.rankingTextView.setText(String.valueOf(siteRankViewModel.getActualRank()));
        siteViewHolder.scoreTextView.setText(String.valueOf(siteRankViewModel.getScore()));
        siteViewHolder.siteNameTextView.setText(siteRankViewModel.getName());

        //Set the width of ranking text view.
        float measureText = siteViewHolder.rankingTextView.getPaint().measureText("00000");
        siteViewHolder.rankingTextView.setWidth(siteViewHolder.rankingTextView.getPaddingLeft() +
                siteViewHolder.rankingTextView.getPaddingRight() + (int) measureText);

        HPUIUtils.setVisibility(!siteRankViewModel.isDummyModel(), siteViewHolder.incognitoImageView,
                siteViewHolder.flagImageView, siteViewHolder.scoreTextView, siteViewHolder.siteNameTextView);

        HPUIUtils.setVisibilityForViews(!siteRankViewModel.isMySiteRank() && !siteRankViewModel.isDummyModel() && siteRankViewModel.isIncognito(), siteViewHolder.incognitoImageView);
        HPUIUtils.setVisibilityForViews(siteRankViewModel.isDummyModel() || !siteRankViewModel.isLowerThird(), siteViewHolder.rankingTextView);

        siteViewHolder.trendLayout.setBackgroundResource(siteRankViewModel.getTrend().getBackgroundIconResId());

        if (siteRankViewModel.getTrend() == RankingViewModel.Trend.EQUALS) {
            siteViewHolder.trendOffset.setText("");
            siteViewHolder.trendIcon.setImageResource(0);
        } else {
            siteViewHolder.trendOffset.setText(String.valueOf(Math.abs(siteRankViewModel.getOffset())));
            siteViewHolder.trendIcon.setImageResource(siteRankViewModel.getTrend().getDirectionIconResId());
        }

        HPUIUtils.setVisibility(siteRankViewModel.isInTop(), siteViewHolder.trophyImageView);

        if (!TextUtils.isEmpty(siteRankViewModel.getCountryFlagUrl())) {
            Picasso.with(context)
                    .load(siteRankViewModel.getCountryFlagUrl())
                    .fit().centerCrop()
                    .into(siteViewHolder.flagImageView);
        }

        if (viewHolder instanceof MySiteViewHolder) {

            MySiteViewHolder mySiteViewHolder = (MySiteViewHolder) viewHolder;
            mySiteViewHolder.itemView.setBackgroundColor(Color.TRANSPARENT);

            HPUIUtils.setVisibility(siteRankViewModel.isLowerThird(), mySiteViewHolder.lowerThirdLabel);
            HPUIUtils.setVisibility(!siteRankViewModel.isLowerThird(), mySiteViewHolder.rankingTextView);

            if (siteRankViewModel.isLowerThird()) {
                HPUIUtils.setVisibility(false, mySiteViewHolder.incognitoImageView);
            } else {
                HPUIUtils.setVisibilityForViews(false, mySiteViewHolder.incognitoImageView);
            }

            LinearLayout.LayoutParams layoutParams = HPUIUtils.getPanelLayoutParams(context);
            RankingBreakdownPanel rankingBreakdownPanel = new RankingBreakdownPanel(context, mySiteViewHolder.mySiteCellContainerView);
            rankingBreakdownPanel.addListener(this);
            rankingBreakdownPanel.updateViewModel(siteRankViewModel.getRankBreakdownViewModel());

            mySiteViewHolder.graphContainer.addView(rankingBreakdownPanel, layoutParams);

        } else {

            viewHolder.itemView.setBackgroundResource(R.drawable.ranking_leaderboard_cell_border);

        }

    }

    @Override
    protected void bindFooterViewHolder(RecyclerView.ViewHolder viewHolder) {
        footerViewHolder = (FooterViewHolder) viewHolder;
    }

    @Override
    protected void showLoadMoreFooter() {
        if (footerViewHolder != null) {
            footerViewHolder.noResultsLayout.setVisibility(View.GONE);
            footerViewHolder.errorLayout.setVisibility(View.GONE);
            footerViewHolder.loadingLayout.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void showErrorFooter() {
        if (footerViewHolder != null) {
            footerViewHolder.noResultsLayout.setVisibility(View.GONE);
            footerViewHolder.loadingLayout.setVisibility(View.GONE);
            footerViewHolder.errorLayout.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void showNoResultsFooter() {
        if (footerViewHolder != null) {
            footerViewHolder.loadingLayout.setVisibility(View.GONE);
            footerViewHolder.errorLayout.setVisibility(View.GONE);
            footerViewHolder.noResultsLayout.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void showLoadMoreHeader() {
    }

    @Override
    protected void showErrorHeader() {
    }

    @Override
    protected void showNoResultsHeader() {
    }

    @Override
    public void addFooterView() {
        removeFooterView();
        isFooterAdded = true;
        add(new RankingViewModel.SiteRankViewModel());
    }

    public void removeFooterView() {

        isFooterAdded = false;

        for (int i = 0; i < items.size(); i++) {
            RankingViewModel.SiteRankViewModel item = items.get(i);
            if (item != null && item.getActualRank() == 0) {
                items.remove(i);
                notifyItemRemoved(i);
            }
        }

    }


    @Override
    public void addHeaderView() {
    }

    @Override
    public void onShareButtonClicked(PanelView panelView) {
    }

    @Override
    public void onShareButtonClicked(Panel panel) {

    }

    @Override
    public void onScreenshotCreated(Panel panel, Uri screenshotUri, String title, String body, SharableObject sharableObject) {
        if (panel != null && callbacks != null) {
            callbacks.onScreenshotCreated(panel, screenshotUri, title, body, sharableObject);
        }
    }

    public void insertItems(List<RankingViewModel.SiteRankViewModel> siteRankViewModels) {

        if (siteRankViewModels == null) {
            return;
        }

        for (RankingViewModel.SiteRankViewModel siteRankViewModel : siteRankViewModels) {
            items.set(siteRankViewModel.getActualRank() - 1, siteRankViewModel);
        }

        notifyDataSetChanged();
    }

    public static class SiteViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.ranking_cell_position)
        TextView rankingTextView;
        @Bind(R.id.ranking_cell_site_name)
        TextView siteNameTextView;
        @Bind(R.id.ranking_cell_score)
        TextView scoreTextView;
        @Bind(R.id.ranking_cell_country_flag)
        ImageView flagImageView;
        @Bind(R.id.ranking_cell_incognito_icon)
        ImageView incognitoImageView;
        @Bind(R.id.ranking_cell_offset_layout)
        View trendLayout;
        @Bind(R.id.ranking_cell_trend_icon)
        ImageView trendIcon;
        @Bind(R.id.ranking_cell_offset)
        TextView trendOffset;
        @Bind(R.id.ranking_cell_trophy_icon)
        ImageView trophyImageView;

        public SiteViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }

    public static class MySiteViewHolder extends SiteViewHolder {

        @Bind(R.id.ranking_leaderboard_my_site_lower_third_label)
        TextView lowerThirdLabel;
        @Bind(R.id.my_site_container_view)
        View mySiteCellContainerView;
        @Bind(R.id.ranking_leaderboard_my_site_graph_container)
        LinearLayout graphContainer;

        public MySiteViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }

    public static class FooterViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.loading_layout)
        FrameLayout loadingLayout;
        @Bind(R.id.error_layout)
        RelativeLayout errorLayout;
        @Bind(R.id.no_results_layout)
        RelativeLayout noResultsLayout;
        @Bind(R.id.retry_btn)
        Button reloadButton;

        FooterViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public static class HeaderViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.loading_layout)
        FrameLayout loadingLayout;
        @Bind(R.id.error_layout)
        RelativeLayout errorLayout;
        @Bind(R.id.no_results_layout)
        RelativeLayout noResultsLayout;
        @Bind(R.id.retry_btn)
        Button reloadButton;

        HeaderViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    interface LeaderboardAdapterCallbacks {
        void onScreenshotCreated(Panel panel, Uri screenshotUri, String title, String body, SharableObject sharableObject);
    }

}
