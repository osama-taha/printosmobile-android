package com.hp.printosmobile.presentation.modules.contacthp;

import android.net.Uri;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.hp.printosmobile.Analytics;
import com.hp.printosmobile.IntercomSdk;
import com.hp.printosmobile.R;
import com.hp.printosmobile.presentation.modules.contacthp.shared.AttachImageView;
import com.hp.printosmobile.presentation.modules.contacthp.shared.ContactHpContactThroughView;
import com.hp.printosmobile.presentation.modules.contacthp.shared.ContactHpTextField;
import com.hp.printosmobile.presentation.modules.tooltip.MessageTooltip;
import com.hp.printosmobile.utils.HPLocaleUtils;
import com.hp.printosmobile.utils.HPUIUtils;
import com.hp.printosmobilelib.core.communications.remote.APIException;
import com.hp.printosmobilelib.core.communications.remote.models.UserData;

import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * Created be Anwar Asbah 10/11/2016
 */
public class AskAQuestionActivity extends ContactHPBaseActivity {

    private static final int TOOLTIP_MARGIN_RIGHT = 30;

    @Bind(R.id.attach_image_view)
    AttachImageView attachImageView;
    @Bind(R.id.question_text_view)
    ContactHpTextField questionTextView;
    @Bind(R.id.subject_text_view)
    ContactHpTextField subjectTextView;
    @Bind(R.id.contact_through_view)
    ContactHpContactThroughView contactThroughView;
    @Bind(R.id.error_layout)
    View errorLayout;
    @Bind(R.id.error_msg_text_view)
    TextView errorMsgTextView;
    @Bind(R.id.country_text_view)
    ContactHpTextField countryTextView;
    @Bind(R.id.country_field)
    View countryField;
    @Bind(R.id.country_tooltip_button)
    View countryTooltip;

    UserData.User userData;
    boolean hasCountry = false;
    Map<String, String> countryList;

    @Override
    protected int getLayoutResource() {
        return R.layout.contact_hp_ask_a_question;
    }

    @Override
    protected void init() {
        IntercomSdk.getInstance(this).logEvent(IntercomSdk.PBM_VIEW_ASK_EVENT);

        attachImageView.addAttachImageClickListener(this);
        questionTextView.setTextFieldRightDrawable(null);
    }

    @Override
    protected void hideSoftKeyboard() {
        HPUIUtils.hideSoftKeyboard(this,
                subjectTextView.getAutoCompleteTextView(),
                questionTextView.getAutoCompleteTextView(),
                countryTextView.getAutoCompleteTextView());
    }

    @Override
    public int getTitleResourceID() {
        return R.string.menu_resolve_technical_issues;
    }

    @Override
    public void sendRequest() {
        if (hasCountry) {
            send();
        } else {
            if (presenter == null) {
                presenter = new ContactHpPresenter();
                presenter.attachView(this);
            }

            String country = countryTextView.getText();
            if (userData != null) {
                UserData.Address address = userData.getAddress();
                if (address == null) {
                    address = new UserData.Address();
                    userData.setAddress(address);
                }
                address.setCountry(country);
                presenter.updateUser(userData);
            }

        }
    }

    private void send() {
        List<Uri> uriList = attachImageView.getAttachedImagesUri();

        if (uriList == null || uriList.isEmpty()) {
            Analytics.sendEvent(Analytics.ASK_THE_EXPERT_SEND_ACTION);
        } else {
            Analytics.sendEvent(Analytics.ASK_THE_EXPERT_SEND_ACTION, Analytics.CONTACT_HP_SCREENSHOT_LABEL);
        }


        presenter.askQuestion(this,
                subjectTextView.getText(),
                questionTextView.getText(),
                uriList,
                contactThroughView.getContactOption());

        IntercomSdk.getInstance(this).logEvent(IntercomSdk.PBM_SEND_QUESTION_EVENT);
    }

    @Override
    public void setFormEnable(boolean isEnabled) {
        attachImageView.setEnabled(isEnabled);
        questionTextView.setEnabled(isEnabled);
        subjectTextView.setEnabled(isEnabled);
        contactThroughView.setEnabled(isEnabled);
        countryTextView.setEnabled(isEnabled);
    }

    @Override
    public void setHasPhone(boolean hasPhone) {
        contactThroughView.setHasPhone(hasPhone);
    }

    @Override
    public boolean isFormFilled() {
        caseLayout.requestFocus();
        hideSoftKeyboard();

        String subject = subjectTextView.getText();
        String question = questionTextView.getText();
        String country = countryTextView.getText();

        if (!hasCountry) {
            return displayWarningMessage(country, R.string.hp_contact_fill_country_warning)
                    && displayWarningMessage(subject, R.string.hp_contact_fill_subject_warning)
                    && displayWarningMessage(question, R.string.hp_contact_fill_question_warning);
        } else {
            return displayWarningMessage(subject, R.string.hp_contact_fill_subject_warning)
                    && displayWarningMessage(question, R.string.hp_contact_fill_question_warning);
        }
    }

    @Override
    public void onUserRetrieved(UserData.User user, APIException apiException) {
        userData = user;

        if (user == null) {
            loadingView.setVisibility(View.GONE);
            caseLayout.setVisibility(View.GONE);
            errorLayout.setVisibility(View.VISIBLE);
            errorMsgTextView.setText(HPLocaleUtils.getSimpleErrorMsg(this, apiException));
            return;
        }

        setHasPhone(user.getPrimaryPhone() != null);

        if (user.getAddress() == null || TextUtils.isEmpty(user.getAddress().getCountry())) {
            hasCountry = false;
            if (presenter == null) {
                presenter = new ContactHpPresenter();
                presenter.attachView(this);
            }
            presenter.getCountries();
            return;
        }

        hasCountry = true;
        loadingView.setVisibility(View.GONE);
        errorLayout.setVisibility(View.GONE);
        caseLayout.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.try_again_text_view)
    public void onTryAgainClicked() {
        intiPresenter();
        errorLayout.setVisibility(View.GONE);
        loadingView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onCountriesRetrieved(Map<String, String> countryDataList, APIException apiException) {
        countryList = countryDataList;

        if (countryDataList == null || countryDataList.size() == 0) {
            loadingView.setVisibility(View.GONE);
            caseLayout.setVisibility(View.GONE);
            errorLayout.setVisibility(View.VISIBLE);
            errorMsgTextView.setText(HPLocaleUtils.getSimpleErrorMsg(this, apiException));
            return;
        }

        loadingView.setVisibility(View.GONE);
        errorLayout.setVisibility(View.GONE);
        caseLayout.setVisibility(View.VISIBLE);
        countryTextView.setAutoCompleteList(countryDataList);
        countryField.setVisibility(View.VISIBLE);

        countryTextView.getAutoCompleteTextView().setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (!hasFocus) {
                    String text = countryTextView.getText();
                    if (countryList == null || !countryList.containsKey(text)) {
                        countryTextView.getAutoCompleteTextView().setText("");
                    }
                }
            }
        });
    }

    @OnClick(R.id.country_tooltip_button)
    public void showCountryTooltip() {
        String tip = getString(R.string.hp_contact_country_tooltip);
        MessageTooltip tooltip = new MessageTooltip(this);
        tooltip.updateView(tip);
        tooltip.showToolTipToLeftSide(countryTooltip,
                HPUIUtils.dpToPx(this, TOOLTIP_MARGIN_RIGHT),
                -countryTooltip.getPaddingBottom() / 2f);
    }

    @Override
    public void onUserUpdated() {
        send();
    }
}
