package com.hp.printosmobile.presentation.modules.rankingpanel;

import android.content.Context;
import android.os.Build;
import android.text.Spannable;
import android.text.SpannableString;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.hp.printosmobile.R;
import com.hp.printosmobile.presentation.modules.week.RankingViewModel;
import com.hp.printosmobile.utils.FileUtils;
import com.hp.printosmobilelib.core.logging.HPLogger;
import com.hp.printosmobilelib.core.utils.HPDateUtils;

import java.io.IOException;

/**
 * Created by Anwar Asbah on 5/30/2018.
 */

public class RankBreakdownUtils {

    private static final String TAG = RankBreakdownUtils.class.getSimpleName();
    private static final String HTML_FILE_NAME = "ranking_graph.html";

    private static final String SERIES_KEY = "@SERIES@";
    private static final String XAXIS_KEY_KEY = "@XAXISVALUES@";
    private static final String MAX_VALUE_KEY = "@MAXVALUE@";
    private static final String FIST_PLACE_STRING_LOCALIZED_KEY = "@FIRSTPLACE@";

    private static final String DATA_ENTRY = "{\n" +
            "          x: %d,\n" +
            "          y: %d\n" +
            "        }";

    private static final String HISTOGRAM_DATE_FORMAT = "MMM dd";

    private RankBreakdownUtils() {
    }

    public static void displayGraph(Context context, WebView progressWebView, RankBreakdownViewModel rankBreakdownViewModel) {
        if (rankBreakdownViewModel == null) {
            return;
        }

        WebSettings webSettings = progressWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setDomStorageEnabled(true);

        progressWebView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            progressWebView.setWebContentsDebuggingEnabled(true);
        }

        try {
            String detail = FileUtils.loadAssestFile(context, HTML_FILE_NAME);
            detail = setSeriesData(detail, rankBreakdownViewModel)
                    .replace(FIST_PLACE_STRING_LOCALIZED_KEY,
                            context.getString(R.string.ranking_breakdown_first_place));
            progressWebView.loadDataWithBaseURL("", detail, "text/html", "utf-8", "");
        } catch (IOException e) {
            HPLogger.e(TAG, "error displaying graph: " + e.getMessage());
        }
    }

    private static String setSeriesData(String html, RankBreakdownViewModel model) {

        if (model == null || html == null || model.getHistoryList() == null) {
            return "";
        }

        String series = "";
        String xValues = "";
        int maxValue = 1;

        int size = model.getHistoryList().size();
        for (int i = 0; i < size; i++) {
            RankBreakdownViewModel.RankHistoryItem item = model.getHistoryList().get(i);

            series += String.format(DATA_ENTRY,
                    i, item.getRankValue());
            series += (i == size - 1) ? "" : ",";

            xValues += "'" + HPDateUtils.formatDate(item.getDate(), HISTOGRAM_DATE_FORMAT) + "'";
            xValues += (i == size - 1) ? "" : ",";

            maxValue = Math.max(maxValue, item.getRankValue());


        }

        return html.replace(SERIES_KEY, series)
                .replace(XAXIS_KEY_KEY, xValues)
                .replace(MAX_VALUE_KEY, String.valueOf(maxValue));
    }

    public static Spannable getTitle(Context context, RankBreakdownViewModel model) {
        if (model == null) {
            return new SpannableString("");
        }

        String title = model.getType() == RankingViewModel.RankAreaType.WORLD_WIDE ?
                context.getString(R.string.ranking_breakdown_title_world_wide)
                : context.getString(R.string.ranking_breakdown_title, model.getName());

        return new SpannableString(title);
    }
}
