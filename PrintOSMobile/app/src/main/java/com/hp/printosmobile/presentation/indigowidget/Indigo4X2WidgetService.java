package com.hp.printosmobile.presentation.indigowidget;

/**
 * Created by Osama Taha on 2/10/17.
 */
public class Indigo4X2WidgetService extends PrintOSWidgetService {

    @Override
    WidgetType getWidgetType() {
        return WidgetType.INDIGO_4X2;
    }

    @Override
    PrintOSWidgetService instance() {
        return Indigo4X2WidgetService.this;
    }

}