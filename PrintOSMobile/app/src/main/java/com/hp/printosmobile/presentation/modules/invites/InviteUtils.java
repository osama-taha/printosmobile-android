package com.hp.printosmobile.presentation.modules.invites;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Handler;
import android.provider.ContactsContract;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.res.ResourcesCompat;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.baoyz.actionsheet.ActionSheet;
import com.hp.printosmobile.Analytics;
import com.hp.printosmobile.Constants;
import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.presentation.modules.drawer.OrganizationViewModel;
import com.hp.printosmobile.utils.AppUtils;
import com.hp.printosmobile.utils.HPLocaleUtils;
import com.hp.printosmobile.utils.HPUIUtils;
import com.hp.printosmobilelib.core.communications.remote.APIException;
import com.hp.printosmobilelib.core.communications.remote.models.AAAError;
import com.hp.printosmobilelib.core.logging.HPLogger;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * Created by Anwar Asbah on 11/2/2017.
 */

public class InviteUtils {

    private static final String TAG = InviteUtils.class.getSimpleName();

    private static final String INVITE_TO_PRINTOS_EMAIL = " tried to invite to PrintOS email: ";
    private static final String USER = "user: ";
    private static final String EMAIL_SPLIT_REGEX = "@";
    private static final String EMAIL_DOMAIN_SPLIT_REGEX = "\\.";
    private static final String NAME_SPLIT_REGEX = " ";
    private static final String SMS_BODY_TAG = "sms_body";
    private static final String SMS_ADDRESS_TAG = "address";
    private static final String SMS_URI = "sms:";
    private static final long DELAY = 50;

    private static InvitesViewModel invitesViewModel;
    private static Map<InvitesListFragment.InvitesType, List<InviteContactViewModel>> contactMap;
    private static List<InviteObservable> observables = new ArrayList<>();
    private static InvitesPresenter invitesPresenter;

    private static List<InviteContactViewModel> inviteContactViewModels;

    private static List<String> ignoreDomains = new ArrayList<>();
    private static Random random;
    private static ActionSheet inviteActionSheet = null;

    static {
        ignoreDomains.add("gmail.com");
        ignoreDomains.add("outlook.com");
        ignoreDomains.add("yahoo.com");
        ignoreDomains.add("inbox.com");
        ignoreDomains.add("icloud.com");
        ignoreDomains.add("mail.com");
        ignoreDomains.add("aol.com");
        ignoreDomains.add("zoho.com");
    }

    public static void retrieveInvitesList() {
        if (invitesPresenter == null) {
            invitesPresenter = new InvitesPresenter();
        }
        invitesPresenter.getInvitesList();
    }

    public static void setInvitesViewModel(InvitesViewModel invitesViewModel) {
        InviteUtils.invitesViewModel = invitesViewModel;
        updateContactMap(true);
        updateObservers();
    }


    static void retrieveContacts(Context context) {
        if (context == null || inviteContactViewModels != null) {
            return;
        }

        String userEmail = PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).getUserEmail();

        ContentResolver contentResolver = context.getContentResolver();
        if (contentResolver == null) {
            return;
        }

        String[] projection = new String[]{ContactsContract.RawContacts._ID,
                ContactsContract.Contacts._ID,
                ContactsContract.Contacts.DISPLAY_NAME,
                ContactsContract.Contacts.PHOTO_ID,
                ContactsContract.Contacts.HAS_PHONE_NUMBER,
                ContactsContract.CommonDataKinds.Email.DATA,
                ContactsContract.CommonDataKinds.Photo.CONTACT_ID,
                ContactsContract.CommonDataKinds.Phone.CONTACT_ID};

        String order = "CASE WHEN "
                + ContactsContract.Contacts.DISPLAY_NAME
                + " NOT LIKE '%@%' THEN 1 ELSE 2 END, "
                + ContactsContract.Contacts.DISPLAY_NAME
                + ", "
                + ContactsContract.CommonDataKinds.Email.DATA
                + " COLLATE NOCASE";
        String filter = ContactsContract.CommonDataKinds.Email.DATA + " NOT LIKE ''";
        Cursor cursor = contentResolver.query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, projection, filter, null, order);

        if (cursor == null) {
            return;
        }

        inviteContactViewModels = new ArrayList<>();

        if (cursor.moveToFirst()) {
            List<String> contactNameEmailList = new ArrayList<>();
            do {

                String phoneId = cursor.getString(7);
                String name = cursor.getString(2);
                String email = cursor.getString(5);
                String contactId = cursor.getString(6);

                String phone = null;
                int hasPhone = cursor.getInt(4);
                if (hasPhone > 0) {
                    Cursor cp = contentResolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                            new String[]{ContactsContract.CommonDataKinds.Phone.NUMBER},
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                            new String[]{phoneId}, null);
                    if (cp != null && cp.moveToFirst()) {
                        phone = cp.getString(cp.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        cp.close();
                    }
                }

                String contactNameEmail = (name == null ? "" : name) + (email == null ? "" : email);
                if (contactNameEmailList.contains(contactNameEmail)) {
                    continue;
                }
                contactNameEmailList.add(contactNameEmail);

                InviteContactViewModel contact = new InviteContactViewModel();
                contact.setName(name);
                contact.setColor(getRandomColor());
                contact.setId(contactId);
                contact.setEmail(email);
                contact.setPhone(phone);
                contact.setPhoto(retrieveContactPhoto(contactId, contentResolver));
                contact.setInitials(InviteUtils.getContactInitials(contact));
                contact.setInviteStatus(InviteContactViewModel.InviteStatus.TO_BE_INVITED);

                if (isEmailValid(email) && !isSameEmails(email, userEmail) && inviteContactViewModels != null) {
                    inviteContactViewModels.add(contact);
                }
            } while (cursor.moveToNext());
        }
        cursor.close();

        if (contactMap == null) {
            contactMap = new HashMap<>();
        }
        List<InviteContactViewModel> suggested = InviteUtils.getTargetContacts(inviteContactViewModels, getUserEmailDomain(), ignoreDomains);
        if (contactMap.containsKey(InvitesListFragment.InvitesType.SUGGESTED)) {
            contactMap.remove(InvitesListFragment.InvitesType.SUGGESTED);
        }
        if (suggested != null && !suggested.isEmpty()) {
            contactMap.put(InvitesListFragment.InvitesType.SUGGESTED, suggested);
        }

        List<InviteContactViewModel> all = InviteUtils.getTargetContacts(inviteContactViewModels, null, null);
        if (contactMap.containsKey(InvitesListFragment.InvitesType.CONTACTS)) {
            contactMap.remove(InvitesListFragment.InvitesType.CONTACTS);
        }
        if (all != null && !all.isEmpty()) {
            contactMap.put(InvitesListFragment.InvitesType.CONTACTS, all);
        }

        updateContactMap(false);
    }

    private static boolean isSameEmails(String email, String email2) {
        if (email == null || email2 == null) {
            return false;
        }

        return email.equalsIgnoreCase(email2);
    }

    public static String getUserEmailDomain() {
        String email = PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).getUserEmail();
        if (email == null) {
            return "";
        }
        String[] emailSplit = email.split(EMAIL_SPLIT_REGEX);
        if (emailSplit.length == 2) {
            return emailSplit[1] == null ? "" : emailSplit[1];
        }
        return "";
    }


    private static Bitmap retrieveContactPhoto(String id, ContentResolver cr) {

        Bitmap photo = null;

        try {
            InputStream inputStream = ContactsContract.Contacts.openContactPhotoInputStream(cr,
                    ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, Long.valueOf(id)));

            if (inputStream != null) {
                photo = BitmapFactory.decodeStream(inputStream);
                inputStream.close();
            }
        } catch (Exception e) {
            //Nothing to do
        }

        return photo;
    }

    private static String getContactInitials(InviteContactViewModel viewModel) {
        if (viewModel == null) {
            return "";
        }

        String name = viewModel.getName();
        name = name == null ? viewModel.getEmail() : name;
        if (name == null) {
            return "";
        }

        String initials = "";
        String[] nameSplit = name.split(NAME_SPLIT_REGEX);
        for (int i = 0; i < nameSplit.length; i++) {
            if ((i == 0 || i == nameSplit.length - 1) && !TextUtils.isEmpty(nameSplit[i])) {
                initials += nameSplit[i].charAt(0);
            }
        }

        if (initials.length() < 2 && name.length() >= 2) {
            initials = name.substring(0, 2);
        }

        return initials.toUpperCase();
    }

    private static void updateContactMap(boolean withUpdate) {
        if (contactMap == null || invitesViewModel == null || invitesViewModel.getInvitedContacts() == null) {
            return;
        }

        List<String> alreadyListedEmails = new ArrayList<>();

        for (InvitesListFragment.InvitesType type : contactMap.keySet()) {
            List<InviteContactViewModel> inviteContactViewModels = contactMap.get(type);
            for (InviteContactViewModel contact : inviteContactViewModels) {
                boolean emailExists = false;
                for (InvitesViewModel.InvitedContact invitedContact : invitesViewModel.getInvitedContacts()) {
                    if (contact.getEmail() != null && invitedContact.getInviteEmail() != null &&
                            contact.getEmail().equalsIgnoreCase(invitedContact.getInviteEmail()) &&
                            (contact.getInviteId() == null || contact.getInviteId().equals(invitedContact.getInviteID()))) {
                        contact.setInviteStatus(invitedContact.getInviteStatus());
                        contact.setInviteId(invitedContact.getInviteID());
                        contact.setResendEnabled(invitedContact.isResendEnabled());
                        contact.setRevokeEnabled(invitedContact.isRevokeEnabled());
                        contact.setLanguage(invitedContact.getLangaugeCode());
                        contact.setRoleId(invitedContact.getRoleID());
                        contact.setRoleType(invitedContact.getRoleType());
                        emailExists = true;
                    }
                }
                if (emailExists) {
                    alreadyListedEmails.add(contact.getEmail());
                }
            }
        }

        if (!contactMap.containsKey(InvitesListFragment.InvitesType.EMAIL)) {
            contactMap.put(InvitesListFragment.InvitesType.EMAIL, new ArrayList<InviteContactViewModel>());
        }
        List<InviteContactViewModel> emailModels = contactMap.get(InvitesListFragment.InvitesType.EMAIL);

        for (InvitesViewModel.InvitedContact invitedContact : invitesViewModel.getInvitedContacts()) {
            String email = invitedContact.getInviteEmail();
            if (email != null && !alreadyListedEmails.contains(email)) {
                boolean isListed = false;
                for (InviteContactViewModel emailContact : emailModels) {
                    if (emailContact != null && emailContact.getEmail() != null && emailContact.getEmail().equalsIgnoreCase(email)) {
                        isListed = true;
                    }
                }

                String userEmail = PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).getUserEmail();
                InviteContactViewModel.InviteStatus inviteStatus = invitedContact.getInviteStatus();
                if (!isListed && (inviteStatus == InviteContactViewModel.InviteStatus.INVITED
                        || inviteStatus == InviteContactViewModel.InviteStatus.ON_BOARD) && isEmailValid(email)
                        && !email.equalsIgnoreCase(userEmail)) {
                    InviteContactViewModel emailModel = new InviteContactViewModel();
                    emailModel.setEmail(email);
                    emailModel.setInviteStatus(invitedContact.getInviteStatus());
                    emailModel.setColor(getRandomColor());
                    emailModel.setId(String.valueOf(System.currentTimeMillis()));
                    emailModel.setInviteId(invitedContact.getInviteID());
                    emailModel.setInitials(getContactInitials(emailModel));
                    emailModel.setResendEnabled(invitedContact.isResendEnabled());
                    emailModel.setRevokeEnabled(invitedContact.isRevokeEnabled());
                    emailModel.setLanguage(invitedContact.getLangaugeCode());
                    emailModel.setRoleId(invitedContact.getRoleID());
                    emailModel.setRoleType(invitedContact.getRoleType());
                    emailModel.setName(invitedContact.getName());
                    emailModels.add(emailModel);
                }
            }
        }

        if (withUpdate) {
            updateObservers();
        }
    }

    private static List<InviteContactViewModel> getTargetContacts(
            List<InviteContactViewModel> contacts, String userDomain, List<String> ignoreDomains) {

        if (contacts == null) {
            return null;
        }

        List<InviteContactViewModel> targetList = new ArrayList<>();
        for (InviteContactViewModel contact : contacts) {
            if (contact != null) {
                String email = contact.getEmail();
                if (email != null) {
                    String[] emailSplit = email.split(EMAIL_SPLIT_REGEX);
                    if (emailSplit.length == 2 && (userDomain == null || userDomain.equalsIgnoreCase(emailSplit[1])) && (ignoreDomains == null || (emailSplit[1] != null &&
                            !ignoreDomains.contains(emailSplit[1].toLowerCase())))) {
                        targetList.add(contact);
                    }
                }
            }
        }
        return targetList;
    }

    public static List<InviteContactViewModel> getContacts(InvitesListFragment.InvitesType invitesType) {
        if (contactMap != null && contactMap.containsKey(invitesType)) {
            return contactMap.get(invitesType);
        }
        return null;
    }

    static void addInvitedEmail(String email) {
        if (email == null) {
            return;
        }

        Context appContext = PrintOSApplication.getAppContext();
        if (email.equalsIgnoreCase(PrintOSPreferences.getInstance(appContext).getUserEmail())) {
            HPUIUtils.displayToast(appContext, appContext.getString(R.string.invites_error_user_email));
            return;
        }

        if (contactMap == null) {
            contactMap = new HashMap<>();
        }

        List<InviteContactViewModel> inviteContactViewModels = null;
        if (contactMap.containsKey(InvitesListFragment.InvitesType.EMAIL)) {
            inviteContactViewModels = contactMap.get(InvitesListFragment.InvitesType.EMAIL);
        }
        if (inviteContactViewModels == null) {
            inviteContactViewModels = new ArrayList<>();
            contactMap.put(InvitesListFragment.InvitesType.EMAIL, inviteContactViewModels);
        }

        InviteContactViewModel contactViewModel = null;
        for (InvitesListFragment.InvitesType type : contactMap.keySet()) {
            List<InviteContactViewModel> list = contactMap.get(type);
            for (InviteContactViewModel contact : list) {
                if (contact != null && contact.getEmail() != null && contact.getEmail().equalsIgnoreCase(email)) {
                    if (contact.getInviteStatus() == InviteContactViewModel.InviteStatus.INVITED ||
                            contact.getInviteStatus() == InviteContactViewModel.InviteStatus.ON_BOARD) {
                        HPUIUtils.displayToast(PrintOSApplication.getAppContext(), PrintOSApplication
                                        .getAppContext().getString(R.string.invites_contact_already_listed)
                        );
                        return;
                    } else if (contact.getInviteStatus() == InviteContactViewModel.InviteStatus.TO_BE_INVITED) {
                        contactViewModel = contact;
                        break;
                    }
                }
            }
        }

        if (contactViewModel == null) {
            contactViewModel = new InviteContactViewModel();
            contactViewModel.setId(String.valueOf(System.currentTimeMillis()));
            contactViewModel.setEmail(email);
            contactViewModel.setColor(getRandomColor());
            contactViewModel.setInitials(InviteUtils.getContactInitials(contactViewModel));
            contactViewModel.setInviteStatus(InviteContactViewModel.InviteStatus.LOADING);
        }
        if (!inviteContactViewModels.contains(contactViewModel)) {
            inviteContactViewModels.add(0, contactViewModel);
        }

        inviteContact(contactViewModel, InvitesListFragment.InvitesType.EMAIL, false, false);
    }

    static boolean isEmailValid(String email) {
        if (email == null) {
            return false;
        }

        String[] emailSplit = email.split(EMAIL_SPLIT_REGEX);
        if (emailSplit.length == 2) {
            String domain = emailSplit[1];
            String[] domainSplit = domain.split(EMAIL_DOMAIN_SPLIT_REGEX);
            if (domainSplit.length >= 2) {
                for (int i = 0; i < domainSplit.length; i++) {
                    if (domainSplit[i].length() < 2) {
                        return false;
                    }
                }
                return true;
            }
        }

        return false;
    }

    public static InvitesViewModel getInvitesViewModel() {
        return invitesViewModel;
    }

    static void inviteContact(InviteContactViewModel inviteContactViewModel, InvitesListFragment.InvitesType invitesType,
                              boolean resend, boolean isAll) {
        if (inviteContactViewModel == null || inviteContactViewModel.getEmail() == null) {
            return;
        }

        String email = inviteContactViewModel.getEmail();
        Context appContext = PrintOSApplication.getAppContext();
        if (email.equalsIgnoreCase(PrintOSPreferences.getInstance(appContext).getUserEmail())) {
            HPUIUtils.displayToast(appContext, appContext.getString(R.string.invites_error_user_email));
            return;
        }

        inviteContactViewModel.setInviteStatus(InviteContactViewModel.InviteStatus.LOADING);
        if (invitesPresenter == null) {
            invitesPresenter = new InvitesPresenter();
        }

        invitesPresenter.inviteContact(inviteContactViewModel, invitesType, resend, isAll);
        updateObservers();
    }

    private static void openSms(InviteContactViewModel contactViewModel) {

        if (contactViewModel == null || TextUtils.isEmpty(contactViewModel.getPhone()) ||
                !PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).isSendSmsEnabled()) {
            return;
        }

        Context context = PrintOSApplication.getAppContext();
        PrintOSPreferences preferences = PrintOSPreferences.getInstance(context);

        OrganizationViewModel organizationViewModel = preferences.getSelectedOrganization();
        String organizationName = organizationViewModel == null ? ""
                : organizationViewModel.getOrganizationName();
        String url = preferences.getServerUrl() + Constants.ACCEPT_INVITE_URL +
                (contactViewModel.getInviteId() == null ? "" : contactViewModel.getInviteId());

        String smsBody = context.getString(R.string.invites_sms,
                contactViewModel.getName(),
                organizationName,
                url);

        Intent sendIntent = new Intent(Intent.ACTION_VIEW);
        sendIntent.setData(Uri.parse(SMS_URI));
        sendIntent.putExtra(SMS_ADDRESS_TAG, contactViewModel.getPhone());
        sendIntent.putExtra(SMS_BODY_TAG, smsBody);

        AppUtils.startApplication(context, sendIntent);
    }

    static void errorInvitingContact(InviteContactViewModel contact, APIException apiException, boolean resend) {

        if (contact == null) {
            return;
        }

        String userEmail = PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).getUserEmail();

        contact.setInviteStatus(resend ? InviteContactViewModel.InviteStatus.INVITED :
                InviteContactViewModel.InviteStatus.TO_BE_INVITED);
        if (apiException != null) {
            String errorMsg = apiException.getMessage();

            boolean isConflict = apiException.getKind() == APIException.Kind.CONFLICT;
            boolean alreadyInvited = false;
            if (apiException.getAAAError() != null && apiException.getAAAError().getSmsError() != null) {
                alreadyInvited = apiException.getAAAError().getSmsError().getSubCode() == AAAError.ErrorSubCode.ERROR_CODE_2000;
                String aaaMsg = apiException.getAAAError().getSmsError().getMessage();
                errorMsg = aaaMsg == null ? errorMsg : aaaMsg;
            }

            if (isConflict) {
                //user already invited
                if (alreadyInvited) {
                    errorMsg = PrintOSApplication.getAppContext().getString(R.string.invites_error_already_invited);
                    HPLogger.d(TAG, USER + userEmail + INVITE_TO_PRINTOS_EMAIL
                            + contact.getEmail() + " but email is already in the organization");
                    contact.setInviteStatus(InviteContactViewModel.InviteStatus.ON_BOARD);
                    Analytics.sendEvent(Analytics.INVITE_FAILED_ONBOARD);
                } else {
                    HPLogger.d(TAG, USER + userEmail + INVITE_TO_PRINTOS_EMAIL
                            + contact.getEmail() + " but failed, error message: "
                            + apiException.getMessage());
                    contact.setInviteStatus(InviteContactViewModel.InviteStatus.INVITED);
                    Analytics.sendEvent(Analytics.INVITE_FAILED_OTHER);
                }
                if (errorMsg != null) {
                    HPUIUtils.displayToast(PrintOSApplication.getAppContext(), errorMsg);
                }
            } else {
                HPUIUtils.displayToast(PrintOSApplication.getAppContext(),
                        HPLocaleUtils.getSimpleErrorMsg(PrintOSApplication.getAppContext(), apiException)
                );
                HPLogger.d(TAG, USER + userEmail + INVITE_TO_PRINTOS_EMAIL
                        + contact.getEmail() + " but failed, error message: "
                        + apiException.getMessage());
                Analytics.sendEvent(Analytics.INVITE_FAILED_OTHER);
            }
        }

        if (resend) {
            HPLogger.d(TAG, "resend invitation fail");
            Analytics.sendEvent(Analytics.RESEND_INVITE_FAIL);
        }

        updateObservers();
    }

    static void successfullyInvitedContact(InviteContactViewModel contact, InvitesListFragment.InvitesType invitesType,
                                           boolean resend, boolean isAll) {
        if (resend) {
            HPLogger.d(TAG, "resend invitation success");
            Analytics.sendEvent(Analytics.RESEND_INVITE_SUCCESS);
        }

        String userEmail = PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).getUserEmail();
        HPLogger.d(TAG, "user: " + userEmail + " successfully invited to PrintOS email: "
                + contact.getEmail());

        Analytics.sendEvent(invitesType.getAnalyticsAction());

        updateObservers();
        if (!isAll) {
            openSms(contact);
        }
    }

    public static void addObserver(InviteObservable inviteObservable) {
        if (observables == null) {
            observables = new ArrayList<>();
        }

        if (!observables.contains(inviteObservable)) {
            observables.add(inviteObservable);
            inviteObservable.updateInviteObserver();
        }
    }

    public static void removeObserver(InviteObservable inviteObservable) {
        if (observables == null) {
            return;
        }

        if (observables.contains(inviteObservable)) {
            observables.remove(inviteObservable);
        }
    }

    private static void updateObservers() {
        if (observables != null) {
            for (InviteObservable observable : observables) {
                observable.updateInviteObserver();
            }
        }
    }

    private static int getRandomColor() {
        if (random == null) {
            random = new Random();
        }

        return Color.argb(255, (random.nextInt(150)) + 25, (random.nextInt(150)) + 25, (random.nextInt(150) + 25));
    }

    public static void reset() {
        contactMap = null;
        invitesViewModel = null;
        inviteContactViewModels = null;
        updateObservers();
        observables = null;
        invitesPresenter = null;
    }

    public static void onInvitedButtonClicked(final Context context, FragmentManager fragmentManager,
                                              final InviteContactViewModel inviteContactViewModel,
                                              InvitesListFragment.InvitesType invitesType) {

        if (inviteContactViewModel == null || inviteContactViewModel.getInviteId() == null) {
            return;
        }

        boolean isResendEnabled = inviteContactViewModel.isResendEnabled();
        boolean isRevokeEnabled = inviteContactViewModel.isRevokeEnabled();

        if (!isResendEnabled || !isRevokeEnabled) {
            return;
        }

        ActionSheet.Builder builder = ActionSheet.createBuilder(context, fragmentManager);
        builder.setCancelButtonTitle(context.getString(R.string.profile_cancel));

        builder.setOtherButtonTitles(
                inviteContactViewModel.getEmail(),
                context.getString(R.string.invite_resend_invitation),
                context.getString(R.string.invite_revoke_invitation));

        inviteActionSheet = builder.setCancelableOnTouchOutside(true)
                .setListener(new InvitedActionSheetListener(inviteContactViewModel, invitesType)).show();


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (inviteActionSheet == null || inviteActionSheet.getActivity() == null ||
                        inviteActionSheet.getActivity().getWindow() == null) {
                    return;
                }
                View actionSheetView = inviteActionSheet.getActivity().getWindow().getDecorView();
                if (actionSheetView instanceof ViewGroup) {
                    for (int childIndex = 0; childIndex < ((ViewGroup) actionSheetView).getChildCount(); childIndex++) {
                        View child = ((ViewGroup) actionSheetView).getChildAt(childIndex);
                        if (child instanceof FrameLayout) {
                            for (int subChildIndex = 0; subChildIndex < ((FrameLayout) child).getChildCount(); subChildIndex++) {
                                View subChild = ((FrameLayout) child).getChildAt(subChildIndex);
                                if (subChild instanceof LinearLayout) {
                                    for (int buttonIndex = 0; buttonIndex < ((LinearLayout) subChild).getChildCount(); buttonIndex++) {
                                        View buttonView = ((LinearLayout) subChild).getChildAt(buttonIndex);
                                        if (buttonView instanceof Button) {
                                            ((Button) buttonView).setAllCaps(false);
                                        }
                                        if (buttonIndex == 0) {
                                            buttonView.setEnabled(false);
                                            ((Button) buttonView).setTextColor(ResourcesCompat.getColor(context.getResources(), R.color.c203, null));
                                            ((Button) buttonView).setTextSize(HPUIUtils.pxToDp(PrintOSApplication.getAppContext(),
                                                    (int) context.getResources().getDimension(R.dimen.invites_text_size)));
                                        }
                                    }
                                    subChild.invalidate();
                                }
                            }
                        }
                    }
                }
            }
        }, DELAY);
    }

    public static void revokeInvite(InviteContactViewModel inviteContactViewModel) {
        if (invitesPresenter == null) {
            invitesPresenter = new InvitesPresenter();
        }

        invitesPresenter.revokeContact(inviteContactViewModel);
        inviteContactViewModel.setInviteStatus(InviteContactViewModel.InviteStatus.LOADING);
        updateObservers();
    }

    public static void successfullyRevokedInvite(InviteContactViewModel inviteContactViewModel) {
        HPLogger.d(TAG, "revoke invitation success");

        inviteContactViewModel.setInviteStatus(InviteContactViewModel.InviteStatus.TO_BE_INVITED);
        if (contactMap.containsKey(InvitesListFragment.InvitesType.EMAIL)) {
            List<InviteContactViewModel> emailList = contactMap.get(InvitesListFragment.InvitesType.EMAIL);
            emailList.remove(inviteContactViewModel);
        }
        updateObservers();

        Analytics.sendEvent(Analytics.REVOKE_INVITE_SUCCESS);
    }

    public static void errorRevokingInvite(InviteContactViewModel inviteContactViewModel, APIException exception) {
        HPLogger.d(TAG, "revoke invitation fail");
        HPUIUtils.displayToast(PrintOSApplication.getAppContext(), HPLocaleUtils.getSimpleErrorMsg(
                PrintOSApplication.getAppContext(), exception));
        inviteContactViewModel.setInviteStatus(InviteContactViewModel.InviteStatus.INVITED);
        updateObservers();

        Analytics.sendEvent(Analytics.REVOKE_INVITE_FAIL);
    }

    public static ActionSheet getInviteActionSheet() {
        return inviteActionSheet;
    }

    public interface InviteObservable {
        void updateInviteObserver();
    }

    public static class InvitedActionSheetListener implements ActionSheet.ActionSheetListener {
        InviteContactViewModel inviteContactViewModel;
        InvitesListFragment.InvitesType invitesType;

        public InvitedActionSheetListener(InviteContactViewModel inviteContactViewModel,
                                          InvitesListFragment.InvitesType invitesType) {
            this.inviteContactViewModel = inviteContactViewModel;
            this.invitesType = invitesType;
        }

        @Override
        public void onDismiss(ActionSheet actionSheet, boolean b) {
            InviteUtils.inviteActionSheet = null;
        }

        @Override
        public void onOtherButtonClick(ActionSheet actionSheet, int i) {
            if (inviteContactViewModel == null) {
                return;
            }
            switch (i) {
                case 1:
                    if (inviteContactViewModel.isResendEnabled()) {
                        inviteContact(inviteContactViewModel, invitesType);
                    } else {
                        InviteUtils.revokeInvite(inviteContactViewModel);
                    }
                    break;
                case 2:
                    InviteUtils.revokeInvite(inviteContactViewModel);
                    break;
                default:
                    break;
            }
        }

        private static synchronized void inviteContact(InviteContactViewModel inviteContactViewModel, InvitesListFragment.InvitesType invitesType) {
            InviteUtils.inviteContact(inviteContactViewModel, invitesType, true, false);
        }
    }
}
