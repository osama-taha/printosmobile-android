package com.hp.printosmobile.data.remote.models;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by anwar asbah on 11/27/2017.
 */

public class AcceptEulaRequestBody {

    @JsonProperty("eulaVersion")
    private String eulaVersion;

    @JsonProperty("eulaVersion")
    public String getEulaVersion() {
        return eulaVersion;
    }

    @JsonProperty("eulaVersion")
    public void setEulaVersion(String eulaVersion) {
        this.eulaVersion = eulaVersion;
    }
}
