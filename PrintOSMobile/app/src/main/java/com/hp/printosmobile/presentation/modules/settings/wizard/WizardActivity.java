package com.hp.printosmobile.presentation.modules.settings.wizard;

import android.os.Bundle;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.hp.printosmobile.R;
import com.hp.printosmobile.presentation.modules.settings.BaseSettingsActivity;

import butterknife.Bind;

/**
 * Created by Osama
 */
public class WizardActivity extends BaseSettingsActivity implements WizardFragment.WizardFragmentCallbacks {

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.toolbar_display_title)
    TextView toolbarDisplayName;
    @Bind(R.id.toolbar_underline)
    View toolbarUnderline;

    private WizardFragment wizardFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(ResourcesCompat.getDrawable(getResources(), R.drawable.back_button, null));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        wizardFragment = (WizardFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_wizard);

        toolbarDisplayName.setText(getString(R.string.wizard_fragment_title));
        toolbarUnderline.setVisibility(View.GONE);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_wizard;
    }

    @Override
    public void onDismissButtonClicked() {
        finish();
    }

    @Override
    public void onWizardStepsCompleted(WizardViewModel wizardViewModel) {
        finish();
    }
    

    @Override
    public void onWizardFragmentAttached() {
        initPresenter(true);
    }

    @Override
    public void onBackPressed() {

        if (wizardFragment != null && wizardFragment.onBackPressed()) {
            return;
        }

        super.onBackPressed();
    }

    @Override
    public void updateProfileImageWithImageUrl(String url) {
        if (wizardFragment != null) {
            wizardFragment.updateProfilePhotoWithUrl(url);
        }
    }

}
