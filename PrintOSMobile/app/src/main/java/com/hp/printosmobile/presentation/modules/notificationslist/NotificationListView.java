package com.hp.printosmobile.presentation.modules.notificationslist;

import com.hp.printosmobilelib.core.communications.remote.APIException;

import java.util.List;

/**
 * Created by Anwar Asbah on 1/23/2017.
 */
public interface NotificationListView {

    void updateNotificationListView(List<NotificationViewModel> notificationViewModels);

    void onNotificationsError(APIException exception);

    void onAllNotificationsDeleted();

    void onDeleteAllNotificationsFailed(APIException e);
}
