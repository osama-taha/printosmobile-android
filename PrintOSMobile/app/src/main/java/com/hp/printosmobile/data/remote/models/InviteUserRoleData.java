package com.hp.printosmobile.data.remote.models;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Anwar Asbah 11/6/2017
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class InviteUserRoleData {

    @JsonProperty("invitationOrgTypeList")
    private List<InvitationOrgType> userRoles = new ArrayList<>();
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<>();

    @JsonProperty("invitationOrgTypeList")
    public List<InvitationOrgType> getUserRoles() {
        return userRoles;
    }

    @JsonProperty("invitationOrgTypeList")
    public void setUserRoles(List<InvitationOrgType> userRoles) {
        this.userRoles = userRoles;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class InvitationOrgType {
        @JsonProperty("invitationType")
        private String invitationType;
        @JsonProperty("systemRoles")
        private List<UserRole> userRoles;
        @JsonIgnore
        private Map<String, Object> additionalProperties = new HashMap<>();

        @JsonProperty("invitationType")
        public String getInvitationType() {
            return invitationType;
        }

        @JsonProperty("invitationType")
        public void setInvitationType(String invitationType) {
            this.invitationType = invitationType;
        }

        @JsonProperty("systemRoles")
        public List<UserRole> getUserRoles() {
            return userRoles;
        }

        @JsonProperty("systemRoles")
        public void setUserRoles(List<UserRole> userRoles) {
            this.userRoles = userRoles;
        }

        @JsonAnyGetter
        public Map<String, Object> getAdditionalProperties() {
            return this.additionalProperties;
        }

        @JsonAnySetter
        public void setAdditionalProperty(String name, Object value) {
            this.additionalProperties.put(name, value);
        }
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class UserRole {
        @JsonProperty("id")
        private String id;
        @JsonProperty("name")
        private String name;
        @JsonProperty("organizationType")
        private String organizationType;
        @JsonIgnore
        private Map<String, Object> additionalProperties = new HashMap<>();

        @JsonProperty("id")
        public String getId() {
            return id;
        }

        @JsonProperty("id")
        public void setId(String id) {
            this.id = id;
        }

        @JsonProperty("name")
        public String getName() {
            return name;
        }

        @JsonProperty("name")
        public void setName(String name) {
            this.name = name;
        }

        @JsonProperty("organizationType")
        public String getOrganizationType() {
            return organizationType;
        }

        @JsonProperty("organizationType")
        public void setOrganizationType(String organizationType) {
            this.organizationType = organizationType;
        }

        @JsonAnyGetter
        public Map<String, Object> getAdditionalProperties() {
            return this.additionalProperties;
        }

        @JsonAnySetter
        public void setAdditionalProperty(String name, Object value) {
            this.additionalProperties.put(name, value);
        }
    }
}
