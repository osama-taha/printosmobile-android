package com.hp.printosmobile.presentation.modules.login;

import android.content.Context;

import com.hp.printosmobile.Analytics;
import com.hp.printosmobile.AnswersSdk;
import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;
import com.hp.printosmobile.aaa.AAAManager;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.presentation.Presenter;
import com.hp.printosmobile.presentation.indigowidget.PrintOSAppWidgetProvider;
import com.hp.printosmobilelib.core.communications.remote.APIException;
import com.hp.printosmobilelib.core.communications.remote.SessionManager;
import com.hp.printosmobilelib.core.communications.remote.models.UserCredentials;
import com.hp.printosmobilelib.core.communications.remote.models.UserData;
import com.hp.printosmobilelib.core.logging.HPLogQueue;
import com.hp.printosmobilelib.core.logging.HPLogger;

import retrofit2.Response;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * A presenter to manage the {@link LoginActivity}
 *
 * @author Osama Taha
 */
public class LoginPresenter extends Presenter<LoginView> {

    private static final String TAG = LoginPresenter.class.getName();

    private Subscription subscription;

    public LoginPresenter() {

    }

    public void login(final String userName, final String password, final String eulaVersion) {

        mView.showLoading();
        mView.setLoginInButtonEnabled(false);

        final UserCredentials credentials = new UserCredentials(userName, password, eulaVersion);

        subscription = PrintOSApplication.getApiServicesProvider().getLoginService().login("", credentials)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<Response<UserData>>() {
                    @Override
                    public void onCompleted() {

                        mView.hideLoading();

                    }

                    @Override
                    public void onError(Throwable e) {

                        mView.setLoginInButtonEnabled(true);
                        mView.hideLoading();
                        if (e instanceof APIException) {
                            mView.showGeneralLoginError((APIException) e);
                        } else {
                            mView.showGeneralLoginError(APIException.create(APIException.Kind.UNEXPECTED));
                        }
                    }

                    @Override
                    public void onNext(Response<UserData> result) {

                        if (result.isSuccessful()) {

                            if (result.body() == null || result.body().getContext() == null || result.body().getUser() == null
                                    || result.body().getContext().getId() == null) {
                                SessionManager.getInstance(PrintOSApplication.getAppContext()).clearCookie();
                                mView.setLoginInButtonEnabled(true);
                                mView.hideLoading();
                                mView.showErrorMsg(PrintOSApplication.getAppContext().getString(R.string.error_login_no_context));
                                return;
                            }

                            Context context = PrintOSApplication.getAppContext();

                            UserData.Context organization = result.body().getContext();
                            UserData.User user = result.body().getUser();

                            //Save user & current org, info
                            PrintOSPreferences.getInstance(context).onLoginCompleted(user, organization);

                            //Save user credentials for auto login.
                            SessionManager.getInstance(context).saveUserCredentials(credentials);

                            //log login completed.
                            HPLogger.i(TAG, String.format("user logged in successfully. locale used: %s", PrintOSPreferences.getInstance(context).getLanguageCode()));
                            HPLogger.d(TAG, "user first name " + user.getFirstName());

                            //Send events.
                            Analytics.sendEvent(Analytics.LOGIN_EVENT_LOGIN_ACTION, user.getUserId() + "|" + organization.getId());
                            Analytics.sendEvent(Analytics.EVENT_LOGIN_SUCCESS);
                            AnswersSdk.logLogin(AnswersSdk.LOGIN_METHOD_BUTTON, true);

                            //Update widget.
                            PrintOSAppWidgetProvider.updateAllWidgets(context);

                            //Send pre-login logs to the server.
                            HPLogQueue.getInstance(context).flushPreLoginLogs();

                            mView.onLoginSuccessful(result.body());

                            AAAManager.initUser();

                        } else {

                            mView.setLoginInButtonEnabled(true);
                        }
                    }
                });

        addSubscriber(subscription);
    }

    @Override
    public void detachView() {
        stopSubscribers();
        mView = null;
    }

}