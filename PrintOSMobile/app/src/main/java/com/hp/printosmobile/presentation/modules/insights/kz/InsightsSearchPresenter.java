package com.hp.printosmobile.presentation.modules.insights.kz;

import android.text.TextUtils;
import android.widget.AutoCompleteTextView;

import com.hp.printosmobile.Analytics;
import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.data.remote.ApiServicesProvider;
import com.hp.printosmobile.data.remote.models.kz.KZSearchBody;
import com.hp.printosmobile.data.remote.models.kz.KZSearchResult;
import com.hp.printosmobile.data.remote.models.kz.KzAutoCompleteData;
import com.hp.printosmobile.data.remote.services.InsightsService;
import com.hp.printosmobile.presentation.Presenter;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitEnum;
import com.hp.printosmobile.presentation.modules.shared.LanguageEnum;
import com.jakewharton.rxbinding.widget.AdapterViewItemClickEvent;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import retrofit2.Response;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by Osama on 3/14/18.
 */
public class InsightsSearchPresenter extends Presenter<InsightsSearchView> {

    public static final int PAGE_SIZE = 25;
    private static final long DEBOUNCE_TIMEOUT = 300;
    private static final int MIN_LENGTH_TO_START = 2;

    private final BusinessUnitEnum businessUnitEnum;
    private final List<String> supportedFormats;
    private Subscription subscription;
    private String searchText;
    private int totalResults;
    private boolean isLastPage;


    public InsightsSearchPresenter(BusinessUnitEnum businessUnitEnum) {
        this.businessUnitEnum = businessUnitEnum;
        this.supportedFormats = PrintOSPreferences.getInstance(PrintOSApplication.getAppContext())
                .getInsightsConfigurations().getKzSupportedFormats();
    }

    private Observable<Response<KZSearchResult>> getSearchResponse(String query, int pageOffset) {

        ApiServicesProvider serviceProvider = PrintOSApplication.getApiServicesProvider();
        InsightsService insightsService = serviceProvider.getInsightsService();

        KZSearchBody body = new KZSearchBody();
        body.setFormats(supportedFormats);
        body.setFrom(pageOffset);
        body.setQuery(query);
        body.setSize(PAGE_SIZE);

        String appLanguage = PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).getLanguage();
        LanguageEnum languageEnum = LanguageEnum.from(appLanguage);

        return insightsService.searchKnowledgeZone(body, businessUnitEnum.getKzName(), true, languageEnum.getKzLanguageCode());

    }

    private Observable<Response<List<KzAutoCompleteData>>> getAutoCompleteResults(String query) {

        ApiServicesProvider serviceProvider = PrintOSApplication.getApiServicesProvider();
        InsightsService insightsService = serviceProvider.getInsightsService();

        String appLanguage = PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).getLanguage();
        LanguageEnum languageEnum = LanguageEnum.from(appLanguage);

        return insightsService.searchSuggestions(query, supportedFormats, businessUnitEnum.getKzName(),
                true, languageEnum.getKzLanguageCode());

    }

    public void onLoadSearch(String query) {

        if (mView == null) {
            return;
        }

        isLastPage = false;
        mView.hideEmptyView();
        mView.hideErrorView();

        subscription = Observable.just(query)
                .doOnNext(new Action1<CharSequence>() {
                    @Override
                    public void call(CharSequence charSequence) {
                        if (mView != null) {
                            mView.hideLoadingView();
                        }
                    }
                }).debounce(50, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .filter(new Func1<CharSequence, Boolean>() {
                    @Override
                    public Boolean call(CharSequence charSequence) {

                        if (mView == null) {
                            return false;
                        }

                        if (TextUtils.isEmpty(charSequence)) {
                            mView.hideLoadingView();

                            mView.clearResults();
                            mView.hideResultsView();

                            mView.hideEmptyView();
                        } else {
                            mView.showLoadingView();
                        }

                        return !TextUtils.isEmpty(charSequence);
                    }
                })
                .map(new Func1<CharSequence, String>() {
                    @Override
                    public String call(CharSequence charSequence) {
                        return charSequence.toString();
                    }
                }).subscribe(new Action1<String>() {
                    @Override
                    public void call(final String query) {

                        if (businessUnitEnum != null) {
                            String label = String.format("%s(%s)", query, businessUnitEnum.getKzName());
                            Analytics.sendEvent(Analytics.SEARCH_INSIGHTS, label);
                        }

                        getSearchResponse(query, 0)
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread()).
                                subscribe(new Subscriber<Response<KZSearchResult>>() {
                                    @Override
                                    public void onCompleted() {

                                    }

                                    @Override
                                    public void onError(Throwable e) {

                                        if (mView == null) {
                                            return;
                                        }

                                        mView.hideLoadingView();
                                        mView.showErrorView();
                                        mView.hideResultsView();
                                        totalResults = 0;
                                    }

                                    @Override
                                    public void onNext(Response<KZSearchResult> listResponse) {

                                        if (mView == null) {
                                            return;
                                        }

                                        if (listResponse.isSuccessful()) {

                                            searchText = query;
                                            mView.hideLoadingView();
                                            mView.clearResults();

                                            boolean hasResults = listResponse.body().getSearchAssetResponses() != null &&
                                                    !listResponse.body().getSearchAssetResponses().isEmpty();

                                            if (hasResults) {
                                                totalResults = listResponse.body().getTotalAssetsCount().intValue();
                                                mView.addResults(listResponse.body().getSearchAssetResponses());
                                                mView.showResultsView();
                                            } else {
                                                totalResults = 0;
                                                mView.hideResultsView();
                                            }

                                            if (hasResults) {
                                                mView.hideEmptyView();
                                            } else {
                                                mView.setEmptyText(searchText);
                                                mView.showEmptyView();
                                            }

                                        } else {

                                            totalResults = 0;
                                            mView.hideLoadingView();
                                            mView.showErrorView();

                                        }

                                    }
                                });

                    }
                });

    }

    public void getNextPage(final int pageOffset) {

        if (mView == null) {
            return;
        }

        mView.addFooterView();
        mView.showLoadingFooterView();
        isLastPage = pageOffset + PAGE_SIZE >= totalResults;

        getSearchResponse(searchText, pageOffset)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).
                subscribe(new Subscriber<Response<KZSearchResult>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                        if (mView != null) {
                            mView.addFooterView();
                            mView.showErrorFooterView();
                        }

                    }

                    @Override
                    public void onNext(Response<KZSearchResult> listResponse) {

                        if (mView == null) {
                            return;
                        }

                        if (listResponse.isSuccessful()) {

                            boolean hasResults = listResponse.body().getSearchAssetResponses() != null &&
                                    !listResponse.body().getSearchAssetResponses().isEmpty();

                            if (hasResults) {

                                mView.addResults(listResponse.body().getSearchAssetResponses());

                                if (isLastPage) {
                                    mView.addFooterView();
                                    mView.showNoResultsFooterView();
                                } else {
                                    mView.removeFooterView();
                                }

                            } else {
                                mView.addFooterView();
                                mView.showNoResultsFooterView();
                            }

                        } else {
                            mView.addFooterView();
                            mView.showErrorFooterView();
                        }
                    }
                });

    }

    public void addOnAutoCompleteTextViewTextChangedObserver(Observable<CharSequence> searchQueryChangeObservable) {

        subscription = searchQueryChangeObservable
                .filter(new Func1<CharSequence, Boolean>() {
                    @Override
                    public Boolean call(CharSequence charSequence) {
                        if (TextUtils.isEmpty(charSequence)) {
                            mView.addAutoCompleteResults(new ArrayList<KZAutoCompleteResult>());
                        }
                        return charSequence.length() >= MIN_LENGTH_TO_START;
                    }
                })
                .debounce(DEBOUNCE_TIMEOUT, TimeUnit.MILLISECONDS)
                .map(new Func1<CharSequence, String>() {
                    @Override
                    public String call(CharSequence charSequence) {
                        return charSequence.toString();
                    }
                })
                .observeOn(Schedulers.io())
                .switchMap(new Func1<String, Observable<? extends Response<List<KzAutoCompleteData>>>>() {
                    @Override
                    public Observable<? extends Response<List<KzAutoCompleteData>>> call(String query) {
                        return getAutoCompleteResults(query);
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .retry()
                .subscribe(new Subscriber<Response<List<KzAutoCompleteData>>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(Response<List<KzAutoCompleteData>> listResponse) {

                        if (mView == null) {
                            return;
                        }

                        if (listResponse.isSuccessful()) {

                            List<KZAutoCompleteResult> kzAutoCompleteResults = new ArrayList<>();

                            for (KzAutoCompleteData data : listResponse.body()) {

                                KZAutoCompleteResult autoCompleteResult = new KZAutoCompleteResult();
                                autoCompleteResult.setName(data.getName());
                                autoCompleteResult.setType(KZAutoCompleteResult.KZAutoCompleteResultType.from(data.getType()));
                                autoCompleteResult.setOrderRes(data.getOrderRes());
                                kzAutoCompleteResults.add(autoCompleteResult);
                            }

                            mView.addAutoCompleteResults(kzAutoCompleteResults);

                        }
                    }
                });
    }

    public void addOnAutoCompleteTextViewItemClickedSubscriber(final AutoCompleteTextView autoCompleteTextView, Observable<AdapterViewItemClickEvent> itemClickedEvent) {

        subscription = itemClickedEvent
                .map(new Func1<AdapterViewItemClickEvent, KZAutoCompleteResult>() {
                    @Override
                    public KZAutoCompleteResult call(AdapterViewItemClickEvent adapterViewItemClickEvent) {
                        return (KZAutoCompleteResult) autoCompleteTextView.getAdapter().getItem(adapterViewItemClickEvent.position());
                    }
                })
                .observeOn(Schedulers.io())
                .subscribe(new Subscriber<KZAutoCompleteResult>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(KZAutoCompleteResult autoCompleteResult) {

                        if (mView == null || autoCompleteResult == null) {
                            return;
                        }

                        mView.onAutoCompleteResultSelected(autoCompleteResult);

                    }
                });


    }

    public boolean isLastPage() {
        return isLastPage;
    }

    public String getSearchText() {
        return searchText;
    }

    @Override
    public void detachView() {
        if (subscription != null) {
            subscription.unsubscribe();
        }
    }

    public void onReloadButtonClicked(String searchText) {
        onLoadSearch(searchText);
    }


}