package com.hp.printosmobile.data.remote.models;

/**
 * A model representing the request body of the ResetPasswordService
 *
 * @author Anwar Asbah
 */
public class ResetPasswordBody {

    private String loginNameOrEmail;

    public ResetPasswordBody(String userName) {
        this.loginNameOrEmail = userName;
    }

    public String getLoginNameOrEmail() {
        return loginNameOrEmail;
    }

    public void setLoginNameOrEmail(String loginNameOrEmail) {
        this.loginNameOrEmail = loginNameOrEmail;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ResetPasswordBody{");
        sb.append("loginNameOrEmail='").append(loginNameOrEmail).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
