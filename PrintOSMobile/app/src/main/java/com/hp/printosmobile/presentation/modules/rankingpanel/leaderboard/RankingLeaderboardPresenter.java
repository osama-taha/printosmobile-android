package com.hp.printosmobile.presentation.modules.rankingpanel.leaderboard;

import android.content.Context;

import com.hp.printosmobile.data.remote.models.PermissionsData;
import com.hp.printosmobile.presentation.PermissionsManager;
import com.hp.printosmobile.presentation.Presenter;
import com.hp.printosmobile.presentation.modules.home.HomeDataManager;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitViewModel;
import com.hp.printosmobile.presentation.modules.rankingpanel.RankBreakdownViewModel;
import com.hp.printosmobile.presentation.modules.rankingpanel.RankingDataManager;
import com.hp.printosmobile.presentation.modules.week.RankingViewModel;

import java.util.List;

import retrofit2.Response;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func2;
import rx.schedulers.Schedulers;

/**
 * Created by Osama Taha
 */
public class RankingLeaderboardPresenter extends Presenter<RankingLeaderboardView> {

    private static final String TAG = RankingLeaderboardPresenter.class.getSimpleName();

    public static final int RANKING_LEADERBOARD_BEAT_COINS = 50;

    private Subscription subscription;
    private BusinessUnitViewModel businessUnitViewModel;
    private RankingViewModel rankingViewModel;
    private boolean hasGraphs;

    public void requestRankingLeaderboard() {

        subscription = RankingDataManager.requestRankingLeaderboardPermission()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<Response<Void>>() {

                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (mView != null) {
                            mView.onRequestLeaderboardPermissionFailed();
                        }
                    }

                    @Override
                    public void onNext(Response<Void> voidResponse) {

                        if (mView != null && voidResponse.isSuccessful()) {

                            PermissionsManager.getInstance().getPermissionsData(true).subscribe(new Subscriber<Response<PermissionsData>>() {
                                @Override
                                public void onCompleted() {

                                }

                                @Override
                                public void onError(Throwable e) {
                                    if (mView != null) {
                                        mView.onRequestLeaderboardPermissionFailed();
                                    }
                                }

                                @Override
                                public void onNext(Response<PermissionsData> permissionsDataResponse) {

                                    if (mView == null) {
                                        return;
                                    }

                                    if (permissionsDataResponse.isSuccessful()) {
                                        mView.onRequestLeaderboardPermissionCompleted();
                                    } else {
                                        mView.onRequestLeaderboardPermissionFailed();
                                    }
                                }
                            });
                        }
                    }
                });

        addSubscriber(subscription);
    }

    public void init(Context context) {

        if (mView == null || businessUnitViewModel == null
                || businessUnitViewModel.getFiltersViewModel() == null) {
            return;
        }

        mView.showLoading();

        String siteId = businessUnitViewModel.getFiltersViewModel().getSelectedSite().getId();

        Observable.combineLatest(HomeDataManager.getSiteRankingData(context, businessUnitViewModel),
                RankingDataManager.getBreakdownRankData(siteId), new Func2<RankingViewModel, List<RankBreakdownViewModel>, RankingViewModel>() {
                    @Override
                    public RankingViewModel call(RankingViewModel rankingViewModel, List<RankBreakdownViewModel> rankBreakdownViewModels) {

                        if (rankingViewModel == null) {
                            hasGraphs = false;
                            return null;
                        }

                        if (rankBreakdownViewModels != null && rankBreakdownViewModels.size() > 0) {

                            hasGraphs = true;

                            for (RankBreakdownViewModel rankBreakdownViewModel : rankBreakdownViewModels) {

                                if (rankingViewModel.hasWorldWideSection() && rankBreakdownViewModel.getType() == RankingViewModel.RankAreaType.WORLD_WIDE) {
                                    rankingViewModel.getWorldWideViewModel().setRankBreakdownViewModel(rankBreakdownViewModel);
                                } else if (rankingViewModel.hasRegionSection() && rankBreakdownViewModel.getType() == RankingViewModel.RankAreaType.REGION) {
                                    rankingViewModel.getRegionViewModel().setRankBreakdownViewModel(rankBreakdownViewModel);
                                } else if (rankingViewModel.hasSubRegionSection()) {

                                    RankingViewModel.RankAreaType subRegionRankType = rankingViewModel.getSubRegionViewModel().getRankAreaType();

                                    if (subRegionRankType == RankingViewModel.RankAreaType.COUNTRY &&
                                            rankBreakdownViewModel.getType() == RankingViewModel.RankAreaType.COUNTRY) {
                                        rankingViewModel.getSubRegionViewModel().setRankBreakdownViewModel(rankBreakdownViewModel);
                                    }

                                    if (subRegionRankType == RankingViewModel.RankAreaType.SUB_REGION &&
                                            rankBreakdownViewModel.getType() == RankingViewModel.RankAreaType.SUB_REGION) {
                                        rankingViewModel.getSubRegionViewModel().setRankBreakdownViewModel(rankBreakdownViewModel);
                                    }
                                }
                            }
                        } else {

                            hasGraphs = true;
                        }

                        return rankingViewModel;
                    }
                }).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<RankingViewModel>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                        hasGraphs = false;

                        if (mView != null) {
                            setRankingViewModel(null);
                            mView.onGettingSiteRankingViewModelFailed();
                        }
                    }

                    @Override
                    public void onNext(RankingViewModel rankingViewModel) {
                        if (mView != null) {
                            setRankingViewModel(rankingViewModel);
                            if (hasGraphs && businessUnitViewModel != null) {
                                mView.onGettingSiteRankingViewModelCompleted(rankingViewModel, businessUnitViewModel.getFiltersViewModel().isGroupSelected());
                            } else {
                                mView.onGettingSiteRankingViewModelFailed();
                            }
                        }
                    }
                });

    }

    public void setRankingViewModel(RankingViewModel rankingViewModel) {
        this.rankingViewModel = rankingViewModel;
    }

    public void setBusinessUnitViewModel(BusinessUnitViewModel businessUnitViewModel) {
        this.businessUnitViewModel = businessUnitViewModel;
    }

    public RankingViewModel getRankingViewModel() {
        return rankingViewModel;
    }

    public BusinessUnitViewModel getBusinessUnitViewModel() {
        return businessUnitViewModel;
    }

    @Override
    public void detachView() {
        stopSubscribers();
        mView = null;
    }

}
