package com.hp.printosmobile.presentation.modules.eula;

import com.hp.printosmobile.presentation.Presenter;
import com.hp.printosmobilelib.core.communications.remote.APIException;
import com.hp.printosmobilelib.core.logging.HPLogger;

import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Anwar Asbah on 9/1/2016.
 */
public class EulaPresenter extends Presenter<EulaView> {

    private static final String TAG = EulaPresenter.class.getName();

    public void getEula(String language) {

        Subscription subscription = EulaDataManager.getEulaText(language)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<EULAViewModel>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        HPLogger.d(TAG, "unable to get the EULA data " + e);
                        onRequestError(e, EulaActivity.TAG);
                    }

                    @Override
                    public void onNext(EULAViewModel eulaViewModel) {
                        mView.displayEula(eulaViewModel);
                    }
                });
        addSubscriber(subscription);
    }


    public void acceptEulaAndPrivacyVersion(EULAViewModel eulaViewModel) {

        if (eulaViewModel == null || eulaViewModel.getPrivacyVersion() == null) {
            return;
        }

        Subscription subscription = EulaDataManager.acceptEulaAndPrivacyVersion(eulaViewModel)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<Boolean>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        HPLogger.d(TAG, "unable to accept eula and privacy version");
                        onRequestError(e, EulaActivity.TAG_ACCEPT);
                    }

                    @Override
                    public void onNext(Boolean aBoolean) {
                        HPLogger.d(TAG, "successfully accepted eula and privacy version");
                        if (mView != null) {
                            mView.onEulaSuccessfullyAccepted();
                        }
                    }
                });
        addSubscriber(subscription);
    }

    private void onRequestError(Throwable e, String tag) {
        APIException exception = APIException.create(APIException.Kind.UNEXPECTED);
        if (e instanceof APIException) {
            exception = (APIException) e;
        }

        if (mView != null) {
            mView.onError(exception, tag);
        }
    }

    @Override
    public void detachView() {
        stopSubscribers();
        mView = null;
    }

}
