package com.hp.printosmobile.presentation.modules.invites;

import android.app.Dialog;
import android.app.DialogFragment;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hp.printosmobile.Analytics;
import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.utils.HPLocaleUtils;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by anwar asbah on 12/04/2017.
 */
public class InviteAllPopup extends DialogFragment {

    public static final String TAG = InviteAllPopup.class.getName();

    public static final String NUMBER_OF_CONTACTS_ARG = "number_of_contacts_arg";
    private static final String EMAIL_DOMAIN_FORMAT = "@%s";

    @Bind(R.id.description_text_view)
    TextView descriptionTextView;
    @Bind(R.id.button_no_thanks)
    TextView buttonNoThanks;

    private InviteAllPopupCallback listener;
    private int numberOfContactsToBeInvited;

    public static InviteAllPopup getInstance(InviteAllPopupCallback popupCallback, int numberOfContactsToBeInvited) {
        InviteAllPopup dialog = new InviteAllPopup();

        dialog.listener = popupCallback;
        Bundle bundle = new Bundle();
        bundle.putInt(NUMBER_OF_CONTACTS_ARG, numberOfContactsToBeInvited);
        dialog.setArguments(bundle);

        return dialog;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE,
                R.style.InvitesPopupStyle);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        Bundle bundle = getArguments();
        if (bundle != null && bundle.containsKey(NUMBER_OF_CONTACTS_ARG)) {
            numberOfContactsToBeInvited = bundle.getInt(NUMBER_OF_CONTACTS_ARG);
        }

        return inflater.inflate(R.layout.dialog_invite_all, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ButterKnife.bind(this, view);
        initView();
        getDialog().setCanceledOnTouchOutside(false);
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null && dialog.getWindow() != null) {
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        }
    }

    private void initView() {
        int coinsPerUser = PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).getBeatCoinsPerInvite();
        String coins = HPLocaleUtils.getLocalizedValue(coinsPerUser * numberOfContactsToBeInvited);
        String text = getString(R.string.invites_popup_invite_all_msg,
                String.format(EMAIL_DOMAIN_FORMAT, InviteUtils.getUserEmailDomain()), coins);

        int startInd = text.indexOf(coins);
        int endInd = startInd + coins.length();
        SpannableString spannableString = new SpannableString(text);
        spannableString.setSpan(new StyleSpan(Typeface.BOLD), startInd, endInd,
                Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        spannableString.setSpan(new RelativeSizeSpan(1.25f), startInd, endInd,
                Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        descriptionTextView.setText(spannableString);

        String noThanks = getString(R.string.invites_popup_invite_no_thanks);
        SpannableString spannable = new SpannableString(noThanks);
        spannable.setSpan(new UnderlineSpan(), 0, noThanks.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        buttonNoThanks.setText(spannable);
    }

    @OnClick(R.id.button_invite_all)
    public void onInviteAllButtonClicked() {
        if (listener != null) {
            Analytics.sendEvent(Analytics.INVITE_ALL_VIA_POPUP);

            listener.onInviteAllClicked();
        }
        dismissAllowingStateLoss();
    }

    @OnClick(R.id.button_remind_me_later)
    public void onRemindMeLaterButtonClicked() {
        Analytics.sendEvent(Analytics.INVITE_CLICK_REMIND_ME_LATER);

        dismissAllowingStateLoss();
    }

    @OnClick(R.id.button_no_thanks)
    public void onNoThanksButtonClicked() {
        Analytics.sendEvent(Analytics.INVITE_CLICK_NO_THANKS);

        PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).setInviteAllPopupSuppressed(true);
        dismissAllowingStateLoss();
    }

    public interface InviteAllPopupCallback {
        void onInviteAllClicked();
    }
}
