package com.hp.printosmobile.utils;

import android.content.Context;
import android.os.Bundle;

import com.hp.printosmobile.Constants;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitEnum;

/**
 * Created by Anwar Asbha on 7/23/2018.
 */

public class UniversalLinksUtils {

    private static final String PARAMETER_LINK_DIVIDER = "\\?";
    private static final String PARAMETER_DIVIDER = "&";
    private static final String PARAMETER_VAL_DIVIDER = "=";
    private static final String KNOWLEDGE_ZONE_LINK_REGEX = ".*/knowledge-zone/#/results.*";
    private static final String KNOWLEDGE_ZONE_ASSETS_REGEX = ".*/knowledge-zone/#/view/asset/[1-9].*";
    private static final String KNOWLEDGE_ZONE_ASSETS_PATH_SPLIT_REGEX = "asset/";

    public static final String KNOWLEDGE_ZONE_BU_PARAM = "businessUnit";
    public static final String KNOWLEDGE_ZONE_SEARCH_PARAM = "q";

    public static String getQueryParameter(String link, String key) {
        if (link == null || key == null) {
            return null;
        }

        String[] parameterSplit = link.split(PARAMETER_LINK_DIVIDER);
        if (parameterSplit.length >= 2) {
            String parameters = parameterSplit[1];
            if (parameters != null) {
                String[] parameterList = parameters.split(PARAMETER_DIVIDER);
                for (String param : parameterList) {
                    String[] paramVal = param.split(PARAMETER_VAL_DIVIDER);
                    if (paramVal.length == 2 && paramVal[0].equalsIgnoreCase(key)) {
                        return paramVal[1];
                    }
                }
            }
        }

        return null;
    }

    public static String getKnowledgeZoneAssetID(String link) {

        if (link == null) {
            return null;
        }

        String linkWithoutParams = link.split(PARAMETER_LINK_DIVIDER)[0];
        String[] linkWithoutParamsSplit = linkWithoutParams.split(KNOWLEDGE_ZONE_ASSETS_PATH_SPLIT_REGEX);
        if (linkWithoutParamsSplit.length >= 2) {
            String assetID = linkWithoutParamsSplit[1];
            if (assetID != null) {
                return assetID.replace("/", "");
            }
        }

        return null;
    }

    public static Bundle getBundle(Context context, String link) {

        if (context == null || link == null) {
            return null;
        }

        Bundle deepLinkBundle = null;

        String orgID = PrintOSPreferences.getInstance(context).getSavedOrganizationId();

        if (UniversalLinksUtils.isKnowledgeZoneAssetLink(link)) {
            deepLinkBundle = new Bundle();
            if (orgID != null) {
                deepLinkBundle.putString(Constants.IntentExtras.MAIN_ACTIVITY_ORGANIZATION_EXTRA, orgID);
            }

            String bu = UniversalLinksUtils.getQueryParameter(link, UniversalLinksUtils.KNOWLEDGE_ZONE_BU_PARAM);
            BusinessUnitEnum businessUnitEnum = BusinessUnitEnum.fromKzBu(bu);
            deepLinkBundle.putString(Constants.IntentExtras.MAIN_ACTIVITY_DIVISION_EXTRA, businessUnitEnum.getName());

            deepLinkBundle.putInt(Constants.UNIVERSAL_LINK_SCREEN_KEY, Constants.UNIVERSAL_LINK_SCREEN_INSIGHTS_ITEM_DETAILS);
            deepLinkBundle.putString(Constants.IntentExtras.MAIN_ACTIVITY_OBJECT_EXTRA, UniversalLinksUtils.getKnowledgeZoneAssetID(link));
        }

        if (UniversalLinksUtils.isKnowledgeZoneSearchLink(link)) {
            deepLinkBundle = new Bundle();
            if (orgID != null) {
                deepLinkBundle.putString(Constants.IntentExtras.MAIN_ACTIVITY_ORGANIZATION_EXTRA, orgID);
            }

            String bu = UniversalLinksUtils.getQueryParameter(link, UniversalLinksUtils.KNOWLEDGE_ZONE_BU_PARAM);
            BusinessUnitEnum businessUnitEnum = BusinessUnitEnum.fromKzBu(bu);
            deepLinkBundle.putString(Constants.IntentExtras.MAIN_ACTIVITY_DIVISION_EXTRA, businessUnitEnum.getName());

            deepLinkBundle.putInt(Constants.UNIVERSAL_LINK_SCREEN_KEY, Constants.UNIVERSAL_LINK_SCREEN_INSIGHTS_SEARCH);
            deepLinkBundle.putString(Constants.IntentExtras.MAIN_ACTIVITY_OBJECT_EXTRA, UniversalLinksUtils.getQueryParameter(link, UniversalLinksUtils.KNOWLEDGE_ZONE_SEARCH_PARAM));
        }

        return deepLinkBundle;
    }

    public static boolean isKnowledgeZoneAssetLink(String link) {
        return link != null && link.matches(KNOWLEDGE_ZONE_ASSETS_REGEX);
    }

    public static boolean isKnowledgeZoneSearchLink(String link) {
        return link != null && link.matches(KNOWLEDGE_ZONE_LINK_REGEX);
    }

}
