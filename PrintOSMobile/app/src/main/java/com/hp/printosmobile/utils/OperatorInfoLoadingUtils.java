package com.hp.printosmobile.utils;

import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;
import com.hp.printosmobile.presentation.modules.shared.DeviceViewModel;

/**
 * Created by Minerva on 4/12/2018.
 */
public class OperatorInfoLoadingUtils {


    public static boolean checkOperatorInfoAvailable(DeviceViewModel deviceViewModel) {
        return deviceViewModel.getOperatorInfo() != null && (!TextUtils.isEmpty(deviceViewModel.getOperatorInfo().getOperatorFirstName()) || !TextUtils.isEmpty(deviceViewModel.getOperatorInfo().getOperatorLastName()));
    }

    public static void loadOperatorInfo(final ImageView pressOperatorImage, final TextView pressOperatorInitials, final TextView pressOperatorName, final DeviceViewModel deviceViewModel) {
        ImageLoadingUtils.setLoadedImageFromUrl(PrintOSApplication.getAppContext(), pressOperatorImage, deviceViewModel.getOperatorInfo().getOperatorImgUrl(), R.drawable.avatar, new ImageLoadingUtils.ImageLoaderCallback() {
            @Override
            public void loadImageCompleted() {
            }

            @Override
            public void loadImageError() {
                pressOperatorImage.setVisibility(View.GONE);
                pressOperatorInitials.setVisibility(View.VISIBLE);
                pressOperatorInitials.setText(HPStringUtils.getInitials(deviceViewModel.getOperatorInfo().getOperatorFirstName(), deviceViewModel.getOperatorInfo().getOperatorLastName()));

            }
        }, false);

        if (pressOperatorName == null) {
            setOnClick(pressOperatorImage, deviceViewModel);
            setOnClick(pressOperatorInitials, deviceViewModel);
        } else {
            pressOperatorName.setVisibility(View.VISIBLE);
            pressOperatorName.setText(HPStringUtils.concatStringsWithSpace(deviceViewModel.getOperatorInfo().getOperatorFirstName(), deviceViewModel.getOperatorInfo().getOperatorLastName()));
        }
    }

    private static void setOnClick(final View view, final DeviceViewModel deviceViewModel) {
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HPUIUtils.displayToast(PrintOSApplication.getAppContext(), HPStringUtils.concatStringsWithSpace(deviceViewModel.getOperatorInfo().getOperatorFirstName(), deviceViewModel.getOperatorInfo().getOperatorLastName()));
            }
        });
    }

    public static void hideOperatorInfo(ImageView pressOperatorImage, TextView pressOperatorInitials) {
        pressOperatorImage.setVisibility(View.GONE);
        pressOperatorInitials.setVisibility(View.GONE);
    }
}
