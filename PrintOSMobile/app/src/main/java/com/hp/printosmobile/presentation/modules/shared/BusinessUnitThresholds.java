package com.hp.printosmobile.presentation.modules.shared;

import com.hp.printosmobile.presentation.modules.main.BusinessUnitEnum;

/**
 * Created by Anwar Asbah on 10/15/2017.
 */

public class BusinessUnitThresholds {

    private BusinessUnitEnum businessUnitEnum;
    private int successPercent;
    private int averagePercent;
    private int failPercent;

    public int getSuccessPercent() {
        return successPercent;
    }

    public void setSuccessPercent(int successPercent) {
        this.successPercent = successPercent;
    }

    public int getAveragePercent() {
        return averagePercent;
    }

    public void setAveragePercent(int averagePercent) {
        this.averagePercent = averagePercent;
    }

    public int getFailPercent() {
        return failPercent;
    }

    public void setFailPercent(int failPercent) {
        this.failPercent = failPercent;
    }

    public BusinessUnitEnum getBusinessUnitEnum() {
        return businessUnitEnum;
    }

    public void setBusinessUnitEnum(BusinessUnitEnum businessUnitEnum) {
        this.businessUnitEnum = businessUnitEnum;
    }
}
