package com.hp.printosmobile.presentation.modules.week;

import com.hp.printosmobile.R;
import com.hp.printosmobile.presentation.modules.rankingpanel.RankBreakdownViewModel;

import java.io.Serializable;

/**
 * Created by Osama Taha on 7/9/17.
 */
public class RankingViewModel implements Serializable {

    private Integer week;
    private boolean showTrophy;
    private String trophyMessage;
    private boolean hasSubRegionSection;
    private SiteRankViewModel regionViewModel;
    private boolean hasRegionSection;
    private SiteRankViewModel subRegionViewModel;
    private boolean hasWorldWideSection;
    private SiteRankViewModel worldWideViewModel;
    private String top5RankingRegionMessage;
    private boolean topFiveWorld;
    private boolean topFiveRegion;
    private boolean topFiveSubRegion;
    private Integer year;
    private boolean top10World;
    private boolean top10Region;
    private boolean top10SubRegion;
    private boolean top100World;

    public boolean isShowTrophy() {
        return showTrophy;
    }

    public void setShowTrophy(boolean showTrophy) {
        this.showTrophy = showTrophy;
    }

    public String getTrophyMessage() {
        return trophyMessage;
    }

    public void setTrophyMessage(String trophyMessage) {
        this.trophyMessage = trophyMessage;
    }

    public boolean hasSubRegionSection() {
        return hasSubRegionSection;
    }

    public void setHasSubRegionSection(boolean hasSubRegionSection) {
        this.hasSubRegionSection = hasSubRegionSection;
    }

    public SiteRankViewModel getRegionViewModel() {
        return regionViewModel;
    }

    public void setRegionViewModel(SiteRankViewModel regionViewModel) {
        this.regionViewModel = regionViewModel;
    }

    public boolean hasRegionSection() {
        return hasRegionSection;
    }

    public void setHasRegionSection(boolean hasRegionSection) {
        this.hasRegionSection = hasRegionSection;
    }

    public SiteRankViewModel getSubRegionViewModel() {
        return subRegionViewModel;
    }

    public void setSubRegionViewModel(SiteRankViewModel subRegionViewModel) {
        this.subRegionViewModel = subRegionViewModel;
    }

    public boolean hasWorldWideSection() {
        return hasWorldWideSection;
    }

    public void setHasWorldWideSection(boolean hasWorldWideSection) {
        this.hasWorldWideSection = hasWorldWideSection;
    }

    public SiteRankViewModel getWorldWideViewModel() {
        return worldWideViewModel;
    }

    public void setWorldWideViewModel(SiteRankViewModel worldWideViewModel) {
        this.worldWideViewModel = worldWideViewModel;
    }

    public Integer getWeek() {
        return week;
    }

    public void setWeek(Integer week) {
        this.week = week;
    }

    @Override
    public String toString() {
        return "RankingViewModel{" +
                "showTrophy=" + showTrophy +
                ", trophyMessage='" + trophyMessage + '\'' +
                ", hasSubRegionSection=" + hasSubRegionSection +
                ", regionViewModel=" + regionViewModel +
                ", hasRegionSection=" + hasRegionSection +
                ", subRegionViewModel=" + subRegionViewModel +
                ", hasWorldWideSection=" + hasWorldWideSection +
                ", worldWideViewModel=" + worldWideViewModel +
                '}';
    }

    public void setTopFiveMessage(String top5RankingRegionMessage) {
        this.top5RankingRegionMessage = top5RankingRegionMessage;
    }

    public String getTop5RankingRegionMessage() {
        return top5RankingRegionMessage;
    }

    public void setTopFiveWorld(boolean worldWideTop5) {
        this.topFiveWorld = worldWideTop5;
    }

    public boolean isTopFiveWorld() {
        return topFiveWorld;
    }

    public void setTopFiveRegion(boolean topFiveRegion) {
        this.topFiveRegion = topFiveRegion;
    }

    public boolean isTopFiveRegion() {
        return topFiveRegion;
    }

    public void setTopFiveSubRegion(boolean topFiveSubRegion) {
        this.topFiveSubRegion = topFiveSubRegion;
    }

    public boolean isTopFiveSubRegion() {
        return topFiveSubRegion;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Integer getYear() {
        return year;
    }

    public void setTop10World(boolean top10World) {
        this.top10World = top10World;
    }

    public boolean isTop10World() {
        return top10World;
    }

    public void setTop10Region(boolean top10Region) {
        this.top10Region = top10Region;
    }

    public boolean isTop10Region() {
        return top10Region;
    }

    public void setTop10SubRegion(boolean top10SubRegion) {
        this.top10SubRegion = top10SubRegion;
    }

    public boolean isTop10SubRegion() {
        return top10SubRegion;
    }

    public void setTop100World(boolean top100World) {
        this.top100World = top100World;
    }

    public boolean isTop100World() {
        return top100World;
    }

    public static class SiteRankViewModel implements Serializable {

        private String title;
        private String name;
        private String message;
        private boolean isLowerThird;
        private int actualRank;
        private int total;
        private int offset;
        private int week;
        private int year;
        private Trend trend;
        private RankAreaType rankAreaType;
        private int lowerThirdValue;
        private int score;
        private boolean isTop;
        private boolean isMySiteRank;
        private String countryFlagUrl;
        private boolean incognito;
        private boolean dummyModel;
        private RankBreakdownViewModel rankBreakdownViewModel;
        private boolean isInTop;
        private boolean showLowerThirdLabel;

        public void setTitle(String title) {
            this.title = title;
        }

        public String getTitle() {
            return title;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public boolean isLowerThird() {
            return isLowerThird;
        }

        public void setLowerThird(boolean lowerThird) {
            this.isLowerThird = lowerThird;
        }

        public int getActualRank() {
            return actualRank;
        }

        public void setActualRank(int actualRank) {
            this.actualRank = actualRank;
        }

        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }

        public Trend getTrend() {
            return trend;
        }

        public void setTrend(Trend trend) {
            this.trend = trend;
        }

        public RankAreaType getRankAreaType() {
            return rankAreaType;
        }

        public void setRankAreaType(RankAreaType rankAreaType) {
            this.rankAreaType = rankAreaType;
        }

        public void setWeek(int week) {
            this.week = week;
        }

        public int getWeek() {
            return week;
        }

        public void setYear(int year) {
            this.year = year;
        }

        public int getYear() {
            return year;
        }

        public void setOffset(int offset) {
            this.offset = offset;
        }

        public int getOffset() {
            return offset;
        }

        public void setLowerThirdValue(int lowerThirdValue) {
            this.lowerThirdValue = lowerThirdValue;
        }

        public int getLowerThirdValue() {
            return lowerThirdValue;
        }

        public void setScore(int score) {
            this.score = score;
        }

        public int getScore() {
            return score;
        }

        public void setIncognito(boolean incognito) {
            this.incognito = incognito;
        }

        public String getCountryFlagUrl() {
            return countryFlagUrl;
        }

        public void setCountryFlagUrl(String countryFlagUrl) {
            this.countryFlagUrl = countryFlagUrl;
        }

        public void setMySiteRank(boolean mySiteRank) {
            isMySiteRank = mySiteRank;
        }

        public void setDummyModel(boolean dummyModel) {
            this.dummyModel = dummyModel;
        }

        public boolean isDummyModel() {
            return dummyModel;
        }

        public boolean isIncognito() {
            return incognito;
        }

        public boolean isMySiteRank() {
            return isMySiteRank;
        }

        public void setTop(boolean top) {
            isTop = top;
        }

        public boolean isTop() {
            return isTop;
        }

        public void setRankBreakdownViewModel(RankBreakdownViewModel rankBreakdownViewModel) {
            this.rankBreakdownViewModel = rankBreakdownViewModel;
        }

        public RankBreakdownViewModel getRankBreakdownViewModel() {
            return rankBreakdownViewModel;
        }

        public void setIsInTop(boolean isInTop) {
            this.isInTop = isInTop;
        }

        public boolean isInTop() {
            return isInTop;
        }

        public void setInTop(boolean isInTop) {
            this.isInTop = isInTop;
        }

        public void setShowLowerThirdLabel(boolean showLowerThirdLabel) {
            this.showLowerThirdLabel = showLowerThirdLabel;
        }

        public boolean isShowLowerThirdLabel() {
            return showLowerThirdLabel;
        }
    }

    public enum RankAreaType implements Serializable {

        COUNTRY("COUNTRY", R.string.weekly_printbeat_site_weekly_rank_country),
        SUB_REGION("SUB_REGION", R.string.weekly_printbeat_site_weekly_rank_sub_region),
        REGION("REGION", R.string.weekly_printbeat_site_weekly_rank_region),
        WORLD_WIDE("WORLD", R.string.weekly_printbeat_site_weekly_rank_world),
        UNKNOWN("-", -1);

        private final int titleResourceId;
        private final String key;

        RankAreaType(String key, int titleResourceId) {
            this.titleResourceId = titleResourceId;
            this.key = key;
        }

        public int getTitleResourceId() {
            return titleResourceId;
        }

        public String getKey() {
            return key;
        }

        public static RankAreaType from (String key) {
            if(key == null) {
                return UNKNOWN;
            }
            for (RankAreaType type : RankAreaType.values()) {
                if (type.key.equalsIgnoreCase(key)) {
                    return type;
                }
            }
            return UNKNOWN;
        }
    }

    public enum Trend {

        UP(R.drawable.up, R.drawable.green),
        DOWN(R.drawable.down, R.drawable.red),
        EQUALS(R.drawable.orange, R.drawable.orange);

        private final int directionIconResId;
        private final int backgroundIconResId;

        Trend(int trendIconResId, int backgroundIconResId) {
            this.directionIconResId = trendIconResId;
            this.backgroundIconResId = backgroundIconResId;
        }

        public int getDirectionIconResId() {
            return directionIconResId;
        }

        public int getBackgroundIconResId() {
            return backgroundIconResId;
        }
    }
}
