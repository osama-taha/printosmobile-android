package com.hp.printosmobile.presentation.modules.main;

import android.Manifest;
import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Color;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.util.Pair;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.OrientationEventListener;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.hp.printosmobile.Analytics;
import com.hp.printosmobile.AppseeSdk;
import com.hp.printosmobile.BuildConfig;
import com.hp.printosmobile.Constants;
import com.hp.printosmobile.IntercomSdk;
import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;
import com.hp.printosmobile.aaa.AAAManager;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.data.remote.models.UserInfoData;
import com.hp.printosmobile.data.remote.models.kz.KZItem;
import com.hp.printosmobile.data.remote.services.VersionCheckService;
import com.hp.printosmobile.presentation.BaseActivity;
import com.hp.printosmobile.presentation.BaseFragment;
import com.hp.printosmobile.presentation.Navigator;
import com.hp.printosmobile.presentation.PermissionsManager;
import com.hp.printosmobile.presentation.TargetViewManager;
import com.hp.printosmobile.presentation.indigowidget.PrintOSAppWidgetProvider;
import com.hp.printosmobile.presentation.modules.beatcoins.BeatCoinsPopup;
import com.hp.printosmobile.presentation.modules.beatcoins.BeatCoinsViewModel;
import com.hp.printosmobile.presentation.modules.chinapopup.SwitchProductionServerDialog;
import com.hp.printosmobile.presentation.modules.contacthp.AskAQuestionActivity;
import com.hp.printosmobile.presentation.modules.contacthp.FeedbackActivity;
import com.hp.printosmobile.presentation.modules.contacthp.ReportAProblemActivity;
import com.hp.printosmobile.presentation.modules.demopopup.DemoDialog;
import com.hp.printosmobile.presentation.modules.drawer.BusinessUnitsAdapter;
import com.hp.printosmobile.presentation.modules.drawer.DivisionSwitchActivity;
import com.hp.printosmobile.presentation.modules.drawer.NavigationFragment;
import com.hp.printosmobile.presentation.modules.drawer.OrganizationViewModel;
import com.hp.printosmobile.presentation.modules.eula.EulaPopup;
import com.hp.printosmobile.presentation.modules.filters.DeviceFilterViewModel;
import com.hp.printosmobile.presentation.modules.filters.FilterItem;
import com.hp.printosmobile.presentation.modules.filters.FiltersFragment;
import com.hp.printosmobile.presentation.modules.home.ActiveShiftsViewModel;
import com.hp.printosmobile.presentation.modules.home.HomeDataManager;
import com.hp.printosmobile.presentation.modules.home.HomeFragment;
import com.hp.printosmobile.presentation.modules.home.HomePresenter;
import com.hp.printosmobile.presentation.modules.home.Panel;
import com.hp.printosmobile.presentation.modules.insights.CorrectiveActionsDialog;
import com.hp.printosmobile.presentation.modules.insights.InsightsFragment;
import com.hp.printosmobile.presentation.modules.insights.kz.KZItemDetailsPresenter;
import com.hp.printosmobile.presentation.modules.insights.kz.KZItemDetailsView;
import com.hp.printosmobile.presentation.modules.insights.kz.SearchActivity;
import com.hp.printosmobile.presentation.modules.insights.kz.kzfooter.FavoritesActivity;
import com.hp.printosmobile.presentation.modules.invites.InviteUtils;
import com.hp.printosmobile.presentation.modules.invites.InvitesFragment;
import com.hp.printosmobile.presentation.modules.next10jobs.JobsTodayFragment;
import com.hp.printosmobile.presentation.modules.notificationslist.NotificationListPresenter;
import com.hp.printosmobile.presentation.modules.notificationslist.NotificationsListActivity;
import com.hp.printosmobile.presentation.modules.npspopup.NPSPopup;
import com.hp.printosmobile.presentation.modules.npspopup.NPSPresenter;
import com.hp.printosmobile.presentation.modules.npspopup.NPSView;
import com.hp.printosmobile.presentation.modules.organization.OrganizationDialog;
import com.hp.printosmobile.presentation.modules.personaladvisor.AdviceViewModel;
import com.hp.printosmobile.presentation.modules.personaladvisor.PersonalAdvisorDialog;
import com.hp.printosmobile.presentation.modules.personaladvisor.presenter.PersonalAdvisorFactory;
import com.hp.printosmobile.presentation.modules.rankingpanel.leaderboard.RankingLeaderboardFragment;
import com.hp.printosmobile.presentation.modules.reportkpibreakdown.KpiBreakdownActivity;
import com.hp.printosmobile.presentation.modules.reportkpibreakdown.KpiBreakdownDataManager;
import com.hp.printosmobile.presentation.modules.reportkpibreakdown.KpiBreakdownEnum;
import com.hp.printosmobile.presentation.modules.servicecallpanel.ServiceCallsFragment;
import com.hp.printosmobile.presentation.modules.settings.NotificationsPresenter;
import com.hp.printosmobile.presentation.modules.settings.SettingsActivity;
import com.hp.printosmobile.presentation.modules.settings.SubscriptionEvent;
import com.hp.printosmobile.presentation.modules.settings.SubscriptionEvent.NotificationEventEnum;
import com.hp.printosmobile.presentation.modules.shared.DeepLinkUtils;
import com.hp.printosmobile.presentation.modules.shared.DeviceViewModel;
import com.hp.printosmobile.presentation.modules.today.HistogramBreakdownActivity;
import com.hp.printosmobile.presentation.modules.today.TodayViewModel;
import com.hp.printosmobile.presentation.modules.todayhistogram.HistogramDetailsActivity;
import com.hp.printosmobile.presentation.modules.todayhistogram.TodayHistogramViewModel;
import com.hp.printosmobile.presentation.modules.week.RankingViewModel;
import com.hp.printosmobile.utils.AppUtils;
import com.hp.printosmobile.utils.DialogManager;
import com.hp.printosmobile.utils.HPLocaleUtils;
import com.hp.printosmobile.utils.HPUIUtils;
import com.hp.printosmobilelib.core.communications.remote.APIException;
import com.hp.printosmobilelib.core.communications.remote.models.UserData;
import com.hp.printosmobilelib.core.logging.HPLogger;
import com.hp.printosmobilelib.ui.common.HPFragment;
import com.hp.printosmobilelib.ui.widgets.HPViewPager;
import com.hp.printosmobilelib.ui.widgets.panel.PanelView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.Bind;
import butterknife.OnClick;
import io.intercom.android.sdk.Intercom;
import io.intercom.android.sdk.UnreadConversationCountListener;
import rx.Subscriber;

/**
 * Main dashboard activity responsible for building the bottom tabs and managing the navigation.
 *
 * @author Osama Taha
 */

public class MainActivity extends BaseActivity implements DialogManager.DialogManagerCallbacks, TargetViewManager.TapTargetCallback, EulaPopup.EulaPopupCallback, SwitchProductionServerDialog.SwitchProductionServerDialogCallbacks, DemoDialog.DemoDialogCallback, NPSView, NPSPopup.NPSPopupCallback, MainView, InsightsFragment.InsightsFragmentCallback, JobsTodayFragment.JobsTodayFragmentCallback, TabLayout.OnTabSelectedListener, HomeFragment.HomeFragmentCallBack, NavigationFragment.NavigationFragmentCallbacks, BusinessUnitsAdapter.BusinessUnitsAdapterCallbacks, FiltersFragment.FiltersFragmentCallbacks, KZItemDetailsView, RankingLeaderboardFragment.RankingLeaderboardFragmentCallbacks {

    public static final String TAG = MainActivity.class.getName();

    private static final int SWITCH_DIVISION = 1;
    private static final int SWITCH_LANGUAGE = 2;
    private static final int SETTINGS_REQUEST_CODE = 3;
    private static final int NOTIFICATION_LIST_ACTIVITY_REQUEST_CODE = 4;
    private static final int HISTOGRAM_DETAILS_ACTIVITY_REQUEST_CODE = 5;
    private static final int READ_WRITE_EXTERNAL_STORAGE_PERMISSION = 1;
    private static final int READ_CONTACTS_PERMISSION_REQUEST_CODE = 6;

    private static final String POSITION = "tab_position";

    private static final int ANGLE_TOP_LEFT = 80;
    private static final int ANGLE_BOTTOM_LEFT = 100;
    private static final int ANGLE_BOTTOM_RIGHT = 260;
    private static final int ANGLE_TOP_RIGHT = 280;

    private static final long TAP_TARGET_DELAY = 500;
    private static final long TAB_CHANGE_DELAY = 300;

    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private BroadcastReceiver mNotificationBroadcastReceiver;
    private boolean isReceiverRegistered;
    private static final int DEFAULT_INDEX = 0;

    private List<OrganizationViewModel> organizations;

    private OrientationEventListener orientationEventListener;
    private boolean wasPhoneRotated = false;
    private boolean hasRTSupportedDevices = false;

    private boolean hasDialog;
    private boolean proceedWithDialogs = false;
    private boolean isGettingBusinessUnitsCompleted = false;
    private boolean isEulaDisplayed;

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.toolbar_title)
    TextView toolbarTitle;
    @Bind(R.id.fragment_name)
    TextView toolbarFragmentName;
    @Bind(R.id.toolbar_main_layout)
    View toolbarMainLayout;
    @Bind(R.id.toolbar_back_button)
    View toolbarBackButton;
    @Bind(R.id.toolbar_beta_label)
    View toolbarBetaLabel;
    @Bind(R.id.back_button_layout)
    View backButtonLayout;
    @Bind(R.id.favorites_icon)
    View favoriteButtonLayout;
    @Bind(R.id.sliding_tabs)
    TabLayout tabLayout;
    @Bind(R.id.tab_top_separator)
    View tabSeparator;
    @Bind(R.id.viewpager)
    HPViewPager viewPager;
    @Bind(R.id.drawer_layout)
    DrawerLayout drawer;
    @Bind(R.id.loading_view)
    ProgressBar loadingView;
    @Bind(R.id.filters_fragment_container)
    View filtersContainer;
    @Bind(R.id.toolbar_search_icon)
    View toolbarSearchButton;
    @Bind(R.id.toolbar_filters_icon)
    View toolbarFilterButton;
    @Bind(R.id.notification_icon_layout)
    View notificationIconLayout;
    @Bind(R.id.number_of_notifications_text_view)
    TextView numberOfNotificationsTextView;
    @Bind(R.id.intercepting_layout)
    View interceptingLayout;
    @Bind(R.id.toolbar_menu_icon)
    View toolbarMenuIcon;
    @Bind(R.id.default_menu_layout)
    View defaultMenuLayout;
    @Bind(R.id.custom_menu_layout)
    LinearLayout customMenuLayout;

    ActionBarDrawerToggle drawerToggle;
    private MainPagerAdapter adapter;
    private MainViewPresenter presenter;
    private NPSPresenter npsPresenter;
    private KZItemDetailsPresenter kzItemDetailsPresenter;

    private NavigationFragment navigationFragment;
    private BusinessUnitViewModel businessUnitViewModel;
    private TodayHistogramViewModel todayHistogramViewModel;

    private FiltersFragment filtersFragment;
    private boolean isFilterDisplayed;
    private int googlePlayServicesStatusCode;

    private UnreadConversationCountListener unreadConversationCountListener;
    private boolean isFirstEntry = true;
    private boolean isResumed = false;
    private TabLayout.Tab selectedTab;


    private Object objectToBeShared;
    private InvitesFragment invitesFragment;

    private ShareRedirectBlockingPopup shareRedirectBlockingPopup;
    private boolean displayTargetLayout;
    private UserInfoData userInfoData;
    private boolean hasDevices;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        HPLocaleUtils.configureAppLanguage(PrintOSApplication.getInstance(), Locale.getDefault());

        super.onCreate(savedInstanceState);


        PermissionsManager.getInstance().clear();

        setReturningFromBackgroundValue(true);
        isFilterDisplayed = false;
        hasDialog = false;
        initView();
        checkGooglePlayServicesAvailabilityStatus();

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                //for later use
            }
        };
        mNotificationBroadcastReceiver = new NotificationBroadCastReceiver();
        registerReceivers();

        if (intentHasExtra(Constants.NOTIFICATION_EVENT_KEY)) {
            String notificationType = getIntentStringExtra(Constants.NOTIFICATION_EVENT_KEY);
            IntercomSdk.getInstance(this).sendUserClickNotificationTypeEvent(notificationType);
            Analytics.sendEvent(Analytics.CLICK_NOTIFICATION_ACTION, notificationType);
        }

        drawer.getViewTreeObserver()
                .addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {

                        drawer.getViewTreeObserver().removeOnGlobalLayoutListener(this);

                        requestNeededPermissions();
                    }
                });

        if (BuildConfig.isBetaVersion) {
            notifyBetaTesters();
        }

        displayTargetLayout = true;
    }

    private static synchronized void setReturningFromBackgroundValue(boolean returningFromBackgroundValue) {
        setReturningFromBackground(returningFromBackgroundValue);
    }

    @Override
    public boolean shouldValidateSession() {
        return !isEulaDisplayed;
    }

    @Override
    public boolean isSupportMaintenanceDialog() {
        return true;
    }

    @Override
    public void onPreValidation() {
        super.onPreValidation();
        interceptingLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void onValidationCompleted(UserData.User user) {
        super.onValidationCompleted(user);

        interceptingLayout.setVisibility(View.GONE);

        if (checkEula(user)) {
            return;
        }

        if (isFirstEntry) {
            isFirstEntry = false;
            init();
        } else {
            processFragmentsAfterValidation();
        }

        shouldEnableRealtimePolling();


        if (navigationFragment != null) {
            navigationFragment.refreshProfile();
        }

        AAAManager.initUser();

        if (presenter != null) {
            presenter.getOrganizationMembers(this);
            presenter.getUserData();
        }

        nslookupQuery();
    }

    private void nslookupQuery() {
        new DialogManager.NSLookupTask(this, new DialogManager.NSLookupTask.AsyncResponse() {
            @Override
            public void processFinish(String output) {
                if (!TextUtils.isEmpty(output)) {
                    PrintOSPreferences.getInstance(MainActivity.this).setLocation(output);
                }
            }
        }).execute();
    }

    @Override
    public void validationSkipped() {
        super.validationSkipped();
        shouldEnableRealtimePolling();
    }

    private void init() {
        HPLogger.d(TAG, "MainActivity.init()");
        initPresenter();

        if (googlePlayServicesStatusCode == ConnectionResult.SUCCESS) {
            if (PrintOSPreferences.getInstance(this).isFirstLaunch()) {
                subscribeToAllEvents();
                PrintOSPreferences.getInstance(this).setIsFirstLaunch(false);
            }

            if (!PrintOSPreferences.getInstance(this).isAutoSubscribeToIntraDailyUpdate() &&
                    VersionCheckService.getVersionName().equalsIgnoreCase(PrintOSPreferences
                            .getInstance(this).getAppVersionForAutoSubscribeToIntraDailyTarget())) {

                NotificationsPresenter notificationsPresenter = new NotificationsPresenter();
                SubscriptionEvent subscriptionEvent = new SubscriptionEvent();
                subscriptionEvent.setNotificationEventEnum(NotificationEventEnum.INTRA_DAY_PROGRESS_UPDATE);
                notificationsPresenter.subscribeEvent(subscriptionEvent);
                PrintOSPreferences.getInstance(this).setDidAutoSubscribeToIntraDailyUpdate(true);
            }
        }

        logIntent();
        openNotificationListIfRequested();

        if (presenter == null) {
            presenter = new MainViewPresenter();
            presenter.attachView(this);
        }

        if (kzItemDetailsPresenter == null) {
            kzItemDetailsPresenter = new KZItemDetailsPresenter();
            kzItemDetailsPresenter.attachView(this);
        }

        if (PrintOSPreferences.getInstance(this).isInvitesCoinEnabled()) {
            presenter.checkInviteBeatCoin();
        } else if (intentHasExtra(Constants.IntentExtras.MAIN_ACTIVITY_BEAT_COIN_EXTRA)) {
            presenter.getBeatCoinData(getIntentStringExtra(Constants.IntentExtras.MAIN_ACTIVITY_BEAT_COIN_EXTRA));
        } else {
            proceedWithDialogs = true;
        }

        if (intentHasExtra(Constants.IntentExtras.MAIN_ACTIVITY_IS_HANDLING_LINK) &&
                DeepLinkUtils.getBooleanExtra(this, Constants.IntentExtras.MAIN_ACTIVITY_IS_HANDLING_LINK)) {
            shareRedirectBlockingPopup = ShareRedirectBlockingPopup.getInstance();
            shareRedirectBlockingPopup.show(this.getSupportFragmentManager(), TAG);
        }
    }

    @Override
    public void onValidationError() {
        super.onValidationError();
        interceptingLayout.setVisibility(View.GONE);
        if (intentHasExtra(Constants.IntentExtras.MAIN_ACTIVITY_NOTIFICATION_EXTRA)) {
            getIntent().removeExtra(Constants.IntentExtras.MAIN_ACTIVITY_NOTIFICATION_EXTRA);
        }
        if (intentHasExtra(Constants.IntentExtras.MAIN_ACTIVITY_ORGANIZATION_EXTRA)) {
            getIntent().removeExtra(Constants.IntentExtras.MAIN_ACTIVITY_ORGANIZATION_EXTRA);
        }

        loadingView.setVisibility(View.GONE);
        onError(APIException.create(APIException.Kind.NETWORK), TAG);
    }

    @Override
    public void onValidationUnauthorized() {
        super.onValidationUnauthorized();
        interceptingLayout.setVisibility(View.GONE);
        triggerSignOut();
    }

    @Override
    public void onValidationCompletedWithoutContextChange(UserData.User user) {
        super.onValidationCompletedWithoutContextChange(user);
        interceptingLayout.setVisibility(View.GONE);

        if (checkEula(user)) {
            return;
        }

        if (isFirstEntry) {
            isFirstEntry = false;
            init();
        } else {
            resetActivity();
        }

        if (navigationFragment != null) {
            navigationFragment.refreshProfile();
        }

        if (presenter != null) {
            presenter.getUserData();
        }
    }

    private boolean checkEula(UserData.User user) {
        if (user == null || !user.isNeedToAcceptEula()) {
            return false;
        }

        new Handler().post(new Runnable() {
            @Override
            public void run() {
                DialogManager.displayEulaPopup(MainActivity.this, MainActivity.this);

                hasDialog = true;
                isEulaDisplayed = true;
                enableOrientationEventListener(false);
            }
        });

        return true;
    }


    @Override
    public void onValidationPeriodPassed() {
        super.onValidationPeriodPassed();
        if (adapter != null && adapter.getFragments() != null) {
            for (Fragment fragment : adapter.getFragments()) {
                if (fragment instanceof IMainFragment && ((IMainFragment) fragment).respondToValidationPeriodExceedance()) {
                    if (InviteUtils.getInviteActionSheet() != null) {
                        InviteUtils.getInviteActionSheet().dismiss();
                    }
                    moveToHome();
                    return;
                }
            }
        }
    }

    private void notifyBetaTesters() {
        if (!PrintOSPreferences.getInstance(this).wasBetaTestersNotified()) {
            HPUIUtils.displayToast(this, getString(R.string.beta_version_welcome_message));
            PrintOSPreferences.getInstance(this).setBetaTestersNotified();
        }
    }

    private void checkGooglePlayServicesAvailabilityStatus() {

        googlePlayServicesStatusCode = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(this);
        String status = AppUtils.getGooglePlayServicesAvailabilityStatusMessage(googlePlayServicesStatusCode);

        PrintOSPreferences printOSPreferences = PrintOSPreferences.getInstance(this);

        if (!printOSPreferences.getGooglePlayServiceStatus().equals(status)) {

            Analytics.sendEvent(Analytics.GOOGLE_PLAY_SERVICES_STATUS_ACTION, status);

            //if status changed to SUCCESS, then call the registration service.
            if (googlePlayServicesStatusCode == ConnectionResult.SUCCESS) {
                startRegistrationService(Constants.IntentExtras.NOTIFICATION_REGISTER_INTENT_ACTION);
            }

            printOSPreferences.setGooglePlayServicesStatus(status);

        }
    }

    private void subscribeToAllEvents() {

        NotificationsPresenter notificationsPresenter = new NotificationsPresenter();

        for (NotificationEventEnum event : NotificationEventEnum.values()) {

            if (!event.isSubscribeOnFirstLaunch() || event == NotificationEventEnum.UNKNOWN) {
                continue;
            }

            SubscriptionEvent subscriptionEvent = new SubscriptionEvent();
            subscriptionEvent.setNotificationEventEnum(event);
            notificationsPresenter.subscribeEvent(subscriptionEvent);
        }

    }

    private void logIntent() {

        if (intentHasExtra(Constants.IntentExtras.MAIN_ACTIVITY_NOTIFICATION_ID_EXTRA) &&
                intentHasExtra(Constants.IntentExtras.MAIN_ACTIVITY_USER_ID_EXTRA)) {
            NotificationListPresenter notificationListPresenter = new NotificationListPresenter();
            notificationListPresenter.markNotificationAsRead(getIntentIntExtra(Constants.IntentExtras.MAIN_ACTIVITY_NOTIFICATION_ID_EXTRA),
                    getIntentIntExtra(Constants.IntentExtras.MAIN_ACTIVITY_USER_ID_EXTRA));
        }

        boolean hasNotificationExtra = intentHasExtra(Constants.IntentExtras.MAIN_ACTIVITY_NOTIFICATION_EXTRA);
        String notificationType = null;

        if (hasNotificationExtra) {
            notificationType = getIntentStringExtra(Constants.IntentExtras.MAIN_ACTIVITY_NOTIFICATION_EXTRA);
            HPLogger.d(TAG, "notificationKey: " + notificationType);
        }

        boolean openedFromPushNotification = intentHasExtra(Constants.IntentExtras.MAIN_ACTIVITY_IS_PUSH_NOTIFICATION);

        if (openedFromPushNotification) {

            //Resets app startup time
            PrintOSApplication.setStartTimeMilliSeconds(0, false);

            Analytics.sendEvent(Analytics.APP_OPENED_ACTION, PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).isInDemoMode() ? Analytics.DEMO_MODE_LABEL : Analytics.REAL_USER_LABEL);

            if (hasNotificationExtra) {
                HPLogger.d(TAG, "click push notification " + notificationType);
                Analytics.sendEvent(Analytics.CLICK_NOTIFICATION_ACTION, notificationType);
                Analytics.sendNotificationClickedFirebaseEvent(notificationType);
            } else {
                Analytics.sendEvent(Analytics.EVENT_OPEN_FROM_NOTIFICATION);
            }
        }

        if (intentHasExtra(Constants.IntentExtras.MAIN_ACTIVITY_ORGANIZATION_EXTRA)) {
            HPLogger.d(TAG, "organization: " + getIntentStringExtra(Constants.IntentExtras.MAIN_ACTIVITY_ORGANIZATION_EXTRA));
        }
        if (intentHasExtra(Constants.IntentExtras.MAIN_ACTIVITY_DIVISION_EXTRA)) {
            HPLogger.d(TAG, "division: " + getIntentStringExtra(Constants.IntentExtras.MAIN_ACTIVITY_DIVISION_EXTRA));
        }
        if (intentHasExtra(Constants.IntentExtras.MAIN_ACTIVITY_FILTER_EXTRA)) {
            HPLogger.d(TAG, "filter: " + getIntentStringExtra(Constants.IntentExtras.MAIN_ACTIVITY_FILTER_EXTRA));
        }
        if (intentHasExtra(Constants.IntentExtras.MAIN_ACTIVITY_TODAY_EXTRA)) {
            HPLogger.d(TAG, "today: " + getIntentStringExtra(Constants.IntentExtras.MAIN_ACTIVITY_TODAY_EXTRA));
        }
        if (intentHasExtra(Constants.IntentExtras.MAIN_ACTIVITY_BEAT_COIN_EXTRA)) {
            HPLogger.d(TAG, "beatCoin: " + getIntentStringExtra(Constants.IntentExtras.MAIN_ACTIVITY_BEAT_COIN_EXTRA));
        }

    }

    @Override
    protected void onResume() {

        if (!isReturningFromBackground()) {
            if (navigationFragment != null) {
                navigationFragment.refreshProfile();
            }
        }

        isResumed = true;
        super.onResume();
        Intercom.client().handlePushMessage();
        shouldEnableOrientation();

        registerIntercomListener();

    }

    @Override
    protected void onPause() {
        super.onPause();
        enableOrientationEventListener(false);
        isResumed = false;
        unregisterIntercomListener();
        enableRealtimePolling(null);
    }

    private void registerIntercomListener() {
        if (unreadConversationCountListener == null) {
            unreadConversationCountListener = new UnreadConversationCountListener() {
                @Override
                public void onCountUpdate(int i) {
                    if (navigationFragment != null && navigationFragment.isAdded()) {
                        navigationFragment.updateIntercomBadge();
                    }
                }
            };
        }

        IntercomSdk.getInstance(this).addUnreadConversationCountListener(unreadConversationCountListener);
    }

    private void unregisterIntercomListener() {
        if (unreadConversationCountListener == null) {
            IntercomSdk.getInstance(this).removeUnreadConversationCountListener(unreadConversationCountListener);
        }
    }

    private void openNotificationListIfRequested() {

        if (intentHasExtra(Constants.IntentExtras.MAIN_ACTIVITY_NOTIFICATION_EXTRA)) {

            String notificationType = getIntentStringExtra(Constants.IntentExtras.MAIN_ACTIVITY_NOTIFICATION_EXTRA);
            NotificationEventEnum eventEnum = NotificationEventEnum
                    .from(notificationType);
            if (eventEnum == NotificationEventEnum.BOX_NEW_FOLDER && intentHasExtra(Constants.IntentExtras.MAIN_ACTIVITY_LINK_EXTRA)) {
                String boxNewFolderLink = getIntentStringExtra(Constants.IntentExtras.MAIN_ACTIVITY_LINK_EXTRA);
                AppUtils.startApplication(this, Uri.parse(boxNewFolderLink));
            }

        }
    }

    private void initView() {

        setupToolbar();
        setupDrawer();
        setupViewPager();
        setupScreenMotionDetector();
    }


    void displayDialogsIfNeeded() {
        if (hasDialog || shareRedirectBlockingPopup != null) {
            return;
        }

        if (!PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).isInDemoMode() || !getIntent().hasExtra(Constants.IntentExtras.COMING_FROM_LOGIN)) {

            if (DialogManager.displayDialogs(businessUnitViewModel, this, false)) {
                hasDialog = true;
                enableOrientationEventListener(false);
            }
            shouldEnableOrientation();
        }
    }

    private void initPresenter() {

        HPLogger.d(TAG, "MainActivity.initPresenter()");

        presenter = new MainViewPresenter();
        presenter.attachView(this);

        new Handler().post(new Runnable() {
            @Override
            public void run() {
                //if intent has organization it is being called from notification action
                //stop normal initialization
                OrganizationViewModel selected = PrintOSPreferences.getInstance(MainActivity.this)
                        .getSelectedOrganization();
                String organizationExtra = getIntentStringExtra(Constants.IntentExtras.MAIN_ACTIVITY_ORGANIZATION_EXTRA);
                if (TextUtils.isEmpty(organizationExtra) || (!TextUtils.isEmpty(organizationExtra) &&
                        organizationExtra.equalsIgnoreCase(selected.getOrganizationId()))) {

                    if (intentHasExtra(Constants.IntentExtras.MAIN_ACTIVITY_ORGANIZATION_EXTRA)) {
                        getIntent().removeExtra(Constants.IntentExtras.MAIN_ACTIVITY_ORGANIZATION_EXTRA);
                        if (intentHasExtra(Constants.IntentExtras.MAIN_ACTIVITY_DIVISION_EXTRA)) {
                            BusinessUnitEnum businessUnitEnum = BusinessUnitEnum.from(getIntentStringExtra(
                                    Constants.IntentExtras.MAIN_ACTIVITY_DIVISION_EXTRA
                            ));
                            PrintOSPreferences.getInstance(MainActivity.this).saveSelectedBusinessUnit(businessUnitEnum);
                            if (!PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).isHPOrg() || !PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).isChannelSupportKz()) {
                                getIntent().removeExtra(Constants.IntentExtras.MAIN_ACTIVITY_DIVISION_EXTRA);
                            }
                        }
                        if (presenter.getView() != null) {
                            presenter.initView(MainActivity.this);
                        }
                        return;
                    }

                    getIntent().removeExtra(Constants.IntentExtras.MAIN_ACTIVITY_ORGANIZATION_EXTRA);
                    if (presenter.getView() != null) {
                        presenter.initView(MainActivity.this);
                    }
                } else {
                    PrintOSPreferences.getInstance(MainActivity.this).clearOrganization();

                    OrganizationViewModel organizationViewModel = new OrganizationViewModel();
                    organizationViewModel.setOrganizationId(organizationExtra);
                    navigationFragment.onOrganizationSelected(organizationViewModel);
                }
            }
        });

    }

    private void setupToolbar() {
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(Color.BLACK);

        backButtonLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    public void openIntercomConversationsList() {
        if (PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).isInDemoMode()) {
            HPUIUtils.displayToast(this, getString(R.string.option_not_available_demo_mode));
        } else {
            IntercomSdk.getInstance(this).displayConversationsList();
        }
    }

    private void setupDrawer() {

        drawer = findViewById(R.id.drawer_layout);

        navigationFragment = (NavigationFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_navigation);

        drawerToggle = new ActionBarDrawerToggle(this, drawer, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {

            /**
             * Called when a drawer has settled in a completely open state.
             * */
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                Analytics.sendEvent(Analytics.EVENT_OPEN_MENU);
                if (navigationFragment != null) {
                    navigationFragment.init();
                }
            }

            /**
             *  Called when a drawer has settled in a completely closed state.
             */
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                Analytics.sendEvent(Analytics.EVENT_CLOSE_MENU);
            }
        };

        drawer.addDrawerListener(drawerToggle);
        drawerToggle.syncState();

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(POSITION, tabLayout.getSelectedTabPosition());
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        viewPager.setCurrentItem(savedInstanceState.getInt(POSITION));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == NOTIFICATION_LIST_ACTIVITY_REQUEST_CODE) {
            updateBadge();
            return;
        }

        if (requestCode == HISTOGRAM_DETAILS_ACTIVITY_REQUEST_CODE) {
            wasPhoneRotated = false;
        }

        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case SWITCH_DIVISION:
                    todayHistogramViewModel = null;
                    displayTargetLayout = true;

                    String result = data.getStringExtra(DivisionSwitchActivity.SELECTION_KEY);

                    Analytics.sendEvent(Analytics.SWITCH_EQUIPMENT_ACTION,
                            result);

                    BusinessUnitEnum businessUnit = BusinessUnitEnum.from(result);

                    if (PrintOSApplication.getBusinessUnits() != null && PrintOSApplication.getBusinessUnits().containsKey(businessUnit)) {
                        IntercomSdk.getInstance(this).sendUserChangeEquipmentEvent(businessUnit.getShortName());
                        Analytics.sendEvent(Analytics.EVENT_VIEW_BUSINESS_UNIT, businessUnit.getShortName());
                        onBusinessUnitSelected(PrintOSApplication.getBusinessUnits().get(businessUnit), true);
                    }

                    hideDetailedView();
                    break;
                case SWITCH_LANGUAGE:
                    HomeDataManager.clearApiCache();
                    resetActivity();
                    break;
                case SETTINGS_REQUEST_CODE:

                    if (data.getBooleanExtra(Constants.IntentExtras.SIGN_OUT_REQUESTED, false)) {
                        performLogout();
                    } else if (data.getBooleanExtra(Constants.IntentExtras.ENABLE_RANKING_LEADERBOARD_REQUESTED, false)) {
                        moveToRankingLeaderboard(null);
                    } else {
                        resetActivity();
                    }
                    break;
                default:
                    break;
            }

        }
    }

    public void setDisplayTargetLayoutForTabFragment(TabLayout.Tab tab, boolean now) {
        if (tab == null || adapter == null) {
            return;
        }

        if (adapter.getFragments() != null) {
            for (Fragment mainFragment : adapter.getFragments()) {
                if (mainFragment instanceof BaseFragment) {
                    ((BaseFragment) mainFragment).setDisplayTargetViews(false);
                }
            }
        }
        final Fragment currentFragment = adapter.getFragment(tab.getText().toString());
        if (currentFragment instanceof BaseFragment) {
            ((BaseFragment) currentFragment).setDisplayTargetViews(true);
            if (now) {
                new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (currentFragment != null) {
                            ((BaseFragment) currentFragment).displayTargetViewIfAny();
                        }
                    }
                }, TAP_TARGET_DELAY);
            }
        }
    }

    private void resetActivity() {
        HPLogger.d(TAG, "reset activity");
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    private void hideDetailedView() {
        for (Fragment fragment : adapter.getFragments()) {
            if (fragment instanceof HPFragment) {
                ((HPFragment) fragment).onBackPressed();
            }
        }
    }

    @OnClick(R.id.toolbar_menu_icon)
    public void onNavigationClicked() {

        if (drawer.isDrawerOpen(Gravity.RIGHT)) {
            drawer.closeDrawer(Gravity.RIGHT);
        } else {
            drawer.openDrawer(Gravity.RIGHT);
            if (navigationFragment != null) {
                navigationFragment.init();
            }
        }
    }

    @OnClick(R.id.toolbar_filters_icon)
    public void onFiltersClicked() {
        if (!isFilterDisplayed) {
            presentFilters();
        } else {
            filtersFragment.resetFilters();
            hideFilters();
        }

    }

    @OnClick(R.id.toolbar_search_icon)
    public void onSearchIconClicked() {

        if (businessUnitViewModel == null) {
            moveToInsights();
        } else {
            openSearchActivity(businessUnitViewModel.getBusinessUnit());
        }

    }

    @OnClick(R.id.favorites_icon)
    public void onFavoritesIconClicked() {
        BusinessUnitEnum businessUnit = null;
        Fragment fragment = adapter.getFragment(getString(R.string.fragment_insights_name_key));
        if (fragment instanceof InsightsFragment) {
            businessUnit = ((InsightsFragment) fragment).getBusinessUnitModel();
        }

        if (businessUnit != null) {
            ActivityCompat.startActivity(this, FavoritesActivity.createIntent(this, businessUnit), null);
        }
    }

    private void openSearchActivity(BusinessUnitEnum businessUnit) {

        Window window = getWindow();

        Bundle animationBundle = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            ActivityOptionsCompat options = getActivityOptionsCompat();
            if (options != null) {
                animationBundle = options.toBundle();
            }
            window.setExitTransition(null);
        }


        Analytics.sendEvent(Analytics.CLICK_INSIGHTS_SEARCH, businessUnit.getShortName());
        ActivityCompat.startActivity(this, SearchActivity.createIntent(this, businessUnit), animationBundle);

    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private ActivityOptionsCompat getActivityOptionsCompat() {
        ActivityOptionsCompat options = null;

        Pair<View, String> navigationBarPair = HPUIUtils.getNavigationBarAnimationPair(this);
        Pair<View, String> statusBarPair = HPUIUtils.getStatusBarAnimationPair(this);

        if (navigationBarPair != null) {
            options = ActivityOptionsCompat.makeSceneTransitionAnimation(this, statusBarPair, navigationBarPair);
        }

        return options;
    }

    @OnClick(R.id.notification_icon_layout)
    public void onNotificationIconClicked() {
        Analytics.sendEvent(Analytics.VIEW_SCREEN_EVENT, Analytics.NOTIFICATION_SCREEN_LABEL);

        startActivityForResult(new Intent(this, NotificationsListActivity.class), NOTIFICATION_LIST_ACTIVITY_REQUEST_CODE);
        numberOfNotificationsTextView.setVisibility(View.INVISIBLE);
    }

    private void presentFilters() {
        HPLogger.d(TAG, "Show Filters");
        if (filtersFragment == null) {
            return;
        }

        AppseeSdk.getInstance(this).startScreen(AppseeSdk.SCREEN_FILTER);
        filtersContainer.setVisibility(View.GONE);
        isFilterDisplayed = true;
        filtersFragment.onShowFilter();
        filtersContainer.setVisibility(View.VISIBLE);

        enableOrientationEventListener(false);
        hasDialog = true;

        if (businessUnitViewModel != null) {
            IntercomSdk.getInstance(this).logEvent(IntercomSdk.PBM_VIEW_FILTER_EVENT,
                    businessUnitViewModel.getBusinessUnit());
        }
    }

    private void hideFilters() {
        HPLogger.d(TAG, "hide Filters");
        isFilterDisplayed = false;
        if (filtersFragment != null) {
            filtersFragment.onHideFilter();
        }
    }

    @Override
    public void onFilterHideDone() {
        filtersContainer.setVisibility(View.GONE);
        hasDialog = false;
        shouldEnableOrientation();
    }

    private void setupViewPager() {

        adapter = new MainPagerAdapter(getSupportFragmentManager(), getApplicationContext());

        adapter.addFragment(HomeFragment.newInstance(), getString(R.string.fragment_home_name_key), getString(R.string.fragment_home_name), R.drawable.home_idle, R.drawable.home_selected);
        adapter.addFragment(InsightsFragment.newInstance(), getString(R.string.fragment_insights_name_key), getString(R.string.fragment_insights_name), R.drawable.insights_idle, R.drawable.insights_selected);

        RankingLeaderboardFragment rankingLeaderboardFragment = new RankingLeaderboardFragment();
        rankingLeaderboardFragment.attachListener(this);

        adapter.addFragment(rankingLeaderboardFragment, getString(R.string.fragment_ranking_leaderboard_name_key), getString(R.string.fragment_ranking_leaderboard_name), R.drawable.chat_idle, R.drawable.chat_selected);

        JobsTodayFragment jobsTodayFragment = JobsTodayFragment.newInstance(null, false);
        jobsTodayFragment.attachListener(this);

        adapter.addFragment(jobsTodayFragment, getString(R.string.fragment_jobs_key), getString(R.string.fragment_jobs_name), R.drawable.job_idle, R.drawable.job_selected);

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        viewPager.setScrollingEnabled(false);
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(adapter.getFragments().size());

        tabLayout.setupWithViewPager(viewPager);

        tabLayout.addOnTabSelectedListener(this);

        // Iterate over all tabs and set the custom view
        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            TabLayout.Tab tab = tabLayout.getTabAt(i);
            tab.setCustomView(adapter.getTabView(i, i == DEFAULT_INDEX));
        }

        final int index = adapter.fragmentKeys.indexOf(getString(R.string.fragment_contact_hp_name_key));
        if (index > 0) {
            ((ViewGroup) tabLayout.getChildAt(0)).getChildAt(index).setVisibility(View.GONE);
        }

        tabLayout.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                tabLayout.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                setTabEnabled(getString(R.string.fragment_insights_name_key), false);
                setTabEnabled(getString(R.string.fragment_jobs_key), false);
                setTabEnabled(getString(R.string.fragment_ranking_leaderboard_name_key), false);
            }
        });
    }

    private void setupScreenMotionDetector() {
        orientationEventListener = new OrientationEventListener(this,
                SensorManager.SENSOR_DELAY_NORMAL) {

            @Override
            public void onOrientationChanged(int orientation) {
                if ((orientation > ANGLE_TOP_LEFT && orientation < ANGLE_BOTTOM_LEFT)
                        || (orientation > ANGLE_BOTTOM_RIGHT && orientation < ANGLE_TOP_RIGHT)) {
                    if (hasRTSupportedDevices && wasPhoneRotated) {
                        HPLogger.d(TAG, "onOrientationChanged");
                        enableOrientationEventListener(false);
                        onHistogramClicked(todayHistogramViewModel);
                    }
                } else if (orientation > 0) {
                    wasPhoneRotated = true;
                }
            }
        };

        if (orientationEventListener.canDetectOrientation()) {
            HPLogger.d(TAG, "Can DetectOrientation");
        } else {
            HPLogger.d(TAG, "Can't DetectOrientation");
        }

        enableOrientationEventListener(false);
    }

    @Override
    public void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }


    @Override
    protected int getLayoutResource() {
        return R.layout.activity_main;
    }

    @Override
    public void onBackPressed() {

        DrawerLayout drawerLayout = findViewById(R.id.drawer_layout);
        if (drawerLayout.isDrawerOpen(GravityCompat.END)) {
            drawerLayout.closeDrawer(GravityCompat.END);
        } else {

            if (filtersContainer.getVisibility() == View.VISIBLE) {
                filtersContainer.setVisibility(View.GONE);
                if (filtersFragment != null) {
                    filtersFragment.onCancelFiltersClicked();
                }
                return;
            }

            String tabName = tabLayout.getTabAt(tabLayout.getSelectedTabPosition()).getText().toString();
            int index = adapter.fragmentKeys.indexOf(tabName);
            Fragment fragment = adapter.getItem(index);
            if (fragment instanceof HPFragment && ((HPFragment) fragment).onBackPressed()) {
                shouldEnableOrientation();
                return;
            }

            InviteUtils.reset();
            super.onBackPressed();
        }


    }

    private void updateToolbar(Fragment fragment) {
        if (fragment instanceof HPFragment) {
            toolbarFragmentName.setText(((HPFragment) fragment).getToolbarDisplayName());
            updateBar(fragment);
        }
    }

    private void updateToolbarUsingExtras(Fragment fragment) {
        if (fragment instanceof HPFragment) {
            toolbarFragmentName.setText(getString(
                    ((HPFragment) fragment).getToolbarDisplayName(),
                    ((HPFragment) fragment).getToolbarDisplayNameExtra()));
            updateBar(fragment);
        }
    }

    private void updateBar(Fragment fragment) {

        boolean update = ((HPFragment) fragment).isFragmentNameToBeDisplayed();
        toolbarMainLayout.setVisibility(update ? View.GONE : View.VISIBLE);
        backButtonLayout.setVisibility(update ? View.VISIBLE : View.GONE);
        favoriteButtonLayout.setVisibility(fragment instanceof InsightsFragment && fragment.isVisible() ? View.VISIBLE : View.GONE);

        toolbarFilterButton.setVisibility(((HPFragment) fragment).isFilteringAvailable() && !isSingleDevice(businessUnitViewModel) ?
                View.VISIBLE : View.GONE);

        toolbarBetaLabel.setVisibility(((HPFragment) fragment).isBeta() ? View.VISIBLE : View.GONE);
        toolbarMenuIcon.setVisibility(((HPFragment) fragment).isMenuIconVisible() ? View.VISIBLE : View.GONE);
        notificationIconLayout.setVisibility(((HPFragment) fragment).isNotificationIconVisible() ? View.VISIBLE : View.GONE);
        toolbarSearchButton.setVisibility(((HPFragment) fragment).isSearchAvailable() ? View.VISIBLE : View.GONE);

        enableInsights(fragment, businessUnitViewModel);

        LayoutInflater layoutInflater = getLayoutInflater();
        View customMenu = null;
        if (((HPFragment) fragment).getCustomToolbarMenu() != -1) {
            customMenu = layoutInflater.inflate(((HPFragment) fragment).getCustomToolbarMenu(),
                    toolbar, false);
        }
        customMenuLayout.setVisibility(customMenu == null ? View.GONE : View.VISIBLE);
        defaultMenuLayout.setVisibility(customMenu == null ? View.VISIBLE : View.GONE);
        if (customMenu != null) {
            customMenuLayout.removeAllViews();
            customMenuLayout.addView(customMenu);
            ((HPFragment) fragment).setCustomMenuView(customMenu);
        }
    }

    private void enableInsights(Fragment fragment, BusinessUnitViewModel businessUnitViewModel) {

        if (businessUnitViewModel == null) {
            enableKZSwitchDivisionTab(fragment);
            return;
        }

        boolean isInsightsEnabledForBu = PrintOSPreferences.getInstance(this).isInsightsEnabledForBu(businessUnitViewModel.getBusinessUnit());

        toolbarSearchButton.setVisibility(isInsightsEnabledForBu && fragment instanceof HomeFragment && ((HPFragment) fragment).isSearchAvailable() ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        selectedTab = tab;
        viewPager.setCurrentItem(tab.getPosition());
        setFragmentName();

        Fragment fragment = adapter.getItem(adapter.fragmentKeys.indexOf(tab.getText()));
        if (fragment instanceof InsightsFragment) {
            ((InsightsFragment) fragment).loadScreen();
            if (businessUnitViewModel != null) {
                Analytics.sendEvent(Analytics.CLICK_INSIGHTS_TAB, businessUnitViewModel.getBusinessUnit().getShortName());
            }
        } else if (fragment instanceof JobsTodayFragment) {
            Analytics.sendEvent(Analytics.VIEW_NEXT_10_JOBS_ACTION);
        } else if (fragment instanceof RankingLeaderboardFragment) {
            ((RankingLeaderboardFragment) fragment).loadScreen(businessUnitViewModel);
        }

        setDisplayTargetLayoutForTabFragment(tab, true);

        enableRealtimePolling(fragment);

        hideHistogramDetailsView();

        if (fragment instanceof HomeFragment && PrintOSApplication.getBusinessUnits() != null) {
            shouldEnableOrientation();
        } else {
            enableOrientationEventListener(false);
        }

        hideHistogramDetailsView();
        updateToolbar(fragment);
        onTabSelected(fragment);

        //Activity can call this method from onRestoreInstanceState when you open the app from background.
        //check if the activity is completely loaded before updating the bottom tabs.
        if (businessUnitViewModel == null) {
            adapter.setSelected(BusinessUnitEnum.INDIGO_PRESS, tab, tab.getPosition());
            return;
        }

        adapter.setSelected(businessUnitViewModel.getBusinessUnit(), tab, tab.getPosition());
    }

    private void enableRealtimePolling(Fragment selectedFragment) {
        String homeName = getString(R.string.fragment_home_name_key);
        Fragment homeFragment = adapter.getItem(adapter.fragmentKeys.indexOf(homeName));
        if (homeFragment instanceof HomeFragment) {
            ((HomeFragment) homeFragment).enableDataPolling(selectedFragment instanceof HomeFragment);
        }
    }

    private void shouldEnableRealtimePolling() {
        if (adapter != null && adapter.fragmentKeys != null) {
            String homeTabName = getString(R.string.fragment_home_name_key);
            String selectedTabName = selectedTab == null ? homeTabName :
                    adapter.fragmentKeys.get(selectedTab.getPosition());
            Fragment fragment = adapter.getFragment(homeTabName);
            if (fragment instanceof HomeFragment) {
                ((HomeFragment) fragment).enableDataPolling(isResumed && homeTabName.equals(selectedTabName));
            }
        }
    }

    private void onTabSelected(Fragment fragment) {

        if (businessUnitViewModel != null) {
            String intercomEvent = null;
            String tabName = null;

            if (fragment instanceof HomeFragment) {
                intercomEvent = IntercomSdk.PBM_VIEW_HOME_EVENT;
                tabName = AppseeSdk.SCREEN_HOME;
                if (intentHasExtra(Constants.IntentExtras.MAIN_ACTIVITY_PB_NOTIFICATION_EXTRA)) {
                    ((HomeFragment) fragment).getPBNotificationData((getIntentStringExtra(Constants.IntentExtras.MAIN_ACTIVITY_PB_NOTIFICATION_EXTRA)));
                    getIntent().removeExtra(Constants.IntentExtras.MAIN_ACTIVITY_PB_NOTIFICATION_EXTRA);
                }
                ((HomeFragment) fragment).setWeekPanelFirstAppearance(true);
            }
            if (fragment instanceof JobsTodayFragment) {
                intercomEvent = IntercomSdk.PBM_VIEW_QUEUED_EVENT;
                tabName = AppseeSdk.JOBS;
            } else if (fragment instanceof InsightsFragment) {
                tabName = AppseeSdk.SCREEN_INSIGHTS;
            } else if (fragment instanceof RankingLeaderboardFragment) {
                tabName = AppseeSdk.SCREEN_RANKING_LEADERBOARD;
            }

            if (intercomEvent != null) {
                IntercomSdk.getInstance(this).logEvent(intercomEvent, businessUnitViewModel.getBusinessUnit());
            }

            if (tabName != null) {
                AppseeSdk.getInstance(this).startScreen(tabName);

                Analytics.sendEvent(Analytics.VIEW_SCREEN_EVENT, String.format(
                        Analytics.TAB_SCREEN_LABEL, tabName.replace("_", " ").toLowerCase()));
            }
        }
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

        //Activity can call this method from onRestoreInstanceState when you open the app from background.
        //check if the activity is completely loaded before updating the bottom tabs.
        if (businessUnitViewModel == null) {
            adapter.setUnselected(BusinessUnitEnum.INDIGO_PRESS, tab, tab.getPosition());
            return;
        }

        adapter.setUnselected(businessUnitViewModel.getBusinessUnit(), tab, tab.getPosition());
    }

    private void hideHistogramDetailsView() {
        String tabName = getString(R.string.fragment_home_name_key);
        int index = adapter.fragmentKeys.indexOf(tabName);
        if (index >= 0) {
            Fragment home = adapter.getItem(index);
            if (home instanceof HPFragment) {
                ((HPFragment) home).onBackPressed();
            }
        }
    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {
        Fragment fragment = adapter.getItem(adapter.fragmentKeys.indexOf(tab.getText()));

        if ((fragment instanceof HomeFragment)) {
            hideHistogramDetailsView();
        }

        if (fragment instanceof HomeFragment && PrintOSApplication.getBusinessUnits() != null) {
            shouldEnableOrientation();
        } else {
            enableOrientationEventListener(false);
        }

        updateToolbar(fragment);

        onTabSelected(fragment);

    }

    @Override
    public void onTodayPanelDevicesRetrieved(List<DeviceViewModel> models) {
        Fragment fragment = adapter.getFragment(getString(R.string.fragment_jobs_key));
        if (fragment instanceof JobsTodayFragment) {
            ((JobsTodayFragment) fragment).setViewModels(models);
        }
    }

    @Override
    public void onTodayDataFailure() {
        Fragment fragment = adapter.getFragment(getString(R.string.fragment_jobs_key));
        if (fragment instanceof JobsTodayFragment) {
            ((JobsTodayFragment) fragment).setLoading(false);
            ((JobsTodayFragment) fragment).onError();
        }
    }

    @Override
    public void onHistogramClicked(TodayHistogramViewModel model) {
        Fragment fragment = adapter.getFragment(getString(R.string.fragment_home_name_key));
        TodayViewModel todayViewModel = null;
        ActiveShiftsViewModel activeShiftsViewModel = null;
        boolean isShift = false;
        if (fragment instanceof HomeFragment) {
            todayViewModel = ((HomeFragment) fragment).getTodayViewModel();
            activeShiftsViewModel = ((HomeFragment) fragment).getActiveShiftViewModel();
            isShift = ((HomeFragment) fragment).isShift();
        }

        HistogramDetailsActivity.startActivityForResult(this, model, todayViewModel, activeShiftsViewModel,
                businessUnitViewModel, isShift, HISTOGRAM_DETAILS_ACTIVITY_REQUEST_CODE);
    }

    @Override
    public void onHistogramViewClosed(HPFragment fragment) {
        updateToolbar(fragment);
    }

    @Override
    public void onHistogramDataRetrieved(TodayHistogramViewModel model) {
        this.todayHistogramViewModel = model;
    }

    @Override
    public void onKpiSelected(String kpiName) {

        KpiBreakdownActivity.startActivity(this, kpiName, businessUnitViewModel,
                DeepLinkUtils.getStringExtra(this, Constants.IntentExtras.MAIN_ACTIVITY_PANEL_EXTRA));

        if (kpiName != null && businessUnitViewModel != null && businessUnitViewModel.getBusinessUnit() != null) {
            String action = String.format(Analytics.EVENT_KPI_CLICK, businessUnitViewModel.getBusinessUnit().getShortName()).toLowerCase();
            Analytics.sendEvent(action, kpiName);


            Analytics.sendEvent(Analytics.VIEW_SCREEN_EVENT, Analytics.KPI_BREAKDOWN_SCREEN_LABEL);
        }

    }

    @Override
    public void onKpiSelected(KpiBreakdownEnum kpi) {
        KpiBreakdownActivity.startActivity(this, kpi.getParentKpi().getKey(), businessUnitViewModel,
                kpi.getDeepLinkTag());

        String kpiName = kpi.getParentKpi().getKey();
        if (kpiName != null && businessUnitViewModel != null && businessUnitViewModel.getBusinessUnit() != null) {
            String action = String.format(Analytics.EVENT_KPI_CLICK, businessUnitViewModel.getBusinessUnit().getShortName()).toLowerCase();
            Analytics.sendEvent(action, kpiName);
        }
    }

    @Override
    public void onWeekPanelClicked() {

        KpiBreakdownActivity.startActivity(this, null, businessUnitViewModel,
                DeepLinkUtils.getStringExtra(this, Constants.IntentExtras.MAIN_ACTIVITY_PANEL_EXTRA));

        if (businessUnitViewModel != null && businessUnitViewModel.getBusinessUnit() != null) {
            String action = String.format(Analytics.EVENT_KPI_CLICK, businessUnitViewModel.getBusinessUnit().getShortName()).toLowerCase();
            Analytics.sendEvent(action, Analytics.LABEL_OVERALL_CLICK);
        }
    }

    @Override
    public void onRefresh() {

        for (Fragment fragment : adapter.getFragments()) {
            if (fragment instanceof JobsTodayFragment) {
                ((JobsTodayFragment) fragment).setLoading(true);
            }
        }

        if (businessUnitViewModel != null) {
            onBusinessUnitSelected(businessUnitViewModel, false);
        } else {
            if (presenter == null) {
                presenter = new MainViewPresenter();
                presenter.attachView(this);
            }
            presenter.initView(this);
        }
    }

    @Override
    public void onRankingPanelClicked(String rankingType) {
        if (PrintOSPreferences.getInstance().isRankingLeaderboardEnabled()) {
            moveToRankingLeaderboard(rankingType);
            Analytics.sendEvent(Analytics.RANKING_LEADERBOARD_CLICK_RANK, rankingType);
        }
    }

    public void processFragmentsAfterValidation() {
        onRefresh();
    }

    @Override
    public void reload() {
        navigationFragment.reload();
    }

    @Override
    public void triggerSignOut() {
        performLogout();
    }

    @Override
    public void onIndigoProductionQueueViewClicked(DeviceViewModel.JobType jobType) {
        openJobsTab(jobType);
    }

    @Override
    public void setNumberOfNotifications(int numberOfNotifications) {
        if (!PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).isInDemoMode()) {
            numberOfNotificationsTextView.setText(String.valueOf(numberOfNotifications));
            numberOfNotificationsTextView.setVisibility(numberOfNotifications > 0 ? View.VISIBLE : View.INVISIBLE);
        }
    }

    @Override
    public void onServiceCallClicked(HPFragment fragment) {
        updateToolbar(fragment);
        enableOrientationEventListener(false);

        Analytics.sendEvent(Analytics.VIEW_SCREEN_EVENT, Analytics.SERVICE_CALL_SCREEN_LABEL);
    }

    @Override
    public void onInvitesSubFragmentOpen(HPFragment fragment) {
        updateToolbarUsingExtras(fragment);
        enableOrientationEventListener(false);
    }

    @Override
    public void onDevicesScreenOpened(HPFragment fragment) {
        updateToolbar(fragment);
        enableOrientationEventListener(false);
    }

    @Override
    public void onAdvicePopupDisplayed() {
        onDialogDisplayed();
    }

    @Override
    public void onAdvicePopupDismissed() {
        onDialogDismissed();
    }

    @Override
    public void onDialogDisplayed() {
        hasDialog = true;
        enableOrientationEventListener(false);
    }

    @Override
    public void onDialogDismissed() {
        hasDialog = false;
        shouldEnableOrientation();
    }

    @Override
    public void onDevicesDetailedClicked(HPFragment fragment) {
        updateToolbar(fragment);
    }

    @Override
    public void onDetailsLevelChanged(HPFragment fragment) {
        updateToolbarUsingExtras(fragment);
    }

    @Override
    public void onDevicesDetailedClosed(HPFragment fragment) {
        updateToolbar(fragment);
    }

    @Override
    public void setHasRtDevices(boolean hasRTDevices) {
        this.hasRTSupportedDevices = hasRTDevices;
        shouldEnableOrientation();
    }

    @Override
    public void scrollToAdvice(AdviceViewModel adviceViewModel) {
        String tabName = getString(R.string.fragment_insights_name_key);
        Fragment fragment = adapter.getFragment(tabName);
        if (fragment != null && fragment instanceof InsightsFragment) {
            ((InsightsFragment) fragment).scrollToAdvice(adviceViewModel);
        }

        int index = adapter.fragmentKeys.indexOf(tabName);
        if (index >= 0) {
            tabLayout.getTabAt(index).select();
        }
    }

    @Override
    public void onGoToPersonalAdvisorClicked(AdviceViewModel model) {
        scrollToAdvice(model);
    }

    @Override
    public void onPollingTimerExecute() {

        Analytics.sendEvent(Analytics.AUTO_REFRESH_ACTION);

    }

    @Override
    public void requestContactPermission(InvitesFragment invitesFragment) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            int contactPermissionGrantedCode = ContextCompat.checkSelfPermission(PrintOSApplication.getAppContext(),
                    Manifest.permission.READ_CONTACTS);

            if (contactPermissionGrantedCode != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.READ_CONTACTS},
                        READ_CONTACTS_PERMISSION_REQUEST_CODE);
                this.invitesFragment = invitesFragment;
                return;
            }
        }
        invitesFragment.init(true);
    }

    @Override
    public void onHistogramWebViewClicked(TodayHistogramViewModel model) {
        HistogramBreakdownActivity.startActivity(this, model, DeepLinkUtils.getStringExtra(this,
                Constants.IntentExtras.MAIN_ACTIVITY_PANEL_EXTRA),
                DeepLinkUtils.getStringExtra(this, Constants.IntentExtras.MAIN_ACTIVITY_OBJECT_EXTRA));

        Analytics.sendEvent(Analytics.VIEW_SCREEN_EVENT, Analytics.HISTOGRAM_BREAKDOWN_SCREEN_LABEL);
    }

    @Override
    public void onDeepLinkHandled() {
        if (shareRedirectBlockingPopup != null && getIntentIntExtra(Constants.UNIVERSAL_LINK_SCREEN_KEY) != Constants.UNIVERSAL_LINK_SCREEN_INSIGHTS_ITEM_DETAILS) {
            shareRedirectBlockingPopup.dismissAllowingStateLoss();
            shareRedirectBlockingPopup = null;
        }

        getIntent().removeExtra(Constants.IntentExtras.MAIN_ACTIVITY_ORGANIZATION_EXTRA);
        getIntent().removeExtra(Constants.IntentExtras.MAIN_ACTIVITY_DIVISION_EXTRA);
        getIntent().removeExtra(Constants.IntentExtras.MAIN_ACTIVITY_FILTER_EXTRA);
        getIntent().removeExtra(Constants.IntentExtras.MAIN_ACTIVITY_FILTER_DEVICE_EXTRA);
        getIntent().removeExtra(Constants.UNIVERSAL_LINK_SCREEN_KEY);
        getIntent().removeExtra(Constants.IntentExtras.MAIN_ACTIVITY_PANEL_EXTRA);
        getIntent().removeExtra(Constants.IntentExtras.MAIN_ACTIVITY_OBJECT_EXTRA);
        getIntent().removeExtra(Constants.IntentExtras.MAIN_ACTIVITY_IS_SHIFT_EXTRA);
    }

    @Override
    public void onRankingDataLoaded(RankingViewModel rankingViewModel) {

        String rankingLeaderboardFragmentKey = getString(R.string.fragment_ranking_leaderboard_name_key);
        int tabIndex = adapter.fragmentKeys.indexOf(rankingLeaderboardFragmentKey);
        if (tabIndex > -1) {
            RankingLeaderboardFragment rankingLeaderboardFragment = (RankingLeaderboardFragment) adapter.getFragment(rankingLeaderboardFragmentKey);
            rankingLeaderboardFragment.setRankingViewModel(rankingViewModel);
        }

    }

    private void openJobsTab(DeviceViewModel.JobType jobType) {
        String tabName = getString(R.string.fragment_jobs_key);
        int index = adapter.fragmentKeys.indexOf(tabName);
        if (index >= 0) {
            tabLayout.getTabAt(index).select();
            Fragment fragment = adapter.getFragment(tabName);
            if (fragment instanceof JobsTodayFragment) {
                ((JobsTodayFragment) fragment).focusOnPage(jobType);
            }
        }
    }

    @Override
    public void onBusinessUnitSelected(final BusinessUnitViewModel businessUnitViewModel, boolean resetFilters) {

        if (businessUnitViewModel == null) {
            return;
        }

        //Reset old selected devices.
        if (resetFilters) {
            businessUnitViewModel.getFiltersViewModel().setSelectedDevices(null);
            updateFiltersView(businessUnitViewModel);
        }

        this.businessUnitViewModel = businessUnitViewModel;

        if (adapter.getFragments() != null) {
            for (final Fragment fragment : adapter.getFragments()) {
                new Handler().post(new Runnable() {
                    @Override
                    public void run() {
                        if (fragment instanceof IMainFragment) {
                            IMainFragment mainFragment = (IMainFragment) fragment;
                            mainFragment.onBusinessUnitSelected(MainActivity.this.businessUnitViewModel);
                        }
                    }
                });
            }
        }

        PrintOSPreferences.getInstance(this).saveSelectedBusinessUnit(businessUnitViewModel.getBusinessUnit());

        setFragmentName();

        if (navigationFragment != null) {
            navigationFragment.onBusinessUnitSelected(businessUnitViewModel);
        }

        shouldShowRatingMenuItem();

        boolean indigoSelected = businessUnitViewModel.getBusinessUnit() == BusinessUnitEnum.INDIGO_PRESS;

        //Show jobs in queue tabs for Indigo only, hide in all other divisions.
        setTabEnabled(getString(R.string.fragment_jobs_key), indigoSelected);
        setTabEnabled(getString(R.string.fragment_ranking_leaderboard_name_key), PrintOSPreferences.getInstance().isRankingLeaderboardEnabled() && indigoSelected);

        boolean isInsightsTabEnabled = PrintOSPreferences.getInstance(this).isInsightsEnabledForBu(businessUnitViewModel.getBusinessUnit());

        setTabEnabled(getString(R.string.fragment_insights_name_key), isInsightsTabEnabled);

        adapter.refreshTabs(businessUnitViewModel.getBusinessUnit(), tabLayout);

        if (intentHasExtra(Constants.IntentExtras.MAIN_ACTIVITY_NOTIFICATION_EXTRA)) {
            NotificationEventEnum notificationEventEnum =
                    NotificationEventEnum.from(getIntentStringExtra(
                            Constants.IntentExtras.MAIN_ACTIVITY_NOTIFICATION_EXTRA));

            switch (notificationEventEnum) {
                case PERSONAL_ADVISOR:
                    moveToInsights();
                    scrollInsightsToPanel(Panel.PERSONAL_ADVISOR);
                    break;
                case DAILY_DIGEST:
                    scrollHomeToPanel(Panel.TODAY);
                    break;
                case NEW_PRINT_BEAT_REPORT:
                    scrollHomeToPanel(Panel.PERFORMANCE);
                    break;
                case SERVICE_CALL_UPDATES:
                    openServiceCallsTab();
                    break;
                default:
                    break;
            }

            getIntent().removeExtra(Constants.IntentExtras.MAIN_ACTIVITY_NOTIFICATION_EXTRA);

        }

        if (intentHasExtra(Constants.UNIVERSAL_LINK_SCREEN_KEY)) {
            int screenNumber = getIntentIntExtra(Constants.UNIVERSAL_LINK_SCREEN_KEY);

            boolean isInsightsEnabledForBu = PrintOSPreferences.getInstance(this).isInsightsEnabledForBu(businessUnitViewModel.getBusinessUnit());

            if (screenNumber == Constants.UNIVERSAL_LINK_SCREEN_INSIGHTS) {
                if (businessUnitViewModel.getBusinessUnit() == BusinessUnitEnum.INDIGO_PRESS) {
                    moveToInsights();
                }
                onDeepLinkHandled();
            } else if (screenNumber == Constants.UNIVERSAL_LINK_SCREEN_INSIGHTS_ITEM_DETAILS) {
                if (isInsightsEnabledForBu) {
                    String kzItemId = DeepLinkUtils.getStringExtra(this, Constants.IntentExtras.MAIN_ACTIVITY_OBJECT_EXTRA);
                    handleDeepLinkInsights(kzItemId, businessUnitViewModel.getBusinessUnit());
                } else {
                    onDeepLinkHandled();
                }
            } else if (screenNumber == Constants.UNIVERSAL_LINK_SCREEN_INSIGHTS_SEARCH) {
                if (isInsightsEnabledForBu) {
                    String query = DeepLinkUtils.getStringExtra(this, Constants.IntentExtras.MAIN_ACTIVITY_OBJECT_EXTRA);
                    handleDeepLinkInsightsSearch(query, businessUnitViewModel.getBusinessUnit());
                } else {
                    onDeepLinkHandled();
                }
            }

            //Handling previous deep links
            String screenName = getIntentStringExtra(Constants.UNIVERSAL_LINK_SCREEN_KEY);
            if (screenName != null) {
                if (screenName.equals(Constants.UNIVERSAL_LINK_SCREEN_NOTIFICATIONS)) {
                    onNotificationIconClicked();
                    onDeepLinkHandled();
                } else if (screenName.equals(Constants.UNIVERSAL_LINK_SCREEN_INVITE_SCREEN)) {
                    moveToHome();
                    String tabName = getString(R.string.fragment_home_name_key);
                    Fragment home = adapter.getFragment(tabName);
                    if (home instanceof HomeFragment) {
                        ((HomeFragment) home).respondToDeepLink();
                    }
                    onDeepLinkHandled();
                } else if (screenName.equals(Constants.UNIVERSAL_LINK_SCREEN_INSIGHTS_TAB)) {
                    if (businessUnitViewModel.getBusinessUnit() == BusinessUnitEnum.INDIGO_PRESS) {
                        moveToInsights();
                    }
                    onDeepLinkHandled();
                }
            }
        }

        TabLayout.Tab tab = tabLayout.getTabAt(tabLayout.getSelectedTabPosition());
        Fragment fragment = adapter.getItem(adapter.fragmentKeys.indexOf(tab.getText()));
        onTabSelected(fragment);

        enableInsights(fragment, businessUnitViewModel);

        displayTargetViewIfAny();

        setBottomTabVisibility(false);

    }

    private void shouldShowRatingMenuItem() {
        if (navigationFragment != null) {
            if (npsPresenter == null) {
                npsPresenter = new NPSPresenter();
                npsPresenter.attachView(this);
            }

            navigationFragment.setNPSMenuItemVisibility(false, npsPresenter, businessUnitViewModel);
            if (businessUnitViewModel != null && businessUnitViewModel.getBusinessUnit() == BusinessUnitEnum.INDIGO_PRESS &&
                    !TextUtils.isEmpty(npsPresenter.getEndUserId(businessUnitViewModel))) {
                requestNPSData(false, false);
            }
        }
    }

    private void displayTargetViewIfAny() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (displayTargetLayout) {
                    TabLayout.Tab currentTab = tabLayout.getTabAt(tabLayout.getSelectedTabPosition());
                    final Fragment currentFragment = adapter.getFragment(currentTab.getText().toString());
                    if (currentFragment instanceof BaseFragment) {
                        ((BaseFragment) currentFragment).setDisplayTargetViews(false);
                    }

                    if (toolbar != null) {
                        displayTargetLayout = !TargetViewManager.showTargetView(MainActivity.this, TargetViewManager.TOOLBAR_TAG, toolbar, MainActivity.this);
                    }

                    if (displayTargetLayout) {
                        displayTargetLayout = !TargetViewManager.showTargetViewForTab(MainActivity.this, tabLayout, adapter, MainActivity.this);
                    }

                    if (displayTargetLayout && currentFragment instanceof BaseFragment) {
                        ((BaseFragment) currentFragment).setDisplayTargetViews(true);
                    }
                }
            }
        }, TAP_TARGET_DELAY);
    }

    private void openServiceCallsTab() {
        moveToHome();
        String fragmentName = getString(R.string.fragment_home_name_key);
        Fragment fragment = adapter.getFragment(fragmentName);
        if (fragment != null && fragment instanceof HomeFragment) {
            HomeFragment home = (HomeFragment) fragment;
            home.openServiceCallFragmentWhenLoaded();

        }
    }

    private void scrollHomeToPanel(Panel panel) {
        String fragmentName = getString(R.string.fragment_home_name_key);
        Fragment fragment = adapter.getFragment(fragmentName);
        if (fragment != null && fragment instanceof HomeFragment) {
            HomeFragment home = (HomeFragment) fragment;

            String extra = null;
            if (intentHasExtra(Constants.IntentExtras.MAIN_ACTIVITY_TODAY_EXTRA)) {
                extra = getIntentStringExtra(Constants.IntentExtras.MAIN_ACTIVITY_TODAY_EXTRA);
            }
            home.setFocusablePanel(panel, extra);
        }
    }

    private void scrollInsightsToPanel(Panel panel) {
        String fragmentName = getString(R.string.fragment_insights_name_key);
        Fragment fragment = adapter.getFragment(fragmentName);
        if (fragment != null && fragment instanceof InsightsFragment) {
            ((InsightsFragment) fragment).setFocusablePanel(panel);
        }
    }

    private void updateFiltersView(BusinessUnitViewModel businessUnitViewModel) {

        if (businessUnitViewModel == null) {
            return;
        }

        if (intentHasExtra(Constants.IntentExtras.MAIN_ACTIVITY_FILTER_EXTRA)) {
            String selectedSiteID = getIntentStringExtra(Constants.IntentExtras.MAIN_ACTIVITY_FILTER_EXTRA);
            getIntent().removeExtra(Constants.IntentExtras.MAIN_ACTIVITY_FILTER_EXTRA);

            boolean siteSuccessfullySelected = businessUnitViewModel.getFiltersViewModel().setSelectedSiteById(selectedSiteID);

            if (!siteSuccessfullySelected) {
                HPUIUtils.displayToast(this, getString(R.string.share_redirect_fail));
                onDeepLinkHandled();
            } else {
                PrintOSPreferences.getInstance(this).setSelectedSiteID(businessUnitViewModel.getBusinessUnit(), selectedSiteID);
                if (intentHasExtra(Constants.IntentExtras.MAIN_ACTIVITY_FILTER_DEVICE_EXTRA)) {
                    List<String> devicesList = getIntentStringListExtra(Constants.IntentExtras.MAIN_ACTIVITY_FILTER_DEVICE_EXTRA);

                    List<FilterItem> filterItems = businessUnitViewModel.getFiltersViewModel().getDevices();
                    List<FilterItem> selectedItems = new ArrayList<>();
                    if (filterItems != null) {
                        for (FilterItem item : filterItems) {
                            if (item instanceof DeviceFilterViewModel && devicesList.contains(((DeviceFilterViewModel) item).getSerialNumber())) {
                                item.setSelected(true);
                                selectedItems.add(item);
                            } else {
                                item.setSelected(false);
                            }
                        }
                    }

                    businessUnitViewModel.getFiltersViewModel().setSelectedDevices(selectedItems);

                    getIntent().removeExtra(Constants.IntentExtras.MAIN_ACTIVITY_FILTER_DEVICE_EXTRA);
                }
            }
        } else {
            String selectedSiteID = PrintOSPreferences.getInstance(this).getSelectedSiteID(businessUnitViewModel.getBusinessUnit());
            if (!selectedSiteID.isEmpty()) {
                businessUnitViewModel.getFiltersViewModel().setSelectedSiteById(selectedSiteID);
            } else if (businessUnitViewModel.getFiltersViewModel() != null &&
                    businessUnitViewModel.getFiltersViewModel().getSelectedSite() != null) {
                PrintOSPreferences.getInstance(this).setSelectedSiteID(businessUnitViewModel.getBusinessUnit(),
                        businessUnitViewModel.getFiltersViewModel().getSelectedSite().getId());
            }
        }

        if (filtersFragment == null || businessUnitViewModel != this.businessUnitViewModel) {

            toolbarFilterButton.setVisibility(isSingleDevice(businessUnitViewModel) ? View.GONE : View.VISIBLE);

            filtersFragment = FiltersFragment.newInstance(businessUnitViewModel.getFiltersViewModel(),
                    businessUnitViewModel.getBusinessUnit());
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.filters_fragment_container, filtersFragment)
                    .commit();
            this.filtersContainer.setVisibility(View.GONE);

        } else {

            toolbarFilterButton.setVisibility(isSingleDevice(businessUnitViewModel) ? View.GONE : View.VISIBLE);

        }

    }


    boolean isSingleDevice(BusinessUnitViewModel businessUnitViewModel) {

        return businessUnitViewModel == null || businessUnitViewModel.getFiltersViewModel().getDevices().size() == 1;
    }

    private void setTabEnabled(final String tabName, final boolean enable) {

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                boolean isTabOpen = adapter.fragmentKeys.get(tabLayout.getSelectedTabPosition()).equals(tabName);
                if (isTabOpen && !enable) {
                    moveToHome();
                }

                LinearLayout tabStrip = ((LinearLayout) tabLayout.getChildAt(0));
                for (int i = 0; i < tabStrip.getChildCount(); i++) {
                    if (adapter.fragmentKeys.get(i).equals(tabName)) {
                        tabStrip.getChildAt(i).setVisibility(enable ? View.VISIBLE : View.GONE);
                    }
                }
            }
        }, TAB_CHANGE_DELAY);
    }

    private void setFragmentName() {

        if (businessUnitViewModel != null && businessUnitViewModel.getBusinessUnit().getBusinessUnitDisplayName(false) != null) {

            if (businessUnitViewModel.getFiltersViewModel().getSelectedSite() != null) {
                if (!businessUnitViewModel.getFiltersViewModel().getSelectedSite().isDefaultSite()) {
                    String title = businessUnitViewModel.getFiltersViewModel().getSelectedSite().getName();
                    toolbarTitle.setText(title);
                    toolbarTitle.setVisibility(View.VISIBLE);
                } else {
                    toolbarTitle.setVisibility(View.GONE);
                }
            }

        } else {
            toolbarTitle.setVisibility(View.GONE);
        }

    }

    public void moveToHome() {
        moveToHome(false);
    }

    private void moveToHome(boolean keepInvites) {
        if (adapter == null) {
            return;
        }

        String tabName = getString(R.string.fragment_home_name_key);

        if (keepInvites) {
            String selectedTabName = selectedTab == null ? null : adapter.fragmentKeys.get(selectedTab.getPosition());
            Fragment homeFragment = adapter.getFragment(tabName);
            if ((selectedTabName == null || tabName.equals(selectedTabName)) && homeFragment instanceof HomeFragment &&
                    ((HomeFragment) homeFragment).getSubFragment() instanceof InvitesFragment) {
                return;
            }
        }

        int index = adapter.fragmentKeys.indexOf(tabName);
        if (index >= 0) {
            tabLayout.getTabAt(index).select();
        }
        AppseeSdk.getInstance(this).startScreen(AppseeSdk.SCREEN_HOME);
    }

    public void moveToInsights() {
        String tabName = getString(R.string.fragment_insights_name_key);
        int index = adapter.fragmentKeys.indexOf(tabName);
        if (index >= 0) {
            tabLayout.getTabAt(index).select();
        }
    }

    private void moveToRankingLeaderboard(String rankingType) {

        String tabName = getString(R.string.fragment_ranking_leaderboard_name_key);

        int index = adapter.fragmentKeys.indexOf(tabName);

        if (index >= 0) {
            RankingLeaderboardFragment rankingLeaderboardFragment = (RankingLeaderboardFragment) adapter.getFragment(tabName);
            rankingLeaderboardFragment.setFocusableTab(rankingType);
            tabLayout.getTabAt(index).select();
        }

    }

    @Override
    public void onOrganizationSelected() {
        PrintOSApplication.setStartTimeMilliSeconds(0, false);
        PrintOSPreferences.getInstance(this).setHasIndigoGBU(false);
        loadingView.setVisibility(View.VISIBLE);
        InviteUtils.reset();
        KpiBreakdownDataManager.resetCachedModels();
        HomeDataManager.clearApiCache();
    }

    @Override
    public void onContextChanged(boolean isSuccessful, String selectedOrganizationID) {
        loadingView.setVisibility(View.GONE);

        if (intentHasExtra(Constants.IntentExtras.MAIN_ACTIVITY_ORGANIZATION_EXTRA)) {
            getIntent().removeExtra(Constants.IntentExtras.MAIN_ACTIVITY_ORGANIZATION_EXTRA);

            if (!isSuccessful) {
                onDeepLinkHandled();
                if (shareRedirectBlockingPopup != null) {
                    HPUIUtils.displayToast(this, getString(R.string.share_redirect_fail));
                }
                presenter.initView(MainActivity.this);
            } else {
                String targetDiv = null;
                if (intentHasExtra(Constants.IntentExtras.MAIN_ACTIVITY_DIVISION_EXTRA)) {
                    targetDiv = getIntentStringExtra(Constants.IntentExtras.MAIN_ACTIVITY_DIVISION_EXTRA);
                    BusinessUnitEnum businessUnitEnum = BusinessUnitEnum.from(targetDiv);
                    PrintOSPreferences.getInstance(this).saveSelectedBusinessUnit(businessUnitEnum);
                    if (!PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).isHPOrg()) {
                        getIntent().removeExtra(Constants.IntentExtras.MAIN_ACTIVITY_DIVISION_EXTRA);
                    }
                }
                presenter.initView(MainActivity.this, targetDiv);
            }

            return;
        }

        if (isSuccessful) {

            HPLogger.d(TAG, "Context changed.");
            IntercomSdk.getInstance(this).login();

            if (googlePlayServicesStatusCode == ConnectionResult.SUCCESS) {
                startRegistrationService(Constants.IntentExtras.NOTIFICATION_REGISTER_INTENT_ACTION);
            }

            PermissionsManager.getInstance().clear();
            PrintOSPreferences.getInstance(this).clearChatPreferences(true);

            resetActivity();
        }

        PrintOSAppWidgetProvider.updateAllWidgets(this);

        if (navigationFragment != null) {
            navigationFragment.setOrganization();
        }
    }

    @Override
    public void onTargetDivisionNotFound() {
        if (shareRedirectBlockingPopup != null) {
            HPUIUtils.displayToast(this, getString(R.string.share_redirect_fail));
        }
        onDeepLinkHandled();
    }

    public void onMenuItemSelected() {
        drawer.closeDrawer(Gravity.RIGHT);
    }

    @Override
    public void onContactHPClicked(int id) {

        if (PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).isInDemoMode()) {
            HPUIUtils.displayToast(this, getString(R.string.option_not_available_demo_mode));
        } else {

            String analyticsEvent;
            if (id == R.id.drawer_send_feedback_item) {
                analyticsEvent = Analytics.FEEDBACK_TO_HP_CLICKED_ACTION;
            } else if (id == R.id.drawer_report_problem_item) {
                analyticsEvent = Analytics.REPORT_A_PROBLEM_CLICKED_ACTION;
            } else {
                analyticsEvent = Analytics.ASK_THE_EXPERT_CLICKED_ACTION;
            }

            Analytics.sendEvent(analyticsEvent);

            Intent intent;
            if (id == R.id.drawer_send_feedback_item) {
                intent = new Intent(this, FeedbackActivity.class);
            } else if (id == R.id.drawer_report_problem_item) {
                intent = new Intent(this, ReportAProblemActivity.class);
            } else {
                intent = new Intent(this, AskAQuestionActivity.class);
            }

            startActivity(intent);
        }
    }

    @Override
    public void onChangeOrganizationClicked(List<OrganizationViewModel> organizations, String selectedOrganizationID) {
        if (isFinishing()) {
            return;
        }
        final OrganizationDialog organizationDialog = OrganizationDialog.getInstance(organizations, selectedOrganizationID);
        organizationDialog.addOrganizationDialogCallback(new OrganizationDialog.OrganizationDialogCallback() {
            @Override
            public void onOrganizationsSelected(OrganizationViewModel selectedOrganization) {
                navigationFragment.onOrganizationSelected(selectedOrganization);
                PrintOSPreferences.getInstance(MainActivity.this).resetInviteUserRoleCache();
                PermissionsManager.getInstance().clear();
                Analytics.sendEvent(Analytics.SWITCH_ACCOUNT_ACTION, selectedOrganization.getOrganizationName());
            }

            @Override
            public void onDialogDismissed() {
                hasDialog = false;
                shouldEnableOrientation();
            }
        });
        enableOrientationEventListener(false);
        hasDialog = true;

        onMenuItemSelected();

        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.add(organizationDialog, OrganizationDialog.TAG);
        transaction.commitAllowingStateLoss();
    }

    private void shouldEnableOrientation() {
        if (tabLayout != null && adapter != null && !hasDialog && isResumed) {
            TabLayout.Tab tab = tabLayout.getTabAt(tabLayout.getSelectedTabPosition());
            if (tab != null && adapter.fragmentKeys != null
                    && adapter.fragmentKeys.indexOf(tab.getText()) < adapter.getCount()) {
                Fragment fragment = adapter.getItem(adapter.fragmentKeys.indexOf(tab.getText()));
                if (fragment instanceof HomeFragment && !((HomeFragment) fragment).isDisplayingDetailsFragment()) {
                    enableOrientationEventListener(true);
                }
            }
        }
    }

    @Override
    public void onSettingMenuItemClicked() {

        Analytics.sendEvent(Analytics.VIEW_SCREEN_EVENT, Analytics.SETTINGS_SCREEN_LABEL);

        startActivityForResult(new Intent(this, SettingsActivity.class), SETTINGS_REQUEST_CODE);
    }

    @Override
    public void onTalkWithUsItemClicked() {
        openIntercomConversationsList();
    }

    @Override
    public void onInvitesItemClicked() {

        if (adapter != null) {
            moveToHome();
            Fragment fragment = adapter.getFragment(getString(R.string.fragment_home_name_key));
            if (fragment != null && fragment instanceof HomeFragment) {
                ((HomeFragment) fragment).openInvites();
            }
        }

    }

    @Override
    public void onRateItemClicked() {
        if (userInfoData != null) {
            onNPSUserInfoRetrieved(userInfoData, true);
        } else {
            requestNPSData(true, true);
        }
    }

    @Override
    public void onDivisionSwitchMenuItemClick() {

        List<String> divisions = new ArrayList<>();
        for (BusinessUnitEnum unit : PrintOSApplication.getBusinessUnits().keySet()) {
            divisions.add(unit.getName());
        }

        List<BusinessUnitViewModel> businessUnits = new ArrayList<>(PrintOSApplication.getBusinessUnits().values());

        Bundle bundle = new Bundle();
        bundle.putSerializable(DivisionSwitchActivity.DIVISIONS_KEY, (ArrayList<BusinessUnitViewModel>) businessUnits);

        Intent intent = new Intent(MainActivity.this, DivisionSwitchActivity.class);
        intent.putExtras(bundle);
        startActivityForResult(intent, SWITCH_DIVISION);
    }


    private void performLogout() {

        if (presenter == null) {
            presenter = new MainViewPresenter();
            presenter.attachView(this);
        }

        startRegistrationService(Constants.IntentExtras.NOTIFICATION_UNREGISTER_AND_CLEAR_CACHE_INTENT_ACTION);

        presenter.logout(this);

    }

    public void onMenuItemClicked(int menuItemId) {

        if (tabLayout.getSelectedTabPosition() > 0) {
            TabLayout.Tab tab = tabLayout.getTabAt(tabLayout.getSelectedTabPosition());
            if (tab != null && tab.getText().equals(getString(R.string.fragment_contact_hp_name_key)) && menuItemId != R.id.drawer_ask_question_item && menuItemId != R.id.drawer_send_feedback_item
                    && menuItemId != R.id.drawer_report_problem_item) {
                moveToHome();
            }
        }

    }

    @Override
    public void onOrganizationsRetrieved(List<OrganizationViewModel> organizationViewModels, String selectedOrganizationID) {
        this.organizations = organizationViewModels;
        setIntentOrganizationIfAny();
    }

    private void setIntentOrganizationIfAny() {
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        if (extras != null && extras.containsKey(Constants.IntentExtras.MAIN_ACTIVITY_ORGANIZATION_EXTRA)) {
            String organizationID = extras.getString(Constants.IntentExtras.MAIN_ACTIVITY_ORGANIZATION_EXTRA);
            if (organizationID != null) {
                for (int i = 0; i < (this.organizations == null ? 0 : this.organizations.size()); i++) {
                    if (organizationID.equalsIgnoreCase(organizations.get(i).getOrganizationId())) {
                        PrintOSPreferences.getInstance(this).clearOrganization();
                        navigationFragment.onOrganizationSelected(organizations.get(i));
                        return;
                    }
                }

                //if organization not found then entity is corrupted
                onContextChanged(false, null);
            }
        }
    }

    @Override
    public void onNotificationButtonClicked() {
        onNotificationIconClicked();
    }

    @Override
    public void hideLoading() {
        loadingView.setVisibility(View.GONE);
    }

    @Override
    public void showErrorView() {

    }

    @Override
    public void hideErrorView() {

    }

    @Override
    public void onFinishedGettingAssetData(Object result) {

    }

    @Override
    public void showLoading() {
        loadingView.setVisibility(View.VISIBLE);
    }

    @Override
    public synchronized void onGettingBusinessUnitsCompleted(Map<BusinessUnitEnum, BusinessUnitViewModel> businessUnitViewModelMap) {

        HPLogger.d(TAG, "onGettingBusinessUnitsCompleted");

        isGettingBusinessUnitsCompleted = true;
        hasDevices = true;

        PrintOSApplication.setBusinessUnits(businessUnitViewModelMap);

        BusinessUnitViewModel selectedBusinessUnit;

        if (businessUnitViewModelMap == null || businessUnitViewModelMap.size() == 0) {
            HPLogger.d(TAG, "onGettingBusinessUnitsCompleted: Empty");
            return;
        } else if (businessUnitViewModelMap.size() == 1) {
            HPLogger.d(TAG, "onGettingBusinessUnitsCompleted: one BU");
            selectedBusinessUnit = businessUnitViewModelMap.values().iterator().next();
        } else {
            HPLogger.d(TAG, "onGettingBusinessUnitsCompleted: multiple BU");
            BusinessUnitEnum businessUnitEnum = PrintOSPreferences.getInstance(this).getSelectedBusinessUnit();
            if (businessUnitEnum != BusinessUnitEnum.UNKNOWN) {

                if (businessUnitViewModelMap.containsKey(businessUnitEnum)) {
                    selectedBusinessUnit = businessUnitViewModelMap.get(businessUnitEnum);
                } else {
                    selectedBusinessUnit = businessUnitViewModelMap.get(new ArrayList<>(businessUnitViewModelMap.keySet()).get(0));
                }
            } else {
                selectedBusinessUnit = businessUnitViewModelMap.get(new ArrayList<>(businessUnitViewModelMap.keySet()).get(0));
            }
        }

        if (selectedBusinessUnit != null) {

            onBusinessUnitSelected(selectedBusinessUnit, true);
            shouldEnableRealtimePolling();
            IntercomSdk.getInstance(this).
                    sendUserChangeEquipmentEvent(selectedBusinessUnit.getBusinessUnit().getShortName());
            Analytics.sendEvent(Analytics.EVENT_VIEW_BUSINESS_UNIT, selectedBusinessUnit.getBusinessUnit().getShortName());
        }

        if (proceedWithDialogs) {
            displayDialogsIfNeeded();
        }

        navigationFragment.didFinishGettingBusinessUnits(businessUnitViewModelMap);
    }

    private void onMainError(String msg, boolean shouldTriggerSignOut) {
        onMainError(msg, shouldTriggerSignOut, false);
    }

    private void onMainError(String msg, boolean shouldTriggerSignOut, boolean isHasNoDevices) {

        HPLogger.d(TAG, "on MainActivity Error : " + msg);

        for (Fragment fragment : adapter.getFragments()) {
            if (fragment instanceof IMainFragment) {
                if (fragment instanceof InsightsFragment && (PrintOSPreferences.getInstance(this).isHPOrg() || PrintOSPreferences.getInstance(this).isChannelSupportKz())) {
                    ((InsightsFragment) fragment).loadScreen();
                    continue;
                }
                ((IMainFragment) fragment).onMainError(msg, shouldTriggerSignOut, isHasNoDevices);

            }
        }

        this.businessUnitViewModel = null;
        if (isFilterDisplayed) {
            hideFilters();
        }
        this.filtersFragment = null;
        this.navigationFragment.onMainError(msg, shouldTriggerSignOut, isHasNoDevices);

        moveToHome(true);
        if (!PrintOSPreferences.getInstance(this).isHPOrg() && !PrintOSPreferences.getInstance(this).isChannelOrg()) {
            onDeepLinkHandled();
        }
    }

    @Override
    public void onEmptyBusinessUnits() {

        HPLogger.d(TAG, "Getting empty devices list");
        isGettingBusinessUnitsCompleted = true;
        if (proceedWithDialogs) {
            displayDialogsIfNeeded();
        }
    }

    @Override
    public void onApplicationLogout() {

        unregisterIntercomListener();

        Navigator.openLoginActivity(this);
        finish();
    }

    public void onError(APIException exception, String tag) {
        if (!TAG.equals(tag)) {
            return;
        }

        switch (exception.getKind()) {
            case NETWORK:
                onMainError(getString(R.string.error_network_error), false);
                break;
            case SERVICE_TIME_OUT:
                onMainError(getString(R.string.error_service_time_out), false);
                break;
            case INTERNAL_SERVER_ERROR:
                onMainError(getString(R.string.error_internal_server_error), false);
                break;
            case URL_NOT_FOUND_ERROR:
                onMainError(getString(R.string.url_not_found_error), false);
                break;
            case UNAUTHORIZED:
                onMainError(getString(R.string.unauthorized_error), true);
                break;
            case FILE_NOT_FOUND:
                onMainError(getString(R.string.file_not_found), true);
                break;
            default:
                onMainError(getString(R.string.unexpected_error), false);
                break;
        }
    }

    @Override
    public void onNoDevicesAttached() {

        onMainError(getString(R.string.error_no_devices), false, true);
        if (PrintOSPreferences.getInstance(this).isHPOrg() || PrintOSPreferences.getInstance(this).isChannelSupportKz()) {
            moveToInsights();
            setBottomTabVisibility(true);

            if (getIntentIntExtra(Constants.UNIVERSAL_LINK_SCREEN_KEY) == Constants.UNIVERSAL_LINK_SCREEN_INSIGHTS_ITEM_DETAILS) {
                String businessUnit = getIntentStringExtra(Constants.IntentExtras.MAIN_ACTIVITY_DIVISION_EXTRA);
                String kzItemId = DeepLinkUtils.getStringExtra(this, Constants.IntentExtras.MAIN_ACTIVITY_OBJECT_EXTRA);
                handleDeepLinkInsights(kzItemId, BusinessUnitEnum.from(businessUnit));
            } else if (getIntentIntExtra(Constants.UNIVERSAL_LINK_SCREEN_KEY) == Constants.UNIVERSAL_LINK_SCREEN_LEADERBOARD) {
                onDeepLinkHandled();
            }
        }
        isGettingBusinessUnitsCompleted = true;
        if (proceedWithDialogs) {
            displayDialogsIfNeeded();
        }
    }

    private void setBottomTabVisibility(final boolean noBu) {

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                int visibility;
                if (businessUnitViewModel != null) {
                    visibility = PrintOSPreferences.getInstance(MainActivity.this).isInsightsEnabledForBu(businessUnitViewModel.getBusinessUnit())
                            ? View.VISIBLE : View.GONE;
                    tabLayout.setVisibility(visibility);
                    tabSeparator.setVisibility(visibility);
                } else {
                    visibility = noBu ? View.VISIBLE : View.INVISIBLE;
                    tabLayout.setVisibility(visibility);
                    tabSeparator.setVisibility(visibility);
                }
            }
        }, TAB_CHANGE_DELAY);
    }

    private void handleDeepLinkInsights(String kzItemId, final BusinessUnitEnum businessUnitEnum) {
        kzItemDetailsPresenter.getKZItemById(kzItemId, businessUnitEnum).subscribe(new Subscriber<KZItem>() {
            @Override
            public void onCompleted() {
                getIntent().removeExtra(Constants.UNIVERSAL_LINK_SCREEN_KEY);
                onDeepLinkHandled();
            }

            @Override
            public void onError(Throwable throwable) {
                getIntent().removeExtra(Constants.UNIVERSAL_LINK_SCREEN_KEY);
                onDeepLinkHandled();
                HPUIUtils.displayToast(PrintOSApplication.getAppContext(), getString(R.string.deep_link_insights_kz_document_failure));
            }

            @Override
            public void onNext(KZItem kzItem) {
                if (kzItem != null) {

                    Navigator.openKZDetailsActivity(MainActivity.this, kzItem, businessUnitEnum);

                } else {
                    HPUIUtils.displayToast(PrintOSApplication.getAppContext(), getString(R.string.deep_link_insights_kz_document_failure));
                }
            }
        });
    }

    private void handleDeepLinkInsightsSearch(String query, final BusinessUnitEnum businessUnitEnum) {
        startActivity(SearchActivity.createIntent(this, businessUnitEnum, query));
        onDeepLinkHandled();
    }

    private void enableKZSwitchDivisionTab(Fragment fragment) {

        boolean isKZSwitchDivisionTabEnabled = PrintOSPreferences.getInstance(this).isHPOrg()
                || PrintOSPreferences.getInstance(this).isChannelSupportKz() || hasDevices;

        setTabEnabled(getString(R.string.fragment_insights_name_key), isKZSwitchDivisionTabEnabled);

        HPUIUtils.setVisibility(isKZSwitchDivisionTabEnabled && fragment instanceof HomeFragment && ((HPFragment) fragment).isSearchAvailable(), toolbarSearchButton);

    }

    @Override
    public synchronized void onBeatCoinDataRetrieved(BeatCoinsViewModel beatCoinsViewModel, boolean isInvite) {
        if (isFinishing()) {
            return;
        }

        if (beatCoinsViewModel == null) {
            if (isInvite) {
                if (intentHasExtra(Constants.IntentExtras.MAIN_ACTIVITY_BEAT_COIN_EXTRA)) {
                    if (presenter == null) {
                        presenter = new MainViewPresenter();
                        presenter.attachView(this);
                    }
                    presenter.getBeatCoinData(getIntentStringExtra(Constants.IntentExtras.MAIN_ACTIVITY_BEAT_COIN_EXTRA));
                } else {
                    proceedWithDialogs = true;
                    if (isGettingBusinessUnitsCompleted) {
                        DialogManager.displayDialogs(businessUnitViewModel, this, false);
                    }
                }
            } else {
                getIntent().removeExtra(Constants.IntentExtras.MAIN_ACTIVITY_BEAT_COIN_EXTRA);
                proceedWithDialogs = true;
                if (isGettingBusinessUnitsCompleted) {
                    DialogManager.displayDialogs(businessUnitViewModel, this, false);
                }
            }

            return;
        }

        hasDialog = true;

        if (!PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).isInDemoMode()) {

            Analytics.sendEvent(Analytics.SHOW_BEAT_COINS_ACTION);

            HPLogger.d(TAG, "show Beat coins popup.");

            BeatCoinsPopup beatCoinsPopup = BeatCoinsPopup.getInstance(new BeatCoinsPopup.BeatCoinPopupCallback() {
                @Override
                public void onDialogDismissed() {
                    hasDialog = false;
                    shouldEnableOrientation();
                }
            }, beatCoinsViewModel);

            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            transaction.add(beatCoinsPopup, BeatCoinsPopup.TAG);
            transaction.commit();
        }
    }

    @Override
    public void onUserEmailRetrieved(String email) {
        if (navigationFragment != null) {
            navigationFragment.setUserEmail(email);
        }
    }

    @Override
    public void onCancelFiltersSelected() {

        hideFilters();
    }

    @Override
    public void onApplyFiltersSelected(List<FilterItem> selectedDevices) {

        hideFilters();

        if (selectedDevices == null || businessUnitViewModel == null ||
                businessUnitViewModel.getFiltersViewModel() == null) {
            return;
        }

        this.businessUnitViewModel.getFiltersViewModel().setSelectedDevices(selectedDevices);

        setFragmentName();

        for (Fragment fragment : adapter.getFragments()) {
            if (fragment instanceof IMainFragment) {
                IMainFragment mainFragment = (IMainFragment) fragment;
                mainFragment.onFiltersChanged(businessUnitViewModel);
            }
        }

        shouldShowRatingMenuItem();

        PersonalAdvisorFactory.getInstance().loadData(businessUnitViewModel);
    }

    @Override
    public void requestFilterHiding() {
        hideFilters();
    }

    @Override
    public void onInitializeJobsView(JobsTodayFragment fragment) {

    }

    @Override
    public void onJobsTodayBackPressed() {
        moveToHome();
    }

    @Override
    public void onTryAgainClicked() {
        String tabName = getString(R.string.fragment_home_name_key);
        Fragment fragment = adapter.getFragment(tabName);
        if (fragment instanceof HomeFragment) {
            validate(false, true);
        }

    }

    @Override
    public void onInsightsFragmentBackPress() {
        moveToHome();
    }

    @Override
    public void onInsightsBusinessUnitSet() {

        if (selectedTab != null && adapter != null && adapter.fragmentKeys != null) {

            String insightsTabName = getString(R.string.fragment_insights_name_key);
            String selectedTabName = adapter.fragmentKeys.get(selectedTab.getPosition());

            if (insightsTabName.equals(selectedTabName)) {
                ((InsightsFragment) adapter.fragmentHashMap.get(insightsTabName)).loadScreen();
            }
        }
    }

    @Override
    public void onKZBusinessUnitSelected(BusinessUnitEnum businessUnit) {
        openSearchActivity(businessUnit);
    }

    @Override
    public void moveToHomeFromTab(String tabKey) {
        if (tabKey != null && selectedTab != null && adapter != null && adapter.fragmentKeys != null) {
            String selectedTabName = adapter.fragmentKeys.get(selectedTab.getPosition());
            if (tabKey.equals(selectedTabName)) {
                moveToHome();
            }
        }
    }

    @Override
    public void onEulaDialogDismissed() {
        hasDialog = false;
        isEulaDisplayed = false;
        shouldEnableOrientation();
        validate(false, true);
    }

    @Override
    public void onEulaRejected() {
        triggerSignOut();
    }

    @Override
    public void onEndDemo() {
        performLogout();
    }

    @Override
    public void onSwitchServer() {
        hasDialog = false;
        shouldEnableOrientation();
        resetActivity();

    }

    @Override
    public void onDismissSwitchServerPopup() {
        hasDialog = false;
        shouldEnableOrientation();
    }

    @Override
    public void onTapTargetClick(TargetViewManager.TargetView targetView) {
        if (targetView == TargetViewManager.TargetView.INSIGHTS_TAB) {
            moveToInsights();
        }
    }

    @Override
    public void onCancel() {

    }

    @Override
    public void onRankingLeaderboardFragmentBackPressed() {
        moveToHome();
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    @Override
    public void onRankingPopupAgreeSelected() {

        if (PrintOSPreferences.getInstance().isRankingLeaderboardEnabled()) {
            moveToRankingLeaderboard(null);
        }
    }

    /**
     * An adapter responsible for populating the pager dynamically.
     *
     * @author Osama Taha
     */
    public static class MainPagerAdapter extends FragmentStatePagerAdapter {

        private final Map<String, Fragment> fragmentHashMap = new HashMap<>();
        private final List<String> fragmentKeys = new ArrayList<>();
        private final List<String> fragmentTitles = new ArrayList<>();
        private final List<Integer> fragmentIcons = new ArrayList<>();
        private final List<Integer> fragmentIconsSelected = new ArrayList<>();
        private final Context context;

        public MainPagerAdapter(FragmentManager fm, Context context) {
            super(fm);
            this.context = context;
        }

        public void addFragment(Fragment fragment, String key, String title, Integer iconResourceId, Integer iconResourceIdSelected) {
            fragmentHashMap.put(key, fragment);
            fragmentKeys.add(key);
            fragmentTitles.add(title);
            fragmentIcons.add(iconResourceId);
            fragmentIconsSelected.add(iconResourceIdSelected);
        }

        public List<String> getFragmentKeys() {
            return fragmentKeys;
        }

        @Override
        public Fragment getItem(int position) {
            return fragmentHashMap.get(fragmentKeys.get(position));
        }

        public Fragment getItemByName(String keyName) {
            if (fragmentHashMap.containsKey(keyName)) {
                return fragmentHashMap.get(keyName);
            }
            return null;
        }

        @Override
        public boolean isViewFromObject(View view, Object fragment) {
            return ((Fragment) fragment).getView() == view;
        }

        /**
         * Here we can safely save a reference to the created
         * Fragment, no matter where it came from (either getItem() or
         * FragmentManager).
         */
        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            Fragment fragment = (Fragment) super.instantiateItem(container, position);

            String name = fragmentKeys.get(position);
            if (fragmentHashMap.containsKey(name)) {
                fragmentHashMap.remove(name);
            }
            fragmentHashMap.put(name, fragment);
            // save the appropriate reference depending on position
            return fragment;
        }

        @Override
        public int getCount() {
            return fragmentKeys.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return fragmentKeys.get(position);
        }

        public List<Fragment> getFragments() {
            return new ArrayList<>(fragmentHashMap.values());
        }

        public Fragment getFragment(String name) {
            if (name != null && fragmentHashMap.containsKey(name)) {
                return fragmentHashMap.get(name);
            }
            return null;
        }

        public View getTabView(int position, boolean isSelected) {
            // Given you have a custom layout in `res/layout/custom_tab.xml` an ImageView
            View view = LayoutInflater.from(context).inflate(R.layout.custom_tab, null);

            ImageView img = view.findViewById(R.id.tab_icon);
            if (isSelected) {
                img.setImageResource(fragmentIconsSelected.get(position));
            } else {
                img.setImageResource(fragmentIcons.get(position));
            }

            TextView tabNameText = view.findViewById(R.id.tab_name);
            tabNameText.setText(fragmentTitles.get(position));

            ImageView newBadge = view.findViewById(R.id.new_badge);
            TextView badgeCountText = view.findViewById(R.id.badge_count);

            newBadge.setVisibility(View.GONE);
            badgeCountText.setVisibility(View.GONE);

            if (isInsightTab(position) && VersionCheckService.getVersionName().equals(PrintOSPreferences.getInstance(context)
                    .getInsightsFirstOccuringVersion(VersionCheckService.getVersionName())) && PrintOSPreferences.getInstance(context).shouldShowInsightsNewBadge(true)) {
                newBadge.setVisibility(View.VISIBLE);
            }

            return view;
        }

        private boolean isInsightTab(int position) {
            String fragmentName = fragmentKeys.get(position);
            fragmentName = fragmentName == null ? "" : fragmentName;
            return context.getString(R.string.fragment_insights_name_key).equalsIgnoreCase(fragmentName);
        }

        public void setSelected(BusinessUnitEnum businessUnitEnum, TabLayout.Tab tab, int position) {

            View view = tab.getCustomView();
            ImageView tabIcon = view.findViewById(R.id.tab_icon);
            TextView tabName = view.findViewById(R.id.tab_name);
            tabIcon.setImageResource(businessUnitEnum.getBottomTabIcon(true, position));
            tabName.setSelected(true);
            if (isInsightTab(position)) {
                PrintOSPreferences.getInstance(context).setShowInsightsNewBadge(false);
                ImageView newBadge = view.findViewById(R.id.new_badge);
                newBadge.setVisibility(View.GONE);
            }
        }

        public void setUnselected(BusinessUnitEnum businessUnitEnum, TabLayout.Tab tab, int position) {

            View view = tab.getCustomView();
            ImageView tabIcon = view.findViewById(R.id.tab_icon);
            TextView tabName = view.findViewById(R.id.tab_name);
            tabIcon.setImageResource(businessUnitEnum.getBottomTabIcon(false, position));
            tabName.setSelected(false);
        }

        public void refreshTabs(BusinessUnitEnum businessUnitEnum, TabLayout tabLayout) {

            for (int tabIndex = 0; tabIndex < tabLayout.getTabCount(); tabIndex++) {
                if (tabIndex == tabLayout.getSelectedTabPosition()) {
                    setSelected(businessUnitEnum, tabLayout.getTabAt(tabIndex), tabIndex);
                } else {
                    setUnselected(businessUnitEnum, tabLayout.getTabAt(tabIndex), tabIndex);
                }
            }
        }

    }

    private void registerReceivers() {

        if (!isReceiverRegistered) {

            LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                    new IntentFilter(Constants.IntentExtras.NOTIFICATION_REGISTRATION_COMPLETED_INTENT_ACTION));

            registerReceiver(mNotificationBroadcastReceiver
                    , new IntentFilter(Constants.IntentExtras.NOTIFICATION_RECEIVED_ACTION));

            isReceiverRegistered = true;
        }
    }

    private void unregisterReceivers() {

        try {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);

            unregisterReceiver(mNotificationBroadcastReceiver);
        } catch (Exception e) {
            HPLogger.e(TAG, "Error in unregister receivers ", e);
            //Try/catch to avoid any unexpected issue while un registering the receivers.
        }


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (presenter != null) {
            presenter.detachView();
        }

        if (npsPresenter != null) {
            npsPresenter.detachView();
        }

        if (kzItemDetailsPresenter != null) {
            kzItemDetailsPresenter.detachView();
        }

        unregisterReceivers();
        enableOrientationEventListener(false);
    }

    private boolean intentHasExtra(String key) {
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        return bundle != null && key != null && bundle.containsKey(key);
    }

    private String getIntentStringExtra(String key) {
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        if (bundle != null && key != null && bundle.containsKey(key)) {
            return bundle.getString(key);
        }
        return "";
    }

    private List<String> getIntentStringListExtra(String key) {
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        if (bundle != null && key != null && bundle.containsKey(key)) {
            return (List<String>) bundle.getSerializable(key);
        }
        return new ArrayList<>();
    }

    private int getIntentIntExtra(String key) {
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        if (bundle != null && key != null && bundle.containsKey(key)) {
            return bundle.getInt(key);
        }
        return -1;
    }

    private void updateBadge() {
        if (adapter != null) {
            String fragmentName = getString(R.string.fragment_home_name_key);
            Fragment fragment = adapter.getFragment(fragmentName);
            if (fragment != null && fragment instanceof HomeFragment) {
                HomeFragment home = (HomeFragment) fragment;
                home.updateNotificationBadge();
            }
        }
    }

    private void enableOrientationEventListener(boolean enable) {
        if (orientationEventListener != null) {
            if (businessUnitViewModel != null && businessUnitViewModel.getBusinessUnit().isHasHistogram()) {
                HPLogger.d(TAG, "enableOrientationEventListener(" + enable + ")");
                if (enable) {
                    orientationEventListener.enable();
                } else {
                    orientationEventListener.disable();
                }
            } else {
                HPLogger.d(TAG, "enableOrientationEventListener(false)");
                orientationEventListener.disable();
            }
        }
    }

    public void share(Object objectToBeShared) {
        this.objectToBeShared = objectToBeShared;
        if (this.objectToBeShared == null) {
            return;
        }

        if (permissionsRequested()) {
            performSharing(this.objectToBeShared);
        }
    }

    private boolean permissionsRequested() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            int writePermissionGrantedCode = ContextCompat.checkSelfPermission(PrintOSApplication.getAppContext(),
                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE);

            int readPermissionGrantedCode = ContextCompat.checkSelfPermission(PrintOSApplication.getAppContext(),
                    Manifest.permission.READ_EXTERNAL_STORAGE);

            if (readPermissionGrantedCode != PackageManager.PERMISSION_GRANTED ||
                    writePermissionGrantedCode != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.READ_EXTERNAL_STORAGE},
                        READ_WRITE_EXTERNAL_STORAGE_PERMISSION);
                return false;
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case READ_WRITE_EXTERNAL_STORAGE_PERMISSION:
                if (grantResults.length > 0) {
                    boolean allGranted = true;
                    for (int i = 0; i < grantResults.length; i++) {
                        allGranted = allGranted && grantResults[i] == PackageManager.PERMISSION_GRANTED;
                    }
                    if (allGranted) {
                        performSharing(this.objectToBeShared);
                    }
                }
                return;

            case READ_CONTACTS_PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0) {
                    boolean allGranted = grantResults.length > 0;
                    for (int i = 0; i < grantResults.length; i++) {
                        allGranted = allGranted && grantResults[i] == PackageManager.PERMISSION_GRANTED;
                    }
                    if (invitesFragment != null) {
                        invitesFragment.init(allGranted);
                        invitesFragment = null;
                    }
                    return;
                }
                if (invitesFragment != null) {
                    invitesFragment.init(false);
                    invitesFragment = null;
                }
                return;
            default:
                break;
        }
    }

    public void performSharing(Object objectToBeShared) {
        if (objectToBeShared != null) {
            if (objectToBeShared instanceof PanelView) {
                ((PanelView) objectToBeShared).sharePanel();
            } else if (objectToBeShared instanceof PersonalAdvisorDialog) {
                ((PersonalAdvisorDialog) objectToBeShared).performSharing();
            } else if (objectToBeShared instanceof ServiceCallsFragment) {
                ((ServiceCallsFragment) objectToBeShared).performSharing();
            } else if (objectToBeShared instanceof CorrectiveActionsDialog) {
                ((CorrectiveActionsDialog) objectToBeShared).performSharing();
            }
        }
    }

    public void requestNPSData(boolean isFromMenu, boolean showDialog) {
        if (npsPresenter == null) {
            npsPresenter = new NPSPresenter();
            npsPresenter.attachView(this);
        }

        npsPresenter.requestNPS(businessUnitViewModel, isFromMenu, showDialog);
    }

    @Override
    public void onNPSUserInfoRetrieved(UserInfoData userInfoData, boolean showDialog) {
        this.userInfoData = userInfoData;
        if (showDialog) {
            DialogManager.displayNPSPopup(this, userInfoData);
        } else {
            if (navigationFragment != null) {
                navigationFragment.setNPSMenuItemVisibility(true, npsPresenter, HomePresenter.getSelectedBusinessUnit());
            }
        }
    }

    @Override
    public void onNPSDataRetrievalFailure(boolean isFromMenu) {
        if (isFromMenu) {
            HPUIUtils.displayToast(this, getString(R.string.notification_nps_not_allowed));
        } else {
            DialogManager.displayDialogs(businessUnitViewModel, this, true);
            onDialogDismissed();
        }
    }

    public class NotificationBroadCastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            updateBadge();
        }
    }
}
