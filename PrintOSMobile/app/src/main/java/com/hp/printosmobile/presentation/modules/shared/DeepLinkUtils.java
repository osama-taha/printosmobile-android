package com.hp.printosmobile.presentation.modules.shared;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;

import com.hp.printosmobile.Constants;
import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.presentation.modules.home.HomePresenter;
import com.hp.printosmobile.presentation.modules.home.Panel;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitEnum;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitViewModel;
import com.hp.printosmobile.presentation.modules.rankingpanel.RankingPanel;
import com.hp.printosmobile.presentation.modules.reportkpibreakdown.KpiBreakdownPanel;
import com.hp.printosmobile.presentation.modules.servicecallpanel.ServiceCallPanel;
import com.hp.printosmobile.presentation.modules.today.HistogramPanel;
import com.hp.printosmobile.presentation.modules.today.TodayPanel;
import com.hp.printosmobile.presentation.modules.week.WeekPanel;
import com.hp.printosmobilelib.core.logging.HPLogger;
import com.hp.printosmobilelib.ui.widgets.panel.PanelView;

import java.util.ArrayList;
import java.util.List;

import io.branch.indexing.BranchUniversalObject;
import io.branch.referral.Branch;
import io.branch.referral.BranchError;
import io.branch.referral.util.LinkProperties;

/**
 * Created by Anwar Asbah 2/27/2018
 */
public class DeepLinkUtils {

    private static final String LIST_SEPARATOR = ",";
    private static final String TAG = DeepLinkUtils.class.getSimpleName();

    public static Bundle getBundle(BranchUniversalObject branchUniversalObject) {
        Bundle bundle = new Bundle();

        addStringIfExist(bundle, branchUniversalObject, Constants.UNIVERSAL_LINK_ORGANIZATION_ID_KEY,
                Constants.IntentExtras.MAIN_ACTIVITY_ORGANIZATION_EXTRA);
        addStringIfExist(bundle, branchUniversalObject, Constants.UNIVERSAL_LINK_BUSINESS_UNIT_KEY,
                Constants.IntentExtras.MAIN_ACTIVITY_DIVISION_EXTRA);
        addStringIfExist(bundle, branchUniversalObject, Constants.UNIVERSAL_LINK_SITE_ID_KEY,
                Constants.IntentExtras.MAIN_ACTIVITY_FILTER_EXTRA);


        Integer screen = addIntIfExist(bundle, branchUniversalObject, Constants.UNIVERSAL_LINK_SCREEN_KEY,
                Constants.UNIVERSAL_LINK_SCREEN_KEY);
        if (screen == null) {
            addStringIfExist(bundle, branchUniversalObject, Constants.UNIVERSAL_LINK_SCREEN_KEY,
                    Constants.UNIVERSAL_LINK_SCREEN_KEY);
        }

        addStringIfExist(bundle, branchUniversalObject, Constants.UNIVERSAL_LINK_PANEL_KEY,
                Constants.IntentExtras.MAIN_ACTIVITY_PANEL_EXTRA);
        addStringIfExist(bundle, branchUniversalObject, Constants.UNIVERSAL_LINK_TARGET_OBJECT_KEY,
                Constants.IntentExtras.MAIN_ACTIVITY_OBJECT_EXTRA);
        addBooleanIfExist(bundle, branchUniversalObject, Constants.UNIVERSAL_LINK_IS_SHIFT,
                Constants.IntentExtras.MAIN_ACTIVITY_IS_SHIFT_EXTRA);
        addListIfExist(bundle, branchUniversalObject, Constants.UNIVERSAL_LINK_FILTER_DEVICE_EXTRA,
                Constants.IntentExtras.MAIN_ACTIVITY_FILTER_DEVICE_EXTRA);

        bundle.putBoolean(Constants.IntentExtras.MAIN_ACTIVITY_IS_HANDLING_LINK, true);

        return bundle;
    }

    public static boolean validateLink(BusinessUnitViewModel businessUnitViewModel, String panelDeepLinkTag, Integer screen) {
        if (screen == null || businessUnitViewModel == null || businessUnitViewModel.getPanels() == null) {
            return false;
        }

        PrintOSPreferences preferences = PrintOSPreferences.getInstance(PrintOSApplication.getAppContext());

        switch (screen.intValue()) {
            case Constants.UNIVERSAL_LINK_SCREEN_INSIGHTS:
                return businessUnitViewModel.getBusinessUnit() == BusinessUnitEnum.INDIGO_PRESS;
            case Constants.UNIVERSAL_LINK_SCREEN_SERVICE_CALLS:
                return businessUnitViewModel.getPanels().contains(Panel.PANEL_SERVICE_CALL);
            case Constants.UNIVERSAL_LINK_SCREEN_STATE_DISTRIBUTION:
                return businessUnitViewModel.getBusinessUnit() == BusinessUnitEnum.INDIGO_PRESS &&
                        preferences.isStateDistributionEnabled() &&
                        businessUnitViewModel.getPanels().contains(Panel.TODAY);
            case Constants.UNIVERSAL_LINK_SCREEN_HISTOGRAM_BREAKDOWN:
                return businessUnitViewModel.getBusinessUnit() == BusinessUnitEnum.INDIGO_PRESS &&
                        preferences.isHistogramGraphEnabled() &&
                        businessUnitViewModel.getPanels().contains(Panel.TODAY);
            case Constants.UNIVERSAL_LINK_SCREEN_PAPOPUP:
                return businessUnitViewModel.getPanels().contains(Panel.TODAY);
            case Constants.UNIVERSAL_LINK_SCREEN_KPI_BREAKDOWN:
                return businessUnitViewModel.getPanels().contains(Panel.PERFORMANCE) &&
                        preferences.isKpiBreakdownReportEnabled();
            case Constants.UNIVERSAL_LINK_SCREEN_HOME:
                Panel panel = Panel.fromDeepLinkTag(panelDeepLinkTag);
                return businessUnitViewModel.getPanels().contains(panel);
            case Constants.UNIVERSAL_LINK_SCREEN_LEADERBOARD:
                return businessUnitViewModel.getPanels().contains(Panel.RANKING);
            case Constants.UNIVERSAL_LINK_SCREEN_INSIGHTS_ITEM_DETAILS:
            case Constants.UNIVERSAL_LINK_SCREEN_INSIGHTS_SEARCH:
                return true;
            default:
                return false;
        }
    }

    private static String addStringIfExist(Bundle bundle, BranchUniversalObject branchUniversalObject, String universalKey,
                                           String intentKey) {
        if (branchUniversalObject != null) {
            if (branchUniversalObject.getMetadata().containsKey(universalKey)) {
                String data = branchUniversalObject.getMetadata().get(universalKey);
                bundle.putString(intentKey, data);
                return data;
            }
        }
        return null;
    }

    private static void addBooleanIfExist(Bundle bundle, BranchUniversalObject branchUniversalObject, String universalKey,
                                          String intentKey) {
        try {
            if (branchUniversalObject != null) {
                if (branchUniversalObject.getMetadata().containsKey(universalKey)) {
                    boolean data = Integer.valueOf(branchUniversalObject.getMetadata().get(universalKey)) == 1;
                    bundle.putBoolean(intentKey, data);
                }
            }
        } catch (Exception e) {
            HPLogger.d(TAG, "Error getting adding boolean to MainActivity intent: " + e.getMessage());
        }
    }

    private static Integer addIntIfExist(Bundle bundle, BranchUniversalObject branchUniversalObject, String universalKey,
                                         String intentKey) {
        try {
            if (branchUniversalObject != null) {
                if (branchUniversalObject.getMetadata().containsKey(universalKey)) {
                    Integer data = Integer.valueOf(branchUniversalObject.getMetadata().get(universalKey));
                    if (data != null) {
                        bundle.putInt(intentKey, data.intValue());
                        return data.intValue();
                    }
                }
            }
        } catch (Exception e) {
            HPLogger.d(TAG, "Error adding int extra to MainActivity: " + e.getMessage());
        }
        return null;
    }

    private static void addListIfExist(Bundle bundle, BranchUniversalObject branchUniversalObject, String universalKey,
                                       String intentKey) {
        if (branchUniversalObject != null) {
            if (branchUniversalObject.getMetadata().containsKey(universalKey)) {
                String data = branchUniversalObject.getMetadata().get(universalKey);

                if (!TextUtils.isEmpty(data)) {
                    String[] dataArray = data.split(LIST_SEPARATOR);
                    ArrayList<String> list = new ArrayList<>();
                    for (String item : dataArray) {
                        list.add(item);
                    }

                    bundle.putSerializable(intentKey, list);
                }
            }
        }
    }

    public static String getStringExtra(Activity activity, String key) {
        try {
            if (activity != null && activity.getIntent() != null && key != null) {
                Bundle bundle = activity.getIntent().getExtras();
                if (bundle != null && bundle.containsKey(key)) {
                    return bundle.getString(key);
                }
            }
        } catch (Exception e) {
            HPLogger.d(TAG, "error getting string extra: " + e.getMessage());
        }
        return null;
    }

    public static boolean getBooleanExtra(Activity activity, String key) {
        try {
            if (activity != null && activity.getIntent() != null && key != null) {
                Bundle bundle = activity.getIntent().getExtras();
                if (bundle != null && bundle.containsKey(key)) {
                    return bundle.getBoolean(key);
                }
            }
        } catch (Exception e) {
        }
        return false;
    }

    public static Integer getIntExtra(Activity activity, String key) {
        try {
            if (activity != null && activity.getIntent() != null && key != null) {
                Bundle bundle = activity.getIntent().getExtras();
                if (bundle != null && bundle.containsKey(key)) {
                    return bundle.getInt(key);
                }
            }
        } catch (Exception e) {
        }
        return null;
    }

    public static void removeExtra(Activity activity, String key) {
        if (activity != null && activity.getIntent() != null && key != null) {
            activity.getIntent().removeExtra(key);
        }
    }

    public static synchronized void respondToIntentForPanel(PanelView panelView, DeepLinkCallbacks callbacks, boolean error) {
        if (callbacks != null) {
            Activity hostingActivity = callbacks.getHostingActivity();
            Integer screenTag = getIntExtra(hostingActivity, Constants.UNIVERSAL_LINK_SCREEN_KEY);
            String panelExtra = getStringExtra(hostingActivity, Constants.IntentExtras.MAIN_ACTIVITY_PANEL_EXTRA);

            if (screenTag != null && screenTag > 0) {
                Panel panel = Panel.fromDeepLinkTag(panelView.getPanelTag());

                switch (panel) {
                    case TODAY:
                        if (panelExtra != null && !TextUtils.isEmpty(panelExtra) && panelExtra.equals(panel.getDeepLinkTag()) &&
                                screenTag.equals(Constants.UNIVERSAL_LINK_SCREEN_HOME)) {
                            callbacks.setFocused(panel);
                            callbacks.onDeepLinkHandled();

                        } else if (screenTag.equals(Constants.UNIVERSAL_LINK_SCREEN_STATE_DISTRIBUTION)) {
                            String pressID = getStringExtra(hostingActivity,
                                    Constants.IntentExtras.MAIN_ACTIVITY_OBJECT_EXTRA);
                            if (!error && !TextUtils.isEmpty(pressID) && panelView instanceof TodayPanel) {
                                ((TodayPanel) panelView).openStateDistribution(pressID);
                            }
                            callbacks.onDeepLinkHandled();
                        }
                        break;

                    case PERFORMANCE:
                        if (screenTag.equals(Constants.UNIVERSAL_LINK_SCREEN_HOME) && panelExtra != null &&
                                !TextUtils.isEmpty(panelExtra) && panelExtra.equals(panel.getDeepLinkTag())) {
                            callbacks.setFocused(panel);
                            callbacks.onDeepLinkHandled();
                        } else if (screenTag.equals(Constants.UNIVERSAL_LINK_SCREEN_KPI_BREAKDOWN) &&
                                panelView instanceof WeekPanel) {
                            String kpiName = getStringExtra(hostingActivity, Constants.IntentExtras.MAIN_ACTIVITY_OBJECT_EXTRA);
                            if (!error && kpiName != null) {
                                if (kpiName.equals(KpiBreakdownPanel.OVER_ALL_PERFORMANCE_TAG)) {
                                    ((WeekPanel) panelView).openOverAll();
                                } else {
                                    ((WeekPanel) panelView).openKpiBreakdown(kpiName);
                                }
                            }
                            callbacks.onDeepLinkHandled();
                        }
                        break;

                    case HISTOGRAM:
                        if (screenTag.equals(Constants.UNIVERSAL_LINK_SCREEN_HOME) && panelExtra != null &&
                                !TextUtils.isEmpty(panelExtra) && panelExtra.equals(panel.getDeepLinkTag())) {
                            callbacks.setFocused(panel);
                            callbacks.onDeepLinkHandled();
                        } else if (screenTag.equals(Constants.UNIVERSAL_LINK_SCREEN_HISTOGRAM_BREAKDOWN) &&
                                panelView instanceof HistogramPanel) {
                            if (!error) {
                                ((HistogramPanel) panelView).onHistogramWebViewClicked();
                            }
                            callbacks.onDeepLinkHandled();
                        }
                        break;

                    case RANKING:
                        if (screenTag.equals(Constants.UNIVERSAL_LINK_SCREEN_HOME) && panelExtra != null &&
                                !TextUtils.isEmpty(panelExtra) && panelExtra.equals(panel.getDeepLinkTag())) {
                            callbacks.setFocused(panel);
                            callbacks.onDeepLinkHandled();
                        } else if (screenTag.equals(Constants.UNIVERSAL_LINK_SCREEN_LEADERBOARD) &&
                                panelView instanceof RankingPanel) {
                            if (!error) {
                                ((RankingPanel) panelView).onRankingPanelClicked(panelExtra);
                            }
                            callbacks.onDeepLinkHandled();
                        }
                        break;
                    case INDIGO_PRODUCTION:
                    case LATEX_PRODUCTION:
                        if (screenTag.equals(Constants.UNIVERSAL_LINK_SCREEN_HOME) && panelExtra != null &&
                                !TextUtils.isEmpty(panelExtra) && panelExtra.equals(panel.getDeepLinkTag())) {
                            callbacks.setFocused(panel);
                            callbacks.onDeepLinkHandled();
                        }
                        break;

                    case PANEL_SERVICE_CALL:
                        if (screenTag.equals(Constants.UNIVERSAL_LINK_SCREEN_HOME) && panelExtra != null &&
                                !TextUtils.isEmpty(panelExtra) && panelExtra.equals(panel.getDeepLinkTag())) {
                            callbacks.setFocused(panel);
                            callbacks.onDeepLinkHandled();
                        } else if (screenTag.equals(Constants.UNIVERSAL_LINK_SCREEN_SERVICE_CALLS) &&
                                panelView instanceof ServiceCallPanel) {
                            if (!error) {
                                ((ServiceCallPanel) panelView).onOpenLayoutClicked();
                            }
                            callbacks.onDeepLinkHandled();
                        }
                        break;
                }
            }
        }
    }

    public static void getShareBody(final Context context, int screenTag, String panelTag, Boolean isShift,
                                    String targetObject, boolean isKnowledgeZoneItem, final BranchIOCallback branchIOCallback) {

        PrintOSPreferences preferences = PrintOSPreferences.getInstance(context);
        String organizationID = preferences.getSavedOrganizationId();
        BusinessUnitEnum businessUnitEnum = preferences.getSelectedBusinessUnit();
        String siteID = preferences.getSelectedSiteID(businessUnitEnum);

        BusinessUnitViewModel businessUnitViewModel = HomePresenter.getSelectedBusinessUnit();
        String devString = "";
        if (businessUnitViewModel != null && businessUnitViewModel.getFiltersViewModel() != null &&
                businessUnitViewModel.getFiltersViewModel().getSiteDevicesIds() != null) {

            List<String> devicesID = businessUnitViewModel.getFiltersViewModel().getSerialNumbers();
            for (int i = 0; i < devicesID.size(); i++) {
                devString += devicesID.get(i) + (i == devicesID.size() - 1 ? "" : LIST_SEPARATOR);
            }
        }

        final String baseUrl = context.getString(R.string.share_link);
        BranchUniversalObject buo = new BranchUniversalObject();
        buo.setContentImageUrl(baseUrl);
        LinkProperties lp = new LinkProperties()
                .addControlParameter(Constants.UNIVERSAL_LINK_ORGANIZATION_ID_KEY, organizationID)
                .addControlParameter(Constants.UNIVERSAL_LINK_BUSINESS_UNIT_KEY, businessUnitEnum.getName())
                .addControlParameter(Constants.UNIVERSAL_LINK_SITE_ID_KEY, siteID)
                .addControlParameter(Constants.UNIVERSAL_LINK_FILTER_DEVICE_EXTRA, devString)
                .addControlParameter(Constants.UNIVERSAL_LINK_SCREEN_KEY, String.valueOf(screenTag));

        if (panelTag != null) {
            lp.addControlParameter(Constants.UNIVERSAL_LINK_PANEL_KEY, panelTag);
        }
        if (targetObject != null) {
            lp.addControlParameter(Constants.UNIVERSAL_LINK_TARGET_OBJECT_KEY, targetObject);
        }
        if (isShift != null) {
            lp.addControlParameter(Constants.UNIVERSAL_LINK_IS_SHIFT, isShift ? "1" : "0");
        }

        if (targetObject != null && isKnowledgeZoneItem) {
            lp.addControlParameter(Constants.UNIVERSAL_LINK_DESKTOP_URL, PrintOSPreferences.getInstance(context).getServerUrl() + Constants.DEEP_LINK_APP_NOT_INSTALLED_LINK + targetObject);
        }

        buo.generateShortUrl(context, lp, new Branch.BranchLinkCreateListener() {
            @Override
            public void onLinkCreate(String url, BranchError error) {
                String body = context.getString(R.string.share_body, error == null ? url : baseUrl);
                if (branchIOCallback != null) {
                    branchIOCallback.onLinkCreated(body);
                }
            }
        });
    }

    public static boolean hasExtra(Activity activity, String extra) {
        if (activity == null || activity.getIntent() == null || activity.getIntent().getExtras() == null || extra == null) {
            return false;
        }
        return activity.getIntent().getExtras().containsKey(extra);
    }

    public interface DeepLinkCallbacks {
        Activity getHostingActivity();

        void setFocused(Panel panel);

        void onDeepLinkHandled();
    }

    public interface BranchIOCallback {
        void onLinkCreated(String link);
    }
}
