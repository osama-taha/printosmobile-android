package com.hp.printosmobile.data.remote.models;

import com.fasterxml.jackson.annotation.JsonProperty;


public class AcceptPrivacyVersionRequestBody {

    @JsonProperty("privacyVersion")
    private String privacyVersion;

    @JsonProperty("privacyVersion")
    public String getPrivacyVersion() {
        return privacyVersion;
    }

    @JsonProperty("privacyVersion")
    public void setPrivacyVersion(String privacyVersion) {
        this.privacyVersion = privacyVersion;
    }
}
