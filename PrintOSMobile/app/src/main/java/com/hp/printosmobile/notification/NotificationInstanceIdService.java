package com.hp.printosmobile.notification;

import android.content.Intent;
import android.os.Build;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.hp.printosmobile.Constants;
import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobilelib.core.communications.remote.SessionManager;
import com.hp.printosmobilelib.core.logging.HPLogger;

import io.intercom.android.sdk.push.IntercomPushClient;

/**
 * Created by Anwar Asbah on 1/17/2017.
 */

public class NotificationInstanceIdService extends FirebaseInstanceIdService {

    private static final String TAG = NotificationInstanceIdService.class.getName();

    private final IntercomPushClient intercomPushClient = new IntercomPushClient();

    @Override
    public void onTokenRefresh() {

        if (!PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).isInDemoMode()) {

            String refreshedToken = FirebaseInstanceId.getInstance().getToken();
            HPLogger.d(TAG, "Refreshed token: " + refreshedToken);

            intercomPushClient.sendTokenToIntercom(getApplication(), refreshedToken);

            sendRegistrationToServer(refreshedToken);
        }
    }

    private void sendRegistrationToServer(String refreshedToken) {
        PrintOSPreferences.getInstance(this).setNotificationToken(refreshedToken);

        SessionManager sessionManager = SessionManager.getInstance(PrintOSApplication.getAppContext());

        if (sessionManager.hasCookie()) {

            HPLogger.d(TAG, "Refresh app token");

            try {
                Intent intent = new Intent(this, RegistrationIntentService.class);
                intent.setAction(Constants.IntentExtras.NOTIFICATION_REGISTER_INTENT_ACTION);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    startForegroundService(intent);
                } else {
                    startService(intent);
                }
            } catch (Exception e) {
                HPLogger.e(TAG, "Unable to start the registration intent service " + e);
            }

        }
    }
}
