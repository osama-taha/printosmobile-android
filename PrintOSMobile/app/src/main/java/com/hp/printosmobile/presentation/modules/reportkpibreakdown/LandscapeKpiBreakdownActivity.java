package com.hp.printosmobile.presentation.modules.reportkpibreakdown;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.content.res.ResourcesCompat;
import android.text.Html;
import android.text.SpannableString;
import android.text.TextUtils;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hp.printosmobile.Analytics;
import com.hp.printosmobile.Constants;
import com.hp.printosmobile.OrientationSupportBaseActivity;
import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.data.remote.models.PreferencesData;
import com.hp.printosmobile.presentation.modules.kpiview.KPIValueHandleEnum;
import com.hp.printosmobile.utils.CustomTypefaceSpan;
import com.hp.printosmobilelib.core.logging.HPLogger;
import com.hp.printosmobilelib.ui.utils.SpannableStringUtils;
import com.hp.printosmobilelib.ui.widgets.TypefaceManager;

import butterknife.Bind;
import butterknife.ButterKnife;

import static com.hp.printosmobile.presentation.modules.reportkpibreakdown.KpiBreakdownActivity.NUMBER_OF_WEEKS;

/**
 * created by Anwar Asbah 3/14/2017
 */
public class LandscapeKpiBreakdownActivity extends OrientationSupportBaseActivity {

    private static final String TAG = LandscapeKpiBreakdownActivity.class.getName();

    public static final String VIEW_MODEL_PARAM = "view_model_param";
    private static final String WEB_VIEW_INTERFACE_NAME = "kpi";

    @Bind(R.id.title)
    TextView titleTextView;
    @Bind(R.id.web_view)
    WebView webView;
    @Bind(R.id.tooltip_title)
    TextView tooltipTitle;
    @Bind(R.id.tooltip_date)
    TextView tooltipDate;
    @Bind(R.id.tooltip_layout)
    LinearLayout tooltipLayout;
    @Bind(R.id.tv_legend)
    TextView tvLegend;

    private KpiBreakdownViewModel viewModel;
    private Context context;
    private long lastAnalyticsEventSentTime;
    private long lastAvailabilityAnalyticsEventSentTime;

    public static void startActivity(Activity activity, KpiBreakdownViewModel kpiBreakdownViewModel) {
        Intent intent = new Intent(activity, LandscapeKpiBreakdownActivity.class);

        Bundle bundle = new Bundle();
        if (kpiBreakdownViewModel != null) {
            bundle.putSerializable(VIEW_MODEL_PARAM, kpiBreakdownViewModel);
        }
        intent.putExtras(bundle);

        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        context = PrintOSApplication.getAppContext();
        Bundle args = getIntent().getExtras();
        if (args != null && args.containsKey(VIEW_MODEL_PARAM)) {
            viewModel = (KpiBreakdownViewModel) args.get(VIEW_MODEL_PARAM);
        }

        displayModel();
    }

    private void displayModel() {

        if (viewModel == null) {
            HPLogger.d(TAG, "last 16 week data not available.");
            return;
        }

        PrintOSPreferences preferences = PrintOSPreferences.getInstance(this);
        PreferencesData.UnitSystem unitSystem = preferences.getUnitSystem();

        KpiBreakdownEnum kpiBreakdownEnum = viewModel.getKpiBreakdownEnum();
        KPIValueHandleEnum valueHandleEnum = kpiBreakdownEnum.getValueHandleEnum();

        String title = kpiBreakdownEnum == KpiBreakdownEnum.INDIGO_LM ?
                valueHandleEnum.getUnitString(unitSystem) :
                getString(viewModel.getKpiBreakdownEnum().getNameResource());
        title = getString(R.string.kpi_breakdown_activity_title, title, NUMBER_OF_WEEKS);

        titleTextView.setText(title);
        KpiBreakdownUtils.displayGraph(this, webView, viewModel, true);
        webView.addJavascriptInterface(new WebViewJavaScriptInterface(), WEB_VIEW_INTERFACE_NAME);

        String action = String.format(Analytics.EVENT_KPI_LANDSCAPE, kpiBreakdownEnum.getBusinessUnitEnum().getShortName()).toLowerCase();
        String label = KpiBreakdownUtils.getTitle(this, viewModel);
        Analytics.sendEvent(action, label);

        Analytics.sendEvent(Analytics.VIEW_SCREEN_EVENT, Analytics.LANDSCAPE_BREAKDOWN_SCREEN_LABEL);
    }

    @Override
    public boolean onPortraitOrientation() {
        super.onPortraitOrientation();
        onBackPressed();
        return true;
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_kpi_breakdown_landscape;
    }

    private class WebViewJavaScriptInterface {

        public WebViewJavaScriptInterface() {

        }

        @JavascriptInterface
        public void tooltipChanged(final String tooltipDate, final String value, final String change, final String isChangeHigher) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    updateTooltipText(tooltipDate, value, change, isChangeHigher);
                    final long currentTime = SystemClock.elapsedRealtime();

                    if (viewModel != null && viewModel.getKpiBreakdownEnum() != null && currentTime - lastAnalyticsEventSentTime > Constants.SENDING_ANALYTICS_EVENT_DELAY) {
                        Analytics.sendEvent(String.format(Analytics.KPI_TOOLTIP_GRAPH_IS_CLICKED, titleTextView.getText().toString(), viewModel.getKpiBreakdownEnum().getBusinessUnitEnum().getShortName()).toLowerCase());
                        lastAnalyticsEventSentTime = currentTime;
                    }
                }
            });

        }

        @JavascriptInterface
        public void availabilityTooltipChanged(final String tooltipColor, final String kpiName, final String weekNumber, final String tooltipDate, final String tooltipTime, final String reset) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    updateAvailabilityTooltipText(tooltipDate, tooltipColor, tooltipTime, weekNumber, kpiName, reset);
                    final long currentTime = SystemClock.elapsedRealtime();

                    if (viewModel != null && viewModel.getKpiBreakdownEnum() != null && currentTime - lastAvailabilityAnalyticsEventSentTime > Constants.SENDING_ANALYTICS_EVENT_DELAY) {
                        Analytics.sendEvent(String.format(Analytics.KPI_TOOLTIP_GRAPH_IS_CLICKED, titleTextView.getText().toString(), viewModel.getKpiBreakdownEnum().getBusinessUnitEnum().getShortName()).toLowerCase());
                        lastAvailabilityAnalyticsEventSentTime = currentTime;
                    }
                }
            });
        }

        @JavascriptInterface
        public void graphLegend(final String legend) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    //spannable for graph legend
                    if (!TextUtils.isEmpty(legend)) {
                        SpannableStringUtils spannableLegend = new SpannableStringUtils(legend);
                        spannableLegend.setSpannable(new CustomTypefaceSpan("", TypefaceManager.getTypeface(getApplicationContext(),
                                TypefaceManager.HPTypeface.HP_REGULAR)), (int) getResources().getDimension(R.dimen.legend_font), ResourcesCompat.getColor(getResources(), R.color.c62, null), 0, legend.length());

                        tvLegend.setVisibility(View.VISIBLE);
                        tvLegend.setText(spannableLegend.getSpannableString());
                    }
                }
            });
        }
    }

    private void updateTooltipText(String tooltipDate, String value, String change, String isHigher) {

        //spannable for tooltip title
        String arrow = String.valueOf(Boolean.parseBoolean(isHigher) ? Html.fromHtml(getResources().getString(R.string.tooltip_change_higher)) : Html.fromHtml(getResources().getString(R.string.tooltip_change_lower)));
        String changeWithArrow = arrow + String.valueOf(Math.abs(Float.parseFloat(change))) + "%";
        int color = Boolean.parseBoolean(isHigher) ? R.color.bar_green : R.color.bar_red;

        SpannableStringUtils spannableValue = new SpannableStringUtils(value);
        spannableValue.setSpannable(new CustomTypefaceSpan("", TypefaceManager.getTypeface(context,
                TypefaceManager.HPTypeface.HP_BOLD)), (int) getResources().getDimension(R.dimen.tooltip_title_font), ResourcesCompat.getColor(getResources(), R.color.tooltip_title_color, null), 0, value.length());
        SpannableString spannableValueString = spannableValue.getSpannableString();

        SpannableStringUtils spannableChange = new SpannableStringUtils(changeWithArrow);
        spannableChange.setSpannable(new CustomTypefaceSpan("", TypefaceManager.getTypeface(context,
                TypefaceManager.HPTypeface.HP_BOLD)), (int) getResources().getDimension(R.dimen.tooltip_change_font), ResourcesCompat.getColor(getResources(), color, null), 0, changeWithArrow.length());

        tooltipTitle.setText(TextUtils.concat(spannableValueString, " ", spannableChange.getSpannableString()));

        //spannable for tooltip date
        SpannableStringUtils spannableDate = new SpannableStringUtils(tooltipDate);
        spannableDate.setSpannable(new CustomTypefaceSpan("", TypefaceManager.getTypeface(context,
                TypefaceManager.HPTypeface.HP_REGULAR)), (int) getResources().getDimension(R.dimen.tooltip_title_date_font), ResourcesCompat.getColor(getResources(), R.color.tooltip_title_color, null), 0, tooltipDate.length());

        this.tooltipDate.setText(spannableDate.getSpannableString());
    }

    private void updateAvailabilityTooltipText(String tooltipDate, String tooltipColor, String tooltipTime, String weekNumber, String kpiName, String reset) {

        SpannableStringUtils spannableDate;
        SpannableStringUtils spannableKpiName;
        String availabilityTitle;

        if (Boolean.valueOf(reset)) {

            String totalUpTime = getResources().getString(R.string.reports_total_up_time);
            availabilityTitle = getResources().getString(R.string.reports_total_up_time) + " - " + weekNumber;

            //spannable for tooltip date
            spannableDate = new SpannableStringUtils(tooltipTime);
            spannableDate.setSpannable(new CustomTypefaceSpan("", TypefaceManager.getTypeface(context,
                    TypefaceManager.HPTypeface.HP_REGULAR)), (int) getResources().getDimension(R.dimen.availability_tooltip_title_font), ResourcesCompat.getColor(getResources(), R.color.tooltip_title_color, null), 0, tooltipTime.length());

            //spannable for kpi name
            spannableKpiName = new SpannableStringUtils(availabilityTitle);
            spannableKpiName.setSpannable(new CustomTypefaceSpan("", TypefaceManager.getTypeface(context,
                    TypefaceManager.HPTypeface.HP_BOLD)), (int) getResources().getDimension(R.dimen.availability_tooltip_title_font), ResourcesCompat.getColor(getResources(), R.color.c62, null), 0, totalUpTime.length());
            spannableKpiName.setSpannable(new CustomTypefaceSpan("", TypefaceManager.getTypeface(context,
                    TypefaceManager.HPTypeface.HP_REGULAR)), (int) getResources().getDimension(R.dimen.tooltip_title_font), ResourcesCompat.getColor(getResources(), R.color.tooltip_title_color, null), availabilityTitle.lastIndexOf(" - "), availabilityTitle.length());
        } else {

            //spannable for tooltip date
            spannableDate = new SpannableStringUtils(tooltipDate.concat("%  ") + "(" + tooltipTime + ")");
            spannableDate.setSpannable(new CustomTypefaceSpan("", TypefaceManager.getTypeface(context,
                    TypefaceManager.HPTypeface.HP_BOLD)), (int) getResources().getDimension(R.dimen.availability_tooltip_title_font), ResourcesCompat.getColor(getResources(), R.color.tooltip_title_color, null), 0, tooltipDate.length() + 1);

            //spannable for kpi name
            availabilityTitle = kpiName + " - " + weekNumber;
            spannableKpiName = new SpannableStringUtils(availabilityTitle);
            spannableKpiName.setSpannable(new CustomTypefaceSpan("", TypefaceManager.getTypeface(context,
                    TypefaceManager.HPTypeface.HP_BOLD)), (int) getResources().getDimension(R.dimen.availability_tooltip_title_font), Color.parseColor(tooltipColor), 0, kpiName.length());
            spannableKpiName.setSpannable(new CustomTypefaceSpan("", TypefaceManager.getTypeface(context,
                    TypefaceManager.HPTypeface.HP_REGULAR)), (int) getResources().getDimension(R.dimen.availability_tooltip_title_font), ResourcesCompat.getColor(getResources(), R.color.tooltip_title_color, null), availabilityTitle.lastIndexOf(" - "), availabilityTitle.length());
        }

        tooltipTitle.setText(spannableKpiName.getSpannableString());
        this.tooltipDate.setText(spannableDate.getSpannableString());

    }

}
