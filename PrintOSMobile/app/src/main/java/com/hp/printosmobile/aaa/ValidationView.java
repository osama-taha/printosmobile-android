package com.hp.printosmobile.aaa;

import com.hp.printosmobile.presentation.MVPView;
import com.hp.printosmobilelib.core.communications.remote.models.UserData;

/**
 * Created by Anwar Asbah on 6/7/2017.
 */

public interface ValidationView extends MVPView {

    void onPreValidation();

    void onPreAutoLogin(boolean showToast);

    void onValidationCompleted(UserData.User user);

    void onValidationError();

    void onValidationUnauthorized();

    void onValidationCompletedWithoutContextChange(UserData.User user);

    void onServerDown();

}
