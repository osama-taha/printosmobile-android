package com.hp.printosmobile.presentation.modules.reportkpibreakdown;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.RelativeSizeSpan;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hp.printosmobile.OrientationSupportBaseActivity;
import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;
import com.hp.printosmobile.presentation.TargetViewManager;
import com.hp.printosmobile.presentation.modules.home.Panel;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitViewModel;
import com.hp.printosmobile.presentation.modules.reports.ReportFilter;
import com.hp.printosmobile.presentation.modules.reports.ReportKpiViewModel;
import com.hp.printosmobile.presentation.modules.reports.presenter.ReportDataManager;
import com.hp.printosmobile.utils.HPLocaleUtils;
import com.hp.printosmobile.utils.HPUIUtils;
import com.hp.printosmobile.utils.ShareUtils;
import com.hp.printosmobilelib.core.communications.remote.APIException;
import com.hp.printosmobilelib.ui.widgets.HPScrollView;
import com.hp.printosmobilelib.ui.widgets.panel.PanelView;
import com.hp.printosmobilelib.ui.widgets.panel.SharableObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;

/**
 * created by Anwar Asbah 1/30/2018
 */
public class KpiBreakdownActivity extends OrientationSupportBaseActivity implements KpiBreakdownView, KpiBreakdownPanel.KpiBreakdownPanelCallback {

    private static final String TAG = KpiBreakdownActivity.class.getName();
    public static final String REPORT_FILTER_PARAM = "report_filter_param";
    public static final String FOCUSABLE_PANEL_PARAM = "focusable_panel_param";
    public static final int NUMBER_OF_WEEKS = 16;

    private static final int READ_WRITE_EXTERNAL_STORAGE_PERMISSION = 1;
    private static final long SCROLL_TO_PANEL_DELAY = 200;

    private Map<KpiBreakdownEnum, PanelView> panelViewMap = new HashMap<>();

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.toolbar_display_title)
    TextView toolbarTitle;
    @Bind(R.id.panels_container)
    LinearLayout panelsContainerView;
    @Bind(R.id.parent_container)
    View parentContainer;
    @Bind(R.id.scroll_view)
    HPScrollView scrollView;
    @Bind(R.id.loading_indicator)
    View loadingView;
    @Bind(R.id.error_msg_text_view)
    TextView errorMsgTextView;

    private PanelView panelRequestedSharing;
    private KpiBreakdownPresenter kpiBreakdownPresenter;
    private ReportFilter reportFilter;
    private String focusablePanel;

    public static void startActivity(Activity activity, String kpiName, BusinessUnitViewModel businessUnitViewModel,
                                     String focusablePanel) {
        Intent intent = new Intent(activity, KpiBreakdownActivity.class);

        Bundle bundle = new Bundle();

        ReportKpiViewModel reportKpiViewModel = new ReportKpiViewModel();
        reportKpiViewModel.setName(kpiName);

        ReportFilter reportFilter = getReportFilter();
        reportFilter.setBusinessUnit(businessUnitViewModel);
        reportFilter.setKpi(reportKpiViewModel);

        bundle.putSerializable(REPORT_FILTER_PARAM, reportFilter);
        if(focusablePanel != null) {
            bundle.putString(FOCUSABLE_PANEL_PARAM, focusablePanel);
        }
        intent.putExtras(bundle);

        activity.startActivity(intent);
    }

    public static ReportFilter getReportFilter() {
        ReportFilter reportFilter = new ReportFilter();
        reportFilter.setOffset(-(NUMBER_OF_WEEKS + 1));
        reportFilter.setDateFormat(ReportDataManager.DATE_FORMAT);
        reportFilter.setUnit(ReportFilter.Unit.SHEETS);
        reportFilter.setResolution(ReportFilter.Resolution.WEEK);

        return reportFilter;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getIntent().getExtras();

        if (args != null) {
            if (args.containsKey(REPORT_FILTER_PARAM)) {
                reportFilter = (ReportFilter) args.get(REPORT_FILTER_PARAM);
            }
            if(args.containsKey(FOCUSABLE_PANEL_PARAM)) {
                focusablePanel = args.getString(FOCUSABLE_PANEL_PARAM);
            }
        }

        KpiBreakdownEnum kpiBreakdownEnum;

        if (reportFilter == null || reportFilter.getKpi() == null) {
            kpiBreakdownEnum = KpiBreakdownEnum.from("", null);
        } else {
            kpiBreakdownEnum = KpiBreakdownEnum.from(reportFilter.getKpi().getName(), reportFilter.getBusinessUnit() == null ?
                    null : reportFilter.getBusinessUnit().getBusinessUnit());
        }

        String kpiName = getString(kpiBreakdownEnum.getNameResource());
        String title = getString(R.string.kpi_breakdown_activity_title, kpiName, NUMBER_OF_WEEKS);
        Spannable titleSpannable = new SpannableString(title);
        titleSpannable.setSpan(new RelativeSizeSpan(0.8f), 0, title.length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);
        toolbarTitle.setText(titleSpannable);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(ResourcesCompat.getDrawable(getResources(), R.drawable.back_button, null));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        initPresenter();
    }

    @Override
    public boolean onLandscapeOrientation() {
        super.onLandscapeOrientation();
        int index = HPUIUtils.getMostVisiblePanelIndex(scrollView, panelsContainerView);
        if (index >= 0 && index < panelsContainerView.getChildCount()) {
            View panel = panelsContainerView.getChildAt(index);
            if (panel instanceof KpiBreakdownPanel) {
                LandscapeKpiBreakdownActivity.startActivity(this, ((KpiBreakdownPanel) panel).getViewModel());
                return true;
            }
        }
        return false;
    }

    private void initPresenter() {
        loadingView.setVisibility(View.VISIBLE);
        kpiBreakdownPresenter = new KpiBreakdownPresenter();
        kpiBreakdownPresenter.attachView(this);
        if (TextUtils.isEmpty(reportFilter.getKpi().getName())) {
            kpiBreakdownPresenter.getOverallData(reportFilter);
        } else {
            kpiBreakdownPresenter.getKpiData(reportFilter, false);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_kpi_break_down;
    }

    @Override
    public void displayKpiBreakdown(List<KpiBreakdownViewModel> kpiBreakdownViewModels) {
        loadingView.setVisibility(View.GONE);
        for (int i = 0; i < kpiBreakdownViewModels.size(); i++) {
            KpiBreakdownViewModel kpiBreakdownViewModel = kpiBreakdownViewModels.get(i);

            if (kpiBreakdownViewModel.getKpiBreakdownEnum() == KpiBreakdownEnum.INDIGO_PRINTING_TIME ||
                    kpiBreakdownViewModel.getKpiBreakdownEnum() == KpiBreakdownEnum.INDIGO_NON_PRINTING_TIME ||
                    kpiBreakdownViewModel.getKpiBreakdownEnum() == KpiBreakdownEnum.INDIGO_UP_TIME) {
                continue;
            }
            initPanel(kpiBreakdownViewModel.getKpiBreakdownEnum(), kpiBreakdownViewModel);
        }

        if(focusablePanel != null) {
            KpiBreakdownEnum breakdownEnum = KpiBreakdownEnum.fromDeepLink(focusablePanel, reportFilter.getBusinessUnit() == null ?
                    null : reportFilter.getBusinessUnit().getBusinessUnit());
            scrollFragmentToPanel(breakdownEnum);
            focusablePanel = null;
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                for (KpiBreakdownEnum kpiBreakdownEnum : panelViewMap.keySet()) {
                    if (TargetViewManager.showTargetView(KpiBreakdownActivity.this, kpiBreakdownEnum.name(), panelViewMap.get(kpiBreakdownEnum), null)) {
                        break;
                    }
                }
            }
        }, 500);

    }

    @Override
    public void onError(APIException exception, String tag) {
        super.onError(exception, tag);
        errorMsgTextView.setText(HPLocaleUtils.getSimpleErrorMsg(this, exception));
        errorMsgTextView.setVisibility(View.VISIBLE);
        loadingView.setVisibility(View.GONE);
    }

    private void initPanel(KpiBreakdownEnum kpiBreakdownEnum, KpiBreakdownViewModel viewModel) {
        PanelView panelView;

        LinearLayout.LayoutParams layoutParams = HPUIUtils.getPanelLayoutParams(this);

        panelView = new KpiBreakdownPanel(this);
        ((KpiBreakdownPanel) panelView).addKpiBreakdownPanelCallback(this);
        ((KpiBreakdownPanel) panelView).updateViewModel(viewModel);

        panelViewMap.put(kpiBreakdownEnum, panelView);
        panelsContainerView.addView(panelView, layoutParams);
    }

    @Override
    public void onDestroy() {
        if (panelViewMap != null) {
            for (KpiBreakdownEnum panel : panelViewMap.keySet()) {
                panelViewMap.get(panel).onDestroy();
            }
        }
        super.onDestroy();
    }

    @Override
    public void onShareButtonClicked(PanelView panelView) {
        panelRequestedSharing = panelView;
        if (this.panelRequestedSharing == null) {
            return;
        }

        if (permissionsRequested()) {
            performSharing(this.panelRequestedSharing);
        }
    }

    private boolean permissionsRequested() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            int writePermissionGrantedCode = ContextCompat.checkSelfPermission(PrintOSApplication.getAppContext(),
                    Manifest.permission.WRITE_EXTERNAL_STORAGE);

            int readPermissionGrantedCode = ContextCompat.checkSelfPermission(PrintOSApplication.getAppContext(),
                    Manifest.permission.READ_EXTERNAL_STORAGE);

            if (readPermissionGrantedCode != PackageManager.PERMISSION_GRANTED ||
                    writePermissionGrantedCode != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.READ_EXTERNAL_STORAGE},
                        READ_WRITE_EXTERNAL_STORAGE_PERMISSION);
                return false;
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == READ_WRITE_EXTERNAL_STORAGE_PERMISSION) {
            if (grantResults.length > 0) {
                boolean allGranted = true;
                for (int i = 0; i < grantResults.length; i++) {
                    allGranted = allGranted && grantResults[i] == PackageManager.PERMISSION_GRANTED;
                }
                if (allGranted) {
                    performSharing(panelRequestedSharing);
                }
            }
        }
    }

    public void performSharing(PanelView panelView) {
        if (panelView != null) {
            panelView.sharePanel();
        }
    }

    @Override
    public void onShareButtonClicked(Panel panel) {
        //do nothing
    }

    @Override
    public void onScreenshotCreated(Panel panel, Uri screenshotUri, String title, String body, SharableObject sharableObject) {
        ShareUtils.onScreenshotCreated(this, screenshotUri, title, body, sharableObject);
    }

    private void scrollFragmentToPanel(final KpiBreakdownEnum kpiBreakdownEnum) {
        if (kpiBreakdownEnum == null || panelViewMap == null || !panelViewMap.containsKey(kpiBreakdownEnum)) {
            return;
        }

        final PanelView panelView = panelViewMap.get(kpiBreakdownEnum);

        scrollView.postDelayed(new Runnable() {
            @Override
            public void run() {
                scrollView.scrollTo(0, panelView.getTop());
            }
        }, SCROLL_TO_PANEL_DELAY);

    }
}
