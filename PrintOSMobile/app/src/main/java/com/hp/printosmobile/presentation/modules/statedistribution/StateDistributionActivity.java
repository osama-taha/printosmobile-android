package com.hp.printosmobile.presentation.modules.statedistribution;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.widget.TextView;

import com.hp.printosmobile.Analytics;
import com.hp.printosmobile.Constants;
import com.hp.printosmobile.OrientationSupportBaseActivity;
import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;
import com.hp.printosmobile.presentation.TargetViewManager;
import com.hp.printosmobile.presentation.modules.home.HomePresenter;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitEnum;
import com.hp.printosmobile.presentation.modules.shared.DeepLinkUtils;
import com.hp.printosmobile.presentation.modules.shared.DeviceViewModel;
import com.hp.printosmobile.utils.FileUtils;
import com.hp.printosmobile.utils.HPUIUtils;
import com.hp.printosmobile.utils.ShareUtils;
import com.hp.printosmobilelib.core.communications.remote.APIException;
import com.hp.printosmobilelib.core.utils.HPDateUtils;
import com.hp.printosmobilelib.ui.widgets.panel.SharableObject;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * created by Anwar Asbah 2/18/2018
 */
public class StateDistributionActivity extends OrientationSupportBaseActivity implements StateDistributionView, SharableObject {

    public static final String TAG = StateDistributionActivity.class.getSimpleName();
    public static final String BUSINESS_UNIT_ARG = "business_unit_arg";
    public static final String DEVICE_NUMBER_ARG = "device_number_arg";
    public static final String IS_SHIFT_SUPPORT_ARG = "is_shift_support_arg";
    private static final String WEB_VIEW_INTERFACE_NAME = "pie";


    private static final String FABRIC_SHARE_STATE_DISTRIBUTION_PORTRAIT = "Portrait state distribution";
    private static final String HEADER_DATE_FORMAT = "h:mm aa";
    public static final String SCREEN_SHOT_FILE_NAME = "PrintOS";

    private static final int READ_WRITE_EXTERNAL_STORAGE_PERMISSION = 1;

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.toolbar_display_title)
    TextView toolbarTitle;
    @Bind(R.id.share_button)
    View shareButton;
    @Bind(R.id.sharable_content)
    View sharableContent;
    @Bind(R.id.press_name_text_view)
    TextView pressNameTextView;
    @Bind(R.id.time_description_text_view)
    TextView timeDescriptionTextView;
    @Bind(R.id.state_distribution_web_view)
    WebView stateDistributionWebView;
    @Bind(R.id.loading_indicator)
    View loadingView;
    @Bind(R.id.error_msg_text_view)
    TextView errorMsgTextView;
    @Bind(R.id.disconnected_text_view)
    TextView disconnectedTextView;
    @Bind(R.id.footer)
    View footer;
    @Bind(R.id.content_with_data)
    View contentWithData;
    @Bind(R.id.no_data_text_view)
    TextView noDataTextView;
    @Bind(R.id.root_container)
    View rootContainer;

    private DeviceViewModel deviceViewModel;
    private BusinessUnitEnum businessUnitEnum;
    private boolean isShiftSupport;
    private StateDistributionViewModel viewModel;

    private StateDistributionPresenter presenter;
    private boolean showTargetView = true;
    private boolean orientationEnabled = true;

    public static void startActivity(Activity activity, DeviceViewModel deviceViewModel, BusinessUnitEnum businessUnitEnum,
                                     boolean isShiftSupport) {

        Intent intent = new Intent(activity, StateDistributionActivity.class);

        Bundle bundle = new Bundle();
        if (deviceViewModel != null) {
            bundle.putSerializable(DEVICE_NUMBER_ARG, deviceViewModel);
        }
        if (businessUnitEnum != null) {
            bundle.putInt(BUSINESS_UNIT_ARG, businessUnitEnum.ordinal());
        }
        bundle.putBoolean(IS_SHIFT_SUPPORT_ARG, isShiftSupport);
        intent.putExtras(bundle);

        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getIntent().getExtras();
        if (args != null) {
            if (args.containsKey(BUSINESS_UNIT_ARG)) {
                businessUnitEnum = BusinessUnitEnum.values()[args.getInt(BUSINESS_UNIT_ARG)];
            }
            if (args.containsKey(IS_SHIFT_SUPPORT_ARG)) {
                isShiftSupport = args.getBoolean(IS_SHIFT_SUPPORT_ARG);
            }
            if (args.containsKey(DEVICE_NUMBER_ARG)) {
                deviceViewModel = (DeviceViewModel) args.getSerializable(DEVICE_NUMBER_ARG);
            }
        }


        stateDistributionWebView.addJavascriptInterface(new WebViewJavaScriptInterface(), WEB_VIEW_INTERFACE_NAME);

        toolbarTitle.setText(isShiftSupport ? R.string.state_distribution_title_shift :
                R.string.state_distribution_title);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(ResourcesCompat.getDrawable(getResources(), R.drawable.back_button, null));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        initPresenter();

        Analytics.sendEvent(Analytics.VIEW_SCREEN_EVENT, Analytics.STATE_DIS_SCREEN_LABEL);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Analytics.sendEvent(Analytics.PORTRAIT_STATE_DISTRIBUTION_SHOWN_EVENT);


        if (viewModel != null && showTargetView) {
            showTarget();
        }
    }

    private void initPresenter() {
        loadingView.setVisibility(View.VISIBLE);
        presenter = new StateDistributionPresenter();
        presenter.attachView(this);
        if (deviceViewModel != null) {
            pressNameTextView.setText(deviceViewModel.getName());
            presenter.getPressStateDistribution(businessUnitEnum, deviceViewModel.getSerialNumber(), isShiftSupport);
        }
    }

    @Override
    public boolean onLandscapeOrientation() {
        super.onLandscapeOrientation();
        if (viewModel != null && viewModel.getPressStates() != null && !viewModel.getPressStates().isEmpty() && orientationEnabled) {
            StateDistributionLandscapeActivity.startActivity(this, deviceViewModel, viewModel, isShiftSupport);
            showTargetView = true;
            return true;
        }
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_state_distribution;
    }

    @Override
    public void onPressStateDistributionRetrieved(final StateDistributionViewModel viewModel) {
        this.viewModel = viewModel;

        if (viewModel == null || viewModel.getAggregateStateMap() == null ||
                viewModel.getPressStates() == null || viewModel.getPressStates().isEmpty()) {
            onError(APIException.create(APIException.Kind.UNEXPECTED), TAG);
            return;
        }

        loadingView.setVisibility(View.GONE);

        boolean isDisconnected = StateDistributionUtils.isPressDisconnected(viewModel);

        HPUIUtils.setVisibility(isDisconnected, noDataTextView);
        HPUIUtils.setVisibility(!isDisconnected, contentWithData);
        if (isDisconnected) {
            noDataTextView.setText(StateDistributionUtils.getNoDataMsg(viewModel));
            return;
        }

        stateDistributionWebView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                stateDistributionWebView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                StateDistributionUtils.displayPortraitGraph(StateDistributionActivity.this, stateDistributionWebView, viewModel, disconnectedTextView);
            }
        });

        String headerTime = HPDateUtils.formatDate(viewModel.getPressStates().get(0).getStartDate(), HEADER_DATE_FORMAT);
        timeDescriptionTextView.setText(getString(R.string.state_distribution_header_time_stamp, headerTime));
        timeDescriptionTextView.setVisibility(View.VISIBLE);

        shareButton.setVisibility(View.VISIBLE);

        showTarget();
    }

    @Override
    public void onError(APIException exception, String tag) {
        super.onError(exception, tag);
        errorMsgTextView.setText(R.string.error_no_data_msg);
        errorMsgTextView.setVisibility(View.VISIBLE);
        loadingView.setVisibility(View.GONE);
        footer.setVisibility(View.GONE);
    }

    @OnClick(R.id.share_button)
    public void onShareButtonClicked() {
        if (permissionsRequested()) {
            performSharing();
        }
    }

    private boolean permissionsRequested() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            int writePermissionGrantedCode = ContextCompat.checkSelfPermission(PrintOSApplication.getAppContext(),
                    Manifest.permission.WRITE_EXTERNAL_STORAGE);

            int readPermissionGrantedCode = ContextCompat.checkSelfPermission(PrintOSApplication.getAppContext(),
                    Manifest.permission.READ_EXTERNAL_STORAGE);

            if (readPermissionGrantedCode != PackageManager.PERMISSION_GRANTED ||
                    writePermissionGrantedCode != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.READ_EXTERNAL_STORAGE},
                        READ_WRITE_EXTERNAL_STORAGE_PERMISSION);
                return false;
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == READ_WRITE_EXTERNAL_STORAGE_PERMISSION) {
            if (grantResults.length > 0) {
                boolean allGranted = true;
                for (int i = 0; i < grantResults.length; i++) {
                    allGranted = allGranted && grantResults[i] == PackageManager.PERMISSION_GRANTED;
                }
                if (allGranted) {
                    performSharing();
                }
            }
        }
    }

    public void performSharing() {
        if (deviceViewModel == null) {
            return;
        }
        lockShareButton(true);

        Bitmap panelBitmap = FileUtils.getScreenShot(sharableContent);

        if (panelBitmap != null) {
            final Uri storageUri = FileUtils.store(this, panelBitmap, SCREEN_SHOT_FILE_NAME);

            DeepLinkUtils.getShareBody(this,
                    Constants.UNIVERSAL_LINK_SCREEN_STATE_DISTRIBUTION,
                    null,
                    HomePresenter.isShiftSupport(),
                    deviceViewModel != null ? deviceViewModel.getSerialNumber() : "", false, new DeepLinkUtils.BranchIOCallback() {
                        @Override
                        public void onLinkCreated(String link) {
                            onScreenshotCreated(storageUri,
                                    getString(R.string.state_distribution_share_subject, deviceViewModel.getName()),
                                    link);
                            lockShareButton(false);
                        }
                    });
        }
    }

    public void lockShareButton(boolean lock) {
        //Concrete classes may hide the implementation of it with their custom implementation.
        if (shareButton != null) {
            shareButton.setEnabled(!lock);
            shareButton.setClickable(!lock);
        }
    }

    public void onScreenshotCreated(Uri screenshotUri, String title, String body) {
        ShareUtils.onScreenshotCreated(this, screenshotUri, title, body, null);
    }

    @Override
    public String getShareAction() {
        return FABRIC_SHARE_STATE_DISTRIBUTION_PORTRAIT;
    }

    private void showTarget() {
        boolean shown = TargetViewManager.showTargetView(this, TAG, rootContainer, new TargetViewManager.TapTargetCallback() {
            @Override
            public void onTapTargetClick(TargetViewManager.TargetView targetView) {
                orientationEnabled = true;
            }

            @Override
            public void onCancel() {
                orientationEnabled = true;
            }
        });

        showTargetView = !shown;
        orientationEnabled = !shown;
    }

    private class WebViewJavaScriptInterface {

        public WebViewJavaScriptInterface() {

        }

        @JavascriptInterface
        public void legendChanged(final String name) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (!TextUtils.isEmpty(name)) {
                        Analytics.sendEvent(Analytics.STATE_DISTRIBUTION_LEGEND_CLICKED, name);
                    }
                }
            });

        }
    }
}
