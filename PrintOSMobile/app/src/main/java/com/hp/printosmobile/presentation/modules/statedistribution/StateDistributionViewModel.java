package com.hp.printosmobile.presentation.modules.statedistribution;

import android.os.Build;
import android.support.annotation.NonNull;

import com.hp.printosmobile.presentation.modules.shared.DeviceState;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * Created by Anwar Asbah on 2/18/2018.
 */
public class StateDistributionViewModel implements Serializable {

    public static final Comparator<PressState> PRESS_STATE_DATE_COMPARATOR = new Comparator<PressState>() {
        @Override
        public int compare(PressState pressStateL, PressState pressStateR) {
            if (pressStateL == null || pressStateR == null) {
                return 0;
            }
            return pressStateL.compareTo(pressStateR);
        }
    };

    private Map<DeviceState, Float> aggregateStateMap;
    private List<PressState> pressStates;

    public Map<DeviceState, Float> getAggregateStateMap() {
        return aggregateStateMap;
    }

    public void setAggregateStateMap(Map<DeviceState, Float> aggregateStateMap) {
        this.aggregateStateMap = aggregateStateMap;
    }

    public List<PressState> getPressStates() {
        return pressStates;
    }

    public void setPressStates(List<PressState> pressStates) {
        this.pressStates = pressStates;
    }

    public static class PressState implements Serializable, Comparable<PressState> {
        private DeviceState deviceState;
        private Date startDate;
        private Date endDate;

        public DeviceState getDeviceState() {
            return deviceState;
        }

        public void setDeviceState(DeviceState deviceState) {
            this.deviceState = deviceState;
        }

        public Date getStartDate() {
            return startDate;
        }

        public void setStartDate(Date startDate) {
            this.startDate = startDate;
        }

        public Date getEndDate() {
            return endDate;
        }

        public void setEndDate(Date endDate) {
            this.endDate = endDate;
        }

        @Override
        public int compareTo(@NonNull PressState pressState) {
            Date dateL = getStartDate();
            Date dateR = pressState.getStartDate();

            if (dateL == null || dateR == null) {
                return 0;
            }

            return dateL.compareTo(dateR);
        }

        @Override
        public boolean equals(Object obj) {
            return super.equals(obj);
        }

        @Override
        public int hashCode() {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                return Objects.hash(deviceState, startDate, endDate);
            }
            return 0;
        }
    }
}
