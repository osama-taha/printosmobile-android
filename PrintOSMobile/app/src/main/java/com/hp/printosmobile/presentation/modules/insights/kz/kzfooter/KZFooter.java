package com.hp.printosmobile.presentation.modules.insights.kz.kzfooter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v4.content.res.ResourcesCompat;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.hp.printosmobile.Constants;
import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;
import com.hp.printosmobile.presentation.modules.insights.InsightsDataManager;
import com.hp.printosmobile.presentation.modules.shared.DeepLinkUtils;
import com.hp.printosmobile.utils.HPUIUtils;

import butterknife.ButterKnife;

/**
 * Created by Minerva on 5/22/2018.
 */
public class KZFooter extends LinearLayout {

    private static final int RATE_NONE = 0;
    private static final int RATE_POOR = 1;
    private static final int RATE_FAIR = 2;
    private static final int RATE_AVERAGE = 3;
    private static final int RATE_GOOD = 4;
    private static final int RATE_EXCELLENT = 5;

    private ImageView likeButton;
    private ImageView dislikeButton;
    private ImageView favoriteButton;
    private ImageView shareButton;
    private Boolean isFavorite;
    private String selectedBu;
    private String kzItemId;
    private int isLiked = 0; // 0 not set, 1 liked , 2 disliked
    private int isLikedOriginalState;
    private int itemRating;
    private boolean likeClicked;
    private boolean dislikeClicked;
    private boolean favButtonEnabled = true;

    @SuppressLint("ResourceType")
    Animation animation = AnimationUtils.loadAnimation(getContext(), R.animator.scale);
    private String kzItemName;

    public KZFooter(Context context) {
        super(context);
        init();
    }

    public KZFooter(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public KZFooter(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public void setIsFavorite(Boolean isFavorite) {
        this.isFavorite = isFavorite;
        favoriteButton.setImageDrawable(ResourcesCompat.getDrawable(getResources(),
                isFavorite ? R.drawable.favorite_active : R.drawable.favorite, null));
    }

    public void setIsLiked(int rating) {
        itemRating = rating;
        if (rating == RATE_POOR || rating == RATE_FAIR) { // dislike
            isLiked = 2;
            likeButton.setImageDrawable(ResourcesCompat.getDrawable(getResources(),
                    R.drawable.kz_like, null));
            dislikeButton.setImageDrawable(ResourcesCompat.getDrawable(getResources(),
                    R.drawable.unlike_active, null));
            dislikeClicked = true;
            likeClicked = false;
        } else if (rating == RATE_AVERAGE || rating == RATE_GOOD || rating == RATE_EXCELLENT) { // like
            isLiked = 1;
            likeButton.setImageDrawable(ResourcesCompat.getDrawable(getResources(),
                    R.drawable.like_active, null));
            dislikeButton.setImageDrawable(ResourcesCompat.getDrawable(getResources(),
                    R.drawable.unlike, null));
            likeClicked = true;
            dislikeClicked = false;
        } else if (rating == RATE_NONE) {
            isLiked = 0;
            likeButton.setImageDrawable(ResourcesCompat.getDrawable(getResources(),
                    R.drawable.kz_like, null));
            dislikeButton.setImageDrawable(ResourcesCompat.getDrawable(getResources(),
                    R.drawable.unlike, null));
            dislikeClicked = false;
            likeClicked = false;
        }
    }

    public void setKZProps(String kzItemId, String selectedBu, String kzItemName) {
        this.kzItemId = kzItemId;
        this.selectedBu = selectedBu;
        this.kzItemName = kzItemName;
    }

    private void init() {
        inflate(getContext(), R.layout.view_kz_footer, this);
        likeButton = findViewById(R.id.kz_like_icon);
        dislikeButton = findViewById(R.id.kz_dislike_icon);
        favoriteButton = findViewById(R.id.kz_fav_icon);
        shareButton = findViewById(R.id.kz_share_icon);
        ButterKnife.bind(this, this);

        favoriteButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (favButtonEnabled) {
                    favButtonEnabled = false;
                    if (isFavorite) {
                        favoriteButton.setImageDrawable(ResourcesCompat.getDrawable(getResources(),
                                R.drawable.favorite, null));
                        if (mEventListener != null) {
                            mEventListener.onFavoritePressed(false);
                        }
                        isFavorite = false;

                        InsightsDataManager.removeFavoriteItem(kzItemId, selectedBu, new InsightsDataManager.ItemCallback() {
                            @Override
                            public void onSuccess() {
                                favButtonEnabled = true;
                                HPUIUtils.displayToastWithCenteredText(PrintOSApplication.getAppContext(), PrintOSApplication.getAppContext().getString(R.string.success_removing_item_from_favorites));
                            }

                            @Override
                            public void onFailure() {
                                favButtonEnabled = true;
                                HPUIUtils.displayToastWithCenteredText(PrintOSApplication.getAppContext(), PrintOSApplication.getAppContext().getString(R.string.failed_to_remove_item_from_favorites));
                                favoriteButton.setImageDrawable(ResourcesCompat.getDrawable(getResources(),
                                        R.drawable.favorite_active, null));
                                if (mEventListener != null) {
                                    mEventListener.onFavoritePressed(true);
                                }
                                isFavorite = true;

                            }
                        });
                    } else {
                        favoriteButton.setImageDrawable(ResourcesCompat.getDrawable(getResources(),
                                R.drawable.favorite_active, null));
                        if (mEventListener != null) {
                            mEventListener.onFavoritePressed(true);
                        }
                        isFavorite = true;

                        InsightsDataManager.setItemFavorite(getContext(), kzItemId, selectedBu, new InsightsDataManager.ItemCallback() {
                            @Override
                            public void onSuccess() {
                                favButtonEnabled = true;
                                HPUIUtils.displayToastWithCenteredText(PrintOSApplication.getAppContext(), PrintOSApplication.getAppContext().getString(R.string.success_adding_item_to_favorites));
                            }

                            @Override
                            public void onFailure() {
                                favButtonEnabled = true;
                                HPUIUtils.displayToastWithCenteredText(PrintOSApplication.getAppContext(), PrintOSApplication.getAppContext().getString(R.string.failed_to_add_item_to_favorites));
                                favoriteButton.setImageDrawable(ResourcesCompat.getDrawable(getResources(),
                                        R.drawable.favorite, null));
                                if (mEventListener != null) {
                                    mEventListener.onFavoritePressed(false);
                                }
                                isFavorite = false;
                            }
                        });
                    }
                }
            }
        });

        likeButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isLiked != 1 && !likeClicked) {
                    likeButton.startAnimation(animation);
                    isLikedOriginalState = isLiked;
                    setIsLiked(RATE_EXCELLENT);
                    if (mEventListener != null) {
                        mEventListener.onLikePressed(itemRating);
                    }
                    InsightsDataManager.rateItem(kzItemId, selectedBu, RATE_EXCELLENT, new InsightsDataManager.ItemCallback() {
                        @Override
                        public void onSuccess() {
                        }

                        @Override
                        public void onFailure() {
                            int rating = 0;
                            if (isLikedOriginalState == 1) {
                                rating = RATE_EXCELLENT;
                                setIsLiked(RATE_EXCELLENT);
                            } else if (isLikedOriginalState == 0) {
                                setIsLiked(RATE_NONE);
                            } else if (isLikedOriginalState == 2) {
                                setIsLiked(RATE_POOR);
                                rating = RATE_POOR;
                            }
                            if (mEventListener != null) {
                                mEventListener.onLikePressed(rating);
                            }
                            HPUIUtils.displayToastWithCenteredText(PrintOSApplication.getAppContext(), PrintOSApplication.getAppContext().getString(R.string.failed_to_like_item));
                        }
                    });
                }
            }
        });

        dislikeButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isLiked != 2 && !dislikeClicked) {
                    dislikeButton.startAnimation(animation);
                    isLikedOriginalState = isLiked;
                    setIsLiked(RATE_POOR);
                    if (mEventListener != null) {
                        mEventListener.onDislikePressed(itemRating);
                    }
                    InsightsDataManager.rateItem(kzItemId, selectedBu, RATE_POOR, new InsightsDataManager.ItemCallback() {
                        @Override
                        public void onSuccess() {
                        }

                        @Override
                        public void onFailure() {
                            int rating = 0;
                            if (isLikedOriginalState == 1) {
                                rating = RATE_EXCELLENT;
                                setIsLiked(RATE_EXCELLENT);
                            } else if (isLikedOriginalState == 0) {
                                rating = RATE_NONE;
                                setIsLiked(RATE_NONE);
                            } else if (isLikedOriginalState == 2) {
                                rating = RATE_POOR;
                                setIsLiked(RATE_POOR);
                            }

                            if (mEventListener != null) {
                                mEventListener.onLikePressed(rating);
                            }
                            HPUIUtils.displayToastWithCenteredText(PrintOSApplication.getAppContext(), PrintOSApplication.getAppContext().getString(R.string.failed_to_dislike_item));
                        }
                    });
                }
            }
        });

        shareButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                DeepLinkUtils.getShareBody(getContext(), Constants.UNIVERSAL_LINK_SCREEN_INSIGHTS_ITEM_DETAILS, null, null, kzItemId, true, new DeepLinkUtils.BranchIOCallback() {
                    @Override
                    public void onLinkCreated(String link) {
                        mEventListener.onSharePressed(kzItemName, link);
                    }
                });
            }
        });
    }

    public interface UserInteractionsEventListener {
        void onFavoritePressed(boolean b);

        void onLikePressed(int i);

        void onDislikePressed(int i);

        void onSharePressed(String name, String link);
    }

    private UserInteractionsEventListener mEventListener;

    public void setEventListener(UserInteractionsEventListener mEventListener) {
        this.mEventListener = mEventListener;
    }
}
