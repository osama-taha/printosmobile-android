package com.hp.printosmobile.presentation.modules.todayhistogram;

import android.app.Activity;
import android.content.Intent;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.OrientationEventListener;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.TextView;

import com.hp.printosmobile.Analytics;
import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.presentation.BaseActivity;
import com.hp.printosmobile.presentation.modules.home.ActiveShiftsViewModel;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitViewModel;
import com.hp.printosmobile.presentation.modules.today.TodayViewModel;
import com.hp.printosmobile.presentation.modules.today.histogrambreakdown.HistogramBreakDownRootFragment;
import com.hp.printosmobile.presentation.modules.today.histogrambreakdown.HistogramBreakDownRootFragment.HistogramResolution;
import com.hp.printosmobile.utils.AnalyticsUtils;
import com.hp.printosmobile.utils.HPLocaleUtils;
import com.hp.printosmobilelib.core.communications.remote.APIException;
import com.hp.printosmobilelib.core.logging.HPLogger;

import butterknife.Bind;
import butterknife.ButterKnife;
import io.fabric.sdk.android.services.common.TimingMetric;

/**
 * created by Anwar Asbah 3/14/2017
 */
public class HistogramDetailsActivity extends BaseActivity implements HistogramPresenterView {

    public static final String TAG = HistogramDetailsActivity.class.getName();

    public static final String HISTOGRAM_VIEW_MODEL_PARAM = "view_model_param";
    private static final String TODAY_MODEL_PARAM = "today_model_param";
    private static final String BU_MODEL_PARAM = "bu_model_param";
    private static final String IS_SHIFT_PARAM = "is_shift_param";
    private static final String SHIFT_MODEL_PARAM = "shift_model_param";
    private static final String BREAKDOWN_TYPE_PARAM = "breakdown_type_param";
    private static final String TIME_RESOLUTION_PARAM = "time_resolution_param";

    private static final int ANGLE_TOP_LEFT = 30;
    private static final int ANGLE_BOTTOM_LEFT = 150;
    private static final int ANGLE_BOTTOM_RIGHT = 210;
    private static final int ANGLE_TOP_RIGHT = 330;

    @Bind(R.id.activity_histogram_details)
    View activityLayout;
    @Bind(R.id.loading_view)
    View loadingView;
    @Bind(R.id.error_msg_text_view)
    TextView errorMsgTextView;

    private OrientationEventListener orientationEventListener;
    private TodayHistogramViewModel histogramViewModel;
    private BusinessUnitViewModel businessUnitViewModel;
    private TodayViewModel todayViewModel;
    private ActiveShiftsViewModel activeShiftsViewModel;
    private TodayHistogramViewModel.DetailTypeEnum breakdownType = null;
    private boolean isShift;
    boolean wasPhoneRotate = false;
    private String histogramResolution = null;
    private TodayHistogramPresenter presenter;

    public static void startActivityForResult(Activity activity, TodayHistogramViewModel viewModel,
                                              TodayViewModel todayViewModel,
                                              ActiveShiftsViewModel activeShiftsViewModel,
                                              BusinessUnitViewModel businessUnitViewModel,
                                              boolean isShift, int flag) {
        Intent intent = new Intent(activity, HistogramDetailsActivity.class);

        Bundle bundle = new Bundle();
        if (viewModel != null) {
            bundle.putSerializable(HISTOGRAM_VIEW_MODEL_PARAM, viewModel);
        }
        if (businessUnitViewModel != null) {
            bundle.putSerializable(BU_MODEL_PARAM, businessUnitViewModel);
        }
        if (todayViewModel != null) {
            bundle.putSerializable(TODAY_MODEL_PARAM, todayViewModel);
        }
        if (activeShiftsViewModel != null) {
            bundle.putSerializable(SHIFT_MODEL_PARAM, activeShiftsViewModel);
        }
        bundle.putBoolean(IS_SHIFT_PARAM, isShift);
        intent.putExtras(bundle);

        activity.startActivityForResult(intent, flag);
    }

    public static void startActivity(Activity activity, TodayHistogramViewModel viewModel,
                                     TodayHistogramViewModel.DetailTypeEnum typeEnum,
                                     String histogramResolution) {
        Intent intent = new Intent(activity, HistogramDetailsActivity.class);

        Bundle bundle = new Bundle();
        if (viewModel != null) {
            bundle.putSerializable(HISTOGRAM_VIEW_MODEL_PARAM, viewModel);
        }
        if(typeEnum != null) {
            bundle.putInt(BREAKDOWN_TYPE_PARAM, typeEnum.ordinal());
        }
        if(histogramResolution != null) {
            bundle.putSerializable(TIME_RESOLUTION_PARAM, histogramResolution);
        }
        intent.putExtras(bundle);

        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_histogram_details);
        ButterKnife.bind(this);

        Bundle args = getIntent().getExtras();
        if (args != null) {
            if (args.containsKey(HISTOGRAM_VIEW_MODEL_PARAM)) {
                histogramViewModel = (TodayHistogramViewModel) args.get(HISTOGRAM_VIEW_MODEL_PARAM);
            }
            if (args.containsKey(BU_MODEL_PARAM)) {
                businessUnitViewModel = (BusinessUnitViewModel) args.get(BU_MODEL_PARAM);
            }
            if (args.containsKey(IS_SHIFT_PARAM)) {
                isShift = args.getBoolean(IS_SHIFT_PARAM);
            }
            if (args.containsKey(TODAY_MODEL_PARAM)) {
                todayViewModel = (TodayViewModel) args.getSerializable(TODAY_MODEL_PARAM);
            }
            if (args.containsKey(SHIFT_MODEL_PARAM)) {
                activeShiftsViewModel = (ActiveShiftsViewModel) args.getSerializable(SHIFT_MODEL_PARAM);
            }
            if(args.containsKey(BREAKDOWN_TYPE_PARAM)) {
                breakdownType = TodayHistogramViewModel.DetailTypeEnum.values()[args.getInt(BREAKDOWN_TYPE_PARAM)];
            }
            if(args.containsKey(TIME_RESOLUTION_PARAM)) {
                histogramResolution = args.getString(TIME_RESOLUTION_PARAM);
            }

            Analytics.sendEvent(Analytics.VIEW_SCREEN_EVENT, breakdownType == null ?
                    Analytics.LANDSCAPE_HISTOGRAM_SCREEN_LABEL : Analytics.LANDSCAPE_HISTOGRAM_BREAKDOWN_SCREEN_LABEL);
        }

        boolean isGraphEnable = PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).isHistogramGraphEnabled();

        if (isGraphEnable) {
            AnalyticsUtils.sendEvents(this, Analytics.HISTOGRAM_LANDSCAPE_SHOW_EVENT);
        } else {
            AnalyticsUtils.sendEvents(this, Analytics.VIEW_LANDSCAPE_HISTOGRAM_ACTION);
        }

        HPLogger.d(TAG, "onCreate");

        activityLayout.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                activityLayout.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                init();
            }
        });

        if (histogramViewModel != null) {
            displayModel();
        } else {
            if (businessUnitViewModel != null) {
                presenter = new TodayHistogramPresenter();
                presenter.attachView(this);
                loadingView.setVisibility(View.VISIBLE);

                if (isGraphEnable && !isShift) {
                    presenter.getActualVsTargetGraphData(businessUnitViewModel);
                } else {
                    presenter.getActualVsTargetData(this, businessUnitViewModel, isShift);
                }
            }
        }
    }

    private void displayModel() {
        loadingView.setVisibility(View.GONE);

        if (histogramViewModel == null) {
            HPLogger.d(TAG, "last 7 days data are not available.");
            return;
        }

        HistogramDetailsFragment histogramDetails = HistogramDetailsFragment.newInstance(histogramViewModel, breakdownType, histogramResolution);

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.activity_histogram_details, histogramDetails);
        transaction.commit();
    }

    private void init() {
        orientationEventListener = new OrientationEventListener(this,
                SensorManager.SENSOR_DELAY_NORMAL) {

            @Override
            public void onOrientationChanged(int orientation) {
                if ((orientation > 0 && orientation < ANGLE_TOP_LEFT)
                        || (orientation > ANGLE_BOTTOM_LEFT && orientation < ANGLE_BOTTOM_RIGHT)
                        || orientation > ANGLE_TOP_RIGHT) {
                    if (wasPhoneRotate) {
                        HPLogger.d(TAG, "onOrientationChanged.");
                        enableOrientationEventListener(false);
                        onBackPressed();
                    }
                } else if (orientation > 0) {
                    wasPhoneRotate = true;
                }
            }
        };
        enableOrientationEventListener(true);
    }

    @Override
    protected void onResume() {
        super.onResume();
        enableOrientationEventListener(true);
    }

    @Override
    protected void onPause() {
        enableOrientationEventListener(false);
        super.onPause();
        onBackPressed();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        enableOrientationEventListener(false);
    }

    @Override
    public void onBackPressed() {
        enableOrientationEventListener(false);
        try {
            super.onBackPressed();
        } catch (Exception e) {
            HPLogger.e(TAG, "landscape histogram on back pressed error ", e);
        }
    }

    private void enableOrientationEventListener(boolean enable) {
        if (orientationEventListener != null) {
            HPLogger.d(TAG, "enableOrientationEventListener(" + enable + ")");
            if (enable) {
                orientationEventListener.enable();
            } else {
                orientationEventListener.disable();
            }
        }
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_histogram_details;
    }

    @Override
    public void updateHistogram(TodayHistogramViewModel actualVsTargetViewModel) {
        histogramViewModel = actualVsTargetViewModel;
        if (todayViewModel != null) {
            if (histogramViewModel != null) {
                HistogramUtility.appendTodayDataToHistogramData(histogramViewModel, todayViewModel, activeShiftsViewModel);
            }
            Intent intent = new Intent();
            Bundle bundle = new Bundle();
            bundle.putSerializable(HISTOGRAM_VIEW_MODEL_PARAM, histogramViewModel);
            intent.putExtras(bundle);
            setResult(RESULT_OK, intent);
        }
        displayModel();
    }

    @Override
    public void onRequestError(APIException exception) {
        errorMsgTextView.setText(HPLocaleUtils.getSimpleErrorMsg(this, exception));
        loadingView.setVisibility(View.GONE);
    }
}
