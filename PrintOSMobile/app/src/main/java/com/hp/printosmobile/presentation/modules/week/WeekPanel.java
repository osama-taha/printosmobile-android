package com.hp.printosmobile.presentation.modules.week;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.util.AttributeSet;
import android.view.View;

import com.hp.printosmobile.Analytics;
import com.hp.printosmobile.AnswersSdk;
import com.hp.printosmobile.Constants;
import com.hp.printosmobile.R;
import com.hp.printosmobile.presentation.modules.home.HomePresenter;
import com.hp.printosmobile.presentation.modules.home.Panel;
import com.hp.printosmobile.presentation.modules.kpiview.KPIViewModel;
import com.hp.printosmobile.presentation.modules.reportkpibreakdown.KpiBreakdownEnum;
import com.hp.printosmobile.presentation.modules.shared.DeepLinkUtils;
import com.hp.printosmobile.presentation.modules.shared.PanelShareCallbacks;
import com.hp.printosmobile.utils.FileUtils;
import com.hp.printosmobilelib.ui.widgets.panel.PanelView;
import com.hp.printosmobilelib.ui.widgets.panel.SharableObject;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Anwar Asbah on 4/12/2016.
 */
public class WeekPanel extends PanelView<WeekCollectionViewModel> implements SharableObject, WeekAdapter.WeekViewCallback {

    public static final String TAG = WeekPanel.class.getSimpleName();

    @Bind(R.id.week_pager)
    ViewPager weekPager;
    @Bind(R.id.week_pager_indicator)
    CirclePageIndicator weekPagerIndicator;

    private WeekAdapter weekAdapter;
    private WeekPanelCallbacks callback;

    public WeekPanel(Context context) {
        super(context);
    }

    public WeekPanel(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public WeekPanel(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void bindViews() {
        ButterKnife.bind(this, getView());

        weekPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            boolean stateChange = false;

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                if (!stateChange) {
                    return;
                }

                stateChange = !stateChange;
                PagerAdapter pagerAdapter = weekPager.getAdapter();
                if (pagerAdapter != null) {
                    int numOfWeeks = weekAdapter.getCount();
                    int diff = numOfWeeks - position - 1;
                    Analytics.sendEvent(Analytics.WEEK_VIEW_ACTION,
                            String.format(Analytics.WEEK_VIEW_SHOW_LABEL, diff));
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                stateChange = true;
            }
        });

        weekPager.setOffscreenPageLimit(5);

    }

    @Override
    protected boolean hasSharingButton() {
        super.hasSharingButton();
        return true;
    }

    @Override
    protected void onShareClicked() {
        super.onShareClicked();
        if (callback != null) {
            callback.onShareButtonClicked(Panel.PERFORMANCE);
        }
    }

    @Override
    public void sharePanel() {

        if (callback != null && weekAdapter != null && weekAdapter.getCount() > 0) {

            int index = weekPager.getCurrentItem();
            View weekView = weekPager.findViewWithTag(weekPager.getCurrentItem());
            final WeekViewModel viewModel = getViewModel().getWeeks() == null ? null : getViewModel().getWeeks().get(index);

            if (weekView == null || viewModel == null) {
                return;
            }

            lockShareButton(true);

            weekView.setBackgroundColor(ResourcesCompat.getColor(getResources(), R.color.panel_view_bg_color, null));

            Bitmap weekPanelBitmap = FileUtils.getScreenShot(weekView);

            weekView.setBackgroundColor(ResourcesCompat.getColor(getResources(), android.R.color.transparent, null));

            if (weekPanelBitmap != null) {
                final Uri storageUri = FileUtils.store(getContext(), weekPanelBitmap, PANEL_SCREEN_SHOT_FILE_NAME);

                DeepLinkUtils.getShareBody(getContext(),
                        Constants.UNIVERSAL_LINK_SCREEN_HOME,
                        getPanelTag(),
                        null, null, false, new DeepLinkUtils.BranchIOCallback() {
                            @Override
                            public void onLinkCreated(String link) {
                                callback.onScreenshotCreated(Panel.PERFORMANCE, storageUri,
                                        getContext().getString(R.string.share_weekly_panel_title, viewModel.getWeekScore(), viewModel.getWeekNumber()),
                                        link,
                                        WeekPanel.this);
                                lockShareButton(false);
                            }
                        });
            }
        }
    }

    @Override
    public Spannable getTitleSpannable() {
        return new SpannableStringBuilder(getContext().getString(R.string.week_panel_title));
    }

    @Override
    public int getContentView() {
        return R.layout.week_content;
    }

    @Override
    public void updateViewModel(WeekCollectionViewModel viewModel) {

        setViewModel(viewModel);

        if (showEmptyCard()) {
            DeepLinkUtils.respondToIntentForPanel(this, callback, true);
            return;
        }

        weekAdapter = new WeekAdapter(getContext(), viewModel, this);
        weekPager.setAdapter(weekAdapter);
        weekPagerIndicator.setViewPager(weekPager);

        weekPagerIndicator.setVisibility(GONE);
        if (weekAdapter.getCount() > 1) {
            weekPagerIndicator.setCurrentItem(weekAdapter.getCount() - 1);
            weekPagerIndicator.setVisibility(VISIBLE);
        }

        DeepLinkUtils.respondToIntentForPanel(this, callback, false);
    }

    @Override
    protected boolean isValidViewModel() {
        return !(getViewModel() == null || getViewModel().getWeeks() == null || getViewModel().getWeeks().isEmpty());
    }

    @Override
    protected String getEmptyCardText() {
        return getContext().getString(R.string.no_information_to_display);
    }

    public void addWeekPanelListener(WeekPanelCallbacks callback) {
        this.callback = callback;

    }

    @Override
    public void onKPISelected(String kpiName) {
        if (callback != null) {
            callback.onKpiSelected(kpiName);
        }
    }

    @Override
    public void onWeekSelected() {
        if (callback != null) {
            Analytics.sendEvent(Analytics.CHART_CLICKED_ACTION);

            callback.onWeekPanelClicked();
        }
    }

    @Override
    public void onPartialDataWarningClicked(List<WeekViewModel.WeekPressStatus> weekPressStatuses, boolean isLowPrintVolume) {
        if (callback != null) {
            callback.onPartialDataWeekClicked(weekPressStatuses, isLowPrintVolume);
        }
    }

    @Override
    public void showKpiExplanationDialog(List<KPIViewModel> kpiList, int position) {
        if (callback != null) {
            callback.showKpiExplanationDialog(kpiList, position);
        }
    }

    @Override
    public String getPanelTag() {
        return Panel.PERFORMANCE.getDeepLinkTag();
    }

    @Override
    public String getShareAction() {
        return AnswersSdk.SHARE_CONTENT_TYPE_WEEKLY_PB_SCORE;
    }

    public void openKpiBreakdown(String kpiName) {
        if (callback != null) {
            KpiBreakdownEnum kpiBreakdownEnum = KpiBreakdownEnum.from(kpiName, HomePresenter.getSelectedBusinessUnit() == null ? null :
                    HomePresenter.getSelectedBusinessUnit().getBusinessUnit()); //to avoid lower case issue
            callback.onKpiSelected(kpiBreakdownEnum.getKey());
        }
    }

    public void openFirstKpi() {
        if (callback != null) {
            KpiBreakdownEnum kpiBreakdownEnum = KpiBreakdownEnum.getFirst(HomePresenter.getSelectedBusinessUnit() == null ?
                    null : HomePresenter.getSelectedBusinessUnit().getBusinessUnit());
            callback.onKpiSelected(kpiBreakdownEnum.getKey());
        }
    }

    public void openOverAll() {
        if (callback != null) {
            callback.onWeekPanelClicked();
        }
    }

    @Override
    public View getCurrentView() {
        if (weekPager != null) {
            return weekPager.findViewWithTag(weekPager.getCurrentItem());
        }
        return super.getCurrentView();
    }

    public interface WeekPanelCallbacks extends PanelShareCallbacks, DeepLinkUtils.DeepLinkCallbacks {

        void onKpiSelected(String kpiName);

        void onWeekPanelClicked();

        void onPartialDataWeekClicked(List<WeekViewModel.WeekPressStatus> weekPressStatuses, boolean isLowPrintVolume);

        void showKpiExplanationDialog(List<KPIViewModel> kpiList, int position);

    }
}
