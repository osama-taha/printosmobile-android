package com.hp.printosmobile.presentation.modules.splash;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;

import com.hp.printosmobile.Analytics;
import com.hp.printosmobile.AppseeSdk;
import com.hp.printosmobile.BuildConfig;
import com.hp.printosmobile.Constants;
import com.hp.printosmobile.IntercomSdk;
import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.presentation.BaseActivity;
import com.hp.printosmobile.presentation.Navigator;
import com.hp.printosmobile.presentation.modules.main.MainViewPresenter;
import com.hp.printosmobile.presentation.modules.shared.DeepLinkUtils;
import com.hp.printosmobile.utils.UniversalLinksUtils;
import com.hp.printosmobilelib.core.communications.remote.SessionManager;
import com.hp.printosmobilelib.core.logging.HPLogConfig;
import com.hp.printosmobilelib.core.logging.HPLogger;

import butterknife.Bind;
import io.branch.indexing.BranchUniversalObject;
import io.branch.referral.Branch;
import io.branch.referral.BranchError;
import io.branch.referral.util.LinkProperties;

public class SplashActivity extends BaseActivity {

    private static final String TAG = SplashActivity.class.getName();

    private static final long DEMO_SESSION_TIMEOUT_IN_MILLS = 10 * 60 * 1000L;

    @Bind(R.id.beta_msg_text_view)
    View betaMsgTextView;

    private Bundle deepLinkBundle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Analytics.sendEvent(Analytics.EVENT_OPEN_APP_FROM_ICON);
        HPLogConfig.getInstance(this).setDispatchingEnabled(false);

        betaMsgTextView.setVisibility(BuildConfig.isBetaVersion ? View.VISIBLE : View.GONE);

    }

    @Override
    protected void onStart() {
        super.onStart();

        Branch branch = Branch.getInstance();
        PrintOSApplication.setStartTimeMilliSeconds(false);

        if (getIntent() == null || getIntent().getData() == null) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    dismissSplash(isLogInNeeded());
                }
            }, 300);
            return;
        }
        branch.initSession(new Branch.BranchUniversalReferralInitListener() {
            @Override
            public void onInitFinished(BranchUniversalObject branchUniversalObject, LinkProperties linkProperties, BranchError error) {
                if (error == null && branchUniversalObject != null) {
                    HPLogger.i(TAG, "Referring Branch Universal Object: " + branchUniversalObject.toString());
                    deepLinkBundle = DeepLinkUtils.getBundle(branchUniversalObject);

                } else {
                    Intent appLinkIntent = getIntent();
                    if (appLinkIntent != null) {
                        String link = appLinkIntent.getDataString();

                        deepLinkBundle = UniversalLinksUtils.getBundle(SplashActivity.this, link);
                    }
                }

                dismissSplash(isLogInNeeded());

            }
        }, this.getIntent().getData(), this);

    }

    @Override
    public void onNewIntent(Intent intent) {
        this.setIntent(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();

        AppseeSdk.getInstance(getApplicationContext()).init();

    }

    boolean isLogInNeeded() {
        //TODO: Add presenter, then move the below logic..
        SessionManager sessionManager = SessionManager.getInstance(PrintOSApplication.getAppContext());
        return !sessionManager.hasCookie();
    }

    private void dismissSplash(boolean loginNeeded) {

        if (!isForceUpdating()) {

            HPLogger.d(TAG, "Dismissing Splash Activity");

            IntercomSdk.getInstance(getApplicationContext()).login();

            if (loginNeeded) {
                Navigator.openLoginActivity(this);
            } else {

                //stop the demo mode.
                if (PrintOSPreferences.getInstance(this).isInDemoMode()) {
                    long demoAccountTimestamp = PrintOSPreferences.getInstance(this).getDemoAccountTimestamp();
                    long demoAccountTimeDiff = System.currentTimeMillis() - demoAccountTimestamp;
                    if (demoAccountTimeDiff >= DEMO_SESSION_TIMEOUT_IN_MILLS) {
                        finishDemoMode();
                        return;
                    }
                }

                // We’re logged in, we can register the user with Intercom
                Navigator.openMainActivity(this, deepLinkBundle);

                PrintOSPreferences preferences = PrintOSPreferences.getInstance(this);
                preferences.incrementAppLaunchesCountForWizard();

                if (preferences.isProductionStack()) {
                    int sessionCount = PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).getSessionCount();
                    PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).setSessionCount(sessionCount + 1);
                }
            }

            finish();

        }

    }

    private void finishDemoMode() {

        MainViewPresenter presenter = new MainViewPresenter();
        startRegistrationService(Constants.IntentExtras.NOTIFICATION_UNREGISTER_AND_CLEAR_CACHE_INTENT_ACTION);
        presenter.logout(this);

        Navigator.openLoginActivity(this);
        finish();

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_splash;
    }

    @Override
    protected void onPause() {
        super.onPause();
        //to mimic returning from background
        setReturningFromBackgroundValue(true);
    }

    private static synchronized void setReturningFromBackgroundValue(boolean returningFromBackgroundValue) {
        setReturningFromBackground(returningFromBackgroundValue);
    }
}
