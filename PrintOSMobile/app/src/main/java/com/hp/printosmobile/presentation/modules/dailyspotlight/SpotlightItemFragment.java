package com.hp.printosmobile.presentation.modules.dailyspotlight;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.hp.printosmobile.R;
import com.hp.printosmobile.presentation.BaseFragment;
import com.hp.printosmobile.presentation.modules.home.HomePresenter;
import com.hp.printosmobile.presentation.modules.reportkpibreakdown.KpiBreakdownActivity;
import com.hp.printosmobile.presentation.modules.reportkpibreakdown.KpiBreakdownEnum;
import com.hp.printosmobile.presentation.modules.reportkpibreakdown.KpiBreakdownPresenter;
import com.hp.printosmobile.presentation.modules.reportkpibreakdown.KpiBreakdownView;
import com.hp.printosmobile.presentation.modules.reportkpibreakdown.KpiBreakdownViewModel;
import com.hp.printosmobile.presentation.modules.reports.ReportFilter;
import com.hp.printosmobile.presentation.modules.reports.ReportKpiViewModel;
import com.hp.printosmobilelib.core.communications.remote.APIException;

import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * @author Anwar Asbah
 */
public class SpotlightItemFragment extends BaseFragment implements KpiBreakdownView {

    private static final String TAG = SpotlightItemFragment.class.getName();
    private static final String ENUM_TAG = "advices";
    private static final String SHARE_TITLE_FORMAT = "%s %s %s";
    private static final long LOAD_DATA_DELAY = 300;
    private static final String UNIT_FORMAT = " (%s)";

    @Bind(R.id.title_text_view)
    TextView titleTextView;
    @Bind(R.id.value_text_view)
    TextView valueTextView;
    @Bind(R.id.description_text_view)
    TextView descriptionTextView;
    @Bind(R.id.loading_View)
    View loadingView;
    @Bind(R.id.kpi_image_view)
    ImageView kpiImageView;
    @Bind(R.id.error_msg_text_view)
    View errorMsg;
    @Bind(R.id.item_content)
    View itemContent;

    KpiBreakdownPresenter kpiBreakdownPresenter;
    DailySpotlightFragmentCallback callback;

    private DailySpotlightEnum dailySpotlightEnum = DailySpotlightEnum.UNKNOWN;
    private List<KpiBreakdownViewModel> viewModels = null;

    public static SpotlightItemFragment newInstance(DailySpotlightEnum dailySpotlightEnum, DailySpotlightFragmentCallback callback) {

        SpotlightItemFragment devicesFragment = new SpotlightItemFragment();
        devicesFragment.callback = callback;
        Bundle args = new Bundle();
        args.putSerializable(ENUM_TAG, dailySpotlightEnum.ordinal());
        devicesFragment.setArguments(args);
        return devicesFragment;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Bundle bundle = getArguments();
        dailySpotlightEnum = DailySpotlightEnum.values()[bundle.getInt(ENUM_TAG)];
    }

    public void loadData() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                getData();
            }
        }, LOAD_DATA_DELAY);

    }

    private void getData() {
        if (hasData() || HomePresenter.getSelectedBusinessUnit() == null) {
            return;
        }

        if (kpiBreakdownPresenter == null) {
            kpiBreakdownPresenter = new KpiBreakdownPresenter();
            kpiBreakdownPresenter.attachView(this);
        }

        ReportKpiViewModel reportKpiViewModel = new ReportKpiViewModel();
        reportKpiViewModel.setName(dailySpotlightEnum.getRootBreakdownEnum().getParentKpi().getKey());

        ReportFilter reportFilter = KpiBreakdownActivity.getReportFilter();
        reportFilter.setBusinessUnit(HomePresenter.getSelectedBusinessUnit());
        reportFilter.setKpi(reportKpiViewModel);

        showLoading();
        kpiBreakdownPresenter.getKpiData(reportFilter, true);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.panel_daily_spotlight_item;
    }

    public void hideLoading() {
        if (loadingView != null) {
            loadingView.setVisibility(View.INVISIBLE);
        }
    }

    public void showLoading() {
        if (loadingView != null) {
            loadingView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public boolean isFragmentNameToBeDisplayed() {
        return false;
    }

    @Override
    public boolean isFilteringAvailable() {
        return false;
    }

    @Override
    public boolean isIntercomAccessible() {
        return false;
    }

    @Override
    public int getToolbarDisplayName() {
        return R.string.unknown_value;
    }

    @Override
    public boolean isSearchAvailable() {
        return false;
    }

    @Override
    public void onCancel() {

    }

    public void displayView() {
        if (!hasData()) {
            return;
        }
        displayModel();
    }

    @Override
    public void displayKpiBreakdown(List<KpiBreakdownViewModel> viewModels) {
        if (!isAdded() || getActivity() == null || getContext() == null) {
            return;
        }
        hideLoading();

        this.viewModels = viewModels;
        displayModel();

        if (callback != null) {
            callback.onDataRetrieved();
        }
    }

    private void displayModel() {
        if (!isAdded() || getActivity() == null || getContext() == null) {
            return;
        }

        if (viewModels == null) {
            itemContent.setVisibility(View.INVISIBLE);
            errorMsg.setVisibility(View.VISIBLE);
            return;
        }

        itemContent.setVisibility(View.VISIBLE);
        errorMsg.setVisibility(View.INVISIBLE);

        KpiBreakdownViewModel impModel = null;
        if (dailySpotlightEnum == DailySpotlightEnum.IND_PRINT_VOLUME_EPM) {
            for (KpiBreakdownViewModel impVModel : viewModels) {
                if (impVModel.getKpiBreakdownEnum() == KpiBreakdownEnum.INDIGO_IMPRESSIONS) {
                    impModel = impVModel;
                    break;
                }
            }
        }

        double value = 0;
        int nonZeroWeeks = 0;
        String unit = null;
        for (KpiBreakdownViewModel vModel : viewModels) {
            if (vModel.getKpiBreakdownEnum() == dailySpotlightEnum.getRootBreakdownEnum()) {
                if(dailySpotlightEnum == DailySpotlightEnum.IND_PRINT_VOLUME_EPM ||
                        dailySpotlightEnum == DailySpotlightEnum.IND_PRINT_VOLUME_IMP) {
                    if (vModel.getMinimumPaperType() != null) {
                        unit = String.format(UNIT_FORMAT, vModel.getMinimumPaperType());
                    } else {
                        unit = "";
                    }
                }
                if (dailySpotlightEnum == DailySpotlightEnum.IND_PRINT_VOLUME_EPM) {
                    if (vModel.getEventList() != null && vModel.getEventList().size() > 0 &&
                            impModel != null && impModel.getEventList() != null && impModel.getEventList().size() > 0
                            && impModel.getEventList().size() >= vModel.getEventList().size()) {
                        for (int i = vModel.getEventList().size() - 1; i >= 0; i--) {
                            float impValue = impModel.getEventList().get(i).getDayValue();
                            float epmValue = vModel.getEventList().get(i).getDayValue();
                            value += (impValue == 0 ? 0 : epmValue / impValue)*100;
                        }

                        if (dailySpotlightEnum.isAverage()) {
                            value = value / vModel.getEventList().size();
                        }
                    }
                } else if (vModel.getEventList() != null && vModel.getEventList().size() > 0) {
                    for (KpiBreakdownViewModel.Event event : vModel.getEventList()) {
                        value += event.getDayValue();
                        nonZeroWeeks += event.getDayValue() == 0 ? 0 : 1;
                    }
                    if (dailySpotlightEnum.isAverage()) {
                        if(dailySpotlightEnum == DailySpotlightEnum.IND_SUPPLIES_PIP ||
                                dailySpotlightEnum == DailySpotlightEnum.IND_SUPPLIES_BLANKET) {
                            value = value / nonZeroWeeks;
                        } else {
                            value = value / vModel.getEventList().size();
                        }
                    }
                }
            }
        }

        if (dailySpotlightEnum.shouldRound()) {
            value = Math.round(value);
        }

        String descriptionExtra = dailySpotlightEnum != DailySpotlightEnum.IND_PRINT_VOLUME_IMP ?
                null : "";

        titleTextView.setText(dailySpotlightEnum.getTitle(getContext()));
        valueTextView.setText(dailySpotlightEnum.getFormattedValue((float) value));
        descriptionTextView.setText(dailySpotlightEnum.getDescription(getContext(), descriptionExtra, unit));
        kpiImageView.setImageDrawable(dailySpotlightEnum.getDrawable(getContext()));
    }

    @Override
    public void onError(APIException exception, String tag) {
        if(!isAdded() || getContext() == null || getActivity() == null) {
            return;
        }
        hideLoading();
        errorMsg.setVisibility(View.VISIBLE);
        itemContent.setVisibility(View.INVISIBLE);
    }

    @OnClick(R.id.spotlight_content)
    public void onItemClicked() {
        if (callback != null && hasData()) {
            callback.onItemClicked(dailySpotlightEnum);
        }
    }

    public boolean hasData() {
        return viewModels != null;
    }

    public DailySpotlightEnum getDailySpotlightEnum() {
        return dailySpotlightEnum == null ? DailySpotlightEnum.UNKNOWN : dailySpotlightEnum;
    }

    public String getShareSubject() {
        if (titleTextView == null || valueTextView == null || descriptionTextView == null) {
            return "";
        }
        return String.format(SHARE_TITLE_FORMAT,
                titleTextView.getText().toString(),
                valueTextView.getText().toString(),
                descriptionTextView.getText().toString());
    }

    public void clear() {
        viewModels = null;
        if (itemContent != null && errorMsg != null) {
            itemContent.setVisibility(View.INVISIBLE);
            errorMsg.setVisibility(View.INVISIBLE);
        }
    }

    public interface DailySpotlightFragmentCallback {
        void onItemClicked(DailySpotlightEnum dailySpotlightEnum);

        void onDataRetrieved();
    }
}
