package com.hp.printosmobile.presentation;

import com.fasterxml.jackson.core.type.TypeReference;
import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.data.remote.ApiServicesProvider;
import com.hp.printosmobile.data.remote.cache.ApiCacheUtils;
import com.hp.printosmobile.data.remote.models.PermissionsData;
import com.hp.printosmobile.data.remote.models.RankingLeaderboardStatus;
import com.hp.printosmobile.data.remote.services.MetaDataService;

import java.util.ArrayList;

import retrofit2.Response;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by Osama Taha
 */
public class PermissionsManager {

    private static final String PERMISSIONS_DATA_CACHE_PREF_KEY = "PERMISSIONS_DATA_CACHE_PREF_KEY";

    private static PermissionsManager instance = new PermissionsManager();

    private PermissionsData permissionsData;
    private ArrayList<Permission> permissionsList;

    private PermissionsManager() {
        //private constructor.
    }

    public static PermissionsManager getInstance() {
        return instance;
    }

    public Observable<Response<PermissionsData>> getPermissionsData(boolean refreshCache) {

        if (refreshCache) {
            clearCache();
        }

        ApiServicesProvider serviceProvider = PrintOSApplication.getApiServicesProvider();
        MetaDataService metaDataService = serviceProvider.getMetaDataService();

        return ApiCacheUtils.getCacheAndUpdate(metaDataService.getPermissions(),
                PERMISSIONS_DATA_CACHE_PREF_KEY, new TypeReference<PermissionsData>() {
                }).map(new Func1<Response<PermissionsData>, Response<PermissionsData>>() {
            @Override
            public Response<PermissionsData> call(Response<PermissionsData> permissionsDataResponse) {
                if (permissionsDataResponse.isSuccessful()) {
                    permissionsData = permissionsDataResponse.body();
                    readPermissions();
                } else {
                    permissionsData = null;
                }

                return permissionsDataResponse;
            }
        }).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io());
    }

    private void readPermissions() {

        permissionsList = new ArrayList<>();

        for (PermissionsData.PermissionData permissionData : permissionsData.getPermissionsData()) {
            Permission permission = Permission.from(permissionData.getName());
            if (permission != Permission.UNKNOWN) {
                permissionsList.add(permission);
            }
        }
    }

    public boolean hasPermissionData() {
        return permissionsData != null;
    }

    public RankingLeaderboardStatus getRankingLeaderboardStatus() {
        return hasPermissionData() ? permissionsData.getRankingLeaderboardStatus() : RankingLeaderboardStatus.DISABLED;
    }

    public boolean hasPermission(Permission permission) {
        return hasPermissionData() && permissionsList.contains(permission);
    }

    public void clearCache() {
        ApiCacheUtils.clearApiCache(PERMISSIONS_DATA_CACHE_PREF_KEY);
    }

    public void clear() {
        permissionsList = null;
        permissionsData = null;
        clearCache();
    }

    public boolean isPermissionLoaded() {
        return permissionsList != null && permissionsData != null;
    }

    public enum Permission {

        DELETE("Delete"),
        RETRIEVE("Retrieve"),
        CLICK_REPORT("ClickReport"),
        REPORT_GENERATION("ReportGeneration"),
        CREATE("Create"),
        UPDATE("Update"),
        COLOR_BEAT_API("colorbeatAPI"),
        UNKNOWN("");

        private final String name;

        Permission(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public static Permission from(String name) {

            if (name == null) {
                return UNKNOWN;
            }

            for (Permission permission : Permission.values()) {
                if (permission.getName().equalsIgnoreCase(name)) {
                    return permission;
                }
            }

            return Permission.UNKNOWN;
        }
    }

}
