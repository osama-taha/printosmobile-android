package com.hp.printosmobile.presentation.modules.dailyspotlight;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.ViewGroup;

import com.hp.printosmobile.presentation.modules.filters.DeviceFilterViewModel;
import com.hp.printosmobile.presentation.modules.filters.FilterItem;
import com.hp.printosmobile.presentation.modules.filters.FiltersViewModel;
import com.hp.printosmobile.presentation.modules.home.HomePresenter;
import com.hp.printosmobile.presentation.modules.shared.MeasureTypeEnum;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * Created by Anwar Asbah on 5/22/2018.
 */

public class SpotlightPagerAdapter extends FragmentStatePagerAdapter implements SpotlightItemFragment.DailySpotlightFragmentCallback {

    private DailySpotlightEnum[] enumList;
    private Map<DailySpotlightEnum, Fragment> fragments;
    private DailySpotlightPagerAdapterCallback callback;

    public SpotlightPagerAdapter(FragmentManager fm, DailySpotlightPagerAdapterCallback callback) {
        super(fm);

        fragments = new HashMap<>();

        boolean isAllLength = true;
        if(HomePresenter.getSelectedBusinessUnit() != null) {
            FiltersViewModel filtersViewModel = HomePresenter.getSelectedBusinessUnit().getFiltersViewModel();
            if(filtersViewModel != null && filtersViewModel.getSelectedDevices() != null) {
                for (FilterItem filterItem : filtersViewModel.getSelectedDevices()) {
                    if(filterItem instanceof DeviceFilterViewModel) {
                        isAllLength = isAllLength &&
                                ((DeviceFilterViewModel) filterItem).getMeasureTypeEnum() == MeasureTypeEnum.LENGTH;
                    }
                }
            }
        }

        List<DailySpotlightEnum> list = new ArrayList<>();
        for (int i = 0; i < DailySpotlightEnum.values().length; i++) {
            if(DailySpotlightEnum.values()[i] != DailySpotlightEnum.UNKNOWN && !(
                    DailySpotlightEnum.values()[i] == DailySpotlightEnum.IND_TECH_JAMS && isAllLength
                    )){
                list.add(DailySpotlightEnum.values()[i]);
            }
        }

        enumList = new DailySpotlightEnum[list.size() - 1];  //skipping unknown
        enumList = list.toArray(enumList);
        shuffleArray(enumList);

        for (DailySpotlightEnum dailySpotlightEnum : enumList) {
            fragments.put(dailySpotlightEnum, SpotlightItemFragment.newInstance(dailySpotlightEnum, this));
        }

        this.callback = callback;
    }


    static void shuffleArray(DailySpotlightEnum[] array) {
        Random random = new Random();
        for (int i = array.length - 1; i > 0; i--) {

            int index = random.nextInt(i + 1);

            DailySpotlightEnum a = array[index];
            array[index] = array[i];
            array[i] = a;
        }
    }

    @Override
    public Fragment getItem(int i) {
        i = i % getCount();
        return fragments.get(enumList[i]);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Fragment fragment = (Fragment) super.instantiateItem(container, position);

        DailySpotlightEnum dailySpotlightEnum = enumList[position];
        if (fragments.containsKey(dailySpotlightEnum)) {
            fragments.remove(dailySpotlightEnum);
        }

        fragments.put(dailySpotlightEnum, fragment);
        // save the appropriate reference depending on position
        return fragment;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return String.valueOf(position);
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public int getCount() {
        return enumList.length;
    }

    @Override
    public void onItemClicked(DailySpotlightEnum dailySpotlightEnum) {
        if (callback != null) {
            callback.onItemClicked(dailySpotlightEnum);
        }
    }

    @Override
    public void onDataRetrieved() {
        if (callback != null) {
            callback.onDataRetrieved();
        }
    }

    public void clear() {
        if (fragments != null) {
            for (DailySpotlightEnum dailySpotlightEnum : fragments.keySet()) {
                Fragment fragment = fragments.get(dailySpotlightEnum);
                if (fragment instanceof SpotlightItemFragment) {
                    ((SpotlightItemFragment) fragment).clear();
                }
            }
        }
        notifyDataSetChanged();
    }

    public interface DailySpotlightPagerAdapterCallback {

        void onItemClicked(DailySpotlightEnum dailySpotlightEnum);

        void onDataRetrieved();
    }
}
