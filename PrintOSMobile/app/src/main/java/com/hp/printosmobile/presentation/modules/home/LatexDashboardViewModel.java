package com.hp.printosmobile.presentation.modules.home;

import com.hp.printosmobile.presentation.modules.printersnapshot.PrinterSnapshotViewModel;
import com.hp.printosmobile.presentation.modules.productionsnapshot.latex.LatexProductionSnapshotViewModel;

/**
 * Created by Anwar Asbah on 9/5/16.
 */
public class LatexDashboardViewModel {

    private PrinterSnapshotViewModel printerSnapshotViewModel;
    private LatexProductionSnapshotViewModel latexProductionSnapshotViewModel;

    public PrinterSnapshotViewModel getPrinterSnapshotViewModel() {
        return printerSnapshotViewModel;
    }

    public void setPrinterSnapshotViewModel(PrinterSnapshotViewModel printerSnapshotViewModel) {
        this.printerSnapshotViewModel = printerSnapshotViewModel;
    }

    public LatexProductionSnapshotViewModel getLatexProductionSnapshotViewModel() {
        return latexProductionSnapshotViewModel;
    }

    public void setLatexProductionSnapshotViewModel(LatexProductionSnapshotViewModel latexProductionSnapshotViewModel) {
        this.latexProductionSnapshotViewModel = latexProductionSnapshotViewModel;
    }
}
