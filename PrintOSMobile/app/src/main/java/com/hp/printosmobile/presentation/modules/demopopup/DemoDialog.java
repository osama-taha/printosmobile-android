package com.hp.printosmobile.presentation.modules.demopopup;

import android.support.v4.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.hp.printosmobile.R;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobilelib.core.logging.HPLogger;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Minerva Halteh on 14/2/2018.
 */
public class DemoDialog extends DialogFragment {

    public static final String TAG = DemoDialog.class.getName();

    private DemoDialogCallback callback;

    public static DemoDialog getInstance(DemoDialogCallback callback) {
        DemoDialog demoDialog = new DemoDialog();
        demoDialog.callback = callback;
        return demoDialog;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        View rootView = inflater.inflate(R.layout.dialog_demo, container, false);
        ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setCancelable(false);
    }

    @OnClick(R.id.end_button)
    public void onThanksButtonClicked() {
        HPLogger.d(TAG, "end demo button clicked");

        dismissAllowingStateLoss();
        if (callback != null) {
            callback.onEndDemo();
        }
    }

    @OnClick(R.id.continue_button)
    public void onLaterButtonClicked() {
        HPLogger.d(TAG, "continue demo button clicked");
        PrintOSPreferences.getInstance(getActivity()).setDemoAccountTimestamp(System.currentTimeMillis());
        dismissAllowingStateLoss();

    }

    public interface DemoDialogCallback {
        void onEndDemo();
    }
}
