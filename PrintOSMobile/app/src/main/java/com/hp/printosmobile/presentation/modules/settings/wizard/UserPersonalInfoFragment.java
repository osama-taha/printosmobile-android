package com.hp.printosmobile.presentation.modules.settings.wizard;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.widget.NestedScrollView;
import android.text.TextUtils;
import android.view.View;

import com.hp.printosmobile.Analytics;
import com.hp.printosmobile.Constants;
import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;
import com.hp.printosmobile.presentation.BaseFragment;
import com.hp.printosmobile.utils.HPUIUtils;
import com.hp.printosmobile.utils.ImageLoadingUtils;
import com.hp.printosmobilelib.core.logging.HPLogger;
import com.hp.printosmobilelib.ui.widgets.HPEditText;
import com.hp.printosmobilelib.ui.widgets.HPTextView;

import butterknife.Bind;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Osama Taha
 */
public class UserPersonalInfoFragment extends BaseFragment implements IWizardFragment {

    public static final String TAG = UserPersonalInfoFragment.class.getName();

    private static final String KEY_DATA = "key_user_data";

    @Bind(R.id.description_text_view)
    HPTextView messageTextView;
    @Bind(R.id.user_info_first_name)
    HPEditText firstNameEditText;
    @Bind(R.id.user_info_last_name)
    HPEditText lastNameEditText;
    @Bind(R.id.profile_image)
    CircleImageView profileImage;

    private WizardViewModel wizardViewModel;
    private UserPersonalInfoFragmentsCallbacks callbacks;

    public static UserPersonalInfoFragment getInstance(WizardViewModel wizardViewModel, UserPersonalInfoFragmentsCallbacks callbacks) {
        UserPersonalInfoFragment userPersonalInfoFragment = new UserPersonalInfoFragment();
        userPersonalInfoFragment.callbacks = callbacks;
        Bundle args = new Bundle();
        args.putSerializable(KEY_DATA, wizardViewModel);
        userPersonalInfoFragment.setArguments(args);
        return userPersonalInfoFragment;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        wizardViewModel = (WizardViewModel) getArguments().getSerializable(KEY_DATA);

        firstNameEditText.setText(wizardViewModel.getFirstName());
        lastNameEditText.setText(wizardViewModel.getLastName());

        ((NestedScrollView) getView()).getChildAt(0).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HPUIUtils.hideSoftKeyboard(getContext(), firstNameEditText, lastNameEditText);
            }

        });


    }

    @Override
    protected int getLayoutResource() {
        return R.layout.wizard_personal_info_page;
    }

    @Override
    public boolean isFragmentNameToBeDisplayed() {
        return false;
    }

    @Override
    public boolean isFilteringAvailable() {
        return false;
    }

    @Override
    public int getToolbarDisplayName() {
        return 0;
    }

    @Override
    public boolean isValid() {

        String firstName = firstNameEditText.getText().toString().trim();
        String lastName = lastNameEditText.getText().toString().trim();

        Context context = PrintOSApplication.getAppContext();

        if (TextUtils.isEmpty(firstName) || TextUtils.isEmpty(lastName)) {
            HPUIUtils.displayToast(context,
                    context.getString(R.string.user_first_name_and_last_name_cant_be_empty));
            return false;
        }

        if (!firstName.matches(Constants.NAME_ALLOWED_CHARS_REGEX_FORMAT)) {
            HPUIUtils.displayToast(context,
                    context.getString(R.string.name_dialog_invalid_first_name));
            return false;
        }

        if (!lastName.matches(Constants.NAME_ALLOWED_CHARS_REGEX_FORMAT)) {
            HPUIUtils.displayToast(context,
                    context.getString(R.string.name_dialog_invalid_last_name));
            return false;
        }

        HPLogger.d(TAG, "Name dialog - Send button clicked.");

        Analytics.sendEvent(Analytics.EVENT_NAME_DIALOG_SEND_BUTTON_CLICKED);

        if (wizardViewModel != null) {
            wizardViewModel.setFirstName(firstName);
            wizardViewModel.setLastName(lastName);
            wizardViewModel.setDisplayName(firstName + " " + lastName);
        }

        return true;

    }

    @Override
    public String getFragmentName() {
        return "Wizard_Personal_Info";
    }

    @OnClick(R.id.profile_click_area)
    public void onProfilePhotoClicked() {
        if (callbacks != null) {
            callbacks.onUserProfilePhotoClicked();
        }
    }

    public void updateProfileImage(String url) {
        if (getActivity() != null) {
            ImageLoadingUtils.setLoadedImageFromUrl(getActivity(), profileImage, url, R.drawable.user_profile,null, true);
        }
    }

    @Override
    public void onCancel() {

    }


    public interface UserPersonalInfoFragmentsCallbacks {
        void onUserProfilePhotoClicked();
    }

}
