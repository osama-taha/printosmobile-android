package com.hp.printosmobile.presentation.modules.insights;

import com.hp.printosmobile.presentation.MVPView;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitEnum;

import java.util.List;

/**
 * Created by Anwar Asbah on 6/20/17.
 */
public interface InsightsView extends MVPView {

    void onInsightTop5DataRetrieved(InsightsViewModel viewModel, InsightsViewModel.InsightKpiEnum kpiEnum,
                                    boolean isDataSelected);

    void showLoadingView();

    void hideLoadingView();

    void showErrorView();

    void hideErrorView();

    void onBusinessUnitDataReceived(List<BusinessUnitEnum> businessUnitEnums);

    void onEmptyBusinessUnits();
}
