package com.hp.printosmobile.presentation.modules.userimagepopup;

import android.net.Uri;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.hp.printosmobile.Analytics;
import com.hp.printosmobile.R;
import com.hp.printosmobile.presentation.modules.settings.BaseSettingsActivity;
import com.hp.printosmobile.utils.HPUIUtils;
import com.hp.printosmobile.utils.ImageLoadingUtils;
import com.hp.printosmobilelib.core.logging.HPLogger;

import butterknife.Bind;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Osama Taha
 */
public class AddUserImageActivity extends BaseSettingsActivity {

    public static final String TAG = AddUserImageActivity.class.getSimpleName();

    @Bind(R.id.dialog_dismiss)
    TextView dismissButton;
    @Bind(R.id.profile_image)
    CircleImageView profileImage;
    @Bind(R.id.loading_indicator)
    ProgressBar uploadImageLoadingIndicator;
    private boolean deleteImageRequested;
    private boolean userAddImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        updateDismissButton(R.string.wizard_popup_dismiss);

        Analytics.sendEvent(Analytics.EVENT_PROFILE_IMAGE_POPUP_SHOWN);

        interceptTouch = false;
    }

    private void updateDismissButton(int textResource) {
        String text = getString(textResource);
        SpannableString spannable = new SpannableString(text);
        spannable.setSpan(new UnderlineSpan(), 0, text.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        dismissButton.setText(spannable);

    }


    @OnClick(R.id.dialog_dismiss)
    public void onDismissClicked() {

        if (!userAddImage) {
            Analytics.sendEvent(Analytics.EVENT_PROFILE_IMAGE_POPUP_SUPPRESSED);
            HPLogger.d(TAG, "add user image dialog - dismiss button clicked.");
        }

        finish();

    }

    @OnClick(R.id.profile_click_area)
    public void onProfilePhotoClicked() {
        super.onUserProfilePhotoClicked();
    }

    @Override
    protected void updateProfileImageWithImageUrl(String url) {

        interceptTouch = false;
        uploadImageLoadingIndicator.setVisibility(View.GONE);

        if (TextUtils.isEmpty(url)) {

            if (!deleteImageRequested) {
                HPUIUtils.displayToast(this, getString(R.string.user_profile_fail_to_upload_profile_image));
            }

            updateDismissButton(R.string.wizard_popup_dismiss);
            userAddImage = false;

        } else {

            Analytics.sendEvent(Analytics.EVENT_PROFILE_IMAGE_POPUP_IMAGE_ADDED);
            updateDismissButton(R.string.wizard_done);
            ImageLoadingUtils.setLoadedImageFromUrl(this, null, url, 0, null, true);
            userAddImage = true;

        }

        deleteImageRequested = false;

    }

    @Override
    protected void onUploadImageStarted(Uri uri) {
        super.onUploadImageStarted(uri);
        profileImage.setImageURI(uri);
        uploadImageLoadingIndicator.setVisibility(View.VISIBLE);
    }

    protected void onDeleteImageStarted() {
        deleteImageRequested = true;
        userAddImage = false;
        updateDismissButton(R.string.wizard_popup_dismiss);
        profileImage.setImageResource(R.drawable.user_profile);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.add_user_image_dialog;
    }

}
