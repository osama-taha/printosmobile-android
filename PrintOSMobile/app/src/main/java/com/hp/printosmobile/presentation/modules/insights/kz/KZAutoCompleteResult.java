package com.hp.printosmobile.presentation.modules.insights.kz;

import com.hp.printosmobile.R;

/**
 * Created by Osama on 4/4/18.
 */

public class KZAutoCompleteResult {

    private String name;
    private KZAutoCompleteResultType type;
    private int orderRes;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public KZAutoCompleteResultType getType() {
        return type;
    }

    public void setType(KZAutoCompleteResultType type) {
        this.type = type;
    }

    public int getOrderRes() {
        return orderRes;
    }

    public void setOrderRes(int orderRes) {
        this.orderRes = orderRes;
    }

    @Override
    public String toString() {
        return getName();
    }

    public enum KZAutoCompleteResultType {

        HISTORIC("historic", R.color.white, R.color.c110d),
        AUTO_COMPLETE("auto_complete", android.R.color.white, R.color.hp_default),
        AUTO_COMPLETE_WORD("auto_complete_word", android.R.color.white, R.color.hp_default),
        UNKNOWN("unknown", android.R.color.white, R.color.hp_default);

        private final String value;
        private final int bgColorResId;
        private final int txtColorResId;

        KZAutoCompleteResultType(String value, int bgColorResId, int txtColorResId) {
            this.value = value;
            this.bgColorResId = bgColorResId;
            this.txtColorResId = txtColorResId;
        }

        public String getValue() {
            return value;
        }

        public int getBgColorResId() {
            return bgColorResId;
        }

        public int getTxtColorResId() {
            return txtColorResId;
        }

        public static KZAutoCompleteResultType from(String value) {

            if (value == null) {
                return UNKNOWN;
            }

            for (KZAutoCompleteResultType kzFormat : KZAutoCompleteResultType.values()) {
                if (kzFormat.getValue().equalsIgnoreCase(value)) {
                    return kzFormat;
                }
            }

            return UNKNOWN;

        }

    }
}
