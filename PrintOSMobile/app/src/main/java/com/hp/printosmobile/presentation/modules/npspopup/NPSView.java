package com.hp.printosmobile.presentation.modules.npspopup;

import com.hp.printosmobile.data.remote.models.UserInfoData;
import com.hp.printosmobile.presentation.MVPView;

/**
 * Created by Anwar Asbah on 9/5/17.
 */
public interface NPSView extends MVPView {

    void onNPSUserInfoRetrieved(UserInfoData userInfoData, boolean showDialog);

    void onNPSDataRetrievalFailure(boolean isFromMenu);
}
