package com.hp.printosmobile.presentation.modules.eula;

import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.data.remote.ApiServicesProvider;
import com.hp.printosmobile.data.remote.models.AcceptEulaRequestBody;
import com.hp.printosmobile.data.remote.models.AcceptPrivacyVersionRequestBody;
import com.hp.printosmobile.data.remote.services.EulaService;

import okhttp3.ResponseBody;
import retrofit2.Response;
import rx.Observable;
import rx.functions.Func1;
import rx.functions.Func2;

/**
 * Created by Anwar Asbah on 9/1/16.
 */
public class EulaDataManager {

    private static final String EULA_VERSION_HEADER = "EULA-Version";
    private static final String PRIVACY_VERSION_HEADER = "PRIVACY-Version";

    private EulaDataManager() {
    }

    /**
     * Returns an observable object responsible for retrieving EULA content.
     */
    public static Observable<EULAViewModel> getEulaText(String language) {

        ApiServicesProvider serviceProvider = PrintOSApplication.getApiServicesProvider();
        EulaService eulaService = serviceProvider.getEULAService();

        return eulaService.getEULA(language).map(new Func1<Response<ResponseBody>, EULAViewModel>() {
            @Override
            public EULAViewModel call(Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        EULAViewModel eulaViewModel = new EULAViewModel();
                        if (response.body() != null) {
                            eulaViewModel.setEulaText(response.body().string());
                        }
                        eulaViewModel.setEulaVersion(response.headers().get(EULA_VERSION_HEADER));
                        eulaViewModel.setPrivacyVersion(response.headers().get(PRIVACY_VERSION_HEADER));

                        return eulaViewModel;

                    } catch (Exception e) {
                        return null;
                    }
                }
                return null;
            }
        });
    }

    public static Observable<Boolean> acceptEulaAndPrivacyVersion(EULAViewModel eulaViewModel) {
        return Observable.combineLatest(acceptEula(eulaViewModel.getEulaVersion()),
                acceptPrivacy(eulaViewModel.getPrivacyVersion()), new Func2<Boolean, Boolean, Boolean>() {
                    @Override
                    public Boolean call(Boolean aBoolean, Boolean aBoolean2) {
                        return aBoolean && aBoolean2;
                    }
                });
    }


    public static Observable<Boolean> acceptEula(String eulaVersion) {

        ApiServicesProvider serviceProvider = PrintOSApplication.getApiServicesProvider();
        EulaService eulaService = serviceProvider.getEULAService();

        AcceptEulaRequestBody input = new AcceptEulaRequestBody();
        input.setEulaVersion(eulaVersion);

        return eulaService.acceptEula(input).map(new Func1<Response<ResponseBody>, Boolean>() {
            @Override
            public Boolean call(Response<ResponseBody> responseBodyResponse) {
                return responseBodyResponse.isSuccessful();
            }
        });
    }

    public static Observable<Boolean> acceptPrivacy(String privacyVersion) {

        ApiServicesProvider serviceProvider = PrintOSApplication.getApiServicesProvider();
        EulaService eulaService = serviceProvider.getEULAService();

        AcceptPrivacyVersionRequestBody input = new AcceptPrivacyVersionRequestBody();
        input.setPrivacyVersion(privacyVersion);

        return eulaService.acceptPrivacy(input).map(new Func1<Response<ResponseBody>, Boolean>() {
            @Override
            public Boolean call(Response<ResponseBody> responseBodyResponse) {
                return responseBodyResponse.isSuccessful();
            }
        });
    }
}