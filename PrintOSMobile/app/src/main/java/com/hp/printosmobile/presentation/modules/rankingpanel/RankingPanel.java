package com.hp.printosmobile.presentation.modules.rankingpanel;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.RelativeSizeSpan;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hp.printosmobile.AnswersSdk;
import com.hp.printosmobile.Constants;
import com.hp.printosmobile.R;
import com.hp.printosmobile.presentation.modules.home.Panel;
import com.hp.printosmobile.presentation.modules.shared.DeepLinkUtils;
import com.hp.printosmobile.presentation.modules.shared.PanelShareCallbacks;
import com.hp.printosmobile.presentation.modules.week.RankingViewModel;
import com.hp.printosmobile.utils.FileUtils;
import com.hp.printosmobile.utils.HPUIUtils;
import com.hp.printosmobilelib.ui.widgets.panel.PanelView;
import com.hp.printosmobilelib.ui.widgets.panel.SharableObject;

import butterknife.Bind;

/**
 * Created by Osama Taha
 */
public class RankingPanel extends PanelView<RankingViewModel> implements SharableObject {

    public static final String TAG = RankingPanel.class.getSimpleName();

    @Bind(R.id.ranking_layout)
    View rankingLayout;
    @Bind(R.id.no_ranking_data_text_view)
    TextView noRankingDataTextView;
    @Bind(R.id.trophy_layout)
    View trophyLayout;
    @Bind(R.id.trophy_image_view)
    ImageView trophyImageView;
    @Bind(R.id.top_5_label)
    TextView top5TextView;
    @Bind(R.id.top_5_in_region_label)
    TextView top5RegionMessageTextView;
    @Bind(R.id.rank_area_layout)
    LinearLayout rankAreaLayout;
    RankingPanelCallbacks callback;

    public RankingPanel(Context context) {
        super(context);
    }

    public RankingPanel(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public RankingPanel(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void addCallback(RankingPanelCallbacks callback) {
        this.callback = callback;
    }

    @Override
    public Spannable getTitleSpannable() {
        String extension = "";
        if (getViewModel() != null && getViewModel().getWeek() != null && getViewModel().getWeek() >= 0) {
            extension = getContext().getString(R.string.ranking_week, getViewModel().getWeek());
        }

        String title = getContext().getString(R.string.weekly_printbeat_site_weekly_rank) +
                (TextUtils.isEmpty(extension) ? "" : (" " + extension));

        Spannable spannable = new SpannableStringBuilder(title);
        if (!TextUtils.isEmpty(extension)) {
            spannable.setSpan(new RelativeSizeSpan(0.8f),
                    title.indexOf(extension),
                    title.indexOf(extension) + extension.length(),
                    Spanned.SPAN_INCLUSIVE_INCLUSIVE);
        }

        return spannable;
    }

    @Override
    public int getContentView() {
        return R.layout.panel_ranking;
    }

    @Override
    public void updateViewModel(RankingViewModel viewModel) {

        setViewModel(viewModel);

        if (showEmptyCard()) {
            DeepLinkUtils.respondToIntentForPanel(this, callback, true);
            return;
        }

        setRankingLayout();
        setTitle(getTitleSpannable());

        DeepLinkUtils.respondToIntentForPanel(this, callback, false);

        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                onRankingPanelClicked("");
            }
        });
    }

    public void onRankingPanelClicked(String focused) {
        if (callback != null) {
            callback.onRankingPanelClicked(focused);
        }
    }

    @Override
    public void bindViews() {

    }

    @Override
    protected boolean isValidViewModel() {
        return getViewModel() != null;
    }

    protected String getEmptyCardText() {
        return getContext().getString(R.string.no_information_to_display);
    }


    @Override
    protected boolean hasSharingButton() {
        super.hasSharingButton();
        return true;
    }

    @Override
    protected void onShareClicked() {
        super.onShareClicked();
        if (callback != null) {
            callback.onShareButtonClicked(Panel.RANKING);
        }
    }

    @Override
    public void sharePanel() {

        if (callback != null && getViewModel() != null) {

            lockShareButton(true);

            shareButton.setVisibility(GONE);
            Bitmap panelBitmap = FileUtils.getScreenShot(this);
            shareButton.setVisibility(VISIBLE);

            if (panelBitmap != null) {
                final Uri storageUri = FileUtils.store(getContext(), panelBitmap, PANEL_SCREEN_SHOT_FILE_NAME);

                final String shareTitle;
                if (getViewModel().isTopFiveWorld()) {
                    shareTitle = getContext().getString(R.string.share_ranking_subject_top5_in_world,
                            String.valueOf(getViewModel().getWorldWideViewModel().getActualRank()));
                } else if (getViewModel().isTopFiveRegion()) {
                    shareTitle = getContext().getString(R.string.share_ranking_subject_top5_in_region,
                            String.valueOf(getViewModel().getRegionViewModel().getActualRank()),
                            getViewModel().getRegionViewModel().getName());
                } else if (getViewModel().isTopFiveSubRegion()) {
                    shareTitle = getContext().getString(R.string.share_ranking_subject_top5_in_region,
                            String.valueOf(getViewModel().getSubRegionViewModel().getActualRank()),
                            getViewModel().getSubRegionViewModel().getName());
                } else {
                    shareTitle = getContext().getString(R.string.share_ranking_subject);
                }

                DeepLinkUtils.getShareBody(getContext(),
                        Constants.UNIVERSAL_LINK_SCREEN_HOME,
                        getPanelTag(),
                        null, null, false, new DeepLinkUtils.BranchIOCallback() {
                            @Override
                            public void onLinkCreated(String link) {
                                callback.onScreenshotCreated(Panel.RANKING, storageUri,
                                        shareTitle,
                                        link,
                                        RankingPanel.this);
                                lockShareButton(false);
                            }
                        });
            }
        }
    }

    @Override
    public String getShareAction() {
        return AnswersSdk.SHARE_CONTENT_TYPE_RANKING;
    }

    private void setRankingLayout() {

        final RankingViewModel rankingViewModel = getViewModel();

        noRankingDataTextView.setVisibility(View.GONE);
        rankAreaLayout.setVisibility(View.VISIBLE);

        HPUIUtils.setVisibility(rankingViewModel.isShowTrophy(), trophyLayout);

        if (rankingViewModel.isShowTrophy()) {
            top5RegionMessageTextView.setText(rankingViewModel.getTop5RankingRegionMessage());
            trophyLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (rankingViewModel != null && !rankingViewModel.isTopFiveWorld()) {
                        HPUIUtils.displayToast(getContext(), rankingViewModel.getTop5RankingRegionMessage());
                    }
                }
            });
        }

        rankAreaLayout.removeAllViews();

        createAreaRankLayoutIfExists(rankAreaLayout, rankingViewModel.getSubRegionViewModel());
        createAreaRankLayoutIfExists(rankAreaLayout, rankingViewModel.getRegionViewModel());
        int count = createAreaRankLayoutIfExists(rankAreaLayout, rankingViewModel.getWorldWideViewModel());

        if (count > 0) {
            rankAreaLayout.getChildAt(count - 1)
                    .findViewById(R.id.separator_view).setVisibility(View.GONE);
        }

    }

    private int createAreaRankLayoutIfExists(LinearLayout rankAreaLayout, final RankingViewModel.SiteRankViewModel siteRankViewModel) {

        if (siteRankViewModel != null) {

            View rankLayout = LayoutInflater.from(getContext()).inflate(R.layout.area_rank_layout, rankAreaLayout, false);

            TextView titleTextView = rankLayout.findViewById(R.id.weekly_printbeat_rank_area_title_text_view);
            TextView rankTextView = rankLayout.findViewById(R.id.weekly_printbeat_rank_text_view);
            TextView rankTotalDividerTextView = rankLayout.findViewById(R.id.weekly_printbeat_divider_text_view);
            TextView totalTextView = rankLayout.findViewById(R.id.weekly_printbeat_rank_total_text_view);
            View trendLayout = rankLayout.findViewById(R.id.weekly_printbeat_rank_trend_layout);
            TextView offsetTextView = rankLayout.findViewById(R.id.weekly_printbeat_rank_offset);
            ImageView trendImageView = rankLayout.findViewById(R.id.weekly_printbeat_rank_trend_icon);
            View separatorView = rankLayout.findViewById(R.id.separator_view);

            separatorView.setVisibility(View.VISIBLE);

            titleTextView.setText(siteRankViewModel.getName() == null ? "" : siteRankViewModel.getName());

            if (siteRankViewModel.isLowerThird()) {
                rankTextView.setText(siteRankViewModel.getMessage());
                HPUIUtils.setVisibility(false, rankTotalDividerTextView, totalTextView);
            } else {
                rankTextView.setText(String.valueOf(siteRankViewModel.getActualRank()));
                totalTextView.setText(String.valueOf(siteRankViewModel.getTotal()));
            }

            trendLayout.setBackgroundResource(siteRankViewModel.getTrend().getBackgroundIconResId());

            if (siteRankViewModel.getTrend() == RankingViewModel.Trend.EQUALS) {
                offsetTextView.setText("");
                trendImageView.setImageResource(0);
            } else {
                offsetTextView.setText(String.valueOf(Math.abs(siteRankViewModel.getOffset())));
                trendImageView.setImageResource(siteRankViewModel.getTrend().getDirectionIconResId());
            }

            View.OnClickListener rankingLayoutClickListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (siteRankViewModel != null && callback != null) {
                        callback.onRankingPanelClicked(siteRankViewModel.getRankAreaType().getKey());
                    }
                }

            };

            rankLayout.setOnClickListener(rankingLayoutClickListener);

            LinearLayout.LayoutParams layoutParams = new LinearLayout
                    .LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT);
            layoutParams.weight = 1;

            rankAreaLayout.addView(rankLayout, layoutParams);
        }

        return rankAreaLayout.getChildCount();

    }

    @Override
    public String getPanelTag() {
        return Panel.RANKING.getDeepLinkTag();
    }

    public interface RankingPanelCallbacks extends PanelShareCallbacks, DeepLinkUtils.DeepLinkCallbacks {
        void onRankingPanelClicked(String focusablePanel);
    }
}
