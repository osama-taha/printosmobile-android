package com.hp.printosmobile.presentation.modules.settings.wizard;

import com.hp.printosmobile.data.remote.models.PreferencesData;

import java.io.Serializable;

/**
 * Created by Osama
 */
public class WizardViewModel implements Serializable {

    private String firstName;
    private String lastName;
    private String phoneNumber;
    private String address1;
    private String address2;
    private String postalCode;
    private String city;
    private String region;
    private String state;
    private String timeZone;
    private PreferencesData.UnitSystem unitSystem;
    private String displayName;
    private boolean collectCoinsEnabled;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getCountry() {
        return state;
    }

    public void setCountry(String state) {
        this.state = state;
    }

    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    public PreferencesData.UnitSystem getUnitSystem() {
        return unitSystem;
    }

    public void setUnitSystem(PreferencesData.UnitSystem unitSystem) {
        this.unitSystem = unitSystem;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setCollectCoinsEnabled(boolean collectCoinsEnabled) {
        this.collectCoinsEnabled = collectCoinsEnabled;
    }

    public boolean isCollectCoinsEnabled() {
        return collectCoinsEnabled;
    }

    public enum WizardMode {
        WIZARD_MODE_POPUP,
        WIZARD_MODE_USER_SETTINGS
    }

    @Override
    public String toString() {
        return "WizardViewModel{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", address1='" + address1 + '\'' +
                ", address2='" + address2 + '\'' +
                ", postalCode='" + postalCode + '\'' +
                ", city='" + city + '\'' +
                ", region='" + region + '\'' +
                ", state='" + state + '\'' +
                ", timeZone='" + timeZone + '\'' +
                ", unitSystem=" + unitSystem +
                ", displayName='" + displayName + '\'' +
                '}';
    }
}
