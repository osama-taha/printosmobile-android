package com.hp.printosmobile.presentation;

import com.hp.printosmobilelib.core.communications.remote.APIException;

/**
 * Basic interface for MVP design pattern which any activity/fragment must implement in order to flow the design pattern.
 *
 * @author Osama Taha
 */
public interface MVPView {
    void onError(APIException exception, String tag);
}
