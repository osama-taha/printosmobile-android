package com.hp.printosmobile.presentation.modules.home;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hp.printosmobile.Analytics;
import com.hp.printosmobile.AppseeSdk;
import com.hp.printosmobile.Constants;
import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.presentation.BaseFragment;
import com.hp.printosmobile.presentation.TargetViewManager;
import com.hp.printosmobile.presentation.indigowidget.PrintOSAppWidgetProvider;
import com.hp.printosmobile.presentation.modules.PBNotificationDataViewModel;
import com.hp.printosmobile.presentation.modules.dailyspotlight.SpotlightPanel;
import com.hp.printosmobile.presentation.modules.devicedetails.IndigoDeviceDetailsFragment;
import com.hp.printosmobile.presentation.modules.devicedetails.LatexDeviceDetailsFragment;
import com.hp.printosmobile.presentation.modules.devices.DevicesFragment;
import com.hp.printosmobile.presentation.modules.invites.InviteContactViewModel;
import com.hp.printosmobile.presentation.modules.invites.InviteUtils;
import com.hp.printosmobile.presentation.modules.invites.InvitesFragment;
import com.hp.printosmobile.presentation.modules.invites.InvitesListFragment;
import com.hp.printosmobile.presentation.modules.invites.InvitesViewModel;
import com.hp.printosmobile.presentation.modules.kpiview.KPIViewModel;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitEnum;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitViewModel;
import com.hp.printosmobile.presentation.modules.main.IMainFragment;
import com.hp.printosmobile.presentation.modules.main.MainActivity;
import com.hp.printosmobile.presentation.modules.notificationslist.NotificationListPresenter;
import com.hp.printosmobile.presentation.modules.notificationslist.NotificationListView;
import com.hp.printosmobile.presentation.modules.notificationslist.NotificationViewModel;
import com.hp.printosmobile.presentation.modules.personaladvisor.AdviceViewModel;
import com.hp.printosmobile.presentation.modules.personaladvisor.PersonalAdvisorDialog;
import com.hp.printosmobile.presentation.modules.personaladvisor.PersonalAdvisorViewModel;
import com.hp.printosmobile.presentation.modules.personaladvisor.presenter.PersonalAdvisorFactory;
import com.hp.printosmobile.presentation.modules.printersnapshot.PrinterSnapshotPanel;
import com.hp.printosmobile.presentation.modules.printersnapshot.PrinterSnapshotViewModel;
import com.hp.printosmobile.presentation.modules.productionsnapshot.indigo.IndigoProductionPanel;
import com.hp.printosmobile.presentation.modules.productionsnapshot.indigo.ProductionViewModel;
import com.hp.printosmobile.presentation.modules.productionsnapshot.latex.LatexProductionPanel;
import com.hp.printosmobile.presentation.modules.rankingpanel.RankingPanel;
import com.hp.printosmobile.presentation.modules.reportkpibreakdown.KpiBreakdownEnum;
import com.hp.printosmobile.presentation.modules.servicecallpanel.ServiceCallPanel;
import com.hp.printosmobile.presentation.modules.servicecallpanel.ServiceCallViewModel;
import com.hp.printosmobile.presentation.modules.servicecallpanel.ServiceCallViewModel.ServiceCallStateEnum;
import com.hp.printosmobile.presentation.modules.servicecallpanel.ServiceCallsFragment;
import com.hp.printosmobile.presentation.modules.shared.DeepLinkUtils;
import com.hp.printosmobile.presentation.modules.shared.DeviceViewModel;
import com.hp.printosmobile.presentation.modules.statedistribution.StateDistributionActivity;
import com.hp.printosmobile.presentation.modules.today.HistogramPanel;
import com.hp.printosmobile.presentation.modules.today.TodayPanel;
import com.hp.printosmobile.presentation.modules.today.TodayViewModel;
import com.hp.printosmobile.presentation.modules.todayhistogram.TodayHistogramViewModel;
import com.hp.printosmobile.presentation.modules.week.RankingViewModel;
import com.hp.printosmobile.presentation.modules.week.WeekCollectionViewModel;
import com.hp.printosmobile.presentation.modules.week.WeekPanel;
import com.hp.printosmobile.presentation.modules.week.WeekPressStatusDialog;
import com.hp.printosmobile.presentation.modules.week.WeekViewModel;
import com.hp.printosmobile.presentation.modules.week.kpiexplanationdialog.KpiExplanationDialog;
import com.hp.printosmobile.utils.HPUIUtils;
import com.hp.printosmobilelib.core.communications.remote.APIException;
import com.hp.printosmobilelib.core.logging.HPLogger;
import com.hp.printosmobilelib.ui.common.HPFragment;
import com.hp.printosmobilelib.ui.widgets.HPScrollView;
import com.hp.printosmobilelib.ui.widgets.panel.PanelView;
import com.hp.printosmobilelib.ui.widgets.panel.SharableObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.Bind;
import butterknife.OnClick;


public class HomeFragment extends BaseFragment implements SpotlightPanel.DailySpotlightPanelCallback, InvitesFragment.InvitesFragmentCallback, InviteUtils.InviteObservable, ServiceCallsFragment.ServiceCallsFragmentCallback, LatexProductionPanel.LatexProductionPanelCallback, IndigoDeviceDetailsFragment.IndigoDeviceDetailsFragmentCallbacks, DevicesFragment.DevicesCallbacks, ServiceCallPanel.ServiceCallPanelCallback, HomeView, IMainFragment, SwipeRefreshLayout.OnRefreshListener, PrinterSnapshotPanel.PrinterSnapshotCallback, TodayPanel.TodayPanelCallbacks, WeekPanel.WeekPanelCallbacks, IndigoProductionPanel.IndigoProductionPanelCallback, NotificationListView, HistogramPanel.HistogramPanelCallback, RankingPanel.RankingPanelCallbacks {

    private static final String TAG = HomeFragment.class.getName();

    private static final int BADGE_MAX_NUMBER_OF_NOTIFICATIONS = 99;
    private static final long SWIPE_TO_REFRESH_DELAY_IN_MILLS = 500;
    private static final long SCROLL_ANIMATION_DURATION = 400;
    private static final long SCROLL_TO_PANEL_DELAY = 200;
    private static final String FOCUSABLE_PANEL_HISTOGRAM_EXTRA = "1";
    private static final int REALTIME_REFRESH_DEF_VALUE = 30;

    private Map<Panel, PanelView> panelViewMap = new HashMap<>();
    private boolean weekPanelFirstAppearance = true;

    @Bind(R.id.panels_container)
    LinearLayout panelsContainerView;
    @Bind(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;
    @Bind(R.id.error_msg_text_view)
    TextView errorMsgTextView;
    @Bind(R.id.try_again_text_view)
    TextView tryAgainTextView;
    @Bind(R.id.error_layout)
    View errorLayout;
    @Bind(R.id.parent_container)
    View parentContainer;
    @Bind(R.id.scroll_view)
    HPScrollView scrollView;
    @Bind(R.id.fab)
    ImageView fab;
    @Bind(R.id.banner_view)
    View bannerView;
    @Bind(R.id.banner_image)
    ImageView bannerImage;
    @Bind(R.id.banner_text_main_with_suggested)
    TextView bannerTextForSuggested;
    @Bind(R.id.banner_view_without_suggested)
    View bannerRegularView;
    @Bind(R.id.banner_text_main)
    TextView bannerTextMain;
    @Bind(R.id.banner_text_description)
    TextView bannerTextDescription;

    private Panel focusablePanel;

    private HPFragment hpDetailsFragment;
    private HomeFragmentCallBack callBack;
    private HomePresenter homePresenter;
    private NotificationListPresenter notificationListPresenter;
    private FabArrowDirection fabArrowDirection = FabArrowDirection.DOWN;

    private BusinessUnitViewModel businessUnitViewModel;
    private Panel panelRequestedSharing;

    private boolean openServiceCallFragment = false;

    private Timer pollingTimer;
    private Handler mainHandler;
    private Runnable mainRunnable;

    private boolean invitesRequested = false;
    private String requestedDeviceSerialNumber;

    public HomeFragment() {
        // Required empty public constructor
    }

    public static HomeFragment newInstance() {
        return new HomeFragment();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initView();
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            callBack = (HomeFragmentCallBack) getActivity();
        } catch (ClassCastException e) {
            throw new RuntimeException("parent activity must implement HomeFragmentCallBack");
        }

        InviteUtils.addObserver(this);
    }

    @Override
    public boolean isIntercomAccessible() {
        return true;
    }

    private void initView() {

        swipeRefreshLayout.setOnRefreshListener(this);
        homePresenter = new HomePresenter();
        homePresenter.attachView(this);
        homePresenter.setShiftSupport(false);

        notificationListPresenter = new NotificationListPresenter();
        notificationListPresenter.attachView(this);

        tryAgainTextView.setClickable(true);
        tryAgainTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tryAgain();
            }
        });


        scrollView.setOnScrollStoppedListener(new HPScrollView.OnScrollStopListener() {
            @Override
            public void onScrollStopped(int y) {
                displayTargetViewIfAny();

                if (panelsContainerView != null && weekPanelFirstAppearance) {
                    int index = HPUIUtils.getMostVisiblePanelIndex(scrollView, panelsContainerView);
                    if (index >= 0 && index < panelsContainerView.getChildCount()) {
                        View view = panelsContainerView.getChildAt(index);
                        if (view instanceof WeekPanel && weekPanelFirstAppearance) {
                            setWeekPanelFirstAppearance(false);
                            Analytics.sendEvent(Analytics.WEEK_PANEL_SHOWN_EVENT);
                        }
                    }
                }
            }

            @Override
            public void onScrollDown() {
                //for later use.
            }

            @Override
            public void onScrollUp() {
                //for later use.
            }

            @Override
            public void onTopReached() {
            }

            @Override
            public void onBottomReached() {
                //for later use.
            }
        });


        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (fabArrowDirection == FabArrowDirection.UP) {

                    scrollToTop();

                } else {

                    scrollToBottom();
                }
            }
        });
    }

    public void setWeekPanelFirstAppearance(boolean firstAppearance) {
        weekPanelFirstAppearance = firstAppearance;
    }

    @Override
    public void displayTargetViewIfAny() {
        super.displayTargetViewIfAny();
        if (hpDetailsFragment != null || !isAdded() || getHostingActivity() == null) {
            return;
        }

        if (isFullyLoaded(panelViewMap) && isDisplayTargetViews()) {
            setDisplayTargetViews(!TargetViewManager.showTargetViewForVisible(
                    getHostingActivity(), scrollView, panelViewMap, this));
        }
    }

    public void tryAgain() {
        HPLogger.d(TAG, "on TryAgain Clicked");
        PrintOSApplication.resetStartupTime();
        if (callBack != null) {
            callBack.onTryAgainClicked();
            PrintOSAppWidgetProvider.updateAllWidgets(getContext());
        }
    }

    public void navToDevice(Panel panel, String extra) {
        if (panel == Panel.TODAY && !TextUtils.isEmpty(extra)) {
            ((TodayPanel) panelViewMap.get(panel)).searchDevicesBySerialNumber(extra);
        }
    }

    public void setFocusablePanel(Panel panel, String extra) {
        this.focusablePanel = panel;

        if (panel == Panel.TODAY && extra != null && extra.equals(FOCUSABLE_PANEL_HISTOGRAM_EXTRA) &&
                panelViewMap.containsKey(Panel.HISTOGRAM)) {
            focusablePanel = Panel.HISTOGRAM;
        }
    }

    private void scrollFragmentToPanel(final Panel panel) {
        focusablePanel = panel;
        if (focusablePanel == null || panelViewMap == null || !panelViewMap.containsKey(panel)) {
            return;
        }

        final PanelView panelView = panelViewMap.get(panel);

        scrollView.postDelayed(new Runnable() {
            @Override
            public void run() {
                scrollView.scrollTo(0, panelView.getTop());
            }
        }, SCROLL_TO_PANEL_DELAY);

    }

    private void scrollToBottom() {

        View lastChild = scrollView.getChildAt(scrollView.getChildCount() - 1);
        int bottom = lastChild.getBottom() + scrollView.getPaddingBottom();
        int sy = 0;
        int sh = scrollView.getHeight();
        final int delta = bottom - (sy + sh);

        ObjectAnimator objectAnimator = ObjectAnimator.ofInt(scrollView, "scrollY", 0, delta).setDuration(SCROLL_ANIMATION_DURATION);
        objectAnimator.start();

    }


    private void scrollToTop() {

        ObjectAnimator objectAnimator = ObjectAnimator.ofInt(scrollView, "scrollY", 0).setDuration(SCROLL_ANIMATION_DURATION);
        objectAnimator.start();

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_home;
    }


    @Override
    public void onRefresh() {

        Analytics.sendEvent(Analytics.PULL_TO_REFRESH_ACTION);

        homePresenter.refresh();
        if (callBack != null) {
            callBack.onRefresh();
        }

        //Show the refresh view for 500ms then close.
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(false);
            }
        }, SWIPE_TO_REFRESH_DELAY_IN_MILLS);

    }

    @Override
    public void onPrinterSnapshotViewAllClicked() {

        openDevicesFragment(homePresenter.getTodayViewModel());

    }

    public boolean isDisplayingDetailsFragment() {
        return hpDetailsFragment != null;
    }

    @Override
    public void enableSwipeToRefresh(boolean enable) {
        swipeRefreshLayout.setEnabled(enable);
    }

    @Override
    public void onTodayViewInitialized() {
        if (focusablePanel != Panel.UNKNOWN) {
            scrollFragmentToPanel(focusablePanel);
            focusablePanel = Panel.UNKNOWN;
        }
    }

    @Override
    public void setHasRTDevices(boolean hasRTDevices) {
        if (callBack != null) {
            callBack.setHasRtDevices(hasRTDevices);
        }

        if (panelViewMap.containsKey(Panel.HISTOGRAM)) {
            PanelView histogramPanel = panelViewMap.get(Panel.HISTOGRAM);
            histogramPanel.setVisibility(hasRTDevices ? View.VISIBLE : View.GONE);
        }
    }

    @Override
    public void resetHistogramData() {

        if (callBack != null) {
            callBack.onHistogramDataRetrieved(null);
        }

        if (panelViewMap.containsKey(Panel.HISTOGRAM)) {
            panelViewMap.get(Panel.HISTOGRAM).setViewModel(null);
        }
    }

    @Override
    public void onStateDistributionClicked(DeviceViewModel model) {
        if (getActivity() != null && isAdded()) {
            StateDistributionActivity.startActivity(getActivity(), model, businessUnitViewModel.getBusinessUnit(), isShift());
        }
    }

    @Override
    public void onTodayPanelViewAllClicked() {
        openDevicesFragment(homePresenter.getTodayViewModel());
    }

    @Override
    public void onDeviceClicked(DeviceViewModel model) {
        if (callBack != null) {
            attachDeviceDetailsScreen(model);
        }
    }


    private void attachDeviceDetailsScreen(DeviceViewModel model) {
        FragmentTransaction trans = getFragmentManager()
                .beginTransaction();

        switch (model.getBusinessUnitEnum()) {
            case INDIGO_PRESS:
                hpDetailsFragment = IndigoDeviceDetailsFragment.newInstance(model);
                ((IndigoDeviceDetailsFragment) hpDetailsFragment).attachListener(this);
                break;
            case LATEX_PRINTER:
                hpDetailsFragment = LatexDeviceDetailsFragment.newInstance(model);
                break;
            default:
                return;
        }

        trans.replace(R.id.parent_container, hpDetailsFragment);
        trans.commitAllowingStateLoss();

        if (callBack != null) {
            callBack.onDevicesDetailedClicked(hpDetailsFragment);
        }
    }

    @Override
    public void onHasAdviceIconClicked(DeviceViewModel model) {
        if (model.hasUnsuppressedAdvices()) {
            onHasAdviceIconClicked(model.getPersonalAdvisorViewModel());
        }
    }

    @Override
    public void onHasAdviceIconClicked(PersonalAdvisorViewModel model) {

        PersonalAdvisorDialog dialog = PersonalAdvisorDialog.newInstance(model,
                new PersonalAdvisorDialog.PersonalAdvisorDialogCallback() {
                    @Override
                    public void goToPersonalAdvisor(AdviceViewModel adviceViewModel) {
                        if (callBack != null) {
                            callBack.scrollToAdvice(adviceViewModel);
                        }
                    }

                    @Override
                    public void onAdvicePopupDismiss() {
                        if (callBack != null) {
                            callBack.onAdvicePopupDismissed();
                        }
                    }

                    @Override
                    public void onScreenshotCreated(final Panel personalAdvisor, final Uri storageUri, String adviceID, final SharableObject sharableObject) {

                        DeepLinkUtils.getShareBody(getContext(),
                                Constants.UNIVERSAL_LINK_SCREEN_PAPOPUP,
                                Panel.PERSONAL_ADVISOR.getDeepLinkTag(), null, adviceID, false,
                                new DeepLinkUtils.BranchIOCallback() {
                                    @Override
                                    public void onLinkCreated(String link) {
                                        HomeFragment.this.onScreenshotCreated(personalAdvisor, storageUri,
                                                getContext().getString(R.string.share_advice_panel_title),
                                                link,
                                                sharableObject);
                                    }
                                });
                    }
                });

        if (callBack != null) {
            callBack.onAdvicePopupDisplayed();
        }
        dialog.show(getFragmentManager(), TAG);
    }

    @Override
    public void onShiftSelected(ShiftViewModel shiftViewModel) {
        boolean isShift = shiftViewModel.getShiftType() == ShiftViewModel.ShiftType.CURRENT;
        homePresenter.setShiftSupport(isShift);
        getTodayData(false, false);


        getData(Panel.HISTOGRAM);
        if (panelViewMap.containsKey(Panel.HISTOGRAM)) {
            panelViewMap.get(Panel.HISTOGRAM).showLoading();
        }
    }

    @Override
    public void onBusinessUnitSelected(BusinessUnitViewModel businessUnitModel) {

        if (businessUnitModel == null) {
            return;
        }

        HPLogger.d(TAG, "onBusinessUnitSelected");

        final boolean reloadPanels = this.businessUnitViewModel == null || this.businessUnitViewModel.getBusinessUnit() != businessUnitModel.getBusinessUnit();
        this.businessUnitViewModel = businessUnitModel;

        if (homePresenter != null) {
            homePresenter.setSelectedBusinessUnit(businessUnitModel);

            if (DeepLinkUtils.hasExtra(getHostingActivity(), Constants.IntentExtras.MAIN_ACTIVITY_IS_SHIFT_EXTRA)) {
                boolean isShift = DeepLinkUtils.getBooleanExtra(getHostingActivity(), Constants.IntentExtras.MAIN_ACTIVITY_IS_SHIFT_EXTRA);
                homePresenter.setShiftSupport(isShift);
                DeepLinkUtils.removeExtra(getActivity(), Constants.IntentExtras.MAIN_ACTIVITY_IS_SHIFT_EXTRA);
            }
        }

        if (reloadPanels) {
            panelsContainerView.removeAllViews();
            panelViewMap.clear();
        }

        for (final Panel panel : businessUnitModel.getPanels()) {
            getData(panel);
            initPanel(panel, reloadPanels);
        }

        if (!DeepLinkUtils.validateLink(businessUnitModel, DeepLinkUtils.getStringExtra(getHostingActivity(),
                Constants.IntentExtras.MAIN_ACTIVITY_PANEL_EXTRA), DeepLinkUtils.getIntExtra(getHostingActivity(),
                Constants.UNIVERSAL_LINK_SCREEN_KEY)) && callBack != null) {
            callBack.onDeepLinkHandled();
        }

        swipeRefreshLayout.setEnabled(true);

        if (PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).isInvitesFeatureEnabled(
                PrintOSApplication.getAppContext())) {
            InviteUtils.addObserver(this);
            InviteUtils.retrieveInvitesList();
        }

        PersonalAdvisorFactory.getInstance().loadData(businessUnitModel);

        notificationListPresenter.getUserNotifications();

        errorLayout.setVisibility(View.GONE);
        panelsContainerView.setVisibility(View.VISIBLE);

        setWeekPanelFirstAppearance(true);
    }

    @Override
    public void onFiltersChanged(BusinessUnitViewModel businessUnitViewModel) {

        HPLogger.d(TAG, "Filter changed " + TAG);

        this.homePresenter.setShiftSupport(false);
        this.homePresenter.setSelectedBusinessUnit(businessUnitViewModel);

        if (callBack != null) {
            callBack.onHistogramDataRetrieved(null);
        }

        for (Panel panel : businessUnitViewModel.getPanels()) {
            PanelView panelView = panelViewMap.get(panel);
            if (panelView != null) {
                panelView.showLoading();
                if (panelView instanceof TodayPanel) {
                    ((TodayPanel) panelView).onFilterSelected();
                } else if (panelView instanceof SpotlightPanel) {
                    ((SpotlightPanel) panelView).requestData();
                }
            }
            getData(panel);
        }

        if (hpDetailsFragment instanceof ServiceCallsFragment) {
            ((ServiceCallsFragment) hpDetailsFragment).onFilterSelected();
        }

        if (hpDetailsFragment instanceof DevicesFragment) {
            ((DevicesFragment) hpDetailsFragment).onFilterSelected();
        }

        notificationListPresenter.getUserNotifications();
        setWeekPanelFirstAppearance(true);
    }

    private void getData(Panel panel) {
        switch (panel) {
            case LATEX_PRODUCTION:
            case INDIGO_PRODUCTION:
                break;
            case HISTOGRAM:
                boolean isGraphEnable = PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).isHistogramGraphEnabled();
                if (isGraphEnable && !homePresenter.isShiftSupport()) {
                    homePresenter.getActualVsTargetGraphData();
                } else {
                    homePresenter.getActualVsTargetData(getActivity(), false);
                }
                break;
            case TODAY:
                getTodayData(true, false);
                break;
            case PRINTERS:
                if (businessUnitViewModel.getBusinessUnit() == BusinessUnitEnum.IHPS_PRESS) {
                    homePresenter.getPrintersData();
                }
                break;
            case PERFORMANCE:
                homePresenter.getWeekData(getActivity());
                break;
            case RANKING:
                homePresenter.getSiteRankingData(getActivity());
                break;
            case PANEL_SERVICE_CALL:
                homePresenter.getServiceCalls();
                break;
            case DAILY_SPOTLIGHT:
                break;
            default:
                break;
        }
    }

    private void getTodayData(boolean loadShifts, boolean isPolling) {

        if (loadShifts) {
            homePresenter.getActiveShifts();
        }

        homePresenter.getTodayData(isPolling);
    }

    private void initPanel(Panel panel, boolean reload) {

        if(!isAdded() || getActivity() == null || getContext() == null ) {
            return;
        }

        PanelView panelView = null;
        if (reload && panelViewMap.containsKey(panel)) {
            panelViewMap.remove(panel);
        }

        if (reload || !panelViewMap.containsKey(panel)) {
            LinearLayout.LayoutParams layoutParams = HPUIUtils.getPanelLayoutParams(getContext());
            switch (panel) {
                case TODAY:
                    panelView = new TodayPanel(getActivity());
                    ((TodayPanel) panelView).addTodayPanelCallbacks(this);
                    panelView.showLoading();
                    break;
                case DAILY_SPOTLIGHT:
                    panelView = new SpotlightPanel(getActivity());
                    ((SpotlightPanel) panelView).addCallback(this);
                    ((SpotlightPanel) panelView).requestData();
                    break;
                case HISTOGRAM:
                    panelView = new HistogramPanel(getActivity());
                    ((HistogramPanel) panelView).addHistogramPanelCallback(this);
                    panelView.showLoading();
                    break;
                case LATEX_PRODUCTION:
                    panelView = new LatexProductionPanel(getActivity());
                    ((LatexProductionPanel) panelView).addCallback(this);
                    panelView.showLoading();
                    break;
                case PANEL_SERVICE_CALL:
                    panelView = new ServiceCallPanel(getActivity());
                    ((ServiceCallPanel) panelView).setServiceCallPanelCallback(this);
                    panelView.showLoading();
                    break;
                case INDIGO_PRODUCTION:
                    panelView = new IndigoProductionPanel(getActivity());
                    ((IndigoProductionPanel) panelView).addCallback(this);
                    panelView.showLoading();
                    break;
                case PRINTERS:
                    panelView = new PrinterSnapshotPanel(getActivity());
                    ((PrinterSnapshotPanel) panelView).addPrinterSnapshotCallback(this);
                    panelView.showLoading();
                    break;
                case RANKING:
                    panelView = new RankingPanel(getActivity());
                    panelView.setVisibility(View.GONE);
                    panelView.showLoading();
                    break;
                case PERFORMANCE:
                    panelView = new WeekPanel(getActivity());
                    panelView.showLoading();
                    break;
                default:
                    break;
            }

            if (panelView != null) {
                panelViewMap.put(panel, panelView);
                panelsContainerView.addView(panelView, layoutParams);
            }

        } else {
            panelView = panelViewMap.get(panel);
            panelView.onRefresh();
            panelView.showLoading();
        }
    }


    @Override
    public void didFinishGettingBusinessUnits(Map<BusinessUnitEnum, BusinessUnitViewModel> businessUnitsMap) {

    }

    @Override
    public void updateProductionSnapshot(ProductionViewModel productionViewModel) {

        if (!panelViewMap.containsKey(Panel.INDIGO_PRODUCTION)) {
            return;
        }

        IndigoProductionPanel panel = (IndigoProductionPanel) panelViewMap.get(Panel.INDIGO_PRODUCTION);
        panel.updateViewModel(productionViewModel);
        panel.hideLoading();

        onLoadingDone(panelViewMap);
    }

    @Override
    public void updateLatexProductionSnapshot(ProductionViewModel productionViewModel) {

        if (!panelViewMap.containsKey(Panel.LATEX_PRODUCTION)) {
            return;
        }

        LatexProductionPanel panel = (LatexProductionPanel) panelViewMap.get(Panel.LATEX_PRODUCTION);
        panel.updateViewModel(productionViewModel);
        panel.hideLoading();

        onLoadingDone(panelViewMap);
    }

    @Override
    public void updateTodayPanel(TodayViewModel todayViewModel, boolean isPolling) {

        if (!panelViewMap.containsKey(Panel.TODAY)) {
            if (callBack != null) {
                callBack.onTodayDataFailure();
            }
            return;
        }

        TodayPanel todayPanel = (TodayPanel) panelViewMap.get(Panel.TODAY);
        todayPanel.updateViewModel(todayViewModel);
        todayPanel.hideLoading();

        onLoadingDone(panelViewMap);

        if (todayViewModel != null && todayViewModel.getDeviceViewModels() != null) {
            if (callBack != null) {
                callBack.onTodayPanelDevicesRetrieved(todayViewModel.getDeviceViewModels());
                if (!TextUtils.isEmpty(requestedDeviceSerialNumber)) {
                    navToDevice(Panel.TODAY, requestedDeviceSerialNumber);
                    requestedDeviceSerialNumber = "";
                    setFocusablePanel(Panel.TODAY, "");
                }
                if (hpDetailsFragment instanceof DevicesFragment) {
                    if (homePresenter.getTodayViewModel() != null) {
                        ((DevicesFragment) hpDetailsFragment).updateView(homePresenter.getTodayViewModel());
                    } else {
                        onBackPressed();
                    }
                }
            }
        } else {

            if (hpDetailsFragment != null && hpDetailsFragment instanceof DevicesFragment) {
                ((DevicesFragment) hpDetailsFragment).updateView(null);
            }

            if (callBack != null) {
                callBack.onTodayDataFailure();
            }
        }

    }

    @Override
    public void updateTodayPanelWithActiveShifts(ActiveShiftsViewModel activeShiftsViewModel) {

        if (!panelViewMap.containsKey(Panel.TODAY)) {
            return;
        }

        TodayPanel todayPanel = (TodayPanel) panelViewMap.get(Panel.TODAY);
        todayPanel.updateActiveShifts(activeShiftsViewModel);
        onLoadingDone(panelViewMap);
    }

    @Override
    public void updateHistogram(TodayHistogramViewModel todayHistogramViewModel) {

        if (panelViewMap.containsKey(Panel.HISTOGRAM)) {
            HistogramPanel histogramPanel = (HistogramPanel) panelViewMap.get(Panel.HISTOGRAM);
            histogramPanel.updateViewModel(todayHistogramViewModel);
        }

        setHistogramModel(todayHistogramViewModel);
        onLoadingDone(panelViewMap);
    }

    public void setHistogramModel(TodayHistogramViewModel todayHistogramViewModel) {
        if (todayHistogramViewModel != null && callBack != null) {
            callBack.onHistogramDataRetrieved(todayHistogramViewModel);
        }
    }

    public TodayViewModel getTodayViewModel() {
        TodayPanel todayPanel = (TodayPanel) panelViewMap.get(Panel.TODAY);
        if (todayPanel == null) {
            return null;
        }
        return todayPanel.getViewModel();
    }

    public ActiveShiftsViewModel getActiveShiftViewModel() {
        TodayPanel todayPanel = (TodayPanel) panelViewMap.get(Panel.TODAY);
        return todayPanel.getActiveShiftsViewModel();
    }

    @Override
    public void updateWeekPanel(WeekCollectionViewModel weekCollectionViewModel) {

        if (!panelViewMap.containsKey(Panel.PERFORMANCE)) {
            return;
        }

        WeekPanel weekPanel = (WeekPanel) panelViewMap.get(Panel.PERFORMANCE);
        weekPanel.addWeekPanelListener(this);
        weekPanel.updateViewModel(weekCollectionViewModel);
        weekPanel.hideLoading();

        onLoadingDone(panelViewMap);

    }

    @Override
    public void updateRankingPanel(RankingViewModel rankingViewModel) {

        if (!panelViewMap.containsKey(Panel.RANKING)) {
            return;
        }

        RankingPanel rankingPanel = (RankingPanel) panelViewMap.get(Panel.RANKING);

        if (rankingViewModel != null) {

            rankingPanel.addCallback(this);
            rankingPanel.updateViewModel(rankingViewModel);
            rankingPanel.hideLoading();

            HPUIUtils.setVisibility(true, rankingPanel);

        } else {

            HPUIUtils.setVisibility(false, rankingPanel);
        }

        if (callBack != null){
            callBack.onRankingDataLoaded(rankingViewModel);
        }

        onLoadingDone(panelViewMap);

    }

    @Override
    public void onKpiSelected(String kpiName) {
        if (callBack != null) {
            callBack.onKpiSelected(kpiName);
        }
    }

    @Override
    public void onWeekPanelClicked() {
        if (callBack != null) {
            callBack.onWeekPanelClicked();
        }
    }

    @Override
    public void onPartialDataWeekClicked(List<WeekViewModel.WeekPressStatus> weekPressStatuses, boolean isLowPrintVolume) {
        WeekPressStatusDialog.newInstance((ArrayList<WeekViewModel.WeekPressStatus>) weekPressStatuses, isLowPrintVolume,
                new WeekPressStatusDialog.WeekPressStatusDialogCallback() {
                    @Override
                    public void onDialogDismiss() {
                        if (callBack != null) {
                            callBack.onDialogDismissed();
                        }
                    }
                })
                .show(getFragmentManager(), "WeekPressStatusDialog");
        if (callBack != null) {
            callBack.onDialogDisplayed();
        }
    }

    @Override
    public void showKpiExplanationDialog(List<KPIViewModel> kpiList, int position) {
        KpiExplanationDialog kpiExplanationDialog = KpiExplanationDialog.newInstance(kpiList, position, new KpiExplanationDialog.KpiExplanationDialogCallback() {
            @Override
            public void onDialogDismiss() {
                if (callBack != null) {
                    callBack.onDialogDismissed();
                }
            }
        });
        kpiExplanationDialog.show(getFragmentManager(), "KpiExplanationDialog");
        if (callBack != null) {
            callBack.onDialogDisplayed();
        }
        Analytics.sendEvent(Analytics.EVENT_KPI_EXPLANATION_SHOWN);
    }

    @Override
    public void onShareButtonClicked(Panel panel) {
        performPanelSharing(panel);
    }

    public void performPanelSharing(Panel panel) {
        if (panelViewMap.containsKey(panel) && getActivity() instanceof MainActivity) {
            PanelView panelView = panelViewMap.get(panel);
            ((MainActivity) getActivity()).share(panelView);
        }

    }

    @Override
    public void onScreenshotCreated(Panel panel, Uri screenshotUri, String title, String body, SharableObject sharableObject) {
        shareImage(screenshotUri, title, body, sharableObject);
    }

    @Override
    public Activity getHostingActivity() {
        return getActivity();
    }

    @Override
    public void setFocused(Panel panel) {
        scrollFragmentToPanel(panel);
    }

    @Override
    public void onDeepLinkHandled() {
        if (callBack != null) {
            callBack.onDeepLinkHandled();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Constants.IntentExtras.APPS_PICKER_INTENT_REQUEST_CODE) {
            switch (resultCode) {
                case Activity.RESULT_OK:
                case Activity.RESULT_CANCELED:
                    if (panelViewMap.containsKey(panelRequestedSharing)) {
                        panelViewMap.get(panelRequestedSharing).lockShareButton(false);
                    }
                    break;
                default:
                    break;
            }
        }
    }

    @Override
    public void updatePrintersPanel(List<DeviceViewModel> deviceViewModels) {

        if (!panelViewMap.containsKey(Panel.PRINTERS)) {
            return;
        }

        final PrinterSnapshotPanel printerSnapshotPanel = (PrinterSnapshotPanel) panelViewMap.get(Panel.PRINTERS);

        PrinterSnapshotViewModel printerSnapshotViewModel = new PrinterSnapshotViewModel();
        printerSnapshotViewModel.setDevices(deviceViewModels);
        printerSnapshotViewModel.setBusinessUnitEnum(businessUnitViewModel == null ? BusinessUnitEnum.UNKNOWN : businessUnitViewModel.getBusinessUnit());

        printerSnapshotPanel.updateViewModel(printerSnapshotViewModel);
        printerSnapshotPanel.hideLoading();

        onLoadingDone(panelViewMap);

        if (hpDetailsFragment instanceof DevicesFragment && homePresenter.getTodayViewModel() != null) {
            ((DevicesFragment) hpDetailsFragment).updateView(homePresenter.getTodayViewModel());
        }

    }

    @Override
    public void updateServiceCallPanel(ServiceCallViewModel serviceCallViewModel) {

        if (!panelViewMap.containsKey(Panel.PANEL_SERVICE_CALL)) {
            return;
        }

        ServiceCallPanel serviceCallPanel = (ServiceCallPanel) panelViewMap.get(Panel.PANEL_SERVICE_CALL);
        serviceCallPanel.updateViewModel(serviceCallViewModel);
        serviceCallPanel.hideLoading();

        onLoadingDone(panelViewMap);

        boolean showPanel = serviceCallViewModel != null && serviceCallViewModel.getCallViewModels() != null
                && !serviceCallViewModel.getCallViewModels().isEmpty();
        HPUIUtils.setVisibility(showPanel, serviceCallPanel);

        if (showPanel && openServiceCallFragment) {
            openServiceCallFragment = false;
            onServiceCallPanelClicked(serviceCallViewModel, serviceCallViewModel.getNumberOfOpenCalls() == 0 ?
                    ServiceCallStateEnum.CLOSED : ServiceCallStateEnum.OPEN);
        }

        if (hpDetailsFragment instanceof ServiceCallsFragment) {
            if ((serviceCallViewModel == null || serviceCallViewModel.getCallViewModels() == null ||
                    serviceCallViewModel.getCallViewModels().isEmpty()) && hpDetailsFragment.isAdded()) {
                onBackPressed();
            } else {
                ((ServiceCallsFragment) hpDetailsFragment).displayModels(serviceCallViewModel);
            }
        }
    }

    @Override
    public void updateInviteObserver() {
        InvitesViewModel invitesViewModel = InviteUtils.getInvitesViewModel();
        if (invitesViewModel == null || !isAdded() || getActivity() == null) {
            bannerView.setVisibility(View.GONE);
            return;
        }

        boolean hasCoins = PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).isInvitesCoinEnabled();
        boolean canInvite = invitesViewModel.canInvite();

        int inviteCount = 0;
        if (hasCoins) {
            List<InviteContactViewModel> suggested = InviteUtils.getContacts(InvitesListFragment.InvitesType.SUGGESTED);
            if (suggested != null && !suggested.isEmpty()) {
                for (InviteContactViewModel model : suggested) {
                    if (model.getInviteStatus() == InviteContactViewModel.InviteStatus.TO_BE_INVITED) {
                        inviteCount = inviteCount + 1;
                    }
                }
            }
        }

        int coinsPerUser = PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).getBeatCoinsPerInvite();

        boolean displayCoins = hasCoins && PrintOSPreferences.getInstance(getActivity()).hasIndigoGBU();

        bannerImage.setImageDrawable(ResourcesCompat.getDrawable(getResources(),
                displayCoins ? R.drawable.get_coins : R.drawable.share_joy, null));

        String bannerUpperText = displayCoins ?
                getString(R.string.invites_text_main_with_coins) :
                getString(R.string.invites_text_main_without_coins);
        bannerTextMain.setText(bannerUpperText);

        bannerTextForSuggested.setText(getString(R.string.invites_contact_invite_x_contacts, coinsPerUser * inviteCount, inviteCount));

        bannerRegularView.setVisibility(displayCoins && inviteCount > 1 ? View.GONE : View.VISIBLE);
        bannerTextForSuggested.setVisibility(displayCoins && inviteCount > 1 ? View.VISIBLE : View.GONE);

        bannerTextDescription.setVisibility(displayCoins
                && inviteCount > 1 ? View.GONE : View.VISIBLE);
        inviteCount = Math.max(inviteCount, 1);
        bannerTextDescription.setText(displayCoins
                ? getString(R.string.invites_text_description_with_coins, coinsPerUser * inviteCount)
                : getString(R.string.invites_text_description_without_coins));
        bannerView.setVisibility(canInvite ? View.VISIBLE : View.GONE);

        if (invitesRequested) {
            invitesRequested = false;
            onInviteBannerClicked();
        }
    }

    public void respondToDeepLink() {
        invitesRequested = true;
        InvitesViewModel invitesViewModel = InviteUtils.getInvitesViewModel();
        if (invitesViewModel == null || !isAdded() || getActivity() == null) {
            bannerView.setVisibility(View.GONE);
            return;
        }

        invitesRequested = false;
        onInviteBannerClicked();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        homePresenter.detachView();
        notificationListPresenter.detachView();
        InviteUtils.removeObserver(this);
    }

    @Override
    public void onDestroy() {
        if (panelViewMap != null) {
            for (Panel panel : panelViewMap.keySet()) {
                panelViewMap.get(panel).onDestroy();
            }
        }
        super.onDestroy();
    }

    @Override
    public void onError(APIException exception, String... tags) {
        for (String tag : tags) {
            if (tag.equals(ServiceCallPanel.TAG) && hpDetailsFragment instanceof ServiceCallsFragment) {
                ((ServiceCallsFragment) hpDetailsFragment).onServiceCallDataRetrived();
            }
            if (tag.equals(TodayPanel.TAG) && callBack != null) {
                callBack.onTodayDataFailure();
            }
            HPUIUtils.displayToastException(getContext(), exception, tag, false);
            hideLoading(Panel.from(tag));
        }

        displayTargetViewIfAny();
    }

    @Override
    public void onPBNotificationDataRetrieved(PBNotificationDataViewModel pbNotificationDataViewModel) {
        requestedDeviceSerialNumber = pbNotificationDataViewModel.getSerialNumber();
    }

    public void getPBNotificationData(String intentStringExtra) {
        homePresenter.getPBNotificationData(intentStringExtra);
    }

    public TodayPanel getTodayPanel() {

        PanelView panelView = getPanel(Panel.TODAY);
        if (panelView != null && panelView instanceof TodayPanel) {
            return (TodayPanel) panelView;
        }

        return null;
    }

    public PanelView getPanel(Panel panel) {
        if (panelViewMap.containsKey(panel)) {
            return panelViewMap.get(panel);
        }
        return null;
    }

    private void hideLoading(Panel panel) {
        if (panelViewMap != null && panelViewMap.containsKey(panel)) {
            panelViewMap.get(panel).hideLoading();
        }
    }

    @Override
    public void onMainError(String msg, boolean triggerSignOut, boolean isHasNoDevices) {

        if (!isHasNoDevices) {
            InviteUtils.setInvitesViewModel(null);
        }

        if (PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).isInvitesFeatureEnabled(
                PrintOSApplication.getAppContext())) {
            InviteUtils.addObserver(this);
            InviteUtils.retrieveInvitesList();
        }

        if (errorLayout == null) {
            return;
        }

        if (triggerSignOut) {
            if (callBack != null) {
                callBack.triggerSignOut();
            }
            return;
        }

        swipeRefreshLayout.setEnabled(false);
        errorLayout.setVisibility(View.VISIBLE);
        panelsContainerView.setVisibility(View.GONE);

        errorMsgTextView.setText(msg);
        errorLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public boolean respondToValidationPeriodExceedance() {
        return hpDetailsFragment != null;
    }

    @Override
    public void onHistogramViewAllClicked(TodayHistogramViewModel model) {

        if (model == null) {
            HPLogger.d(TAG, "last 7 days data are not available.");
            return;
        }

        if (callBack != null) {
            callBack.onHistogramClicked(model);
        }
    }

    @Override
    public void onHistogramWebViewClicked(TodayHistogramViewModel model) {
        if (callBack != null) {
            callBack.onHistogramWebViewClicked(model);
        }
    }

    @Override
    public void onShareButtonClicked(PanelView panelView) {

    }

    @Override
    public boolean onBackPressed() {
        if (hpDetailsFragment != null) {

            if (!hpDetailsFragment.onBackPressed()) {
                FragmentManager fragmentManager = getFragmentManager();

                try {
                    boolean exists = false;
                    for (Fragment f : fragmentManager.getFragments()) {
                        if (hpDetailsFragment == f) {
                            exists = true;
                            break;
                        }
                    }
                    if (exists) {
                        FragmentTransaction trans = getFragmentManager()
                                .beginTransaction();
                        trans.remove(hpDetailsFragment);
                        trans.commit();
                    }
                } catch (IllegalStateException e) {
                    HPLogger.e(TAG, "Illegal State Exception , trying to remove removed fragment of class "
                            + hpDetailsFragment.getClass().getSimpleName());
                }

                hpDetailsFragment = null;

                if (callBack != null) {
                    callBack.onHistogramViewClosed(this);
                }
                AppseeSdk.getInstance(getActivity()).startScreen(AppseeSdk.SCREEN_HOME);
                InviteUtils.addObserver(this);
                return true;
            }

            return true;
        }

        return false;
    }

    @Override
    public boolean isFragmentNameToBeDisplayed() {
        return hpDetailsFragment == null ? false : hpDetailsFragment.isFragmentNameToBeDisplayed();
    }

    @Override
    public boolean isFilteringAvailable() {
        if (hpDetailsFragment != null) {
            return hpDetailsFragment.isFilteringAvailable();
        }
        return true;
    }

    @Override
    public boolean isSearchAvailable() {
        if (hpDetailsFragment != null) {
            return hpDetailsFragment.isSearchAvailable();
        }
        return true;
    }

    @Override
    public int getToolbarDisplayName() {
        return hpDetailsFragment == null ? R.string.unknown_value : hpDetailsFragment.getToolbarDisplayName();
    }

    @Override
    public void onJobsInQueueClicked(DeviceViewModel.JobType jobType) {
        if (callBack != null) {
            callBack.onIndigoProductionQueueViewClicked(jobType);
        }
    }

    public void updateNotificationBadge() {
        if (homePresenter != null) {
            notificationListPresenter.getUserNotifications();
        }
    }

    @Override
    public void onServiceCallPanelClicked(ServiceCallViewModel serviceCallViewModel, ServiceCallStateEnum serviceCallStateEnum) {

        FragmentTransaction transaction = getFragmentManager()
                .beginTransaction();

        hpDetailsFragment = ServiceCallsFragment.newInstance(serviceCallViewModel, serviceCallStateEnum, this);

        transaction.replace(R.id.parent_container, hpDetailsFragment);
        transaction.commitAllowingStateLoss();

        if (callBack != null) {
            callBack.onServiceCallClicked(hpDetailsFragment);
        }
    }

    public void openInvites() {
        onInviteBannerClicked();
    }

    @OnClick(R.id.banner_view)
    public void onInviteBannerClicked() {

        if (!isAdded() || getActivity() == null) {
            return;
        }

        FragmentTransaction transaction = getFragmentManager()
                .beginTransaction();

        hpDetailsFragment = InvitesFragment.newInstance(this);

        transaction.replace(R.id.parent_container, hpDetailsFragment);
        transaction.commitAllowingStateLoss();

        if (callBack != null) {
            callBack.onInvitesSubFragmentOpen(hpDetailsFragment);
        }

        InviteUtils.removeObserver(this);

        Analytics.sendEvent(Analytics.VIEW_SCREEN_EVENT, Analytics.INVITES_SCREEN_LABEL);
    }

    public void openDevicesFragment(TodayViewModel todayViewModel) {

        FragmentTransaction transaction = getFragmentManager()
                .beginTransaction();

        hpDetailsFragment = DevicesFragment.newInstance(todayViewModel, this);

        transaction.replace(R.id.parent_container, hpDetailsFragment);
        transaction.commitAllowingStateLoss();

        if (callBack != null) {
            callBack.onDevicesScreenOpened(hpDetailsFragment);
        }

    }


    public void openServiceCallFragmentWhenLoaded() {
        this.openServiceCallFragment = true;
    }

    @Override
    public void updateNotificationListView(List<NotificationViewModel> notificationViewModels) {
        int count = 0;
        if (notificationViewModels != null) {
            for (NotificationViewModel model : notificationViewModels) {
                count += (model.isRead() ? 0 : 1);
            }
        }
        count = Math.min(count, BADGE_MAX_NUMBER_OF_NOTIFICATIONS);
        if (callBack != null) {
            callBack.setNumberOfNotifications(count);
        }
    }

    @Override
    public void onNotificationsError(APIException exception) {

    }

    @Override
    public void onAllNotificationsDeleted() {

    }

    @Override
    public void onDeleteAllNotificationsFailed(APIException e) {

    }

    public void enableDataPolling(boolean enable) {
        if (pollingTimer != null) { // making sure to delete any previous instance of it
            pollingTimer.cancel();
            pollingTimer.purge();
            pollingTimer = null;
        }

        if (enable && PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).isRealtimeRefreshEnabled()) {
            int refreshRate = PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).getRealtimeRefreshRate(REALTIME_REFRESH_DEF_VALUE);
            refreshRate *= 1000;
            pollingTimer = new Timer();
            pollingTimer.schedule(new TimerTask() {
                @Override
                public void run() {
                    if (mainHandler == null && getActivity() != null) {
                        mainHandler = new Handler(getActivity().getMainLooper());
                    }
                    if (mainRunnable == null) {
                        mainRunnable = new Runnable() {
                            @Override
                            public void run() {
                                if (panelViewMap != null && panelViewMap.containsKey(Panel.TODAY)) {
                                    panelViewMap.get(Panel.TODAY).showLoading();
                                    if (panelViewMap.containsKey(Panel.INDIGO_PRODUCTION)) {
                                        panelViewMap.get(Panel.INDIGO_PRODUCTION).showLoading();
                                    }
                                    if (panelViewMap.containsKey(Panel.LATEX_PRODUCTION)) {
                                        panelViewMap.get(Panel.LATEX_PRODUCTION).showLoading();
                                    }

                                    getTodayData(false, true);
                                }

                                if (callBack != null) {
                                    callBack.onPollingTimerExecute();
                                }
                            }
                        };
                    }
                    if (mainHandler != null) {
                        mainHandler.post(mainRunnable);
                    }

                }
            }, refreshRate, refreshRate);
        }
    }

    @Override
    public void onDevicesDetailedClicked(HPFragment hpFragment) {
        if (callBack != null) {
            callBack.onDevicesDetailedClicked(hpFragment);
        }
    }

    @Override
    public void onDetailsLevelChanged(HPFragment hpFragment) {
        if (callBack != null) {
            callBack.onDetailsLevelChanged(hpFragment);
        }
    }

    @Override
    public void onDevicesDetailedClosed(HPFragment hpFragment) {
        if (callBack != null) {
            callBack.onDevicesDetailedClosed(hpFragment);
        }
    }

    @Override
    public void onNext10JobsClosed(HPFragment fragment) {
        onDevicesDetailedClicked(fragment);
    }

    @Override
    public void onNext10JobsClicked(HPFragment fragment) {
        onDetailsLevelChanged(fragment);
    }

    @Override
    public void onGoToPersonalAdvisorClicked(AdviceViewModel model) {
        if (callBack != null) {
            callBack.onGoToPersonalAdvisorClicked(model);
        }
    }

    @Override
    public void onInviteSubFragmentOpened(HPFragment subFragment) {
        if (callBack != null) {
            callBack.onInvitesSubFragmentOpen(subFragment);
        }
    }

    @Override
    public void requestContactPermissions(InvitesFragment invitesFragment) {
        if (callBack != null) {
            callBack.requestContactPermission(invitesFragment);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        InviteUtils.removeObserver(this);
    }

    public HPFragment getSubFragment() {
        return hpDetailsFragment;
    }

    public boolean isShift() {
        return homePresenter == null ? false : homePresenter.isShiftSupport();
    }

    @Override
    public void onTapTargetClick(TargetViewManager.TargetView targetView) {
        super.onTapTargetClick(targetView);
        switch (targetView) {
            case TODAY_PANEL_STATE_DISTRIBUTION:
                if (panelViewMap != null && panelViewMap.containsKey(Panel.TODAY)) {
                    PanelView panelView = panelViewMap.get(Panel.TODAY);
                    if (panelView instanceof TodayPanel) {
                        ((TodayPanel) panelView).openStateDistribution(null);
                    }
                }
                return;
            case WEEK_PANEL_KPI_BREAKDOWN_REPORT:
                if (panelViewMap != null && panelViewMap.containsKey(Panel.PERFORMANCE)) {
                    PanelView panelView = panelViewMap.get(Panel.PERFORMANCE);
                    if (panelView instanceof WeekPanel) {
                        ((WeekPanel) panelView).openFirstKpi();
                    }
                }
                return;
            case DAILY_SPOTLIGHT_MORE_INFO:
                if (panelViewMap != null && panelViewMap.containsKey(Panel.DAILY_SPOTLIGHT)) {
                    PanelView panelView = panelViewMap.get(Panel.DAILY_SPOTLIGHT);
                    if (panelView instanceof SpotlightPanel) {
                        ((SpotlightPanel) panelView).onItemClicked();
                    }
                }
                return;
            case RANKING_HISTORY:
                onRankingPanelClicked(null);
            case HISTOGRAM_BREAKDOWN:
                if(panelViewMap != null && panelViewMap.containsKey(Panel.HISTOGRAM)) {
                    PanelView panel = panelViewMap.get(Panel.HISTOGRAM);
                    if(panel instanceof HistogramPanel) {
                        onHistogramWebViewClicked(((HistogramPanel) panel).getViewModel());
                    }
                }
            default:
                break;
        }
    }

    @Override
    public void onCancel() {

    }

    @Override
    public void onMoreInfoClicked(KpiBreakdownEnum kpi) {
        if (callBack != null) {
            callBack.onKpiSelected(kpi);
        }
    }

    @Override
    public void onRankingPanelClicked(String rankingType) {
        if (callBack != null){
            callBack.onRankingPanelClicked(rankingType);
        }
    }

    public interface HomeFragmentCallBack {

        void onTodayPanelDevicesRetrieved(List<DeviceViewModel> models);

        void onTodayDataFailure();

        void onHistogramClicked(TodayHistogramViewModel model);

        void onHistogramViewClosed(HPFragment fragment);

        void onHistogramDataRetrieved(TodayHistogramViewModel model);

        void onKpiSelected(String kpiName);

        void onKpiSelected(KpiBreakdownEnum kpi);

        void onWeekPanelClicked();

        void onRefresh();

        void onRankingPanelClicked(String focusablePanel);

        void reload();

        void onTryAgainClicked();

        void triggerSignOut();

        void onIndigoProductionQueueViewClicked(DeviceViewModel.JobType jobType);

        void setNumberOfNotifications(int numberOfNotifications);

        void onServiceCallClicked(HPFragment fragment);

        void onInvitesSubFragmentOpen(HPFragment fragment);

        void onDevicesScreenOpened(HPFragment fragment);

        void onAdvicePopupDisplayed();

        void onAdvicePopupDismissed();

        void onDialogDisplayed();

        void onDialogDismissed();

        void onDevicesDetailedClicked(HPFragment fragment);

        void onDetailsLevelChanged(HPFragment fragment);

        void onDevicesDetailedClosed(HPFragment fragment);

        void setHasRtDevices(boolean hasRTDevices);

        void scrollToAdvice(AdviceViewModel adviceViewModel);

        void onGoToPersonalAdvisorClicked(AdviceViewModel model);

        void onPollingTimerExecute();

        void requestContactPermission(InvitesFragment invitesFragment);

        void onHistogramWebViewClicked(TodayHistogramViewModel model);

        void onDeepLinkHandled();

        void onRankingDataLoaded(RankingViewModel rankingViewModel);
    }

    public enum FabArrowDirection {
        UP, DOWN
    }
}