package com.hp.printosmobile.presentation.modules.insights.kz;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.hp.printosmobile.Analytics;
import com.hp.printosmobile.Constants;
import com.hp.printosmobile.R;
import com.hp.printosmobile.presentation.BaseActivity;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitEnum;

/**
 * Created by Osama on 3/13/18.
 */

public class SearchActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.content_layout);
        if (fragment == null) {
            fragment = InsightsSearchFragment.newInstance(getIntent().getExtras());
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.content_layout, fragment, "")
                    .commit();
        } else {
            getSupportFragmentManager()
                    .beginTransaction()
                    .attach(fragment)
                    .commit();
        }

        Analytics.sendEvent(Analytics.VIEW_SCREEN_EVENT, Analytics.INSIGHTS_SEARCH_SCREEN_LABEL);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_search;
    }

    public static Intent createIntent(Context context, BusinessUnitEnum businessUnitEnum) {
        return createIntent(context, businessUnitEnum, null);
    }

    public static Intent createIntent(Context context, BusinessUnitEnum businessUnitEnum, String query) {
        Intent intent = new Intent(context, SearchActivity.class);
        intent.putExtra(Constants.IntentExtras.BUSINESS_UNIT_EXTRA, businessUnitEnum);

        if(query != null) {
            intent.putExtra(Constants.IntentExtras.MAIN_ACTIVITY_OBJECT_EXTRA, query);
        }

        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        return intent;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment fragment = fragmentManager.findFragmentById(R.id.content_layout);
        if (fragment instanceof InsightsSearchFragment) {
            ((InsightsSearchFragment) fragment).onBackPressed();
        }
    }
}
