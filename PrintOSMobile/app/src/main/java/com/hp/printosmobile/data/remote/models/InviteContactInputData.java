package com.hp.printosmobile.data.remote.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Anwar Asbah 11/7/2017
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class InviteContactInputData {

    @JsonProperty("id")
    String id;
    @JsonProperty("roleId")
    String roleId;
    @JsonProperty("firstName")
    String firstName;
    @JsonProperty("lastName")
    String lastName;
    @JsonProperty("languageCode")
    String languageCode;
    @JsonProperty("email")
    String email;
    @JsonProperty("organizationType")
    String organizationType;
    @JsonProperty("invitationType")
    String invitationType;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("roleId")
    public String getRoleId() {
        return roleId;
    }

    @JsonProperty("roleId")
    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    @JsonProperty("firstName")
    public String getFirstName() {
        return firstName;
    }

    @JsonProperty("firstName")
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @JsonProperty("lastName")
    public String getLastName() {
        return lastName;
    }

    @JsonProperty("lastName")
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @JsonProperty("languageCode")
    public String getLanguageCode() {
        return languageCode;
    }

    @JsonProperty("languageCode")
    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }

    @JsonProperty("email")
    public String getEmail() {
        return email;
    }

    @JsonProperty("email")
    public void setEmail(String email) {
        this.email = email;
    }

    @JsonProperty("organizationType")
    public String getOrganizationType() {
        return organizationType;
    }

    @JsonProperty("organizationType")
    public void setOrganizationType(String organizationType) {
        this.organizationType = organizationType;
    }

    @JsonProperty("invitationType")
    public String getInvitationType() {
        return invitationType;
    }

    @JsonProperty("invitationType")
    public void setInvitationType(String invitationType) {
        this.invitationType = invitationType;
    }
}