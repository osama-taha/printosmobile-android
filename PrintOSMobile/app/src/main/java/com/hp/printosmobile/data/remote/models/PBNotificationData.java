package com.hp.printosmobile.data.remote.models;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class PBNotificationData {

    @JsonIgnore
    private Map additionalProperties = new HashMap();

    @JsonProperty("orgId")
    String orgId;
    @JsonProperty("siteId")
    String siteId;
    @JsonProperty("bu")
    String businessUnit;
    @JsonProperty("serialNumber")
    String serialNumber;
    @JsonProperty("pressName")
    String pressName;

    @JsonProperty("orgId")
    public String getOrgId() {
        return orgId;
    }

    @JsonProperty("orgId")
    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    @JsonProperty("siteId")
    public String getSiteId() {
        return siteId;
    }

    @JsonProperty("siteId")
    public void setSiteId(String siteId) {
        this.siteId = siteId;
    }

    @JsonProperty("bu")
    public String getBusinessUnit() {
        return businessUnit;
    }

    @JsonProperty("bu")
    public void setBusinessUnit(String bu) {
        this.businessUnit = bu;
    }

    @JsonProperty("serialNumber")
    public String getSerialNumber() {
        return serialNumber;
    }

    @JsonProperty("serialNumber")
    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    @JsonProperty("pressName")
    public String getPressName() {
        return pressName;
    }

    @JsonProperty("pressName")
    public void setPressName(String pressName) {
        this.pressName = pressName;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}