package com.hp.printosmobile.presentation.modules.settings;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.content.res.ResourcesCompat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.baoyz.actionsheet.ActionSheet;
import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;
import com.hp.printosmobile.presentation.BaseActivity;
import com.hp.printosmobile.utils.FileUtils;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.util.List;

/**
 * Created by Osama on 12/21/17.
 */

public abstract class BaseSettingsActivity extends BaseActivity implements ActionSheet.ActionSheetListener, ProfileView {

    private static final int READ_WRITE_EXTERNAL_STORAGE_WITH_CAMERA_PERMISSION = 1;
    private static final int CAMERA_REQUEST_CODE = 1;
    private static final String CAMERA_FILE = "PrintOSProfile";
    private static final int PROFILE_IMAGE_REQUESTED_SIZE = 300;

    protected boolean hasProfileImage;
    protected Uri captureImageUri;
    protected boolean interceptTouch = true;
    protected boolean editClicked = false;
    protected ProfilePresenter profilePresenter;

    @Override
    public void onDismiss(ActionSheet actionSheet, boolean b) {

    }

    @Override
    public void onOtherButtonClick(ActionSheet actionSheet, int i) {
        editClicked = false;
        if (!hasProfileImage) {
            switch (i) {
                case 0: //capture image
                    if (permissionsRequested()) {
                        openCamera();
                    }
                    break;
                case 1: //select image
                    openCropActivity(null);
                    break;
                default:
                    break;
            }
        } else {
            switch (i) {
                case 0:
                    //delete image
                    if (profilePresenter == null) {
                        initPresenter(false);
                    }
                    onDeleteImageStarted();
                    profilePresenter.deleteProfileImage();
                    interceptTouch = true;
                    break;
                case 1: //capture image
                    if (permissionsRequested()) {
                        openCamera();
                    }
                    break;
                case 2: //select image
                    openCropActivity(null);
                    break;
                default:
                    break;
            }
        }
    }

    protected void onUploadImageStarted(Uri uri) {
        //optional
    }

    protected void onDeleteImageStarted() {
        //optional
    }

    private boolean permissionsRequested() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            int writePermissionGrantedCode = ContextCompat.checkSelfPermission(PrintOSApplication.getAppContext(),
                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE);

            int readPermissionGrantedCode = ContextCompat.checkSelfPermission(PrintOSApplication.getAppContext(),
                    Manifest.permission.READ_EXTERNAL_STORAGE);

            int cameraPermissionGrantedCode = ContextCompat.checkSelfPermission(PrintOSApplication.getAppContext(),
                    Manifest.permission.CAMERA);

            if (readPermissionGrantedCode != PackageManager.PERMISSION_GRANTED ||
                    writePermissionGrantedCode != PackageManager.PERMISSION_GRANTED ||
                    cameraPermissionGrantedCode != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.READ_EXTERNAL_STORAGE,
                                Manifest.permission.CAMERA},
                        READ_WRITE_EXTERNAL_STORAGE_WITH_CAMERA_PERMISSION);
                return false;
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == READ_WRITE_EXTERNAL_STORAGE_WITH_CAMERA_PERMISSION) {
            cameraPermissionGranted(grantResults);
        }
    }

    private void cameraPermissionGranted(int[] grantResults) {
        if (grantResults.length > 0) {
            boolean allGranted = true;
            for (int i = 0; i < grantResults.length; i++) {
                allGranted = allGranted && grantResults[i] == PackageManager.PERMISSION_GRANTED;
            }
            if (allGranted) {
                openCamera();
            }
        }
    }


    public void openCamera() {
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        captureImageUri = FileProvider.getUriForFile(this,
                this.getApplicationContext().getPackageName() + ".provider", FileUtils.getOutputMediaFile(CAMERA_FILE));
        intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, captureImageUri);

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            List<ResolveInfo> resInfoList = getPackageManager().queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
            for (ResolveInfo resolveInfo : resInfoList) {
                String packageName = resolveInfo.activityInfo.packageName;
                grantUriPermission(packageName, captureImageUri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
            }
        }

        startActivityForResult(intent, CAMERA_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case CAMERA_REQUEST_CODE:
                    openCropActivity(captureImageUri);
                    break;
                case CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE:
                    if (profilePresenter == null) {
                        initPresenter(false);
                    }
                    CropImage.ActivityResult result = CropImage.getActivityResult(data);
                    if (result != null) {
                        onUploadImageStarted(result.getUri());
                        profilePresenter.uploadProfileImage(this, result.getUri(), editClicked);
                    }
                    interceptTouch = true;
                    break;
                default:
                    break;
            }
        }
    }

    protected void initPresenter(boolean loadInfo) {
        profilePresenter = new ProfilePresenter();
        profilePresenter.attachView(this);
        if (loadInfo) {
            profilePresenter.getProfileImageInfo();
        }
    }


    private void openCropActivity(Uri uri) {
        CropImage.ActivityBuilder activityBuilder = uri != null ? CropImage.activity(uri) : CropImage.activity();
        activityBuilder
                .setGuidelines(CropImageView.Guidelines.ON)
                .setCropShape(CropImageView.CropShape.OVAL)
                .setFixAspectRatio(true)
                .setOutputCompressFormat(Bitmap.CompressFormat.JPEG)
                .setRequestedSize(PROFILE_IMAGE_REQUESTED_SIZE, PROFILE_IMAGE_REQUESTED_SIZE)
                .start(this);
    }


    public void onUserProfilePhotoClicked() {
        if (interceptTouch) {
            return;
        }

        ActionSheet.Builder builder = ActionSheet.createBuilder(this, getSupportFragmentManager());
        builder.setCancelButtonTitle(getString(R.string.profile_cancel));

        if (hasProfileImage) {
            builder.setOtherButtonTitles(
                    getString(R.string.profile_delete_image),
                    getString(R.string.profile_take_picture),
                    getString(R.string.profile_select_image));
        } else {
            builder.setOtherButtonTitles(getString(R.string.profile_take_picture),
                    getString(R.string.profile_select_image));
        }

        final ActionSheet actionSheet = builder.setCancelableOnTouchOutside(true)
                .setListener(this).show();

        if (hasProfileImage) { //setting delete button color
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    View actionSheetView = actionSheet.getActivity().getWindow().getDecorView();
                    if (actionSheetView instanceof ViewGroup) {
                        for (int childIndex = 0; childIndex < ((ViewGroup) actionSheetView).getChildCount(); childIndex++) {
                            View child = ((ViewGroup) actionSheetView).getChildAt(childIndex);
                            if (child instanceof FrameLayout) {
                                for (int subChildIndex = 0; subChildIndex < ((FrameLayout) child).getChildCount(); subChildIndex++) {
                                    View subChild = ((FrameLayout) child).getChildAt(subChildIndex);
                                    if (subChild instanceof LinearLayout) {
                                        for (int buttonIndex = 0; buttonIndex < ((LinearLayout) subChild).getChildCount(); buttonIndex++) {
                                            View buttonView = ((LinearLayout) subChild).getChildAt(buttonIndex);
                                            if (buttonView instanceof Button) {
                                                ((Button) buttonView).setAllCaps(false);
                                                if (((Button) buttonView).getText().toString().equalsIgnoreCase(getString(R.string.profile_delete_image))) {
                                                    ((Button) buttonView).setTextColor(
                                                            ResourcesCompat.getColor(getResources(), R.color.bar_red, null)
                                                    );
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }, 50);
        }
    }

    @Override
    public void onProfileImageUrlRetrieved(String url) {
        interceptTouch = false;
        hasProfileImage = url != null;
        updateProfileImageWithImageUrl(url);
    }

    @Override
    public boolean isBackwardCompatible() {
        return true;
    }

    protected abstract void updateProfileImageWithImageUrl(String url);

}
