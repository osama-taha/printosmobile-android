package com.hp.printosmobile.presentation.modules.shared;

import android.content.Context;
import android.support.v4.content.res.ResourcesCompat;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.view.View;
import android.widget.TextView;

import com.hp.printosmobile.R;
import com.hp.printosmobile.data.remote.models.PreferencesData;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitEnum;
import com.hp.printosmobile.utils.CustomTypefaceSpan;
import com.hp.printosmobile.utils.HPLocaleUtils;
import com.hp.printosmobile.utils.HPScoreUtils;
import com.hp.printosmobile.utils.HPUIUtils;
import com.hp.printosmobilelib.ui.widgets.HPProgressBar;
import com.hp.printosmobilelib.ui.widgets.TypefaceManager;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by anwar asbah on 10/8/2016.
 */
public class DevicesUtils {

    private static final double DEVICE_DATA_NOT_UP_TO_DATE_ALPHA = 0.4;
    private static final float BAR_CORNER_RADIUS = 0;
    private static final float TARGET_INDICATOR_RADIUS = 3;

    private DevicesUtils() {
    }

    public static void updateProgressBar(Context context, BusinessUnitEnum businessUnitEnum, HPProgressBar bar, double max, double target, double current) {
        updateProgressBar(context, businessUnitEnum, bar, max, target, current, false);
    }

    public static void updateProgressBar(Context context, BusinessUnitEnum businessUnitEnum, HPProgressBar bar, double max, double target, double current, boolean addAlpha) {

        int primaryColor = HPScoreUtils.getProgressColor(context, businessUnitEnum, current, target, false);
        int secondaryColor = HPScoreUtils.getProgressColor(context, businessUnitEnum, current, target, true);

        int color = addAlpha ? HPUIUtils.getColorWithAlpha(primaryColor, DEVICE_DATA_NOT_UP_TO_DATE_ALPHA) : primaryColor;

        bar.setColors(color, secondaryColor);
        bar.setTargetIndicatorRadius(TARGET_INDICATOR_RADIUS)
                .setProgressBarCorner(BAR_CORNER_RADIUS)
                .setValues((int) current, (int) max, (int) target);
    }

    public static void updateProgressBar(Context context, BusinessUnitEnum businessUnitEnum, HPProgressBar bar, double max, double target, double current, float indicatorRadius, float barCornerRadius, boolean isBackgroundGrayed) {

        int trackColor = isBackgroundGrayed ?
                ResourcesCompat.getColor(context.getResources(), R.color.bar_bg_gray, null) :
                HPScoreUtils.getProgressColor(context, businessUnitEnum, current, target, true);

        int progressColor = HPScoreUtils.getProgressColor(context, businessUnitEnum, current, target, false);

        updateProgressBar(bar, progressColor, trackColor, max, target, current, indicatorRadius, barCornerRadius);

    }


    public static void updateProgressBar(HPProgressBar bar, int progressColor, int trackColor, double max, double target, double current, float indicatorRadius, float barCornerRadius) {

        bar.setColors(progressColor, trackColor);
        bar.setTargetIndicatorRadius(indicatorRadius)
                .setProgressBarCorner(barCornerRadius)
                .setValues((int) current, (int) max, (int) target);
    }

    public static SpannableStringBuilder getPrinted(Context context, DeviceViewModel indigoDeviceViewModel, PreferencesData.UnitSystem unitSystem) {

        int sheets = indigoDeviceViewModel.getSheets();
        int meters = indigoDeviceViewModel.getMeters();
        int metersSquare = indigoDeviceViewModel.getMetersSquare();
        List<String> strings = new ArrayList<>();
        if (sheets >= 0) {
            strings.add(sheets == 1 ? context.getString(R.string.printed_value_one_sheet)
                    : String.format(context.getString(R.string.printed_value_sheets), getPrintedValue(sheets)));
        }
        if (meters >= 0) {
            strings.add(Unit.METER.getUnitTextWithValue(context, unitSystem, Unit.UnitStyle.VALUE, getPrintedValue(meters)));
        }
        if (metersSquare >= 0) {
            strings.add(Unit.SQM.getUnitTextWithValue(context, unitSystem, Unit.UnitStyle.VALUE, getPrintedValue(metersSquare)));
        }

        String toReturn = "";
        for (int i = 0; i < strings.size(); i++) {
            toReturn = toReturn + strings.get(i);
            if (i != strings.size() - 1) {
                toReturn = toReturn + ", ";
            }
        }

        String impressionLabel = String.format(" (%s)", indigoDeviceViewModel.getImpressionType());

        toReturn += impressionLabel;

        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(toReturn);

        int index = toReturn.indexOf(impressionLabel);

        if (index > -1) {

            spannableStringBuilder.setSpan(new CustomTypefaceSpan("", TypefaceManager.getTypeface(context,
                    TypefaceManager.HPTypeface.HP_BOLD)), 0, index,
                    Spannable.SPAN_INCLUSIVE_INCLUSIVE);

            spannableStringBuilder.setSpan(new CustomTypefaceSpan("", TypefaceManager.getTypeface(context,
                    TypefaceManager.HPTypeface.HP_REGULAR)),
                    index, index + impressionLabel.length(),
                    Spannable.SPAN_INCLUSIVE_INCLUSIVE);

        }

        return spannableStringBuilder;
    }

    public static String getPrintedValue(int value) {

        return HPLocaleUtils.getLocalizedValue(value);
    }

    public static void updateJobsInQueue(Context context, DeviceViewModel deviceViewModel, boolean hasQueuedJobs,
                                         TextView jobsInQueueTextView, TextView printingTimeTextView, View timeView) {

        int jobsInQueue = deviceViewModel.getJobsInQueue();

        jobsInQueueTextView.setText(getJobsInQueueString(context, jobsInQueue, hasQueuedJobs));

        int durationToDone = deviceViewModel.getDurationToDone();
        if (durationToDone > 0) {
            printingTimeTextView.setText(HPLocaleUtils.getLocalizedTimeString(context, deviceViewModel.getDurationToDone(), deviceViewModel.getDurationToDoneUnit()));
        }

        timeView.setVisibility(jobsInQueue == 0 || durationToDone <= 0 ? View.GONE : View.VISIBLE);
    }

    public static SpannableStringBuilder getJobsInQueueString(Context context, int jobsInQueue) {

        return getJobsInQueueString(context, jobsInQueue, true);
    }

    public static SpannableStringBuilder getJobsInQueueString(Context context, int jobsInQueue, boolean hasQueuedJobs) {

        String jobsInQueueString;
        if (jobsInQueue == 0) {
            jobsInQueueString = context.getString(hasQueuedJobs ? R.string.latex_no_jobs_in_queue : R.string.jobs_no_info_msg);
        } else if (jobsInQueue == 1) {
            jobsInQueueString = context.getString(R.string.one_job_in_queue);
        } else {
            jobsInQueueString = String.format(context.getString(R.string.jobs_in_queue), HPLocaleUtils.getLocalizedValue(jobsInQueue));
        }

        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(jobsInQueueString);

        String number = String.valueOf(jobsInQueue);
        int index = jobsInQueueString.indexOf(number);

        spannableStringBuilder.setSpan(new CustomTypefaceSpan("", TypefaceManager.getTypeface(context,
                TypefaceManager.HPTypeface.HP_REGULAR)), 0, jobsInQueueString.length(),
                Spannable.SPAN_INCLUSIVE_INCLUSIVE);

        if (index > -1) {
            spannableStringBuilder.setSpan(new CustomTypefaceSpan("", TypefaceManager.getTypeface(context,
                    TypefaceManager.HPTypeface.HP_BOLD)),
                    index, index + number.length(),
                    Spannable.SPAN_INCLUSIVE_INCLUSIVE);

        }

        return spannableStringBuilder;
    }

    public static void updateTimeInState(Context context, DeviceViewModel deviceViewModel, TextView timeInStateTextView, TextView stateTextView) {

        if (deviceViewModel.isPressDisconnected()) {
            timeInStateTextView.setVisibility(View.GONE);
            stateTextView.setText(context.getString(R.string.press_disconnected));
            stateTextView.setTextColor(ResourcesCompat.getColor(context.getResources(), R.color.c1, null));

        } else {

            stateTextView.setText(getDeviceStateString(context, deviceViewModel));
            stateTextView.setTextColor(ResourcesCompat.getColor(context.getResources(), deviceViewModel.getDeviceState().getColor(), null));
            timeInStateTextView.setVisibility(View.VISIBLE);

            if (deviceViewModel.getTimeInStateMin() < 1) {
                timeInStateTextView.setText(context.getString(R.string.press_entered_state));
                timeInStateTextView.setTextColor(ResourcesCompat.getColor(context.getResources(), deviceViewModel.getDeviceState().getColor(), null));
            } else {
                timeInStateTextView.setText(HPLocaleUtils.getLocalizedTimeString(context, deviceViewModel.getTimeInStateMin(), TimeUnit.MINUTES));
            }

        }
    }

    /**
     * A method to get a string representing the device state based on a given state value.
     */
    public static String getDeviceStateString(Context context, DeviceViewModel deviceViewModel) {

        if (deviceViewModel.getDeviceState() == DeviceState.UNW) {
            return deviceViewModel.getDeviceStateDisplayName();
        }

        return context.getString(deviceViewModel.getDeviceState().getDisplayNameResourceId());
    }

    /**
     * A method to get an integer resource for a drawable representing the maintenance state based on a given state value.
     */
    public static int getTodayPanelDeviceMaintenanceImage(MaintenanceState state) {

        switch (state) {
            case OK:
                return R.drawable.maintenance_good_indication;
            case WARNING:
                return R.drawable.maintenance_maintenance_indication;
            case ERROR:
                return R.drawable.maintenance_error_indication;
            default:
                return R.drawable.offline_unknown;
        }
    }

    /**
     * A method to get an integer resource for a drawable representing the maintenance state based on a given state value.
     */
    public static int getDeviceListMaintenanceImage(MaintenanceState state) {

        switch (state) {
            case OK:
                return R.drawable.device_list_maintenance_good;
            case WARNING:
                return R.drawable.device_list_maintenance_average;
            case ERROR:
                return R.drawable.device_list_maintenance_bad;
            default:
                return 0;
        }
    }

    /**
     * A method to get an integer resource for a drawable representing the maintenance state based on a given state value.
     */
    public static int getDeviceDetailsMaintenanceImage(MaintenanceState state) {

        switch (state) {
            case OK:
                return R.drawable.device_details_maintenance_good;
            case WARNING:
                return R.drawable.device_details_maintenance_average;
            case ERROR:
                return R.drawable.device_details_maintenance_bad;
            default:
                return 0;
        }
    }

    public static float getPercentageRemaining(float level, float capacity) {
        return (level / capacity) * 100;
    }

    public static int getPressImage(BusinessUnitEnum businessUnitEnum, String pressModel) {

        switch (businessUnitEnum) {
            case IHPS_PRESS:

                if (pressModel.equalsIgnoreCase("HP T200")
                        || pressModel.equalsIgnoreCase("HP T230")
                        || pressModel.equalsIgnoreCase("HP T235")
                        || pressModel.equalsIgnoreCase("HP T240 HD")) {
                    return R.drawable.pwp_t200;
                } else if (pressModel.equalsIgnoreCase("HP T260M")
                        || pressModel.equalsIgnoreCase("HP T260ML")) {
                    return R.drawable.pwp_t260m;
                } else if (pressModel.equalsIgnoreCase("HP T300")
                        || pressModel.equalsIgnoreCase("HP T300M")
                        || pressModel.equalsIgnoreCase("HP T350")
                        || pressModel.equalsIgnoreCase("HP T350M")
                        || pressModel.equalsIgnoreCase("HP T360")
                        || pressModel.equalsIgnoreCase("HP T360M")) {
                    return R.drawable.pwp_t300m;
                } else if (pressModel.equalsIgnoreCase("HP T400")
                        || pressModel.equalsIgnoreCase("HP T400M")
                        || pressModel.equalsIgnoreCase("HP T410")
                        || pressModel.equalsIgnoreCase("HP T410M")
                        || pressModel.equalsIgnoreCase("HP T470 HD")
                        || pressModel.equalsIgnoreCase("HP T480 HD")
                        || pressModel.equalsIgnoreCase("HP T490 HD")
                        || pressModel.equalsIgnoreCase("HP T490M HD")
                        || pressModel.equalsIgnoreCase("HP T480_HD")) {
                    return R.drawable.pwp_t400;
                } else if (pressModel.equalsIgnoreCase("HP T400S")) {
                    return R.drawable.pwp_t400s;
                } else if (pressModel.equalsIgnoreCase("HP T1100S")) {
                    return R.drawable.pwp_t1100s;
                }

                break;
            case LATEX_PRINTER:

                if (pressModel.startsWith("HP LATEX")) {
                    //Support other servers.
                    pressModel = pressModel.replace("HP LATEX", "HP LF");
                }

                if (pressModel.equalsIgnoreCase("HP LF 335 PRINTER")) {
                    return R.drawable.hp_latex_335_printer;
                } else if (pressModel.equalsIgnoreCase("HP LF 110 PRINTER")) {
                    return R.drawable.hp_latex_110_printer;
                } else if (pressModel.equalsIgnoreCase("HP LF 115 PRINTER")) {
                    return R.drawable.hp_latex_110_printer;
                } else if (pressModel.equalsIgnoreCase("HP LF 3500 PRINTER")) {
                    return R.drawable.hp_latex_3500_printer;
                } else if (pressModel.equalsIgnoreCase("HP LF 3800 PRINTER")) {
                    return R.drawable.hp_latex_3800_printer;
                } else if (pressModel.equalsIgnoreCase("HP LF 1500 PRINTER")) {
                    return R.drawable.hp_latex_1500_printer;
                } else if (pressModel.equalsIgnoreCase("HP LF 360 PRINTER")) {
                    return R.drawable.hp_latex_360_printer;
                } else if (pressModel.equalsIgnoreCase("HP LF 280 PRINTER")) {
                    return R.drawable.hp_latex_280_printer;
                } else if (pressModel.equalsIgnoreCase("HP LF 3600 PRINTER")) {
                    return R.drawable.hp_latex_3500_printer;
                } else if (pressModel.equalsIgnoreCase("HP LF 3000 PRINTER")) {
                    return R.drawable.hp_latex_3000_printer;
                } else if (pressModel.equalsIgnoreCase("HP LF 365 PRINTER")) {
                    return R.drawable.hp_latex_365_printer;
                } else if (pressModel.equalsIgnoreCase("HP LF 310 PRINTER")) {
                    return R.drawable.hp_latex_310_printer;
                } else if (pressModel.equalsIgnoreCase("HP LF 370 PRINTER")) {
                    return R.drawable.hp_latex_375_printer;
                } else if (pressModel.equalsIgnoreCase("HP LF 3100 PRINTER")) {
                    return R.drawable.hp_latex_3100_printer;
                } else if (pressModel.equalsIgnoreCase("HP LF 375 PRINTER")) {
                    return R.drawable.hp_latex_375_printer;
                } else if (pressModel.equalsIgnoreCase("HP LF 315 PRINTER")) {
                    return R.drawable.hp_latex_315_printer;
                } else if (pressModel.equalsIgnoreCase("HP LF 560 PRINTER")) {
                    return R.drawable.hp_latex_560_printer;
                } else if (pressModel.equalsIgnoreCase("HP LF 3200 PRINTER")) {
                    return R.drawable.hp_latex_3100_printer;
                } else if (pressModel.equalsIgnoreCase("HP LF 570 PRINTER")) {
                    return R.drawable.hp_latex_570_printer;
                } else if (pressModel.equalsIgnoreCase("HP LF 330 PRINTER")) {
                    return R.drawable.hp_latex_330_printer;
                } else if (pressModel.equalsIgnoreCase("T8W15A")
                        || pressModel.equalsIgnoreCase("T8W16A")
                        || pressModel.equalsIgnoreCase("T8W18A")
                        || pressModel.equalsIgnoreCase("T8W18B")
                        || pressModel.equalsIgnoreCase("X9D24A")
                        || pressModel.equalsIgnoreCase("X9D24B")
                        || pressModel.equalsIgnoreCase("W3Z71A")
                        || pressModel.equalsIgnoreCase("W3Z72A")) {
                    return R.drawable.jaguar;
                } else if (pressModel.equalsIgnoreCase("K0Q45A")
                        || pressModel.equalsIgnoreCase("K0Q46A")) {
                    return R.drawable.skaar;
                } else if (pressModel.equalsIgnoreCase("2ET72A") || pressModel.equalsIgnoreCase("HP Stitch S300 64in")) {
                    return R.drawable.hp_latex_365ds;
                } else if (pressModel.equalsIgnoreCase("2ET73A") || pressModel.equalsIgnoreCase("HP Stitch S500 64in")) {
                    return R.drawable.hp_latex_570ds;
                }

                break;

            case SCITEX_PRESS:

                if (pressModel.equalsIgnoreCase("HP Scitex 11000") || pressModel.equalsIgnoreCase("CX100A") || pressModel.equalsIgnoreCase("CX102A") || pressModel.equalsIgnoreCase("CX102AR")) {
                    return R.drawable.scitex_11000;
                } else if (pressModel.equalsIgnoreCase("HP Scitex 9000") || pressModel.equalsIgnoreCase("CX109A")) {
                    return R.drawable.scitex_9000;
                } else if (pressModel.equalsIgnoreCase("HP Scitex 15500") || pressModel.equalsIgnoreCase("CX110A") || pressModel.equalsIgnoreCase("CX112A") || pressModel.equalsIgnoreCase("CX112AR")) {
                    return R.drawable.scitex_15500;
                } else if (pressModel.equalsIgnoreCase("HP Scitex 17000") || pressModel.equalsIgnoreCase("CX120A")) {
                    return R.drawable.scitex_17000;
                } else if (pressModel.equalsIgnoreCase("HP Scitex C500") || pressModel.equalsIgnoreCase("CX200A")) {
                    return R.drawable.scitex_c500;
                }
                break;

            case INDIGO_PRESS:

                if (pressModel.equalsIgnoreCase("HP Indigo 3500")) {
                    return R.drawable.indigo_3500;
                }
                if (pressModel.equalsIgnoreCase("HP Indigo 3600")) {
                    return R.drawable.indigo_3500;
                } else if (pressModel.equalsIgnoreCase("HP Indigo 3550")) {
                    return R.drawable.indigo_3550;
                } else if (pressModel.equalsIgnoreCase("HP Indigo 5000")) {
                    return R.drawable.indigo_5000;
                } else if (pressModel.equalsIgnoreCase("HP Indigo 5500")) {
                    return R.drawable.indigo_5500;
                } else if (pressModel.equalsIgnoreCase("HP Indigo 5600")) {
                    return R.drawable.indigo_5600;
                } else if (pressModel.equalsIgnoreCase("HP Indigo 5900")) {
                    return R.drawable.indigo_5900;
                } else if (pressModel.equalsIgnoreCase("HP Indigo 6000p")) {
                    return R.drawable.indigo_6000p;
                } else if (pressModel.equalsIgnoreCase("HP Indigo 6600p")) {
                    return R.drawable.indigo_6600p;
                } else if (pressModel.equalsIgnoreCase("HP Indigo WS6800p")) {
                    return R.drawable.indigo_6800p;
                } else if (pressModel.equalsIgnoreCase("HP Indigo W7200")) {
                    return R.drawable.indigo_w7200;
                } else if (pressModel.equalsIgnoreCase("HP Indigo W7250")) {
                    return R.drawable.indigo_w7250;
                } else if (pressModel.equalsIgnoreCase("HP Indigo 7000")) {
                    return R.drawable.indigo_7000;
                } else if (pressModel.equalsIgnoreCase("HP Indigo 7500")) {
                    return R.drawable.indigo_7500;
                } else if (pressModel.equalsIgnoreCase("HP Indigo 7600")) {
                    return R.drawable.indigo_7600;
                } else if (pressModel.equalsIgnoreCase("HP Indigo 7800")) {
                    return R.drawable.indigo_7800;
                } else if (pressModel.equalsIgnoreCase("HP Indigo 7900")) {
                    return R.drawable.indigo_7900;
                } else if (pressModel.equalsIgnoreCase("HP Indigo 8000")) {
                    return R.drawable.indigo_8000;
                } else if (pressModel.equalsIgnoreCase("HP Indigo 10000") || pressModel.equalsIgnoreCase("HP Indigo 10000HD Digital Press")) {
                    return R.drawable.indigo_10000;
                } else if (pressModel.equalsIgnoreCase("HP Indigo 12000") || pressModel.equalsIgnoreCase("HP Indigo 12000HD Digital Press")) {
                    return R.drawable.indigo_12000;
                } else if (pressModel.equalsIgnoreCase("HP Indigo 20000")) {
                    return R.drawable.indigo_20000;
                } else if (pressModel.equalsIgnoreCase("HP Indigo 30000")) {
                    return R.drawable.indigo_30000;
                } else if (pressModel.equalsIgnoreCase("HP Indigo 50000")) {
                    return R.drawable.indigo_50000;
                } else if (pressModel.equalsIgnoreCase("HP Indigo WS6000") ||
                        pressModel.equalsIgnoreCase("HP Indigo WS6000p")) {
                    return R.drawable.indigo_ws6000;
                } else if (pressModel.equalsIgnoreCase("HP Indigo WS6600") ||
                        pressModel.equalsIgnoreCase("HP Indigo WS6600p")) {
                    return R.drawable.indigo_ws6600;
                } else if (pressModel.equalsIgnoreCase("HP Indigo WS6800") ||
                        pressModel.equalsIgnoreCase("HP Indigo WS6900") ||
                        pressModel.equalsIgnoreCase("HP Indigo 6900")) {
                    return R.drawable.indigo_ws6800;
                } else if (pressModel.equalsIgnoreCase("HP Indigo 5r")) {
                    return R.drawable.indigo_5r;
                } else if (pressModel.equalsIgnoreCase("HP Indigo 6r")) {
                    return R.drawable.indigo_6r;
                } else if (pressModel.equalsIgnoreCase("HP Indigo 7r")) {
                    return R.drawable.indigo_7r;
                }
                break;
            default:
                break;
        }

        return R.drawable.press_default;

    }

    public static SpannableStringBuilder getLitersConsumed(Context context, PreferencesData.UnitSystem unitSystem, double litersConsumed) {

        String roundedValue = HPLocaleUtils.getDecimalString((float) litersConsumed, true, 2);
        String string = Unit.LITERS.getUnitTextWithValue(context, unitSystem, Unit.UnitStyle.VALUE, roundedValue);

        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(string);

        int index = string.indexOf(roundedValue);

        spannableStringBuilder.setSpan(new CustomTypefaceSpan("", TypefaceManager.getTypeface(context,
                TypefaceManager.HPTypeface.HP_REGULAR)), 0, string.length(),
                Spannable.SPAN_INCLUSIVE_INCLUSIVE);

        if (index > -1) {
            spannableStringBuilder.setSpan(new CustomTypefaceSpan("", TypefaceManager.getTypeface(context,
                    TypefaceManager.HPTypeface.HP_BOLD)),
                    index, index + roundedValue.length(),
                    Spannable.SPAN_INCLUSIVE_INCLUSIVE);

        }

        return spannableStringBuilder;
    }

    public static SpannableStringBuilder getSheetsPrinted(Context context, int sheets) {

        String sheetsString = HPLocaleUtils.getLocalizedValue(sheets);
        String string = context.getString(R.string.sheets_printed_value, sheetsString);
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(string);

        int index = string.indexOf(sheetsString);

        spannableStringBuilder.setSpan(new CustomTypefaceSpan("", TypefaceManager.getTypeface(context,
                TypefaceManager.HPTypeface.HP_REGULAR)), 0, string.length(),
                Spannable.SPAN_INCLUSIVE_INCLUSIVE);

        if (index > -1) {
            spannableStringBuilder.setSpan(new CustomTypefaceSpan("", TypefaceManager.getTypeface(context,
                    TypefaceManager.HPTypeface.HP_BOLD)),
                    index, index + sheetsString.length(),
                    Spannable.SPAN_INCLUSIVE_INCLUSIVE);

        }

        return spannableStringBuilder;
    }


}
