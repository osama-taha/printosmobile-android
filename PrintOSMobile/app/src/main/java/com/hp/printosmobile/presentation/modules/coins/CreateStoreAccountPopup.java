package com.hp.printosmobile.presentation.modules.coins;

import android.app.Dialog;
import android.app.DialogFragment;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hp.printosmobile.Analytics;
import com.hp.printosmobile.R;
import com.hp.printosmobile.data.local.PrintOSPreferences;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Osama Taha on 31/07/2017.
 */
public class CreateStoreAccountPopup extends DialogFragment {

    public static final String TAG = CreateStoreAccountPopup.class.getName();

    @Bind(R.id.description_text_view)
    TextView descriptionTextView;
    @Bind(R.id.button_cancel)
    TextView buttonNoThanks;

    private CreateStoreAccountPopupCallback callbacks;

    public static CreateStoreAccountPopup getInstance(CreateStoreAccountPopupCallback popupCallback) {
        CreateStoreAccountPopup dialog = new CreateStoreAccountPopup();

        dialog.callbacks = popupCallback;
        Bundle bundle = new Bundle();
        dialog.setArguments(bundle);

        return dialog;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.CreateStoreAccountPopupStyle);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        return inflater.inflate(R.layout.dailog_create_store_account, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ButterKnife.bind(this, view);
        initView();
        getDialog().setCanceledOnTouchOutside(false);
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null && dialog.getWindow() != null) {
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        }
    }

    private void initView() {

        String userEmail = PrintOSPreferences.getInstance().getUserEmail();
        String text = getString(R.string.ranking_leaderboard_create_store_account_message, userEmail);

        int startInd = text.indexOf(userEmail);
        int endInd = startInd + userEmail.length();
        SpannableString spannableString = new SpannableString(text);
        spannableString.setSpan(new StyleSpan(Typeface.BOLD), startInd, endInd, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        descriptionTextView.setText(spannableString);

        String noThanks = getString(R.string.invites_popup_invite_no_thanks);
        SpannableString spannable = new SpannableString(noThanks);
        spannable.setSpan(new UnderlineSpan(), 0, noThanks.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        buttonNoThanks.setText(spannable);

    }

    @OnClick(R.id.button_create_account)
    public void onCreateAccountClicked() {

        if (callbacks != null) {
            Analytics.sendEvent(Analytics.CREATE_STORE_ACCOUNT_CLICKED);
            callbacks.onCreateAccountClicked();
        }

        dismissAllowingStateLoss();
    }

    @OnClick(R.id.button_cancel)
    public void onCancelClicked() {

        if (callbacks != null) {
            Analytics.sendEvent(Analytics.CREATE_STORE_CANCEL_CLICKED);
            callbacks.onCancelClicked();
        }

        dismissAllowingStateLoss();
    }

    public interface CreateStoreAccountPopupCallback {
        void onCreateAccountClicked();
        void onCancelClicked();
    }
}
