package com.hp.printosmobile.presentation.modules.settings.wizard;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;

import com.hp.printosmobile.Constants;
import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.data.remote.ApiServicesProvider;
import com.hp.printosmobile.data.remote.models.CoinsStoreData;
import com.hp.printosmobile.data.remote.models.PreferencesData;
import com.hp.printosmobile.data.remote.services.BeatCoinService;
import com.hp.printosmobile.data.remote.services.PreferencesService;
import com.hp.printosmobile.data.remote.services.TimeZone;
import com.hp.printosmobile.data.remote.services.UserDataService;
import com.hp.printosmobile.presentation.BaseActivity;
import com.hp.printosmobile.presentation.Navigator;
import com.hp.printosmobile.presentation.modules.contacthp.ContactHpManager;
import com.hp.printosmobile.presentation.modules.main.MainActivity;
import com.hp.printosmobile.presentation.modules.settings.ProfileManager;
import com.hp.printosmobile.utils.HPUIUtils;
import com.hp.printosmobile.aaa.AAAManager;
import com.hp.printosmobilelib.core.communications.remote.models.UserData;
import com.hp.printosmobilelib.core.logging.HPLogger;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.functions.Func2;
import rx.functions.Func4;
import rx.schedulers.Schedulers;

/**
 * Created by Osama
 */
public class UserProfileManager {

    private static final String TAG = UserProfileManager.class.getName();

    private static UserProfileManager userProfileManager = new UserProfileManager();

    private UserData.User userData;
    private boolean isShowing;
    private PreferencesData preferencesData;
    private Map<String, String> countriesMap;
    private Map<String, String> timeZonesMap;

    public static UserProfileManager getInstance() {
        return userProfileManager;
    }

    public boolean showAddUserImagePopup(final BaseActivity activity, final UserInfoPopupsCallBacks userInfoPopupsCallBacks) {

        PrintOSPreferences preferences = PrintOSPreferences.getInstance(activity);

        if (!preferences.wizardEnabled() || !preferences.timeToDisplayWizardPopup() || isShowing) {
            preferences.setWizardPopupShown(true);
            return false;
        }

        preferences.setWizardPopupShown(true);

        ProfileManager.getProfileImageInfo()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<String>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        HPLogger.d(TAG, "Unable to get profile image info " + e.getMessage());
                        onDialogDismissed(activity, userInfoPopupsCallBacks);
                    }

                    @Override
                    public void onNext(String url) {

                        PrintOSPreferences.getInstance(activity).resetAppLaunchesCountWizard();

                        onDialogDismissed(activity, userInfoPopupsCallBacks);

                        if (TextUtils.isEmpty(url)) {

                            isShowing = true;

                            Navigator.openAddUserImageActivity(activity);

                        }

                    }
                });

        return true;

    }


    public Observable<Boolean> getProfileData() {

        ApiServicesProvider serviceProvider = PrintOSApplication.getApiServicesProvider();
        UserDataService userDataService = serviceProvider.getUserDataService();

        PreferencesService preferencesService = serviceProvider.getPreferencesService();

        String locale = PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).getLanguage(PrintOSApplication.getAppContext().getString(R.string.default_language));

        return Observable.combineLatest(userDataService.getUser(), preferencesService.getPreferences(), ContactHpManager.getCountries(locale), getTimeZones(), new Func4<Response<UserData.User>, Response<PreferencesData>, Map<String, String>, Map<String, String>, Boolean>() {
            @Override
            public Boolean call(Response<UserData.User> userResponse, Response<PreferencesData> preferencesDataResponse, Map<String, String> countriesMapResponse, Map<String, String> timeZonesResponse) {

                countriesMap = countriesMapResponse;
                timeZonesMap = timeZonesResponse;

                if (userResponse.isSuccessful()) {
                    userData = userResponse.body();
                }

                if (preferencesDataResponse.isSuccessful()) {
                    preferencesData = preferencesDataResponse.body();
                }

                return hasProfileData();
            }

        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public void getEditMyDetailsUrl(final FetchHPIDEditMyDetailsUrlCallback fetchHPIDEditMyDetailsUrlCallback) {

        ApiServicesProvider serviceProvider = PrintOSApplication.getApiServicesProvider();
        UserDataService userDataService = serviceProvider.getUserDataService();

        Call<ResponseBody> editUserDetailsCall = userDataService.getEditUserDetailsUrl();
        editUserDetailsCall.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        String hpidEditMyDetailsUrl = jsonObject
                                .getJSONObject(Constants.API_CONSTANT_REGISTRY)
                                .getJSONObject(Constants.API_CONSTANT_SERVICES)
                                .getJSONObject(Constants.API_CONSTANT_HPID_ACCOUNT)
                                .getString(Constants.API_CONSTANT_SV_URL);
                        if (TextUtils.isEmpty(hpidEditMyDetailsUrl)) {
                            fetchHPIDEditMyDetailsUrlCallback.onFailure();
                        } else {
                            fetchHPIDEditMyDetailsUrlCallback.onSuccess(hpidEditMyDetailsUrl);
                        }
                    } catch (Exception e) {
                        HPLogger.d(TAG, e.getMessage());
                    }
                } else {
                    fetchHPIDEditMyDetailsUrlCallback.onFailure();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable throwable) {
                fetchHPIDEditMyDetailsUrlCallback.onFailure();
            }
        });

    }

    public static Observable<Map<String, String>> getTimeZones() {
        ApiServicesProvider serviceProvider = PrintOSApplication.getApiServicesProvider();
        PreferencesService preferencesService = serviceProvider.getPreferencesService();

        return preferencesService.getTimeZones().map(new Func1<Response<List<TimeZone>>, Map<String, String>>() {
            @Override
            public Map<String, String> call(Response<List<TimeZone>> listResponse) {

                if (listResponse != null && listResponse.isSuccessful() && listResponse.body() != null
                        && !listResponse.body().isEmpty()) {

                    List<String> timeZonesNames = new ArrayList<>();
                    Map<String, String> flippedTimeZonesList = new HashMap<>();
                    for (TimeZone timeZone : listResponse.body()) {
                        if (!TextUtils.isEmpty(timeZone.getValue())) {
                            timeZonesNames.add(timeZone.getValue());
                            flippedTimeZonesList.put(timeZone.getValue(), timeZone.getId());
                        }
                    }

                    Collections.sort(timeZonesNames, new Comparator<String>() {
                        @Override
                        public int compare(String s, String t1) {
                            return s.compareToIgnoreCase(t1);
                        }
                    });

                    Map<String, String> timeZoneMap = new LinkedHashMap<>();
                    for (int i = 0; i < timeZonesNames.size(); i++) {
                        String timeZoneName = timeZonesNames.get(i);
                        if (flippedTimeZonesList.containsKey(timeZoneName)) {
                            timeZoneMap.put(timeZoneName, timeZoneName);
                        }
                    }

                    return timeZoneMap;
                }
                return null;
            }
        });
    }

    public void sendUserInfo(final UserData.User userData, final PreferencesData preferencesData, Subscriber<? super Boolean> subscriber) {

        UserDataService userDataService = PrintOSApplication.getApiServicesProvider().getUserDataService();
        PreferencesService preferencesService = PrintOSApplication.getApiServicesProvider().getPreferencesService();

        Observable.combineLatest(userDataService.updateUser(userData), preferencesService.savePreferences(preferencesData), new Func2<Response<UserData.User>, Response<PreferencesData>, Boolean>() {
            @Override
            public Boolean call(Response<UserData.User> userResponse, Response<PreferencesData> preferencesDataResponse) {

                if (userResponse.isSuccessful()) {

                    HPLogger.d(TAG, "user info has been successfully saved " + userResponse.body());

                    if (userResponse.body() != null) {
                        PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).saveUserInfo(userResponse.body());
                    }

                } else {

                    showErrorSavingPreferences();

                    HPLogger.d(TAG, "response code for set user info " + userResponse.code());
                }

                return userResponse.isSuccessful() && preferencesDataResponse.isSuccessful();

            }
        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).
                subscribe(subscriber);

    }

    private Subscriber<? super Boolean> saveProfileDefaultSubscriber = new Subscriber<Boolean>() {
        @Override
        public void onCompleted() {

        }

        @Override
        public void onError(Throwable e) {

            HPLogger.d(TAG, "failed to save user info " + e);
            showErrorSavingPreferences();
        }

        @Override
        public void onNext(Boolean aBoolean) {

        }
    };

    public Observable<String> createCoinsClaimLink(Context context, int coinsAmount, int expiryInDays) {

        List<CoinsStoreData> body = new ArrayList();

        CoinsStoreData coinsStoreData = new CoinsStoreData();
        coinsStoreData.setAmount(coinsAmount);
        coinsStoreData.setExpires(expiryInDays);
        coinsStoreData.setEmail(PrintOSPreferences.getInstance(context).getUserEmail());
        coinsStoreData.setCustomerType("External");
        body.add(coinsStoreData);

        ApiServicesProvider serviceProvider = PrintOSApplication.getApiServicesProvider();
        BeatCoinService beatCoinService = serviceProvider.getBeatCoinService();

        return beatCoinService.createCoinsUrl(body)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).map(new Func1<Response<List<CoinsStoreData>>, String>() {
                    @Override
                    public String call(Response<List<CoinsStoreData>> listResponse) {

                        if (listResponse.isSuccessful() && !listResponse.body().isEmpty()) {
                            return listResponse.body().get(0).getClaimUrl();
                        }

                        return null;
                    }
                });
    }

    public boolean hasProfileData() {
        return getUserData() != null && getPreferencesData() != null && getCountriesMap() != null
                && !getCountriesMap().isEmpty() && getTimeZonesMap() != null
                && !getTimeZonesMap().isEmpty();
    }


    public UserData.User getUserData() {
        return userData;
    }

    public PreferencesData getPreferencesData() {
        return preferencesData;
    }

    public Map<String, String> getCountriesMap() {
        return countriesMap;
    }

    public Map<String, String> getTimeZonesMap() {
        return timeZonesMap;
    }

    public void sendWizardResults(WizardViewModel viewModel, Subscriber<? super Boolean> subscriber) {

        if (viewModel == null || preferencesData == null || userData == null) {
            return;
        }

        userData.setFirstName(viewModel.getFirstName());
        userData.setLastName(viewModel.getLastName());
        userData.setDisplayName(viewModel.getDisplayName());
        userData.setPrimaryPhone(viewModel.getPhoneNumber());
        userData.setAddress(new UserData.Address());
        userData.getAddress().setAddressLine1(viewModel.getAddress1());
        userData.getAddress().setAddressLine2(viewModel.getAddress2());
        userData.getAddress().setCountry(viewModel.getCountry());
        userData.getAddress().setRegion(viewModel.getRegion());
        userData.getAddress().setLocality(viewModel.getCity());
        userData.getAddress().setPostalCode(viewModel.getPostalCode());

        if (preferencesData.getGeneral() != null) {
            preferencesData.getGeneral().setTimeZone(viewModel.getTimeZone());
        }

        Subscriber rxSubscriber = subscriber == null ? saveProfileDefaultSubscriber : subscriber;

        sendUserInfo(userData, preferencesData, rxSubscriber);

    }


    public void onDialogDismissed(Activity activity, UserInfoPopupsCallBacks callBacks) {

        if (activity != null && activity instanceof MainActivity) {
            ((MainActivity) activity).onDialogDismissed();
        }

        if (callBacks != null) {
            callBacks.onFinishCheckingUserInfoPopups();
        }

        isShowing = false;

    }

    public void showErrorSavingPreferences() {
        HPUIUtils.displayToast(PrintOSApplication.getAppContext(), PrintOSApplication.getAppContext()
                .getString(R.string.error_failed_to_save_preferences));
    }

    public interface UserInfoPopupsCallBacks {
        void onFinishCheckingUserInfoPopups();
    }

    public interface FetchHPIDEditMyDetailsUrlCallback {
        void onSuccess(String hpidEditMyDetailsUrl);

        void onFailure();
    }

}
