package com.hp.printosmobile.presentation.modules.insights.kz;

import com.hp.printosmobile.R;

/**
 * Created by Osama on 3/15/18.
 */

public enum KZSupportedFormat {

    PDF("pdf", R.color.c208, R.color.hp_default),
    URL("url", R.color.c210,android.R.color.white),
    HTM("htm", R.color.c210,android.R.color.white),
    HTML("html", R.color.c210,android.R.color.white),
    MP4("mp4", R.color.c209,android.R.color.white),
    UNKNOWN("unknown", R.color.c208,R.color.hp_default);

    private final String value;
    private final int cardBackgroundColorResId;
    private final int cardElementsDefaultColorResId;

    KZSupportedFormat(String value, int cardBackgroundColorResId, int cardElementsDefaultColorResId) {
        this.value = value;
        this.cardBackgroundColorResId = cardBackgroundColorResId;
        this.cardElementsDefaultColorResId = cardElementsDefaultColorResId;
    }

    public String getValue() {
        return value;
    }

    public int getCardBackgroundColorResId() {
        return cardBackgroundColorResId;
    }

    public int getCardDefaultColorResId() {
        return cardElementsDefaultColorResId;
    }

    public static KZSupportedFormat from(String value) {

        if (value == null) {
            return UNKNOWN;
        }

        for (KZSupportedFormat kzFormat : KZSupportedFormat.values()) {
            if (kzFormat.getValue().equalsIgnoreCase(value)) {
                return kzFormat;
            }
        }

        return UNKNOWN;

    }

}
