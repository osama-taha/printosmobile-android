package com.hp.printosmobile.presentation.modules.next10jobs;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hp.printosmobile.R;
import com.hp.printosmobile.presentation.modules.shared.DeviceViewModel;
import com.hp.printosmobile.utils.HPLocaleUtils;

import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Anwar Asbah on 12/6/2016.
 */
class JobsChildListAdapter extends RecyclerView.Adapter<JobsChildListAdapter.JobViewHolder> {

    private Context context;
    private List<DeviceViewModel.Job> jobs = null;
    private DeviceViewModel.JobType jobType;

    JobsChildListAdapter(Context context, List<DeviceViewModel.Job> jobs, DeviceViewModel.JobType jobType) {
        this.context = context;
        this.jobs = jobs;
        this.jobType = jobType;
    }

    @Override
    public JobViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ViewGroup jobView = (ViewGroup) inflater.inflate(R.layout.fragment_next_10_jobs_child_item, parent, false);
        return new JobViewHolder(jobView);
    }

    @Override
    public void onBindViewHolder(JobViewHolder holder, int position) {
        DeviceViewModel.Job job = jobs.get(position);
        holder.jobName.setText(job.getName());
        if (jobType.isHasTimeParameters()) {
            holder.jobTime.setText(HPLocaleUtils.getLocalizedTimeStringV2(context, job.getDuration(), job.getTimeUnit()));
        }
    }

    int getTotalTimeInSeconds() {
        int totalTime = 0;
        if (jobs != null) {
            for (DeviceViewModel.Job job : jobs) {
                totalTime += TimeUnit.SECONDS.convert(job.getDuration(), job.getTimeUnit());
            }
        }
        return totalTime;
    }

    @Override
    public int getItemCount() {
        return jobs == null ? 0 : jobs.size();
    }

    class JobViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.job_name_text_view)
        TextView jobName;
        @Bind(R.id.job_time_text_view)
        TextView jobTime;

        JobViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
