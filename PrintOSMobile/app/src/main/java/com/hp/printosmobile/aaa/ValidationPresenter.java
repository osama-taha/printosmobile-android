package com.hp.printosmobile.aaa;

import android.content.Context;

import com.hp.printosmobile.AnswersSdk;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.data.remote.models.AccountType;
import com.hp.printosmobile.presentation.Presenter;
import com.hp.printosmobilelib.core.communications.remote.APIException;
import com.hp.printosmobilelib.core.communications.remote.Preferences;
import com.hp.printosmobilelib.core.communications.remote.models.UserData;
import com.hp.printosmobilelib.core.logging.HPLogger;

import okhttp3.ResponseBody;
import retrofit2.Response;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Anwar Asbah on 6/7/2017.
 */

public class ValidationPresenter extends Presenter<ValidationView> {

    public static final String TAG = ValidationPresenter.class.getName();

    private static final long HP_USER_VALIDATION_PERIOD_MILLS = (long) (5 * 60 * 1000);

    public void authenticate(final Context context, final boolean isFirstCreation) {

        long lastSessionValidationTime = PrintOSPreferences.getInstance(context).getLastSessionValidationTime();
        long timeDifferenceSinceLastSession = System.currentTimeMillis() - lastSessionValidationTime;
        boolean timePassed = timeDifferenceSinceLastSession > HP_USER_VALIDATION_PERIOD_MILLS;
        boolean isHPUser = PrintOSPreferences.getInstance(context).getAccountType() == AccountType.HP;

        if (timePassed && isHPUser) {
            //Skip the validation if more than 5 minutes passed since last session.
            HPLogger.i(TAG, "HP user, start auto login....");
            login(context, isFirstCreation);
            PrintOSPreferences.getInstance(context).setLastSessionValidationTime();
            return;
        }

        HPLogger.d(TAG, "Start validate... checking cookie");
        Subscription subscription = AAAManager.validateToken()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<UserData.User>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        HPLogger.d(TAG, "Cookie is not valid");
                        if (e instanceof APIException && ((APIException) e).getKind() == APIException.Kind.NETWORK) {
                            if (mView != null) {
                                mView.onValidationError();
                            }
                            return;
                        }
                        login(context, isFirstCreation);
                    }

                    @Override
                    public void onNext(UserData.User user) {
                        HPLogger.d(TAG, "Token Valid");
                        if (mView != null) {
                            mView.onValidationCompleted(user);
                        }
                    }
                });

        addSubscriber(subscription);
    }

    public void login(final Context context, boolean isFirstCreation) {

        HPLogger.d(TAG, "Start AutoLogin.");

        if (mView != null) {
            mView.onPreAutoLogin(isFirstCreation);
        }

        Subscription subscription = AAAManager.performAutoLogin(context)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<Response<UserData>>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {

                        if (e instanceof APIException) {

                            APIException.Kind errorKind = ((APIException) e).getKind();

                            if (errorKind == APIException.Kind.UNAUTHORIZED) {
                                HPLogger.d(TAG, "AutoLogin Unauthorized");
                                if (mView != null) {
                                    mView.onValidationUnauthorized();
                                }
                                return;
                            }

                            if (errorKind == APIException.Kind.URL_NOT_FOUND_ERROR
                                    || errorKind == APIException.Kind.SERVICE_UNAVAILABLE
                                    || errorKind == APIException.Kind.INTERNAL_SERVER_ERROR) {
                                if (mView != null) {
                                    mView.onServerDown();
                                }
                                return;
                            }
                        }

                        HPLogger.d(TAG, "AutoLogin failed: " + e.getMessage());
                        if (mView != null) {
                            mView.onValidationError();
                        }
                    }

                    @Override
                    public void onNext(Response<UserData> userData) {

                        HPLogger.d(TAG, "AutoLogin Success" + userData.toString());

                        if (userData.isSuccessful()) {
                            AnswersSdk.logLogin(AnswersSdk.LOGIN_METHOD_RENEW_SESSION, true);
                            updateContext(context, userData.body());
                        } else {

                            AnswersSdk.logLogin(AnswersSdk.LOGIN_METHOD_RENEW_SESSION, false);

                            if (userData.code() == APIException.URL_NOT_FOUND_ERROR_CODE ||
                                    userData.code() == APIException.SERVICE_UNAVAILABLE_ERROR_CODE ||
                                    userData.code() == APIException.INTERNAL_SERVER_ERROR_CODE) {
                                if (mView != null) {
                                    mView.onServerDown();
                                }
                                return;
                            }

                            if (userData.code() >= 400 && userData.code() < 500) {
                                HPLogger.d(TAG, "AutoLogin fail: Unauthorized");
                                if (mView != null) {
                                    mView.onValidationUnauthorized();
                                }
                                return;
                            }
                            HPLogger.d(TAG, "AutoLogin failed");
                            if (mView != null) {
                                mView.onValidationError();
                            }
                        }
                    }
                });

        addSubscriber(subscription);
    }


    private void updateContext(Context context, final UserData userData) {

        if (Preferences.getInstance(context).getSavedOrganizationId() != null) {

            HPLogger.d(TAG, "Start Change context.");

            Subscription subscription = AAAManager.changeContext(context)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(new Subscriber<ResponseBody>() {
                        @Override
                        public void onCompleted() {
                        }

                        @Override
                        public void onError(Throwable e) {
                            HPLogger.d(TAG, "Change context failed");
                            if (mView != null) {
                                mView.onValidationCompletedWithoutContextChange(userData == null ? null :
                                        userData.getUser());
                            }
                        }

                        @Override
                        public void onNext(ResponseBody responseBody) {
                            HPLogger.d(TAG, "Change context success");
                            if (mView != null) {
                                mView.onValidationCompleted(userData == null ? null : userData.getUser());
                            }
                        }
                    });

            addSubscriber(subscription);

        } else if (userData != null) {
            HPLogger.d(TAG, "no context saved");
            UserData.Context organization = userData.getContext();
            Preferences.getInstance(context).saveOrganization(organization);
            if (mView != null) {
                mView.onValidationCompletedWithoutContextChange(userData.getUser());
            }
        }
    }

    @Override
    public void detachView() {
        stopSubscribers();
        mView = null;
    }
}
