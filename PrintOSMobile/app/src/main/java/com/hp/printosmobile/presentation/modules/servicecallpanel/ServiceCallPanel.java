package com.hp.printosmobile.presentation.modules.servicecallpanel;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.v4.content.res.ResourcesCompat;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

import com.hp.printosmobile.AnswersSdk;
import com.hp.printosmobile.Constants;
import com.hp.printosmobile.R;
import com.hp.printosmobile.presentation.modules.home.Panel;
import com.hp.printosmobile.presentation.modules.servicecallpanel.ServiceCallViewModel.ServiceCallStateEnum;
import com.hp.printosmobile.presentation.modules.shared.DeepLinkUtils;
import com.hp.printosmobile.presentation.modules.shared.PanelShareCallbacks;
import com.hp.printosmobile.utils.FileUtils;
import com.hp.printosmobile.utils.HPLocaleUtils;
import com.hp.printosmobilelib.ui.widgets.panel.PanelView;
import com.hp.printosmobilelib.ui.widgets.panel.SharableObject;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * Created by Anwar Asbah on 3/9/2017.
 */
public class ServiceCallPanel extends PanelView<ServiceCallViewModel> implements SharableObject {

    public static final String TAG = ServiceCallPanel.class.getName();

    @Bind(R.id.service_call_panel_content)
    View content;
    @Bind(R.id.text_closed_call_label)
    TextView closedLabel;
    @Bind(R.id.text_open_calls_label)
    TextView openedLabel;
    @Bind(R.id.text_open_calls_value)
    TextView numberOfOpenCalls;
    @Bind(R.id.text_closed_call_value)
    TextView numberOfClosedCalls;

    private ServiceCallPanelCallback serviceCallPanelCallback;

    public ServiceCallPanel(Context context) {
        super(context);
    }

    public ServiceCallPanel(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ServiceCallPanel(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setServiceCallPanelCallback(ServiceCallPanelCallback serviceCallPanelCallback) {
        this.serviceCallPanelCallback = serviceCallPanelCallback;
    }

    @Override
    public Spannable getTitleSpannable() {
        return new SpannableStringBuilder(getContext().getString(R.string.service_call_panel_title));
    }

    @Override
    public int getContentView() {
        return R.layout.service_call_panel;
    }

    @Override
    public void updateViewModel(ServiceCallViewModel viewModel) {

        setViewModel(viewModel);

        if (showEmptyCard()) {
            DeepLinkUtils.respondToIntentForPanel(this, serviceCallPanelCallback, true);
            return;
        }

        if (viewModel != null && viewModel.getCallViewModels() != null) {

            int closed = viewModel.getNumberOfClosedCalls();
            int opened = viewModel.getNumberOfOpenCalls();

            numberOfOpenCalls.setText(HPLocaleUtils.getLocalizedValue(opened));
            openedLabel.setText(getContext().getString(opened == 1 ? R.string.service_call_open
                    : R.string.service_calls_open));

            numberOfClosedCalls.setText(HPLocaleUtils.getLocalizedValue(closed));
            closedLabel.setText(getContext().getString(closed == 1 ? R.string.service_call_closed
                    : R.string.service_calls_closed));

            numberOfOpenCalls.setTextColor(ResourcesCompat.getColor(getResources(),
                    ServiceCallStateEnum.OPEN.getColor(), null));
            numberOfClosedCalls.setTextColor(ResourcesCompat.getColor(getResources(),
                    ServiceCallStateEnum.CLOSED.getColor(), null));
        }

        DeepLinkUtils.respondToIntentForPanel(this, serviceCallPanelCallback, false);
    }

    @Override
    public void bindViews() {

    }

    @Override
    protected boolean isValidViewModel() {
        return !(getViewModel() == null
                || getViewModel().getCallViewModels() == null
                || getViewModel().getCallViewModels().isEmpty());
    }

    protected String getEmptyCardText() {
        return getContext().getString(R.string.no_information_to_display);
    }

    @OnClick(R.id.opened_layout)
    public void onOpenLayoutClicked() {
        if (getViewModel() != null && serviceCallPanelCallback != null) {
            serviceCallPanelCallback.onServiceCallPanelClicked(getViewModel(), ServiceCallStateEnum.OPEN);
        }
    }

    @OnClick(R.id.closed_layout)
    public void onCloseLayoutClicked() {
        if (getViewModel() != null && serviceCallPanelCallback != null) {
            serviceCallPanelCallback.onServiceCallPanelClicked(getViewModel(), ServiceCallStateEnum.CLOSED);
        }
    }

    @Override
    public String getPanelTag() {
        return Panel.PANEL_SERVICE_CALL.getDeepLinkTag();
    }

    @Override
    protected boolean hasSharingButton() {
        super.hasSharingButton();
        return true;
    }

    @Override
    protected void onShareClicked() {
        super.onShareClicked();
        if (serviceCallPanelCallback != null) {
            serviceCallPanelCallback.onShareButtonClicked(Panel.PANEL_SERVICE_CALL);
        }
    }

    @Override
    public void sharePanel() {
        if (serviceCallPanelCallback != null && content != null) {
            lockShareButton(true);

            shareButton.setVisibility(GONE);
            final Bitmap panelBitmap = FileUtils.getScreenShot(this);
            shareButton.setVisibility(VISIBLE);

            if (panelBitmap != null) {
                DeepLinkUtils.getShareBody(getContext(),
                        Constants.UNIVERSAL_LINK_SCREEN_HOME,
                        getPanelTag(), null, null, false, new DeepLinkUtils.BranchIOCallback() {
                            @Override
                            public void onLinkCreated(String link) {
                                Uri storageUri = FileUtils.store(getContext(), panelBitmap, PANEL_SCREEN_SHOT_FILE_NAME);
                                serviceCallPanelCallback.onScreenshotCreated(Panel.PANEL_SERVICE_CALL, storageUri,
                                        getContext().getString(R.string.share_service_call_panel_title),
                                        link,
                                        ServiceCallPanel.this);
                                lockShareButton(false);
                            }
                        });
            }
        }
    }

    @Override
    public String getShareAction() {
        return AnswersSdk.SHARE_CONTENT_TYPE_SERVICE_CALLS;
    }

    public interface ServiceCallPanelCallback extends PanelShareCallbacks, DeepLinkUtils.DeepLinkCallbacks {
        void onServiceCallPanelClicked(ServiceCallViewModel serviceCallViewModel, ServiceCallStateEnum serviceCallStateEnum);
    }
}
