package com.hp.printosmobile.presentation.modules.todayhistogram;

import android.content.Context;

import com.hp.printosmobile.presentation.Presenter;
import com.hp.printosmobile.presentation.modules.home.HomeDataManager;
import com.hp.printosmobile.presentation.modules.home.HomePresenter;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitViewModel;
import com.hp.printosmobile.presentation.modules.today.histogrambreakdown.HistogramBreakDownRootFragment.HistogramResolution;
import com.hp.printosmobilelib.core.communications.remote.APIException;
import com.hp.printosmobilelib.core.logging.HPLogger;

import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Anwar Asbah on 05/03/2017.
 */
public class TodayHistogramPresenter extends Presenter<HistogramPresenterView> {

    private static final String TAG = TodayHistogramPresenter.class.getName();

    public synchronized void getActualVsTargetData(Context context, BusinessUnitViewModel selectedBusinessUnit,
                                                   boolean isShiftSupport) {


        Subscription subscription = HomeDataManager.getActualVsTarget(context, selectedBusinessUnit, isShiftSupport, false)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<TodayHistogramViewModel>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        HPLogger.d(TAG, "unable to get the actual vs target data " + e);
                        onRequestError(e);
                    }

                    @Override
                    public void onNext(TodayHistogramViewModel actualVsTargetViewModel) {
                        mView.updateHistogram(actualVsTargetViewModel);
                    }
                });
        addSubscriber(subscription);
    }

    public void getActualVsTargetGraphData(BusinessUnitViewModel selectedBusinessUnit) {
        Subscription subscription = HomeDataManager.getActualVsTargetGraph(selectedBusinessUnit)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<TodayHistogramViewModel>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        HPLogger.d(TAG, "unable to fetch actual vs target graph data " + e);
                        if(mView != null) {
                            mView.updateHistogram(null);
                        }
                    }

                    @Override
                    public void onNext(TodayHistogramViewModel histogramViewModel) {
                        if(mView != null) {
                            mView.updateHistogram(histogramViewModel);
                        }
                    }
                });
        addSubscriber(subscription);
    }

    public void getPrintVolumeDataPerResolution(final HistogramResolution resolution) {

        if(HomePresenter.getSelectedBusinessUnit() == null || resolution == null) {
            return;
        }

        Subscription subscription = HomeDataManager.getPrintVolumeDataPerResolution(resolution)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<TodayHistogramViewModel>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        HPLogger.d(TAG, "unable to fetch print volume data for resolution: " + resolution.name() + " " + e);
                        onRequestError(e);
                    }

                    @Override
                    public void onNext(TodayHistogramViewModel histogramViewModel) {
                        if(mView != null) {
                            mView.updateHistogram(histogramViewModel);
                        }
                    }
                });
        addSubscriber(subscription);
    }

    private void onRequestError(Throwable e) {
        APIException exception = APIException.create(APIException.Kind.UNEXPECTED);
        if (e instanceof APIException) {
            exception = (APIException) e;
        }

        mView.onRequestError(exception);
    }

    @Override
    public void detachView() {
        stopSubscribers();
        mView = null;
    }

}