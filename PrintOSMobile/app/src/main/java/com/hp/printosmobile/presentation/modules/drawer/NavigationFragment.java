package com.hp.printosmobile.presentation.modules.drawer;

import android.content.Context;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.view.ViewCompat;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.hp.printosmobile.AnswersSdk;
import com.hp.printosmobile.BuildConfig;
import com.hp.printosmobile.IntercomSdk;
import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.presentation.modules.invites.InviteUtils;
import com.hp.printosmobile.presentation.modules.invites.InvitesViewModel;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitEnum;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitViewModel;
import com.hp.printosmobile.presentation.modules.main.IMainFragment;
import com.hp.printosmobile.presentation.modules.npspopup.NPSPresenter;
import com.hp.printosmobile.presentation.modules.shared.UserViewModel;
import com.hp.printosmobile.utils.AppUtils;
import com.hp.printosmobile.utils.CustomTypefaceSpan;
import com.hp.printosmobile.utils.EmailUtils;
import com.hp.printosmobile.utils.HPLocaleUtils;
import com.hp.printosmobile.utils.HPUIUtils;
import com.hp.printosmobile.utils.ImageLoadingUtils;
import com.hp.printosmobilelib.core.communications.remote.APIException;
import com.hp.printosmobilelib.core.logging.HPLogger;
import com.hp.printosmobilelib.ui.common.HPFragment;
import com.hp.printosmobilelib.ui.widgets.TypefaceManager;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * A fragment to manage the NavigationDrawer.
 * Activities that contain this fragment must implement the {@link NavigationFragmentCallbacks} interface
 * to handle interaction events.
 *
 * @author Osama Taha
 */
public class NavigationFragment extends HPFragment implements NavView, IMainFragment, BusinessUnitsAdapter.BusinessUnitsAdapterCallbacks, InviteUtils.InviteObservable {

    private static final String TAG = NavigationFragment.class.getName();

    private static final int MAX_NUMBER_OF_NOTIFICATION_COUNT = 99;

    @Bind(R.id.navigation_drawer)
    NavigationView navigationView;
    @Bind(R.id.navigation_drawer_bottom)
    NavigationView navigationViewFooter;
    @Bind(R.id.header)
    View header;
    @Bind(R.id.refer_a_friend_container)
    View referAFriendContainer;

    ImageView profileImage;
    TextView userNameTextView;
    TextView userAccountTextView;
    View expandArrow;
    ProgressBar organizationLoadingIndicator;
    TextView emailTextView;
    View profileClickArea;

    NavigationPresenter presenter;

    private NavigationFragmentCallbacks callbacks;
    private OrganizationViewModel selectedOrganization;
    private TextView serverNameTextView;
    private View serverInfoLayout;
    private boolean openOrganizationPopup;

    public NavigationFragment() {
        // Required empty public constructor
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView();
    }

    private void initPresenter() {
        presenter = new NavigationPresenter();
        presenter.attachView(this);
    }

    private void initView() {

        initPresenter();

        //Set the tint color of the menu icons to be null in order to show the original menu icons.
        navigationView.setItemIconTintList(null);
        navigationViewFooter.setItemIconTintList(null);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            navigationView.setElevation(0);
        } else {
            ViewCompat.setElevation(navigationView, 0);
        }

        Menu m = navigationView.getMenu();
        for (int i = 0; i < m.size(); i++) {
            MenuItem mi = m.getItem(i);
            applyFontToMenuItem(mi);
        }
        m.findItem(R.id.drawer_send_beta_test_review).setVisible(BuildConfig.isBetaVersion);

        m = navigationViewFooter.getMenu();
        for (int i = 0; i < m.size(); i++) {
            MenuItem mi = m.getItem(i);
            applyFontToMenuItem(mi);
        }

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {

                callbacks.onMenuItemSelected();

                switch (menuItem.getItemId()) {
                    case R.id.drawer_invites_item:
                        callbacks.onInvitesItemClicked();
                        break;
                    case R.id.drawer_notification_item:
                        callbacks.onNotificationButtonClicked();
                        break;
                    case R.id.drawer_share_the_app_item:
                        onShareAppMenuItemClicked();
                        break;
                    case R.id.drawer_ask_question_item:
                    case R.id.drawer_send_feedback_item:
                    case R.id.drawer_report_problem_item:
                        callbacks.onContactHPClicked(menuItem.getItemId());
                        break;
                    case R.id.drawer_settings_item:
                        callbacks.onSettingMenuItemClicked();
                        break;
                    case R.id.drawer_talk_with_us_item:
                        callbacks.onTalkWithUsItemClicked();
                        break;
                    case R.id.drawer_refer_a_friend_item:
                        String referUrl = PrintOSPreferences.getInstance(getActivity()).getReferToAFriendUrl();
                        if (referUrl != null) {
                            AppUtils.startApplication(getActivity(), Uri.parse(referUrl));
                        }
                        break;
                    case R.id.drawer_send_beta_test_review:
                        sendFeedbackWithAttachmentThroughEmail();
                        break;
                    case R.id.drawer_rate_item:
                        callbacks.onRateItemClicked();
                        break;
                    default:
                        break;
                }

                HPLogger.d(TAG, "Click menu item " + menuItem.getTitle());

                callbacks.onMenuItemClicked(menuItem.getItemId());
                return false;
            }
        });

        navigationViewFooter.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {

                callbacks.onMenuItemSelected();

                HPLogger.d(TAG, "Click menu item " + menuItem.getTitle());

                callbacks.onMenuItemClicked(menuItem.getItemId());
                return false;
            }
        });

        setUserAndServerInfo();

        updateReferAFriendVisibility();

        TextView organizationTextView = header.findViewById(R.id.user_accounts_text_view);
        OrganizationViewModel model = PrintOSPreferences.getInstance(getActivity()).getSelectedOrganization();
        if (model != null && model.getOrganizationName() != null) {
            organizationTextView.setText(model.getOrganizationName());
        }

        initAccountSpinner();
    }

    private void updateReferAFriendVisibility() {
        boolean isReferFeatureEnabled = PrintOSPreferences.getInstance(getActivity()).isReferToAFriendEnabled() &&
                !PrintOSPreferences.getInstance(getActivity()).isLatexOnly();
        String referUrl = PrintOSPreferences.getInstance(getActivity()).getReferToAFriendUrl();
        referAFriendContainer.setVisibility(isReferFeatureEnabled && referUrl != null ? View.VISIBLE : View.GONE);

    }

    @OnClick(R.id.refer_a_friend_container)
    public void onReferAFriendClicked() {
        String referUrl = PrintOSPreferences.getInstance(getActivity()).getReferToAFriendUrl();
        if (referUrl != null) {
            AppUtils.startApplication(getActivity(), Uri.parse(referUrl));
        }
        if (callbacks != null) {
            callbacks.onMenuItemSelected();
        }
    }

    private void onShareAppMenuItemClicked() {
        AppUtils.sendInvites(getContext(), AnswersSdk.INVITE_METHOD_MENU_ITEM);
    }

    @Override
    public void onResume() {
        super.onResume();
        updateUserNameEmail();
        InviteUtils.addObserver(this);
    }

    public void onValidationCompleted() {
        presenter.getProfileImageInfo();
        initAccountSpinner();
    }

    public void setNumberOfNotifications(int numberOfNotifications) {
        Menu menu = navigationView.getMenu();
        MenuItem notificationMenuItem = menu.findItem(R.id.drawer_talk_with_us_item);
        if (PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).isInDemoMode()) {
            numberOfNotifications = 0;
        }

        notificationMenuItem.setIcon(numberOfNotifications > 0 ?
                HPUIUtils.getNotificationIconBadgeDrawable(getActivity(), R.drawable.talk_to_us, String.valueOf(numberOfNotifications))
                : ResourcesCompat.getDrawable(getResources(), R.drawable.talk_to_us, null));
    }

    private void applyFontToMenuItem(MenuItem menuItem) {
        Typeface font = TypefaceManager.getTypeface(PrintOSApplication.getAppContext(), TypefaceManager.HPTypeface.HP_LIGHT);
        SpannableString mNewTitle = new SpannableString(menuItem.getTitle());
        mNewTitle.setSpan(new CustomTypefaceSpan("", font), 0, mNewTitle.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        menuItem.setTitle(mNewTitle);
    }

    private void initAccountSpinner() {
        userAccountTextView = header.findViewById(R.id.user_accounts_text_view);
        expandArrow = header.findViewById(R.id.expand_arrow);
        organizationLoadingIndicator = header.findViewById(R.id.organization_loading_indicator);
        header.findViewById(R.id.account_container).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              getOrganizationsList(true);
            }
        });
    }

    private void setUserAndServerInfo() {

        userNameTextView = header.findViewById(R.id.drawer_header_user_name);
        emailTextView = header.findViewById(R.id.drawer_header_email);
        serverInfoLayout = header.findViewById(R.id.drawer_header_server_info_layout);
        serverNameTextView = header.findViewById(R.id.drawer_header_server_name);
        profileImage = header.findViewById(R.id.profile_image);
        profileClickArea = header.findViewById(R.id.profile_click_area);

        updateUserNameEmail();

        PrintOSPreferences userPreferences = PrintOSPreferences.getInstance(PrintOSApplication.getAppContext());

        if (!userPreferences.isProductionStack()) {
            String serverName = userPreferences.getServerName();
            serverInfoLayout.setVisibility(View.VISIBLE);
            serverNameTextView.setText(serverName);
        } else {
            serverInfoLayout.setVisibility(View.GONE);
        }

        profileClickArea.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (callbacks != null) {
                    callbacks.onMenuItemSelected();
                    callbacks.onSettingMenuItemClicked();
                }
            }
        });

    }

    private void updateUserNameEmail() {

        if (userNameTextView == null || emailTextView == null) {
            return;
        }

        UserViewModel userViewModel = PrintOSPreferences.getInstance(getContext()).getUserInfo();

        userNameTextView.setText(userViewModel.getDisplayName());
        emailTextView.setText(PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).getUserEmail());

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_navigation;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof NavigationFragmentCallbacks) {
            callbacks = (NavigationFragmentCallbacks) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement NavigationFragmentCallbacks");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();

        callbacks = null;
        presenter.detachView();
    }

    @Override
    public void onBusinessUnitSelected(BusinessUnitViewModel businessUnitViewModel, boolean resetFilters) {
        updateBusinessUnitTextView(businessUnitViewModel);
    }

    private void updateBusinessUnitTextView(BusinessUnitViewModel businessUnitViewModel) {

        TextView businessUnitTextView = header.findViewById(R.id.business_unit_text_view);
        String businessUnitDisplayName = businessUnitViewModel.getBusinessUnit().getBusinessUnitDisplayName(false);
        businessUnitTextView.setText(businessUnitDisplayName);

    }

    @Override
    public void onBusinessUnitSelected(BusinessUnitViewModel businessUnitViewModel) {
        updateBusinessUnitTextView(businessUnitViewModel);
    }

    @Override
    public void onFiltersChanged(BusinessUnitViewModel businessUnitViewModel) {
        //FOR LATER USE.
    }

    @Override
    public void didFinishGettingBusinessUnits(Map<BusinessUnitEnum, BusinessUnitViewModel> businessUnitsMap) {

        View businessUnitContainer = header.findViewById(R.id.business_units_container);
        HPUIUtils.setVisibility(businessUnitsMap.size() > 1, businessUnitContainer);
        if (businessUnitsMap.size() > 1) {
            header.findViewById(R.id.business_units_container).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (callbacks != null) {
                        callbacks.onMenuItemSelected();
                        callbacks.onDivisionSwitchMenuItemClick();
                    }
                }
            });
        }

        updateReferAFriendVisibility();
    }

    @Override
    public void onMainError(String msg, boolean triggerSignOut, boolean isHasNoDevices) {
        didFinishGettingBusinessUnits(new HashMap<BusinessUnitEnum, BusinessUnitViewModel>());
    }

    @Override
    public boolean respondToValidationPeriodExceedance() {
        return false;
    }

    @Override
    public void onError(APIException exception, String tag) {

    }

    @Override
    public boolean isFragmentNameToBeDisplayed() {
        return false;
    }

    @Override
    public boolean isFilteringAvailable() {
        return false;
    }

    @Override
    public int getToolbarDisplayName() {
        return R.string.unknown_value;
    }

    @Override
    public void onOrganizationsRetrieved(List<OrganizationViewModel> organizations) {

        if (!isAdded() || getActivity() == null) {
            return;
        }

        organizationLoadingIndicator.setVisibility(View.GONE);

        String selectedOrganizationID = "";
        if (organizations != null) {
            if (organizations.size() <= 1) {
                header.findViewById(R.id.account_container).setOnClickListener(null);
                userAccountTextView.setText(!organizations.isEmpty() ? organizations.get(0).getOrganizationName() : "");
                expandArrow.setVisibility(View.GONE);
                return;
            } else {
                expandArrow.setVisibility(View.VISIBLE);
                header.findViewById(R.id.account_container).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        getOrganizationsList(true);
                    }
                });

                for (OrganizationViewModel organization : organizations) {
                    if (organization.getOrganizationId().equals(PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).getSavedOrganizationId())) {
                        userAccountTextView.setText(organization.getOrganizationName());
                        selectedOrganizationID = organization.getOrganizationId();
                        break;
                    }
                }
            }
        } else {
            expandArrow.setVisibility(View.VISIBLE);
            return;
        }

        if (callbacks != null && openOrganizationPopup) {
            callbacks.onChangeOrganizationClicked(organizations, selectedOrganizationID);
        }
    }

    public void getOrganizationsList(boolean openOrganizationPopup) {

        if (getActivity() == null || !isAdded()){
            return;
        }

        this.openOrganizationPopup = openOrganizationPopup;
        organizationLoadingIndicator.setVisibility(View.VISIBLE);
        expandArrow.setVisibility(View.GONE);

        if (presenter == null) {
            initPresenter();
        }
        presenter.getOrganizations();

    }

    public void setOrganization() {
        if (userAccountTextView != null && getActivity() != null && isAdded()) {
            OrganizationViewModel organizationViewModel = PrintOSPreferences.getInstance(getActivity()).getSelectedOrganization();
            userAccountTextView.setText(organizationViewModel == null || organizationViewModel.getOrganizationName() == null ?
                    "" : organizationViewModel.getOrganizationName());
        }
    }

    public void onOrganizationSelected(OrganizationViewModel organizationViewModel) {
        //Prevent the selected organization from being selected again.
        if (presenter != null && !PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).getSavedOrganizationId().equals(organizationViewModel.getOrganizationId())) {
            presenter.changeContext(organizationViewModel);
            callbacks.onOrganizationSelected();
        }
    }

    @Override
    public void onContextChanged(boolean isSuccessful, OrganizationViewModel organizationViewModel, APIException e) {

        if (isSuccessful) {
            if (organizationViewModel != null) {
                selectedOrganization = organizationViewModel;
                ((TextView) header.findViewById(R.id.user_accounts_text_view)).setText(organizationViewModel.getOrganizationName());
            }
        } else {
            HPUIUtils.displayToast(PrintOSApplication.getAppContext(), HPLocaleUtils.getSimpleErrorMsg(PrintOSApplication.getAppContext(), e));
        }
        callbacks.onContextChanged(isSuccessful, organizationViewModel == null ? null : organizationViewModel.getOrganizationId());
    }

    @Override
    public void onProfileImageUrlRetrieved(String url) {
        ImageLoadingUtils.setLoadedImageFromUrl(getActivity(), profileImage, url, R.drawable.user_name, null, true);
    }

    public void reload() {
        initAccountSpinner();
        refreshProfile();
    }

    private void sendFeedbackWithAttachmentThroughEmail() {

        String to = getString(R.string.reviewer_email);
        String subject = getString(R.string.beta_tester_review_email_title);
        String body = getDeviceInfo();
        File logsFile = new File(HPLogger.getInstance(getActivity()).getFilePath());

        HPLogger.d(TAG, "Sending beta testing review");

        ArrayList<Uri> attachments = new ArrayList<>();
        attachments.add(Uri.fromFile(logsFile));
        EmailUtils.sendEmail(getActivity(), new String[]{to}, subject, body, attachments);

    }

    private String getDeviceInfo() {

        return getString(R.string.beta_tester_review_email_body, Build.MODEL, Build.VERSION.SDK_INT);
    }

    public void refreshProfile() {
        if (presenter == null) {
            presenter = new NavigationPresenter();
            presenter.attachView(this);
        }
        presenter.getProfileImageInfo();
    }

    public void setUserEmail(String email) {
        if (emailTextView != null && email != null) {
            emailTextView.setText(email);
        }
    }

    public void init() {
        updateUserNameEmail();
    }

    public void updateIntercomBadge() {

        int unreadCount = Math.min(IntercomSdk.getInstance(PrintOSApplication.getAppContext()).getNumberOfUnreadMsgs(),
                MAX_NUMBER_OF_NOTIFICATION_COUNT);
        setNumberOfNotifications(unreadCount);

    }

    @Override
    public void updateInviteObserver() {

        InvitesViewModel invitesViewModel = InviteUtils.getInvitesViewModel();
        if (invitesViewModel == null || !isAdded() || getActivity() == null || navigationView == null) {
            return;
        }

        navigationView.getMenu().findItem(R.id.drawer_invites_item).setVisible(invitesViewModel.canInvite());

    }

    public void setNPSMenuItemVisibility(Boolean show, NPSPresenter npsPresenter, BusinessUnitViewModel businessUnit) {
        if(navigationView != null && navigationView.getMenu() != null) {
            MenuItem menuItem = navigationView.getMenu().findItem(R.id.drawer_rate_item);
            menuItem.setVisible(show && npsPresenter != null && !TextUtils.isEmpty(npsPresenter.getEndUserId(businessUnit)));
        }

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment.
     *
     * @author Osama Taha
     */
    public interface NavigationFragmentCallbacks {

        void onOrganizationSelected();

        void onContextChanged(boolean isSuccessful, String selectedOrganizationID);

        void onMenuItemSelected();

        void onContactHPClicked(int id);

        void onDivisionSwitchMenuItemClick();

        void onNotificationButtonClicked();

        void onMenuItemClicked(int menuItemId);

        void onOrganizationsRetrieved(List<OrganizationViewModel> organizationViewModels, String selectOrganizationID);

        void onChangeOrganizationClicked(List<OrganizationViewModel> organizationViewModels, String selectedOrganizationID);

        void onSettingMenuItemClicked();

        void onTalkWithUsItemClicked();

        void onInvitesItemClicked();

        void onRateItemClicked();
    }
}