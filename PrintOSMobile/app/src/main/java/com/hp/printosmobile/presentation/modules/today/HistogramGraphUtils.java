package com.hp.printosmobile.presentation.modules.today;

import android.content.Context;
import android.os.Build;
import android.support.v4.content.res.ResourcesCompat;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.RelativeSizeSpan;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.data.remote.models.PreferencesData;
import com.hp.printosmobile.presentation.modules.home.HomePresenter;
import com.hp.printosmobile.presentation.modules.home.ShiftViewModel;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitEnum;
import com.hp.printosmobile.presentation.modules.shared.Unit;
import com.hp.printosmobile.presentation.modules.today.histogrambreakdown.HistogramBreakDownRootFragment;
import com.hp.printosmobile.presentation.modules.today.histogrambreakdown.HistogramBreakDownRootFragment.HistogramResolution;
import com.hp.printosmobile.presentation.modules.todayhistogram.TodayHistogramViewModel;
import com.hp.printosmobile.presentation.modules.todayhistogram.TodayHistogramViewModel.TodayHistogramItem;
import com.hp.printosmobile.utils.FileUtils;
import com.hp.printosmobile.utils.HPLocaleUtils;
import com.hp.printosmobile.utils.HPMathUtils;
import com.hp.printosmobilelib.core.utils.HPDateUtils;

import java.io.IOException;
import java.text.DateFormatSymbols;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Anwar Asbah on 12/17/2017.
 */

public class HistogramGraphUtils {

    public enum HistogramChartType {
        AREA("area", "areaspline"),
        COLUMN("column", "column");

        String tag;
        String htmlTag;

        HistogramChartType(String tag, String htmlTag) {
            this.tag = tag;
            this.htmlTag = htmlTag;
        }

        public String getTag() {
            return tag;
        }

        public String getHtmlTag() {
            return htmlTag;
        }

        public static HistogramChartType from(String tag) {
            if (tag != null) {
                for (HistogramChartType type : HistogramChartType.values()) {
                    if (tag.equalsIgnoreCase(type.tag)) {
                        return type;
                    }
                }
            }

            return COLUMN;
        }
    }

    private static final String GRAPH_LAST_DAYS_HTML_FILE_NAME = "last_days_blueprint.html";
    private static final String GRAPH_SHIFT_HTML_FILE_NAME = "last_shifts_blueprint.html";

    private static final String SERIES_KEY = "@TODAYSERIES@";
    private static final String XAXIS_KEY_KEY = "@XAXISVALUES@";
    private static final String SHOULD_BREAKDOWN_KEY = "@SHOULDBREAKDOWN@";
    private static final String LOCALE_KEY = "@LOCALE@";
    private static final String TOOLTIP_IMP_KEY = "@IMPRESSIONLOCALIZED@";
    private static final String TOOLTIP_SQM_KEY = "@SQMLOCALIZED@";
    private static final String TOOLTIP_LM_KEY = "@LMLOCALIZED@";
    private static final String TOOLTIP_SHEETS_KEY = "@SHEETSLOCALIZED@";
    private static final String CHART_TYPE_KEY = "@CHARTTYPE@";
    private static final String IMPRESSION_TYPE_KEY = "@IMPRESSION_TYPE@";
    private static final String VALUES_LABEL_KEY = "@VALUESLABELKEY@";
    private static final String VALUES_DIVIDER_KEY = "@VALUESDIVIDERKEY@";
    private static final String SERIES_COLOR_KEY = "@SERIESCOLOR@";
    private static final String HOVER_COLOR_KEY = "@HOVERCOLOR@";
    private static final String HAS_TOOLTIP_KEY = "@HASTOOLTIP@";
    private static final String TOOLTIP_HEADER_KEY = "@TOOLTIPHEADERDIV@";
    private static final String Y_AXIS_EXTRA_KEY = "@YAXISEXTRA@";

    private static final String HISTOGRAM_DATE_FORMAT = "MMM dd";
    private static final String HISTOGRAM_DATE_FORMAT_V2 = "MMM yyyy";
    private static final String TITLE_UNIT_FORMAT = "(%s)";
    private static final String VALUE_UNIT_FORMAT = "%1$s %2$s";
    private static final String TODAY_DATA_ENTRY_ITEM_FORMAT = "{x: %1$s," +
            "      y: %2$d,\n" +
            "      yWithUnit: \"%3$s\",\n" +
            "      lm: %4$s,\n" +
            "      sheets: %5$s,\n" +
            "      sqm: %6$s,\n" +
            "      change: %7$s, \n" +
            "      isHigher: '%8$s',\n" +
            "      tooltipdate: '%9$s'}";
    private static final String CHANGE_LOWER = "'<span style=\"font-size: 70%%;color:red;font-weight: bold\">&#9660;%.1f%%</span>'";
    private static final String CHANGE_HIGHER = "'<span style=\"font-size: 70%%;color:green;font-weight: bold\">&#9650;%.1f%%</span>'";
    private static final String SHIFT_DATA_ENTRY_ITEM_FORMAT = " {x: %1$d,\n" +
            "      y: %2$d,\n" +
            "      myData: '<b>%3$s</b></br>%4$s',\n" +
            "      lm: %5$d,\n" +
            "      sheets: %6$d,\n" +
            "      sqm: %7$d}";
    private static final String SHITE_DATA_SET_FORMAT = "{\n" +
            "            name: '%1$s',\n" +
            "            animation: true,\n" +
            "            data: [%2$s],\n" +
            "            color: '%3$s'\n" +
            "        }";
    private static final String HEADER_TOOLTIP = "    <div id=\"tooltip\" style=\"width: 95%%;margin: 10px auto\">\n" +
            "        <div>\n" +
            "        <span id=\"tooltip_value\" style=\"font-size: 110%%\">\n" +
            "          </br>\n" +
            "        </span>\n" +
            "        <span id=\"tooltip_change\">\n" +
            "        </span>\n" +
            "        </div>\n" +
            "        <div>\n" +
            "        </div>\n" +
            "\n" +
            "        <div id=breakdown_values>%s</div>\n" +
            "        <div id=\"tooltip_date\"></br></div>\n" +
            "        <div style=\"height: 10px\"></div>\n" +
            "    </div>";
    private static final String MAX_VALUE_HTML_TAG = "max: %d,\n";
    private static final String TICK_AMOUNT_HTML_TAG = "tickAmount: %d,\n";
    private static final String UNIT_WITH_IMPRESSION = " (%s)";
    private static final String SHARE_SHEET_TITLE = "Sheets";
    private static final String SHARE_IMPRESSION_TITLE = "Impressions";

    public static void displayGraph(Context context, WebView progressWebView, TodayHistogramViewModel histogramViewModel,
                                    TodayHistogramViewModel.DetailTypeEnum detailTypeEnum, HistogramResolution resolution,
                                    boolean showBreakDown, boolean showTooltip) {

        if (histogramViewModel == null || histogramViewModel.getData() == null) {
            return;
        }

        WebSettings webSettings = progressWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setDomStorageEnabled(true);

        progressWebView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            progressWebView.setWebContentsDebuggingEnabled(true);
        }

        boolean isShift = histogramViewModel.isShiftSupport();

        try {
            String detail = FileUtils.loadAssestFile(context, isShift ? GRAPH_SHIFT_HTML_FILE_NAME : GRAPH_LAST_DAYS_HTML_FILE_NAME);
            detail = isShift ? setShiftSeriesData(detail, histogramViewModel) : setLastDaySeriesData(context, detail, histogramViewModel, detailTypeEnum, resolution);

            PrintOSPreferences preferences = PrintOSPreferences.getInstance(context);
            PreferencesData.UnitSystem unitSystem = preferences.getUnitSystem();

            String localizedImp;
            if (histogramViewModel.getBusinessUnit() == BusinessUnitEnum.INDIGO_PRESS) {
                localizedImp = context.getString(R.string.kpi_explanation_bar_tag_print_volume_indigo);
            } else {
                localizedImp = Unit.SQM.getUnitText(context, unitSystem, Unit.UnitStyle.DEFAULT);
            }

            String unitExtension = "";
            if (histogramViewModel.getImpressionType() != null
                    && histogramViewModel.getBusinessUnit() == BusinessUnitEnum.INDIGO_PRESS) {
                unitExtension = String.format(UNIT_WITH_IMPRESSION, histogramViewModel.getImpressionType());
            }

            String locale = preferences.getLanguage(context.getString(R.string.default_language));
            String tooltipHeaderKey;
            if (showTooltip) {
                tooltipHeaderKey = String.format(HEADER_TOOLTIP,
                        showBreakDown ? "</br>" : "");
            } else {
                tooltipHeaderKey = "</br>";
            }
            detail = detail.replace(LOCALE_KEY, locale)
                    .replace(TOOLTIP_IMP_KEY, localizedImp)
                    .replace(TOOLTIP_SHEETS_KEY, context.getString(R.string.sheets))
                    .replace(TOOLTIP_LM_KEY, Unit.METER.getUnitText(context, unitSystem, Unit.UnitStyle.DEFAULT))
                    .replace(TOOLTIP_SQM_KEY, Unit.SQM.getUnitText(context, unitSystem, Unit.UnitStyle.DEFAULT))
                    .replace(IMPRESSION_TYPE_KEY, unitExtension)
                    .replace(CHART_TYPE_KEY, isShift ? preferences.getHistogramShifChartType().getHtmlTag() :
                            preferences.getHistogramDayChartType().getHtmlTag())
                    .replace(SHOULD_BREAKDOWN_KEY, String.valueOf(showBreakDown))
                    .replace(SERIES_COLOR_KEY, detailTypeEnum.getPrimaryColor())
                    .replace(HOVER_COLOR_KEY, detailTypeEnum.getSecondaryColor())
                    .replace(HAS_TOOLTIP_KEY, String.valueOf(showTooltip))
                    .replace(TOOLTIP_HEADER_KEY, tooltipHeaderKey);

            progressWebView.loadDataWithBaseURL("", detail, "text/html", "utf-8", "");
        } catch (IOException e) {
            //nothing to do
        }
    }

    private static String setShiftSeriesData(String html, TodayHistogramViewModel histogramViewModel) {

        if (histogramViewModel == null || html == null || histogramViewModel.getData() == null) {
            return "";
        }

        DateFormatSymbols dateFormatSymbols = new DateFormatSymbols();
        String[] localizedDaysSymbols = dateFormatSymbols.getShortWeekdays();

        Map<String, String> seriesMap = new HashMap<>();
        Map<String, String> shiftsColorMap = new HashMap<>();

        for (int index = 0; index < histogramViewModel.getData().size(); index++) {
            TodayHistogramItem todayHistogramItem = histogramViewModel.getData().get(index);
            ShiftViewModel shiftViewModel = todayHistogramItem.getShift();

            String shiftName = shiftViewModel.getShiftName();
            String timeRangeLabel = HPLocaleUtils.getTimeRange(PrintOSApplication.getAppContext(),
                    localizedDaysSymbols[shiftViewModel.getStartDay()],
                    shiftViewModel.getStartHour(),
                    localizedDaysSymbols[shiftViewModel.getEndDay()],
                    shiftViewModel.getEndHour(),
                    true);

            String entry = String.format(SHIFT_DATA_ENTRY_ITEM_FORMAT,
                    index,
                    todayHistogramItem.getActual(),
                    shiftName,
                    timeRangeLabel,
                    (int) todayHistogramItem.getMeters(),
                    (int) todayHistogramItem.getSheets(),
                    (int) todayHistogramItem.getMetersSquare());

            if (seriesMap.containsKey(shiftName)) {
                String date = seriesMap.get(shiftName) + "," + entry;
                seriesMap.remove(shiftName);
                seriesMap.put(shiftName, date);
            } else {
                seriesMap.put(shiftName, entry);
            }

            if (!shiftsColorMap.containsKey(shiftName)) {
                int color = ResourcesCompat.getColor(PrintOSApplication.getAppContext().getResources(),
                        histogramViewModel.getShiftLegends().get(todayHistogramItem.getShift().getShiftId()).getLegendColor(), null);
                String colorString = String.format("#%06X", (0xFFFFFF & color));
                shiftsColorMap.put(shiftName, colorString);
            }
        }

        String series = "";
        boolean isFirst = true;
        for (String shift : seriesMap.keySet()) {
            series += isFirst ? "" : ",";
            isFirst = false;
            series += String.format(SHITE_DATA_SET_FORMAT,
                    shift,
                    seriesMap.get(shift),
                    shiftsColorMap.get(shift));
        }

        return html.replace(SERIES_KEY, series);
    }

    private static String setLastDaySeriesData(Context context, String html, TodayHistogramViewModel histogramViewModel,
                                               TodayHistogramViewModel.DetailTypeEnum detailTypeEnum, HistogramResolution resolution) {

        if (histogramViewModel == null || html == null || histogramViewModel.getData() == null) {
            return "";
        }

        String series = "";
        String xValues = "";
        int maxValue = 0;

        for (int i = 0; i < histogramViewModel.getData().size(); i++) {
            TodayHistogramItem item = histogramViewModel.getData().get(i);

            float dayData = item.getDayData(detailTypeEnum);
            float lastWeekData = item.getLastWeekData(detailTypeEnum);

            float percentageChange;
            if (lastWeekData == 0) {
                percentageChange = dayData == 0 ? 0 : 1;
            } else {
                percentageChange = (dayData - lastWeekData) / lastWeekData;
            }

            Calendar cal = Calendar.getInstance();
            cal.setTime(item.getDate());

            int value = Math.max(0, (int) dayData);
            String unit = getLocalizedUnit(context, histogramViewModel, detailTypeEnum, false);

            int tooltipDateFormatID;
            if(resolution == null) {
                tooltipDateFormatID = R.string.histogram_tooltip_date_format;
            } else {
                switch (resolution) {
                    case MONTHLY:
                        tooltipDateFormatID = R.string.histogram_tooltip_date_format_monthly;
                        break;
                    case WEEKLY:
                        tooltipDateFormatID = R.string.histogram_tooltip_date_format_weekly;
                        break;
                    default:
                        tooltipDateFormatID = R.string.histogram_tooltip_date_format;
                        break;
                }
            }


            series += String.format(TODAY_DATA_ENTRY_ITEM_FORMAT,
                    String.valueOf(i),
                    value,
                    String.format(VALUE_UNIT_FORMAT, HPLocaleUtils.getLocalizedValue(value), unit),
                    Math.max(0, (int) item.getMeters()),
                    Math.max(0, (int) item.getSheets()),
                    Math.max(0, (int) item.getMetersSquare()),
                    String.format("%.1f", Math.abs(percentageChange) * 100).replace(",", "."),
                    percentageChange >= 0 ? true : false,
                    HPDateUtils.formatDate(item.getDate(), context.getString(tooltipDateFormatID)));
            series += (i == histogramViewModel.getData().size() - 1) ? "" : ",";

            String targetFormat;
            if(resolution == null) {
                targetFormat = HISTOGRAM_DATE_FORMAT;
            } else {
                switch (resolution) {
                    case MONTHLY:
                    case WEEKLY:
                        targetFormat = HISTOGRAM_DATE_FORMAT_V2;
                        break;
                    default:
                        targetFormat = HISTOGRAM_DATE_FORMAT;
                        break;
                }
            }
            xValues += "'" + HPDateUtils.formatDate(item.getDate(), targetFormat) + "'";
            xValues += (i == histogramViewModel.getData().size() - 1) ? "" : ",";

            maxValue = Math.max(maxValue, (int) dayData);
        }

        int power = HPMathUtils.get1000Power(maxValue);

        return html.replace(SERIES_KEY, series)
                .replace(VALUES_LABEL_KEY, HPMathUtils.get1000PowerLabel(context, power))
                .replace(VALUES_DIVIDER_KEY, String.valueOf(Math.pow(1000, Math.min(power, 3))))
                .replace(XAXIS_KEY_KEY, xValues)
                .replace(Y_AXIS_EXTRA_KEY, String.format(MAX_VALUE_HTML_TAG, maxValue)
                        + String.format(TICK_AMOUNT_HTML_TAG, 4));
    }

    public static Spannable getHistogramTitle(Context context, TodayHistogramViewModel viewModel,
                                              TodayHistogramViewModel.DetailTypeEnum typeEnum,
                                              HistogramResolution resolution,
                                              boolean isBreakDownActivity, boolean isPanel) {

        if (context == null) {
            return new SpannableStringBuilder("");
        } else if (viewModel == null) {
            return new SpannableStringBuilder(context.getString(R.string.histogram_view_days_title, ""));
        }

        boolean isShiftSupport = viewModel.isShiftSupport();

        String localizedUnit = getLocalizedUnit(context, viewModel, typeEnum, true);
        if (isBreakDownActivity) {
            return new SpannableString(localizedUnit);
        }

        localizedUnit = String.format(TITLE_UNIT_FORMAT, localizedUnit);
        BusinessUnitEnum businessUnitEnum = HomePresenter.getSelectedBusinessUnit() != null ?
                HomePresenter.getSelectedBusinessUnit().getBusinessUnit() : BusinessUnitEnum.UNKNOWN;


        if(businessUnitEnum == BusinessUnitEnum.INDIGO_PRESS && isPanel) {
            return new SpannableString(context.getString(R.string.histogram_view_days_title_with_resolution));
        }

        int titleResourceID;
        if(isShiftSupport) {
            titleResourceID = R.string.histogram_view_shifts_title;
        } else {
            if(resolution == null) {
                titleResourceID = R.string.histogram_view_days_title;
            } else {
                switch (resolution) {
                    case MONTHLY:
                        titleResourceID = R.string.histogram_view_month_title;
                        break;
                    case WEEKLY:
                        titleResourceID = R.string.histogram_view_week_title;
                        break;
                    default:
                        titleResourceID = R.string.histogram_view_days_title;
                        break;
                }
            }
        }
        String title = context.getString(titleResourceID, localizedUnit);

        SpannableString spannableString = new SpannableString(title);
        spannableString.setSpan(new RelativeSizeSpan(0.8f), title.indexOf(localizedUnit),
                title.indexOf(localizedUnit) + localizedUnit.length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);

        return spannableString;
    }

    private static String getLocalizedUnit(Context context, TodayHistogramViewModel viewModel,
                                           TodayHistogramViewModel.DetailTypeEnum typeEnum,
                                           boolean withImpressionType) {

        PrintOSPreferences preferences = PrintOSPreferences.getInstance(context);
        PreferencesData.UnitSystem unitSystem = preferences.getUnitSystem();

        switch (typeEnum) {
            case SQM:
                return Unit.SQM.getUnitText(context, unitSystem, Unit.UnitStyle.DEFAULT);
            case LM:
                return Unit.METER.getUnitText(context, unitSystem, Unit.UnitStyle.DEFAULT);
            case SHEETS:
                String sheets = context.getString(R.string.sheets);
                if (withImpressionType) {
                    return viewModel.getImpressionType() == null ?
                            sheets.substring(0, 1).toUpperCase() + sheets.substring(1)
                            : viewModel.getImpressionType() + " " + sheets;
                } else {
                    return sheets.toLowerCase();
                }
            default:
                if (viewModel.getBusinessUnit() == BusinessUnitEnum.INDIGO_PRESS) {
                    if (withImpressionType) {
                        return viewModel.getImpressionType() != null ?
                                context.getString(R.string.type_impressions, viewModel.getImpressionType()) :
                                context.getString(R.string.report_y_axis_impressions);
                    } else {
                        return context.getString(R.string.impression).toLowerCase();
                    }
                } else {
                    return Unit.SQM.getUnitText(context, unitSystem, Unit.UnitStyle.DEFAULT);
                }
        }
    }

    public static String getHistogramAnalyticsTitle(Context context, TodayHistogramViewModel viewModel,
                                                    TodayHistogramViewModel.DetailTypeEnum typeEnum) {

        PrintOSPreferences preferences = PrintOSPreferences.getInstance(context);
        PreferencesData.UnitSystem unitSystem = preferences.getUnitSystem();

        switch (typeEnum) {
            case SQM:
                return Unit.SQM.getShortName(unitSystem);
            case LM:
                return Unit.METER.getShortName(unitSystem);
            case SHEETS:
                return SHARE_SHEET_TITLE;
            default:
                if (viewModel.getBusinessUnit() == BusinessUnitEnum.INDIGO_PRESS) {
                    return SHARE_IMPRESSION_TITLE;
                } else {
                    return Unit.SQM.getShortName(unitSystem);
                }
        }
    }

    public static boolean hasValues(TodayHistogramViewModel viewModel, TodayHistogramViewModel.DetailTypeEnum typeEnum) {
        if (viewModel == null || viewModel.getData() == null) {
            return false;
        }

        boolean hasData = false;
        for (TodayHistogramItem item : viewModel.getData()) {
            hasData = hasData || item.getDayData(typeEnum) != TodayHistogramItem.ERROR_VALUE;
        }

        return hasData;
    }
}
