package com.hp.printosmobile.presentation.modules.insights;

import android.content.Context;

import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.data.remote.ApiServicesProvider;
import com.hp.printosmobile.data.remote.models.InsightData;
import com.hp.printosmobile.data.remote.models.kz.KZFavorites;
import com.hp.printosmobile.data.remote.models.kz.KZItem;
import com.hp.printosmobile.data.remote.models.kz.RecommendedContentData;
import com.hp.printosmobile.data.remote.services.InsightsService;
import com.hp.printosmobile.presentation.modules.insights.InsightsViewModel.InsightKpiEnum;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitViewModel;
import com.hp.printosmobile.presentation.modules.shared.LanguageEnum;
import com.hp.printosmobile.utils.HPUIUtils;
import com.hp.printosmobilelib.core.logging.HPLogger;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Response;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by Anwar Asbah on 6/20/17.
 */
public final class InsightsDataManager {

    private static final int TOP = 5;
    private static final String TO_BE_REPLACE_REGEX = "_";
    private static final String REPLACING_REGEX = " ";
    private static final int RECOMMENDED_CONTENT_SIZE = 20;
    private static final String TAG = InsightsDataManager.class.getSimpleName();

    private InsightsDataManager() {

    }

    public static Observable<InsightsViewModel> getTop5Data(final BusinessUnitViewModel businessUnitViewModel, final InsightKpiEnum kpi, String fromDate, String toDate) {

        ApiServicesProvider serviceProvider = PrintOSApplication.getApiServicesProvider();
        InsightsService insightsService = serviceProvider.getInsightsService();
        List<String> serialNumbers = businessUnitViewModel.getFiltersViewModel().getSerialNumbers();
        String language = PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).getLanguage(
                PrintOSApplication.getAppContext().getString(R.string.default_language));

        return insightsService.getTop5Data(serialNumbers, fromDate, toDate, kpi.getTag(), TOP, language).map(new Func1<Response<InsightData>, InsightsViewModel>() {
            @Override
            public InsightsViewModel call(Response<InsightData> insightData) {
                if (insightData.isSuccessful() && insightData.body() != null && insightData.body().getInsightList() != null) {
                    return mapInsightsData(businessUnitViewModel, kpi, insightData.body().getInsightList());
                }
                return null;
            }
        });
    }

    private static InsightsViewModel mapInsightsData(BusinessUnitViewModel bu, InsightKpiEnum kpi, List<InsightData.Insight> insightDataList) {

        InsightsViewModel insightsViewModel = new InsightsViewModel();
        List<InsightsViewModel.InsightModel> insightModels = new ArrayList<>();

        boolean correctiveActionsEnabled = PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).getCorrectiveActionsEnabled();

        for (int i = 0; i < insightDataList.size(); i++) {
            InsightData.Insight data = insightDataList.get(i);
            if (data != null) {
                InsightsViewModel.InsightModel model = new InsightsViewModel.InsightModel();

                String label = data.getEventName();
                label = label == null ? "" : label;
                label = label.replace(TO_BE_REPLACE_REGEX, REPLACING_REGEX);
                model.setLabel(label);
                model.setKpi(kpi);
                model.setPercentage(data.getPercentage());
                model.setOccurrences(data.getOccurrences());
                model.setColor(HPUIUtils.getInsightColor(i));
                model.setInsightCursorDrawableID(HPUIUtils.getInsightCursorDrawable(i));
                model.setCorrectiveActions(data.getHints());
                model.setHasCorrectiveActions(correctiveActionsEnabled && data.getHints() != null && !data.getHints().isEmpty());

                insightModels.add(model);
            }
        }

        insightsViewModel.setInsightModels(insightModels);
        insightsViewModel.setInsightKpiEnum(kpi);
        insightsViewModel.setSelectedDeviceCount(bu.getFiltersViewModel().getSelectedDevices().size());
        insightsViewModel.setTotalDeviceCount(bu.getFiltersViewModel().getSiteDevicesIds().size());

        return insightsViewModel;
    }

    public static Observable<List<KZItem>> getKZRecommendedContent(final BusinessUnitViewModel businessUnitViewModel) {

        String appLanguage = PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).getLanguage();
        LanguageEnum languageEnum = LanguageEnum.from(appLanguage);

        List<String> supportedFormats = PrintOSPreferences.getInstance(PrintOSApplication.getAppContext())
                .getInsightsConfigurations().getKzSupportedFormats();

        ApiServicesProvider serviceProvider = PrintOSApplication.getApiServicesProvider();
        InsightsService insightsService = serviceProvider.getInsightsService();

        return insightsService.getKZRecommendedContent(businessUnitViewModel.getBusinessUnit().getKzName(),
                true, RECOMMENDED_CONTENT_SIZE, languageEnum.getKzLanguageCode(), supportedFormats).map(new Func1<Response<RecommendedContentData>, List<KZItem>>() {
            @Override
            public List<KZItem> call(Response<RecommendedContentData> recommendedContentDataResponse) {
                if (recommendedContentDataResponse.isSuccessful()) {
                    return recommendedContentDataResponse.body().getItems();
                }
                return null;
            }
        });

    }

    public static void setItemFavorite(final Context context, String kzItem, String businessUnitName, final ItemCallback setFavoriteItemCallback) {
        final InsightsService insightsService = PrintOSApplication.getApiServicesProvider().getInsightsService();
        insightsService.setFavorite(businessUnitName, kzItem).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Response<ResponseBody>>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        if (setFavoriteItemCallback != null) {
                            setFavoriteItemCallback.onFailure();
                        }
                    }

                    @Override
                    public void onNext(Response<ResponseBody> responseBodyResponse) {
                        HPUIUtils.displayToastWithCenteredText(context, PrintOSApplication.getAppContext().getString(responseBodyResponse.isSuccessful() ? R.string.success_adding_item_to_favorites : R.string.failed_to_add_item_to_favorites));
                        if (setFavoriteItemCallback != null) {
                            if (responseBodyResponse.isSuccessful()) {
                                setFavoriteItemCallback.onSuccess();
                            } else {

                                setFavoriteItemCallback.onFailure();
                            }
                        }
                    }
                });
    }

    public static void removeFavoriteItem(String kzItem, String businessUnitName, final ItemCallback removeFavoriteItemCallback) {
        final InsightsService insightsService = PrintOSApplication.getApiServicesProvider().getInsightsService();
        insightsService.removeFavorite(businessUnitName, kzItem).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Response<ResponseBody>>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        if (removeFavoriteItemCallback != null) {
                            removeFavoriteItemCallback.onFailure();
                        }
                    }

                    @Override
                    public void onNext(Response<ResponseBody> responseBodyResponse) {
                        if (removeFavoriteItemCallback != null) {
                            if (responseBodyResponse.isSuccessful()) {
                                removeFavoriteItemCallback.onSuccess();
                            } else {
                                removeFavoriteItemCallback.onFailure();
                            }
                        }
                    }
                });
    }

    public static void rateItem(String kzItem, String businessUnitName, int rating, final ItemCallback rateCallback) {
        final InsightsService insightsService = PrintOSApplication.getApiServicesProvider().getInsightsService();
        insightsService.rateItem(businessUnitName, kzItem, rating).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Response<ResponseBody>>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        if (rateCallback != null) {
                            rateCallback.onFailure();
                        }
                    }

                    @Override
                    public void onNext(Response<ResponseBody> responseBodyResponse) {
                        if (rateCallback != null) {
                            if (responseBodyResponse != null && responseBodyResponse.isSuccessful()) {
                                rateCallback.onSuccess();
                            } else {
                                rateCallback.onFailure();
                            }
                        }
                    }
                });
    }

    public static Observable<Response<KZFavorites>> getFavorites(final String businessUnitViewModelName, int pageSize, int page, List<String> supportedFormats) {

        String appLanguage = PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).getLanguage();
        LanguageEnum languageEnum = LanguageEnum.from(appLanguage);

        ApiServicesProvider serviceProvider = PrintOSApplication.getApiServicesProvider();
        InsightsService insightsService = serviceProvider.getInsightsService();

        return insightsService.getFavorites(businessUnitViewModelName, page,
                pageSize, true, languageEnum.getKzLanguageCode(), supportedFormats).
                map(new Func1<Response<KZFavorites>, Response<KZFavorites>>() {
                        @Override
                        public Response<KZFavorites> call(Response<KZFavorites> kzFavoritesResponse) {
                            if (kzFavoritesResponse != null) {
                                return kzFavoritesResponse;
                            }
                            return null;
                        }
                    }
                );

    }

    public static Observable<String> getURLContent(final String url) {

        ApiServicesProvider serviceProvider = PrintOSApplication.getApiServicesProvider();
        InsightsService insightsService = serviceProvider.getInsightsService();

        return insightsService.getURLContent(url).
                map(new Func1<Response<ResponseBody>, String>() {
                    @Override
                    public String call(Response<ResponseBody> responseBodyResponse) {
                        if (responseBodyResponse != null && responseBodyResponse.isSuccessful()) {
                            String responseBody = null;
                            try {
                                if (responseBodyResponse.body() != null) {
                                    responseBody = responseBodyResponse.body().string();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                HPLogger.e(TAG, "Error getting knowledge zone url", e);
                            }
                            return responseBody;
                        } else {
                            return null;
                        }
                    }
                });
    }

    public interface ItemCallback {
        void onSuccess();

        void onFailure();
    }
}