package com.hp.printosmobile.data.remote.services;

import com.hp.printosmobile.data.remote.ApiConstants;
import com.hp.printosmobile.data.remote.models.BusinessUnit;
import com.hp.printosmobile.data.remote.models.PermissionsData;

import java.util.Map;

import retrofit2.Response;
import retrofit2.http.GET;
import rx.Observable;

/**
 * Retrofit REST interface definition for Meta data endpoint.
 *
 * @author Osama Taha
 */
public interface MetaDataService {

    @GET(ApiConstants.KPI_META_DATA_API)
    Observable<Response<Map<String, BusinessUnit>>> getMetaData();

    @GET(ApiConstants.PERMISSIONS_META_DATA_API)
    Observable<Response<PermissionsData>> getPermissions();


}
