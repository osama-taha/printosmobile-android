package com.hp.printosmobile.data.local;

import android.content.Context;
import android.text.TextUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hp.printosmobile.Constants;
import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;
import com.hp.printosmobile.data.remote.models.AccountType;
import com.hp.printosmobile.data.remote.models.InviteUserRoleData;
import com.hp.printosmobile.data.remote.models.MergedTodayAndLastWeekData;
import com.hp.printosmobile.data.remote.models.PreferencesData;
import com.hp.printosmobile.data.remote.models.VersionsData;
import com.hp.printosmobile.presentation.indigowidget.WidgetType;
import com.hp.printosmobile.presentation.modules.coins.BeatCoinsPresenter;
import com.hp.printosmobile.presentation.modules.drawer.OrganizationViewModel;
import com.hp.printosmobile.presentation.modules.insights.kz.KZSupportedFormat;
import com.hp.printosmobile.presentation.modules.invites.InviteUserRoleViewModel;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitEnum;
import com.hp.printosmobile.presentation.modules.shared.BusinessUnitThresholds;
import com.hp.printosmobile.presentation.modules.shared.UserViewModel;
import com.hp.printosmobile.presentation.modules.today.HistogramGraphUtils;
import com.hp.printosmobile.utils.HPStringUtils;
import com.hp.printosmobilelib.core.communications.remote.Preferences;
import com.hp.printosmobilelib.core.communications.remote.models.UserData;
import com.hp.printosmobilelib.core.logging.HPLogger;
import com.hp.printosmobilelib.core.utils.HPDateUtils;

import java.io.IOException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by Osama Taha on 6/14/16.
 */
public class PrintOSPreferences {

    private static final String TAG = PrintOSPreferences.class.getName();

    private static final String WIDGET_LAST_REFRESH_TIME = "appwidget_refresh_time_";
    private static final String GOOGLE_PLAY_SERVICES_STATUS = "google_play_services_status";
    private static final String UNIT_SYSTEM_PREF = "unit_system";
    private static final String KEY_IS_COMPLETED_ENABLED = "key_is_completed_enabled";
    private static final String LANGUAGE_SPLIT_REGEX = "-";
    private static final String NPS_NOTIFICATION = "key_nps_notification";
    private static final String NPS_DISPLAY_TIMING = "key_nps_display_timing";
    private static final String RANKING_LEADERBOARD_POPUP_DISPLAY_TIMING = "key_ranking_leaderboard_popup_display_timing";
    private static final String RANKING_LEADERBOARD_POPUP_DONT_SHOW_AGAIN = "key_ranking_leaderboard_popup_dont_show_again";
    private static final String SEND_INVITES_TIMING = "key_send_invites_display_timing";
    private static final String HAS_INDIGO_DIVISION = "key_has_indigo_division";
    private static final String NPS_FEATURE_ENABLED = "nps_feature_enabled";
    private static final String BEAT_COIN_FEATURE_ENABLED = "beat_coin_feature_enabled";
    private static final String WEEK_INCREMENTAL_ENABLED = "week_incremental_enabled";
    private static final String REFER_TO_FRIEND_ENABLED = "refer_to_friend_enabled";
    private static final String REFER_TO_FRIEND_URL = "refer_to_friend_url";
    private static final String CLOSED_SERVICE_CALLS_DAYS = "closed_service_calls_days";
    private static final String NPS_MIN_DISPLAY_TIME_INTERVAL = "nps_min_display_time_interval";
    private static final String KEY_IGNORE_FLAGS = "ignore_flags_config";
    private static final String KEY_CHAT_USER_MEMBER_IN_ORGANIZATION = "user_member_in_org";
    private static final String TODAY_GRAPH_IDENTIFIER = "today_graph_identifier";
    private static final String TODAY_GRAPH_DATE = "today_graph_data";
    private static final String TODAY_GRAPH_SUPPORTED_EQUIPMENTS = "today_graph_supported_equipments";
    private static final String LOW_PRINT_VOLUME_KEY = "low_print_volume_key";
    private static final int LOW_IMPRESSION_DEFAULT_VALUE = 30000;
    private static final String FORGOT_PASS_ENABLED = "forgot_password_enabled";
    private static final String CORRECTIVE_ACTIONS_ENABLED = "corrective_actions_enabled";
    private static final String RANKING_LEADERBOARD_ENABLED = "ranking_leaderboard_enabled";


    private static final String SELECTED_BUSINESS_UNIT = "selected_business_unit";
    private static final String KEY_LOGIN_USER_FIRST_NAME = "LoggedInUserFirstName";
    private static final String KEY_LOGIN_USER_LAST_NAME = "LoggedInUserLastName";
    private static final String KEY_LOGIN_USER_DISPLAY_NAME = "LoggedInUserDisplayName";
    private static final String KEY_LOGIN_USER_ID = "LoggedInUserID";
    private static final String KEY_USER_COUNTRY_CODE = "LoggedInUserCountryCode";
    private static final String KEY_LAST_VERSION_CHECKING_TIME = "lastVersionCheckinTime";
    private static final String KEY_SUPPORTED_VERSIONS = "supportedVersions";
    private static final String KEY_DID_SHOW_WARNING_DIALOG = "didShowWarningDialog";
    private static final String KEY_IS_LOGGED_IN = "isLoggedIn";
    private static final String KEY_BASE_ORGANIZATION_ID = "baseOrganizationId";
    private static final String LANGUAGE_PREFERENCE = "Language";
    private static final String USER_ROLES = "LoggedInUserRoles";
    private static final String SELF_PROVISION_ORGANIZATION = "selfProvisionOrganization";
    private static final String IS_FIRST_LAUNCH_KEY = "isFirstLaunchKey";
    private static final String BETA_TESTER_NOTIFICATION = "beta_testers_notification";

    private static final String CHECK_COOKE_EXPIRY_TIME_KEY = "checkCookieExpiryHrs";
    private static final String MOVE_TO_HOME_TIME_KEY = "moveToHomeMins";
    private static final String SHOW_VALIDATION_TOAST_KEY = "showToastMessage";

    //Keys for GCM.
    private static final String KEY_SENT_TOKEN_TO_SERVER = "sentTokenToServer";
    private static final String KEY_DEVICE_GCM_TOKEN = "deviceGCMToken";

    //rating keys
    private static final String VERSION_NAME_PREF = "version_name_pref";
    private static final String RATE_STATUS_PREF = "rate_status_pref";
    private static final String RATE_DIALOG_DATE_SHOWN_PREF = "rate_dialog_date_shown_pref";
    private static final String RATE_DIALOG_RECURRING_PREF = "rate_dialog_recurring_pref";
    private static final String RATE_DIALOG_DATE_FORMAT = "dd-MM-yyyy";
    private static final int RATE_DIALOG_RECURRING_DEF_VALUE = 90;

    private static final String INSIGHTS_NEW_TAB = "Insights_NewTab";
    private static final String INSIGHTS_TAB_VERSION = "insights_tabVersion";
    private static final String INSIGHTS_JAMS_ENABLED = "insights_jamsEnabled";
    private static final String INSIGHTS_FAILURES_ENABLED = "insights_failuresEnabled";
    private static final String CHANNEL_SUPPORT_KZ = "channel_kz_supported";

    private static final String KPI_EXPLANATION_ENABLED = "kpi_explanation_enabled";

    private static final String FILTER_SEARCH_BOX_DEVICE_COUNT_KEY = "filter_search_box_device_coount_key";

    private static final String TODAY_GRAPH_ENABLED_KEY = "today_graph_enabled";

    private static final String REALTIME_REFRESH_ENABLED = "realtime_refresh_enabled";
    private static final String REALTIME_REFRESH_RATE = "realtime_refresh_rate";

    private static final String SUCCESS_THRESHOLD_KEY = "success_threshold_key";
    private static final String AVERAGE_THRESHOLD_KEY = "average_threshold_key";
    private static final String FAIL_THRESHOLD_KEY = "fail_threshold_key";

    private static final String PROFILE_IMAGE = "profile_image_url";

    private static final String KEY_USER_EMAIL = "key_user_email";
    private static final String LANGUAGE_INVITES_PREFERENCE = "language_invites_preference";
    private static final String INVITES_WITH_COINS_KEY = "invites_with_coins_key";
    private static final String CACHED_INVITE_USER_ROLE_DATA_KEY = "cached_invite_user_role_data_key";
    private static final String CACHED_INVITE_USER_ROLE_DATA_TIME_KEY = "cached_invite_user_role_data_time_key";
    private static final String SELECTED_INVITE_USER_ROLE_KEY_ID = "selected_invite_user_role_key_id";
    private static final String SELECTED_INVITE_USER_ROLE_KEY_NAME = "selected_invite_user_role_key_name";
    private static final String SELECTED_INVITE_USER_ROLE_KEY_TYPE = "selected_invite_user_role_key_type";
    private static final String INVITE_FEATURE_FLAG = "invite_feature_flag";
    private static final String DID_AUTO_SUBSCRIBE_TO_INTRA_DAILY = "did_auto_subscribe_to_intra_daily";
    private static final String INTRA_DAILY_UPDATE_AUTO_SUBSCRIPTION_VERSION = "intra_daily_update_auto_subscription_version";
    private static final String INVITE_ALL_SUPPRESS_KEY = "invite_all_suppress_key";

    private static final int SUCCESS_THRESHOLD_DEF = 80;
    private static final int AVERAGE_THRESHOLD_DEF = 70;
    private static final int FAIL_THRESHOLD_DEF = 50;

    private static final String BEAT_COINS_PER_INVITE_KEY = "beat_coins_per_invite_key";
    private static final String INVITE_POPUP_DISPLAY_COUNTER_KEY = "invite_popup_display_counter_key";
    private static final String SMS_ENABLED_KEY = "sms_enabled_key";
    private static final int BEAT_COINS_PER_INVITE_DEF_VALUE = 25;
    private static final int INVITE_COUNTER_DEF_VALUE = 3;

    private static final String HISTOGRAM_GRAPH_ENABLED_KEY = "histogram_graph_enabled_key";
    private static final String HISTOGRAM_DAY_CHART_KEY = "histogram_day_chart_key";
    private static final String HISTOGRAM_SHIFT_CHART_KEY = "histogram_shift_chart_key";
    private static final String HISTOGRAM_NUMBER_OF_SHIFTS_KEY = "histogram_number_of_shifts_key";

    private static final int WIZARD_DEFAULT_COINS_REWARD = 50;
    private static final Integer WIZARD_DEFAULT_APP_LAUNCHES_COUNT = 10;
    private static final String WIZARD_APP_LAUNCHES = "wizard_app_launches";
    private static final String WIZARD_POPUP_SHOWN = "wizard_popup_shown";

    private static final String KPI_BREAKDOWN_REPORT_ENABLED_KEY = "kpi_breakdown_report_enabled";
    private static final String STATE_DISTRIBUTION_ENABLED_KEY = "kpi_breakdown_report_enabled";
    private static final String IS_IN_DEMO_MODE_KEY = "IS_IN_DEMO_MODE_KEY";
    private static final String DEMO_ACCOUNT_TIMESTAMP = "DEMO_ACCOUNT_TIMESTAMP";
    private static final String HPID_FEATURE_FLAG_ENABLED_KEY = "hpid_enabled";
    private static final String NSLOOKUP_FILE_NAME = "nslookup_file_name";
    private static final String CHINA_LOGGING_ENABLED_KEY = "china_logging_enabled";

    // china/location dialog
    private static final String LAST_SAVED_LOCATION = "LAST_SAVED_LOCATION";
    private static final String SWITCH_SERVER_DIALOG_OPENED = "SWITCH_SERVER_DIALOG_OPENED";
    private static final String SESSION_NUMBER = "SESSION_NUMBER";
    private static final String FAKE_LOCATION = "FAKE_LOCATION";

    private static final String LATEX_ONLY_KEY = "latex_only";

    private static final String TARGET_VIEW_SHOWN_KEY_FORMAT = "date_%s";
    private static final String TARGET_VIEW_SHOWN_DATE_FORMAT = "yyyy-MM-dd";
    private static final String KZ_LAST_SELECTED_BU = "kz_last_selected_bu";

    private static final String LAST_SESSION_VALIDATION_TIME = "last_valid_session_time";

    private static final String BEAT_COINS_CLAIMED = "beat_coins_claimed";
    private static final String HAS_INDIGO_GBU_KEY = "has_indigo_gbu_key";

    private final Context context;
    private BusinessUnitThresholds thresholds;

    private static PrintOSPreferences sSharedPrintOSPreferences;
    private static Preferences sPreferences;

    private PrintOSPreferences(Context context) {
        sPreferences = Preferences.getInstance(context);
        this.context = context;
    }

    public static PrintOSPreferences getInstance() {
        return getInstance(PrintOSApplication.getAppContext());
    }

    public static PrintOSPreferences getInstance(Context context) {

        if (sSharedPrintOSPreferences == null) {
            sSharedPrintOSPreferences = new PrintOSPreferences(context.getApplicationContext());
        }

        return sSharedPrintOSPreferences;
    }


    public void saveSelectedBusinessUnit(BusinessUnitEnum selectedBusinessUnit) {

        sPreferences.put(SELECTED_BUSINESS_UNIT, selectedBusinessUnit.getName());
        sPreferences.commit();
    }

    public BusinessUnitEnum getSelectedBusinessUnit() {

        return BusinessUnitEnum.from(sPreferences.getString(SELECTED_BUSINESS_UNIT, null));

    }

    /**
     * Saves the information pertaining to the currently logged in user.
     *
     * @param userData - An object contains the user details.
     */
    public void saveUserInfo(UserData.User userData) {

        sPreferences.put(KEY_LOGIN_USER_ID, userData.getUserId());
        sPreferences.put(KEY_LOGIN_USER_FIRST_NAME, userData.getFirstName());
        sPreferences.put(KEY_LOGIN_USER_LAST_NAME, userData.getLastName());
        sPreferences.put(KEY_LOGIN_USER_DISPLAY_NAME, userData.getDisplayName());
        sPreferences.put(KEY_USER_EMAIL, userData.getPrimaryEmail() != null ? userData.getPrimaryEmail() : "");
        sPreferences.put(KEY_USER_COUNTRY_CODE, userData.getAddress() != null && userData.getAddress().getCountry() != null ? userData.getAddress().getCountry() : "");

        //Commit the changes.
        sPreferences.commit();

        saveUserInfo();

    }

    /**
     * Retrieves the information pertaining to the currently logged in user.
     *
     * @return user object.
     */
    public UserViewModel getUserInfo() {

        UserViewModel user = new UserViewModel();

        user.setUserId(sPreferences.getString(KEY_LOGIN_USER_ID, ""));
        user.setDisplayName(sPreferences.getString(KEY_LOGIN_USER_DISPLAY_NAME, ""));
        user.setFirstName(sPreferences.getString(KEY_LOGIN_USER_FIRST_NAME, ""));
        user.setLastName(sPreferences.getString(KEY_LOGIN_USER_LAST_NAME, ""));
        user.setUserRoles(sPreferences.getString(USER_ROLES, ""));
        user.setSelfProvisionOrg(sPreferences.getString(SELF_PROVISION_ORGANIZATION, ""));
        user.setEmail(sPreferences.getString(KEY_USER_EMAIL, ""));
        user.setCountryCode(sPreferences.getString(KEY_USER_COUNTRY_CODE, ""));
        user.setUserOrganization(getSelectedOrganization());

        return user;
    }

    public void saveCurrentOrganization(UserData.Context organization) {
        sPreferences.saveOrganization(organization);
    }

    public void saveUserInfo() {
        UserViewModel userViewModel = getUserInfo();
        sPreferences.saveUserId(userViewModel.getUserId());
    }

    public void saveCurrentOrganization(OrganizationViewModel organization) {

        UserData.Context organizationContext = new UserData.Context();
        organizationContext.setId(organization.getOrganizationId());
        organizationContext.setType(organization.getOrganizationTypeDisplayName());
        organizationContext.setName(organization.getOrganizationName());

        sPreferences.saveOrganization(organizationContext);
    }

    public void setBaseOrganization(String organizationId) {
        sPreferences.put(KEY_BASE_ORGANIZATION_ID, organizationId);
        sPreferences.commit();
    }

    public static String getBaseOrganizationId() {
        return sPreferences.getString(KEY_BASE_ORGANIZATION_ID, null);
    }

    public void saveUserType(String userType) {
        sPreferences.saveUserType(userType);
    }

    public AccountType getAccountType() {
        return AccountType.from(sPreferences.getUserType());
    }

    public void clearOrganization() {
        sPreferences.clearOrganization();
    }


    public OrganizationViewModel getSelectedOrganization() {

        UserData.Context organizationContext = Preferences.getInstance(PrintOSApplication.getAppContext()).getSelectedOrganization();

        OrganizationViewModel organizationViewModel = new OrganizationViewModel();
        organizationViewModel.setOrganizationId(organizationContext.getId());
        organizationViewModel.setOrganizationName(organizationContext.getName());
        organizationViewModel.setOrganizationTypeDisplayName(organizationContext.getType());
        organizationViewModel.setAccountType(AccountType.from(organizationContext.getType()));

        return organizationViewModel;
    }

    VersionsData data = null;

    public VersionsData getVersionsData() {

        if (data != null) {
            return data;
        }

        String versionsDataJson = sPreferences.getString(KEY_SUPPORTED_VERSIONS, null);

        if (!TextUtils.isEmpty(versionsDataJson)) {

            ObjectMapper mapper = new ObjectMapper();
            try {
                data = mapper.readValue(versionsDataJson, VersionsData.class);
                return data;
            } catch (Exception e) {
                HPLogger.e(TAG, "unable to parse the versions data " + e);
            }
        }

        return null;
    }

    public void setSupportedVersions(String versions) {
        data = null;
        sPreferences.put(KEY_SUPPORTED_VERSIONS, versions);
        sPreferences.commit();
    }

    public VersionsData.Configuration.SendInvitesData getSendInvitesData() {
        VersionsData versionsData = getVersionsData();
        if (versionsData == null || versionsData.getConfiguration() == null) {
            return null;
        }
        return versionsData.getConfiguration().getSendInvitesData();
    }

    public VersionsData.Configuration.Wizard getWizardData() {

        VersionsData versionsData = getVersionsData();
        if (versionsData == null || versionsData.getConfiguration() == null || versionsData.getConfiguration().getWizard() == null) {
            VersionsData.Configuration.Wizard wizard = new VersionsData.Configuration.Wizard();
            wizard.setAppLaunchesCount(WIZARD_DEFAULT_APP_LAUNCHES_COUNT);
            wizard.setCoinsReward(WIZARD_DEFAULT_COINS_REWARD);
            wizard.setState(VersionsData.FeatureFlag.ENGLISH_ONLY);
            return wizard;
        }

        return versionsData.getConfiguration().getWizard();
    }

    public VersionsData.Configuration.Insights getInsightsConfigurations() {

        VersionsData versionsData = getVersionsData();
        if (versionsData == null || versionsData.getConfiguration() == null || versionsData.getConfiguration().getInsights() == null) {
            VersionsData.Configuration.Insights insights = new VersionsData.Configuration.Insights();
            insights.setJamsEnabled(true);
            insights.setFailuresEnabled(true);
            insights.setBadgeVersion(null);
            insights.setKzSupportedFormats(Arrays.asList(KZSupportedFormat.PDF.getValue()));
            insights.setKzSearchEnabled(shouldIgnoreFlags() ? VersionsData.FeatureFlag.ON : VersionsData.FeatureFlag.OFF);
            insights.setKzSupportedEquipments(Arrays.asList(BusinessUnitEnum.INDIGO_PRESS.getName()));

            return insights;
        }

        List<String> supportedFormats = versionsData.getConfiguration().getInsights().getKzSupportedFormats();

        if (supportedFormats == null || supportedFormats.isEmpty()) {
            versionsData.getConfiguration().getInsights().setKzSupportedFormats(Arrays.asList(KZSupportedFormat.PDF.getValue()));
        }

        List<String> supportedEquipments = versionsData.getConfiguration().getInsights().getKzSupportedEquipments();
        if (supportedEquipments == null || supportedEquipments.isEmpty()) {
            versionsData.getConfiguration().getInsights().setKzSupportedEquipments(Arrays.asList(BusinessUnitEnum.INDIGO_PRESS.getName()));
        }

        return versionsData.getConfiguration().getInsights();
    }

    public boolean isKnowledgeZoneSearchEnabled() {

        VersionsData.Configuration.Insights insightsConfigurations = getInsightsConfigurations();
        if (insightsConfigurations.getKzSearchEnabled() == VersionsData.FeatureFlag.ENGLISH_ONLY && isDefaultLanguageSet(context)) {
            return true;
        }

        return insightsConfigurations.getKzSearchEnabled() == VersionsData.FeatureFlag.ON;
    }

    public boolean wizardEnabled() {

        VersionsData.Configuration.Wizard wizard = getWizardData();

        if (wizard.getState() == VersionsData.FeatureFlag.ENGLISH_ONLY && isDefaultLanguageSet(context)) {
            return true;
        }

        return wizard.getState() == VersionsData.FeatureFlag.ON;

    }

    public boolean isWizardPopupShown() {
        return sPreferences.getBoolean(WIZARD_POPUP_SHOWN, false);
    }

    public void setWizardPopupShown(boolean shown) {
        sPreferences.put(WIZARD_POPUP_SHOWN, shown);
        sPreferences.commit();
    }

    public boolean timeToDisplayWizardPopup() {

        VersionsData.Configuration.Wizard wizard = getWizardData();
        return !isWizardPopupShown() || getWizardAppLaunchesCount() >= wizard.getAppLaunchesCount();

    }

    public int getWizardReward() {
        VersionsData.Configuration.Wizard wizard = getWizardData();
        return wizard.getCoinsReward();
    }

    public int getWizardAppLaunchesCount() {
        return sPreferences.getInt(WIZARD_APP_LAUNCHES, -1);
    }

    public void incrementAppLaunchesCountForWizard() {
        int count = getWizardAppLaunchesCount();
        sPreferences.put(WIZARD_APP_LAUNCHES, count + 1);
        sPreferences.commit();
    }

    public void resetAppLaunchesCountWizard() {
        sPreferences.put(WIZARD_APP_LAUNCHES, 0);
        sPreferences.commit();
    }

    public int getWizard() {

        VersionsData.Configuration.Wizard wizard = getWizardData();

        if (wizard == null) {
            return WIZARD_DEFAULT_COINS_REWARD;
        }

        return wizard.getCoinsReward();
    }

    public void setShouldIgnoreFlags(boolean ignore) {
        sPreferences.put(KEY_IGNORE_FLAGS, ignore);
        sPreferences.commit();
    }


    public boolean shouldIgnoreFlags() {
        return sPreferences.getBoolean(KEY_IGNORE_FLAGS, false);
    }

    public long getVersionCheckingTime() {
        return sPreferences.getLong(KEY_LAST_VERSION_CHECKING_TIME, -1);
    }

    public void setVersionCheckingTime(long time) {
        sPreferences.put(KEY_LAST_VERSION_CHECKING_TIME, time);
        sPreferences.commit();
    }

    public boolean isWarningUpdateDialogEnabled() {
        return sPreferences.getBoolean(KEY_DID_SHOW_WARNING_DIALOG, true);
    }

    public void enableWarningUpdateDialog(Boolean enable) {
        sPreferences.put(KEY_DID_SHOW_WARNING_DIALOG, enable);
        sPreferences.commit();
    }


    public void enableCompletedJobsFeature(boolean enable) {
        sPreferences.put(KEY_IS_COMPLETED_ENABLED, enable);
        sPreferences.commit();
    }

    public boolean isCompletedJobsFeatureEnabled() {
        return sPreferences.getBoolean(KEY_IS_COMPLETED_ENABLED, false) || shouldIgnoreFlags();
    }

    public String getSavedOrganizationId() {
        return sPreferences.getSavedOrganizationId();
    }

    public void setSentNotificationTokenToServer(boolean sent) {
        sPreferences.put(KEY_SENT_TOKEN_TO_SERVER, sent);
        sPreferences.commit();
    }

    public void setNotificationToken(String token) {
        sPreferences.put(KEY_DEVICE_GCM_TOKEN, token);
        sPreferences.commit();
    }

    public String getNotificationToken() {
        return sPreferences.getString(KEY_DEVICE_GCM_TOKEN, "");
    }

    public void setLoggedIn(boolean isLoggedIn) {
        sPreferences.put(KEY_IS_LOGGED_IN, isLoggedIn);
        sPreferences.commit();
    }


    public boolean isLoggedIn() {
        return sPreferences.getBoolean(KEY_IS_LOGGED_IN, false);
    }

    public String getServerUrl() {
        return sPreferences.getString(context.getString(R.string.pref_server_choose),
                context.getString(R.string.default_server));
    }

    public void setServerUrl(Context context, String serverUrl) {
        sPreferences.put(context.getString(R.string.pref_server_choose),
                serverUrl);
    }

    public String getServerName() {

        String prefValue = getServerUrl();
        CharSequence[] keys = PrintOSApplication.getAppContext().getResources().getTextArray(R.array.pref_servers_titles);
        CharSequence[] values = PrintOSApplication.getAppContext().getResources().getTextArray(R.array.pref_servers_values);
        int length = values.length;
        for (int i = 0; i < length; i++) {
            if (values[i].equals(prefValue)) {
                return (String) keys[i];
            }
        }

        return "";
    }

    public boolean isProductionStack() {
        return getServerName().equals(PrintOSApplication.getAppContext().getString(R.string.default_server_item)) || isChinaProductionStack();
    }

    public boolean isChinaProductionStack() {
        return getServerUrl().equals(PrintOSApplication.getAppContext().getString(R.string.china_production_server));
    }

    private String lang;

    public void setLanguage(String language) {
        lang = language;
        sPreferences.put(LANGUAGE_PREFERENCE, language);
        sPreferences.commit();
    }

    public String getLanguage(String defaultValue) {
        if (lang != null) {
            return lang;
        }
        lang = sPreferences.getString(LANGUAGE_PREFERENCE, defaultValue);
        return lang;
    }

    public String getLanguage() {
        if (lang != null) {
            return lang;
        }
        lang = sPreferences.getString(LANGUAGE_PREFERENCE, PrintOSApplication.getAppContext().getString(R.string.default_language));
        return lang;
    }

    public void setInvitesLanguage(String language) {
        sPreferences.put(LANGUAGE_INVITES_PREFERENCE, language);
        sPreferences.commit();
    }

    public String getInvitesLanguage() {
        return sPreferences.getString(LANGUAGE_INVITES_PREFERENCE, getLanguage());
    }

    public String getLanguageCode() {
        String language = getLanguage(PrintOSApplication.getAppContext().getString(R.string.default_language));
        return HPStringUtils.getStringBeforeRegex(language, LANGUAGE_SPLIT_REGEX);
    }

    public String getPreferenceValue(String key) {
        return sPreferences.getString(key, "");
    }

    public void savePreferenceValue(String key, String value) {
        sPreferences.put(key, value);
        sPreferences.commit();
    }

    public void saveUserRoles(String roles) {
        sPreferences.put(USER_ROLES, roles);
        sPreferences.commit();
    }

    public void saveSelfProvisionOrganization(String provision) {
        sPreferences.put(SELF_PROVISION_ORGANIZATION, provision);
        sPreferences.commit();
    }

    public void clearChatPreferences(boolean commitChanges) {

        sPreferences.remove(KEY_CHAT_USER_MEMBER_IN_ORGANIZATION);
        if (commitChanges) {
            sPreferences.commit();
        }
    }

    public void saveWidgetLastRefreshTime(WidgetType widgetType, long lastRefreshTime) {
        sPreferences.put(String.valueOf(WIDGET_LAST_REFRESH_TIME + widgetType.name()), lastRefreshTime);
        sPreferences.commit();
    }

    public long getWidgetLastRefreshTime(WidgetType widgetType) {
        return sPreferences.getLong(String.valueOf(WIDGET_LAST_REFRESH_TIME + widgetType.name()), 0);
    }

    public boolean isFirstLaunch() {
        return sPreferences.getBoolean(IS_FIRST_LAUNCH_KEY, true);
    }

    public void setIsFirstLaunch(boolean isFirstLaunch) {
        sPreferences.put(IS_FIRST_LAUNCH_KEY, isFirstLaunch);
        sPreferences.commit();
    }

    public void setGooglePlayServicesStatus(String status) {
        sPreferences.put(GOOGLE_PLAY_SERVICES_STATUS, status);
        sPreferences.commit();
    }

    public String getGooglePlayServiceStatus() {
        return sPreferences.getString(GOOGLE_PLAY_SERVICES_STATUS, "");
    }

    public boolean wasBetaTestersNotified() {
        return sPreferences.getBoolean(BETA_TESTER_NOTIFICATION, false);
    }

    public void setBetaTestersNotified() {
        sPreferences.put(BETA_TESTER_NOTIFICATION, true);
        sPreferences.commit();
    }

    public String getAppVersion() {
        return sPreferences.getString(VERSION_NAME_PREF, "");
    }

    public void setAppVersion(String versionName) {
        sPreferences.put(VERSION_NAME_PREF, versionName == null ? "" : versionName);
        sPreferences.commit();
    }

    public void setRateStatus(String rateStatus) {
        sPreferences.put(RATE_STATUS_PREF, rateStatus == null ? "" : rateStatus);
        sPreferences.commit();
    }

    public String getRateStatus() {
        return sPreferences.getString(RATE_STATUS_PREF, "");
    }

    public synchronized void setRateDialogDateShown() {
        String dateS = HPDateUtils.formatDate(Calendar.getInstance().getTime(), RATE_DIALOG_DATE_FORMAT);
        sPreferences.put(RATE_DIALOG_DATE_SHOWN_PREF, dateS);
        sPreferences.commit();
    }

    public synchronized int getRateDialogNumberOfDaysSinceLastShown() {
        String dateS = sPreferences.getString(RATE_DIALOG_DATE_SHOWN_PREF, "");
        Date date = HPDateUtils.parseDate(dateS, RATE_DIALOG_DATE_FORMAT);
        if (date == null) {
            setRateDialogDateShown();
            return 0;
        }
        Date today = Calendar.getInstance().getTime();
        int diff = (int) Math.abs(HPDateUtils.getDaysDiff(date.getTime(), today.getTime()));
        return diff;
    }

    public int getRateDialogRecurringPeriod() {
        return sPreferences.getInt(RATE_DIALOG_RECURRING_PREF, RATE_DIALOG_RECURRING_DEF_VALUE);
    }

    public void setRateDialogRecurringPeriod(int recurringPeriod) {
        sPreferences.put(RATE_DIALOG_RECURRING_PREF, recurringPeriod);
        sPreferences.commit();
    }

    private static PreferencesData.UnitSystem cacheUnitSystem;

    public static void saveUnitSystem(PreferencesData.UnitSystem unitSystem) {
        cacheUnitSystem = null;
        if (unitSystem == null) {
            return;
        }

        HPLogger.d(TAG, "save unit " + unitSystem.name());

        sPreferences.put(UNIT_SYSTEM_PREF, unitSystem.name());
        sPreferences.commit();
    }

    public PreferencesData.UnitSystem getUnitSystem() {
        if (cacheUnitSystem != null) {
            return cacheUnitSystem;
        }

        String unitSystemString = sPreferences.getString(UNIT_SYSTEM_PREF, PreferencesData.UnitSystem.Metric.name());
        return PreferencesData.UnitSystem.from(unitSystemString);
    }

    public void saveValidationParams(int checkCookieExpiryHrs, int moveToHomeMins, boolean showToastMessage) {
        sPreferences.put(CHECK_COOKE_EXPIRY_TIME_KEY, checkCookieExpiryHrs);
        sPreferences.put(MOVE_TO_HOME_TIME_KEY, moveToHomeMins);
        sPreferences.put(SHOW_VALIDATION_TOAST_KEY, showToastMessage);
        sPreferences.commit();
    }

    public int getCookieExpiryTimeFloor(int defValue) {
        return sPreferences.getInt(CHECK_COOKE_EXPIRY_TIME_KEY, defValue);
    }

    public int getMoveToHomeTimePeriod(int defValue) {
        return sPreferences.getInt(MOVE_TO_HOME_TIME_KEY, defValue);
    }

    public boolean showValidationToastMsg(boolean defValue) {
        return sPreferences.getBoolean(SHOW_VALIDATION_TOAST_KEY, defValue);
    }

    public boolean shouldShowInsightsNewBadge(boolean defValue) {
        return sPreferences.getBoolean(INSIGHTS_NEW_TAB, defValue);
    }

    public void setShowInsightsNewBadge(boolean show) {
        sPreferences.put(INSIGHTS_NEW_TAB, show);
    }

    public boolean supportV2() {
        return true;
    }

    public boolean isInsightsJamsEnabled(boolean defValue) {
        return sPreferences.getBoolean(INSIGHTS_JAMS_ENABLED, defValue) || shouldIgnoreFlags();
    }

    public void setInsightsJamsEnabled(boolean enabled) {
        sPreferences.put(INSIGHTS_JAMS_ENABLED, enabled);
        sPreferences.commit();
    }

    public boolean isInsightsFailuresEnabled(boolean defValue) {
        return sPreferences.getBoolean(INSIGHTS_FAILURES_ENABLED, defValue) || shouldIgnoreFlags();
    }

    public void setInsightsFailureEnabled(boolean enabled) {
        sPreferences.put(INSIGHTS_FAILURES_ENABLED, enabled);
        sPreferences.commit();
    }

    public String getInsightsFirstOccuringVersion(String defValue) {
        return sPreferences.getString(INSIGHTS_TAB_VERSION, defValue);
    }

    public void setInsightsFirstOccuringVersion(String version) {
        sPreferences.put(INSIGHTS_TAB_VERSION, version);
        sPreferences.commit();
    }

    public boolean isNPSNotificationsEnabled() {
        return sPreferences.getBoolean(NPS_NOTIFICATION, true);
    }

    public void setNPSNotificationsEnabled(boolean enabled) {
        sPreferences.put(NPS_NOTIFICATION, enabled);
        sPreferences.commit();
    }

    public boolean hasNPSNotificationPreference() {
        return sPreferences.containsKey(NPS_NOTIFICATION);
    }

    public void setNPSLastDisplayTiming(long milliSeconds) {
        sPreferences.put(NPS_DISPLAY_TIMING, milliSeconds);
        sPreferences.commit();
    }

    public long getNPSLastDisplayTiming() {
        return sPreferences.getLong(NPS_DISPLAY_TIMING, 0);
    }

    public void setSendInvitesPopupLastDisplayTiming(long milliSeconds) {
        sPreferences.put(SEND_INVITES_TIMING, milliSeconds);
        sPreferences.commit();
    }

    public long getSendInvitesPopupLastDisplayTiming() {
        return sPreferences.getLong(SEND_INVITES_TIMING, 0);
    }

    public void setHasIndigoDivision(boolean hasIndigoDivision) {
        sPreferences.put(HAS_INDIGO_DIVISION, hasIndigoDivision);
        sPreferences.commit();
    }

    public boolean getHasIndigoDivision() {
        return sPreferences.getBoolean(HAS_INDIGO_DIVISION, false);
    }

    public boolean isNPSFeatureEnabled() {
        return sPreferences.getBoolean(NPS_FEATURE_ENABLED, true) || shouldIgnoreFlags();
    }

    public void setNPSFeatureEnabled(boolean enabled) {
        sPreferences.put(NPS_FEATURE_ENABLED, enabled);
        sPreferences.commit();
    }

    public boolean isBeatCoinFeatureEnabled() {
        return sPreferences.getBoolean(BEAT_COIN_FEATURE_ENABLED, true) || shouldIgnoreFlags();
    }

    public void setBeatCoinFeatureEnabled(boolean enabled) {
        sPreferences.put(BEAT_COIN_FEATURE_ENABLED, enabled);
        sPreferences.commit();
    }


    public boolean isWeekIncrementalFeatureEnabled() {
        return sPreferences.getBoolean(WEEK_INCREMENTAL_ENABLED, false) || shouldIgnoreFlags();
    }

    public void setWeekIncrementalFeatureEnabled(boolean enabled) {
        sPreferences.put(WEEK_INCREMENTAL_ENABLED, enabled);
        sPreferences.commit();
    }

    public boolean isReferToAFriendEnabled() {
        return sPreferences.getBoolean(REFER_TO_FRIEND_ENABLED, false) || shouldIgnoreFlags();
    }

    public void setReferToAFriendEnabled(boolean enabled) {
        sPreferences.put(REFER_TO_FRIEND_ENABLED, enabled);
        sPreferences.commit();
    }

    public String getReferToAFriendUrl() {
        return sPreferences.getString(REFER_TO_FRIEND_URL, null);
    }

    public void setReferToAFriendUrl(String url) {
        sPreferences.put(REFER_TO_FRIEND_URL, url);
        sPreferences.commit();
    }

    public boolean isKpiExplanationEnabled() {
        return sPreferences.getBoolean(KPI_EXPLANATION_ENABLED, false) || shouldIgnoreFlags();
    }

    public void setKpiExplanationEnabled(boolean enabled) {
        sPreferences.put(KPI_EXPLANATION_ENABLED, enabled);
        sPreferences.commit();
    }


    public void firstTimeAskingPermission(String permission, boolean isFirstTime) {
        sPreferences.put(permission, isFirstTime);
        sPreferences.commit();
    }

    public boolean isFirstTimeAskingPermission(String permission) {
        return sPreferences.getBoolean(permission, true);
    }

    public void setFilterSearchBoxDeviceCount(int count) {
        sPreferences.put(FILTER_SEARCH_BOX_DEVICE_COUNT_KEY, count);
        sPreferences.commit();
    }

    public int getFilterSearchBoxDeviceCount(int defValue) {
        return sPreferences.getInt(FILTER_SEARCH_BOX_DEVICE_COUNT_KEY, defValue);
    }

    public int closedServiceCallsDays(int closedServiceCallsDaysDefault) {

        if (sPreferences.containsKey(CLOSED_SERVICE_CALLS_DAYS) && sPreferences.getInt(CLOSED_SERVICE_CALLS_DAYS) > 0) {
            return sPreferences.getInt(CLOSED_SERVICE_CALLS_DAYS);
        }

        return closedServiceCallsDaysDefault;
    }

    public void setClosedServiceCallsDays(int closedServiceCallsDays) {
        sPreferences.put(CLOSED_SERVICE_CALLS_DAYS, closedServiceCallsDays);
        sPreferences.commit();
    }

    public void setNPSMinDisplayTimeInterval(int time) {
        sPreferences.put(NPS_MIN_DISPLAY_TIME_INTERVAL, time);
        sPreferences.commit();
    }

    public int getNPSMinDisplayTimeInterval(int defValue) {
        return sPreferences.getInt(NPS_MIN_DISPLAY_TIME_INTERVAL, defValue);
    }

    public void setTodayGraphEnabled(boolean enabled) {
        sPreferences.put(TODAY_GRAPH_ENABLED_KEY, enabled);
        sPreferences.commit();
    }

    public int getMaintenanceMessageShowCount(String messageGuid) {
        return sPreferences.getInt(messageGuid, 0);
    }

    public void incrementMaintenanceMessageShowCount(String messageGuid) {
        int showCount = getMaintenanceMessageShowCount(messageGuid);
        sPreferences.put(messageGuid, showCount + 1);
        sPreferences.commit();
    }

    public boolean isCurrentUserMemberInOrganization() {
        return sPreferences.getBoolean(KEY_CHAT_USER_MEMBER_IN_ORGANIZATION, false);
    }

    public void setCurrentUserMemberInOrganization(boolean isMember) {
        sPreferences.put(KEY_CHAT_USER_MEMBER_IN_ORGANIZATION, isMember);
        sPreferences.commit();
    }

    public void setTargetViewShown(String feature) {
        if (feature == null) {
            return;
        }

        Date today = Calendar.getInstance().getTime();
        String datePref = HPDateUtils.formatDate(today, TARGET_VIEW_SHOWN_DATE_FORMAT);
        String key = String.format(TARGET_VIEW_SHOWN_KEY_FORMAT, feature);
        sPreferences.put(key, datePref);
        sPreferences.commit();
    }

    public boolean isTargetViewShown(String feature, int recurringPeriod) {
        if (feature == null) {
            return false;
        }

        String key = String.format(TARGET_VIEW_SHOWN_KEY_FORMAT, feature);
        String datePref = sPreferences.getString(key, "");
        try {
            Date dateShown = HPDateUtils.parseDate(datePref, TARGET_VIEW_SHOWN_DATE_FORMAT);
            Date today = Calendar.getInstance().getTime();

            long diffInMils = Math.abs(dateShown.getTime() - today.getTime());
            long diffInDays = TimeUnit.DAYS.convert(diffInMils, TimeUnit.MILLISECONDS);

            return diffInDays < recurringPeriod;
        } catch (Exception e) {
            return false;
        }
    }

    public void setTodayGraphIdentifier(String identifier) {
        sPreferences.put(TODAY_GRAPH_IDENTIFIER, identifier == null ? "" : identifier);
        sPreferences.commit();
    }

    public String getTodayGraphIdentifier() {
        return sPreferences.getString(TODAY_GRAPH_IDENTIFIER, "");
    }

    public void setTodayGraphCache(MergedTodayAndLastWeekData data) {
        String json;
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            json = objectMapper.writeValueAsString(data);
        } catch (Exception e) {
            json = "";
        }

        sPreferences.put(TODAY_GRAPH_DATE, json);
        sPreferences.commit();
    }

    public MergedTodayAndLastWeekData getTodayGraphCache() {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            String cachedJson = sPreferences.getString(TODAY_GRAPH_DATE, "");
            if (TextUtils.isEmpty(cachedJson)) {
                return null;
            }
            return objectMapper.readValue(cachedJson, MergedTodayAndLastWeekData.class);
        } catch (IOException e) {
            return null;
        }
    }

    public void setRealtimeRefreshEnabled(boolean enabled) {
        sPreferences.put(REALTIME_REFRESH_ENABLED, enabled);
        sPreferences.commit();
    }

    public boolean isRealtimeRefreshEnabled() {
        return sPreferences.getBoolean(REALTIME_REFRESH_ENABLED, false);
    }

    public void setRealtimeRefreshRate(int rate) {
        sPreferences.put(REALTIME_REFRESH_RATE, rate);
        sPreferences.commit();
    }

    public int getRealtimeRefreshRate(int defVal) {
        return sPreferences.getInt(REALTIME_REFRESH_RATE, defVal);
    }

    public void setTodayGraphSupportedEquipments(String supportedEquipments) {
        sPreferences.put(TODAY_GRAPH_SUPPORTED_EQUIPMENTS, supportedEquipments);
        sPreferences.commit();
    }

    public boolean isTodayGraphSupported(BusinessUnitEnum unitEnum) {
        String supported = sPreferences.getString(TODAY_GRAPH_SUPPORTED_EQUIPMENTS, "");
        return unitEnum != null && unitEnum.getName() != null && supported.toLowerCase().contains(
                unitEnum.getName().toLowerCase());
    }

    public void setCorrectiveActionsFeatureFlag(VersionsData.FeatureFlag correctiveActionsFlag) {
        sPreferences.put(CORRECTIVE_ACTIONS_ENABLED, correctiveActionsFlag.name());
        sPreferences.commit();
    }

    public boolean getCorrectiveActionsEnabled() {

        VersionsData.FeatureFlag featureFlag = VersionsData.FeatureFlag.valueOf(
                sPreferences.getString(CORRECTIVE_ACTIONS_ENABLED, VersionsData.FeatureFlag.OFF.name()));

        if (featureFlag == VersionsData.FeatureFlag.ENGLISH_ONLY && isDefaultLanguageSet(context)) {
            return true;
        }

        return featureFlag == VersionsData.FeatureFlag.ON;
    }

    private boolean isDefaultLanguageSet(Context context) {
        return getLanguage().equals(context.getString(R.string.default_language));
    }

    public void setThresholds(BusinessUnitEnum businessUnitEnum, int success, int average, int fail) {
        if (businessUnitEnum == null) {
            return;
        }
        sPreferences.put(SUCCESS_THRESHOLD_KEY + businessUnitEnum.getName(), success);
        sPreferences.put(AVERAGE_THRESHOLD_KEY + businessUnitEnum.getName(), average);
        sPreferences.put(FAIL_THRESHOLD_KEY + businessUnitEnum.getName(), fail);
        sPreferences.commit();
    }

    public BusinessUnitThresholds getThresholds(BusinessUnitEnum businessUnitEnum) {

        if (thresholds != null && thresholds.getBusinessUnitEnum() == businessUnitEnum) {
            return thresholds;
        }

        BusinessUnitThresholds businessUnitThresholds = new BusinessUnitThresholds();

        if (businessUnitEnum != null) {
            businessUnitThresholds.setBusinessUnitEnum(businessUnitEnum);
            businessUnitThresholds.setSuccessPercent(sPreferences.getInt(SUCCESS_THRESHOLD_KEY + businessUnitEnum.getName(),
                    SUCCESS_THRESHOLD_DEF));
            businessUnitThresholds.setAveragePercent(sPreferences.getInt(AVERAGE_THRESHOLD_KEY + businessUnitEnum.getName(),
                    AVERAGE_THRESHOLD_DEF));
            businessUnitThresholds.setFailPercent(sPreferences.getInt(FAIL_THRESHOLD_KEY + businessUnitEnum.getName(),
                    FAIL_THRESHOLD_DEF));
        }
        this.thresholds = businessUnitThresholds;
        return businessUnitThresholds;
    }

    public void setSelectedSiteID(BusinessUnitEnum businessUnitEnum, String siteID) {
        if (businessUnitEnum == null || siteID == null) {
            return;
        }

        OrganizationViewModel organizationViewModel = getSelectedOrganization();
        String key = organizationViewModel == null ? "" : organizationViewModel.getOrganizationId();
        key = key + businessUnitEnum.getName();

        sPreferences.put(key, siteID);
        sPreferences.commit();
    }

    public String getSelectedSiteID(BusinessUnitEnum businessUnitEnum) {
        if (businessUnitEnum == null) {
            return "";
        }

        OrganizationViewModel organizationViewModel = getSelectedOrganization();
        String key = organizationViewModel == null ? "" : organizationViewModel.getOrganizationId();
        key = key + businessUnitEnum.getName();

        return sPreferences.getString(key, "");
    }

    public void setLowImpressionPrintVolume(int value) {
        sPreferences.put(LOW_PRINT_VOLUME_KEY, value);
        sPreferences.commit();
    }

    public int getLowImpressionPrintVolume() {
        return sPreferences.getInt(LOW_PRINT_VOLUME_KEY, LOW_IMPRESSION_DEFAULT_VALUE);
    }

    public void saveUserImage(String imageUrl) {
        sPreferences.put(PROFILE_IMAGE, imageUrl);
        sPreferences.commit();
    }

    public String getProfileImageUrl() {
        return sPreferences.getString(PROFILE_IMAGE, "");
    }

    public void setInvitesWithCoinsEnabled(boolean enabled) {
        sPreferences.put(INVITES_WITH_COINS_KEY, enabled);
        sPreferences.commit();
    }

    public boolean isInvitesCoinEnabled() {
        return sPreferences.getBoolean(INVITES_WITH_COINS_KEY, false);
    }

    public void setSelectedInviteUserRole(InviteUserRoleViewModel userRole) {
        if (userRole == null) {
            return;
        }
        sPreferences.put(SELECTED_INVITE_USER_ROLE_KEY_ID, userRole.getId());
        sPreferences.put(SELECTED_INVITE_USER_ROLE_KEY_NAME, userRole.getName());
        sPreferences.put(SELECTED_INVITE_USER_ROLE_KEY_TYPE, userRole.getOrgType());
        sPreferences.commit();
    }

    public InviteUserRoleViewModel getSelectedInviteUserRole() {
        InviteUserRoleViewModel model = new InviteUserRoleViewModel();
        model.setId(sPreferences.getString(SELECTED_INVITE_USER_ROLE_KEY_ID, ""));
        model.setName(sPreferences.getString(SELECTED_INVITE_USER_ROLE_KEY_NAME, ""));
        model.setOrgType(sPreferences.getString(SELECTED_INVITE_USER_ROLE_KEY_TYPE, ""));
        if (TextUtils.isEmpty(model.getId()) || TextUtils.isEmpty(model.getOrgType())) {
            return null;
        }
        return model;
    }

    public void setInviteUserRoleCachingTime(long timeInMills) {
        sPreferences.put(CACHED_INVITE_USER_ROLE_DATA_TIME_KEY, timeInMills);
        sPreferences.commit();
    }

    public long getInviteUserRoleCachingTime() {
        return sPreferences.getLong(CACHED_INVITE_USER_ROLE_DATA_TIME_KEY, 0);
    }

    public void saveInviteUserRoleCache(InviteUserRoleData data) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            String cache = mapper.writeValueAsString(data);
            sPreferences.put(CACHED_INVITE_USER_ROLE_DATA_KEY, cache);
        } catch (JsonProcessingException e) {
            HPLogger.e(TAG, "Failed to save invite user role cache, corrupted string");
        }
    }

    public InviteUserRoleData getInviteUserRoleCache() {
        ObjectMapper mapper = new ObjectMapper();
        try {

            String cachedJson = sPreferences.getString(CACHED_INVITE_USER_ROLE_DATA_KEY, "");
            if (TextUtils.isEmpty(cachedJson)) {
                return null;
            }
            return mapper.readValue(cachedJson, InviteUserRoleData.class);
        } catch (IOException e) {
            HPLogger.e(TAG, "Failed to get invite user role " + e);
        }
        return null;
    }

    public String getUserEmail() {
        return sPreferences.getString(KEY_USER_EMAIL, "");
    }

    public boolean isInvitesFeatureEnabled(Context context) {
        VersionsData.FeatureFlag featureFlag = VersionsData.FeatureFlag.valueOf(
                sPreferences.getString(INVITE_FEATURE_FLAG, VersionsData.FeatureFlag.OFF.name()));

        if (featureFlag == VersionsData.FeatureFlag.ENGLISH_ONLY && isDefaultLanguageSet(context)) {
            return true;
        }

        return featureFlag == VersionsData.FeatureFlag.ON;
    }

    public void setInvitesFeatureFlag(VersionsData.FeatureFlag inviteFeatureFlag) {
        sPreferences.put(INVITE_FEATURE_FLAG, inviteFeatureFlag.name());
        sPreferences.commit();
    }

    public void resetInviteUserRoleCache() {
        sPreferences.put(SELECTED_INVITE_USER_ROLE_KEY_ID, "");
        sPreferences.put(SELECTED_INVITE_USER_ROLE_KEY_NAME, "");
        sPreferences.put(SELECTED_INVITE_USER_ROLE_KEY_TYPE, "");
        sPreferences.put(CACHED_INVITE_USER_ROLE_DATA_KEY, "");
        sPreferences.commit();
    }

    public void setForgotPasswordEnabled(Boolean forgotPasswordEnabled) {
        sPreferences.put(FORGOT_PASS_ENABLED, forgotPasswordEnabled);
        sPreferences.commit();
    }

    public boolean isForgotPasswordEnabled() {
        return sPreferences.getBoolean(FORGOT_PASS_ENABLED, false);
    }

    public void setDidAutoSubscribeToIntraDailyUpdate(boolean isSubscribed) {
        sPreferences.put(DID_AUTO_SUBSCRIBE_TO_INTRA_DAILY, isSubscribed);
        sPreferences.commit();
    }

    public boolean isAutoSubscribeToIntraDailyUpdate() {
        return sPreferences.getBoolean(DID_AUTO_SUBSCRIBE_TO_INTRA_DAILY, false);
    }

    public void setAppVersionForAutoSubscribeToIntraDailyTarget(String version) {
        sPreferences.put(INTRA_DAILY_UPDATE_AUTO_SUBSCRIPTION_VERSION, version);
        sPreferences.commit();
    }

    public String getAppVersionForAutoSubscribeToIntraDailyTarget() {
        return sPreferences.getString(INTRA_DAILY_UPDATE_AUTO_SUBSCRIPTION_VERSION, "");
    }

    public void setInviteAllPopupSuppressed(boolean suppress) {
        sPreferences.put(INVITE_ALL_SUPPRESS_KEY, suppress);
        sPreferences.commit();
    }

    public boolean isInviteAllPopupSuppressed() {
        return sPreferences.getBoolean(INVITE_ALL_SUPPRESS_KEY, false);
    }

    public void setBeatCoinsPerInvite(int beatCoinsPerInvite) {
        sPreferences.put(BEAT_COINS_PER_INVITE_KEY, beatCoinsPerInvite);
        sPreferences.commit();
    }

    public int getBeatCoinsPerInvite() {
        int coins = sPreferences.getInt(BEAT_COINS_PER_INVITE_KEY, BEAT_COINS_PER_INVITE_DEF_VALUE);
        return coins == 0 ? BEAT_COINS_PER_INVITE_DEF_VALUE : coins;
    }

    public void setInviteAllPopupDisplayCounter(int inviteAllPopupDisplayCounter) {
        sPreferences.put(INVITE_POPUP_DISPLAY_COUNTER_KEY, inviteAllPopupDisplayCounter);
        sPreferences.commit();
    }

    public int getInviteAllPopupDisplayCounter() {
        int count = sPreferences.getInt(INVITE_POPUP_DISPLAY_COUNTER_KEY, INVITE_COUNTER_DEF_VALUE);
        count = count == 0 ? INVITE_COUNTER_DEF_VALUE : count;
        return count;
    }

    public void setSendSmsEnabled(boolean sendSmsEnabled) {
        sPreferences.put(SMS_ENABLED_KEY, sendSmsEnabled);
        sPreferences.commit();
    }

    public boolean isSendSmsEnabled() {
        return sPreferences.getBoolean(SMS_ENABLED_KEY, false);
    }

    public void setHistogramChartFeatureFlag(VersionsData.FeatureFlag histogramFeatureFlag) {
        sPreferences.put(HISTOGRAM_GRAPH_ENABLED_KEY, histogramFeatureFlag.name());
        sPreferences.commit();
    }

    public boolean isHistogramGraphEnabled() {
        VersionsData.FeatureFlag featureFlag = VersionsData.FeatureFlag.valueOf(
                sPreferences.getString(HISTOGRAM_GRAPH_ENABLED_KEY, VersionsData.FeatureFlag.OFF.name()));

        if (featureFlag == VersionsData.FeatureFlag.ENGLISH_ONLY && isDefaultLanguageSet(context)) {
            return true;
        }

        return featureFlag == VersionsData.FeatureFlag.ON;
    }

    public void setHistogramDayChartType(String type) {
        sPreferences.put(HISTOGRAM_DAY_CHART_KEY, type);
        sPreferences.commit();
    }

    public HistogramGraphUtils.HistogramChartType getHistogramDayChartType() {
        return HistogramGraphUtils.HistogramChartType.from(
                sPreferences.getString(HISTOGRAM_DAY_CHART_KEY, ""));
    }

    public void setHistogramShifChartType(String type) {
        sPreferences.put(HISTOGRAM_SHIFT_CHART_KEY, type);
        sPreferences.commit();
    }

    public HistogramGraphUtils.HistogramChartType getHistogramShifChartType() {
        return HistogramGraphUtils.HistogramChartType.from(
                sPreferences.getString(HISTOGRAM_SHIFT_CHART_KEY, ""));
    }

    public void setHistogramNumberOfShifts(int numberOfShifts) {
        sPreferences.put(HISTOGRAM_NUMBER_OF_SHIFTS_KEY, numberOfShifts);
        sPreferences.commit();
    }

    public int getHistogramNumberOfShifts() {
        int numberOfShifts = sPreferences.getInt(HISTOGRAM_NUMBER_OF_SHIFTS_KEY, 0);

        return numberOfShifts == 0 ? PrintOSApplication.getAppContext().getResources()
                .getInteger(R.integer.today_panel_shift_histogram_span) : numberOfShifts;
    }

    public void setKpiBreakdownReportFeatureFlag(VersionsData.FeatureFlag featureFlag) {
        sPreferences.put(KPI_BREAKDOWN_REPORT_ENABLED_KEY, featureFlag.name());
        sPreferences.commit();
    }

    public boolean isKpiBreakdownReportEnabled() {
        VersionsData.FeatureFlag featureFlag = VersionsData.FeatureFlag.valueOf(
                sPreferences.getString(KPI_BREAKDOWN_REPORT_ENABLED_KEY, VersionsData.FeatureFlag.ENGLISH_ONLY.name()));

        if (featureFlag == VersionsData.FeatureFlag.ENGLISH_ONLY && isDefaultLanguageSet(context)) {
            return true;
        }

        return featureFlag == VersionsData.FeatureFlag.ON;
    }

    public void setStateDistributionFeatureFlag(VersionsData.FeatureFlag featureFlag) {
        sPreferences.put(STATE_DISTRIBUTION_ENABLED_KEY, featureFlag.name());
        sPreferences.commit();
    }

    public boolean isStateDistributionEnabled() {
        VersionsData.FeatureFlag featureFlag = VersionsData.FeatureFlag.valueOf(
                sPreferences.getString(STATE_DISTRIBUTION_ENABLED_KEY, VersionsData.FeatureFlag.ENGLISH_ONLY.name()));

        if (featureFlag == VersionsData.FeatureFlag.ENGLISH_ONLY && isDefaultLanguageSet(context)) {
            return true;
        }

        return featureFlag == VersionsData.FeatureFlag.ON;
    }

    public void setChinaLoggingFeatureFlag(VersionsData.FeatureFlag featureFlag) {
        sPreferences.put(CHINA_LOGGING_ENABLED_KEY, featureFlag.name());
        sPreferences.commit();
    }

    public boolean isChinaLoggingFeatureFlagEnabled() {
        VersionsData.FeatureFlag featureFlag = VersionsData.FeatureFlag.valueOf(
                sPreferences.getString(CHINA_LOGGING_ENABLED_KEY, VersionsData.FeatureFlag.OFF.name()));

        if (featureFlag == VersionsData.FeatureFlag.ENGLISH_ONLY && isDefaultLanguageSet(context)) {
            return true;
        }

        return featureFlag == VersionsData.FeatureFlag.ON;
    }

    public boolean isSelectServerEnabled() {
        String currentServerUrl = getServerUrl();
        boolean isProduction = currentServerUrl != null && (currentServerUrl.equals(context.getString(R.string.production_server)) || currentServerUrl.equals(context.getString(R.string.china_production_server)));
        return isProduction && isChinaLoggingFeatureFlagEnabled();
    }

    public void setLocation(String isInChina) {
        sPreferences.put(LAST_SAVED_LOCATION, isInChina);
        sPreferences.commit();
    }

    public String getLastSavedLocation() {
        return sPreferences.getString(LAST_SAVED_LOCATION, "");
    }

    public void setFakeLocation(String fakeLocation) {
        sPreferences.put(FAKE_LOCATION, fakeLocation);
        sPreferences.commit();
    }

    public String getFakeLocation() {
        return sPreferences.getString(FAKE_LOCATION, "");
    }


    public void setDemoModeFlag(boolean isDemoMode) {
        sPreferences.put(IS_IN_DEMO_MODE_KEY, isDemoMode);
        sPreferences.commit();
    }

    public boolean isInDemoMode() {
        return sPreferences.getBoolean(IS_IN_DEMO_MODE_KEY, false);
    }

    public void setDemoAccountTimestamp(long timeInMills) {
        sPreferences.put(DEMO_ACCOUNT_TIMESTAMP, timeInMills);
        sPreferences.commit();
    }

    public long getDemoAccountTimestamp() {
        return sPreferences.getLong(DEMO_ACCOUNT_TIMESTAMP, 0);
    }

    public void setHPIDFeatureFlag(VersionsData.FeatureFlag hpidFeatureFlag) {
        sPreferences.put(HPID_FEATURE_FLAG_ENABLED_KEY, hpidFeatureFlag.name());
        sPreferences.commit();
    }

    public boolean isHPIDFeatureEnabled() {

        VersionsData.FeatureFlag featureFlag = VersionsData.FeatureFlag.valueOf(
                sPreferences.getString(HPID_FEATURE_FLAG_ENABLED_KEY,
                        VersionsData.FeatureFlag.OFF.name()));

        if (featureFlag == VersionsData.FeatureFlag.ENGLISH_ONLY && isDefaultLanguageSet(context)) {
            return true;
        }

        return featureFlag == VersionsData.FeatureFlag.ON;
    }

    public void setNsLookupTextFileName(String nsLookupTextFileName) {
        sPreferences.put(NSLOOKUP_FILE_NAME, nsLookupTextFileName);
        sPreferences.commit();
    }

    public String getNsLookupTextFileName() {
        return sPreferences.getString(NSLOOKUP_FILE_NAME, Constants.PRINTOS_NS_LOOKUP_TEXT);
    }

    public void setSwitchServerDialogOpened(boolean opened) {
        sPreferences.put(SWITCH_SERVER_DIALOG_OPENED, opened);
        sPreferences.commit();
    }

    public boolean wasSwitchServerDialogOpened() {
        return sPreferences.getBoolean(SWITCH_SERVER_DIALOG_OPENED, false);
    }

    public void setSessionCount(int count) {
        sPreferences.put(SESSION_NUMBER, count);
        sPreferences.commit();
    }

    public int getSessionCount() {
        return sPreferences.getInt(SESSION_NUMBER);
    }

    public void setLatexOnly(boolean isLatext) {
        sPreferences.put(LATEX_ONLY_KEY, isLatext);
        sPreferences.commit();
    }

    public boolean isLatexOnly() {
        return sPreferences.getBoolean(LATEX_ONLY_KEY, false);
    }

    public AccountType getOrgType() {
        return getUserInfo().getUserOrganization().getAccountType();
    }

    public boolean isHPOrg() {
        return getOrgType() == AccountType.HP;
    }

    public boolean isChannelOrg() {
        return getOrgType() == AccountType.CHANNEL;
    }

    public void setKZLastSelectedBu(BusinessUnitEnum businessUnitEnum) {
        sPreferences.put(KZ_LAST_SELECTED_BU, businessUnitEnum.getName());
        sPreferences.commit();
    }

    public BusinessUnitEnum getKZLastSelectedBu() {
        return BusinessUnitEnum.from(sPreferences.getString(KZ_LAST_SELECTED_BU, ""));
    }

    public long getLastSessionValidationTime() {
        return sPreferences.getLong(LAST_SESSION_VALIDATION_TIME, 0);
    }

    public void setLastSessionValidationTime() {
        sPreferences.put(LAST_SESSION_VALIDATION_TIME, System.currentTimeMillis());
        sPreferences.commit();
    }

    public void onLoginCompleted(UserData.User user, UserData.Context organization) {

        saveCurrentOrganization(organization);
        saveUserInfo(user);
        setLoggedIn(true);

    }

    public void onLogoutCompleted() {

        sPreferences.put(KEY_LOGIN_USER_ID, "");
        sPreferences.put(KEY_LOGIN_USER_DISPLAY_NAME, "");
        sPreferences.put(KEY_LOGIN_USER_FIRST_NAME, "");
        sPreferences.put(KEY_LOGIN_USER_LAST_NAME, "");
        sPreferences.put(USER_ROLES, "");
        sPreferences.put(SELF_PROVISION_ORGANIZATION, "");
        sPreferences.put(KEY_BASE_ORGANIZATION_ID, "");
        sPreferences.put(KEY_USER_EMAIL, "");
        sPreferences.remove(RANKING_LEADERBOARD_POPUP_DISPLAY_TIMING, RANKING_LEADERBOARD_POPUP_DONT_SHOW_AGAIN);
        sPreferences.saveUserType("");
        sPreferences.clearOrganization();

        setDemoModeFlag(false);
        setDemoAccountTimestamp(0);
        clearChatPreferences(false);
        resetInviteUserRoleCache();
        setLoggedIn(false);
        setHasIndigoDivision(false);

        sPreferences.commit();
    }

    public void setChannelSupportKz(boolean channelSupportKz) {
        sPreferences.put(CHANNEL_SUPPORT_KZ, channelSupportKz);
        sPreferences.commit();
    }

    public boolean isChannelSupportKz() {
        return sPreferences.getBoolean(CHANNEL_SUPPORT_KZ, false);
    }

    public boolean isInsightsEnabledForBu(BusinessUnitEnum businessUnit) {
        return isKnowledgeZoneSearchEnabled() && getInsightsConfigurations().getKzSupportedEquipments().contains(businessUnit.getName());
    }

    public void setBeatCoinsClaimed(BeatCoinsPresenter.BeatCoinsMode beatCoinsMode) {
        String key = getPrefKeyForBeatCoin(beatCoinsMode);
        sPreferences.put(key, true);
        sPreferences.commit();
    }

    public boolean isBeatCoinsClaimed(BeatCoinsPresenter.BeatCoinsMode beatCoinsMode) {
        String key = getPrefKeyForBeatCoin(beatCoinsMode);
        return sPreferences.getBoolean(key);
    }

    private String getPrefKeyForBeatCoin(BeatCoinsPresenter.BeatCoinsMode beatCoinsMode) {
        return String.format("%s_%s_%s", getUserInfo().getUserId(),
                getUserInfo().getUserOrganization().getOrganizationId(), beatCoinsMode.name());
    }

    public boolean isRankingLeaderboardEnabled() {

        VersionsData.FeatureFlag featureFlag = VersionsData.FeatureFlag.valueOf(
                sPreferences.getString(RANKING_LEADERBOARD_ENABLED,
                        VersionsData.FeatureFlag.OFF.name()));

        if (featureFlag == VersionsData.FeatureFlag.ENGLISH_ONLY && isDefaultLanguageSet(context)) {
            return true;
        }

        return featureFlag == VersionsData.FeatureFlag.ON;

    }

    public void setRankingLeaderboardFeatureFlag(VersionsData.FeatureFlag rankingLeaderboardFeatureFlag) {
        sPreferences.put(RANKING_LEADERBOARD_ENABLED, rankingLeaderboardFeatureFlag.name());
        sPreferences.commit();
    }

    public void setHasIndigoGBU(boolean hasIndigoGBU) {
        sPreferences.put(HAS_INDIGO_GBU_KEY, hasIndigoGBU);
        sPreferences.commit();
    }


    public void setRankingLeaderboardPopupLastDisplayTiming(long milliSeconds) {
        sPreferences.put(RANKING_LEADERBOARD_POPUP_DISPLAY_TIMING, milliSeconds);
        sPreferences.commit();
    }

    public long getRankingLeaderboardPopupLastDisplayTiming() {
        return sPreferences.getLong(RANKING_LEADERBOARD_POPUP_DISPLAY_TIMING, Long.MAX_VALUE);

    }

    public void setRankingLeaderboardDontShowAgain(boolean dontShow) {
        sPreferences.put(RANKING_LEADERBOARD_POPUP_DONT_SHOW_AGAIN, dontShow);
        sPreferences.commit();
    }

    public boolean isRankingLeaderboardPopupEnabled() {
        return !sPreferences.getBoolean(RANKING_LEADERBOARD_POPUP_DONT_SHOW_AGAIN, false);
    }

    public boolean hasIndigoGBU() {
        return sPreferences.getBoolean(HAS_INDIGO_GBU_KEY, false);
    }
}