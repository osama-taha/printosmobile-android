package com.hp.printosmobile.presentation.modules.invites;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobilelib.core.communications.remote.APIException;
import com.hp.printosmobilelib.core.logging.HPLogger;
import com.hp.printosmobilelib.ui.common.HPFragment;
import com.hp.printosmobilelib.ui.utils.DividerItemDecoration;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * created by anwar asbah 11/6/2017
 **/
public class UserRoleFragment extends HPFragment implements InvitesView {

    private static final String TAG = UserRoleFragment.class.getSimpleName();
    public static final String DEFAULT_ROLE_TYPE = "Press Operator";

    @Bind(R.id.loading_view)
    View loadingView;
    @Bind(R.id.roles_list)
    RecyclerView rolesList;
    @Bind(R.id.done_button)
    View doneButton;

    RolesAdapter rolesAdapter;
    InviteUserRoleSettingFragmentCallback callback;
    InvitesPresenter presenter;

    public static UserRoleFragment getNewInstance(InviteUserRoleSettingFragmentCallback callback) {
        UserRoleFragment notificationsFragment = new UserRoleFragment();
        notificationsFragment.callback = callback;
        return notificationsFragment;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        doneButton.setVisibility(View.GONE);
        initPresenter();
    }

    private void initPresenter() {
        presenter = new InvitesPresenter();
        presenter.attachView(this);
        presenter.getInviteUserRoles();
    }

    private void initView(List<InviteUserRoleViewModel> models) {
        if (models == null) {
            return;
        }

        InviteUserRoleViewModel selectedViewModel = PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).getSelectedInviteUserRole();
        int selectedIndex = -1;
        if (selectedViewModel != null) {
            for (int i = 0; i < models.size(); i++) {
                InviteUserRoleViewModel model = models.get(i);
                if (model.getId().equals(selectedViewModel.getId())) {
                    selectedIndex = i;
                    break;
                }
            }
        }

        if (selectedIndex == -1) {
            selectedIndex = 0;
        }

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        rolesList.setLayoutManager(layoutManager);

        rolesAdapter = new RolesAdapter(models);
        rolesAdapter.selectedIndex = selectedIndex;
        rolesList.setAdapter(rolesAdapter);
        rolesList.addItemDecoration(new DividerItemDecoration(getActivity(), 1, models.size(), false));

        doneButton.setVisibility(View.VISIBLE);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_invites_user_role;
    }

    @Override
    public boolean isFragmentNameToBeDisplayed() {
        return true;
    }

    @Override
    public boolean isFilteringAvailable() {
        return false;
    }

    @Override
    public int getToolbarDisplayName() {
        return R.string.menu_user_roles;
    }

    @OnClick(R.id.done_button)
    public void onDoneClicked() {
        HPLogger.d(TAG, "done clicked");

        if (callback != null && rolesAdapter != null) {
            InviteUserRoleViewModel inviteUserRoleViewModel = rolesAdapter.getSelectedRole();
            if (inviteUserRoleViewModel != null) {
                callback.onRoleChanged(inviteUserRoleViewModel);
            }
        }
    }

    @Override
    public void onUserRolesRetrieved(List<InviteUserRoleViewModel> models) {
        loadingView.setVisibility(View.GONE);
        initView(models);
    }

    @Override
    public void errorRetrievingUserRoles() {
        loadingView.setVisibility(View.GONE);
    }

    @Override
    public void onError(APIException exception, String tag) {
        loadingView.setVisibility(View.GONE);
    }

    @Override
    public int getCustomToolbarMenu() {
        super.getCustomToolbarMenu();
        return R.layout.empty_custom_menu;
    }

    public class RolesAdapter extends RecyclerView.Adapter<RolesAdapter.RoleViewHolder> {

        private List<InviteUserRoleViewModel> roleViewModels;
        private int selectedIndex;

        public RolesAdapter(List<InviteUserRoleViewModel> roleViewModels) {
            this.roleViewModels = roleViewModels;
            this.selectedIndex = -1;
        }

        @Override
        public RolesAdapter.RoleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            ViewGroup view = (ViewGroup) inflater.inflate(R.layout.fragment_invites_user_role_item, parent, false);
            return new RolesAdapter.RoleViewHolder(view);
        }

        @Override
        public void onBindViewHolder(RolesAdapter.RoleViewHolder holder, int position) {
            holder.roleName.setText(roleViewModels.get(position).getName());
            holder.index = position;

            holder.viewItem.setSelected(selectedIndex == position);
            holder.checkImage.setVisibility(selectedIndex == position ? View.VISIBLE : View.GONE);
        }

        @Override
        public int getItemCount() {
            return roleViewModels == null ? 0 : roleViewModels.size();
        }

        public InviteUserRoleViewModel getSelectedRole() {
            if (roleViewModels == null || selectedIndex < 0 || selectedIndex >= roleViewModels.size()) {
                return null;
            }

            return roleViewModels.get(selectedIndex);
        }

        public class RoleViewHolder extends RecyclerView.ViewHolder {

            @Bind(R.id.role_name_text_view)
            TextView roleName;
            @Bind(R.id.checked_image)
            View checkImage;

            View viewItem;
            int index;

            public RoleViewHolder(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
                viewItem = itemView;

                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        selectedIndex = index;
                        notifyDataSetChanged();
                    }
                });
            }
        }
    }

    public interface InviteUserRoleSettingFragmentCallback {
        void onRoleChanged(InviteUserRoleViewModel inviteUserRoleViewModel);
    }
}
