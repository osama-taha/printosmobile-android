package com.hp.printosmobile.presentation.modules.insights.kz.kzfooter;

import com.hp.printosmobile.data.remote.models.kz.KZItem;

/**
 * Created by Minerva on 5/31/2018.
 */
public interface KZItemDetailsCallback {

    void onFavoritePressed(KZItem kzItem);

    void onLikePressed(KZItem kzItem);

    void onDislikePressed(KZItem kzItem);

}