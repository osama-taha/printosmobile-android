package com.hp.printosmobile.presentation.modules.insights.kz;

import com.hp.printosmobile.data.remote.models.kz.KZItem;

import java.util.List;


/**
 * Created by Osama Taha on 3/19/18.
 */
interface InsightsSearchView {

    void showEmptyView();

    void hideEmptyView();

    void showLoadingView();

    void hideLoadingView();

    void addAutoCompleteResults(List<KZAutoCompleteResult> kzAutoCompleteResults);

    void addResults(List<KZItem> knowledgeZoneItems);

    void clearResults();

    void hideResultsView();

    void showResultsView();

    void setEmptyText(String emptyText);

    void showErrorView();

    void hideErrorView();

    void showLoadingFooterView();

    void addFooterView();

    void removeFooterView();

    void showErrorFooterView();

    void openKnowledgeZoneItem(KZItem knowledgeZoneItem);

    void showNoResultsFooterView();

    void onAutoCompleteResultSelected(KZAutoCompleteResult autoCompleteResult);
}
