package com.hp.printosmobile.data.remote.models;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class PermissionsData {

    @JsonProperty("isChannel")
    private Boolean isChannel;
    @JsonProperty("permissions")
    private List<PermissionData> permissionsData;
    @JsonProperty("version")
    private String version;
    @JsonProperty("clicksEnabled")
    private Boolean clicksEnabled;
    @JsonProperty("colorBeatEnabled")
    private Boolean colorBeatEnabled;
    @JsonProperty("oeeEnabled")
    private Boolean oeeEnabled;
    @JsonProperty("oeePotential")
    private Boolean oeePotential;
    @JsonProperty("operatorRankingEnabled")
    private Boolean operatorRankingEnabled;
    @JsonProperty("rankingLeaderboardStatus")
    private RankingLeaderboardStatus rankingLeaderboardStatus;
    @JsonProperty("lfpFleetView")
    private Boolean lfpFleetView;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<>();

    @JsonProperty("isChannel")
    public Boolean getIsChannel() {
        return isChannel;
    }

    @JsonProperty("isChannel")
    public void setIsChannel(Boolean isChannel) {
        this.isChannel = isChannel;
    }

    @JsonProperty("permissions")
    public List<PermissionData> getPermissionsData() {
        return permissionsData;
    }

    @JsonProperty("permissions")
    public void setPermissionsData(List<PermissionData> permissionsData) {
        this.permissionsData = permissionsData;
    }

    @JsonProperty("version")
    public String getVersion() {
        return version;
    }

    @JsonProperty("version")
    public void setVersion(String version) {
        this.version = version;
    }

    @JsonProperty("clicksEnabled")
    public Boolean getClicksEnabled() {
        return clicksEnabled;
    }

    @JsonProperty("clicksEnabled")
    public void setClicksEnabled(Boolean clicksEnabled) {
        this.clicksEnabled = clicksEnabled;
    }

    @JsonProperty("colorBeatEnabled")
    public Boolean getColorBeatEnabled() {
        return colorBeatEnabled;
    }

    @JsonProperty("colorBeatEnabled")
    public void setColorBeatEnabled(Boolean colorBeatEnabled) {
        this.colorBeatEnabled = colorBeatEnabled;
    }

    @JsonProperty("oeeEnabled")
    public Boolean getOeeEnabled() {
        return oeeEnabled;
    }

    @JsonProperty("oeeEnabled")
    public void setOeeEnabled(Boolean oeeEnabled) {
        this.oeeEnabled = oeeEnabled;
    }

    @JsonProperty("oeePotential")
    public Boolean getOeePotential() {
        return oeePotential;
    }

    @JsonProperty("oeePotential")
    public void setOeePotential(Boolean oeePotential) {
        this.oeePotential = oeePotential;
    }

    @JsonProperty("operatorRankingEnabled")
    public Boolean getOperatorRankingEnabled() {
        return operatorRankingEnabled;
    }

    @JsonProperty("operatorRankingEnabled")
    public void setOperatorRankingEnabled(Boolean operatorRankingEnabled) {
        this.operatorRankingEnabled = operatorRankingEnabled;
    }

    @JsonProperty("rankingLeaderboardStatus")
    public RankingLeaderboardStatus getRankingLeaderboardStatus() {
        return rankingLeaderboardStatus;
    }

    @JsonProperty("rankingLeaderboardStatus")
    public void setRankingLeaderboardStatus(RankingLeaderboardStatus rankingLeaderboardStatus) {
        this.rankingLeaderboardStatus = rankingLeaderboardStatus;
    }

    @JsonProperty("lfpFleetView")
    public Boolean getLfpFleetView() {
        return lfpFleetView;
    }

    @JsonProperty("lfpFleetView")
    public void setLfpFleetView(Boolean lfpFleetView) {
        this.lfpFleetView = lfpFleetView;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class PermissionData {

        @JsonProperty("permissionId")
        private String permissionId;
        @JsonProperty("name")
        private String name;
        @JsonProperty("description")
        private String description;
        @JsonProperty("serviceId")
        private String serviceId;
        @JsonIgnore
        private Map<String, Object> additionalProperties = new HashMap<>();

        @JsonProperty("permissionId")
        public String getPermissionId() {
            return permissionId;
        }

        @JsonProperty("permissionId")
        public void setPermissionId(String permissionId) {
            this.permissionId = permissionId;
        }

        @JsonProperty("name")
        public String getName() {
            return name;
        }

        @JsonProperty("name")
        public void setName(String name) {
            this.name = name;
        }

        @JsonProperty("description")
        public String getDescription() {
            return description;
        }

        @JsonProperty("description")
        public void setDescription(String description) {
            this.description = description;
        }

        @JsonProperty("serviceId")
        public String getServiceId() {
            return serviceId;
        }

        @JsonProperty("serviceId")
        public void setServiceId(String serviceId) {
            this.serviceId = serviceId;
        }

        @JsonAnyGetter
        public Map<String, Object> getAdditionalProperties() {
            return this.additionalProperties;
        }

        @JsonAnySetter
        public void setAdditionalProperty(String name, Object value) {
            this.additionalProperties.put(name, value);
        }

    }

}