package com.hp.printosmobile.presentation.modules.npspopup;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobilelib.core.communications.remote.SessionManager;

/**
 * Created by Anwar Asbah on 7/16/2017.
 */

public class NPSBroadcastReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        SessionManager sessionManager = SessionManager.getInstance(PrintOSApplication.getAppContext());
        //only if user is logged in
        if (sessionManager.hasCookie()) {
            NPSNotificationManager.displayNotification(context);
        }
    }
}
