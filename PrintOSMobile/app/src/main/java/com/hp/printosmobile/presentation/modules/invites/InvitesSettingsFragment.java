package com.hp.printosmobile.presentation.modules.invites;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.TextView;

import com.hp.printosmobile.Analytics;
import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.presentation.modules.settings.LanguageFragment;
import com.hp.printosmobilelib.core.logging.HPLogger;
import com.hp.printosmobilelib.ui.common.HPFragment;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * create by anwar asbah 11/5/2017
 */
public class InvitesSettingsFragment extends HPFragment implements LanguageFragment.LanguageFragmentCallback, UserRoleFragment.InviteUserRoleSettingFragmentCallback {

    private static final String TAG = InvitesSettingsFragment.class.getSimpleName();
    private HPFragment subMenuFragment;

    @Bind(R.id.def_lang)
    TextView defLangTextView;
    @Bind(R.id.def_role)
    TextView defRoleTextView;

    InviteSettingsFragmentCallback callback;

    public static InvitesSettingsFragment newInstance(InviteSettingsFragmentCallback callback) {
        InvitesSettingsFragment fragment = new InvitesSettingsFragment();
        fragment.callback = callback;
        return fragment;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
    }

    private void init() {
        PrintOSPreferences preferences = PrintOSPreferences.getInstance(PrintOSApplication.getAppContext());
        String defLang = preferences.getInvitesLanguage();
        defLangTextView.setText(defLang);

        String defRole = "";
        InviteUserRoleViewModel model = preferences.getSelectedInviteUserRole();
        if (model != null) {
            defRole = model.getName();
        }
        defRoleTextView.setText(defRole);
    }

    @OnClick(R.id.languages_button)
    public void onLanguagesClicked() {
        Analytics.sendEvent(Analytics.VIEW_SCREEN_EVENT, Analytics.INVITES_LANG_SCREEN_LABEL);

        openSubMenu(LanguageFragment.getNewInstance(this, LanguageFragment.LanguageSpecs.INVITES));
    }

    @OnClick(R.id.roles_button)
    public void onRolesClicked() {
        Analytics.sendEvent(Analytics.VIEW_SCREEN_EVENT, Analytics.INVITES_ROLL_SCREEN_LABEL);

        openSubMenu(UserRoleFragment.getNewInstance(this));
    }

    private void openSubMenu(HPFragment fragment) {
        HPLogger.d(TAG, "open sub menu " + (fragment == null ? "null"
                : fragment.getClass().getName()));
        FragmentTransaction trans = getActivity().getSupportFragmentManager()
                .beginTransaction();

        subMenuFragment = fragment;

        trans.replace(R.id.sub_menu_container, subMenuFragment);
        trans.commit();

        if (callback != null) {
            callback.onInviteSubFragmentOpened(subMenuFragment);
        }
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_invites_settings;
    }

    @Override
    public String getToolbarDisplayNameExtra() {
        return "";
    }

    @Override
    public boolean isFragmentNameToBeDisplayed() {
        return true;
    }

    @Override
    public boolean isFilteringAvailable() {
        return false;
    }

    @Override
    public int getToolbarDisplayName() {
        return R.string.invites_settings_fragment_name;
    }

    @Override
    public boolean onBackPressed() {
        if (subMenuFragment != null) {
            if (!subMenuFragment.onBackPressed()) {
                FragmentTransaction trans = getFragmentManager()
                        .beginTransaction();
                trans.remove(subMenuFragment);
                trans.commit();
                subMenuFragment = null;
                if (callback != null) {
                    callback.onInviteSubFragmentOpened(this);
                }
            }
            return true;
        }
        return false;
    }

    @Override
    public boolean isIntercomAccessible() {
        return false;
    }

    @Override
    public void onLanguageChanged(String selectedLanguageKey) {
        PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).setInvitesLanguage(selectedLanguageKey);
        onBackPressed();

        defLangTextView.setText(selectedLanguageKey);
    }

    @Override
    public void onRoleChanged(InviteUserRoleViewModel inviteUserRoleViewModel) {
        PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).setSelectedInviteUserRole(
                inviteUserRoleViewModel);
        onBackPressed();

        defRoleTextView.setText(inviteUserRoleViewModel == null || inviteUserRoleViewModel.getName() == null ? "" :
                inviteUserRoleViewModel.getName());
    }

    @Override
    public int getCustomToolbarMenu() {
        super.getCustomToolbarMenu();
        return R.layout.empty_custom_menu;
    }

    public interface InviteSettingsFragmentCallback {
        void onInviteSubFragmentOpened(HPFragment subFragment);
    }
}
