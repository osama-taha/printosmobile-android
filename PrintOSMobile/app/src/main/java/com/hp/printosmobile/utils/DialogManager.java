package com.hp.printosmobile.utils;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;

import com.hp.printosmobile.Analytics;
import com.hp.printosmobile.Constants;
import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.data.remote.models.AccountType;
import com.hp.printosmobile.data.remote.models.PermissionsData;
import com.hp.printosmobile.data.remote.models.RankingLeaderboardStatus;
import com.hp.printosmobile.data.remote.models.UserInfoData;
import com.hp.printosmobile.presentation.BaseActivity;
import com.hp.printosmobile.presentation.PermissionsManager;
import com.hp.printosmobile.presentation.modules.HiddenSettingsDialog;
import com.hp.printosmobile.presentation.modules.chinapopup.SwitchProductionServerDialog;
import com.hp.printosmobile.presentation.modules.demopopup.DemoDialog;
import com.hp.printosmobile.presentation.modules.eula.EulaPopup;
import com.hp.printosmobile.presentation.modules.home.HomeDataManager;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitEnum;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitViewModel;
import com.hp.printosmobile.presentation.modules.main.MainActivity;
import com.hp.printosmobile.presentation.modules.npspopup.NPSPopup;
import com.hp.printosmobile.presentation.modules.rankingpanel.leaderboard.RankingLeaderboardPopup;
import com.hp.printosmobile.presentation.modules.settings.wizard.UserProfileManager;
import com.hp.printosmobile.presentation.modules.week.RankingViewModel;
import com.hp.printosmobilelib.core.logging.HPLogger;
import com.hp.printosmobilelib.core.utils.NSLookupUtils;

import retrofit2.Response;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func2;
import rx.schedulers.Schedulers;

/**
 * Created by Anwar Asbah on 7/16/2017.
 */

public class DialogManager {

    private static final String TAG = DialogManager.class.getName();

    public static final int NPS_MIN_DISPLAY_TIME_INTERVAL_DEF_VALUE = 7;
    public static final long DAY_TO_MILLIS = 24 * 60 * 60 * 1000L;
    private static final long RANKING_LEADERBOARD_POPUP_DAYS = 7L;
    private static boolean isCheckingRankingPopup;

    private DialogManager() {
    }

    public static boolean displayDialogs(BusinessUnitViewModel businessUnitViewModel, final BaseActivity activity, final boolean skipNps) {

        if (PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).isInDemoMode()) {
            displayDemoPopup(activity);
        } else {

            if (PrintOSPreferences.getInstance().isRankingLeaderboardEnabled() && businessUnitViewModel != null
                    && shouldDisplayForRankingLeaderboardPopup(activity, businessUnitViewModel)) {

                HPLogger.d(TAG, "checking leaderboard popup.");

            } else if (PrintOSPreferences.getInstance(activity).isSelectServerEnabled()) {

                if (shouldDisplaySwitchServerDialogFromHiddenSettings(activity)) {
                    return true;
                } else {
                    new NSLookupTask(activity, new NSLookupTask.AsyncResponse() {
                        @Override
                        public void processFinish(String output) {
                            if (!shouldDisplaySwitchServerDialog(activity, output)) {
                                if (!displayOtherPopups(activity, skipNps)) {
                                    dismissPopups(activity);
                                }
                            }
                        }
                    }).execute();
                }

            } else {
                if (!displayOtherPopups(activity, skipNps)) {
                    dismissPopups(activity);
                    return false;
                }

            }

        }

        return true;
    }

    private static boolean shouldDisplayForRankingLeaderboardPopup(final Activity activity, BusinessUnitViewModel businessUnitViewModel) {

        if (isCheckingRankingPopup){
            return false;
        }

        isCheckingRankingPopup = true;

        long currentTime = System.currentTimeMillis();

        long popupLastDisplayTime = PrintOSPreferences.getInstance().getRankingLeaderboardPopupLastDisplayTiming();

        long timeDiff = popupLastDisplayTime == Long.MAX_VALUE ? popupLastDisplayTime : (currentTime - popupLastDisplayTime);

        if (timeDiff < (RANKING_LEADERBOARD_POPUP_DAYS * DAY_TO_MILLIS) ||
                businessUnitViewModel.getBusinessUnit() != BusinessUnitEnum.INDIGO_PRESS ||
                activity == null || activity.isFinishing() || !PrintOSPreferences.getInstance().isRankingLeaderboardPopupEnabled()) {
            return false;
        }

        Observable.combineLatest(PermissionsManager.getInstance().getPermissionsData(false),
                HomeDataManager.getSiteRankingData(activity, businessUnitViewModel),
                new Func2<Response<PermissionsData>, RankingViewModel, RankingViewModel>() {
                    @Override
                    public RankingViewModel call(Response<PermissionsData> permissionsDataResponse, RankingViewModel rankingViewModel) {

                        RankingLeaderboardStatus rankingLeaderboardStatus = PermissionsManager.getInstance().getRankingLeaderboardStatus();
                        boolean hasUpdatePermission = PermissionsManager.getInstance().hasPermission(PermissionsManager.Permission.UPDATE);

                        return (rankingLeaderboardStatus == RankingLeaderboardStatus.DISABLED) && hasUpdatePermission ? rankingViewModel : null;
                    }
                }).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<RankingViewModel>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        PrintOSPreferences.getInstance().setRankingLeaderboardPopupLastDisplayTiming(System.currentTimeMillis());
                        dismissPopups(activity);
                    }

                    @Override
                    public void onNext(RankingViewModel rankingViewModel) {

                        PrintOSPreferences.getInstance().setRankingLeaderboardPopupLastDisplayTiming(System.currentTimeMillis());

                        if (rankingViewModel != null) {

                            if (rankingViewModel.isTop100World() || rankingViewModel.isTop10World() ||
                                    rankingViewModel.isTop10Region() || rankingViewModel.isTop10SubRegion()) {
                                showRankingPopup(activity, rankingViewModel);
                            } else {
                                scrollToRankingTab(activity);
                                dismissPopups(activity);
                            }
                        } else {
                            dismissPopups(activity);
                        }
                    }
                });

        return true;

    }

    private static void scrollToRankingTab(Activity activity) {
        if (activity instanceof DialogManagerCallbacks) {
            DialogManagerCallbacks callbacks = (DialogManagerCallbacks) activity;
            callbacks.onRankingPopupAgreeSelected();
        }
    }

    private static void showRankingPopup(final Activity activity, RankingViewModel rankingViewModel) {

        isCheckingRankingPopup = false;

        if (activity.isFinishing()) {
            return;
        }

        RankingLeaderboardPopup.getInstance(new RankingLeaderboardPopup.RankingPopupCallbacks() {
            @Override
            public void onAgreeButtonClicked() {
                scrollToRankingTab(activity);
            }

            @Override
            public void onDialogDismissed() {
            }
        }, rankingViewModel).show(activity.getFragmentManager(), RankingLeaderboardPopup.TAG);
    }

    private static void dismissPopups(Activity activity) {

        isCheckingRankingPopup = false;

        if (activity instanceof MainActivity) {
            MainActivity mainActivity = (MainActivity) activity;
            mainActivity.onDialogDismissed();
        }
    }

    private static boolean displayOtherPopups(BaseActivity activity, boolean skipNps) {
        PrintOSPreferences preferences = PrintOSPreferences.getInstance(activity);
        if (!preferences.isWizardPopupShown()) {
            return !shouldShowWizardPopup(activity);
        }

        if (activity.getIntent() != null && activity.getIntent().hasExtra(Constants.IntentExtras.MAIN_ACTIVITY_BEAT_COIN_EXTRA)) {
            return false;
        }

        if (skipNps || !shouldDisplayNPSDialog(activity)) {
            if (!RateUtils.displayRatingDialog(activity)) {
                if (!shouldShowWizardPopup(activity)) {
                    return shouldShowSendInvitesDialog(activity);
                }
            }
        }
        return false;
    }

    private static void displayDemoPopup(final BaseActivity activity) {

        if (activity.isFinishing()) {
            return;
        }

        DemoDialog demoDialog = DemoDialog.getInstance(activity instanceof DemoDialog.DemoDialogCallback ?
                (DemoDialog.DemoDialogCallback) activity : null);

        activity.getSupportFragmentManager()
                .beginTransaction()
                .add(demoDialog, DemoDialog.TAG)
                .commitAllowingStateLoss();

    }

    public static void displayHiddenSettingsPopup(final BaseActivity activity) {

        if (activity.isFinishing()) {
            return;
        }

        HiddenSettingsDialog.getInstance(activity instanceof HiddenSettingsDialog.HiddenSettingsDialogCallback ?
                (HiddenSettingsDialog.HiddenSettingsDialogCallback) activity : null)
                .show(activity.getFragmentManager(), HiddenSettingsDialog.TAG);

    }

    private static boolean isUsingWrongServerAndNumberOfSessionsPassed(final BaseActivity activity, String location) {

        if (activity.isFinishing()) {
            return false;
        }

        String currentServerUrl = PrintOSPreferences.getInstance(activity).getServerUrl();
        //check if user is not using the right server that matches to his location (location is china and using .com or location is default and using .cn)
        boolean isUsingWrongServer = (location.equalsIgnoreCase(Constants.CHINA) && currentServerUrl.equals(activity.getString(R.string.production_server))) || (location.equalsIgnoreCase(Constants.DEFAULT_LOCATION) && currentServerUrl.equals(activity.getString(R.string.china_production_server)));
        int sessionCount = PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).getSessionCount();
        boolean wasSwitchServerDialogOpened = PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).wasSwitchServerDialogOpened();

        //max session count has passed or first launch
        return isUsingWrongServer && (sessionCount >= Constants.SWITCH_SERVER_POPUP_MAX_SESSION_COUNT || !wasSwitchServerDialogOpened);
    }

    private static boolean shouldShowWizardPopup(final BaseActivity activity) {

        if (activity.isFinishing()) {
            return false;
        }


        return UserProfileManager.getInstance().showAddUserImagePopup(activity, new UserProfileManager.UserInfoPopupsCallBacks() {
            @Override
            public void onFinishCheckingUserInfoPopups() {
                shouldShowSendInvitesDialog(activity);
            }
        });


    }

    private static boolean shouldShowSendInvitesDialog(BaseActivity activity) {

        /*
        if (activity.isFinishing()) {
            return false;
        }

        VersionsData.Configuration.SendInvitesData sendInvitesData = PrintOSPreferences
                .getInstance(activity).getSendInvitesData();

        long currentTime = System.currentTimeMillis();

        if (PrintOSPreferences.getInstance(activity).getSendInvitesPopupLastDisplayTiming() == 0) {
            PrintOSPreferences.getInstance(activity).setSendInvitesPopupLastDisplayTiming(currentTime);
        }

        if (sendInvitesData == null || !sendInvitesData.isEnabled() ||
                sendInvitesData.getLink() == null
                || sendInvitesData.getMinDisplayTimeInterval() == null) {
            HPLogger.d(TAG, "send invites data is not available.");
            return false;
        }

        long timeDiff = currentTime - PrintOSPreferences.getInstance(activity).getSendInvitesPopupLastDisplayTiming();

        if (timeDiff >= sendInvitesData.getMinDisplayTimeInterval() * DAY_TO_MILLIS) {

            HPLogger.d(TAG, "show send invites popup.");

            Analytics.sendEvent(Analytics.SHOW_SEND_INVITES_ACTION);

            PrintOSPreferences.getInstance(activity).setSendInvitesPopupLastDisplayTiming(currentTime);

            SendInvitesPopup sendInvitesPopup = SendInvitesPopup.getInstance(activity instanceof SendInvitesPopup.SendInvitesPopupCallback ?
                    (SendInvitesPopup.SendInvitesPopupCallback) activity : null);

            activity.getSupportFragmentManager().beginTransaction()
                    .add(sendInvitesPopup, SendInvitesPopup.TAG)
                    .commitAllowingStateLoss();


            return true;

        }
        */

        return false;

    }

    private static boolean shouldDisplayNPSDialog(BaseActivity activity) {

        if (activity.isFinishing()) {
            return false;
        }

        long currentTime = System.currentTimeMillis();

        if (PrintOSPreferences.getInstance(activity).getNPSLastDisplayTiming() == 0) {
            PrintOSPreferences.getInstance(activity).setNPSLastDisplayTiming(currentTime);
        }

        AccountType accountType = PrintOSPreferences.getInstance(activity).getAccountType();
        boolean isRegularUser = accountType == AccountType.PSP;

        if (!PrintOSPreferences.getInstance(activity).getHasIndigoDivision() ||
                !PrintOSPreferences.getInstance(activity).isNPSFeatureEnabled()
                || !isRegularUser) {
            return false;
        }

        Bundle bundle = activity.getIntent().getExtras();
        if (bundle != null && bundle.containsKey(Constants.IntentExtras.NPS_EXTRA)) {
            HPLogger.v(TAG, "has NPS extra");
            activity.getIntent().removeExtra(Constants.IntentExtras.NPS_EXTRA);
            if (activity instanceof MainActivity) {
                ((MainActivity) activity).requestNPSData(false, true);
            }
            return true;
        } else if (!PrintOSPreferences.getInstance(activity).isNPSNotificationsEnabled()) {
            HPLogger.v(TAG, "local notifications off");
            long timeDiff = currentTime - PrintOSPreferences.getInstance(activity).getNPSLastDisplayTiming();
            HPLogger.v(TAG, "timeDiff: " + timeDiff);
            if (timeDiff >= getNPSMinDisplayTimeInMillSeconds(activity)) {
                if (activity instanceof MainActivity) {
                    ((MainActivity) activity).requestNPSData(false, true);
                }
                return true;
            }
        }
        return false;
    }

    public static long getNPSMinDisplayTimeInMillSeconds(Context context) {
        return PrintOSPreferences.getInstance(context).getNPSMinDisplayTimeInterval(
                NPS_MIN_DISPLAY_TIME_INTERVAL_DEF_VALUE) * DAY_TO_MILLIS;
    }

    public static void displayNPSPopup(BaseActivity activity, UserInfoData userInfoData) {

        if (activity.isFinishing()) {
            return;
        }

        HPLogger.d(TAG, "show NPS popup.");

        Analytics.sendEvent(Analytics.SHOW_NPS_ACTION);

        PrintOSPreferences.getInstance(activity).setNPSLastDisplayTiming(System.currentTimeMillis());
        NPSPopup.getInstance(activity instanceof NPSPopup.NPSPopupCallback ?
                (NPSPopup.NPSPopupCallback) activity : null, userInfoData)
                .show(activity.getSupportFragmentManager(), NPSPopup.TAG);

    }

    public static void displayEulaPopup(BaseActivity activity, EulaPopup.EulaPopupCallback callback) {
        if (activity == null || activity.isFinishing()) {
            return;
        }

        DialogFragment eulaPopup = EulaPopup.getInstance(callback);

        FragmentTransaction transaction = activity.getFragmentManager().beginTransaction();
        transaction.add(eulaPopup, "loading");
        transaction.commitAllowingStateLoss();
    }

    public static class NSLookupTask extends AsyncTask<Void, Void, String> {

        private final BaseActivity context;
        private final AsyncResponse delegate;

        public NSLookupTask(BaseActivity activity, AsyncResponse delegate) {
            this.context = activity;
            this.delegate = delegate;
        }

        public interface AsyncResponse {
            void processFinish(String output);
        }

        @Override
        protected String doInBackground(Void... voids) {
            try {
                return NSLookupUtils.resolveAPITXT(context, PrintOSPreferences.getInstance(context).getNsLookupTextFileName());
            } catch (Exception e) {
                HPLogger.d(TAG, "failed to locate printos dns " + e);
                return null;
            }
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            HPLogger.d(TAG, String.format("nslookup result %s", result));
            Analytics.sendEvent(Analytics.NSLOOKUP_RESULT_EVENT, result);
            if (delegate != null) {
                delegate.processFinish(result);
            }
        }

    }

    private static boolean shouldDisplaySwitchServerDialogFromHiddenSettings(final BaseActivity activity) {

        String fakeLocation = PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).getFakeLocation();

        if (!TextUtils.isEmpty(fakeLocation)) {
            displayChinaLoggingDialog(activity, false);
            PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).setFakeLocation("");
            PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).setSwitchServerDialogOpened(true);
            return true;
        }

        return false;
    }

    private static boolean shouldDisplaySwitchServerDialog(BaseActivity activity, String result) {

        //check if ns lookup result is empty or is not equal to China or default
        if (TextUtils.isEmpty(result) || (!result.equalsIgnoreCase(Constants.CHINA) && !result.equalsIgnoreCase(Constants.DEFAULT_LOCATION))) {
            return false;
        }

        boolean shouldDisplayChinaLoggingPopup = isUsingWrongServerAndNumberOfSessionsPassed(activity, result);

        //get last saved location of ns lookup result
        String lastLocation = PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).getLastSavedLocation();

        boolean isLocationChanged = false;
        if (!TextUtils.isEmpty(lastLocation)) {
            //current location is China and last location was default or current location is default and last location was China
            isLocationChanged = (result.equalsIgnoreCase(Constants.CHINA) && lastLocation.equalsIgnoreCase(Constants.DEFAULT_LOCATION)) || (result.equalsIgnoreCase(Constants.DEFAULT_LOCATION) && lastLocation.equalsIgnoreCase(Constants.CHINA));
        }

        if (isLocationChanged || shouldDisplayChinaLoggingPopup) {
            displayChinaLoggingDialog(activity, true);
            PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).setLocation(result);
            PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).setSwitchServerDialogOpened(true);

            return true;
        }
        return false;
    }

    public static void displayChinaLoggingDialog(BaseActivity mainActivity, boolean shouldResetSessionCount) {
        String currentServerUrl = PrintOSPreferences.getInstance(mainActivity).getServerUrl();
        if (!TextUtils.isEmpty(currentServerUrl)) {
            SwitchProductionServerDialog.getInstance(mainActivity instanceof SwitchProductionServerDialog.SwitchProductionServerDialogCallbacks ?
                    (SwitchProductionServerDialog.SwitchProductionServerDialogCallbacks) mainActivity : null, currentServerUrl, shouldResetSessionCount)
                    .show(mainActivity.getFragmentManager(), SwitchProductionServerDialog.TAG);
        }
    }

    public interface DialogManagerCallbacks {
        void onRankingPopupAgreeSelected();
    }
}
