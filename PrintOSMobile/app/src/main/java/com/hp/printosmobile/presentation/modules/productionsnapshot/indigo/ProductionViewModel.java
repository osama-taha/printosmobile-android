package com.hp.printosmobile.presentation.modules.productionsnapshot.indigo;

import com.hp.printosmobile.presentation.modules.main.BusinessUnitEnum;

/**
 * Created by Anwar Asbah on 4/12/2016.
 */
public class ProductionViewModel {

    private int jobsInQueue;
    private int jobsCompleted;
    private int printed;
    private int numberOfPrintingJobs;
    private int numberOfTodayPrintedJobs;
    private int numberOfTodayFailedPrintedJobs;
    private String printedUnit;
    private BusinessUnitEnum businessUnitEnum;
    private boolean hasCompletedJobs;

    public int getJobsInQueue() {
        return jobsInQueue;
    }

    public void setJobsInQueue(int jobsInQueue) {
        this.jobsInQueue = jobsInQueue;
    }

    public int getJobsCompleted() {
        return jobsCompleted;
    }

    public void setJobsCompleted(int jobsCompleted) {
        this.jobsCompleted = jobsCompleted;
    }

    public int getPrinted() {
        return printed;
    }

    public void setPrinted(int printed) {
        this.printed = printed;
    }

    public String getPrintedUnit() {
        return printedUnit;
    }

    public void setPrintedUnit(String printedUnit) {
        this.printedUnit = printedUnit;
    }

    public int getNumberOfPrintingJobs() {
        return numberOfPrintingJobs;
    }

    public void setNumberOfPrintingJobs(int numberOfPrintingJobs) {
        this.numberOfPrintingJobs = numberOfPrintingJobs;
    }

    public int getNumberOfTodayPrintedJobs() {
        return numberOfTodayPrintedJobs;
    }

    public void setNumberOfTodayPrintedJobs(int numberOfTodayPrintedJobs) {
        this.numberOfTodayPrintedJobs = numberOfTodayPrintedJobs;
    }

    public int getNumberOfTodayFailedPrintedJobs() {
        return numberOfTodayFailedPrintedJobs;
    }

    public void setNumberOfTodayFailedPrintedJobs(int numberOfTodayFailedPrintedJobs) {
        this.numberOfTodayFailedPrintedJobs = numberOfTodayFailedPrintedJobs;
    }

    public void setBusinessUnitEnum(BusinessUnitEnum businessUnitEnum) {
        this.businessUnitEnum = businessUnitEnum;
    }

    public BusinessUnitEnum getBusinessUnitEnum() {
        return businessUnitEnum;
    }

    public boolean isHasCompletedJobs() {
        return hasCompletedJobs;
    }

    public void setHasCompletedJobs(boolean hasCompletedJobs) {
        this.hasCompletedJobs = hasCompletedJobs;
    }
}
