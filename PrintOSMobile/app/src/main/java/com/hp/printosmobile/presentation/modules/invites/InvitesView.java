package com.hp.printosmobile.presentation.modules.invites;

import com.hp.printosmobile.presentation.MVPView;

import java.util.List;

/**
 * Created by Anwar Asbah on 11/6/17.
 */
public interface InvitesView extends MVPView {

    void onUserRolesRetrieved(List<InviteUserRoleViewModel> models);

    void errorRetrievingUserRoles();

}