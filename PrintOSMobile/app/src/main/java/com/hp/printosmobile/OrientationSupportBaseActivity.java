package com.hp.printosmobile;

import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.OrientationEventListener;
import android.view.View;
import android.view.ViewTreeObserver;

import com.hp.printosmobile.presentation.BaseActivity;
import com.hp.printosmobilelib.core.logging.HPLogger;

/**
 * created by Anwar Asbah 2/5/2018
 */
public abstract class OrientationSupportBaseActivity extends BaseActivity {

    public static final String TAG = OrientationSupportBaseActivity.class.getName();

    private static final int P_ANGLE_TOP_LEFT = 30;
    private static final int P_ANGLE_BOTTOM_LEFT = 150;
    private static final int P_ANGLE_BOTTOM_RIGHT = 210;
    private static final int P_ANGLE_TOP_RIGHT = 330;

    private static final int L_ANGLE_TOP_LEFT = 80;
    private static final int L_ANGLE_BOTTOM_LEFT = 100;
    private static final int L_ANGLE_BOTTOM_RIGHT = 260;
    private static final int L_ANGLE_TOP_RIGHT = 280;

    private OrientationEventListener orientationEventListener;
    boolean wasPhoneRotate = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final View activityLayout = findViewById(android.R.id.content);
        activityLayout.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                activityLayout.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                init();
            }
        });
    }

    private void init() {
        orientationEventListener = new OrientationEventListener(this,
                SensorManager.SENSOR_DELAY_NORMAL) {

            @Override
            public void onOrientationChanged(int orientation) {
                if ((orientation > 0 && orientation < P_ANGLE_TOP_LEFT)
                        || (orientation > P_ANGLE_BOTTOM_LEFT && orientation < P_ANGLE_BOTTOM_RIGHT)
                        || orientation > P_ANGLE_TOP_RIGHT) {
                    if (wasPhoneRotate && onPortraitOrientation()) {
                        HPLogger.d(TAG, "onOrientationChanged.");
                        enableOrientationEventListener(false);
                    }
                } else if ((orientation > L_ANGLE_TOP_LEFT && orientation < L_ANGLE_BOTTOM_LEFT)
                        || (orientation > L_ANGLE_BOTTOM_RIGHT && orientation < L_ANGLE_TOP_RIGHT)) {
                    if (wasPhoneRotate && onLandscapeOrientation()) {
                        HPLogger.d(TAG, "onOrientationChanged.");
                        enableOrientationEventListener(false);
                    }
                } else if (orientation > 0) {
                    wasPhoneRotate = true;
                }
            }
        };
        enableOrientationEventListener(true);
    }

    public boolean onLandscapeOrientation() {
        return false;
    }

    public boolean onPortraitOrientation() {
        return false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        enableOrientationEventListener(true);
    }

    @Override
    protected void onPause() {
        enableOrientationEventListener(false);
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        enableOrientationEventListener(false);
    }

    @Override
    public void onBackPressed() {
        enableOrientationEventListener(false);
        try {
            super.onBackPressed();
        } catch (Exception e) {
            HPLogger.e(TAG, "landscape on back pressed error ", e);
        }
    }

    private void enableOrientationEventListener(boolean enable) {
        if (orientationEventListener != null) {
            HPLogger.d(TAG, "enableOrientationEventListener(" + enable + ")");
            if (enable) {
                orientationEventListener.enable();
            } else {
                orientationEventListener.disable();
            }
        }
    }
}
