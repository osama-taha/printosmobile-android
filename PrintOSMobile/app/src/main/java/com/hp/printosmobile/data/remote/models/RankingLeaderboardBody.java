package com.hp.printosmobile.data.remote.models;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Osama Taha
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RankingLeaderboardBody {

    @JsonProperty("rankingLeaderboardStatus")
    private RankingLeaderboardStatus rankingLeaderboardStatus;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<>();

    @JsonProperty("rankingLeaderboardStatus")
    public void setRankingLeaderboardStatus(RankingLeaderboardStatus rankingLeaderboardStatus) {
        this.rankingLeaderboardStatus = rankingLeaderboardStatus;
    }

    @JsonProperty("rankingLeaderboardStatus")
    public RankingLeaderboardStatus getRankingLeaderboardStatus() {
        return rankingLeaderboardStatus;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
