package com.hp.printosmobile.data.remote.services;

import com.hp.printosmobile.data.remote.ApiConstants;
import com.hp.printosmobile.data.remote.models.UserInfoData;
import com.hp.printosmobile.data.remote.models.UserRolesData;
import com.hp.printosmobilelib.core.communications.remote.models.UserData;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.PUT;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by Osama Taha on 10/10/16.
 */

public interface UserDataService {

    @GET(ApiConstants.USER_ROLES_API)
    Observable<Response<UserRolesData>> getUserRoles();

    @GET(ApiConstants.USER_INFO_API)
    Observable<Response<UserInfoData>> getUserInfo(@Query("euid") String id);

    @GET(ApiConstants.USERS_URL)
    Observable<Response<UserData.User>> getUser();

    @PUT(ApiConstants.USERS_URL)
    Observable<Response<UserData.User>> updateUser(@Body UserData.User user);

    @GET(ApiConstants.HPID_EDIT_MY_DETAILS_URL)
    Call<ResponseBody> getEditUserDetailsUrl();
}
