package com.hp.printosmobile.presentation.modules.reportkpibreakdown;

import android.text.TextUtils;

import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.data.remote.ApiServicesProvider;
import com.hp.printosmobile.data.remote.models.PreferencesData;
import com.hp.printosmobile.data.remote.models.ReportData;
import com.hp.printosmobile.data.remote.models.WeekData;
import com.hp.printosmobile.data.remote.services.PerformanceTrackingService;
import com.hp.printosmobile.data.remote.services.ReportDataV2;
import com.hp.printosmobile.data.remote.services.WeeklyDataV2;
import com.hp.printosmobile.presentation.modules.filters.DeviceFilterViewModel;
import com.hp.printosmobile.presentation.modules.filters.FilterItem;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitEnum;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitViewModel;
import com.hp.printosmobile.presentation.modules.reports.ReportFilter;
import com.hp.printosmobile.presentation.modules.shared.MeasureTypeEnum;
import com.hp.printosmobilelib.core.utils.HPDateUtils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Response;
import rx.Observable;
import rx.functions.Func1;
import rx.functions.FuncN;

import static com.hp.printosmobile.presentation.modules.reportkpibreakdown.KpiBreakdownActivity.NUMBER_OF_WEEKS;

/**
 * created by Anwar Asbah 1/30/2018
 */
public class KpiBreakdownDataManager {

    public static final String SUB_TAG = "sub_";

    private static final String TAG = KpiBreakdownDataManager.class.getSimpleName();

    private static final String WEEK_CACHE_KEY = "week_cache";
    private static final String DATE_FORMAT = "yyyy-MM-dd";
    private static final int AVAILABILITY_MAX_SCORE = 120;

    private static Map<String, List<KpiBreakdownViewModel>> cache = new HashMap<>();
    private static String cacheDataStamp = "";

    private KpiBreakdownDataManager() {
    }

    public static Observable<List<KpiBreakdownViewModel>> getOverallData(final ReportFilter reportFilter) {

        validateCache();

        ApiServicesProvider serviceProvider = PrintOSApplication.getApiServicesProvider();
        PerformanceTrackingService performanceTrackingService = serviceProvider.getPerformanceTrackingService();

        final BusinessUnitViewModel businessUnit = reportFilter.getBusinessUnit();

        String buName = businessUnit.getBusinessUnit().getName();
        List<String> devices = businessUnit.getFiltersViewModel().getSerialNumbers();
        int offset = reportFilter.getOffSet();

        final String cacheKey = getCacheKey(TAG, businessUnit.getBusinessUnit(), devices, "");
        if (cache != null && cache.containsKey(cacheKey)) {
            final List<KpiBreakdownViewModel> kpiBreakdownViewModels = cache.get(cacheKey);
            if (kpiBreakdownViewModels != null && !kpiBreakdownViewModels.isEmpty()) {
                //return cache
                return Observable.just(kpiBreakdownViewModels);
            }
        }

        return performanceTrackingService.getOverallReport(buName, devices, offset).map(new Func1<Response<ReportData>, List<KpiBreakdownViewModel>>() {
            @Override
            public List<KpiBreakdownViewModel> call(Response<ReportData> reportDataResponse) {
                if (reportDataResponse.isSuccessful()) {
                    if (reportDataResponse.body().getUnitEvents() == null
                            || reportDataResponse.body().getUnitEvents().isEmpty()) {
                        return null;
                    }
                    List<KpiBreakdownViewModel> kpiBreakdownViewModels = parseOverallApiResponse(
                            reportDataResponse.body(), businessUnit);
                    cacheDataStamp = HPDateUtils.formatDate(Calendar.getInstance().getTime(), DATE_FORMAT);
                    cache.put(cacheKey, kpiBreakdownViewModels);
                    return kpiBreakdownViewModels;
                }
                return null;
            }
        });
    }

    private static List<KpiBreakdownViewModel> parseOverallApiResponse(
            ReportData reportData, BusinessUnitViewModel businessUnit) {

        BusinessUnitEnum businessUnitEnum = businessUnit.getBusinessUnit();

        List<KpiBreakdownViewModel> kpiBreakdownViewModels = new ArrayList<>();
        KpiBreakdownEnum mainBreakdownEnum = KpiBreakdownEnum.from("", businessUnitEnum);
        List<KpiBreakdownEnum> breakdownEnumList = mainBreakdownEnum.getBreakdownEnumList();

        Map<String, Integer> overallMap = new HashMap<>();

        for (ReportData.UnitEvent unitEvent : reportData.getUnitEvents()) {
            if (unitEvent.getEvents() != null) {
                KpiBreakdownEnum subKpi = KpiBreakdownEnum.from(unitEvent.getUnitName(), businessUnitEnum);
                if (subKpi == KpiBreakdownEnum.UNKNOWN || !breakdownEnumList.contains(subKpi)) {
                    continue;
                }

                List<KpiBreakdownViewModel.Event> subKpiEvents = new ArrayList<>();
                for (ReportData.UnitEvent.Event event : unitEvent.getEvents()) {
                    if (event.getHappenedOn() != null) {
                        int value = event.getValue();
                        KpiBreakdownViewModel.Event breakdownEvent = new KpiBreakdownViewModel.Event();
                        breakdownEvent.setDate(HPDateUtils.parseDate(event.getHappenedOn(), DATE_FORMAT));
                        breakdownEvent.setDayValue(value);
                        subKpiEvents.add(breakdownEvent);

                        if (overallMap.containsKey(event.getHappenedOn())) {
                            value += overallMap.get(event.getHappenedOn());
                            overallMap.remove(event.getHappenedOn());
                        }
                        overallMap.put(event.getHappenedOn(), value);
                    }
                }

                Collections.sort(subKpiEvents, KpiBreakdownViewModel.Event.DATE_COMPARATOR);
                for (int index = 1; index < subKpiEvents.size(); index++) {
                    KpiBreakdownViewModel.Event dayEvent = subKpiEvents.get(index);
                    KpiBreakdownViewModel.Event lastWeekEvent = subKpiEvents.get(index - 1);
                    dayEvent.setLastWeekValue(lastWeekEvent.getDayValue());
                }

                while (subKpiEvents.size() > NUMBER_OF_WEEKS) {
                    subKpiEvents.remove(0);
                }

                KpiBreakdownViewModel subKpiModel = new KpiBreakdownViewModel();
                subKpiModel.setKpiBreakdownEnum(subKpi);
                subKpiModel.setEventList(subKpiEvents);
                subKpiModel.setOverall(true);

                if (businessUnit.getKpiMaxScoreMap() != null) {
                    subKpiModel.setMaxScore(businessUnit.getKpiMaxScoreMap().get(subKpi.getKey()));
                }

                kpiBreakdownViewModels.add(subKpiModel);
            }
        }

        List<KpiBreakdownViewModel.Event> overallEvents = new ArrayList<>();
        for (String dateKey : overallMap.keySet()) {
            int value = overallMap.get(dateKey);
            KpiBreakdownViewModel.Event breakdownEvent = new KpiBreakdownViewModel.Event();
            breakdownEvent.setDate(HPDateUtils.parseDate(dateKey, DATE_FORMAT));
            breakdownEvent.setDayValue(value);
            overallEvents.add(breakdownEvent);
        }

        Collections.sort(overallEvents, KpiBreakdownViewModel.Event.DATE_COMPARATOR);
        for (int index = 1; index < overallEvents.size(); index++) {
            KpiBreakdownViewModel.Event dayEvent = overallEvents.get(index);
            KpiBreakdownViewModel.Event lastWeekEvent = overallEvents.get(index - 1);
            dayEvent.setLastWeekValue(lastWeekEvent.getDayValue());
        }

        while (overallEvents.size() > NUMBER_OF_WEEKS) {
            overallEvents.remove(0);
        }

        KpiBreakdownViewModel subKpiModel = new KpiBreakdownViewModel();
        subKpiModel.setKpiBreakdownEnum(mainBreakdownEnum);
        subKpiModel.setEventList(overallEvents);
        subKpiModel.setOverall(true);
        kpiBreakdownViewModels.add(subKpiModel);

        Collections.sort(kpiBreakdownViewModels, KpiBreakdownViewModel.KPI_BREAKDOWN_VIEW_MODEL_COMPARATOR);
        return kpiBreakdownViewModels;
    }

    public static Observable<List<KpiBreakdownViewModel>> getKpiData(final ReportFilter reportFilter, boolean isSpotlight) {

        validateCache();

        String kpiName = reportFilter.getKpi().getName();
        BusinessUnitEnum businessUnitName = reportFilter.getBusinessUnit() == null ? BusinessUnitEnum.UNKNOWN : reportFilter.getBusinessUnit().getBusinessUnit();

        KpiBreakdownEnum mainKpiBreakdownEnum = KpiBreakdownEnum.from(kpiName, businessUnitName);


        ApiServicesProvider serviceProvider = PrintOSApplication.getApiServicesProvider();
        PerformanceTrackingService performanceTrackingService = serviceProvider.getPerformanceTrackingService();

        final BusinessUnitViewModel businessUnit = reportFilter.getBusinessUnit();
        BusinessUnitEnum businessUnitEnum = businessUnit == null ? BusinessUnitEnum.UNKNOWN  : businessUnit.getBusinessUnit();
        List<String> devices = businessUnit.getFiltersViewModel().getSerialNumbers();

        if (mainKpiBreakdownEnum == KpiBreakdownEnum.INDIGO_PRINT_VOLUME) {
            boolean hasLength = false;
            boolean hasSheets = false;
            if(businessUnit != null && businessUnit.getFiltersViewModel() != null && businessUnit.getFiltersViewModel().getSelectedDevices() != null) {
                for (FilterItem filterItem : businessUnit.getFiltersViewModel().getSelectedDevices()) {
                    if(filterItem instanceof DeviceFilterViewModel){
                        MeasureTypeEnum measureType = ((DeviceFilterViewModel) filterItem).getMeasureTypeEnum();
                        hasLength = hasLength || measureType == MeasureTypeEnum.LENGTH || measureType == MeasureTypeEnum.MIXED;
                        hasSheets = hasSheets || measureType == MeasureTypeEnum.SHEETS || measureType == MeasureTypeEnum.MIXED;
                    }
                }
            }

            List<Observable<List<KpiBreakdownViewModel>>> requests = new ArrayList<>();
            requests.add(getScorableRequest(KpiBreakdownEnum.INDIGO_PRINT_VOLUME, performanceTrackingService,
                    kpiName, reportFilter, devices, businessUnitEnum, MeasureTypeEnum.IMPRESSIONS.getRequestUnitSystem()));
            if (hasLength) {
                requests.add(getScorableRequest(KpiBreakdownEnum.INDIGO_LM, performanceTrackingService,
                        kpiName, reportFilter, devices, businessUnitEnum, MeasureTypeEnum.LENGTH.getRequestUnitSystem()));
            }
            if (hasSheets) {
                requests.add(getScorableRequest(KpiBreakdownEnum.INDIGO_SHEETS, performanceTrackingService,
                        kpiName, reportFilter, devices, businessUnitEnum, MeasureTypeEnum.SHEETS.getRequestUnitSystem()));
            }

            return Observable.combineLatest(requests, new FuncN<List<KpiBreakdownViewModel>>() {
                @Override
                public List<KpiBreakdownViewModel> call(Object... args) {
                    if (args == null) {
                        return null;
                    }
                    List<KpiBreakdownViewModel> kpiBreakdownViewModels = new ArrayList<>();
                    for (Object list : args) {
                        if (list instanceof List) {
                            for (Object item : (List) list) {
                                if (item instanceof KpiBreakdownViewModel) {
                                    kpiBreakdownViewModels.add((KpiBreakdownViewModel) item);
                                }
                            }
                        }
                    }
                    Collections.sort(kpiBreakdownViewModels, KpiBreakdownViewModel.KPI_BREAKDOWN_VIEW_MODEL_COMPARATOR);
                    return kpiBreakdownViewModels;
                }
            });
        } else if (mainKpiBreakdownEnum == KpiBreakdownEnum.INDIGO_AVAILABILITY) {
            List<Observable<List<KpiBreakdownViewModel>>> requests = new ArrayList<>();
            requests.add(getScorableRequest(mainKpiBreakdownEnum, performanceTrackingService,
                    kpiName, reportFilter, devices, businessUnitEnum, reportFilter.getUnit().getValue()));

            return Observable.combineLatest(requests, new FuncN<List<KpiBreakdownViewModel>>() {
                @Override
                public List<KpiBreakdownViewModel> call(Object... args) {
                    if (args == null) {
                        return null;
                    }
                    List<KpiBreakdownViewModel> kpiBreakdownViewModels = new ArrayList<>();
                    for (Object list : args) {
                        if (list instanceof List) {
                            for (Object item : (List) list) {
                                if (item instanceof KpiBreakdownViewModel) {
                                    kpiBreakdownViewModels.add((KpiBreakdownViewModel) item);
                                }
                            }
                        }
                    }
                    Collections.sort(kpiBreakdownViewModels, KpiBreakdownViewModel.KPI_BREAKDOWN_VIEW_MODEL_COMPARATOR);
                    return kpiBreakdownViewModels;
                }
            });
        } else {
            return getWeekRequest(mainKpiBreakdownEnum, performanceTrackingService, devices, businessUnit, isSpotlight);
        }
    }

    private static Observable<List<KpiBreakdownViewModel>> getScorableRequest(
            final KpiBreakdownEnum breakdownEnum, PerformanceTrackingService performanceTrackingService,
            final String kpiName, ReportFilter reportFilter, List<String> devices,
            final BusinessUnitEnum businessUnit, String impType) {

        String buName = businessUnit.getName();
        final String cacheKey = getCacheKey(kpiName, businessUnit, devices, impType);
        if (cache.containsKey(cacheKey)) {
            final List<KpiBreakdownViewModel> kpiBreakdownViewModels = cache.get(cacheKey);
            if (kpiBreakdownViewModels != null && !kpiBreakdownViewModels.isEmpty()) {
                //return cache
                return Observable.just(kpiBreakdownViewModels);
            }
        }

        PrintOSPreferences printOSPreferences = PrintOSPreferences.getInstance(PrintOSApplication.getAppContext());
        PreferencesData.UnitSystem unitSystem = printOSPreferences.getUnitSystem();

        return performanceTrackingService.getKpiReportV2(kpiName, buName, false, devices,
                reportFilter.getOffSet(), reportFilter.getResolution().getValue(),
                impType, unitSystem.name()).map(new Func1<Response<ReportDataV2>, List<KpiBreakdownViewModel>>() {
            @Override
            public List<KpiBreakdownViewModel> call(Response<ReportDataV2> reportDataV2Response) {
                if (reportDataV2Response.isSuccessful() && reportDataV2Response.body() != null) {
                    List<KpiBreakdownViewModel> kpiBreakdownViewModels = parseKpiData(
                            reportDataV2Response.body().getData(), breakdownEnum);

                    if (breakdownEnum == KpiBreakdownEnum.INDIGO_AVAILABILITY) {
                        KpiBreakdownViewModel aggregate = new KpiBreakdownViewModel();
                        aggregate.setMaxScore(AVAILABILITY_MAX_SCORE);
                        aggregate.setKpiBreakdownEnum(KpiBreakdownEnum.INDIGO_AVAILABILITY_AGGREGATE);
                        List<KpiBreakdownViewModel> models = new ArrayList<>();
                        for (int i = 0; i < kpiBreakdownViewModels.size(); i++) {
                            models.add(kpiBreakdownViewModels.get(i));
                        }
                        aggregate.setAggregateModels(models);

                        kpiBreakdownViewModels.add(aggregate);
                        Collections.sort(kpiBreakdownViewModels, KpiBreakdownViewModel.KPI_BREAKDOWN_VIEW_MODEL_COMPARATOR);
                    }

                    cacheDataStamp = HPDateUtils.formatDate(Calendar.getInstance().getTime(), DATE_FORMAT);

                    cache.put(cacheKey, kpiBreakdownViewModels);
                    return kpiBreakdownViewModels;
                }
                return null;
            }
        });
    }

    private static List<KpiBreakdownViewModel> parseKpiData(ReportData reportData, KpiBreakdownEnum mainBreakdownEnum) {
        List<KpiBreakdownViewModel> kpiBreakdownViewModels = new ArrayList<>();

        Map<String, Map<KpiBreakdownEnum, Integer>> breakdownValuesPerDateMap = new HashMap<>();

        List<KpiBreakdownEnum> breakdownChildEnumList = mainBreakdownEnum.getBreakdownEnumList();

        List<KpiBreakdownEnum> breakdownEnumList = new ArrayList<>();
        if (breakdownChildEnumList != null && !breakdownChildEnumList.isEmpty()) {
            for (KpiBreakdownEnum subKpi : breakdownChildEnumList) {
                breakdownEnumList.add(subKpi);
            }
            if (mainBreakdownEnum == KpiBreakdownEnum.INDIGO_AVAILABILITY) {
                breakdownEnumList.add(KpiBreakdownEnum.INDIGO_UP_TIME);
            }
        } else {
            breakdownEnumList.add(mainBreakdownEnum);
        }

        //grouping values by date
        for (ReportData.UnitEvent unitEvent : reportData.getUnitEvents()) {
            if (unitEvent.getEvents() != null) {
                for (ReportData.UnitEvent.Event event : unitEvent.getEvents()) {
                    if (event.getHappenedOn() != null) {
                        Map<KpiBreakdownEnum, Integer> breakdownValueMap = new HashMap<>();
                        if (breakdownValuesPerDateMap.containsKey(event.getHappenedOn())) {
                            breakdownValueMap = breakdownValuesPerDateMap.get(event.getHappenedOn());
                        } else {
                            breakdownValuesPerDateMap.put(event.getHappenedOn(), breakdownValueMap);
                        }

                        for (KpiBreakdownEnum kpiBreakdownEnum : breakdownEnumList) {
                            int value = getValue(event, kpiBreakdownEnum);
                            if (breakdownValueMap.containsKey(kpiBreakdownEnum)) {
                                value += breakdownValueMap.get(kpiBreakdownEnum);
                                breakdownValueMap.remove(kpiBreakdownEnum);
                            }
                            breakdownValueMap.put(kpiBreakdownEnum, value);
                        }
                    }
                }
            }
        }

        //grouping data by kpi
        for (KpiBreakdownEnum kpiBreakdownEnum : breakdownEnumList) {

            if (kpiBreakdownEnum == KpiBreakdownEnum.INDIGO_AVAILABILITY) {
                continue;
            }

            KpiBreakdownViewModel kpiBreakdownViewModel = new KpiBreakdownViewModel();
            kpiBreakdownViewModel.setMinimumPaperType(reportData.getMinimumPaperType());
            List<KpiBreakdownViewModel.Event> events = new ArrayList<>();
            for (String dateS : breakdownValuesPerDateMap.keySet()) {
                Map<KpiBreakdownEnum, Integer> valuePerKpi = breakdownValuesPerDateMap.get(dateS);

                Date date = HPDateUtils.parseDate(dateS, DATE_FORMAT);
                KpiBreakdownViewModel.Event event = new KpiBreakdownViewModel.Event();
                event.setDate(date);
                float value = 0;
                if (valuePerKpi.containsKey(kpiBreakdownEnum)) {
                    value = valuePerKpi.get(kpiBreakdownEnum);
                }
                event.setRowDayValue((int) value);
                if (mainBreakdownEnum == KpiBreakdownEnum.INDIGO_AVAILABILITY) {
                    float upTime = valuePerKpi.containsKey(KpiBreakdownEnum.INDIGO_UP_TIME) ?
                            valuePerKpi.get(KpiBreakdownEnum.INDIGO_UP_TIME) : 0;

                    if (upTime == 0) {
                        if (value == 0) {
                            value = 0;
                        } else {
                            value = 1;
                        }
                    } else {
                        value = value / upTime;
                    }

                    value *= 100;
                }
                event.setDayValue(value);
                events.add(event);
            }

            Collections.sort(events, KpiBreakdownViewModel.Event.DATE_COMPARATOR);
            for (int index = 1; index < events.size(); index++) {
                KpiBreakdownViewModel.Event dayEvent = events.get(index);
                KpiBreakdownViewModel.Event lastWeekEvent = events.get(index - 1);
                dayEvent.setLastWeekValue(lastWeekEvent.getDayValue());
            }

            while (events.size() > NUMBER_OF_WEEKS) {
                events.remove(0);
            }

            kpiBreakdownViewModel.setKpiBreakdownEnum(kpiBreakdownEnum);
            kpiBreakdownViewModel.setEventList(events);

            kpiBreakdownViewModels.add(kpiBreakdownViewModel);
        }

        Collections.sort(kpiBreakdownViewModels, KpiBreakdownViewModel.KPI_BREAKDOWN_VIEW_MODEL_COMPARATOR);

        return kpiBreakdownViewModels;
    }

    private static int getValue(ReportData.UnitEvent.Event event, KpiBreakdownEnum kpiBreakdownEnum) {
        switch (kpiBreakdownEnum) {
            case INDIGO_LM:
            case INDIGO_SHEETS:
                return event.getCount();
            case INDIGO_IMPRESSIONS:
                return event.getWeight();
            case INDIGO_EPM:
                return event.getWeightedEpm();
            case INDIGO_UP_TIME:
            case INDIGO_PRINTING_TIME:
            case INDIGO_RESTARTS_TIME:
            case INDIGO_FAILURE_RECOVERY_TIME:
            case INDIGO_JAMS_RECOVERY_TIME:
            case INDIGO_SUPPLIES_TIME:
                if (event.getValues() != null) {
                    for (ReportData.UnitEvent.Event.Value value : event.getValues()) {
                        if (value != null && value.getName() != null && value.getName().equalsIgnoreCase(kpiBreakdownEnum.getKey())) {
                            return value.getValue() == null ? 0 : value.getValue().intValue();
                        }
                    }
                }
                return 0;
            case INDIGO_NON_PRINTING_TIME:
                return getValue(event, KpiBreakdownEnum.INDIGO_UP_TIME)
                        - getValue(event, KpiBreakdownEnum.INDIGO_PRINTING_TIME)
                        - getValue(event, KpiBreakdownEnum.INDIGO_RESTARTS_TIME)
                        - getValue(event, KpiBreakdownEnum.INDIGO_FAILURE_RECOVERY_TIME)
                        - getValue(event, KpiBreakdownEnum.INDIGO_JAMS_RECOVERY_TIME)
                        - getValue(event, KpiBreakdownEnum.INDIGO_SUPPLIES_TIME);
            default:
                return event.getValue();
        }
    }

    private static Observable<List<KpiBreakdownViewModel>> getWeekRequest(
            final KpiBreakdownEnum mainBreakdownEnum, PerformanceTrackingService performanceTrackingService,
            final List<String> devices, final BusinessUnitViewModel businessUnit, final boolean isDailySpot) {

        String buName = businessUnit.getBusinessUnit().getName();

        PrintOSPreferences printOSPreferences = PrintOSPreferences.getInstance(PrintOSApplication.getAppContext());
        final PreferencesData.UnitSystem unitSystem = printOSPreferences.getUnitSystem();

        List<KpiBreakdownEnum> breakdownChildEnumList = mainBreakdownEnum.getBreakdownEnumList();
        final List<KpiBreakdownEnum> breakdownEnumList = new ArrayList<>();
        if (breakdownChildEnumList != null && !breakdownChildEnumList.isEmpty() && mainBreakdownEnum != KpiBreakdownEnum.INDIGO_AVAILABILITY) {
            breakdownEnumList.addAll(breakdownChildEnumList);
        } else {
            breakdownEnumList.add(mainBreakdownEnum);
        }

        String cacheKey = getCacheKey(WEEK_CACHE_KEY, businessUnit.getBusinessUnit(), devices, unitSystem.name());
        if (cache != null && cache.containsKey(cacheKey)) {
            List<KpiBreakdownViewModel> cachedModels = cache.get(cacheKey);
            List<KpiBreakdownViewModel> toReturn = new ArrayList<>();
            if (cachedModels != null && !cachedModels.isEmpty()) {
                for (KpiBreakdownViewModel viewModel : cachedModels) {
                    if (breakdownEnumList.contains(viewModel.getKpiBreakdownEnum())) {
                        toReturn.add(viewModel);
                    }
                }
            }
            if (!toReturn.isEmpty()) {
                Collections.sort(toReturn, KpiBreakdownViewModel.KPI_BREAKDOWN_VIEW_MODEL_COMPARATOR);
                return Observable.just(toReturn);
            }
        }

        return performanceTrackingService.getWeeklyReport(buName, devices, NUMBER_OF_WEEKS + 1, null,
                unitSystem.name(), businessUnit.getFiltersViewModel().getSelectedSite().getId(),
                PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).getLanguage())
                .map(new Func1<Response<WeeklyDataV2>, List<KpiBreakdownViewModel>>() {
                    @Override
                    public List<KpiBreakdownViewModel> call(Response<WeeklyDataV2> weeklyDataV2Response) {
                        if (weeklyDataV2Response.isSuccessful() && weeklyDataV2Response.body() != null) {
                            List<KpiBreakdownViewModel> kpiBreakdownViewModels = parseKpiWeeksData(
                                    weeklyDataV2Response.body(), businessUnit.getBusinessUnit(), isDailySpot);

                            cacheDataStamp = HPDateUtils.formatDate(Calendar.getInstance().getTime(), DATE_FORMAT);
                            String cacheKey = getCacheKey(WEEK_CACHE_KEY, businessUnit.getBusinessUnit(), devices, unitSystem.name());
                            cache.put(cacheKey, kpiBreakdownViewModels);

                            List<KpiBreakdownViewModel> toReturn = new ArrayList<>();
                            for (KpiBreakdownViewModel viewModel : kpiBreakdownViewModels) {
                                if (breakdownEnumList.contains(viewModel.getKpiBreakdownEnum())) {
                                    toReturn.add(viewModel);
                                }
                            }

                            Collections.sort(toReturn, KpiBreakdownViewModel.KPI_BREAKDOWN_VIEW_MODEL_COMPARATOR);
                            return toReturn;
                        }
                        return null;
                    }
                });
    }

    private static List<KpiBreakdownViewModel> parseKpiWeeksData(WeeklyDataV2 body, BusinessUnitEnum businessUnit, boolean isDailySpot) {
        List<WeekData> weekDataList = body.getData();
        if (weekDataList == null) {
            return null;
        }

        Map<String, Map<KpiBreakdownEnum, Integer>> kpiDataPerDate = new HashMap<>();

        for (WeekData weekData : weekDataList) {
            Map<KpiBreakdownEnum, Integer> valuesPerKpi = new HashMap<>();
            List<WeekData.Scorable> scorables = weekData.getScorables();
            if (scorables != null) {
                for (WeekData.Scorable scorable : scorables) {
                    if (scorable != null) {
                        KpiBreakdownEnum kpiBreakdownEnum = KpiBreakdownEnum.from(scorable.getName(), businessUnit);
                        Map<KpiBreakdownEnum, Integer> kpiValues = getKpiValues(scorable, kpiBreakdownEnum, isDailySpot);
                        valuesPerKpi.putAll(kpiValues);
                    }
                }
            }
            kpiDataPerDate.put(weekData.getUiStartDate(), valuesPerKpi);
        }

        Map<KpiBreakdownEnum, KpiBreakdownViewModel> kpiBreakdownViewModelMap = new HashMap<>();
        for (String dateS : kpiDataPerDate.keySet()) {
            Date date = HPDateUtils.parseDate(dateS, DATE_FORMAT);
            Map<KpiBreakdownEnum, Integer> kpiBreakdownValuesMap = kpiDataPerDate.get(dateS);
            for (KpiBreakdownEnum kpiBreakdownEnum : kpiBreakdownValuesMap.keySet()) {
                KpiBreakdownViewModel viewModel;
                if (kpiBreakdownViewModelMap.containsKey(kpiBreakdownEnum)) {
                    viewModel = kpiBreakdownViewModelMap.get(kpiBreakdownEnum);
                } else {
                    viewModel = new KpiBreakdownViewModel();
                    viewModel.setKpiBreakdownEnum(kpiBreakdownEnum);
                    viewModel.setEventList(new ArrayList<KpiBreakdownViewModel.Event>());
                    kpiBreakdownViewModelMap.put(kpiBreakdownEnum, viewModel);
                }

                List<KpiBreakdownViewModel.Event> events = viewModel.getEventList();
                KpiBreakdownViewModel.Event event = new KpiBreakdownViewModel.Event();
                event.setDayValue(kpiBreakdownValuesMap.get(kpiBreakdownEnum));
                event.setDate(date);
                events.add(event);
            }
        }

        List<KpiBreakdownViewModel> viewModels = new ArrayList<>();
        for (KpiBreakdownEnum kpiBreakdownEnum : kpiBreakdownViewModelMap.keySet()) {
            KpiBreakdownViewModel kpiBreakdownViewModel = kpiBreakdownViewModelMap.get(kpiBreakdownEnum);
            List<KpiBreakdownViewModel.Event> events = kpiBreakdownViewModel.getEventList();
            Collections.sort(events, KpiBreakdownViewModel.Event.DATE_COMPARATOR);
            for (int index = 1; index < events.size(); index++) {
                KpiBreakdownViewModel.Event dayEvent = events.get(index);
                KpiBreakdownViewModel.Event lastWeekEvent = events.get(index - 1);
                dayEvent.setLastWeekValue(lastWeekEvent.getDayValue());
            }
            while (events.size() > NUMBER_OF_WEEKS) {
                events.remove(0);
            }
            viewModels.add(kpiBreakdownViewModel);
        }

        return viewModels;
    }

    private static Map<KpiBreakdownEnum, Integer> getKpiValues(WeekData.Scorable scorable, KpiBreakdownEnum kpiBreakdownEnum, boolean isSpotlight) {
        Map<KpiBreakdownEnum, Integer> valuesMap = new HashMap<>();
        switch (kpiBreakdownEnum) {
            case INDIGO_RESTART:
            case INDIGO_TECHNICAL_ISSUES:
            case INDIGO_SUPPLIES_LIFESPAN:
                List<WeekData.KpiExplanationItem> items = scorable.getKpiExplanationItems();
                if (items != null) {
                    for (WeekData.KpiExplanationItem item : items) {
                        KpiBreakdownEnum subKpi = KpiBreakdownEnum.from(SUB_TAG + item.getName(), kpiBreakdownEnum.getBusinessUnitEnum());
                        String propertyTag = isSpotlight ? subKpi.getDailySpotPropertyTag() : subKpi.getPropertyTag();
                        List<WeekData.Property> properties = item.getProperties();
                        if (properties != null) {
                            for (WeekData.Property property : properties) {
                                if (property.getName() != null && property.getName().equalsIgnoreCase(propertyTag)) {
                                    valuesMap.put(subKpi, (int) property.getValue());
                                }
                            }
                        }
                    }
                }
                valuesMap.put(kpiBreakdownEnum, scorable.getValue());
                break;
            case INDIGO_AVAILABILITY:
            case LATEX_MAINTENANCE:
            case LATEX_PRINT_VOLUME:
            case LATEX_UTILIZATION:
            case PWP_MAINTENANCE:
            case PWP_SYSTEM_HEALTH:
            case PWP_PRINT_VOLUME:
            case PWP_PAGES_PER_RESTART:
            case PWP_OPERATION_EFFICIENCY:
            case SCITEX_AVAILABILITY:
            case SCITEX_PAPER_JAMS:
            case SCITEX_PRINT_VOLUME:
                valuesMap.put(kpiBreakdownEnum, scorable.getValue());
                break;
            default:
                break;
        }
        return valuesMap;
    }

    private static String getCacheKey(String kpiName, BusinessUnitEnum businessUnitEnum, List<String> devices, String impType) {
        PrintOSPreferences printOSPreferences = PrintOSPreferences.getInstance(PrintOSApplication.getAppContext());
        PreferencesData.UnitSystem unitSystem = printOSPreferences.getUnitSystem();

        String key = (TextUtils.isEmpty(kpiName) ? "/" : kpiName)
                + (businessUnitEnum == null ? "/" : ("/" + businessUnitEnum.getName()))
                + (TextUtils.isEmpty(impType) ? "/" : ("/" + impType))
                + (unitSystem == null ? "/" : ("/" + unitSystem.name()));
        if (devices != null) {
            for (int i = 0; i < devices.size(); i++) {
                key += "/" + devices.get(i);
            }
        }
        return key;
    }

    public static void resetCachedModels() {
        cache = new HashMap<>();
    }

    private static void validateCache() {
        Calendar now = Calendar.getInstance();
        String timeStamp = HPDateUtils.formatDate(now.getTime(), DATE_FORMAT);

        if (!timeStamp.equals(cacheDataStamp)) {
            resetCachedModels();
        }
    }
}