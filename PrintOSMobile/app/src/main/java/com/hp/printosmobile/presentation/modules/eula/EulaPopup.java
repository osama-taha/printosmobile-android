package com.hp.printosmobile.presentation.modules.eula;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import com.hp.printosmobile.Analytics;
import com.hp.printosmobile.Constants;
import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.utils.AppUtils;
import com.hp.printosmobile.utils.HPLocaleUtils;
import com.hp.printosmobile.utils.HPUIUtils;
import com.hp.printosmobilelib.core.communications.remote.APIException;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by anwar asbah on 11/26/2017.
 */
public class EulaPopup extends DialogFragment implements EulaView {

    public static final String TAG = EulaPopup.class.getName();

    @Bind(R.id.eula_web_view)
    WebView eulaWebView;
    @Bind(R.id.buttons_view)
    View buttonsView;
    @Bind(R.id.progress_bar)
    View loadingView;
    @Bind(R.id.error_msg_text_view)
    TextView errorMsg;
    @Bind(R.id.error_layout)
    View errorLayout;

    EulaPopupCallback listener;
    EulaPresenter presenter;
    EULAViewModel model;

    public static EulaPopup getInstance(EulaPopupCallback popupCallback) {
        EulaPopup dialog = new EulaPopup();

        dialog.listener = popupCallback;
        Bundle bundle = new Bundle();
        dialog.setArguments(bundle);

        return dialog;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE,
                R.style.NPSDialogStyle);


    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        return inflater.inflate(R.layout.dialog_eula, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Analytics.sendEvent(Analytics.EULA_SHOWN_ACTION);

        ButterKnife.bind(this, view);
        initView();
        getDialog().setCanceledOnTouchOutside(false);
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            dialog.setCancelable(false);
        }
    }

    private void initView() {
        initPresenter();
    }

    private void initPresenter() {
        if (presenter == null) {
            presenter = new EulaPresenter();
            presenter.attachView(this);
        }

        String languageCode = PrintOSPreferences.getInstance(PrintOSApplication.getAppContext())
                .getLanguageCode();
        presenter.getEula(languageCode);
    }

    @Override
    public void dismiss() {
        super.dismiss();
        if (listener != null) {
            listener.onEulaDialogDismissed();
        }
    }

    @OnClick(R.id.button_cancel)
    public void onCancelClicked() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());

        alertDialogBuilder
                .setMessage(getString(R.string.eula_cancel_warning_msg))
                .setCancelable(false)
                .setPositiveButton(getString(R.string.logout_yes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        Analytics.sendEvent(Analytics.REJECT_EULA_ACTION);

                        //trigger sign out
                        if (listener != null) {
                            listener.onEulaRejected();
                        }
                    }
                })
                .setNegativeButton(getString(R.string.logout_no), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //return back to eula
                        dialog.dismiss();
                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    @OnClick(R.id.button_accept)
    public void onAcceptClicked() {
        if (model == null) {
            return;
        }

        if (presenter == null) {
            presenter = new EulaPresenter();
            presenter.attachView(this);
        }

        Analytics.sendEvent(Analytics.ACCEPT_EULA_ACTION);

        loadingView.setVisibility(View.VISIBLE);

        presenter.acceptEulaAndPrivacyVersion(model);
    }

    @OnClick(R.id.error_layout)
    public void onTryAgainClicked() {
        errorLayout.setVisibility(View.GONE);
        initPresenter();
    }

    @Override
    public void displayEula(EULAViewModel eulaViewModel) {
        model = eulaViewModel;

        WebSettings webSettings = eulaWebView.getSettings();
        webSettings.setDefaultFontSize(15);

        String myHtmlString = Constants.TERMS_OF_SERVICES_HTML_START_TAG + eulaViewModel.getEulaText() + Constants.TERMS_OF_SERVICES_HTML_END_TAG;
        eulaWebView.loadDataWithBaseURL(null, myHtmlString, "text/html", "UTF-8", null);

        eulaWebView.setWebViewClient(new WebViewClient() {

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                AppUtils.startApplication(PrintOSApplication.getAppContext(), Uri.parse(url));
                return true;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                buttonsView.setVisibility(View.VISIBLE);
                loadingView.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public void onEulaSuccessfullyAccepted() {
        dismiss();
    }

    @Override
    public void onError(APIException exception, String tag) {
        loadingView.setVisibility(View.GONE);
        buttonsView.setVisibility(View.VISIBLE);
        Context context = PrintOSApplication.getAppContext();
        if (tag != null) {
            if (tag.equals(EulaActivity.TAG)) {
                errorMsg.setText(HPLocaleUtils.getSimpleErrorMsg(context, exception));
                errorLayout.setVisibility(View.VISIBLE);
            } else if (tag.equals(EulaActivity.TAG_ACCEPT)) {
                HPUIUtils.displayToast(context, HPLocaleUtils.getSimpleErrorMsg(context, exception));
            }
        }
    }

    public interface EulaPopupCallback {
        void onEulaDialogDismissed();

        void onEulaRejected();
    }
}
