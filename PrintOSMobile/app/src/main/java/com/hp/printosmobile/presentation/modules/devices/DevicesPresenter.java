package com.hp.printosmobile.presentation.modules.devices;

import com.hp.printosmobile.presentation.Presenter;
import com.hp.printosmobile.presentation.modules.home.HomeDataManager;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitViewModel;
import com.hp.printosmobile.presentation.modules.shared.DeviceViewModel;
import com.hp.printosmobilelib.core.logging.HPLogger;

import java.util.List;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by Osama Taha on 5/23/16.
 */
public class DevicesPresenter extends Presenter<DevicesView> {

    private static final String TAG = DevicesPresenter.class.getName();

    private BusinessUnitViewModel selectedBusinessUnit;
    private boolean isShiftSupport;

    public void loadData(boolean isPolling) {

        mView.showLoading();

        Subscription subscription = HomeDataManager.getDevicesDataForBusinessUnit(selectedBusinessUnit, true, isShiftSupport, isPolling)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).subscribe(new Action1<List<DeviceViewModel>>() {
                    @Override
                    public void call(List<DeviceViewModel> deviceViewModels) {

                        if (deviceViewModels != null) {

                            mView.hideLoading();

                            if (deviceViewModels.isEmpty()) {
                                mView.onEmptyDevicesList();
                            } else {
                                mView.onGettingDevicesCompleted(deviceViewModels);
                            }

                        } else {
                            loadError();
                        }

                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        loadError();
                        HPLogger.d(TAG, "unable to get the devices data " + throwable);
                    }
                });
        addSubscriber(subscription);
    }

    private void loadError() {

        mView.hideLoading();
        mView.onError();
        //TODO: show error.
        mView.hideLoading();

    }

    public BusinessUnitViewModel getSelectedBusinessUnit() {
        return selectedBusinessUnit;
    }

    public void setSelectedBusinessUnit(BusinessUnitViewModel selectedBusinessUnit) {
        this.selectedBusinessUnit = selectedBusinessUnit;
    }

    public boolean isShiftSupport() {
        return isShiftSupport;
    }

    public void setShiftSupport(boolean shiftSupport) {
        isShiftSupport = shiftSupport;
    }

    public void onRefresh() {
        stopSubscribers();
    }

    @Override
    public void detachView() {
        stopSubscribers();
        mView = null;
    }

}