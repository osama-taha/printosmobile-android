package com.hp.printosmobile.data.remote.services;

import com.hp.printosmobile.data.remote.ApiConstants;
import com.hp.printosmobile.data.remote.models.ServiceCallData;

import java.util.List;

import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * created by Anwar Asbah 3/9/2017
 */
public interface ServiceCallService {

    @GET(ApiConstants.SERVICE_CALL_API)
    Observable<Response<ServiceCallData>> getServiceCalls(@Query("serial") List<String> serialNumbers, @Query("closedDaysBack") int closedDaysBack);

}
