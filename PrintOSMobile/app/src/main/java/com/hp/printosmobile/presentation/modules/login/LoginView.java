package com.hp.printosmobile.presentation.modules.login;

import com.hp.printosmobile.presentation.MVPView;
import com.hp.printosmobilelib.core.communications.remote.APIException;
import com.hp.printosmobilelib.core.communications.remote.models.UserData;

/**
 * @author Osama Taha
 */
public interface LoginView extends MVPView {

    void showLoading();

    void hideLoading();

    void setLoginInButtonEnabled(boolean enabled);

    void onLoginSuccessful(UserData userData);

    void showGeneralLoginError(APIException error);

    void showErrorMsg(String msg);
}