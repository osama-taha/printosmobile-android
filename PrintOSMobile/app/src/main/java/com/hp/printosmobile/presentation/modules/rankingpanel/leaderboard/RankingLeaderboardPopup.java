package com.hp.printosmobile.presentation.modules.rankingpanel.leaderboard;

import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hp.printosmobile.Analytics;
import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.presentation.modules.week.RankingViewModel;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Osama Taha
 */
public class RankingLeaderboardPopup extends DialogFragment {

    public static final String TAG = RankingLeaderboardPopup.class.getName();

    private static final String RANKING_MODEL_ARG = "ranking_model_arg";

    @Bind(R.id.description_text_view)
    TextView descriptionTextView;
    @Bind(R.id.button_no_thanks)
    TextView buttonNoThanks;

    private RankingPopupCallbacks callbacks;
    private RankingViewModel rankingViewModel;

    public static RankingLeaderboardPopup getInstance(RankingPopupCallbacks callbacks, RankingViewModel rankingViewModel) {
        RankingLeaderboardPopup dialog = new RankingLeaderboardPopup();

        dialog.callbacks = callbacks;
        Bundle bundle = new Bundle();
        bundle.putSerializable(RANKING_MODEL_ARG, rankingViewModel);
        dialog.setArguments(bundle);

        return dialog;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.RankingLeaderboardPopup);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        Bundle bundle = getArguments();
        if (bundle != null && bundle.containsKey(RANKING_MODEL_ARG)) {
            rankingViewModel = (RankingViewModel) bundle.getSerializable(RANKING_MODEL_ARG);
        }

        return inflater.inflate(R.layout.dialog_ranking, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ButterKnife.bind(this, view);
        initView();
        getDialog().setCanceledOnTouchOutside(false);

        Analytics.sendEvent(Analytics.RANKING_LEADERBOARD_POPUP_SHOWN);

    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null && dialog.getWindow() != null) {
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        }
    }

    private void initView() {

        String popupMessage;

        if (rankingViewModel.isTop100World()) {

            popupMessage = PrintOSApplication.getAppContext().getString(R.string.ranking_leaderboard_popup_rank_world_top_100_message);

        } else {

            String regionName = "";
            int rank = 0;
            if (rankingViewModel.isTop10World()) {

                rank = rankingViewModel.getWorldWideViewModel().getActualRank();
                popupMessage = PrintOSApplication.getAppContext().getString(R.string.ranking_leaderboard_popup_rank_world_top_10, rank);

            } else {

                if (rankingViewModel.isTop10Region()) {
                    regionName = rankingViewModel.getRegionViewModel().getName();
                    rank = rankingViewModel.getRegionViewModel().getActualRank();
                } else {
                    regionName = rankingViewModel.getSubRegionViewModel().getName();
                    rank = rankingViewModel.getSubRegionViewModel().getActualRank();
                }

                popupMessage = PrintOSApplication.getAppContext().getString(R.string.ranking_leaderboard_popup_rank_top_10_message, rank, regionName);

            }
        }


        descriptionTextView.setText(popupMessage);

        String dontShowAgain = getString(R.string.ranking_leaderboard_popup_dont_show_again);
        SpannableString spannable = new SpannableString(dontShowAgain);
        spannable.setSpan(new UnderlineSpan(), 0, dontShowAgain.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);

        buttonNoThanks.setText(spannable);
    }

    @OnClick(R.id.button_yes)
    public void onYesButtonClicked() {

        if (callbacks != null) {
            callbacks.onAgreeButtonClicked();
        }

        dismissAllowingStateLoss();

        Analytics.sendEvent(Analytics.RANKING_LEADERBOARD_POPUP_YES_CLICKED);

    }

    @OnClick(R.id.button_remind_me_later)
    public void onRemindMeLaterButtonClicked() {

        dismissAllowingStateLoss();

        if (callbacks != null){
            callbacks.onDialogDismissed();
        }

        Analytics.sendEvent(Analytics.RANKING_LEADERBOARD_NO_THANKS_CLICKED);

    }

    @OnClick(R.id.button_no_thanks)
    public void onNoThanksButtonClicked() {

        dismissAllowingStateLoss();

        PrintOSPreferences.getInstance().setRankingLeaderboardDontShowAgain(true);

        if (callbacks != null){
            callbacks.onDialogDismissed();
        }

        Analytics.sendEvent(Analytics.RANKING_LEADERBOARD_POPUP_DONT_SHOW_CLICKED);

    }

    public interface RankingPopupCallbacks {
        void onAgreeButtonClicked();
        void onDialogDismissed();
    }
}
