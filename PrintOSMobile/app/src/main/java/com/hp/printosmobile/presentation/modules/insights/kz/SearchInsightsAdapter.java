package com.hp.printosmobile.presentation.modules.insights.kz;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

import com.hp.printosmobile.R;
import com.hp.printosmobile.data.remote.models.kz.KZItem;
import com.hp.printosmobile.presentation.modules.shared.PrintOSBaseAdapter;
import com.hp.printosmobilelib.ui.widgets.panel.PanelView;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Osama Taha
 */
public class SearchInsightsAdapter extends PrintOSBaseAdapter<KZItem> {

    private static final int ITEM_LAYOUT_ID = 2;
    private static final int VIDEO_LAYOUT_ID = 3;
    private final String selectedBu;
    private final Activity activity;

    private String searchQuery;

    private FooterViewHolder footerViewHolder;

    SearchInsightsAdapter(Activity context, String selectedBu) {
        this.activity = context;
        this.selectedBu = selectedBu;
    }

    @Override
    public int getItemViewType(int position) {
        return (isLastPosition(position) && isFooterAdded) ? FOOTER : getSearchItemViewType(position);
    }

    private int getSearchItemViewType(int position) {
        KZItem item = getItem(position);
        if (KZSupportedFormat.from(item.getFormat()) == KZSupportedFormat.MP4) {
            return VIDEO_LAYOUT_ID;
        }
        return ITEM_LAYOUT_ID;
    }

    @Override
    protected RecyclerView.ViewHolder createHeaderViewHolder(ViewGroup parent) {
        return null;
    }

    @Override
    protected RecyclerView.ViewHolder createItemViewHolder(final ViewGroup parent, int viewType) {

        PanelView panelView;
        if (viewType == VIDEO_LAYOUT_ID) {
            panelView = new KZVideoView(activity, selectedBu);
        } else {
            panelView = new KZDefaultView(activity, selectedBu);
        }

        final BaseSearchItemViewHolder holder = new BaseSearchItemViewHolder(panelView);

        holder.itemView.findViewById(R.id.info_ll).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int adapterPos = holder.getAdapterPosition();
                if (adapterPos != RecyclerView.NO_POSITION && onItemClickListener != null) {
                    onItemClickListener.onItemClick(adapterPos, holder.itemView);
                }
            }
        });

        return holder;
    }

    @Override
    protected RecyclerView.ViewHolder createFooterViewHolder(ViewGroup parent) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.search_adapter_footer, parent, false);

        final FooterViewHolder holder = new FooterViewHolder(view);
        holder.reloadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (onReloadClickListener != null) {
                    onReloadClickListener.onReloadClick();
                }
            }
        });

        return holder;
    }

    @Override
    protected void bindHeaderViewHolder(RecyclerView.ViewHolder viewHolder) {

    }

    @Override
    protected void showLoadMoreHeader() {

    }

    @Override
    protected void showErrorHeader() {

    }

    @Override
    protected void showNoResultsHeader() {

    }

    @Override
    protected void bindItemViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        final BaseSearchItemViewHolder holder = (BaseSearchItemViewHolder) viewHolder;
        final KZItem kzItem = getItem(position);

        if (kzItem != null) {
            ((PanelView<KZItem>) holder.itemView).updateViewModel(kzItem);
        }

        if (getSearchItemViewType(position) == ITEM_LAYOUT_ID) {
            ((KZDefaultView) holder.itemView).setUpDescription(kzItem, searchQuery);
        }
    }

    @Override
    protected void bindFooterViewHolder(RecyclerView.ViewHolder viewHolder) {
        footerViewHolder = (FooterViewHolder) viewHolder;
    }

    @Override
    protected void showLoadMoreFooter() {
        if (footerViewHolder != null) {
            footerViewHolder.noResultsLayout.setVisibility(View.GONE);
            footerViewHolder.errorLayout.setVisibility(View.GONE);
            footerViewHolder.loadingLayout.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void showErrorFooter() {
        if (footerViewHolder != null) {
            footerViewHolder.noResultsLayout.setVisibility(View.GONE);
            footerViewHolder.loadingLayout.setVisibility(View.GONE);
            footerViewHolder.errorLayout.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void showNoResultsFooter() {
        if (footerViewHolder != null) {
            footerViewHolder.loadingLayout.setVisibility(View.GONE);
            footerViewHolder.errorLayout.setVisibility(View.GONE);
            footerViewHolder.noResultsLayout.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void addFooterView() {
        removeFooterView();
        isFooterAdded = true;
        add(new KZItem());
    }

    @Override
    public void addHeaderView() {

    }


    public void removeFooterView() {

        isFooterAdded = false;

        for (int i = 0 ; i < items.size() ; i++) {
            KZItem item = items.get(i);
            if (item != null && item.getId() == null) {
                items.remove(i);
                notifyItemRemoved(i);
            }
        }

    }

    public static class BaseSearchItemViewHolder extends RecyclerView.ViewHolder {

        public BaseSearchItemViewHolder(View itemView) {
            super(itemView);
        }
    }

    public static class FooterViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.loading_layout)
        FrameLayout loadingLayout;
        @Bind(R.id.error_layout)
        RelativeLayout errorLayout;
        @Bind(R.id.no_results_layout)
        RelativeLayout noResultsLayout;
        @Bind(R.id.retry_btn)
        Button reloadButton;

        FooterViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public void setSearchQuery(String searchQuery) {
        this.searchQuery = searchQuery;
    }
}