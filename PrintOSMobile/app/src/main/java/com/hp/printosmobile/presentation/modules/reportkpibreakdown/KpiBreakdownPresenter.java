package com.hp.printosmobile.presentation.modules.reportkpibreakdown;

import com.hp.printosmobile.presentation.Presenter;
import com.hp.printosmobile.presentation.modules.reports.ReportFilter;
import com.hp.printosmobilelib.core.communications.remote.APIException;
import com.hp.printosmobilelib.core.logging.HPLogger;

import java.util.List;

import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Anwar Asbah on 1/30/2018.
 */
public class KpiBreakdownPresenter extends Presenter<KpiBreakdownView> {

    private static final String TAG = KpiBreakdownPresenter.class.getSimpleName();

    public void getOverallData(ReportFilter reportFilter) {
        Subscription subscription = KpiBreakdownDataManager.getOverallData(reportFilter)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<List<KpiBreakdownViewModel>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        HPLogger.d(TAG, "failed to get overall report: " + e.getMessage());
                        onFailure(e);
                    }

                    @Override
                    public void onNext(List<KpiBreakdownViewModel> kpiBreakdownViewModels) {
                        if (mView != null) {
                            mView.displayKpiBreakdown(kpiBreakdownViewModels);
                        }
                    }
                });
        addSubscriber(subscription);
    }

    public void getKpiData(final ReportFilter reportFilter, boolean isSpotlight) {
        if (reportFilter == null) {
            HPLogger.d(TAG, "failed to get kpi report: report filter == null");
            onFailure(null);
            return;
        }
        Subscription subscription = KpiBreakdownDataManager.getKpiData(reportFilter, isSpotlight)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<List<KpiBreakdownViewModel>>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        String kpiName = "null";
                        if (reportFilter != null && reportFilter.getKpi() != null && reportFilter.getKpi().getName() != null) {
                            kpiName = reportFilter.getKpi().getName();
                        }
                        HPLogger.d(TAG, "failed to get " + kpiName + " report: " + e.getMessage());
                        onFailure(e);
                    }

                    @Override
                    public void onNext(List<KpiBreakdownViewModel> kpiBreakdownViewModels) {
                        if (mView != null) {
                            mView.displayKpiBreakdown(kpiBreakdownViewModels);
                        }
                    }
                });
        addSubscriber(subscription);
    }

    private void onFailure(Throwable e) {
        APIException exception = APIException.create(APIException.Kind.UNEXPECTED);
        if (e instanceof APIException) {
            exception = (APIException) e;
        }
        if (mView != null) {
            mView.onError(exception, TAG);
        }
    }

    public void onRefresh() {
        stopSubscribers();
    }

    @Override
    public void detachView() {
        stopSubscribers();
        mView = null;
    }
}
