package com.hp.printosmobile.data.remote.models.kz;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Osama on 3/15/18.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class KZSearchResult {

    @JsonProperty("total_assets_count")
    private Integer totalAssetsCount;
    @JsonProperty("total_assets_returned")
    private Integer totalAssetsReturned;
    @JsonProperty("searchAssetResponses")
    private List<KZItem> searchAssetResponses = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<>();

    @JsonProperty("total_assets_count")
    public Integer getTotalAssetsCount() {
        return totalAssetsCount;
    }

    @JsonProperty("total_assets_count")
    public void setTotalAssetsCount(Integer totalAssetsCount) {
        this.totalAssetsCount = totalAssetsCount;
    }

    @JsonProperty("total_assets_returned")
    public Integer getTotalAssetsReturned() {
        return totalAssetsReturned;
    }

    @JsonProperty("total_assets_returned")
    public void setTotalAssetsReturned(Integer totalAssetsReturned) {
        this.totalAssetsReturned = totalAssetsReturned;
    }

    @JsonProperty("searchAssetResponses")
    public List<KZItem> getSearchAssetResponses() {
        return searchAssetResponses;
    }

    @JsonProperty("searchAssetResponses")
    public void setSearchAssetResponses(List<KZItem> searchAssetResponses) {
        this.searchAssetResponses = searchAssetResponses;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}