package com.hp.printosmobile.data.remote.models;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Anwar Asbah 11/7/2017
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class LoginBeatCoinData {

    @JsonProperty("numberOfCoins")
    Integer coins;
    @JsonProperty("linkToCoins")
    String coinsURL;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<>();

    @JsonProperty("numberOfCoins")
    public Integer getCoins() {
        return coins;
    }

    @JsonProperty("numberOfCoins")
    public void setCoins(Integer coins) {
        this.coins = coins;
    }

    @JsonProperty("linkToCoins")
    public String getCoinsURL() {
        return coinsURL;
    }

    @JsonProperty("linkToCoins")
    public void setCoinsURL(String coinsURL) {
        this.coinsURL = coinsURL;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
}