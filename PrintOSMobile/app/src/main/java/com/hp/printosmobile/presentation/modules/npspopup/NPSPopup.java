package com.hp.printosmobile.presentation.modules.npspopup;

import android.app.Dialog;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.SpannableString;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hp.printosmobile.Analytics;
import com.hp.printosmobile.AppseeSdk;
import com.hp.printosmobile.BuildConfig;
import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.data.remote.ApiConstants;
import com.hp.printosmobile.data.remote.models.UserInfoData;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitEnum;
import com.hp.printosmobile.utils.AppUtils;
import com.hp.printosmobile.utils.HPUIUtils;
import com.hp.printosmobilelib.core.logging.HPLogger;
import com.hp.printosmobilelib.core.utils.HPDateUtils;

import java.io.UnsupportedEncodingException;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by anwar asbah on 7/17/2017.
 */
public class NPSPopup extends DialogFragment {

    public static final String TAG = NPSPopup.class.getName();
    private static final int NUMBER_OF_BIT_COINS = 25;
    private static final int MARGIN_BETWEEN_RATING_BUTTONS_DP = 5;
    private static final int SMILEYS_COUNT = 5;
    private static final int SMILEYS_MAX_VALUE = 10;
    private static final String USER_INFO_ARG = "user_info_arg";
    private static final int RATE_ANGRY = 0;
    private static final int RATE_SAD = 2;
    private static final int RATE_NEUTRAL = 5;
    private static final int RATE_SMILE = 8;
    private static final int RATE_LOVE = 10;

    @Bind(R.id.rating_layout)
    LinearLayout ratingLayout;
    @Bind(R.id.title)
    TextView titleTextView;
    @Bind(R.id.not_sure_button)
    TextView notSureButton;
    @Bind(R.id.descriptive_text_view)
    TextView descriptiveTextView;

    NPSPopupCallback listener;
    UserInfoData userInfoData;

    public static NPSPopup getInstance(NPSPopupCallback popupCallback, UserInfoData userInfoData) {
        NPSPopup dialog = new NPSPopup();

        dialog.listener = popupCallback;
        Bundle bundle = new Bundle();
        bundle.putSerializable(USER_INFO_ARG, userInfoData);
        dialog.setArguments(bundle);

        return dialog;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE,
                R.style.NPSDialogStyle);

        Bundle bundle = getArguments();
        if (bundle != null && bundle.containsKey(USER_INFO_ARG)) {
            userInfoData = (UserInfoData) bundle.getSerializable(USER_INFO_ARG);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        return inflater.inflate(R.layout.nps_popup, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        initView();
        getDialog().setCanceledOnTouchOutside(false);
        AppseeSdk.getInstance(getActivity()).startScreen(AppseeSdk.SCREEN_NPS);
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null && dialog.getWindow() != null) {
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        }
    }

    private void initView() {
        ratingLayout.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                ratingLayout.getViewTreeObserver().removeOnGlobalLayoutListener(this);

                int layoutWidth = ratingLayout.getMeasuredWidth();
                int availableWidth = layoutWidth - (SMILEYS_COUNT - 1) * HPUIUtils.dpToPx(PrintOSApplication.getAppContext(), MARGIN_BETWEEN_RATING_BUTTONS_DP);
                int itemWidth = availableWidth / SMILEYS_COUNT;

                for (int i = 0; i <= 10; i++) {
                    if (i == RATE_ANGRY || i == RATE_SAD || i == RATE_NEUTRAL || i == RATE_SMILE || i == RATE_LOVE) {
                        ratingLayout.addView(getItemView(i, itemWidth));
                    }
                }
            }
        });

        String notSureText = getString(R.string.nps_not_sure);
        SpannableString notSureSpannable = new SpannableString(notSureText);
        notSureSpannable.setSpan(new UnderlineSpan(), 0, notSureText.length(), 0);
        notSureButton.setText(notSureSpannable);

        String title = getString(R.string.nps_title, NUMBER_OF_BIT_COINS);
        String bitCoinsString = String.valueOf(NUMBER_OF_BIT_COINS);
        int start = title.indexOf(bitCoinsString);
        int end = start + bitCoinsString.length();
        SpannableString titleSpannable = new SpannableString(title);
        titleSpannable.setSpan(new RelativeSizeSpan(2f), start, end, 0);
        titleSpannable.setSpan(new StyleSpan(Typeface.BOLD), 0, title.length(), 0);
        titleTextView.setText(titleSpannable);

        String descriptiveString = descriptiveTextView.getText().toString();

        String indigoString = BusinessUnitEnum.INDIGO_PRESS.getBusinessUnitDisplayName(false);
        if (indigoString != null && descriptiveString.contains(indigoString)) {
            start = descriptiveString.indexOf(indigoString);
            end = start + indigoString.length();
            SpannableString descriptiveSpannable = new SpannableString(descriptiveString);
            descriptiveSpannable.setSpan(new StyleSpan(Typeface.BOLD), start, end, 0);
            descriptiveSpannable.setSpan(new RelativeSizeSpan(1.2f), start, end, 0);
            descriptiveTextView.setText(descriptiveSpannable);
        }
    }

    public ImageView getItemView(final int position, int itemSize) {
        final ImageView imageView = new ImageView(getActivity());

        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.height = itemSize;
        layoutParams.width = itemSize;
        layoutParams.setMargins(0, 0, position == SMILEYS_MAX_VALUE ? 0 :
                HPUIUtils.dpToPx(PrintOSApplication.getAppContext(), MARGIN_BETWEEN_RATING_BUTTONS_DP), 0);

        imageView.setLayoutParams(layoutParams);

        int imageResource = 0;
        switch (position) {
            case RATE_ANGRY:
                imageResource = R.drawable.angry;
                break;
            case RATE_SAD:
                imageResource = R.drawable.sad;
                break;
            case RATE_NEUTRAL:
                imageResource = R.drawable.neutral;
                break;
            case RATE_SMILE:
                imageResource = R.drawable.smile;
                break;
            case RATE_LOVE:
                imageResource = R.drawable.love;
                break;
            default:
                break;
        }
        imageView.setImageResource(imageResource);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    int imageResource = 0;
                    switch (position) {
                        case RATE_ANGRY:
                            imageResource = R.drawable.angry_selected;
                            break;
                        case RATE_SAD:
                            imageResource = R.drawable.sad_selected;
                            break;
                        case RATE_NEUTRAL:
                            imageResource = R.drawable.neutral_selected;
                            break;
                        case RATE_SMILE:
                            imageResource = R.drawable.smile_selected;
                            break;
                        case RATE_LOVE:
                            imageResource = R.drawable.love_selected;
                            break;
                        default:
                            break;
                    }

                    imageView.setImageResource(imageResource);

                    String url = getUrl(position);
                    AppUtils.startApplication(getActivity(), Uri.parse(url));
                } catch (Exception ignored) {
                }
                dismissAllowingStateLoss();
            }
        });

        return imageView;
    }

    private String getUrl(int score) throws UnsupportedEncodingException {

        HPLogger.d(TAG, "submit nps with score " + score);

        Analytics.sendEvent(Analytics.SUBMIT_NPS_ACTION, String.valueOf(score));

        PrintOSPreferences printOSPreferences = PrintOSPreferences.getInstance(getActivity());

        String locale = printOSPreferences.getLanguage(getString(R.string.default_language));
        String source = printOSPreferences.isProductionStack() ? "PrintOS_Mobile" : "PrintOS_Mobile_DEV";

        return String.format(ApiConstants.NPS_URL, score, locale,
                String.valueOf(HPDateUtils.getWeek()),
                String.valueOf(HPDateUtils.getYear()),
                source,
                userInfoData == null ? "" : userInfoData.getCustomerName(),
                userInfoData == null ? "" : userInfoData.getQueryString(),
                BuildConfig.VERSION_NAME);
    }

    @Override
    public void dismiss() {
        super.dismiss();
        if (listener != null) {
            listener.onDialogDismissed();
        }
    }

    @Override
    public void dismissAllowingStateLoss() {
        super.dismissAllowingStateLoss();
        if (listener != null) {
            listener.onDialogDismissed();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        Analytics.sendEvent(Analytics.DISMISS_NPS_ACTION);
    }

    @OnClick(R.id.not_sure_button)
    public void notSureClicked() {
        dismissAllowingStateLoss();
    }

    public interface NPSPopupCallback {
        void onDialogDismissed();
    }
}
