package com.hp.printosmobile.presentation.modules.insights.kz;

import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.data.remote.models.kz.KZAssetResult;
import com.hp.printosmobile.data.remote.models.kz.KZItem;
import com.hp.printosmobile.data.remote.services.InsightsService;
import com.hp.printosmobile.presentation.Presenter;
import com.hp.printosmobile.presentation.modules.insights.InsightsUtils;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitEnum;
import com.hp.printosmobile.presentation.modules.shared.LanguageEnum;

import retrofit2.Response;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by Osama Taha on 3/20/18.
 */

public class KZItemDetailsPresenter extends Presenter<KZItemDetailsView> {

    private Subscription subscription;

    public void getAssetData(final KZItem kzItem, BusinessUnitEnum businessUnitEnum) {

        if (mView == null) {
            return;
        }

        mView.hideErrorView();
        mView.showLoading();

        String appLanguage = PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).getLanguage();
        LanguageEnum languageEnum = LanguageEnum.from(appLanguage);

        final InsightsService insightsService = PrintOSApplication.getApiServicesProvider().getInsightsService();
        subscription = insightsService.getAsset(kzItem.getId(), businessUnitEnum.getKzName(),
                true, languageEnum.getKzLanguageCode())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Response<KZAssetResult>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                        if (mView == null) {
                            return;
                        }

                        mView.showErrorView();
                        mView.hideLoading();
                    }

                    @Override
                    public void onNext(Response<KZAssetResult> kzAssetResultResponse) {

                        if (mView == null) {
                            return;
                        }

                        if (kzAssetResultResponse.isSuccessful() && kzAssetResultResponse.body() != null) {

                            mView.hideErrorView();

                            KZAssetResult assetResult = kzAssetResultResponse.body();

                            KZSupportedFormat format = KZSupportedFormat.from(kzItem.getFormat());

                            if (format == KZSupportedFormat.MP4) {

                                KZAssetModel model = new KZAssetModel();
                                model.setKZItem(kzItem);
                                model.setExternalUrl(assetResult.getExternalUrl());

                                if (assetResult.getSubtitleEntity() != null && !assetResult.getSubtitleEntity().isEmpty()) {

                                    String appLanguage = PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).getLanguage();
                                    LanguageEnum selectedLanguage = LanguageEnum.from(appLanguage);

                                    KZAssetResult.SubtitleEntity selectedSubtitleEntity = null;
                                    KZAssetResult.SubtitleEntity enSubtitleEntity = null;

                                    for (KZAssetResult.SubtitleEntity subtitleEntity : assetResult.getSubtitleEntity()) {

                                        if (selectedLanguage.getKzLanguageCode().equalsIgnoreCase(subtitleEntity.getLanguageEntity().getLangAnsi())) {
                                            selectedSubtitleEntity = subtitleEntity;
                                        }

                                        if (LanguageEnum.EN.getKzLanguageCode().equalsIgnoreCase(subtitleEntity.getLanguageEntity().getLangAnsi())) {
                                            enSubtitleEntity = subtitleEntity;
                                        }
                                    }

                                    if (selectedSubtitleEntity == null) {
                                        selectedSubtitleEntity = enSubtitleEntity;
                                    }

                                    if (selectedSubtitleEntity == null) {
                                        selectedSubtitleEntity = assetResult.getSubtitleEntity().get(0);
                                    }

                                    model.setSubtitleUrl(selectedSubtitleEntity.getPreSignedUrl());
                                }

                                mView.hideLoading();
                                mView.onFinishedGettingAssetData(model);

                            } else if (format == KZSupportedFormat.PDF){

                                mView.onFinishedGettingAssetData(InsightsUtils.getKzPDFUrl(assetResult));

                            } else {

                                mView.onFinishedGettingAssetData(assetResult.getExternalUrl());
                            }

                        } else {
                            mView.showErrorView();
                            mView.hideLoading();
                        }

                    }
                });


    }

    public Observable<KZItem> getKZItemById(final String kzItemId, BusinessUnitEnum businessUnitEnum) {

        String appLanguage = PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).getLanguage();
        LanguageEnum languageEnum = LanguageEnum.from(appLanguage);

        final InsightsService insightsService = PrintOSApplication.getApiServicesProvider().getInsightsService();
        return insightsService.getAsset(kzItemId, businessUnitEnum.getKzName(),
                false, languageEnum.getKzLanguageCode())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).map(new Func1<Response<KZAssetResult>, KZItem>() {
                    @Override
                    public KZItem call(Response<KZAssetResult> kzAssetResultResponse) {

                        KZAssetResult assetResult = kzAssetResultResponse.body();

                        KZItem kzItem = new KZItem();

                        if (assetResult != null) {
                            kzItem.setId(assetResult.getId());
                            kzItem.setName(assetResult.getName());
                            kzItem.setRatingResponse(assetResult.getRatingResponse());
                            kzItem.setUserAssetPref(assetResult.getUserAssetPref());
                            kzItem.setFormat(assetResult.getFormat());
                            kzItem.setProvider(assetResult.getProvider());
                            kzItem.setLanguage(assetResult.getLanguage());
                            kzItem.setModificationDate(assetResult.getModificationDate());
                            kzItem.setHighlight(assetResult.getHighlight());
                        }

                        if (KZSupportedFormat.from(kzItem.getFormat()) == KZSupportedFormat.PDF) {

                            if (assetResult != null) {
                                kzItem.setExternalUrl(InsightsUtils.getKzPDFUrl(assetResult));
                            }

                        } else if (KZSupportedFormat.from(kzItem.getFormat()) == KZSupportedFormat.MP4) {

                            if (assetResult != null) {
                                kzItem.setExternalUrl(assetResult.getExternalUrl());
                            }

                            if (assetResult != null && assetResult.getSubtitleEntity() != null && !assetResult.getSubtitleEntity().isEmpty()) {

                                String appLanguage = PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).getLanguage();
                                LanguageEnum selectedLanguage = LanguageEnum.from(appLanguage);

                                KZAssetResult.SubtitleEntity selectedSubtitleEntity = null;
                                KZAssetResult.SubtitleEntity enSubtitleEntity = null;

                                for (KZAssetResult.SubtitleEntity subtitleEntity : assetResult.getSubtitleEntity()) {

                                    if (selectedLanguage.getKzLanguageCode().equalsIgnoreCase(subtitleEntity.getLanguageEntity().getLangAnsi())) {
                                        selectedSubtitleEntity = subtitleEntity;
                                    }

                                    if (LanguageEnum.EN.getKzLanguageCode().equalsIgnoreCase(subtitleEntity.getLanguageEntity().getLangAnsi())) {
                                        enSubtitleEntity = subtitleEntity;
                                    }
                                }

                                if (selectedSubtitleEntity == null) {
                                    selectedSubtitleEntity = enSubtitleEntity;
                                }

                                if (selectedSubtitleEntity == null) {
                                    selectedSubtitleEntity = assetResult.getSubtitleEntity().get(0);
                                }

                                kzItem.setSubtitleUrl(selectedSubtitleEntity.getPreSignedUrl());
                            }
                        }

                        return kzItem;
                    }
                });


    }

    @Override
    public void detachView() {

        if (subscription != null) {
            subscription.unsubscribe();
        }
    }

}
