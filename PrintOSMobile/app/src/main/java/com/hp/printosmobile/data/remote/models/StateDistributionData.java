package com.hp.printosmobile.data.remote.models;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class StateDistributionData {

    @JsonProperty("reportedStates")
    private List<ReportedState> reportedStates;
    @JsonProperty("statesAggregation")
    private Map<String, Float> statesAggregation;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<>();

    @JsonProperty("reportedStates")
    public List<ReportedState> getReportedStates() {
        return reportedStates;
    }

    @JsonProperty("reportedStates")
    public void setReportedStates(List<ReportedState> reportedStates) {
        this.reportedStates = reportedStates;
    }

    @JsonProperty("statesAggregation")
    public Map<String, Float> getStatesAggregation() {
        return statesAggregation;
    }

    @JsonProperty("statesAggregation")
    public void setStatesAggregation(Map<String, Float> statesAggregation) {
        this.statesAggregation = statesAggregation;
    }


    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class ReportedState {

        @JsonProperty("endTime")
        private String endTime;
        @JsonProperty("state")
        private String state;
        @JsonProperty("timestamp")
        private String timestamp;
        @JsonIgnore
        private Map<String, Object> additionalProperties = new HashMap<>();

        @JsonProperty("endTime")
        public String getEndTime() {
            return endTime;
        }

        @JsonProperty("endTime")
        public void setEndTime(String endTime) {
            this.endTime = endTime;
        }

        @JsonProperty("state")
        public String getState() {
            return state;
        }

        @JsonProperty("state")
        public void setState(String state) {
            this.state = state;
        }

        @JsonProperty("timestamp")
        public String getTimestamp() {
            return timestamp;
        }

        @JsonProperty("timestamp")
        public void setTimestamp(String timestamp) {
            this.timestamp = timestamp;
        }


        @JsonAnyGetter
        public Map<String, Object> getAdditionalProperties() {
            return this.additionalProperties;
        }

        @JsonAnySetter
        public void setAdditionalProperty(String name, Object value) {
            this.additionalProperties.put(name, value);
        }
    }
}
