package com.hp.printosmobile.presentation.modules.invites;

import android.content.Context;
import android.graphics.Point;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.hp.printosmobile.Analytics;
import com.hp.printosmobile.AppseeSdk;
import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.presentation.BaseFragment;
import com.hp.printosmobile.presentation.modules.drawer.OrganizationViewModel;
import com.hp.printosmobile.presentation.modules.home.HomePresenter;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitEnum;
import com.hp.printosmobile.presentation.modules.tooltip.InviteTooltip;
import com.hp.printosmobile.utils.HPUIUtils;
import com.hp.printosmobilelib.ui.common.HPFragment;
import com.hp.printosmobilelib.ui.widgets.HPViewPager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;

/**
 * create by anwar asbah 10/31/2017
 */
public class InvitesFragment extends BaseFragment implements InvitesSettingsFragment.InviteSettingsFragmentCallback, TabLayout.OnTabSelectedListener {

    private static final long HIDE_DELAY = 200;
    private static int numberOfEntries = 0;

    @Bind(R.id.main_layout)
    View mainLayout;
    @Bind(R.id.invites_pager_container)
    View invitesPagerContainer;
    @Bind(R.id.invites_tabs)
    TabLayout invitesTabs;
    @Bind(R.id.invites_pages)
    HPViewPager invitesPages;
    @Bind(R.id.loading_indicator)
    View loadingIndicator;
    @Bind(R.id.msg_text_view)
    TextView msgTextView;

    InvitesPagerAdapter adapter;
    HPFragment subFragment;
    InvitesFragmentCallback callback;

    InviteTooltip tooltip;

    public static InvitesFragment newInstance(InvitesFragmentCallback callback) {
        InvitesFragment fragment = new InvitesFragment();
        fragment.callback = callback;
        return fragment;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Analytics.sendEvent(Analytics.SHOW_INVITE_SCREEN_ACTION);
        AppseeSdk.getInstance(getActivity()).startScreen(AppseeSdk.SCREEN_INVITES);

        if (!PrintOSPreferences.getInstance(getActivity()).isCurrentUserMemberInOrganization()) {
            msgTextView.setText(R.string.invites_message_user_not_member_in_org);
            msgTextView.setVisibility(View.VISIBLE);
            invitesPagerContainer.setVisibility(View.GONE);
            loadingIndicator.setVisibility(View.GONE);
            return;
        }

        if (callback != null) {
            callback.requestContactPermissions(this);
        }

        setNumberOfEntries(getNumberOfEntries() + 1);
    }

    public void init(boolean permissionGranted) {
        if (permissionGranted) {
            new RetrieveContactsAsync().execute("");
        } else {
            setupViewPager();
        }
    }

    private void setupViewPager() {
        if (getActivity() == null || !isAdded()) {
            return;
        }
        loadingIndicator.setVisibility(View.GONE);
        invitesTabs.setVisibility(View.GONE);

        adapter = new InvitesPagerAdapter(getActivity().getSupportFragmentManager(),
                getActivity().getApplicationContext());

        if (InviteUtils.getContacts(InvitesListFragment.InvitesType.SUGGESTED) != null) {
            adapter.addFragment(InvitesListFragment.newInstance(
                    InvitesListFragment.InvitesType.SUGGESTED),
                    getString(R.string.fragment_invites_suggested_tab_key),
                    getString(R.string.invites_fragment_suggested_tab),
                    R.drawable.suggested,
                    R.drawable.suggested_selected);
            invitesTabs.setVisibility(View.VISIBLE);
        }
        if (InviteUtils.getContacts(InvitesListFragment.InvitesType.CONTACTS) != null) {
            adapter.addFragment(InvitesListFragment.newInstance(
                    InvitesListFragment.InvitesType.CONTACTS),
                    getString(R.string.fragment_invites_email_tab_key),
                    getString(R.string.invites_fragment_all_tab),
                    R.drawable.contacts,
                    R.drawable.contacts_selected);
            invitesTabs.setVisibility(View.VISIBLE);
        }

        adapter.addFragment(InvitesListFragment.newInstance(InvitesListFragment.InvitesType.EMAIL),
                getString(R.string.fragment_invites_all_tab_key),
                getString(R.string.invites_fragment_email_tab),
                R.drawable.email,
                R.drawable.email_selected);

        invitesPages.setOffscreenPageLimit(adapter.getFragments().size());
        invitesPages.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(invitesTabs));
        invitesPages.setScrollingEnabled(false);
        invitesPages.setAdapter(adapter);

        invitesTabs.setupWithViewPager(invitesPages);
        invitesTabs.addOnTabSelectedListener(this);

        for (int i = 0; i < invitesTabs.getTabCount(); i++) {
            TabLayout.Tab tab = invitesTabs.getTabAt(i);
            tab.setCustomView(adapter.getTabView(i));
        }

        adapter.refreshTabs(invitesTabs);
    }

    private void hideSoftKeyboard() {
        if (adapter != null) {
            List<Fragment> fragments = adapter.getFragments();
            for (Fragment fragment : fragments) {
                if (fragment instanceof InvitesListFragment) {
                    ((InvitesListFragment) fragment).hideSoftKeyboard();
                }
            }
        }
    }

    private void hideSoftKeyboardWithDelay() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                hideSoftKeyboard();
            }
        }, HIDE_DELAY);
    }

    @Override
    public void onInviteSubFragmentOpened(HPFragment subFragment) {
        if (callback != null) {
            callback.onInviteSubFragmentOpened(subFragment);
        }
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        if (adapter != null) {
            adapter.refreshTabs(invitesTabs);
        }
        hideSoftKeyboardWithDelay();
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {
        hideSoftKeyboardWithDelay();
    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {
        hideSoftKeyboardWithDelay();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_invites;
    }

    @Override
    public String getToolbarDisplayNameExtra() {
        OrganizationViewModel viewModel = PrintOSPreferences.getInstance(PrintOSApplication.getAppContext())
                .getSelectedOrganization();
        if (viewModel != null) {
            String name = viewModel.getOrganizationName();
            return name == null ? "" : name;
        }
        return "";
    }

    @Override
    public boolean isFragmentNameToBeDisplayed() {
        return true;
    }

    @Override
    public boolean isFilteringAvailable() {
        return false;
    }

    @Override
    public int getToolbarDisplayName() {
        return R.string.invites_fragment_name;
    }

    @Override
    public boolean isIntercomAccessible() {
        return false;
    }

    @Override
    public boolean onBackPressed() {
        hideSoftKeyboard();

        if (subFragment != null) {
            if (!subFragment.onBackPressed()) {
                FragmentTransaction trans = getFragmentManager()
                        .beginTransaction();
                trans.remove(subFragment);
                trans.commit();
                subFragment = null;
                if (callback != null) {
                    callback.onInviteSubFragmentOpened(this);
                }
            }
            return true;
        } else if (InviteUtils.getInviteActionSheet() != null) {
            InviteUtils.getInviteActionSheet().dismiss();
            return true;
        }

        if (adapter != null) {
            for (Fragment fragment : adapter.getFragments()) {
                if (fragment instanceof InviteUtils.InviteObservable) {
                    InviteUtils.removeObserver((InviteUtils.InviteObservable) fragment);
                }
            }
        }

        return false;
    }

    public static int getNumberOfEntries() {
        return numberOfEntries;
    }

    public static void setNumberOfEntries(int numberOfEntries) {
        InvitesFragment.numberOfEntries = numberOfEntries;
    }

    @Override
    public void onCancel() {

    }

    private static class InvitesPagerAdapter extends FragmentStatePagerAdapter {

        private final Map<String, Fragment> fragmentHashMap = new HashMap<>();
        private final List<String> fragmentKeys = new ArrayList<>();
        private final List<String> fragmentNames = new ArrayList<>();
        private final List<Integer> fragmentIcons = new ArrayList<>();
        private final List<Integer> fragmentSelectedIcons = new ArrayList<>();
        private final Context context;

        public InvitesPagerAdapter(FragmentManager fm, Context context) {
            super(fm);
            this.context = context;
        }

        public void addFragment(Fragment fragment, String key, String name, Integer iconResourceId, Integer iconResourceIdSelected) {
            fragmentHashMap.put(key, fragment);
            fragmentKeys.add(key);
            fragmentNames.add(name);
            fragmentIcons.add(iconResourceId);
            fragmentSelectedIcons.add(iconResourceIdSelected);
        }

        @Override
        public Fragment getItem(int position) {
            return fragmentHashMap.get(fragmentKeys.get(position));
        }

        @Override
        public boolean isViewFromObject(View view, Object fragment) {
            return ((Fragment) fragment).getView() == view;
        }

        /**
         * Here we can safely save a reference to the created
         * Fragment, no matter where it came from (either getItem() or
         * FragmentManager).
         */
        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            Fragment fragment = (Fragment) super.instantiateItem(container, position);

            String name = fragmentKeys.get(position);
            if (fragmentHashMap.containsKey(name)) {
                fragmentHashMap.remove(name);
            }
            fragmentHashMap.put(name, fragment);
            // save the appropriate reference depending on position
            return fragment;
        }

        @Override
        public int getCount() {
            return fragmentKeys.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return fragmentKeys.get(position);
        }

        private List<Fragment> getFragments() {
            return new ArrayList<>(fragmentHashMap.values());
        }

        View getTabView(int position) {
            // Given you have a custom layout in `res/layout/custom_tab.xml` an ImageView
            View view = LayoutInflater.from(context).inflate(R.layout.invites_custom_tab, null);

            ImageView img = view.findViewById(R.id.tab_icon);
            img.setImageResource(fragmentIcons.get(position));

            TextView title = view.findViewById(R.id.tab_title);
            title.setText(fragmentNames.get(position));

            return view;
        }

        public void setSelected(TabLayout.Tab tab, int position) {
            if (tab == null || tab.getCustomView() == null || fragmentSelectedIcons == null ||
                    position >= fragmentSelectedIcons.size()) {
                return;
            }
            View view = tab.getCustomView();
            ImageView tabIcon = view.findViewById(R.id.tab_icon);
            tabIcon.setImageResource(fragmentSelectedIcons.get(position));
        }

        public void setUnselected(TabLayout.Tab tab, int position) {
            if (tab == null || tab.getCustomView() == null || position >= fragmentIcons.size()) {
                return;
            }

            View view = tab.getCustomView();
            ImageView tabIcon = view.findViewById(R.id.tab_icon);
            tabIcon.setImageResource(fragmentIcons.get(position));
        }

        public void refreshTabs(TabLayout tabLayout) {
            if (tabLayout == null) {
                return;
            }
            for (int tabIndex = 0; tabIndex < tabLayout.getTabCount(); tabIndex++) {
                if (tabIndex == tabLayout.getSelectedTabPosition()) {
                    setSelected(tabLayout.getTabAt(tabIndex), tabIndex);
                } else {
                    setUnselected(tabLayout.getTabAt(tabIndex), tabIndex);
                }
            }
        }
    }

    @Override
    public int getCustomToolbarMenu() {
        super.getCustomToolbarMenu();
        return R.layout.invites_custom_menu;
    }

    @Override
    public void setCustomMenuView(final View container) {
        super.setCustomMenuView(container);
        View settingsView = container.findViewById(R.id.invites_settings_button);
        final View tooltipView = container.findViewById(R.id.info_tooltip_button);

        if (!PrintOSPreferences.getInstance(getActivity()).isCurrentUserMemberInOrganization()) {
            settingsView.setVisibility(View.GONE);
            tooltipView.setVisibility(View.GONE);
            return;
        }

        settingsView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideSoftKeyboard();
                onSettingsClicked();
            }
        });

        boolean hasCoins = PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).isInvitesCoinEnabled();

        tooltipView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Context context = PrintOSApplication.getAppContext();

                Analytics.sendEvent(Analytics.INVITE_OPEN_TOOLTIP);

                if (tooltip == null) {
                    tooltip = new InviteTooltip(context);
                }

                tooltip.updateView(context.getString(R.string.invites_disclaimer_text,
                        PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).getBeatCoinsPerInvite()));

                WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
                Display display = windowManager.getDefaultDisplay();
                Point size = new Point();
                display.getSize(size);

                int[] location = new int[2];
                tooltipView.getLocationOnScreen(location);

                int marginRight = size.x - location[0] - tooltipView.getWidth() / 2;
                tooltip.showToolTipFullWidth(tooltipView, HPUIUtils.dpToPx(context, 15),
                        HPUIUtils.dpToPx(context, 25), marginRight);
            }
        });

        boolean isLatex = HomePresenter.getSelectedBusinessUnit() != null &&
                HomePresenter.getSelectedBusinessUnit().getBusinessUnit() == BusinessUnitEnum.LATEX_PRINTER;

        tooltipView.setVisibility(hasCoins && !isLatex ? View.VISIBLE : View.GONE);
    }

    private void onSettingsClicked() {

        FragmentTransaction trans = getFragmentManager()
                .beginTransaction();

        subFragment = InvitesSettingsFragment.newInstance(this);

        trans.replace(R.id.main_layout, subFragment);
        trans.commit();

        if (callback != null) {
            callback.onInviteSubFragmentOpened(subFragment);
        }


        Analytics.sendEvent(Analytics.VIEW_SCREEN_EVENT, Analytics.INVITES_SETTINGS_SCREEN_LABEL);
    }

    private class RetrieveContactsAsync extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... args) {
            InviteUtils.retrieveContacts(getActivity());
            return "";
        }

        @Override
        protected void onPostExecute(String param) {
            super.onPostExecute(param);
            setupViewPager();
        }
    }

    public interface InvitesFragmentCallback {
        void onInviteSubFragmentOpened(HPFragment subFragment);

        void requestContactPermissions(InvitesFragment invitesFragment);
    }
}
