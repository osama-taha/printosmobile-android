package com.hp.printosmobile.presentation.modules.reportkpibreakdown;

import android.os.Build;
import android.support.annotation.NonNull;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * Created by Anwar Asbah on 1/30/2018.
 */
public class KpiBreakdownViewModel implements Serializable, Comparable<KpiBreakdownViewModel> {

    private KpiBreakdownEnum kpiBreakdownEnum;
    private int maxScore = 100;
    private boolean isOverall = false;
    private String minimumPaperType;
    private List<Event> eventList;
    private List<KpiBreakdownViewModel> aggregateModels;

    public static final Comparator<KpiBreakdownViewModel> KPI_BREAKDOWN_VIEW_MODEL_COMPARATOR = new Comparator<KpiBreakdownViewModel>() {
        @Override
        public int compare(KpiBreakdownViewModel modelL, KpiBreakdownViewModel modelR) {
            if (modelL == null) {
                if (modelR == null) return 0;
                return 1;
            } else if (modelR == null) {
                return -1;
            }
            return modelL.compareTo(modelR);
        }
    };

    public KpiBreakdownEnum getKpiBreakdownEnum() {
        return kpiBreakdownEnum;
    }

    public void setKpiBreakdownEnum(KpiBreakdownEnum kpiBreakdownEnum) {
        this.kpiBreakdownEnum = kpiBreakdownEnum;
    }

    public List<Event> getEventList() {
        return eventList;
    }

    public void setEventList(List<Event> eventList) {
        this.eventList = eventList;
    }

    public boolean isOverall() {
        return isOverall;
    }

    public void setOverall(boolean overall) {
        isOverall = overall;
    }

    public int getMaxScore() {
        return maxScore;
    }

    public void setMaxScore(int maxScore) {
        this.maxScore = maxScore;
    }

    public List<KpiBreakdownViewModel> getAggregateModels() {
        return aggregateModels;
    }

    public void setAggregateModels(List<KpiBreakdownViewModel> aggregateModels) {
        this.aggregateModels = aggregateModels;
    }

    public String getMinimumPaperType() {
        return minimumPaperType;
    }

    public void setMinimumPaperType(String minimumPaperType) {
        this.minimumPaperType = minimumPaperType;
    }

    @Override
    public int compareTo(@NonNull KpiBreakdownViewModel kpiBreakdownViewModel) {
        KpiBreakdownEnum kpiBreakdownEnumL = getKpiBreakdownEnum();
        KpiBreakdownEnum kpiBreakdownEnumR = kpiBreakdownViewModel.getKpiBreakdownEnum();

        if (kpiBreakdownEnumL == null) {
            if (kpiBreakdownEnumR == null) {
                return 0;
            }
            return 1;
        } else if (kpiBreakdownEnumR == null) {
            return -1;
        }

        return kpiBreakdownEnumL.getSortOrder() - kpiBreakdownEnumR.getSortOrder();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @Override
    public int hashCode() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            return Objects.hash(kpiBreakdownEnum, maxScore, isOverall, minimumPaperType, eventList, aggregateModels);
        }
        return 0;
    }

    public static class Event implements Comparable<Event>, Serializable {
        public static final Comparator<Event> DATE_COMPARATOR = new Comparator<Event>() {
            @Override
            public int compare(Event eL, Event eR) {
                if (eL == null) {
                    if (eR == null) {
                        return 0;
                    }
                    return 1;
                }
                if (eR == null) {
                    return -1;
                }
                return eL.compareTo(eR);
            }
        };

        private float dayValue;
        private int rowDayValue;
        private float lastWeekValue;
        private Date date;

        public float getDayValue() {
            return dayValue;
        }

        public void setDayValue(float dayValue) {
            this.dayValue = dayValue;
        }

        public float getLastWeekValue() {
            return lastWeekValue;
        }

        public void setLastWeekValue(float lastWeekValue) {
            this.lastWeekValue = lastWeekValue;
        }

        public Date getDate() {
            return date;
        }

        public void setDate(Date date) {
            this.date = date;
        }

        public int getRowDayValue() {
            return rowDayValue;
        }

        public void setRowDayValue(int rowDayValue) {
            this.rowDayValue = rowDayValue;
        }

        @Override
        public int compareTo(@NonNull Event event) {
            Date dateL = getDate();
            Date dateR = event.getDate();
            if (dateL == null) {
                if (dateR == null) {
                    return 0;
                }
                return -1;
            }
            if (dateR == null) {
                return 1;
            }
            return dateL.compareTo(dateR);
        }

        @Override
        public boolean equals(Object obj) {
            return super.equals(obj);
        }

        @Override
        public int hashCode() {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                return Objects.hash(dayValue, rowDayValue, lastWeekValue, date);
            }
            return 0;
        }
    }
}
