package com.hp.printosmobile.presentation.modules.today;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.SystemClock;
import android.support.v4.content.res.ResourcesCompat;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.webkit.JavascriptInterface;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.mikephil.charting.matrix.Vector3;
import com.hp.printosmobile.Analytics;
import com.hp.printosmobile.AnswersSdk;
import com.hp.printosmobile.Constants;
import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.data.remote.models.PreferencesData;
import com.hp.printosmobile.presentation.modules.home.HomePresenter;
import com.hp.printosmobile.presentation.modules.home.Panel;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitEnum;
import com.hp.printosmobile.presentation.modules.shared.DeepLinkUtils;
import com.hp.printosmobile.presentation.modules.shared.PanelShareCallbacks;
import com.hp.printosmobile.presentation.modules.today.histogrambreakdown.HistogramBreakDownRootFragment.HistogramResolution;
import com.hp.printosmobile.presentation.modules.todayhistogram.TodayHistogramView;
import com.hp.printosmobile.presentation.modules.todayhistogram.TodayHistogramViewModel;
import com.hp.printosmobile.utils.CustomTypefaceSpan;
import com.hp.printosmobile.utils.FileUtils;
import com.hp.printosmobile.utils.HPLocaleUtils;
import com.hp.printosmobilelib.ui.utils.SpannableStringUtils;
import com.hp.printosmobilelib.ui.widgets.TypefaceManager;
import com.hp.printosmobilelib.ui.widgets.panel.PanelView;
import com.hp.printosmobilelib.ui.widgets.panel.SharableObject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Anwar Asbah on 9/6/2017.
 */
public class HistogramPanel extends PanelView<TodayHistogramViewModel> implements SharableObject {

    public static final String TAG = HistogramPanel.class.getSimpleName();
    public static final String BREAKDOWN_SHARE_ACTION = "daily print volume breakdown %s";
    private static final String WEB_VIEW_INTERFACE_NAME = "app";

    @Bind(R.id.view_button_separator)
    View buttonSeparator;
    @Bind(R.id.histogram_panel_content)
    View histogramPanelContent;
    @Bind(R.id.histogram_view)
    TodayHistogramView histogram;
    @Bind(R.id.histogram_web_view)
    WebView histogramWebView;
    @Bind(R.id.empty_data_set_text_view)
    TextView emptyDataSetTextView;
    @Bind(R.id.button_rotate_for_detailed_view)
    View button;
    @Bind(R.id.histogram_web_view_container)
    View histogramWebViewContainer;
    @Bind(R.id.tooltip_title)
    TextView tooltipTitle;
    @Bind(R.id.tooltip_date)
    TextView tooltipDate;
    @Bind(R.id.tooltip_layout)
    LinearLayout tooltipLayout;
    @Bind(R.id.tv_legend)
    TextView tvLegend;
    @Bind(R.id.web_view_container)
    LinearLayout webViewContainer;

    private HistogramPanelCallback callbacks;
    private TodayHistogramViewModel.DetailTypeEnum breakdownType = TodayHistogramViewModel.DetailTypeEnum.MAIN;
    private boolean showBreakDown;
    private boolean isBreakDownPanel;
    private HistogramResolution resolution = HistogramResolution.DAILY;
    private long lastAnalyticsEventSentTime;

    public HistogramPanel(Context context) {
        super(context);
    }

    public HistogramPanel(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public HistogramPanel(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void bindViews() {
        ButterKnife.bind(this, getView());
        initView();
    }

    private void initView() {
        histogramPanelContent.setVisibility(GONE);

        if (isBreakDownPanel) {
            histogramWebView.setVisibility(INVISIBLE);
            showLoading();
        }

        histogramWebView.addJavascriptInterface(new WebViewJavaScriptInterface(), WEB_VIEW_INTERFACE_NAME);
        histogramWebView.setOnTouchListener(new WebViewCustomeTouchListener(getContext()) {

            private static final float MAX_DISTANCE = 20;

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (isLoading()) {
                    return true;
                }

                if (!isBreakDownPanel && getViewModel() != null && !getViewModel().isShiftSupport()
                        && getViewModel().getBusinessUnit() == BusinessUnitEnum.INDIGO_PRESS) {
                    int action = event.getAction();
                    switch (action) {
                        case MotionEvent.ACTION_DOWN:
                            initialY = event.getY();
                            initialX = event.getX();
                            break;
                        case MotionEvent.ACTION_CANCEL:
                        case MotionEvent.ACTION_UP:
                            float deltaX = event.getX() - initialX;
                            float deltaY = event.getY() - initialY;

                            Vector3 vector3 = new Vector3(deltaX, deltaY, 0);
                            float moveDistancePx = vector3.length();

                            if (moveDistancePx < MAX_DISTANCE) {
                                onHistogramWebViewClicked();
                            }

                            break;
                        default:
                            break;
                    }
                    return true;
                }

                return super.onTouch(v, event);
            }
        });

        histogramWebView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                hideLoading();
                histogramWebView.setVisibility(VISIBLE);
            }

            @Override
            public void onReceivedHttpError(WebView view, WebResourceRequest request, WebResourceResponse errorResponse) {
                super.onReceivedHttpError(view, request, errorResponse);
                hideLoading();
                histogramWebView.setVisibility(INVISIBLE);
            }
        });
    }

    @OnClick(R.id.histogram_web_view)
    public void onHistogramWebViewClicked() {
        TodayHistogramViewModel viewModel = getViewModel();
        if (viewModel != null && viewModel.getBusinessUnit() == BusinessUnitEnum.INDIGO_PRESS &&
                callbacks != null) {
            callbacks.onHistogramWebViewClicked(getViewModel());
        }
    }

    public void setBreakdownType(TodayHistogramViewModel.DetailTypeEnum type, boolean showBreakDown,
                                 boolean isBreakDownPanel) {
        breakdownType = type;
        this.showBreakDown = showBreakDown;
        this.isBreakDownPanel = isBreakDownPanel;
    }

    public void setBreakdownResolution (HistogramResolution resolution) {
        this.resolution = resolution;
    }

    public void onFilterSelected() {
        onRefresh();
    }

    @Override
    public Spannable getTitleSpannable() {
        boolean isGraphEnable = PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).isHistogramGraphEnabled();
        return isGraphEnable ? HistogramGraphUtils.getHistogramTitle(getContext(), getViewModel(), breakdownType, resolution, isBreakDownPanel, true) :
                new SpannableString(getHistogramCardTitle());
    }

    private CharSequence getHistogramCardTitle() {
        if (getViewModel() == null) {
            return getContext().getString(R.string.today_panel_title_last_7_days_view);
        }

        boolean isShiftSupport = getViewModel().isShiftSupport();
        if (getViewModel().getBusinessUnit() == BusinessUnitEnum.INDIGO_PRESS) {
            return getContext().getString(isShiftSupport ? R.string.today_panel_last_shifts : R.string.today_panel_last_days);
        } else {
            PreferencesData.UnitSystem unitSystem = PrintOSPreferences.getInstance(getContext()).getUnitSystem();
            if (unitSystem == PreferencesData.UnitSystem.Metric) {
                return getContext().getString(isShiftSupport ? R.string.today_panel_last_shifts_sqm : R.string.today_panel_last_days_sqm);
            } else {
                return getContext().getString(isShiftSupport ?
                        R.string.today_panel_last_shifts_ft_square : R.string.today_panel_last_days_ft_square);
            }
        }

    }

    @Override
    public int getContentView() {
        return R.layout.histogram_panel_content;
    }

    @Override
    public void updateViewModel(final TodayHistogramViewModel viewModel) {

        setViewModel(viewModel);
        setTitle(getTitleSpannable());

        if (showEmptyCard()) {
            DeepLinkUtils.respondToIntentForPanel(this, callbacks, true);
            return;
        }

        emptyDataSetTextView.setVisibility(View.INVISIBLE);
        histogramPanelContent.setVisibility(VISIBLE);

        boolean isShift = getViewModel() != null && getViewModel().isShiftSupport();
        if (!isShift && (isBreakDownPanel || (getViewModel() != null && getViewModel().getBusinessUnit() != BusinessUnitEnum.INDIGO_PRESS))) {
            showTooltip();
        } else {
            tooltipLayout.setVisibility(GONE);
            tvLegend.setVisibility(GONE);
        }

        boolean isGraphEnable = PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).isHistogramGraphEnabled();
        if (isGraphEnable) {
            histogramWebViewContainer.setVisibility(VISIBLE);
            histogramWebView.getViewTreeObserver().
                    addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                        @Override
                        public void onGlobalLayout() {
                            histogramWebView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                            HistogramGraphUtils.displayGraph(PrintOSApplication.getAppContext(),
                                    histogramWebView,
                                    viewModel,
                                    breakdownType,
                                    resolution,
                                    showBreakDown,
                                    isBreakDownPanel || (getViewModel() != null && getViewModel().getBusinessUnit() != BusinessUnitEnum.INDIGO_PRESS));
                        }
                    });
        } else {
            int daysSpan = getContext().getResources().getInteger(R.integer.today_panel_histogram_span);
            histogram.setDaysSpan(daysSpan);
            histogram.setPaging(false);
            histogram.setOffset(0);

            histogram.setViewModel(viewModel);
        }

        DeepLinkUtils.respondToIntentForPanel(this, callbacks, false);
    }

    @Override
    protected boolean hasSharingButton() {
        super.hasSharingButton();
        return true;
    }

    @Override
    protected void onShareClicked() {
        super.onShareClicked();
        if (callbacks != null) {
            if (isBreakDownPanel) {
                callbacks.onShareButtonClicked(this);
            } else {
                callbacks.onShareButtonClicked(Panel.HISTOGRAM);
            }
        }
    }

    @Override
    public void sharePanel() {
        if (callbacks != null) {
            lockShareButton(true);

            shareButton.setVisibility(GONE);
            buttonSeparator.setVisibility(GONE);
            Bitmap panelBitmap = FileUtils.getScreenShot(this);
            buttonSeparator.setVisibility(VISIBLE);
            shareButton.setVisibility(VISIBLE);

            if (panelBitmap != null) {

                final String title = getShareTitle();
                final Uri storageUri = FileUtils.store(getContext(), panelBitmap, PANEL_SCREEN_SHOT_FILE_NAME);

                String resolutionTag = resolution == null ? null : resolution.name();

                DeepLinkUtils.getShareBody(getContext(),
                        isBreakDownPanel ? Constants.UNIVERSAL_LINK_SCREEN_HISTOGRAM_BREAKDOWN
                                : Constants.UNIVERSAL_LINK_SCREEN_HOME,
                        getPanelTag(),
                        HomePresenter.isShiftSupport(), resolutionTag, false, new DeepLinkUtils.BranchIOCallback() {
                            @Override
                            public void onLinkCreated(String link) {
                                callbacks.onScreenshotCreated(Panel.HISTOGRAM, storageUri,
                                        title,
                                        link,
                                        HistogramPanel.this);
                                lockShareButton(false);
                            }
                        });
            }
        }
    }

    private String getShareTitle () {
        boolean isShift = getViewModel() != null && getViewModel().isShiftSupport();
        boolean isHistogramGraphEnabled = PrintOSPreferences.getInstance(PrintOSApplication.getAppContext())
                .isHistogramGraphEnabled();
        boolean isGraphEnable = PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).isHistogramGraphEnabled();

        int days = isShift && isHistogramGraphEnabled ?
                PrintOSPreferences.getInstance(getContext()).getHistogramNumberOfShifts() :
                1 + getResources().getInteger(R.integer.today_panel_histogram_span);

        String title;
        if (isBreakDownPanel) {
            title = getContext().getString(
                    resolution == HistogramResolution.MONTHLY ? R.string.share_caption_title_last_months_breakdown
                            : resolution == HistogramResolution.WEEKLY ? R.string.share_caption_title_last_week_breakdown
                            : R.string.share_caption_title_last_days_breakdown,
                    HistogramGraphUtils.getHistogramTitle(getContext(), getViewModel(), breakdownType, resolution, true, false));
        } else {
            if (isShift) {
                title = getContext().getString(R.string.share_histogram_panel_title_shift, days);
            } else if (isGraphEnable) {
                title = getContext().getString(R.string.share_caption_title_last_days_breakdown,
                        HistogramGraphUtils.getHistogramTitle(getContext(), getViewModel(), TodayHistogramViewModel.DetailTypeEnum.MAIN, resolution, true, false));
            } else {
                title = getContext().getString(R.string.share_histogram_panel_title, days);
            }
        }

        return title;
    }

    @Override
    protected boolean isValidViewModel() {
        return getViewModel() != null;
    }

    @Override
    protected String getEmptyCardText() {
        return getContext().getString(R.string.no_information_to_display);
    }

    public void addHistogramPanelCallback(HistogramPanelCallback callbacks) {
        this.callbacks = callbacks;
    }

    @OnClick(R.id.button_rotate_for_detailed_view)
    void onViewAllButtonClicked() {
        if (callbacks != null) {
            callbacks.onHistogramViewAllClicked(getViewModel());
        }
    }

    @Override
    public String getShareAction() {
        if (getViewModel() == null) {
            return "";
        }
        boolean isGraphEnable = PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).isHistogramGraphEnabled();

        if (getViewModel().isShiftSupport()) {
            return AnswersSdk.SHARE_CONTENT_TYPE_LAST_SHIFTS;
        } else {
            if (!isGraphEnable) {
                return AnswersSdk.SHARE_CONTENT_TYPE_LAST_DAYS;
            } else if (!isBreakDownPanel) {
                return AnswersSdk.SHARE_DAILY_PRINT_VOLUME;
            } else {
                return String.format(AnswersSdk.SHARE_DAILY_PRINT_VOLUME_BREAKDOWN,
                        HistogramGraphUtils.getHistogramAnalyticsTitle(getContext(), getViewModel(),
                                breakdownType));
            }
        }
    }

    public void setHistogramWebViewHeight(final int height) {
        ViewGroup.LayoutParams webViewlayoutParams = webViewContainer.getLayoutParams();
        webViewlayoutParams.height = height;
        webViewContainer.setLayoutParams(webViewlayoutParams);

        requestLayout();
    }

    public TodayHistogramViewModel.DetailTypeEnum getBreakdownType() {
        return breakdownType;
    }

    public HistogramResolution getHistogramResolution () {
        return resolution;
    }

    @Override
    public String getPanelTag() {
        return isBreakDownPanel ? breakdownType.getKey() : Panel.HISTOGRAM.getDeepLinkTag();
    }

    public void showTooltip() {
        tooltipLayout.setVisibility(VISIBLE);
    }

    public interface HistogramPanelCallback extends PanelShareCallbacks, DeepLinkUtils.DeepLinkCallbacks {
        void onHistogramViewAllClicked(TodayHistogramViewModel model);

        void onHistogramWebViewClicked(TodayHistogramViewModel model);

        void onShareButtonClicked(PanelView panelView);
    }

    private class WebViewJavaScriptInterface {

        public WebViewJavaScriptInterface() {

        }

        @JavascriptInterface
        public void tooltipChanged(final String tooltipDate, final String value, final String change, final String isChangeHigher, final String breakdownString) {

            if (getContext() instanceof Activity) {

                ((Activity) getContext()).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        updateTooltipText(tooltipDate, value, change, isChangeHigher);
                        final long currentTime = SystemClock.elapsedRealtime();

                        if (getViewModel() != null && currentTime - lastAnalyticsEventSentTime > Constants.SENDING_ANALYTICS_EVENT_DELAY) {
                            Analytics.sendEvent(String.format(Analytics.LAST_DAYS_TOOLTIP_GRAPH_IS_CLICKED, HistogramGraphUtils.getHistogramAnalyticsTitle(getContext(), getViewModel(), breakdownType), getViewModel().getBusinessUnit().getShortName()).toLowerCase());
                            lastAnalyticsEventSentTime = currentTime;
                        }
                    }
                });
            }
        }

        @JavascriptInterface
        public void graphLegend(final String legend) {

            if (getContext() instanceof Activity) {

                ((Activity) getContext()).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        //spannable for graph legend
                        if (!TextUtils.isEmpty(legend)) {
                            SpannableStringUtils spannableLegend = new SpannableStringUtils(legend);
                            spannableLegend.setSpannable(new CustomTypefaceSpan("", TypefaceManager.getTypeface(PrintOSApplication.getAppContext(),
                                    TypefaceManager.HPTypeface.HP_REGULAR)), (int) getResources().getDimension(R.dimen.legend_font), ResourcesCompat.getColor(getResources(), R.color.c62, null), 0, legend.length());

                            tvLegend.setVisibility(VISIBLE);
                            tvLegend.setText(spannableLegend.getSpannableString());
                        }
                    }
                });
            }
        }
    }

    private void updateTooltipText(String tooltipDate, String value, String change, String isHigher) {

        change = HPLocaleUtils.getDecimalString(Math.abs(Float.parseFloat(change)), true, 1);

        String arrow = String.valueOf(Boolean.parseBoolean(isHigher) ? Html.fromHtml(getResources().getString(R.string.tooltip_change_higher)) : Html.fromHtml(getResources().getString(R.string.tooltip_change_lower)));
        String changeWithArrow = arrow + change + "%";
        int color = Boolean.parseBoolean(isHigher) ? R.color.bar_green : R.color.bar_red;

        SpannableStringUtils spannableValue = new SpannableStringUtils(value);
        spannableValue.setSpannable(new CustomTypefaceSpan("", TypefaceManager.getTypeface(PrintOSApplication.getAppContext(),
                TypefaceManager.HPTypeface.HP_BOLD)), (int) getResources().getDimension(R.dimen.tooltip_title_font), ResourcesCompat.getColor(getResources(), R.color.tooltip_title_color, null), 0, value.length());
        SpannableString spannableValueString = spannableValue.getSpannableString();

        SpannableStringUtils spannableChange = new SpannableStringUtils(changeWithArrow);
        spannableChange.setSpannable(new CustomTypefaceSpan("", TypefaceManager.getTypeface(PrintOSApplication.getAppContext(),
                TypefaceManager.HPTypeface.HP_BOLD)), (int) getResources().getDimension(R.dimen.tooltip_change_font), ResourcesCompat.getColor(getResources(), color, null), 0, changeWithArrow.length());

        tooltipTitle.setText(TextUtils.concat(spannableValueString, " ", spannableChange.getSpannableString()));

        //spannable for tooltip date
        SpannableStringUtils spannableDate = new SpannableStringUtils(tooltipDate);
        spannableDate.setSpannable(new CustomTypefaceSpan("", TypefaceManager.getTypeface(PrintOSApplication.getAppContext(),
                TypefaceManager.HPTypeface.HP_REGULAR)), (int) getResources().getDimension(R.dimen.tooltip_title_date_font), ResourcesCompat.getColor(getResources(), R.color.tooltip_title_color, null), 0, tooltipDate.length());

        this.tooltipDate.setText(spannableDate.getSpannableString());

    }

}
