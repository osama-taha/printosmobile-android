package com.hp.printosmobile.presentation.modules.tooltip;

import android.content.Context;
import android.text.Spannable;
import android.widget.TextView;

import com.hp.printosmobile.R;

/**
 * Created by anwar asbah on 12/06/17.
 */
public class InviteTooltip extends Tooltip<String> {

    private TextView hintTextView;

    public InviteTooltip(Context context) {
        super(context);
    }

    @Override
    protected void initContentView() {
        hintTextView = contentView.findViewById(R.id.hint_text_view);
    }

    @Override
    public int getLayoutResourceId() {
        return R.layout.invite_tool_tip_layout;
    }

    @Override
    public void updateView(String tipText) {
        hintTextView.setText(tipText);
    }

    public void updateView(Spannable spannableStringBuilder) {
        hintTextView.setText(spannableStringBuilder);
    }
}
