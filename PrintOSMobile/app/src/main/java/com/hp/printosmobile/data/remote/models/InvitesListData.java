package com.hp.printosmobile.data.remote.models;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Anwar Asbah 11/6/2017
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class InvitesListData {

    @JsonProperty("invitations")
    List<Invite> inviteList;
    @JsonProperty("_links")
    Links links;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<>();

    @JsonProperty("invitations")
    public List<Invite> getInviteList() {
        return inviteList;
    }

    @JsonProperty("invitations")
    public void setInviteList(List<Invite> inviteList) {
        this.inviteList = inviteList;
    }

    @JsonProperty("_links")
    public Links getLinks() {
        return links;
    }

    @JsonProperty("_links")
    public void setLinks(Links link) {
        this.links = link;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class Invite {

        @JsonProperty("id")
        String id;
        @JsonProperty("email")
        String email;
        @JsonProperty("status")
        String status;
        @JsonProperty("_links")
        Links links;
        @JsonProperty("languageCode")
        String languageCode;
        @JsonProperty("roleId")
        String roleId;
        @JsonProperty("organizationType")
        String roleType;
        @JsonProperty("firstName")
        String firstName;
        @JsonProperty("lastName")
        String lastName;
        @JsonIgnore
        private Map<String, Object> additionalProperties = new HashMap<>();

        @JsonProperty("firstName")
        public String getFirstName() {
            return firstName;
        }

        @JsonProperty("firstName")
        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        @JsonProperty("lastName")
        public String getLastName() {
            return lastName;
        }

        @JsonProperty("lastName")
        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        @JsonProperty("organizationType")
        public String getRoleType() {
            return roleType;
        }

        @JsonProperty("organizationType")
        public void setRoleType(String roleType) {
            this.roleType = roleType;
        }

        @JsonProperty("roleId")
        public String getRoleId() {
            return roleId;
        }

        @JsonProperty("roleId")
        public void setRoleId(String roleId) {
            this.roleId = roleId;
        }

        @JsonProperty("languageCode")
        public String getLanguageCode() {
            return languageCode;
        }

        @JsonProperty("languageCode")
        public void setLanguageCode(String languageCode) {
            this.languageCode = languageCode;
        }

        @JsonProperty("email")
        public String getEmail() {
            return email;
        }

        @JsonProperty("email")
        public void setEmail(String email) {
            this.email = email;
        }

        @JsonProperty("status")
        public String getStatus() {
            return status;
        }

        @JsonProperty("status")
        public void setStatus(String status) {
            this.status = status;
        }

        @JsonProperty("id")
        public String getId() {
            return id;
        }

        @JsonProperty("id")
        public void setId(String id) {
            this.id = id;
        }

        @JsonProperty("_links")
        public Links getLinks() {
            return links;
        }

        @JsonProperty("_links")
        public void setLinks(Links links) {
            this.links = links;
        }

        @JsonAnyGetter
        public Map<String, Object> getAdditionalProperties() {
            return this.additionalProperties;
        }

        @JsonAnySetter
        public void setAdditionalProperty(String name, Object value) {
            this.additionalProperties.put(name, value);
        }
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class Links {

        @JsonProperty("new")
        Link newLinks;
        @JsonProperty("resend")
        Link resend;
        @JsonProperty("revoke")
        Link revoke;
        @JsonIgnore
        private Map<String, Object> additionalProperties = new HashMap<>();

        @JsonProperty("new")
        public Link getNewLinks() {
            return newLinks;
        }

        @JsonProperty("new")
        public void setNewLinks(Link newLinks) {
            this.newLinks = newLinks;
        }

        @JsonProperty("resend")
        public Link getResend() {
            return resend;
        }

        @JsonProperty("resend")
        public void setResend(Link resend) {
            this.resend = resend;
        }

        @JsonProperty("revoke")
        public Link getRevoke() {
            return revoke;
        }

        @JsonProperty("revoke")
        public void setRevoke(Link revoke) {
            this.revoke = revoke;
        }

        @JsonAnyGetter
        public Map<String, Object> getAdditionalProperties() {
            return this.additionalProperties;
        }

        @JsonAnySetter
        public void setAdditionalProperty(String name, Object value) {
            this.additionalProperties.put(name, value);
        }
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class Link {

        @JsonProperty("href")
        String href;
        @JsonIgnore
        private Map<String, Object> additionalProperties = new HashMap<>();

        @JsonProperty("href")
        public String getHref() {
            return href;
        }

        @JsonProperty("href")
        public void setHref(String href) {
            this.href = href;
        }

        @JsonAnyGetter
        public Map<String, Object> getAdditionalProperties() {
            return this.additionalProperties;
        }

        @JsonAnySetter
        public void setAdditionalProperty(String name, Object value) {
            this.additionalProperties.put(name, value);
        }
    }
}