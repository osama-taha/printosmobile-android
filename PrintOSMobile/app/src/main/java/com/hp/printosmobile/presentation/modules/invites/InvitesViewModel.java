package com.hp.printosmobile.presentation.modules.invites;

import java.util.List;

/**
 * Created by Anwar Asbah on 11/6/2017.
 */

public class InvitesViewModel {

    boolean canInvite;
    List<InvitedContact> invitedContacts;

    public boolean canInvite() {
        return canInvite;
    }

    public void setCanInvite(boolean canInvite) {
        this.canInvite = canInvite;
    }

    public List<InvitedContact> getInvitedContacts() {
        return invitedContacts;
    }

    public void setInvitedContacts(List<InvitedContact> invitedContacts) {
        this.invitedContacts = invitedContacts;
    }

    public static class InvitedContact {
        String inviteEmail;
        InviteContactViewModel.InviteStatus inviteStatus;
        String inviteID;
        String langaugeCode;
        String roleID;
        String roleType;
        String name;
        private boolean revokeEnabled;
        private boolean resendEnabled;

        public String getInviteEmail() {
            return inviteEmail;
        }

        public void setInviteEmail(String inviteEmail) {
            this.inviteEmail = inviteEmail;
        }

        public InviteContactViewModel.InviteStatus getInviteStatus() {
            return inviteStatus;
        }

        public void setInviteStatus(InviteContactViewModel.InviteStatus inviteStatus) {
            this.inviteStatus = inviteStatus;
        }

        public String getInviteID() {
            return inviteID;
        }

        public void setInviteID(String inviteID) {
            this.inviteID = inviteID;
        }

        public boolean isRevokeEnabled() {
            return revokeEnabled;
        }

        public void setRevokeEnabled(boolean revokeEnabled) {
            this.revokeEnabled = revokeEnabled;
        }

        public boolean isResendEnabled() {
            return resendEnabled;
        }

        public void setResendEnabled(boolean resendEnabled) {
            this.resendEnabled = resendEnabled;
        }

        public String getLangaugeCode() {
            return langaugeCode;
        }

        public void setLangaugeCode(String langaugeCode) {
            this.langaugeCode = langaugeCode;
        }

        public String getRoleID() {
            return roleID;
        }

        public void setRoleID(String roleID) {
            this.roleID = roleID;
        }

        public String getRoleType() {
            return roleType;
        }

        public void setRoleType(String roleType) {
            this.roleType = roleType;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}
