package com.hp.printosmobile.presentation.modules.insights.kz;

import com.hp.printosmobile.data.remote.models.kz.KZItem;

/**
 * Created by Osama on 4/18/18.
 */

public class KZAssetModel {

    private String externalUrl;
    private String subtitleUrl;
    private KZItem KZItem;

    public String getExternalUrl() {
        return externalUrl;
    }

    public void setExternalUrl(String externalUrl) {
        this.externalUrl = externalUrl;
    }

    public String getSubtitleUrl() {
        return subtitleUrl;
    }

    public void setSubtitleUrl(String subtitleUrl) {
        this.subtitleUrl = subtitleUrl;
    }

    public void setKZItem(KZItem KZItem) {
        this.KZItem = KZItem;
    }

    public KZItem getKZItem() {
        return KZItem;
    }
}
