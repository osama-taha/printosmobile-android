package com.hp.printosmobile.data.remote.models.kz;


import android.text.TextUtils;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Osama on 3/15/18.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class KZItem implements Serializable {

    @JsonProperty("id")
    private String id;
    @JsonProperty("name")
    private String name;
    @JsonProperty("format")
    private String format;
    @JsonProperty("language")
    private String language;
    @JsonProperty("ratingResponse")
    private RatingResponse ratingResponse;
    @JsonProperty("modificationDate")
    private String modificationDate;
    @JsonProperty("provider")
    private Provider provider;
    @JsonProperty("highlight")
    private String highlight;
    @JsonProperty("userAssetPref")
    private UserAssetPref userAssetPref;
    @JsonProperty("external_url")
    private String externalUrl;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<>();
    private String subtitleUrl;

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("format")
    public String getFormat() {
        return format;
    }

    @JsonProperty("format")
    public void setFormat(String format) {
        this.format = format;
    }

    @JsonProperty("language")
    public String getLanguage() {
        return language;
    }

    @JsonProperty("language")
    public void setLanguage(String language) {
        this.language = language;
    }

    @JsonProperty("ratingResponse")
    public RatingResponse getRatingResponse() {
        return ratingResponse;
    }

    @JsonProperty("ratingResponse")
    public void setRatingResponse(RatingResponse ratingResponse) {
        this.ratingResponse = ratingResponse;
    }

    @JsonProperty("modificationDate")
    public String getModificationDate() {
        return modificationDate;
    }

    @JsonProperty("modificationDate")
    public void setModificationDate(String modificationDate) {
        this.modificationDate = modificationDate;
    }

    @JsonProperty("provider")
    public Provider getProvider() {
        return provider;
    }

    @JsonProperty("provider")
    public void setProvider(Provider provider) {
        this.provider = provider;
    }

    @JsonProperty("highlight")
    public String getHighlight() {
        return highlight;
    }

    @JsonProperty("highlight")
    public void setHighlight(String highlight) {
        this.highlight = highlight;
    }

    @JsonProperty("userAssetPref")
    public UserAssetPref getUserAssetPref() {
        return userAssetPref;
    }

    @JsonProperty("userAssetPref")
    public void setUserAssetPref(UserAssetPref userAssetPref) {
        this.userAssetPref = userAssetPref;
    }

    @JsonProperty("external_url")
    public String getExternalUrl() {
        return externalUrl;
    }

    @JsonProperty("external_url")
    public void setExternalUrl(String externalUrl) {
        this.externalUrl = externalUrl;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public void setSubtitleUrl(String subtitleUrl) {
        this.subtitleUrl = subtitleUrl;
    }

    public String getSubtitleUrl() {
        return subtitleUrl;
    }

    public enum Provider {

        STORAGE_SERVICE,
        YOUTUBE;

        private static Map<String, Provider> valuesMap = new HashMap<>();

        static {
            valuesMap.put("storage-service", STORAGE_SERVICE);
            valuesMap.put("youtube", YOUTUBE);
        }

        @JsonCreator
        public static Provider forValue(String value) {
            if (TextUtils.isEmpty(value)) {
                return null;
            }
            return valuesMap.get(value.toLowerCase());
        }

        @JsonValue
        public String toValue() {
            for (Map.Entry<String, Provider> entry : valuesMap.entrySet()) {
                if (entry.getValue() == this)
                    return entry.getKey();
            }

            return null;
        }

    }

}