package com.hp.printosmobile.presentation.modules.settings;

import com.hp.printosmobilelib.core.communications.remote.APIException;

import java.util.List;

/**
 * Created by Anwar Asbah on 1/17/16.
 */
public interface NotificationsSubMenuView {

    void setSupportedSubscriptions(List<String> events);

    void setSubscriptions(List<SubscriptionEvent> events);

    void onFailedToGetSubscription(APIException apiExceptions);

    void onSubscribed(SubscriptionEvent subscriptionEvent);

}
