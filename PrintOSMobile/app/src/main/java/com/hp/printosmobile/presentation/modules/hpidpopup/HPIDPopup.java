package com.hp.printosmobile.presentation.modules.hpidpopup;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hp.printosmobile.Analytics;
import com.hp.printosmobile.AppseeSdk;
import com.hp.printosmobile.R;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Osama Taha
 */
public class HPIDPopup extends DialogFragment {

    public static final String TAG = HPIDPopup.class.getName();

    private HPIDPopupCallbacks callback;

    public static HPIDPopup getInstance(HPIDPopupCallbacks hpidPopupCallbacks) {

        HPIDPopup dialog = new HPIDPopup();
        dialog.callback = hpidPopupCallbacks;

        return dialog;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.PopupStyle);
        Analytics.sendEvent(Analytics.EVENT_HPID_TOOLTIP_SHOWN);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        return inflater.inflate(R.layout.hpid_popup, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        getDialog().setCanceledOnTouchOutside(true);
        AppseeSdk.getInstance(getActivity()).startScreen(AppseeSdk.HPID_POPUP);
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        }
    }

    @Override
    public void dismissAllowingStateLoss() {
        super.dismissAllowingStateLoss();
        if (callback != null) {
            callback.onDialogDismissed();
        }
    }

    @OnClick(R.id.dismiss_button)
    public void closeButtonClicked() {
        dismissAllowingStateLoss();
    }

    public interface HPIDPopupCallbacks {
        void onDialogDismissed();
    }

}
