package com.hp.printosmobile;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.InviteEvent;
import com.crashlytics.android.answers.LoginEvent;
import com.crashlytics.android.answers.ShareEvent;
import com.hp.printosmobilelib.core.logging.HPLogger;

/**
 * Created by Osama Taha on 9/6/17.
 */

public class AnswersSdk {

    private static final String TAG = AnswersSdk.class.getSimpleName();

    public static final String SHARE_CONTENT_TYPE_ADVICE = "Advice";
    public static final String SHARE_CONTENT_TYPE_ADVICE_POPUP = "Advice - Popup";
    public static final String SHARE_CORRECTIVE_ACTIONS_JAMS = "Jams corrective actions shared";
    public static final String SHARE_CORRECTIVE_ACTIONS_FAILURES = "Failures corrective actions shared";
    public static final String SHARE_CONTENT_TYPE_WEEKLY_PB_SCORE = "Weekly PB Score";
    public static final String SHARE_CONTENT_TYPE_INSIGHTS_JAMS = "Insights - Jams";
    public static final String SHARE_CONTENT_TYPE_INSIGHTS_FAILURES = "Insights - Failures";
    public static final String SHARE_CONTENT_TYPE_JOBS_TODAY = "Jobs today";
    public static final String SHARE_CONTENT_TYPE_RANKING = "PB Ranking";
    public static final String SHARE_CONTENT_TYPE_DAILY_SPOTLIGHT = "Daily spotlight";
    public static final String SHARE_CONTENT_TYPE_PRODUCTION_TODAY = "Production today";
    public static final String SHARE_CONTENT_TYPE_LAST_DAYS = "Last days";
    public static final String SHARE_CONTENT_TYPE_LAST_SHIFTS = "Last shifts";
    public static final String SHARE_DAILY_PRINT_VOLUME = "Daily Print Volume Portrait";
    public static final String SHARE_DAILY_PRINT_VOLUME_BREAKDOWN = "Daily Print Volume Breakdown %s";
    public static final String SHARE_CONTENT_TYPE_SERVICE_CALLS = "Service calls";
    public static final String SHARE_CONTENT_TYPE_KNOWLEDGE_ZONE = "Knowledge zone";

    public static final String LOGIN_METHOD_BUTTON = "Login Button";
    public static final String LOGIN_METHOD_AUTO_LOGIN = "Auto Login";
    public static final String LOGIN_METHOD_RENEW_SESSION = "Renew Session";

    public static final String INVITE_METHOD_POPUP = "Popup";
    public static final String INVITE_METHOD_MENU_ITEM = "Menu Item";
    public static final String INVITE_METHOD_INVITES_SCREEN = "Invites";

    private AnswersSdk() {
    }

    public static void logShare(String contentType) {

        if (!BuildConfig.DEBUG) {

            HPLogger.d(TAG, "Fabric analytics share event sent: " + contentType);

            Answers.getInstance().logShare(new ShareEvent()
                    .putContentType(contentType));
        }
    }

    public static void logInvite(String method) {

        if (!BuildConfig.DEBUG) {

            HPLogger.d(TAG, "Fabric analytics Invite event sent: " + method);

            Answers.getInstance().logInvite(new InviteEvent()
                    .putMethod(method));
        }
    }

    public static void logLogin(String method, boolean success) {

        if (!BuildConfig.DEBUG) {

            HPLogger.d(TAG, "Fabric analytics login event sent: " + String.valueOf(success));

            Answers.getInstance().logLogin(new LoginEvent()
                    .putMethod(method)
                    .putSuccess(success));
        }
    }

}
