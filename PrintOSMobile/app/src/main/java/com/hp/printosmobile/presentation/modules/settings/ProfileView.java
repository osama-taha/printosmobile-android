package com.hp.printosmobile.presentation.modules.settings;

import com.hp.printosmobile.presentation.MVPView;

/**
 * Created by Anwar Asbah on 10/3/17.
 */
public interface ProfileView extends MVPView {

    void onProfileImageUrlRetrieved(String url);

}
