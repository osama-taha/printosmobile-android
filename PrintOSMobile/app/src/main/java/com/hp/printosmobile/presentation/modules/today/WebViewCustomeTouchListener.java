package com.hp.printosmobile.presentation.modules.today;

import android.content.Context;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebView;

import com.github.mikephil.charting.matrix.Vector3;
import com.hp.printosmobile.utils.HPUIUtils;

/**
 * Created by anwar asbah on 12/27/2017.
 */

public class WebViewCustomeTouchListener implements WebView.OnTouchListener {

    private static final float MIN_MOVE_DISTANCE = 100;
    private static final float ACUTE_ANGLE_LIMIT = 30;
    private static final float OBTUSE_ANGLE_LIMIT = 135;
    private static final double STRAIGHT_ANGLE = 180;

    float initialY = 0;
    float initialX = 0;
    Context context;

    public WebViewCustomeTouchListener(Context context) {
        this.context = context;
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        int action = event.getAction();
        switch (action) {
            case MotionEvent.ACTION_DOWN:
                // Disallow ScrollView to intercept touch events.
                v.getParent().requestDisallowInterceptTouchEvent(true);

                initialY = event.getY();
                initialX = event.getX();

                break;

            case MotionEvent.ACTION_MOVE:
                float deltaY = event.getY() - initialY;
                float deltaX = event.getX() - initialX;
                Vector3 vector3 = new Vector3(deltaX, deltaY, 0);
                float moveDistancePx = vector3.length();
                float moveDistanceDp = HPUIUtils.pxToDp(context, (int) moveDistancePx);
                double absAngle = Math.abs((STRAIGHT_ANGLE / Math.PI * Math.atan2(deltaX, deltaY)));
                if (moveDistanceDp > MIN_MOVE_DISTANCE && (absAngle <= ACUTE_ANGLE_LIMIT
                        || absAngle >= OBTUSE_ANGLE_LIMIT)) {
                    v.getParent().requestDisallowInterceptTouchEvent(false);
                }
                break;

            case MotionEvent.ACTION_CANCEL:
            case MotionEvent.ACTION_UP:
                // Allow ScrollView to intercept touch events.
                v.getParent().requestDisallowInterceptTouchEvent(false);
                break;
            default:
                break;
        }

        // Handle ListView touch events.
        v.onTouchEvent(event);
        return true;
    }
}