package com.hp.printosmobile.presentation.modules.rankingpanel.leaderboard;

import com.hp.printosmobile.presentation.MVPView;
import com.hp.printosmobile.presentation.modules.week.RankingViewModel;

import java.util.List;

/**
 * Created by Osama Taha on 31/07/2017.
 */
interface LeaderboardListView extends MVPView {

    void onInitLeaderboardListCompleted(List<RankingViewModel.SiteRankViewModel> list);

    void onNextPage(List<RankingViewModel.SiteRankViewModel> siteRankViewModels);

    void showErrorView();

    void hideErrorView();

    void addFooterView();

    void removeFooterView();

    void showLoadingFooterView();

    void showErrorFooterView();

    void showLoadingView();

    void hideLoadingView();

    void showNoResultsFooterView();

    void onPrevPageLoaded(List<RankingViewModel.SiteRankViewModel> siteRankViewModels);
}
