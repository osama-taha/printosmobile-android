package com.hp.printosmobile.data.remote.services;

import com.hp.printosmobile.data.remote.ApiConstants;
import com.hp.printosmobile.data.remote.models.ProfileImageInfo;

import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import rx.Observable;

/**
 * Created by Anwar Asbah 10/2/2017
 */
public interface ProfileService {

    @GET(ApiConstants.PROFILE_INFO_API)
    Observable<Response<ProfileImageInfo>> getProfileInfo();

    @Multipart
    @POST(ApiConstants.PROFILE_IMAGE_API)
    Observable<Response<ResponseBody>> uploadProfileImage(
            @Part MultipartBody.Part file);

    @DELETE(ApiConstants.PROFILE_IMAGE_API)
    Observable<Response<ResponseBody>> deleteProfileImage();
}
