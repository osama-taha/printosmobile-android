package com.hp.printosmobile.utils;

import android.graphics.Typeface;
import android.os.Build;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.StyleSpan;

/**
 * Created by User on 5/15/2016.
 */
public class HPStringUtils {

    private HPStringUtils() {
    }

    public static String capitalizeWords(String string, String separator) {
        if (string == null) {
            return "";
        }

        String[] words = string.split(separator);
        if (words.length == 0) {
            return "";
        }

        String name = "";
        for (int i = 0; i < words.length; i++) {
            String word = words[i];
            if (word.length() > 0) {
                name += Character.toUpperCase(words[i].charAt(0)) + words[i].substring(1) + " ";
            }
        }
        return name;
    }

    public static String getStringBeforeRegex(String string, String regex) {
        if (string == null || regex == null) {
            return string;
        }

        return string.split(regex)[0].trim();
    }

    public static Spannable setSpannableRegexBold(String string, String regex) {
        int startDailyValueIndex = string.indexOf(regex);
        int lastDailyValueIndex = startDailyValueIndex + regex.length();
        final SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(string);

        final StyleSpan boldStyle = new StyleSpan(android.graphics.Typeface.BOLD);
        spannableStringBuilder.setSpan(boldStyle, startDailyValueIndex, lastDailyValueIndex, Spannable.SPAN_INCLUSIVE_INCLUSIVE);

        return spannableStringBuilder;
    }

    public static int getNumberOfExp(String string, String splitRegex) {
        if (string == null || splitRegex == null) {
            return 0;
        }

        return string.split(splitRegex).length;
    }

    public static String addStringIfNotFound(String query, String itemToBeAdded, String splitRegex) {
        if (itemToBeAdded == null || splitRegex == null || query == null) {
            return "";
        }

        String[] items = query.split(splitRegex);
        for (String item : items) {
            if (item.equals(itemToBeAdded)) {
                return query;
            }
        }

        if (query.length() > 0) {
            query += splitRegex;
        }

        return query + itemToBeAdded;
    }

    /**
     * Makes a substring of a string bold.
     *
     * @param text       Full text
     * @param textToBold Text you want to make bold
     * @return String with bold substring
     */
    public static SpannableStringBuilder boldPartOfAString(String text, String textToBold) {

        SpannableStringBuilder builder = new SpannableStringBuilder();

        if (textToBold.length() > 0 && !textToBold.trim().equals("")) {

            //for counting start/end indexes
            String testText = text.toLowerCase();
            String testTextToBold = textToBold.toLowerCase();
            int startingIndex = testText.indexOf(testTextToBold);
            int endingIndex = startingIndex + testTextToBold.length();

            if (startingIndex < 0 || endingIndex < 0) {
                return builder.append(text);
            } else if (startingIndex >= 0 && endingIndex >= 0) {

                builder.append(text);
                builder.setSpan(new StyleSpan(Typeface.BOLD), startingIndex, endingIndex, 0);
            }
        } else {
            return builder.append(text);
        }

        return builder;
    }

    public static String concatStringsWithSpace(String firstString, String secondString) {
        if (TextUtils.isEmpty(firstString)) {
            if (TextUtils.isEmpty(secondString)) {
                return "";
            } else {
                return secondString;
            }
        } else {
            if (TextUtils.isEmpty(secondString)) {
                return firstString;
            } else {
                return String.format("%s %s", firstString, secondString);
            }
        }
    }

    public static String getInitials(String firstString, String secondString) {
        String initials = "";

        if (!TextUtils.isEmpty(firstString)) {
            initials += firstString.charAt(0);
        }

        if (!TextUtils.isEmpty(secondString)) {
            initials += secondString.charAt(0);
        }

        return initials.toUpperCase();

    }

    public static String replaceLastWord(String string, String word) {
        return string.trim().replaceAll("\\b(\\w+)$", word);
    }

    public static String getHighlightedText(String highlight, String searchQuery) {

        String[] words = searchQuery.replaceAll("[^a-zA-Z0-9]", " ").split(" ");
        if (words != null) {
            for (int i = 0; i < words.length; i++) {
                String word = words[i];
                if (word.trim().length() > 0) {
                    highlight = highlight.replaceAll("(?i)(" + word + ")", "<b>$1</b>");
                }
            }
        }

        return highlight;
    }

    public static Spanned getSpannedText(String string) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Html.fromHtml(string, Html.FROM_HTML_MODE_COMPACT);
        } else {
            return Html.fromHtml(string);
        }
    }
}
