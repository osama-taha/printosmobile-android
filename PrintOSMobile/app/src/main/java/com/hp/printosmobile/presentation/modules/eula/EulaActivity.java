package com.hp.printosmobile.presentation.modules.eula;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import com.hp.printosmobile.Analytics;
import com.hp.printosmobile.Constants;
import com.hp.printosmobile.IntercomSdk;
import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.presentation.BaseActivity;
import com.hp.printosmobile.utils.AppUtils;
import com.hp.printosmobile.utils.HPUIUtils;
import com.hp.printosmobilelib.core.communications.remote.APIException;

import butterknife.Bind;
import butterknife.OnClick;

public class EulaActivity extends BaseActivity implements EulaView {

    public static final String TAG = EulaActivity.class.getName();
    public static final String TAG_ACCEPT = EulaActivity.class.getName() + "_ACCEPT";

    @Bind(R.id.content_view)
    View contentView;
    @Bind(R.id.eula_web_view)
    WebView eulaWebView;
    @Bind(R.id.buttons_view)
    View buttonsView;
    @Bind(R.id.progress_bar)
    View loadingView;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.toolbar_display_title)
    TextView toolbarDisplayName;

    boolean displayButtons;

    private EulaPresenter presenter;

    private EULAViewModel eulaViewModel;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_eula;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        IntercomSdk.getInstance(this).logEvent(IntercomSdk.PBM_VIEW_PRIVACY_EVENT);
        Analytics.sendEvent(Analytics.EULA_SHOWN_ACTION);

        displayButtons = false;

        Bundle bundle = getIntent().getExtras();
        if (bundle != null && bundle.containsKey(Constants.IntentExtras.EULA_ACTIVITY_TO_ACCEPT_TERMS)) {
            displayButtons = bundle.getBoolean(Constants.IntentExtras.EULA_ACTIVITY_TO_ACCEPT_TERMS, false);
        }

        initPresenter();

        toolbarDisplayName.setText(getString(R.string.eula_title));
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(ResourcesCompat.getDrawable(getResources(), R.drawable.back_button, null));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    private void initPresenter() {

        presenter = new EulaPresenter();
        presenter.attachView(this);

        String languageCode = PrintOSPreferences.getInstance(this).getLanguageCode();

        presenter.getEula(languageCode);

    }

    @OnClick(R.id.button_accept)
    void onAcceptButtonClicked() {
        Analytics.sendEvent(Analytics.ACCEPT_EULA_ACTION);

        deliverResult(true);
    }

    @OnClick(R.id.button_cancel)
    void onCancelButtonClicked() {
        Analytics.sendEvent(Analytics.REJECT_EULA_ACTION);

        deliverResult(false);
    }

    private void deliverResult(boolean eulaAccepted) {

        Intent resultIntent = new Intent();
        resultIntent.putExtra(Constants.IntentExtras.EULA_ACTIVITY_ACCEPTED_RESULT, eulaAccepted);
        resultIntent.putExtra(Constants.IntentExtras.EULA_ACTIVITY_RESULT_OBJECT, eulaViewModel);
        setResult(Constants.IntentExtras.EULA_ACTIVITY_RESULT_CODE, resultIntent);
        finish();
    }

    @Override
    public void displayEula(EULAViewModel eulaViewModel) {

        this.eulaViewModel = eulaViewModel;

        WebSettings webSettings = eulaWebView.getSettings();
        webSettings.setDefaultFontSize(15);

        String myHtmlString = Constants.TERMS_OF_SERVICES_HTML_START_TAG + eulaViewModel.getEulaText() + Constants.TERMS_OF_SERVICES_HTML_END_TAG;
        eulaWebView.loadDataWithBaseURL(null, myHtmlString, "text/html", "UTF-8", null);

        eulaWebView.setWebViewClient(new WebViewClient() {

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                AppUtils.startApplication(PrintOSApplication.getAppContext(), Uri.parse(url));
                return true;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                contentView.setVisibility(View.VISIBLE);
                buttonsView.setVisibility(displayButtons ? View.VISIBLE : View.GONE);
                loadingView.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public void onEulaSuccessfullyAccepted() {
        //Not used.
    }

    @Override
    public void onError(APIException exception, String tag) {

        HPUIUtils.displayToastException(this, exception, tag, false);
        contentView.setVisibility(View.INVISIBLE);
        loadingView.setVisibility(View.GONE);
    }

    @Override
    public boolean isBackwardCompatible() {
        return true;
    }
}