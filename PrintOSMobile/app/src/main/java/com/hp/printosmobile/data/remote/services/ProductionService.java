package com.hp.printosmobile.data.remote.services;

import com.hp.printosmobile.data.remote.models.ProductionData;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by leviasaf on 26/04/2016.
 */
public interface ProductionService {
    @GET("api/PrintbeatService/Production")
    Call<ProductionData> getProductionData();
}
