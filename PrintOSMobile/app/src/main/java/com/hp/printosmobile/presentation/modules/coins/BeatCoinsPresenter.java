package com.hp.printosmobile.presentation.modules.coins;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.data.remote.models.CoinsStoreData;
import com.hp.printosmobile.presentation.Presenter;
import com.hp.printosmobile.presentation.modules.invites.InviteAllPopup;
import com.hp.printosmobilelib.core.communications.remote.APIException;
import com.hp.printosmobilelib.core.logging.HPLogger;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Osama Taha
 */
public class BeatCoinsPresenter extends Presenter<BeatCoinsView> {


    private static final String TAG = BeatCoinsPresenter.class.getSimpleName();

    private static Observable<Response<List<CoinsStoreData>>> createCoinsObservable(int coins) {

        List<CoinsStoreData> body = new ArrayList();

        CoinsStoreData coinsStoreData = new CoinsStoreData();
        coinsStoreData.setAmount(coins);
        coinsStoreData.setEmail(PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).getUserEmail());

        body.add(coinsStoreData);

        return PrintOSApplication.getApiServicesProvider().getBeatCoinService().collectCoins(body);
    }

    public void claimCoins(final BeatCoinsMode mode, int coins) {

        Subscription subscription = createCoinsObservable(coins)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<Response<List<CoinsStoreData>>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                        if (mView == null){
                            return;
                        }

                        if (e instanceof APIException) {

                            HPLogger.d(TAG, "failed to claim coins " + e);

                            APIException.Kind errorKind = ((APIException) e).getKind();
                            if (errorKind == APIException.Kind.INVALID) {
                                mView.onStoreAccountNotFound();
                            } else {
                                mView.onClaimCoinsFailed();
                            }
                        }
                    }

                    @Override
                    public void onNext(Response<List<CoinsStoreData>> response) {

                        if (mView == null){
                            return;
                        }

                        if (response.isSuccessful()) {
                            HPLogger.d(TAG, "successfully claimed coins");
                            mView.onClaimCoinsSucceeded();
                            onCoinsClaimed(mode);
                        } else if (response.code() == APIException.INVALID_ERROR_CODE) {
                            mView.onStoreAccountNotFound();
                        }

                    }
                });
        addSubscriber(subscription);
    }

    private void onCoinsClaimed(BeatCoinsMode beatCoinsMode) {
        PrintOSPreferences.getInstance().setBeatCoinsClaimed(beatCoinsMode);
    }

    public void showCreateStoreAccountPopup(Activity activity) {

        CreateStoreAccountPopup.getInstance(new CreateStoreAccountPopup.CreateStoreAccountPopupCallback() {
            @Override
            public void onCreateAccountClicked() {
                if (mView != null) {
                    mView.onCreateAccountSelected();
                }
            }

            @Override
            public void onCancelClicked() {
                if (mView != null) {
                    mView.onCancelCreateAccountSelected();
                }
            }
        }).show(activity.getFragmentManager(), CreateStoreAccountPopup.TAG);

    }

    @Override
    public void detachView() {
        stopSubscribers();
    }

    public enum BeatCoinsMode {
        RANKING_LEADERBOARD
    }
}
