package com.hp.printosmobile.presentation.modules.kpiview;

import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;
import com.hp.printosmobile.data.remote.models.PreferencesData;

/**
 * An enum represents the kpi value handle.
 * <p/>
 * Created by Anwar Asbah on 5/12/16.
 */
public enum KPIValueHandleEnum {

    EMPTY("", -1, -1),
    PERCENT("Percents", R.string.unit_percentage, R.string.unit_percentage),
    KILO("Kilo", -1, -1),
    MEGA("Mega", -1, -1),
    SQM("Sqm", R.string.square_meters, R.string.foot_square),
    METER("m", R.string.meters, R.string.foot),
    HOURS("Hours", R.string.unit_hours, R.string.unit_hours),
    TECHNICAL_ISSUES("Technical_issues", -1, -1),
    IMPRESSIONS("Impression", R.string.unit_imp, R.string.unit_imp),
    SHEET("Sheet", R.string.sheets, R.string.sheets),
    EPM("EPM", R.string.unit_imp, R.string.unit_imp),
    OVER_DUE_TASK("Over due task", R.string.unit_over_due_task, R.string.unit_over_due_task),
    PAPER_JAMS("paper jams", R.string.unit_paper_jams, R.string.unit_paper_jams),
    PAGES("pages", R.string.unit_pages, R.string.unit_pages),
    SCORE("score", R.string.report_fragment_points, R.string.report_fragment_points),
    RESTARTS("restarts", R.string.unit_restarts, R.string.unit_restarts),
    FAILURE("failure", R.string.unit_failures, R.string.unit_failures),
    JAM("jam", R.string.unit_jams, R.string.unit_jams);


    private String valueHandle;
    private int metricResource;
    private int imperialResource;

    KPIValueHandleEnum(String valueHandle, int metricResource, int imperialResource) {
        this.valueHandle = valueHandle;
        this.metricResource = metricResource;
        this.imperialResource = imperialResource;
    }

    public String getValueHandle() {
        return valueHandle;
    }

    public static KPIValueHandleEnum from(String valueHandle) {
        for (KPIValueHandleEnum kpiState : KPIValueHandleEnum.values()) {
            if (kpiState.getValueHandle().equals(valueHandle))
                return kpiState;
        }
        return KPIValueHandleEnum.EMPTY;
    }

    public String getUnitString(PreferencesData.UnitSystem unitSystem) {
        String unit = "";
        if (unitSystem == PreferencesData.UnitSystem.Imperial && imperialResource != -1) {
            unit = PrintOSApplication.getAppContext().getString(imperialResource);
        } else if (unitSystem == PreferencesData.UnitSystem.Metric && metricResource != -1) {
            unit = PrintOSApplication.getAppContext().getString(metricResource);
        }
        unit = (this == PERCENT ? "" : " ") + unit;
        return unit;
    }

}
