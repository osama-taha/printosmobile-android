package com.hp.printosmobile.presentation.modules.reports;

import com.github.mikephil.charting.interfaces.dataprovider.ChartInterface;

/**
 * A base performance chart builder. Any concrete builder should extend and override the functionalities
 * in order to provide implementation for different kinds of charts.
 * The performance chart will support:
 * 1-Line charts.
 * 2-Stacked bar charts.
 * <p/>
 *
 * @Author : Osama Taha
 * Created on 06/08/2015.
 */
public abstract class PerformanceChartBuilderInterface<T extends ChartInterface> {

    protected T chart;

    public abstract void buildChart();

    public abstract void buildAxes();

    public abstract void animateX();

    public abstract void animateY();

    public T getChart() {
        return chart;
    }

    public void setChart(T chart) {
        this.chart = chart;
    }
}
