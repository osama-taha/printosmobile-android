package com.hp.printosmobile.data.remote.models;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class VersionsData {

    @JsonProperty("versions")
    private Versions versions;
    @JsonProperty("messages")
    private Messages messages;
    @JsonProperty("configuration")
    private Configuration configuration;
    @JsonProperty("maintenance_message")
    private MaintenanceMessage maintenanceMessage;

    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<>();

    @JsonProperty("versions")
    public Versions getVersions() {
        return versions;
    }

    @JsonProperty("versions")
    public void setVersions(Versions versions) {
        this.versions = versions;
    }

    @JsonProperty("messages")
    public Messages getMessages() {
        return messages;
    }

    @JsonProperty("messages")
    public void setMessages(Messages messages) {
        this.messages = messages;
    }

    @JsonProperty("configuration")
    public Configuration getConfiguration() {
        return configuration;
    }

    @JsonProperty("configuration")
    public void setConfiguration(Configuration configuration) {
        this.configuration = configuration;
    }


    @JsonProperty("maintenance_message")
    public void setMaintenanceMessage(MaintenanceMessage maintenanceMessage) {
        this.maintenanceMessage = maintenanceMessage;
    }

    @JsonProperty("maintenance_message")
    public MaintenanceMessage getMaintenanceMessage() {
        return maintenanceMessage;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return "VersionsData{" +
                "versions=" + versions +
                ", messages=" + messages +
                '}';
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class Versions {

        @JsonProperty("printbeat")
        private Application printbeat;
        @JsonIgnore
        private Map<String, Object> additionalProperties = new HashMap<>();

        @JsonProperty("printbeat")
        public Application getPrintbeat() {
            return printbeat;
        }

        @JsonProperty("printbeat")
        public void setPrintbeat(Application printbeat) {
            this.printbeat = printbeat;
        }

        @JsonAnyGetter
        public Map<String, Object> getAdditionalProperties() {
            return this.additionalProperties;
        }

        @JsonAnySetter
        public void setAdditionalProperty(String name, Object value) {
            this.additionalProperties.put(name, value);
        }

        @Override
        public String toString() {
            return "Versions{" +
                    "printbeat=" + printbeat +
                    '}';
        }
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class Messages {

        @JsonProperty("message_default")
        private Map<String, String> messageDefault;
        @JsonProperty("message_update")
        private Map<String, String> messageUpdate;
        @JsonProperty("message_warning")
        private Map<String, String> messageWarning;
        @JsonIgnore
        private Map<String, Object> additionalProperties = new HashMap<>();

        @JsonProperty("message_default")
        public Map<String, String> getMessageDefault() {
            return messageDefault;
        }

        @JsonProperty("message_default")
        public void setMessageDefault(Map<String, String> messageDefault) {
            this.messageDefault = messageDefault;
        }

        @JsonProperty("message_update")
        public Map<String, String> getMessageUpdate() {
            return messageUpdate;
        }

        @JsonProperty("message_update")
        public void setMessageUpdate(Map<String, String> messageUpdate) {
            this.messageUpdate = messageUpdate;
        }

        @JsonProperty("message_warning")
        public Map<String, String> getMessageWarning() {
            return messageWarning;
        }

        @JsonProperty("message_warning")
        public void setMessageWarning(Map<String, String> messageWarning) {
            this.messageWarning = messageWarning;
        }

        @JsonAnyGetter
        public Map<String, Object> getAdditionalProperties() {
            return this.additionalProperties;
        }

        @JsonAnySetter
        public void setAdditionalProperty(String name, Object value) {
            this.additionalProperties.put(name, value);
        }

        @Override
        public String toString() {
            return "Messages{" +
                    "messageDefault=" + messageDefault +
                    ", messageUpdate=" + messageUpdate +
                    ", messageWarning=" + messageWarning +
                    '}';
        }
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class Application {

        @JsonProperty("ios")
        private Platform ios;
        @JsonProperty("android")
        private Platform android;
        @JsonIgnore
        private Map<String, Object> additionalProperties = new HashMap<>();

        @JsonProperty("ios")
        public Platform getIos() {
            return ios;
        }

        @JsonProperty("ios")
        public void setIos(Platform ios) {
            this.ios = ios;
        }

        @JsonProperty("android")
        public Platform getAndroid() {
            return android;
        }

        @JsonProperty("android")
        public void setAndroid(Platform android) {
            this.android = android;
        }

        @JsonAnyGetter
        public Map<String, Object> getAdditionalProperties() {
            return this.additionalProperties;
        }

        @JsonAnySetter
        public void setAdditionalProperty(String name, Object value) {
            this.additionalProperties.put(name, value);
        }

        @Override
        public String toString() {
            return "Application{" +
                    "ios=" + ios +
                    ", android=" + android +
                    '}';
        }

    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class Platform {

        @JsonProperty("warning")
        private List<String> warning = new ArrayList<>();
        @JsonProperty("update")
        private List<String> update = new ArrayList<>();
        @JsonProperty("valid")
        private List<String> valid = new ArrayList<>();
        @JsonProperty("link_in_store")
        private String linkInStore;
        @JsonIgnore
        private Map<String, Object> additionalProperties = new HashMap<>();

        @JsonProperty("warning")
        public List<String> getWarning() {
            return warning;
        }

        @JsonProperty("warning")
        public void setWarning(List<String> warning) {
            this.warning = warning;
        }

        @JsonProperty("update")
        public List<String> getUpdate() {
            return update;
        }

        @JsonProperty("update")
        public void setUpdate(List<String> update) {
            this.update = update;
        }

        @JsonProperty("valid")
        public List<String> getValid() {
            return valid;
        }

        @JsonProperty("valid")
        public void setValid(List<String> valid) {
            this.valid = valid;
        }

        @JsonProperty("link_in_store")
        public String getLinkInStore() {
            return linkInStore;
        }

        @JsonProperty("link_in_store")
        public void setLinkInStore(String linkInStore) {
            this.linkInStore = linkInStore;
        }

        @JsonAnyGetter
        public Map<String, Object> getAdditionalProperties() {
            return this.additionalProperties;
        }

        @JsonAnySetter
        public void setAdditionalProperty(String name, Object value) {
            this.additionalProperties.put(name, value);
        }

        @Override
        public String toString() {
            return "Platform{" +
                    "warning=" + warning +
                    ", update=" + update +
                    ", valid=" + valid +
                    ", linkInStore='" + linkInStore + '\'' +
                    '}';
        }
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class Configuration {

        @JsonProperty("app_rating")
        private AppRating appRating;
        @JsonProperty("v2_support")
        private boolean v2Support;
        @JsonProperty("validation")
        private Validation validation;
        @JsonProperty("insights")
        private Insights insights;
        @JsonProperty("completed_jobs_enabled")
        private boolean completedJobsEnabled;
        @JsonProperty("supported_languages")
        private List<String> supportedLanguages;
        @JsonProperty("weekly_printbeat")
        private WeeklyPrintbeat weeklyPrintbeat;
        @JsonProperty("beat_coins_popup_enabled")
        private Boolean beatCoinsPopupEnabled;
        @JsonProperty("nps_popup_enabled")
        private Boolean npsPopupEnabled;
        @JsonProperty("kpi_explanation_enabled")
        private Boolean kpiExplanationEnabled;
        @JsonProperty("time_zone_popup_enabled")
        private Boolean timeZonePopupEnabled;
        @JsonProperty("refer_a_friend")
        private ReferFriend referFriend;
        @JsonProperty("location_based_notifications_enabled")
        private boolean locationBasedNotificationsEnabled;
        @JsonProperty("closed_service_calls_days")
        private int closedServiceCallsDays;
        @JsonProperty("filter_search_box_device_count")
        private Integer filterSearchBoxDeviceCount;
        @JsonProperty("nps_min_display_time_interval")
        private Integer npsMinDisplayTimeInterval;
        @JsonProperty("today_graph_enabled")
        private Boolean todayGraphEnabled;
        @JsonProperty("set_phone_number_popup_enabled")
        private Boolean setPhoneNumberPopupEnabled;
        @JsonProperty("send_invites")
        private SendInvitesData sendInvitesData;
        @JsonProperty("realtime_data_refresh")
        private RealtimeDataRefresh realtimeDataRefresh;
        @JsonProperty("today_graph_feature")
        private TodayGraphFeature todayGraphFeature;
        @JsonProperty("chat_tab_state")
        private FeatureFlag chatTabFeatureFlag;
        @JsonProperty("profile_tap_view_state")
        private String profileTapViewState;
        @JsonProperty("invite_to_printos")
        private InviteToPrintOS inviteToPrintOS;
        @JsonProperty("forgot_password_enabled")
        private Boolean forgotPasswordEnabled;
        @JsonProperty("name_popup_state")
        private String namePopupState;
        @JsonProperty("intra_daily_updates_auto_subscribe_version")
        private String intraDailyUpdatesAutoSubscribeVersion;
        @JsonProperty("days_shifts_graph")
        private DaysShiftsGraph daysShiftsGraph;
        @JsonProperty("wizard")
        private Wizard wizard;
        @JsonProperty("corrective_actions_enabled")
        private FeatureFlag correctiveActionsEnabled;
        @JsonProperty("kpi_breakdown_state")
        private FeatureFlag kpiBreakdownState;
        @JsonProperty("state_distribution_state")
        private FeatureFlag stateDistributionState;
        @JsonProperty("hpid_state")
        private FeatureFlag hpidState;
        @JsonProperty("dns_txt_file")
        private String dnsTextFile;
        @JsonProperty("switch_server_popup_enabled")
        private FeatureFlag chinaLoggingState;
        @JsonProperty("chat_non_member")
        private FeatureFlag chatNonMemberFlag;

        @JsonIgnore
        private Map<String, Object> additionalProperties = new HashMap<>();

        @JsonProperty("chat_non_member")
        public FeatureFlag getChatNonMemberFlag() {
            return chatNonMemberFlag;
        }

        @JsonProperty("chat_non_member")
        public void setChatNonMemberFlag(FeatureFlag chatNonMemberFlag) {
            this.chatNonMemberFlag = chatNonMemberFlag;
        }

        @JsonProperty("state_distribution_state")
        public FeatureFlag getStateDistributionState() {
            return stateDistributionState;

        }

        @JsonProperty("state_distribution_state")
        public void setStateDistributionState(FeatureFlag stateDistributionState) {
            this.stateDistributionState = stateDistributionState;
        }

        @JsonProperty("switch_server_popup_enabled")
        public FeatureFlag getChinaLoggingState() {
            return chinaLoggingState;
        }

        @JsonProperty("switch_server_popup_enabled")
        public void setChinaLoggingState(FeatureFlag chinaLoggingState) {
            this.chinaLoggingState = chinaLoggingState;
        }

        @JsonProperty("days_shifts_graph")
        public DaysShiftsGraph getDaysShiftsGraph() {
            return daysShiftsGraph;
        }

        @JsonProperty("days_shifts_graph")
        public void setDaysShiftsGraph(DaysShiftsGraph daysShiftsGraph) {
            this.daysShiftsGraph = daysShiftsGraph;
        }

        @JsonProperty("intra_daily_updates_auto_subscribe_version")
        public String getIntraDailyUpdatesAutoSubscribeVersion() {
            return intraDailyUpdatesAutoSubscribeVersion;
        }

        @JsonProperty("intra_daily_updates_auto_subscribe_version")
        public void setIntraDailyUpdatesAutoSubscribeVersion(String intraDailyUpdatesAutoSubscribeVersion) {
            this.intraDailyUpdatesAutoSubscribeVersion = intraDailyUpdatesAutoSubscribeVersion;
        }

        @JsonProperty("invite_to_printos")
        public InviteToPrintOS getInviteToPrintOS() {
            return inviteToPrintOS;
        }

        @JsonProperty("invite_to_printos")
        public void setInviteToPrintOS(InviteToPrintOS inviteToPrintOS) {
            this.inviteToPrintOS = inviteToPrintOS;
        }

        @JsonProperty("realtime_data_refresh")
        public RealtimeDataRefresh getRealtimeDataRefresh() {
            return realtimeDataRefresh;
        }

        @JsonProperty("realtime_data_refresh")
        public void setRealtimeDataRefresh(RealtimeDataRefresh realtimeDataRefresh) {
            this.realtimeDataRefresh = realtimeDataRefresh;
        }

        @JsonProperty("profile_tap_view_state")
        public String getProfileTapViewState() {
            return profileTapViewState;
        }

        @JsonProperty("profile_tap_view_state")
        public void setProfileTapViewState(String profileTapViewState) {
            this.profileTapViewState = profileTapViewState;
        }

        @JsonProperty("today_graph_feature")
        public TodayGraphFeature getTodayGraphFeature() {
            return todayGraphFeature;
        }

        @JsonProperty("today_graph_feature")
        public void setTodayGraphFeature(TodayGraphFeature todayGraphFeature) {
            this.todayGraphFeature = todayGraphFeature;
        }

        @JsonProperty("today_graph_enabled")
        public Boolean isTodayGraphEnabled() {
            return todayGraphEnabled;
        }

        @JsonProperty("today_graph_enabled")
        public void setTodayGraphEnabled(Boolean todayGraphEnabled) {
            this.todayGraphEnabled = todayGraphEnabled;
        }

        @JsonProperty("nps_min_display_time_interval")
        public Integer getNpsMinDisplayTimeInterval() {
            return npsMinDisplayTimeInterval;
        }

        @JsonProperty("nps_min_display_time_interval")
        public void setNpsMinDisplayTimeInterval(Integer npsMinDisplayTimeInterval) {
            this.npsMinDisplayTimeInterval = npsMinDisplayTimeInterval;
        }

        @JsonProperty("refer_a_friend")
        public ReferFriend getReferFriend() {
            return referFriend;
        }

        @JsonProperty("refer_a_friend")
        public void setReferFriend(ReferFriend referFriend) {
            this.referFriend = referFriend;
        }

        @JsonProperty("beat_coins_popup_enabled")
        public Boolean isBeatCoinsPopupEnabled() {
            return beatCoinsPopupEnabled;
        }

        @JsonProperty("beat_coins_popup_enabled")
        public void setBeatCoinsPopupEnabled(Boolean beatCoinsPopupEnabled) {
            this.beatCoinsPopupEnabled = beatCoinsPopupEnabled;
        }

        @JsonProperty("nps_popup_enabled")
        public Boolean isNpsPopupEnabled() {
            return npsPopupEnabled;
        }

        @JsonProperty("nps_popup_enabled")
        public void setNpsPopupEnabled(Boolean npsPopupEnabled) {
            this.npsPopupEnabled = npsPopupEnabled;
        }

        @JsonProperty("time_zone_popup_enabled")
        public Boolean isTimeZonePopupEnabled() {
            return timeZonePopupEnabled;
        }

        @JsonProperty("time_zone_popup_enabled")
        public void setTimeZonePopupEnabled(Boolean timeZonePopupEnabled) {
            this.timeZonePopupEnabled = timeZonePopupEnabled;
        }

        @JsonProperty("insights")
        public Insights getInsights() {
            return insights;
        }

        @JsonProperty("insights")
        public void setInsights(Insights insights) {
            this.insights = insights;
        }

        @JsonProperty("app_rating")
        public AppRating getAppRating() {
            return appRating;
        }

        @JsonProperty("app_rating")
        public void setAppRating(AppRating appRating) {
            this.appRating = appRating;
        }

        @JsonProperty("v2_support")
        public boolean isV2Support() {
            return v2Support;
        }

        @JsonProperty("v2_support")
        public void setV2Support(boolean v2Support) {
            this.v2Support = v2Support;
        }

        @JsonProperty("validation")
        public Validation getValidation() {
            return validation;
        }

        @JsonProperty("validation")
        public void setValidation(Validation validation) {
            this.validation = validation;
        }

        @JsonProperty("completed_jobs_enabled")
        public boolean isCompletedJobsEnabled() {
            return completedJobsEnabled;
        }

        @JsonProperty("completed_jobs_enabled")
        public void setCompletedJobsEnabled(boolean completedJobsEnabled) {
            this.completedJobsEnabled = completedJobsEnabled;
        }

        @JsonProperty("supported_languages")
        public void setSupportedLanguages(List<String> supportedLanguages) {
            this.supportedLanguages = supportedLanguages;
        }

        @JsonProperty("supported_languages")
        public List<String> getSupportedLanguages() {
            return supportedLanguages;
        }

        @JsonProperty("weekly_printbeat")
        public WeeklyPrintbeat getWeeklyPrintbeat() {
            return weeklyPrintbeat;
        }

        @JsonProperty("weekly_printbeat")
        public void setWeeklyPrintbeat(WeeklyPrintbeat weeklyPrintbeat) {
            this.weeklyPrintbeat = weeklyPrintbeat;
        }

        @JsonProperty("kpi_explanation_enabled")
        public Boolean getKpiExplanationEnabled() {
            return kpiExplanationEnabled;
        }

        @JsonProperty("kpi_explanation_enabled")
        public void setKpiExplanationEnabled(Boolean kpiExplanationEnabled) {
            this.kpiExplanationEnabled = kpiExplanationEnabled;
        }

        @JsonProperty("location_based_notifications_enabled")
        public boolean isLocationBasedNotificationsEnabled() {
            return locationBasedNotificationsEnabled;
        }

        @JsonProperty("location_based_notifications_enabled")
        public void setLocationBasedNotificationsEnabled(boolean locationBasedNotificationsEnabled) {
            this.locationBasedNotificationsEnabled = locationBasedNotificationsEnabled;
        }

        @JsonProperty("closed_service_calls_days")
        public void setClosedServiceCallsDays(int closedServiceCallsDays) {
            this.closedServiceCallsDays = closedServiceCallsDays;
        }

        @JsonProperty("closed_service_calls_days")
        public int getClosedServiceCallsDays() {
            return closedServiceCallsDays;
        }

        @JsonProperty("filter_search_box_device_count")
        public Integer getFilterSearchBoxDeviceCount() {
            return filterSearchBoxDeviceCount;
        }

        @JsonProperty("filter_search_box_device_count")
        public void setFilterSearchBoxDeviceCount(Integer filterSearchBoxDeviceCount) {
            this.filterSearchBoxDeviceCount = filterSearchBoxDeviceCount;
        }

        @JsonProperty("set_phone_number_popup_enabled")
        public Boolean isSetPhoneNumberPopupEnabled() {
            return setPhoneNumberPopupEnabled;
        }

        @JsonProperty("send_invites")
        public SendInvitesData getSendInvitesData() {
            return sendInvitesData;
        }

        @JsonProperty("chat_tab_state")
        public FeatureFlag getChatTabFeatureFlag() {
            return chatTabFeatureFlag;
        }

        @JsonProperty("chat_tab_state")
        public void setChatTabFeatureFlag(FeatureFlag chatTabFeatureFlag) {
            this.chatTabFeatureFlag = chatTabFeatureFlag;
        }

        @JsonProperty("forgot_password_enabled")
        public Boolean getForgotPasswordEnabled() {
            return forgotPasswordEnabled;
        }

        @JsonProperty("forgot_password_enabled")
        public void setForgotPasswordEnabled(Boolean forgotPasswordEnabled) {
            this.forgotPasswordEnabled = forgotPasswordEnabled;
        }

        @JsonProperty("wizard")
        public Wizard getWizard() {
            return wizard;
        }

        @JsonProperty("wizard")
        public void setWizard(Wizard wizard) {
            this.wizard = wizard;
        }

        @JsonAnyGetter
        public Map<String, Object> getAdditionalProperties() {
            return this.additionalProperties;
        }

        @JsonAnySetter
        public void setAdditionalProperty(String name, Object value) {
            this.additionalProperties.put(name, value);
        }

        @JsonProperty("name_popup_state")
        public String getNamePopupState() {
            return namePopupState;
        }

        @JsonProperty("name_popup_state")
        public void setNamePopupState(String namePopupState) {
            this.namePopupState = namePopupState;
        }

        @JsonProperty("corrective_actions_enabled")
        public FeatureFlag getCorrectiveActionsEnabled() {
            return correctiveActionsEnabled;
        }

        @JsonProperty("corrective_actions_enabled")
        public void setCorrectiveActionsEnabled(FeatureFlag correctiveActionsEnabled) {
            this.correctiveActionsEnabled = correctiveActionsEnabled;
        }

        public FeatureFlag getKpiBreakdownState() {
            return kpiBreakdownState;
        }

        public void setKpiBreakdownState(FeatureFlag kpiBreakdownState) {
            this.kpiBreakdownState = kpiBreakdownState;
        }

        @JsonProperty("hpid_state")
        public FeatureFlag getHpidState() {
            return hpidState;
        }

        @JsonProperty("hpid_state")
        public void setHpidState(FeatureFlag hpidState) {
            this.hpidState = hpidState;
        }

        @JsonProperty("dns_txt_file")
        public void setDnsTextFile(String dnsTextFile) {
            this.dnsTextFile = dnsTextFile;
        }

        @JsonProperty("dns_txt_file")
        public String getDnsTextFile() {
            return dnsTextFile;
        }

        @JsonInclude(JsonInclude.Include.NON_NULL)
        public static class DaysShiftsGraph {

            @JsonProperty("last_days_chart_type")
            private String lastDaysChartType;
            @JsonProperty("last_shifts_chart_type")
            private String lastShiftsChartType;
            @JsonProperty("feature_enabled")
            private FeatureFlag featureEnabled;
            @JsonProperty("number_of_shifts")
            private int numberOfShifts;
            @JsonIgnore
            private Map<String, Object> additionalProperties = new HashMap<>();

            @JsonProperty("last_days_chart_type")
            public String getLastDaysChartType() {
                return lastDaysChartType;
            }

            @JsonProperty("last_days_chart_type")
            public void setLastDaysChartType(String lastDaysChartType) {
                this.lastDaysChartType = lastDaysChartType;
            }

            @JsonProperty("last_shifts_chart_type")
            public String getLastShiftsChartType() {
                return lastShiftsChartType;
            }

            @JsonProperty("last_shifts_chart_type")
            public void setLastShiftsChartType(String lastShiftsChartType) {
                this.lastShiftsChartType = lastShiftsChartType;
            }

            @JsonProperty("feature_enabled")
            public FeatureFlag getFeatureEnabled() {
                return featureEnabled;
            }

            @JsonProperty("feature_enabled")
            public void setFeatureEnabled(FeatureFlag featureEnabled) {
                this.featureEnabled = featureEnabled;
            }

            @JsonProperty("number_of_shifts")
            public int getNumberOfShifts() {
                return numberOfShifts;
            }

            @JsonProperty("number_of_shifts")
            public void setNumberOfShifts(int numberOfShifts) {
                this.numberOfShifts = numberOfShifts;
            }

            @JsonAnyGetter
            public Map<String, Object> getAdditionalProperties() {
                return this.additionalProperties;
            }

            @JsonAnySetter
            public void setAdditionalProperty(String name, Object value) {
                this.additionalProperties.put(name, value);
            }
        }

        @JsonInclude(JsonInclude.Include.NON_NULL)
        public static class InviteToPrintOS {

            @JsonProperty("include_coins")
            private boolean includeCoins;
            @JsonProperty("state")
            private FeatureFlag state;
            @JsonProperty("beat_coins_per_invite")
            private int beatCoinsPerInvite;
            @JsonProperty("invite_all_popup_display_counter")
            private int inviteAllPopupDisplayCounter;
            @JsonProperty("send_sms_enabled")
            private boolean sendSmsEnabled;

            @JsonIgnore
            private Map<String, Object> additionalProperties = new HashMap<>();

            @JsonProperty("include_coins")
            public boolean isIncludeCoins() {
                return includeCoins;
            }

            @JsonProperty("include_coins")
            public void setIncludeCoins(boolean includeCoins) {
                this.includeCoins = includeCoins;
            }

            @JsonProperty("state")
            public FeatureFlag getState() {
                return state;
            }

            @JsonProperty("state")
            public void setState(FeatureFlag state) {
                this.state = state;
            }

            @JsonAnyGetter
            public Map<String, Object> getAdditionalProperties() {
                return this.additionalProperties;
            }

            @JsonAnySetter
            public void setAdditionalProperty(String name, Object value) {
                this.additionalProperties.put(name, value);
            }

            @JsonProperty("beat_coins_per_invite")
            public int getBeatCoinsPerInvite() {
                return beatCoinsPerInvite;
            }

            @JsonProperty("beat_coins_per_invite")
            public void setBeatCoinsPerInvite(int beatCoinsPerInvite) {
                this.beatCoinsPerInvite = beatCoinsPerInvite;
            }

            @JsonProperty("invite_all_popup_display_counter")
            public int getInviteAllPopupDisplayCounter() {
                return inviteAllPopupDisplayCounter;
            }

            @JsonProperty("invite_all_popup_display_counter")
            public void setInviteAllPopupDisplayCounter(int inviteAllPopupDisplayCounter) {
                this.inviteAllPopupDisplayCounter = inviteAllPopupDisplayCounter;
            }

            @JsonProperty("send_sms_enabled")
            public boolean isSendSmsEnabled() {
                return sendSmsEnabled;
            }

            @JsonProperty("send_sms_enabled")
            public void setSendSmsEnabled(boolean sendSmsEnabled) {
                this.sendSmsEnabled = sendSmsEnabled;
            }
        }

        @JsonInclude(JsonInclude.Include.NON_NULL)
        public static class RealtimeDataRefresh {

            @JsonProperty("enabled")
            private boolean enabled;
            @JsonProperty("repeat_interval_in_secs")
            private int repeatIntervalInSecs;
            @JsonIgnore
            private Map<String, Object> additionalProperties = new HashMap<>();

            @JsonProperty("enabled")
            public boolean isEnabled() {
                return enabled;
            }

            @JsonProperty("enabled")
            public void setEnabled(boolean enabled) {
                this.enabled = enabled;
            }

            @JsonProperty("repeat_interval_in_secs")
            public int getRepeatIntervalInSecs() {
                return repeatIntervalInSecs;
            }

            @JsonProperty("repeat_interval_in_secs")
            public void setRepeatIntervalInSecs(int repeatIntervalInSecs) {
                this.repeatIntervalInSecs = repeatIntervalInSecs;
            }

            @JsonAnyGetter
            public Map<String, Object> getAdditionalProperties() {
                return this.additionalProperties;
            }

            @JsonAnySetter
            public void setAdditionalProperty(String name, Object value) {
                this.additionalProperties.put(name, value);
            }
        }

        @JsonInclude(JsonInclude.Include.NON_NULL)
        public static class TodayGraphFeature {

            @JsonProperty("supported_equipments")
            private List<String> supportedEquipments;
            @JsonIgnore
            private Map<String, Object> additionalProperties = new HashMap<>();

            @JsonProperty("supported_equipments")
            public List<String> getSupportedEquipments() {
                return supportedEquipments;
            }

            @JsonProperty("supported_equipments")
            public void setSupportedEquipments(List<String> supportedEquipments) {
                this.supportedEquipments = supportedEquipments;
            }

            @JsonAnyGetter
            public Map<String, Object> getAdditionalProperties() {
                return this.additionalProperties;
            }

            @JsonAnySetter
            public void setAdditionalProperty(String name, Object value) {
                this.additionalProperties.put(name, value);
            }
        }

        @JsonInclude(JsonInclude.Include.NON_NULL)
        public static class AppRating {

            @JsonProperty("rated_versions")
            private List<String> versionList;
            @JsonProperty("days_after_installation")
            private int daysAfterInstallation;
            @JsonProperty("rate_recurring_period")
            private int rateRecurringPeriod;
            @JsonIgnore
            private Map<String, Object> additionalProperties = new HashMap<>();

            @JsonProperty("rated_versions")
            public List<String> getVersionList() {
                return versionList;
            }

            @JsonProperty("rated_versions")
            public void setVersionList(List<String> versionList) {
                this.versionList = versionList;
            }

            @JsonProperty("days_after_installation")
            public int getDaysAfterInstallation() {
                return daysAfterInstallation;
            }

            @JsonProperty("days_after_installation")
            public void setDaysAfterInstallation(int daysAfterInstallation) {
                this.daysAfterInstallation = daysAfterInstallation;
            }

            @JsonProperty("rate_recurring_period")
            public int getRateRecurringPeriod() {
                return rateRecurringPeriod;
            }

            @JsonProperty("rate_recurring_period")
            public void setRateRecurringPeriod(int rateRecurringPeriod) {
                this.rateRecurringPeriod = rateRecurringPeriod;
            }

            @JsonAnyGetter
            public Map<String, Object> getAdditionalProperties() {
                return this.additionalProperties;
            }

            @JsonAnySetter
            public void setAdditionalProperty(String name, Object value) {
                this.additionalProperties.put(name, value);
            }
        }

        @JsonInclude(JsonInclude.Include.NON_NULL)
        public static class Validation {

            @JsonProperty("check_cookie_expiry_hrs")
            private int checkCookieExpiryHrs;
            @JsonProperty("move_to_home_mins")
            private int moveToHomeMins;
            @JsonProperty("show_toast_message")
            private boolean showToastMessage;
            @JsonIgnore
            private Map<String, Object> additionalProperties = new HashMap<>();

            @JsonProperty("check_cookie_expiry_hrs")
            public int getCheckCookieExpiryHrs() {
                return checkCookieExpiryHrs;
            }

            @JsonProperty("check_cookie_expiry_hrs")
            public void setCheckCookieExpiryHrs(int checkCookieExpiryHrs) {
                this.checkCookieExpiryHrs = checkCookieExpiryHrs;
            }

            @JsonProperty("move_to_home_mins")
            public int getMoveToHomeMins() {
                return moveToHomeMins;
            }

            @JsonProperty("move_to_home_mins")
            public void setMoveToHomeMins(int moveToHomeMins) {
                this.moveToHomeMins = moveToHomeMins;
            }

            @JsonProperty("show_toast_message")
            public boolean isShowToastMessage() {
                return showToastMessage;
            }

            @JsonProperty("show_toast_message")
            public void setShowToastMessage(boolean showToastMessage) {
                this.showToastMessage = showToastMessage;
            }

            @JsonAnyGetter
            public Map<String, Object> getAdditionalProperties() {
                return this.additionalProperties;
            }

            @JsonAnySetter
            public void setAdditionalProperty(String name, Object value) {
                this.additionalProperties.put(name, value);
            }

        }

        @JsonInclude(JsonInclude.Include.NON_NULL)
        public static class Insights {

            @JsonProperty("jams_enabled")
            private boolean jamsEnabled;
            @JsonProperty("failures_enabled")
            private boolean failuresEnabled;
            @JsonProperty("badge_version")
            private String badgeVersion;
            @JsonProperty("kz_supported_formats")
            private List<String> kzSupportedFormats;
            @JsonProperty("kz_search_enabled")
            private FeatureFlag kzSearchEnabled;
            @JsonProperty("kz_supported_equipments")
            private List<String> kzSupportedEquipments;

            @JsonIgnore
            private Map<String, Object> additionalProperties = new HashMap<>();

            @JsonProperty("jams_enabled")
            public boolean isJamsEnabled() {
                return jamsEnabled;
            }

            @JsonProperty("jams_enabled")
            public void setJamsEnabled(boolean jamsEnabled) {
                this.jamsEnabled = jamsEnabled;
            }

            @JsonProperty("failures_enabled")
            public boolean isFailuresEnabled() {
                return failuresEnabled;
            }

            @JsonProperty("failures_enabled")
            public void setFailuresEnabled(boolean failuresEnabled) {
                this.failuresEnabled = failuresEnabled;
            }

            @JsonProperty("badge_version")
            public String getBadgeVersion() {
                return badgeVersion;
            }

            @JsonProperty("badge_version")
            public void setBadgeVersion(String badgeVersion) {
                this.badgeVersion = badgeVersion;
            }

            @JsonProperty("kz_supported_formats")
            public void setKzSupportedFormats(List<String> kzSupportedFormats) {
                this.kzSupportedFormats = kzSupportedFormats;
            }

            @JsonProperty("kz_supported_formats")
            public List<String> getKzSupportedFormats() {
                return kzSupportedFormats;
            }

            @JsonProperty("kz_supported_equipments")
            public void setKzSupportedEquipments(List<String> kzSupportedEquipments) {
                this.kzSupportedEquipments = kzSupportedEquipments;
            }

            @JsonProperty("kz_supported_equipments")
            public List<String> getKzSupportedEquipments() {
                return kzSupportedEquipments;
            }

            @JsonProperty("kz_search_enabled")
            public FeatureFlag getKzSearchEnabled() {
                return kzSearchEnabled;
            }

            @JsonProperty("kz_search_enabled")
            public void setKzSearchEnabled(FeatureFlag kzSearchEnabled) {
                this.kzSearchEnabled = kzSearchEnabled;
            }

            @JsonAnyGetter
            public Map<String, Object> getAdditionalProperties() {
                return this.additionalProperties;
            }

            @JsonAnySetter
            public void setAdditionalProperty(String name, Object value) {
                this.additionalProperties.put(name, value);
            }
        }

        @JsonInclude(JsonInclude.Include.NON_NULL)
        public static class WeeklyPrintbeat {

            @JsonProperty("kpi_data_incrementally_enabled")
            private Boolean kpiDataIncrementallyEnabled;
            @JsonProperty("ranking_feature")
            private RankingFeature rankingFeature;
            @JsonProperty("low_print_volume_imp")
            private Integer lowPrintVolumeImp;
            @JsonProperty("ranking_leaderboard_enabled")
            private FeatureFlag leaderboardRankingFeatureFlag;
            @JsonIgnore
            private Map<String, Object> additionalProperties = new HashMap<>();

            @JsonProperty("low_print_volume_imp")
            public Integer getLowPrintVolumeImp() {
                return lowPrintVolumeImp;
            }

            @JsonProperty("low_print_volume_imp")
            public void setLowPrintVolumeImp(Integer lowPrintVolumeImp) {
                this.lowPrintVolumeImp = lowPrintVolumeImp;
            }

            @JsonProperty("kpi_data_incrementally_enabled")
            public Boolean isKpiDataIncrementallyEnabled() {
                return kpiDataIncrementallyEnabled;
            }

            @JsonProperty("kpi_data_incrementally_enabled")
            public void setKpiDataIncrementallyEnabled(Boolean kpiDataIncrementlyEnabled) {
                this.kpiDataIncrementallyEnabled = kpiDataIncrementlyEnabled;
            }

            @JsonProperty("ranking_feature")
            public RankingFeature getRankingFeature() {
                return rankingFeature;
            }

            @JsonProperty("ranking_feature")
            public void setRankingFeature(RankingFeature rankingFeature) {
                this.rankingFeature = rankingFeature;
            }

            @JsonProperty("ranking_leaderboard_enabled")
            public FeatureFlag leaderboardRankingEnabled() {
                return leaderboardRankingFeatureFlag;
            }

            @JsonProperty("ranking_leaderboard_enabled")
            public void setLeaderboardRankingFeatureFlag(FeatureFlag leaderboardRankingFeatureFlag) {
                this.leaderboardRankingFeatureFlag = leaderboardRankingFeatureFlag;
            }

            public void setAdditionalProperties(Map<String, Object> additionalProperties) {
                this.additionalProperties = additionalProperties;
            }

            @JsonAnyGetter
            public Map<String, Object> getAdditionalProperties() {
                return this.additionalProperties;
            }

            @JsonAnySetter
            public void setAdditionalProperty(String name, Object value) {
                this.additionalProperties.put(name, value);
            }

            @JsonInclude(JsonInclude.Include.NON_NULL)
            public static class RankingFeature {

                @JsonProperty("enabled")
                private boolean enabled;
                @JsonProperty("supported_equipments")
                private List<String> supportedEquipments;

                @JsonProperty("enabled")
                public boolean isEnabled() {
                    return enabled;
                }

                @JsonProperty("enabled")
                public void setEnabled(boolean enabled) {
                    this.enabled = enabled;
                }

                @JsonProperty("supported_equipments")
                public List<String> getSupportedEquipments() {
                    return supportedEquipments;
                }

                @JsonProperty("supported_equipments")
                public void setSupportedEquipments(List<String> supportedEquipments) {
                    this.supportedEquipments = supportedEquipments;
                }


                public void setAdditionalProperties(Map<String, Object> additionalProperties) {
                    this.additionalProperties = additionalProperties;
                }

                @JsonIgnore
                private Map<String, Object> additionalProperties = new HashMap<>();

                @JsonAnyGetter
                public Map<String, Object> getAdditionalProperties() {
                    return this.additionalProperties;
                }

                @JsonAnySetter
                public void setAdditionalProperty(String name, Object value) {
                    this.additionalProperties.put(name, value);
                }

            }
        }

        @JsonInclude(JsonInclude.Include.NON_NULL)
        public static class ReferFriend {

            @JsonProperty("enabled")
            private Boolean enabled;
            @JsonProperty("url")
            private String url;
            @JsonIgnore
            private Map<String, Object> additionalProperties = new HashMap<>();

            @JsonProperty("enabled")
            public Boolean isEnabled() {
                return enabled;
            }

            @JsonProperty("enabled")
            public void setEnabled(Boolean enabled) {
                this.enabled = enabled;
            }

            @JsonProperty("url")
            public String getUrl() {
                return url;
            }

            @JsonProperty("url")
            public void setUrl(String url) {
                this.url = url;
            }

            @JsonAnyGetter
            public Map<String, Object> getAdditionalProperties() {
                return this.additionalProperties;
            }

            @JsonAnySetter
            public void setAdditionalProperty(String name, Object value) {
                this.additionalProperties.put(name, value);
            }

        }

        @JsonInclude(JsonInclude.Include.NON_NULL)
        public class SendInvitesData {

            @JsonProperty("enabled")
            private Boolean enabled;
            @JsonProperty("min_display_time_interval")
            private Integer minDisplayTimeInterval;
            @JsonProperty("link")
            private String link;

            @JsonProperty("enabled")
            public Boolean isEnabled() {
                return enabled;
            }

            @JsonProperty("min_display_time_interval")
            public Integer getMinDisplayTimeInterval() {
                return minDisplayTimeInterval;
            }

            @JsonProperty("link")
            public String getLink() {
                return link;
            }
        }

        @JsonInclude(JsonInclude.Include.NON_NULL)
        public static class Wizard {

            @JsonProperty("state")
            private FeatureFlag state;
            @JsonProperty("coins_reward")
            private Integer coinsReward;
            @JsonProperty("app_launches_count")
            private Integer appLaunchesCount;

            @JsonProperty("state")
            public FeatureFlag getState() {
                return state;
            }

            @JsonProperty("state")
            public void setState(FeatureFlag state) {
                this.state = state;
            }

            @JsonProperty("coins_reward")
            public Integer getCoinsReward() {
                return coinsReward;
            }

            @JsonProperty("coins_reward")
            public void setCoinsReward(Integer coinsReward) {
                this.coinsReward = coinsReward;
            }

            @JsonProperty("app_launches_count")
            public Integer getAppLaunchesCount() {
                return appLaunchesCount;
            }

            @JsonProperty("app_launches_count")
            public void setAppLaunchesCount(Integer appLaunchesCount) {
                this.appLaunchesCount = appLaunchesCount;
            }
        }
    }


    @JsonInclude(JsonInclude.Include.NON_NULL)
    public class MaintenanceMessage {

        @JsonProperty("message_text")
        private Map<String, String> messageText;
        @JsonProperty("event_time")
        @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ", timezone = JsonFormat.DEFAULT_TIMEZONE)
        private Date eventTime;
        @JsonProperty("messageGuid")
        private String messageGuid;
        @JsonProperty("showCount")
        private Integer showCount;

        @JsonProperty("message_text")
        public Map<String, String> getMessageText() {
            return messageText;
        }

        @JsonProperty("message_text")
        public void setMessageText(Map<String, String> messageText) {
            this.messageText = messageText;
        }

        @JsonProperty("event_time")
        public Date getEventTime() {
            return eventTime;
        }

        @JsonProperty("event_time")
        public void setEventTime(Date eventTime) {
            this.eventTime = eventTime;
        }

        @JsonProperty("messageGuid")
        public String getMessageGuid() {
            return messageGuid;
        }

        @JsonProperty("messageGuid")
        public void setMessageGuid(String messageGuid) {
            this.messageGuid = messageGuid;
        }

        @JsonProperty("showCount")
        public Integer getShowCount() {
            return showCount;
        }

        @JsonProperty("showCount")
        public void setShowCount(Integer showCount) {
            this.showCount = showCount;
        }
    }

    public enum FeatureFlag {
        ON, OFF, ENGLISH_ONLY
    }
}