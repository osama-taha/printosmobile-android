package com.hp.printosmobile.presentation.modules.settings.wizard;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.ScrollView;
import android.widget.TextView;

import com.hbb20.CountryCodePicker;
import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;
import com.hp.printosmobile.presentation.BaseFragment;
import com.hp.printosmobile.presentation.modules.contacthp.shared.ContactHpTextField;
import com.hp.printosmobile.utils.HPUIUtils;
import com.hp.printosmobilelib.ui.widgets.HPEditText;
import com.hp.printosmobilelib.ui.widgets.HPTextView;
import com.hp.printosmobilelib.ui.widgets.TypefaceManager;

import java.util.Map;

import butterknife.Bind;
import io.michaelrocks.libphonenumber.android.PhoneNumberUtil;
import io.michaelrocks.libphonenumber.android.Phonenumber;

/**
 * Created by Osama Taha
 */
public class UserAdditionalInfoFragment extends BaseFragment implements IWizardFragment {

    public static final String TAG = UserAdditionalInfoFragment.class.getName();

    private static final String KEY_DATA = "key_user_data";

    @Bind(R.id.description_text_view)
    HPTextView messageTextView;
    @Bind(R.id.country_code_picker)
    CountryCodePicker countryCodePicker;
    @Bind(R.id.set_phone_number_dialog_phone_number)
    HPEditText phoneNumberEditText;
    @Bind(R.id.time_zone_spinner)
    ContactHpTextField timeZoneSpinner;

    private WizardViewModel wizardViewModel;
    private Map<String, String> timeZonesMap;

    public static UserAdditionalInfoFragment getInstance(WizardViewModel wizardViewModel) {
        UserAdditionalInfoFragment userAdditionalInfoFragment = new UserAdditionalInfoFragment();
        Bundle args = new Bundle();
        args.putSerializable(KEY_DATA, wizardViewModel);
        userAdditionalInfoFragment.setArguments(args);
        return userAdditionalInfoFragment;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        wizardViewModel = (WizardViewModel) getArguments().getSerializable(KEY_DATA);

        //Init phone number component.
        countryCodePicker.registerCarrierNumberEditText(phoneNumberEditText);
        countryCodePicker.setDialogTypeFace(TypefaceManager.getTypeface(getContext(), TypefaceManager.HPTypeface.ROBOTO_REGULAR));
        countryCodePicker.setTypeFace(TypefaceManager.getTypeface(getContext(), TypefaceManager.HPTypeface.ROBOTO_LIGHT));

        try {

            PhoneNumberUtil phoneUtil = PhoneNumberUtil.createInstance(getActivity());
            Phonenumber.PhoneNumber numberProto = phoneUtil.parse(wizardViewModel.getPhoneNumber(), "");
            int countryCode = numberProto.getCountryCode();
            countryCodePicker.setCountryForPhoneCode(countryCode);
            phoneNumberEditText.setText(String.valueOf(numberProto.getNationalNumber()));

        } catch (Exception e) {

            phoneNumberEditText.setText(wizardViewModel.getPhoneNumber());

        }

        timeZonesMap = UserProfileManager.getInstance().getTimeZonesMap();
        timeZoneSpinner.setAutoCompleteList(UserProfileManager.getInstance().getTimeZonesMap());
        timeZoneSpinner.setTextFieldRightDrawable(ContextCompat.getDrawable(getContext(), com.hp.printosmobilelib.ui.R.drawable.cancel_light));
        timeZoneSpinner.getAutoCompleteTextView().setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (!hasFocus) {
                    String text = timeZoneSpinner.getText();
                    if (timeZonesMap == null || !timeZonesMap.containsKey(text)) {
                        timeZoneSpinner.getAutoCompleteTextView().setText("");
                    }
                }
            }
        });

        if (!TextUtils.isEmpty(wizardViewModel.getTimeZone())) {
            String timeZone = timeZonesMap != null && timeZonesMap.containsKey(wizardViewModel.getTimeZone())
                    ? timeZonesMap.get(wizardViewModel.getTimeZone()) : "";
            timeZoneSpinner.getAutoCompleteTextView().setText(timeZone);
        }

        ((ScrollView) view).getChildAt(0).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeyboard();
            }
        });

        timeZoneSpinner.getAutoCompleteTextView().setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    phoneNumberEditText.requestFocus();
                    return true;
                }
                return false;
            }
        });

    }

    private void hideKeyboard() {

        timeZoneSpinner.getAutoCompleteTextView().clearFocus();
        HPUIUtils.hideSoftKeyboard(getContext(), phoneNumberEditText, timeZoneSpinner.getAutoCompleteTextView());

    }


    @Override
    protected int getLayoutResource() {
        return R.layout.wizard_additional_info_page;
    }

    @Override
    public boolean isFragmentNameToBeDisplayed() {
        return false;
    }

    @Override
    public boolean isFilteringAvailable() {
        return false;
    }

    @Override
    public int getToolbarDisplayName() {
        return 0;
    }

    @Override
    public boolean isValid() {

        Context context = PrintOSApplication.getAppContext();

        if (TextUtils.isEmpty(phoneNumberEditText.getText())) {
            HPUIUtils.displayToast(context, context.getString(R.string.error_message_phone_number_required));
            return false;
        }

        if (!countryCodePicker.isValidFullNumber()) {
            HPUIUtils.displayToast(context, context.getString(R.string.error_message_phone_number_invalid));
            return false;
        }

        if (TextUtils.isEmpty(timeZoneSpinner.getText())) {
            HPUIUtils.displayToast(context, context.getString(R.string.error_message_timezone_required));
            return false;
        }

        if (wizardViewModel != null) {
            wizardViewModel.setPhoneNumber(countryCodePicker.getFullNumberWithPlus());
            if (!TextUtils.isEmpty(timeZoneSpinner.getText())) {
                wizardViewModel.setTimeZone(timeZoneSpinner.getAutoCompleteTextView().getText().toString());
            }
        }

        return true;
    }

    @Override
    public String getFragmentName() {
        return "Wizard_additional_info";
    }

    @Override
    public void onCancel() {

    }
}
