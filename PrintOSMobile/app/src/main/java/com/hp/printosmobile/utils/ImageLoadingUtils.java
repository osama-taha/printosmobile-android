package com.hp.printosmobile.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.support.v4.content.res.ResourcesCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.hp.printosmobile.Constants;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobilelib.core.communications.remote.SessionManager;
import com.jakewharton.picasso.OkHttp3Downloader;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.HashMap;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by Anwar Asbah on 10/2/2017.
 */

public class ImageLoadingUtils {

    private static final String TAG = ImageLoadingUtils.class.getSimpleName();

    private static Bitmap profileBitmap;
    private static String cachedImageUrl;
    private static HashMap<String, Bitmap> cache;

    public static void setLoadedImageFromUrl(final Context context, final ImageView imageView, final String imageUrl,
                                             final int defaultImageID, final ImageLoaderCallback callback, final boolean isProfileImage) {

        if (cache == null) {
            cache = new HashMap<>();
        }

        if (TextUtils.isEmpty(imageUrl)) {
            if (callback != null) {
                callback.loadImageError();
            }
            return;
        }

        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        Request newRequest = chain.request().newBuilder()
                                .addHeader(Constants.COOKIE_TAG, SessionManager.getInstance(context).getCookie())
                                .addHeader(Constants.ACCEPT_TAG, Constants.ACCEPT_VAL)
                                .addHeader(Constants.ACCEPT_ENCODING_TAG, Constants.ACCEPT_ENCODING_VAL)
                                .build();
                        Response response = chain.proceed(newRequest);

                        if (isProfileImage) {
                            try {

                                String imageUrl = null;
                                if (response.networkResponse() != null) {
                                    imageUrl = response.networkResponse().request().url().toString();
                                }

                                if (cachedImageUrl == null) {
                                    cachedImageUrl = PrintOSPreferences.getInstance(context).getProfileImageUrl();
                                }

                                if (imageUrl != null && !imageUrl.equals(cachedImageUrl)) {
                                    cachedImageUrl = imageUrl;
                                    PrintOSPreferences.getInstance(context).saveUserImage(imageUrl);
                                }

                            } catch (Exception e) {
                                //Image url doesn't exist.
                            }
                        }

                        return response;
                    }
                })
                .followRedirects(true)
                .followSslRedirects(true)
                .build();

        if (imageView == null) {
            return;
        }

        if (!isProfileImage && cache.get(imageUrl) != null) {
            imageView.setVisibility(View.VISIBLE);
            imageView.setImageBitmap(cache.get(imageUrl));
        } else {
            imageView.setVisibility(View.VISIBLE);
            Picasso picasso = new Picasso.Builder(context)
                    .listener(new Picasso.Listener() {
                        @Override
                        public void onImageLoadFailed(Picasso picasso, Uri uri, Exception exception) {
                            if (callback != null) {
                                callback.loadImageError();
                            }

                            imageView.setImageDrawable(ResourcesCompat.getDrawable(context.getResources(),
                                    defaultImageID, null));
                        }
                    })
                    .downloader(new OkHttp3Downloader(client))
                    .build();
            picasso.load(imageUrl).into(imageView, new Callback() {
                @Override
                public void onSuccess() {

                    Log.v(TAG, "success loading operator image");

                    if (callback != null) {
                        callback.loadImageCompleted();
                    }

                    if (isProfileImage) {
                        profileBitmap = imageView.getDrawable() == null ?
                                null : ((BitmapDrawable) imageView.getDrawable()).getBitmap();
                    } else {
                        cache.put(imageUrl, ((BitmapDrawable) imageView.getDrawable()).getBitmap());
                    }
                }

                @Override
                public void onError() {

                    Log.v(TAG, "error loading operator image");

                    if (callback != null) {
                        callback.loadImageError();
                    }
                }
            });

            if (isProfileImage && profileBitmap != null) {
                imageView.setImageBitmap(profileBitmap);
            }
        }

    }

    public interface ImageLoaderCallback {
        void loadImageCompleted();

        void loadImageError();
    }

    public static void clearCache() {
        profileBitmap = null;
        cachedImageUrl = null;
    }

}