package com.hp.printosmobile.data.remote.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Osama on 12/19/17.
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class CoinsStoreData {

    @JsonProperty("amount")
    int amount;
    @JsonProperty("customerType")
    String customerType;
    @JsonProperty("Expires")
    int expires;
    @JsonProperty("claimToken")
    String claimToken;
    @JsonProperty("claimUrl")
    String claimUrl;
    @JsonProperty("email")
    private String email;

    @JsonProperty("amount")
    public int getAmount() {
        return amount;
    }

    @JsonProperty("amount")
    public void setAmount(int amount) {
        this.amount = amount;
    }

    @JsonProperty("customerType")
    public String getCustomerType() {
        return customerType;
    }

    @JsonProperty("customerType")
    public void setCustomerType(String customerType) {
        this.customerType = customerType;
    }

    @JsonProperty("Expires")
    public int getExpires() {
        return expires;
    }

    @JsonProperty("Expires")
    public void setExpires(int expires) {
        this.expires = expires;
    }

    @JsonProperty("claimToken")
    public String getClaimToken() {
        return claimToken;
    }

    @JsonProperty("claimToken")
    public void setClaimToken(String claimToken) {
        this.claimToken = claimToken;
    }

    @JsonProperty("claimUrl")
    public String getClaimUrl() {
        return claimUrl;
    }

    @JsonProperty("claimUrl")
    public void setClaimUrl(String claimUrl) {
        this.claimUrl = claimUrl;
    }

    @JsonProperty("email")
    public void setEmail(String email) {
        this.email = email;
    }

    @JsonProperty("email")
    public String getEmail() {
        return email;
    }
}
