package com.hp.printosmobile;

import android.app.Activity;
import android.text.TextUtils;

import com.amplitude.api.Amplitude;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.presentation.modules.shared.UserViewModel;
import com.hp.printosmobilelib.core.logging.HPLogger;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Osama Taha on 1/14/18.
 */

public class AmplitudeSDK {

    private static final String TAG = AmplitudeSDK.class.getSimpleName();
    private static final String LABEL_PROPERTY = "label";

    private static final AmplitudeSDK sInstance = new AmplitudeSDK();

    public static AmplitudeSDK getInstance() {
        return sInstance;
    }

    private AmplitudeSDK() {
    }

    public void initializeForActivity(Activity activity) {
        Amplitude.getInstance()
                .initialize(activity, activity.getString(R.string.amplitude_api_key))
                .enableForegroundTracking(activity.getApplication());

        setUserId();
    }

    private void setUserId() {
        UserViewModel userViewModel = PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).getUserInfo();

        if (userViewModel != null && !TextUtils.isEmpty(userViewModel.getUserId())) {
            Amplitude.getInstance().setUserId(userViewModel.getUserId());
        } else {
            Amplitude.getInstance().setUserId("");
        }
    }

    public void sendEvent(String action, String label) {

        if (!TextUtils.isEmpty(label)) {
            JSONObject eventProperties = new JSONObject();
            try {
                eventProperties.put(LABEL_PROPERTY, label);
            } catch (JSONException e) {
                HPLogger.e(TAG, "Error creating amplitude eventProperties.");
            }
            Amplitude.getInstance().logEvent(action, eventProperties);
        } else {
            Amplitude.getInstance().logEvent(action);
        }
    }
}
