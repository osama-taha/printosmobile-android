package com.hp.printosmobile.presentation.modules.home;

import android.content.Context;

import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.presentation.Presenter;
import com.hp.printosmobile.presentation.modules.PBNotificationDataViewModel;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitEnum;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitViewModel;
import com.hp.printosmobile.presentation.modules.servicecallpanel.ServiceCallViewModel;
import com.hp.printosmobile.presentation.modules.shared.BaseDeviceViewModel;
import com.hp.printosmobile.presentation.modules.shared.DeviceViewModel;
import com.hp.printosmobile.presentation.modules.today.TodayViewModel;
import com.hp.printosmobile.presentation.modules.todayhistogram.TodayHistogramViewModel;
import com.hp.printosmobile.presentation.modules.week.RankingViewModel;
import com.hp.printosmobile.presentation.modules.week.WeekCollectionViewModel;
import com.hp.printosmobilelib.core.communications.remote.APIException;
import com.hp.printosmobilelib.core.logging.HPLogger;

import java.util.Collections;
import java.util.List;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by Osama Taha on 21/05/2016.
 */
public class HomePresenter extends Presenter<HomeView> {

    private static final String TAG = HomePresenter.class.getName();

    private static BusinessUnitViewModel selectedBusinessUnit;
    private static boolean isShiftSupport;

    private boolean isLoadingTodayData = false;

    private TodayViewModel todayViewModel;

    public void getActiveShifts() {

        Subscription subscription = HomeDataManager.getActiveShifts(selectedBusinessUnit).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<ActiveShiftsViewModel>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        HPLogger.d(TAG, "unable to fetch the active shifts data " + e);
                    }

                    @Override
                    public void onNext(ActiveShiftsViewModel activeShiftsViewModel) {
                        if (mView != null) {
                            mView.updateTodayPanelWithActiveShifts(activeShiftsViewModel);
                        }
                    }
                });
        addSubscriber(subscription);
    }

    public void getTodayData(final boolean isPolling) {

        HPLogger.d(TAG, "getTodayData");

        Observable<PrintBeatDashboardViewModel> observable;

        if (selectedBusinessUnit.getBusinessUnit() == BusinessUnitEnum.LATEX_PRINTER) {
            observable = HomeDataManager.getLatexDashboardViewModel(selectedBusinessUnit, false, isShiftSupport, isPolling);
        } else {

            observable = HomeDataManager.getPrintBeatViewModel(selectedBusinessUnit, false, isShiftSupport, isPolling);
        }

        Subscription subscription = observable
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<PrintBeatDashboardViewModel>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        HPLogger.d(TAG, "unable to fetch the today data " + e);
                        updateTodayView(null, isPolling);
                    }

                    @Override
                    public void onNext(PrintBeatDashboardViewModel printBeatDashboardViewModel) {
                        HPLogger.d(TAG, "getTodayData.onNext");
                        if (!PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).isTodayGraphSupported(
                                selectedBusinessUnit == null ? BusinessUnitEnum.UNKNOWN : selectedBusinessUnit.getBusinessUnit())) {
                            updateTodayView(printBeatDashboardViewModel, isPolling);
                            return;
                        }

                        if (printBeatDashboardViewModel != null && printBeatDashboardViewModel.getTodayViewModel() != null &&
                                printBeatDashboardViewModel.getTodayViewModel().getDeviceViewModels() != null) {

                            for (DeviceViewModel model : printBeatDashboardViewModel.getTodayViewModel().getDeviceViewModels()) {
                                if (model != null && model.isRTSupported()) {
                                    getRealtimeTodayVsWeekData(printBeatDashboardViewModel, selectedBusinessUnit, isShiftSupport, false, isPolling);
                                    return;
                                }
                            }

                        }

                        updateTodayView(printBeatDashboardViewModel, isPolling);
                    }
                });
        addSubscriber(subscription);
    }


    private void updateTodayView(PrintBeatDashboardViewModel printBeatDashboardViewModel, boolean isPolling) {

        HPLogger.d(TAG, "updateTodayView");

        if (mView != null) {

            this.todayViewModel = printBeatDashboardViewModel == null ? null :
                    printBeatDashboardViewModel.getTodayViewModel();

            if (todayViewModel != null && todayViewModel.getDeviceViewModels() != null) {
                BusinessUnitEnum businessUnitEnum = selectedBusinessUnit.getBusinessUnit();
                if (businessUnitEnum == BusinessUnitEnum.LATEX_PRINTER) {
                    Collections.sort(todayViewModel.getDeviceViewModels(), BaseDeviceViewModel.STATUS_COMPARATOR);
                } else if (businessUnitEnum == BusinessUnitEnum.INDIGO_PRESS) {
                    Collections.sort(todayViewModel.getDeviceViewModels(), BaseDeviceViewModel.SORT_POSITION_COMPARATOR);
                } else {
                    Collections.sort(todayViewModel.getDeviceViewModels(), BaseDeviceViewModel.NAME_COMPARATOR);
                }
            }

            mView.updateTodayPanel(printBeatDashboardViewModel == null ? null :
                    printBeatDashboardViewModel.getTodayViewModel(), isPolling);

            if (BusinessUnitEnum.INDIGO_PRESS == selectedBusinessUnit.getBusinessUnit()) {
                mView.updateProductionSnapshot(printBeatDashboardViewModel == null ? null :
                        printBeatDashboardViewModel.getProductionViewModel());
            } else if (BusinessUnitEnum.LATEX_PRINTER == selectedBusinessUnit.getBusinessUnit()) {
                mView.updateLatexProductionSnapshot(printBeatDashboardViewModel == null ? null :
                        printBeatDashboardViewModel.getProductionViewModel());
            }
        }
    }

    public void getRealtimeTodayVsWeekData(
            final PrintBeatDashboardViewModel printBeatDashboardViewModel, BusinessUnitViewModel businessUnitViewModel,
            boolean isShiftSupport, boolean perSite, final boolean isPolling) {

        HPLogger.d(TAG, "getRealtimeTodayVsWeekData");

        Subscription subscription = HomeDataManager.getRealtimeTodayVsLastWeekData(printBeatDashboardViewModel.getTodayViewModel(),
                businessUnitViewModel, perSite, isShiftSupport)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<TodayViewModel>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        HPLogger.d(TAG, "Failed to fetch the today graph data " + e);
                        updateTodayView(printBeatDashboardViewModel, isPolling);
                    }

                    @Override
                    public void onNext(TodayViewModel todayViewModel) {
                        HPLogger.d(TAG, "getRealtimeTodayVsWeekData.onNext()");
                        printBeatDashboardViewModel.setTodayViewModel(todayViewModel);
                        updateTodayView(printBeatDashboardViewModel, isPolling);
                    }
                });
        addSubscriber(subscription);
    }

    public void getPBNotificationData(String id) {

        HPLogger.d(TAG, "getPBNotificationData(" + id + ")");
        Subscription subscription = HomeDataManager.getPBNotificationData(id)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<PBNotificationDataViewModel>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        HPLogger.d(TAG, "getPBNotificationData: fail " + e.getMessage());
                        if (mView != null) {
                            mView.onPBNotificationDataRetrieved(null);
                        }
                    }

                    @Override
                    public void onNext(PBNotificationDataViewModel viewModel) {
                        HPLogger.d(TAG, "getPBNotificationData: success");
                        if (mView != null) {
                            mView.onPBNotificationDataRetrieved(viewModel);
                        }
                    }
                });

        addSubscriber(subscription);

    }

    public synchronized void getActualVsTargetData(Context context, boolean isPolling) {

        if (isLoadingTodayData) {
            return;
        }

        isLoadingTodayData = true;

        Subscription subscription = HomeDataManager.getActualVsTarget(context, selectedBusinessUnit, isShiftSupport, isPolling)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<TodayHistogramViewModel>() {
                    @Override
                    public void onCompleted() {
                        isLoadingTodayData = false;
                    }

                    @Override
                    public void onError(Throwable e) {
                        HPLogger.d(TAG, "unable to fetch actual vs target data " + e);
                        isLoadingTodayData = false;
                        if (mView != null) {
                            mView.updateHistogram(null);
                        }
                    }

                    @Override
                    public void onNext(TodayHistogramViewModel actualVsTargetViewModel) {
                        if (mView != null) {
                            mView.updateHistogram(actualVsTargetViewModel);
                        }
                    }
                });
        addSubscriber(subscription);
    }

    public void getActualVsTargetGraphData() {
        Subscription subscription = HomeDataManager.getActualVsTargetGraph(selectedBusinessUnit)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<TodayHistogramViewModel>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        HPLogger.d(TAG, "unable to fetch actual vs target graph data " + e);
                        if (mView != null) {
                            mView.updateHistogram(null);
                        }
                    }

                    @Override
                    public void onNext(TodayHistogramViewModel histogramViewModel) {
                        if (mView != null) {
                            mView.updateHistogram(histogramViewModel);
                        }
                    }
                });
        addSubscriber(subscription);
    }

    public void getWeekData(Context context) {

        Subscription subscription = HomeDataManager.getMultipleWeekData(context, selectedBusinessUnit)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<WeekCollectionViewModel>() {
                    @Override
                    public void onCompleted() {

                        logPWPStartup();
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (mView != null) {
                            mView.updateWeekPanel(null);
                        }
                    }

                    @Override
                    public void onNext(WeekCollectionViewModel weekCollectionViewModel) {
                        if (mView != null) {
                            mView.updateWeekPanel(weekCollectionViewModel);
                        }
                    }
                });
        addSubscriber(subscription);

    }

    public void getSiteRankingData(Context context) {

        Subscription subscription = HomeDataManager.getSiteRankingData(context, selectedBusinessUnit).
                observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).subscribe(new Subscriber<RankingViewModel>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (mView != null) {
                            mView.updateRankingPanel(null);
                        }
                    }

                    @Override
                    public void onNext(RankingViewModel rankingViewModel) {
                        if (mView != null) {
                            mView.updateRankingPanel(rankingViewModel);
                        }
                    }
                });
        addSubscriber(subscription);

    }

    private void logPWPStartup() {

        if (selectedBusinessUnit != null && selectedBusinessUnit.getBusinessUnit() == BusinessUnitEnum.IHPS_PRESS) {
            PrintOSApplication.logStartup();
        }
    }

    public void getServiceCalls() {
        Subscription subscription = HomeDataManager.getServiceCallData(selectedBusinessUnit)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<ServiceCallViewModel>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        HPLogger.d(TAG, "unable to fetch the service calls data " + e);
                        if (mView != null) {
                            mView.updateServiceCallPanel(null);
                        }
                    }

                    @Override
                    public void onNext(ServiceCallViewModel serviceCallViewModel) {
                        if (mView != null) {
                            mView.updateServiceCallPanel(serviceCallViewModel);
                        }
                    }
                });
        addSubscriber(subscription);
    }

    public void getPrintersData() {

        Subscription subscription = HomeDataManager.getPWPDevices(selectedBusinessUnit, false).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).subscribe(new Subscriber<List<DeviceViewModel>>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (mView != null) {
                            mView.updatePrintersPanel(null);
                        }
                    }

                    @Override
                    public void onNext(List<DeviceViewModel> deviceViewModels) {

                        TodayViewModel todayViewModelObj = new TodayViewModel();
                        todayViewModelObj.setBusinessUnitViewModel(getSelectedBusinessUnit());
                        todayViewModelObj.setDeviceViewModels(deviceViewModels);
                        todayViewModelObj.setSiteViewModel(getSelectedBusinessUnit().getFiltersViewModel().getSelectedSite());

                        Collections.sort(todayViewModelObj.getDeviceViewModels(), BaseDeviceViewModel.NAME_COMPARATOR);

                        setTodayViewModel(todayViewModelObj);

                        if (mView != null) {
                            mView.updatePrintersPanel(deviceViewModels);
                        }
                    }
                });
        addSubscriber(subscription);
    }

    private void onRequestError(Throwable e, String... tags) {

        APIException exception = APIException.create(APIException.Kind.UNEXPECTED);
        if (e instanceof APIException) {
            exception = (APIException) e;
        }

        if (mView != null) {
            mView.onError(exception, tags);
        }
    }

    public static BusinessUnitViewModel getSelectedBusinessUnit() {
        return selectedBusinessUnit;
    }

    public void setSelectedBusinessUnit(BusinessUnitViewModel selectedBusinessUnit) {
        this.selectedBusinessUnit = selectedBusinessUnit;
    }

    public TodayViewModel getTodayViewModel() {
        return todayViewModel;
    }

    private void setTodayViewModel(TodayViewModel todayViewModel) {
        this.todayViewModel = todayViewModel;
    }

    public static boolean isShiftSupport() {
        return isShiftSupport;
    }

    public void setShiftSupport(boolean shiftSupport) {
        isShiftSupport = shiftSupport;
    }

    public void refresh() {
        stopSubscribers();
    }

    @Override
    public void detachView() {
        stopSubscribers();
        mView = null;
    }

}