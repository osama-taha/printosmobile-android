package com.hp.printosmobile.presentation.modules.devices;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.ProgressBar;

import com.hp.printosmobile.Analytics;
import com.hp.printosmobile.AppseeSdk;
import com.hp.printosmobile.Constants;
import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;
import com.hp.printosmobile.presentation.BaseFragment;
import com.hp.printosmobile.presentation.modules.devicedetails.IndigoDeviceDetailsFragment;
import com.hp.printosmobile.presentation.modules.devicedetails.LatexDeviceDetailsFragment;
import com.hp.printosmobile.presentation.modules.home.Panel;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitEnum;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitViewModel;
import com.hp.printosmobile.presentation.modules.personaladvisor.AdviceViewModel;
import com.hp.printosmobile.presentation.modules.personaladvisor.PersonalAdvisorDialog;
import com.hp.printosmobile.presentation.modules.personaladvisor.PersonalAdvisorViewModel;
import com.hp.printosmobile.presentation.modules.personaladvisor.presenter.PersonalAdvisorFactory;
import com.hp.printosmobile.presentation.modules.shared.DeepLinkUtils;
import com.hp.printosmobile.presentation.modules.shared.DeviceViewModel;
import com.hp.printosmobile.presentation.modules.today.TodayViewModel;
import com.hp.printosmobilelib.core.communications.remote.APIException;
import com.hp.printosmobilelib.core.logging.HPLogger;
import com.hp.printosmobilelib.ui.common.HPFragment;
import com.hp.printosmobilelib.ui.utils.DividerItemDecoration;
import com.hp.printosmobilelib.ui.widgets.HPSegmentedRecyclerView;
import com.hp.printosmobilelib.ui.widgets.HPTextView;
import com.hp.printosmobilelib.ui.widgets.panel.SharableObject;

import java.util.List;

import butterknife.Bind;

/**
 * @author Osama Taha
 */
public class DevicesFragment extends BaseFragment implements DevicesView, DevicesAdapter.DevicesAdapterListener, IndigoDeviceDetailsFragment.IndigoDeviceDetailsFragmentCallbacks, PersonalAdvisorFactory.PersonalAdvisorObserver {

    private static final String TAG = DevicesFragment.class.getName();
    private static final String ADVICE_TAG = "advices";
    private static final String ARG_VIEW_MODEL = "today_view_model_arg";

    @Bind(R.id.recycler_view_devices)
    HPSegmentedRecyclerView devicesRecyclerView;
    @Bind(R.id.loading_view)
    ProgressBar loadingView;
    @Bind(R.id.filtered_devices_text_view)
    HPTextView filteredDevicesTextView;
    @Bind(R.id.error_msg_text_view)
    View errorMsg;

    private DevicesCallbacks listener;
    private DevicesAdapter deviceAdapter;
    private HPFragment deviceDetails;
    private List<DeviceViewModel> deviceDataList;
    private TodayViewModel todayViewModel;

    private boolean fromDevices;

    public DevicesFragment() {
        // Required empty public constructor
    }

    public static DevicesFragment newInstance(TodayViewModel todayViewModel, DevicesCallbacks listener) {

        DevicesFragment devicesFragment = new DevicesFragment();
        devicesFragment.listener = listener;
        Bundle args = new Bundle();
        args.putSerializable(ARG_VIEW_MODEL, todayViewModel);
        devicesFragment.setArguments(args);
        return devicesFragment;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Bundle bundle = getArguments();

        if (bundle != null) {
            todayViewModel = (TodayViewModel) bundle.getSerializable(ARG_VIEW_MODEL);

            initView();

            AppseeSdk.getInstance(getActivity()).startScreen(AppseeSdk.SCREEN_DEVICES);
            Analytics.sendEvent(Analytics.VIEW_SCREEN_EVENT, Analytics.DEVICE_LIST_SCREEN_LABEL);
        }
    }

    private void initView() {

        devicesRecyclerView.setHasFixedSize(true);
        devicesRecyclerView.setContentLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        onGettingDevicesCompleted(todayViewModel.getDeviceViewModels());

        PersonalAdvisorFactory.getInstance().addObserver(this);

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_devices;
    }

    @Override
    public void hideLoading() {
        loadingView.setVisibility(View.INVISIBLE);
    }

    @Override
    public void showLoading() {
        loadingView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onGettingDevicesCompleted(List<DeviceViewModel> deviceDataList) {

        if (!isAdded() || getActivity() == null) {
            return;
        }

        loadingView.setVisibility(View.GONE);

        if (deviceDataList == null || todayViewModel.getBusinessUnitViewModel() == null) {
            devicesRecyclerView.setVisibility(View.INVISIBLE);
            filteredDevicesTextView.setVisibility(View.GONE);
            errorMsg.setVisibility(View.VISIBLE);
            return;
        }

        BusinessUnitViewModel businessUnitViewModel = todayViewModel.getBusinessUnitViewModel();

        this.deviceDataList = deviceDataList;

        updatePersonalAdvisorObserver();

        deviceAdapter = new DevicesAdapter(getContext(), deviceDataList, businessUnitViewModel.getBusinessUnit(), this);
        devicesRecyclerView.setAdapter(deviceAdapter);
        devicesRecyclerView.addItemDecoration(new DividerItemDecoration(getContext(), 1, deviceDataList.size(), false));
        deviceAdapter.notifyDataSetChanged();

        devicesRecyclerView.setSegmentsVisible(businessUnitViewModel.getBusinessUnit() == BusinessUnitEnum.LATEX_PRINTER);
        devicesRecyclerView.setVisibility(View.VISIBLE);

        int selectedDevicesCount = todayViewModel.getDeviceViewModels().size();
        int siteDevicesCount = todayViewModel.getSiteViewModel().getDevices().size();

        if (selectedDevicesCount != siteDevicesCount) {

            int titleResId = businessUnitViewModel.getBusinessUnit() == BusinessUnitEnum.LATEX_PRINTER
                    ? R.string.devices_filter_label_latex : R.string.devices_filter_label;

            filteredDevicesTextView.setText(PrintOSApplication.getAppContext()
                    .getString(titleResId, selectedDevicesCount, siteDevicesCount));

            filteredDevicesTextView.setVisibility(View.VISIBLE);

        } else {
            filteredDevicesTextView.setVisibility(View.GONE);
        }


    }

    public void updateView(TodayViewModel todayViewModel) {

        this.todayViewModel = todayViewModel;
        if (todayViewModel == null) {
            onGettingDevicesCompleted(null);
        } else {
            onGettingDevicesCompleted(todayViewModel.getDeviceViewModels());
        }
    }

    @Override
    public void onError() {
        devicesRecyclerView.setVisibility(View.INVISIBLE);
        errorMsg.setVisibility(View.VISIBLE);
        onGettingDevicesCompleted(null);
    }

    @Override
    public void onEmptyDevicesList() {

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        PersonalAdvisorFactory.getInstance().removeObserver(this);
    }

    @Override
    public void onDeviceClicked(DeviceViewModel model, boolean fromDevices) {

        if (getActivity() == null || getActivity().isFinishing() || !isAdded()) {
            return;
        }

        this.fromDevices = fromDevices;

        attachDeviceDetailsScreen(model);
    }

    @Override
    public void onAdvicesIconClicked(DeviceViewModel model) {

        if (model.hasUnsuppressedAdvices()) {

            PersonalAdvisorDialog dialog = PersonalAdvisorDialog.newInstance(model.getPersonalAdvisorViewModel(),
                    new PersonalAdvisorDialog.PersonalAdvisorDialogCallback() {
                        @Override
                        public void goToPersonalAdvisor(AdviceViewModel adviceViewModel) {
                            onGoToPersonalAdvisorClicked(adviceViewModel);
                        }

                        @Override
                        public void onAdvicePopupDismiss() {

                        }

                        @Override
                        public void onScreenshotCreated(Panel personalAdvisor, final Uri storageUri, final String adviceID, final SharableObject sharableObject) {
                            DeepLinkUtils.getShareBody(getContext(),
                                    Constants.UNIVERSAL_LINK_SCREEN_PAPOPUP,
                                    Panel.PERSONAL_ADVISOR.getDeepLinkTag(), null, adviceID, false,
                                    new DeepLinkUtils.BranchIOCallback() {
                                        @Override
                                        public void onLinkCreated(String link) {
                                            shareImage(storageUri,
                                                    getContext().getString(R.string.share_advice_panel_title),
                                                    link,
                                                    sharableObject);
                                        }
                                    });
                        }
                    });

            dialog.show(getFragmentManager(), ADVICE_TAG);
        }
    }

    @Override
    public void onSortChanged() {
        devicesRecyclerView.getContentRecyclerView().smoothScrollToPosition(0);
    }

    @Override
    public void onStateDistributionClicked(DeviceViewModel model) {
        if (listener != null) {
            listener.onStateDistributionClicked(model);
        }
    }

    public void onFilterSelected() {
        loadingView.setVisibility(View.VISIBLE);
        devicesRecyclerView.getContentRecyclerView().smoothScrollToPosition(0);
    }

    private void attachDeviceDetailsScreen(DeviceViewModel model) {

        if (todayViewModel == null || todayViewModel.getBusinessUnitViewModel() == null) {
            return;
        }

        FragmentTransaction trans = getFragmentManager()
                .beginTransaction();

        switch (todayViewModel.getBusinessUnitViewModel().getBusinessUnit()) {
            case INDIGO_PRESS:
                deviceDetails = IndigoDeviceDetailsFragment.newInstance(model);
                ((IndigoDeviceDetailsFragment) deviceDetails).attachListener(this);
                break;
            case LATEX_PRINTER:
                deviceDetails = LatexDeviceDetailsFragment.newInstance(model);
                break;
            default:
                return;
        }

        trans.replace(R.id.root_frame, deviceDetails);
        trans.commitAllowingStateLoss();

        if (listener != null) {
            listener.onDevicesDetailedClicked(deviceDetails);
        }
    }

    @Override
    public boolean onBackPressed() {

        if (deviceDetails != null) {

            if (!deviceDetails.onBackPressed()) {
                FragmentTransaction trans = getFragmentManager()
                        .beginTransaction();
                trans.remove(deviceDetails);
                trans.commit();
                deviceDetails = null;
                if (listener != null) {
                    listener.onDevicesDetailedClosed(this);
                }

                if (fromDevices) {
                    AppseeSdk.getInstance(getActivity()).startScreen(AppseeSdk.SCREEN_DEVICES);
                    return true;
                } else {
                    return false;
                }
            }

            return true;
        }

        return false;
    }

    @Override
    public boolean isFragmentNameToBeDisplayed() {
        return true;
    }

    @Override
    public boolean isFilteringAvailable() {

        if (deviceDetails != null) {
            return deviceDetails.isFilteringAvailable();
        }

        return true;
    }

    @Override
    public boolean isIntercomAccessible() {
        if (deviceDetails != null) {
            return deviceDetails.isIntercomAccessible();
        }

        return true;
    }

    @Override
    public int getToolbarDisplayName() {

        if (deviceDetails != null) {
            return deviceDetails.getToolbarDisplayName();
        }

        if (getArguments() != null && getArguments().getSerializable(ARG_VIEW_MODEL) != null) {
            todayViewModel = (TodayViewModel) getArguments().getSerializable(ARG_VIEW_MODEL);
        }

        if (todayViewModel != null && todayViewModel.getBusinessUnitViewModel() != null) {
            return todayViewModel.getBusinessUnitViewModel().getBusinessUnit() != BusinessUnitEnum.LATEX_PRINTER
                    ? R.string.devices : R.string.printers;
        }

        return R.string.devices;
    }

    @Override
    public void onNext10JobsClosed(HPFragment fragment) {
        if (listener != null) {
            listener.onDevicesDetailedClicked(fragment);
        }
    }

    @Override
    public void onNext10JobsClicked(HPFragment fragment) {
        if (listener != null) {
            listener.onDetailsLevelChanged(fragment);
        }
    }

    @Override
    public void onGoToPersonalAdvisorClicked(AdviceViewModel model) {
        if (listener != null) {
            listener.onGoToPersonalAdvisorClicked(model);
        }
    }

    @Override
    public void updatePersonalAdvisorObserver() {
        HPLogger.d(TAG, "updating Personal Advisor Observer");
        if (deviceDataList != null) {
            PersonalAdvisorFactory personalAdvisorFactory = PersonalAdvisorFactory.getInstance();
            for (DeviceViewModel deviceViewModel : deviceDataList) {
                PersonalAdvisorViewModel personalAdvisorViewModel = personalAdvisorFactory
                        .getDeviceAdvices(deviceViewModel.getSerialNumber());
                deviceViewModel.setPersonalAdvisorViewModel(personalAdvisorViewModel);
            }
            if (deviceAdapter != null) {
                deviceAdapter.setDeviceData(deviceDataList);
            }
        }
    }

    @Override
    public void onPersonalAdvisorError(APIException e, String tag) {
        //Nothing to do
    }

    @Override
    public boolean isSearchAvailable() {
        return true;
    }

    @Override
    public void onCancel() {

    }

    public interface DevicesCallbacks {

        void onDevicesDetailedClicked(HPFragment hpFragment);

        void onDetailsLevelChanged(HPFragment hpFragment);

        void onDevicesDetailedClosed(HPFragment hpFragment);

        void onGoToPersonalAdvisorClicked(AdviceViewModel model);

        void onStateDistributionClicked(DeviceViewModel model);
    }
}
