package com.hp.printosmobile.presentation.indigowidget;

/**
 * Created by Osama Taha on 2/10/17.
 */
public class Indigo4X3WidgetProvider extends PrintOSAppWidgetProvider {

    @Override
    public WidgetType getWidgetType() {
        return WidgetType.INDIGO_4X3;
    }
}
