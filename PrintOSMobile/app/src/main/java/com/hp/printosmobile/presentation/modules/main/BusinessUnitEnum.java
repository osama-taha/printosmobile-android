package com.hp.printosmobile.presentation.modules.main;

import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;

/**
 * An enum represents the supported business units.
 * <p/>
 * Created by Osama Taha on 5/31/16.
 */
public enum BusinessUnitEnum {

    UNKNOWN("", "", "", "", 0, false, 5, true),
    INDIGO_PRESS("IndigoPress", "Indigo", "Indigo", "IND", R.drawable.indigo_20000, true, 1,true),
    SCITEX_PRESS("ScitexPress", "Scitex", "Pwi", "SCI", R.drawable.scitex_11000, true, 2, true),
    IHPS_PRESS("IHPSPress", "IHPS", "PWP", "PWP", R.drawable.pwp_t240_hd, false, 3 , false),
    LATEX_PRINTER("LatexPrinter", "Latex", "Latex", "LAT", R.drawable.hp_latex_110_printer, true, 4,false);

    private static final int HOME_TAB_INDEX = 0;
    private static final int INSIGHTS_TAB_INDEX = 1;
    private static final int RANKING_LEADERBOARD_TAB_INDEX = 2;
    private static final int JOBS_TAB_INDEX = 3;

    private final int sortOrder;
    private final String gbu;
    private String name;
    private final String kzName;
    private String shortName;
    private final int kzDivisionSwitchImageResId;
    private boolean hasHistogram;
    private final boolean channelSupportKz;

    BusinessUnitEnum(String name, String gbu, String kzName, String shortName, int kzDivisionSwitchImageResId, boolean hasHistogram, int sortOrder, boolean channelSupportKz) {
        this.name = name;
        this.gbu = gbu;
        this.kzName = kzName;
        this.shortName = shortName;
        this.hasHistogram = hasHistogram;
        this.kzDivisionSwitchImageResId = kzDivisionSwitchImageResId;
        this.sortOrder = sortOrder;
        this.channelSupportKz = channelSupportKz;
    }

    public String getName() {
        return name;
    }

    public String getKzName() {
        return kzName;
    }

    public String getGbu() {
        return gbu;
    }

    public String getShortName() {
        return shortName;
    }

    public boolean isHasHistogram() {
        return hasHistogram;
    }

    public int getKzDivisionSwitchImageResId() {
        return kzDivisionSwitchImageResId;
    }

    public boolean isChannelSupportKz() {
        return channelSupportKz;
    }

    public String getBusinessUnitDisplayName(boolean kz) {
        switch (this) {
            case INDIGO_PRESS:
                return PrintOSApplication.getAppContext().getString(R.string.hp_indigo);
            case LATEX_PRINTER:
                return PrintOSApplication.getAppContext().getString(R.string.latex);
            case IHPS_PRESS:
                return PrintOSApplication.getAppContext().getString(kz ? R.string.hp_pwi : R.string.hp_pwp);
            case SCITEX_PRESS:
                return PrintOSApplication.getAppContext().getString(kz ? R.string.hp_pwi_post_print : R.string.hp_scitex);
            default:
                return null;
        }
    }

    public int getSortOrder() {
        return sortOrder;
    }

    public int getBottomTabIcon(boolean selected, int tabIndex) {

        switch (tabIndex) {
            case HOME_TAB_INDEX:
                return selected ? R.drawable.home_selected : R.drawable.home_idle;
            case INSIGHTS_TAB_INDEX:
                return selected ? R.drawable.insights_selected : R.drawable.insights_idle;
            case RANKING_LEADERBOARD_TAB_INDEX:
                return selected ? R.drawable.rank_tab_selected : R.drawable.rank_tab;
            case JOBS_TAB_INDEX:
                return selected ? R.drawable.job_selected : R.drawable.job_idle;
            default:
                return 0;
        }

    }

    public static BusinessUnitEnum from(String businessUnit) {
        if (businessUnit == null) {
            return UNKNOWN;
        }
        for (BusinessUnitEnum businessUnitEnum : BusinessUnitEnum.values()) {
            if (businessUnitEnum.getName().equalsIgnoreCase(businessUnit))
                return businessUnitEnum;
        }
        return BusinessUnitEnum.UNKNOWN;
    }

    public static BusinessUnitEnum fromKzBu(String businessUnit) {

        if (businessUnit == null) {
            return UNKNOWN;
        }

        for (BusinessUnitEnum businessUnitEnum : BusinessUnitEnum.values()) {
            if (businessUnitEnum.getKzName().equalsIgnoreCase(businessUnit))
                return businessUnitEnum;
        }

        return BusinessUnitEnum.UNKNOWN;
    }

    public static BusinessUnitEnum fromGbu(String gbu) {

        if (gbu == null) {
            return UNKNOWN;
        }

        for (BusinessUnitEnum businessUnitEnum : BusinessUnitEnum.values()) {
            if (businessUnitEnum.getGbu().equalsIgnoreCase(gbu))
                return businessUnitEnum;
        }

        return BusinessUnitEnum.UNKNOWN;
    }

}
