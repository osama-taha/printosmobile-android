package com.hp.printosmobile.presentation;

import android.app.Activity;
import android.graphics.Rect;
import android.support.design.widget.TabLayout;
import android.view.View;
import android.webkit.WebView;
import android.widget.LinearLayout;

import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetView;
import com.hp.printosmobile.Analytics;
import com.hp.printosmobile.Constants;
import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.presentation.modules.home.HomePresenter;
import com.hp.printosmobile.presentation.modules.home.Panel;
import com.hp.printosmobile.presentation.modules.insights.KpiInsightPanel;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitEnum;
import com.hp.printosmobile.presentation.modules.main.MainActivity;
import com.hp.printosmobile.presentation.modules.reportkpibreakdown.KpiBreakdownEnum;
import com.hp.printosmobile.presentation.modules.statedistribution.StateDistributionActivity;
import com.hp.printosmobile.presentation.modules.statedistribution.StateDistributionLandscapeActivity;
import com.hp.printosmobile.utils.HPUIUtils;
import com.hp.printosmobilelib.ui.widgets.HPScrollView;
import com.hp.printosmobilelib.ui.widgets.panel.PanelView;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by Osama Taha on 10/1/17.
 */

public class TargetViewManager {

    private static final String TAB_TAG = "tabTag"; //for later use
    public static final String TOOLBAR_TAG = "toolbarTag"; //for later use

    public enum TargetView {

        TODAY_PANEL_STATE_DISTRIBUTION(R.string.tap_target_layout_title_state_distribution,
                R.string.tap_target_layout_body_state_distribution,
                R.id.image_state_distribution,
                Panel.TODAY.getDeepLinkTag(),
                "state distribution",
                "TODAY_PANEL_STATE_DISTRIBUTION",
                Constants.TARGET_VIEW_DISPLAY_PERIOD,
                R.color.c62),

        WEEK_PANEL_KPI_BREAKDOWN_REPORT(R.string.tap_target_layout_title_kpi_breakdown,
                R.string.tap_target_layout_body_kpi_breakdown,
                R.id.image_kpi_drawable,
                Panel.PERFORMANCE.getDeepLinkTag(),
                "kpis",
                "WEEK_PANEL_KPI_BREAKDOWN_REPORT",
                Constants.TARGET_VIEW_DISPLAY_PERIOD,
                R.color.c62b),

        INSIGHTS_TAB(R.string.kz_search_target_view_title,
                R.string.kz_search_target_view_message,
                R.string.fragment_insights_name_key, TAB_TAG,
                "insights tab",
                "INSIGHTS_TAB",
                Constants.TARGET_VIEW_DISPLAY_PERIOD,
                R.color.c62c),

        INSIGHTS_SEARCH_BAR(R.string.kz_search_bar_target_view_title,
                R.string.kz_search_bar_target_view_message,
                R.id.kz_search, "INSIGHTS_SEARCH_BAR",
                "insights search bar",
                "INSIGHTS_SEARCH_BAR",
                Constants.TARGET_VIEW_DISPLAY_PERIOD,
                R.color.c62c),

        INSIGHTS_CORRECTIVE_ACTION_JAMS(R.string.corrective_action_target_view_title,
                R.string.corrective_action_target_view_message,
                R.id.corrective_actions_icon, Panel.JAM_CHART.getDeepLinkTag(),
                "corrective actions",
                "INSIGHTS_CORRECTIVE_ACTION",
                Constants.TARGET_VIEW_DISPLAY_PERIOD,
                R.color.c62c),

        INSIGHTS_CORRECTIVE_ACTION_FAILURE(R.string.corrective_action_target_view_title,
                R.string.corrective_action_target_view_message,
                R.id.corrective_actions_icon, Panel.FAILURE_CHART.getDeepLinkTag(),
                "corrective actions",
                "INSIGHTS_CORRECTIVE_ACTION",
                Constants.TARGET_VIEW_DISPLAY_PERIOD,
                R.color.c62c),

        STATE_DISTRIBUTION_PIE_CHART(R.string.web_view_target_view_title,
                R.string.web_view_target_view_message_state_dis_pie,
                R.id.state_distribution_web_view, StateDistributionActivity.TAG,
                "state distribution portrait",
                "STATE_DISTRIBUTION_PIE_CHART",
                Constants.TARGET_VIEW_DISPLAY_PERIOD,
                R.color.c62),

        STATE_DISTRIBUTION_PIE_CHART_GRAPH(R.string.distribution_graph_target_view_title,
                R.string.distribution_graph_target_view_body,
                R.id.state_distribution_web_view, StateDistributionActivity.TAG,
                "state distribution portrait",
                "STATE_DISTRIBUTION_PIE_CHART_GRAPH",
                Constants.TARGET_VIEW_DISPLAY_PERIOD,
                R.color.c62),

        STATE_DISTRIBUTION_PIE_CHART_ROTATE(R.string.distribution_rotate_target_view_title,
                R.string.distribution_rotate_target_view_body,
                R.id.rotate_msg_text_view, StateDistributionActivity.TAG,
                "state distribution portrait",
                "STATE_DISTRIBUTION_PIE_CHART_ROTATE",
                Constants.TARGET_VIEW_DISPLAY_PERIOD,
                R.color.c62),

        STATE_DISTRIBUTION_LANDSCAPE(R.string.web_view_target_view_title,
                R.string.web_view_target_view_message_state_dis_land,
                R.id.state_distribution_web_view, StateDistributionLandscapeActivity.TAG,
                "state distribution landscape",
                "STATE_DISTRIBUTION_LANDSCAPE",
                Constants.TARGET_VIEW_DISPLAY_PERIOD,
                R.color.c62),

        AVAILABILITY_WEB_VIEW(R.string.web_view_target_view_title,
                R.string.web_view_target_view_message_state_availability,
                R.id.web_view, KpiBreakdownEnum.INDIGO_AVAILABILITY_AGGREGATE.name(),
                "availability kpi report breakdown",
                "AVAILABILITY_WEB_VIEW",
                Constants.TARGET_VIEW_DISPLAY_PERIOD,
                R.color.c62b),

        DAILY_SPOTLIGHT_MORE_INFO(R.string.daily_spot_light_more_info_target_view_title,
                R.string.daily_spot_light_more_info_target_view_msg,
                R.id.item_content, Panel.DAILY_SPOTLIGHT.getDeepLinkTag(),
                "daily spotlight",
                "DAILY_SPOTLIGHT_MORE_INFO",
                Constants.TARGET_VIEW_DISPLAY_PERIOD,
                R.color.c62b),

        RANKING_HISTORY (R.string.tap_target_layout_title_ranking_history,
                R.string.tap_target_layout_body_ranking_history,
                R.id.weekly_printbeat_rank_area_title_text_view, Panel.RANKING.getDeepLinkTag(),
                "ranking history",
                "RANKING_HISTORY1",
                Constants.TARGET_VIEW_DISPLAY_PERIOD,
                R.color.c62b),

        HISTOGRAM_BREAKDOWN (R.string.tap_target_layout_title_histogram_breakdown,
                R.string.tap_target_layout_body_histogram_breakdown,
                R.id.histogram_web_view, Panel.HISTOGRAM.getDeepLinkTag(),
                "histogram breakdown",
                "HISTOGRAM_BREAKDOWN",
                Constants.TARGET_VIEW_DISPLAY_PERIOD,
                R.color.c62b);

        private final int titleResourceId;
        private final int messageResourceId;
        private final int layoutId;
        private final String tag;
        private final String featureName;
        private final String preferenceTag;
        private final int recurringPeriod;
        private final int colorID;

        TargetView(int titleResourceId, int messageResourceId, int layoutId, String tag, String featureName,
                   String preferenceTag, int recurringPeriod, int colorID) {
            this.titleResourceId = titleResourceId;
            this.messageResourceId = messageResourceId;
            this.tag = tag;
            this.layoutId = layoutId;
            this.featureName = featureName;
            this.preferenceTag = preferenceTag;
            this.recurringPeriod = recurringPeriod;
            this.colorID = colorID;
        }

        public String getTitle() {
            return PrintOSApplication.getAppContext().getString(titleResourceId);
        }

        public String getMessage() {
            return PrintOSApplication.getAppContext().getString(messageResourceId);
        }

        public String getTag() {
            return tag;
        }

        public int getColorID() {
            return colorID;
        }

        public void setAsShown() {
            PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).setTargetViewShown(this.preferenceTag);
        }

        public boolean shownBefore() {
            return PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).isTargetViewShown(this.preferenceTag, recurringPeriod);
        }

        public String getFeatureName() {
            return featureName;
        }

        public static List<TargetView> getQueue(String tag) {
            List<TargetView> views = new LinkedList<>();

            if (tag != null) {
                for (TargetView view : TargetView.values()) {
                    if (view.tag.equalsIgnoreCase(tag)) {
                        views.add(view);
                    }
                }
            }

            if(views.isEmpty()) {
                return null;
            }

            return views;
        }
    }

    public static class TargetViewStyle {

        private int outerCircleColor;
        private float outerCircleAlpha;
        private int targetCircleColor;
        private int dimColor;
        private int titleTextColor;
        private int descriptionTextColor;
        private int titleTextSizeSP;
        private int descriptionTextSizeSP;
        private int targetRadiusDP;
    }

    public static TargetViewStyle getDefaultStyle(TargetView targetView) {

        TargetViewStyle targetViewStyle = new TargetViewStyle();
        // Specify a color for the outer circle
        targetViewStyle.outerCircleColor = targetView == null ? R.color.hp_blue : targetView.getColorID();
        // Specify the alpha amount for the outer circle
        targetViewStyle.outerCircleAlpha = 0.70f;
        // Specify a color for the target circle
        targetViewStyle.targetCircleColor = R.color.white;
        targetViewStyle.titleTextColor = R.color.white;
        // Specify a color for description text
        targetViewStyle.descriptionTextColor = R.color.white;
        targetViewStyle.dimColor = R.color.hp_semi_transparent;
        // Specify the size (in sp) of the title text
        targetViewStyle.titleTextSizeSP = 38;
        // Specify the size (in sp) of the description text
        targetViewStyle.descriptionTextSizeSP = 28;
        targetViewStyle.targetRadiusDP = 60;

        return targetViewStyle;
    }

    public static boolean showTargetViewForVisible(Activity activity, HPScrollView scrollView, Map<?, PanelView> panelViewMap,
                                                   TapTargetCallback callback) {

        if (scrollView == null) {
            return false;
        }

        int scrollTop = scrollView.getScrollY();
        int scrollBottom = scrollView.getScrollY() + scrollView.getHeight();

        for (Object key : panelViewMap.keySet()) {
            PanelView panelView = panelViewMap.get(key);
            if (HPUIUtils.isViewVisibleWithinBounds(panelView, scrollTop, scrollBottom)) {
                if(panelView instanceof KpiInsightPanel) {
                    if (TargetViewManager.showTargetView(activity, panelView.getPanelTag(),
                            panelView, callback)) {
                        return true;
                    }
                } else if (TargetViewManager.showTargetView(activity, panelView.getPanelTag(),
                        panelView.getCurrentView(), callback)) {
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean showTargetViewForTab(Activity activity, TabLayout tabLayout, MainActivity.MainPagerAdapter mainPagerAdapter,
                                               final TapTargetCallback callback) {
        if (tabLayout == null || mainPagerAdapter == null || mainPagerAdapter.getFragmentKeys() == null) {
            return false;
        }

        List<TargetView> targetViews = TargetView.getQueue(TAB_TAG);
        if(targetViews != null) {
            for (final TargetView targetView : targetViews) {

                if(targetView == TargetView.INSIGHTS_TAB && !PrintOSPreferences.getInstance(activity).isKnowledgeZoneSearchEnabled()){
                    continue;
                }

                if (!targetView.shownBefore()) {

                    int index = 0;
                    if(targetView.layoutId > 0){
                        index = mainPagerAdapter.getFragmentKeys().indexOf(activity.getString(targetView.layoutId));
                    }

                    if (index > 0) {
                        View tabView = null;

                        LinearLayout tabStrip = ((LinearLayout) tabLayout.getChildAt(0));
                        if (index >= 0 && index < tabStrip.getChildCount()) {
                            tabView = tabStrip.getChildAt(index);
                        }

                        if (tabView != null && tabView.getVisibility() == View.VISIBLE
                                && HPUIUtils.isViewFullyVisible(tabView)) {
                            showTargetView(activity, targetView, tabView, getDefaultStyle(targetView), new TapTargetView.Listener() {
                                @Override
                                public void onTargetClick(TapTargetView view) {
                                    super.onTargetClick(view);
                                    if (callback != null) {
                                        callback.onTapTargetClick(targetView);
                                    }
                                }

                                @Override
                                public void onTargetLongClick(TapTargetView view) {
                                    super.onTargetLongClick(view);
                                    if (callback != null) {
                                        callback.onTapTargetClick(targetView);
                                    }
                                }
                            });
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    private static View getView(View root, int id) {
        if (root != null) {
            return root.findViewById(id);
        }
        return null;
    }

    public static boolean showTargetView(Activity activity, String tag, View rootView, final TapTargetCallback callback) {
        if (tag == null || rootView == null || activity == null) {
            return false;
        }

        List<TargetView> targetViews = TargetView.getQueue(tag);
        if(targetViews != null) {
            for (final TargetView targetView : targetViews) {
                if (!targetView.shownBefore()) {

                    View childView;
                    if(rootView instanceof KpiInsightPanel) {
                        childView = ((KpiInsightPanel) rootView).getViewForTargetView(targetView);
                    } else {
                        childView = getView(rootView, targetView.layoutId);
                    }

                    if (childView != null && childView.getVisibility() == View.VISIBLE && HPUIUtils.isViewFullyVisible(childView)) {
                        showTargetView(activity, targetView, childView, getDefaultStyle(targetView), new TapTargetView.Listener() {
                            @Override
                            public void onTargetClick(TapTargetView view) {
                                super.onTargetClick(view);
                                if (callback != null) {
                                    callback.onTapTargetClick(targetView);
                                }
                            }

                            @Override
                            public void onTargetLongClick(TapTargetView view) {
                                super.onTargetLongClick(view);
                                if (callback != null) {
                                    callback.onTapTargetClick(targetView);
                                }
                            }

                            @Override
                            public void onTargetCancel(TapTargetView view) {
                                super.onTargetCancel(view);
                                if (callback != null) {
                                    callback.onCancel();
                                }
                            }
                        });
                        return true;
                    }
                }
            }
        }
        return false;

    }

    public static void showTargetView(Activity activity, TargetView targetView, View view, TargetViewStyle style, TapTargetView.Listener listener) {
        Rect bounds = new Rect();
        view.getGlobalVisibleRect(bounds);

        if (targetView == TargetView.HISTOGRAM_BREAKDOWN && (HomePresenter.getSelectedBusinessUnit() == null ||
                HomePresenter.getSelectedBusinessUnit().getBusinessUnit() != BusinessUnitEnum.INDIGO_PRESS)) {
            return;
        }

        if(view instanceof WebView) {
            if (targetView == TargetView.STATE_DISTRIBUTION_PIE_CHART_GRAPH) {
                bounds.set(bounds.left, bounds.top, bounds.right, (int)(bounds.bottom/1.275));
            } else {
                bounds.set(bounds.left, (bounds.top + bounds.bottom) / 2, (bounds.left + bounds.right) / 2, bounds.bottom);
            }
        }

        TapTargetView.showFor(activity,
                TapTarget.forBounds(bounds, targetView.getTitle(), targetView.getMessage())
                        .outerCircleColor(style.outerCircleColor)
                        .outerCircleAlpha(style.outerCircleAlpha)
                        .targetCircleColor(style.targetCircleColor)
                        .titleTextSize(style.titleTextSizeSP)
                        .descriptionTextSize(style.descriptionTextSizeSP)
                        .descriptionTextAlpha(1f)
                        .titleTextColor(style.titleTextColor)
                        .descriptionTextColor(style.descriptionTextColor)
                        .dimColor(style.dimColor)
                        .drawShadow(true)                   // Whether to draw a drop shadow or not
                        .cancelable(true)                  // Whether tapping outside the outer circle dismisses the view
                        .tintTarget(true)                  // Whether to tint the target view's color
                        .transparentTarget(true)           // Specify whether the target is transparent (displays the content underneath)
                        .targetRadius(style.targetRadiusDP),
                listener);

        targetView.setAsShown();
        Analytics.sendEvent(Analytics.TARGET_LAYOUT_SHOW_ACTION, targetView.getFeatureName());
    }

    public interface TapTargetCallback {
        void onTapTargetClick(TargetView targetView);

        void onCancel ();
    }
}
