package com.hp.printosmobile.presentation.modules.dailyspotlight;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.res.ResourcesCompat;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;

import com.hp.printosmobile.Constants;
import com.hp.printosmobile.R;
import com.hp.printosmobile.presentation.modules.reportkpibreakdown.KpiBreakdownEnum;
import com.hp.printosmobile.utils.CustomTypefaceSpan;
import com.hp.printosmobile.utils.HPLocaleUtils;
import com.hp.printosmobilelib.ui.widgets.TypefaceManager;

/**
 * Created by Anwar Asbah on 5/7/2018.
 */

public enum DailySpotlightEnum {

    IND_PRINT_VOLUME_IMP(KpiBreakdownEnum.INDIGO_IMPRESSIONS,
            R.string.spotlight_title_ind_print_volume_impression,
            Constants.NONE,
            R.drawable.printing_volume,
            "%s",
            R.string.spotlight_desc_ind_print_volume_impression,
            R.string.spotlight_desc_extra_ind_print_volume_impression,
            false,
            false),
    IND_PRINT_VOLUME_EPM(KpiBreakdownEnum.INDIGO_EPM,
            R.string.spotlight_title_ind_print_volume_epm,
            Constants.NONE,
            R.drawable.printing_volume,
            "%s%%",
            R.string.spotlight_desc_ind_print_volume_emp,
            R.string.spotlight_desc_extra_ind_print_volume_epm,
            true,
            false),
    IND_TECH_JAMS(KpiBreakdownEnum.INDIGO_JAMS,
            R.string.spotlight_title_ind_tech,
            Constants.NONE,
            R.drawable.workweek_technical,
            "%s",
            R.string.spotlight_desc_ind_tech,
            R.string.spotlight_desc_extra_ind_tech_jams,
            true,
            true),
    IND_TECH_FAILS(KpiBreakdownEnum.INDIGO_FAILURES,
            R.string.spotlight_title_ind_tech,
            Constants.NONE,
            R.drawable.workweek_technical,
            "%s",
            R.string.spotlight_desc_ind_tech,
            R.string.spotlight_desc_extra_ind_tech_fails,
            true,
            true),
    IND_AVAILABILITY_SUPPLIES(KpiBreakdownEnum.INDIGO_SUPPLIES_TIME,
            R.string.spotlight_title_ind_availability,
            Constants.NONE,
            R.drawable.workweek_availability,
            "%s%%",
            R.string.spotlight_desc_ind_availability,
            R.string.spotlight_desc_extra_ind_availability_supplies,
            true,
            false),
    IND_AVAILABILITY_JAMS(KpiBreakdownEnum.INDIGO_JAMS_RECOVERY_TIME,
            R.string.spotlight_title_ind_availability,
            Constants.NONE,
            R.drawable.workweek_availability,
            "%s%%",
            R.string.spotlight_desc_ind_availability,
            R.string.spotlight_desc_extra_ind_availability_jams,
            true,
            false),
    IND_AVAILABILITY_RESTARTS(KpiBreakdownEnum.INDIGO_RESTARTS_TIME,
            R.string.spotlight_title_ind_availability,
            Constants.NONE,
            R.drawable.workweek_availability,
            "%s%%",
            R.string.spotlight_desc_ind_availability,
            R.string.spotlight_desc_extra_ind_availability_restarts,
            true,
            false),
    IND_AVAILABILITY_PRINTING(KpiBreakdownEnum.INDIGO_PRINTING_TIME,
            R.string.spotlight_title_ind_availability_printing,
            R.string.spotlight_title_extra_ind_availability_printing,
            R.drawable.workweek_availability,
            "%s%%",
            R.string.spotlight_desc_ind_availability_printing,
            Constants.NONE,
            true,
            false),
    IND_AVAILABILITY_FAILURES(KpiBreakdownEnum.INDIGO_FAILURE_RECOVERY_TIME,
            R.string.spotlight_title_ind_availability,
            Constants.NONE,
            R.drawable.workweek_availability,
            "%s%%",
            R.string.spotlight_desc_ind_availability,
            R.string.spotlight_desc_extra_ind_availability_failures,
            true,
            false),
    IND_RESTARTS(KpiBreakdownEnum.INDIGO_RESTART_OCCURRENCES,
            R.string.spotlight_title_ind_restarts,
            Constants.NONE,
            R.drawable.restart,
            "%s",
            R.string.spotlight_desc_ind_restarts,
            R.string.spotlight_desc_extra_ind_restarts,
            true,
            false),
    IND_SUPPLIES_PIP(KpiBreakdownEnum.INDIGO_PIP_REPLACEMENTS,
            R.string.spotlight_title_ind_supplies,
            R.string.spotlight_title_extra_ind_supplies_pip,
            R.drawable.supplied_yield,
            "%s",
            R.string.spotlight_desc_ind_supplies,
            R.string.spotlight_desc_extra_ind_supplies,
            true,
            true),
    IND_SUPPLIES_BLANKET(KpiBreakdownEnum.INDIGO_BLANKET_REPLACEMENTS,
            R.string.spotlight_title_ind_supplies,
            R.string.spotlight_title_extra_ind_supplies_blanket,
            R.drawable.supplied_yield,
            "%s",
            R.string.spotlight_desc_ind_supplies,
            R.string.spotlight_desc_extra_ind_supplies,
            true,
            true),
    UNKNOWN(KpiBreakdownEnum.UNKNOWN, -1, -1, -1, "", -1, -1,
            false, false);

    private int titleID;
    private int titleExtraID;
    private int drawableID;
    private String valueFormat;
    private int descriptionID;
    private int descriptionExtraID;
    private KpiBreakdownEnum rootBreakdownEnum;
    private boolean average;
    private boolean round;

    DailySpotlightEnum(KpiBreakdownEnum rootBreakdownEnum, int titleID, int titleExtraID, int drawableID,
                       String valueFormat, int descriptionID, int descriptionExtraID, boolean average, boolean round) {
        this.titleID = titleID;
        this.titleExtraID = titleExtraID;
        this.drawableID = drawableID;
        this.valueFormat = valueFormat;
        this.descriptionID = descriptionID;
        this.descriptionExtraID = descriptionExtraID;
        this.rootBreakdownEnum = rootBreakdownEnum;
        this.average = average;
        this.round = round;
    }

    public Spannable getTitle(Context context) {
        if (context == null || this == UNKNOWN) {
            return new SpannableString("");
        }

        String extra = titleExtraID == Constants.NONE ? null : context.getString(titleExtraID);
        if (extra == null) {
            return new SpannableString(context.getString(titleID));
        }

        String title = context.getString(titleID, extra);
        int start = title.indexOf(extra);
        int end = start + extra.length();

        Spannable spannable = new SpannableString(title);
        if (start < 0) {
            return spannable;
        }

        spannable.setSpan(new CustomTypefaceSpan("", TypefaceManager.getTypeface(context, TypefaceManager.HPTypeface.HP_BOLD)), start, end, Spanned.SPAN_INCLUSIVE_INCLUSIVE);
        return spannable;
    }

    public Drawable getDrawable(Context context) {
        if (this == UNKNOWN) {
            return ResourcesCompat.getDrawable(context.getResources(), R.drawable.printing_volume, null);
        }
        return ResourcesCompat.getDrawable(context.getResources(), drawableID, null);
    }

    public String getFormattedValue(float value) {
        return String.format(valueFormat, HPLocaleUtils.getDecimalString(value, true, 2));
    }

    public Spannable getDescription(Context context, String extraString, String unit) {
        if (context == null || this == UNKNOWN) {
            return new SpannableString("");
        }

        String extra = descriptionExtraID == Constants.NONE ? null : extraString != null ?
                context.getString(descriptionExtraID, extraString) :
                context.getString(descriptionExtraID);

        if (extra == null) {
            return unit == null ? new SpannableString(context.getString(descriptionID, "")) :
                    new SpannableString(context.getString(descriptionID, unit));
        }

        String description = unit == null ? context.getString(descriptionID, extra) :
                context.getString(descriptionID, extra, unit);
        int start = description.indexOf(extra);
        int end = start + extra.length();

        Spannable spannable = new SpannableString(description);
        if (start < 0) {
            return spannable;
        }

        if (TextUtils.isEmpty(extra)) {
            return spannable;
        }
        spannable.setSpan(new CustomTypefaceSpan("", TypefaceManager.getTypeface(context, TypefaceManager.HPTypeface.HP_BOLD)), start, end, Spanned.SPAN_INCLUSIVE_INCLUSIVE);
        return spannable;
    }

    public KpiBreakdownEnum getRootBreakdownEnum() {
        return rootBreakdownEnum;
    }

    public boolean isAverage() {
        return average;
    }

    public boolean shouldRound() {
        return round;
    }

    public static DailySpotlightEnum from(String name) {
        if (name == null) {
            return UNKNOWN;
        }
        for (DailySpotlightEnum dailySpotlightEnum : DailySpotlightEnum.values()) {
            if (name.equalsIgnoreCase(dailySpotlightEnum.name())) {
                return dailySpotlightEnum;
            }
        }

        return UNKNOWN;
    }

}
