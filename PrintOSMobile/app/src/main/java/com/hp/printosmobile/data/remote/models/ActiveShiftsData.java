package com.hp.printosmobile.data.remote.models;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Osama Taha on 10/4/16.
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ActiveShiftsData {

    @JsonProperty("siteContainShifts")
    private boolean siteContainShifts;
    @JsonProperty("dayShift")
    private ShiftData dayShift;
    @JsonProperty("currShift")
    private ShiftData currShift;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<>();

    /**
     * @return The siteContainShifts
     */
    @JsonProperty("siteContainShifts")
    public boolean getSiteContainShifts() {
        return siteContainShifts;
    }

    /**
     * @param siteContainShifts The siteContainShifts
     */
    @JsonProperty("siteContainShifts")
    public void setSiteContainShifts(boolean siteContainShifts) {
        this.siteContainShifts = siteContainShifts;
    }

    /**
     * @return The dayShift
     */
    @JsonProperty("dayShift")
    public ShiftData getDayShift() {
        return dayShift;
    }

    /**
     * @param dayShift The dayShift
     */
    @JsonProperty("dayShift")
    public void setDayShift(ShiftData dayShift) {
        this.dayShift = dayShift;
    }

    /**
     * @return The currShift
     */
    @JsonProperty("currShift")
    public ShiftData getCurrShift() {
        return currShift;
    }

    /**
     * @param currShift The currShift
     */
    @JsonProperty("currShift")
    public void setCurrShift(ShiftData currShift) {
        this.currShift = currShift;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}