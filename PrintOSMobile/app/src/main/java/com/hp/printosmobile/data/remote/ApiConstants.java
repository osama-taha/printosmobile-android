package com.hp.printosmobile.data.remote;

/**
 * A class to hold the API constants.
 *
 * @author Osama Taha
 */
public final class ApiConstants {

    public static final String BOX_SERVICE = "api/dashboard/stats";

    private static final String PRINT_BEAT_SERVICES = "api/PrintbeatService/";

    private static final String AAA_V4 = "/api/aaa/v4/";

    public static final String CONFIG_DEVICES_API = PRINT_BEAT_SERVICES + "Config/devices?bu=*";
    public static final String CONFIG_ORGANIZATION_DEVICES_API = AAA_V4 + "organizations/devices?limit=10000&offset=0";
    public static final String DEVICES_STATISTICS_API = "/api/system-statistics/v2/devices/latest";
    public static final String CONFIG_SCOREABLES_API = PRINT_BEAT_SERVICES + "Config/scoreables";

    public static final String KPI_META_DATA_API = PRINT_BEAT_SERVICES + "Metadata/kpi";
    public static final String PERMISSIONS_META_DATA_API = PRINT_BEAT_SERVICES + "Metadata/permissions";

    public static final String PERFORMANCE_TRACKING_WEEKLY_REPORT_API = PRINT_BEAT_SERVICES + "gsb/reports";
    public static final String PERFORMANCE_TRACKING_WEEKLY_REPORT_API_V2 = PERFORMANCE_TRACKING_WEEKLY_REPORT_API + "/v2";
    public static final String PERFORMANCE_TRACKING_WEEKLY_OVERALL_REPORT_API = PRINT_BEAT_SERVICES + "gsb/weekly/overall";
    public static final String SCOREABLE_API = PRINT_BEAT_SERVICES + "gsb/Scoreable/{kpiName}";
    public static final String SCOREABLE_API_V2 = PRINT_BEAT_SERVICES + "gsb/Scoreable/v2/{kpiName}";

    public static final String REALTIME_TRACKING_MANY = PRINT_BEAT_SERVICES + "RealtimeTracking/many";
    public static final String REALTIME_TRACKING_MANY_V2 = REALTIME_TRACKING_MANY + "/v2";
    public static final String REALTIME_TRACKING_TARGET = PRINT_BEAT_SERVICES + "RealtimeTracking/target/many";
    public static final String REALTIME_TRACKING_TARGET_V2 = REALTIME_TRACKING_TARGET + "/v2";
    public static final String REALTIME_TRACKING_ACTUAL_VS_TARGET = PRINT_BEAT_SERVICES + "RealtimeTracking/actualVStarget/many";
    public static final String REALTIME_TRACKING_ACTUAL_VS_TARGET_V2 = REALTIME_TRACKING_ACTUAL_VS_TARGET + "/v2";
    public static final String REALTIME_TRACKING_ACTUAL_VS_TARGET_GRAPH = "api/PrintbeatService/RealtimeTracking/actualVStarget/LastPvData";
    public static final String REALTIME_ACTIVE_SHIFTS = PRINT_BEAT_SERVICES + "Shift/ActiveShifts";
    public static final String REALTIME_GRAPH_API = PRINT_BEAT_SERVICES + "RealtimeTracking/actualVStarget/TodayVsLastWeek";

    public static final String PERSONAL_ADVISOR_API = PRINT_BEAT_SERVICES + "Advisor";
    public static final String PERSONAL_ADVISOR_LIKE_API = PRINT_BEAT_SERVICES + "Advisor/user/like/{adviceId}";
    public static final String PERSONAL_ADVISOR_SUPPRESS_API = PRINT_BEAT_SERVICES + "Advisor/user/suppress/{adviceId}";
    public static final String PERSONAL_ADVISOR_UNHIDE_ALL_API = PRINT_BEAT_SERVICES + "Advisor/user/suppress/many";

    public static final String NOTIFICATION_SERVICE_REGISTRATION_API = "api/notification-service/v1/mobile?os=android";

    public static final String EULA_URL = "/api/portal/v1/i18n/eula/{language}";
    public static final String EULA_ACCEPT_URL = AAA_V4 + "users/eula";
    public static final String PRIVACY_ACCEPT_URL = AAA_V4 + "users/privacy";

    public static final String RESET_PASS_API = "/api/email-service/v1/invitations/resetpw";

    public static final String CONTACT_HP_URL = "/api/supportapp-service/v1/cases";

    //AAA users/orgs
    public static final String USERS_URL = AAA_V4 + "users";
    public static final String GET_ORGANIZATION_URL = AAA_V4 + "users/context?limit=10000";
    public static final String CHANGE_ORGANIZATION_CONTEXT_URL = AAA_V4 + "users/context";
    public static final String USER_ROLES_API = AAA_V4 + "users/roles/";
    public static final String ORGANIZATION_DATA_API = AAA_V4 + "organizations";
    public static final String ORGANIZATION_MEMBERS_API = ORGANIZATION_DATA_API + "/users/names";

    public static final String SUPPORTED_VERSIONS_URL = "/api/PrintbeatService/mobile/version";

    public static final String NOTIFICATION_SUBSCRIPTION_API = "api/notification-service/v1/subscription";
    public static final String NOTIFICATIONS_REMOVAL_API = "api/notification-service/v1/subscription/{eventDescID}";
    public static final String NOTIFICATIONS_STRINGS_API = "api/notification-service/v1/i18n/notification/{locale}";
    public static final String NOTIFICATIONS_ITEMS_API = "api/notification-service/v1/";
    public static final String NOTIFICATIONS_ACTION_API = "api/notification-service/v1/event/{id}";
    public static final String NOTIFICATION_SUBSCRIPTION_DESTINATION = "api/notification-service/v1/subscription/{subscription_id}?toggle=mobile";
    public static final String NOTIFICATION_APPLICATION_API = "api/notification-service/v1/application";
    public static final String NOTIFICATION_EVENT_DESCRIPTION_API = "api/notification-service/v1/application/{application_id}";

    public static final String INSIGHTS_API = "api/PrintbeatService/PerformanceTracking/DrillDown/charts";

    public static final String SERVICE_CALL_API = "api/PrintbeatService/serviceCalls";

    //
    public static final String PREFERENCES_API = "api/portal/v1/preferences";
    public static final String PREFERENCES_TIME_ZONES_API = "api/portal/v1/preferences/choices?appId=general&name=timeZone";
    public static final String HPID_EDIT_MY_DETAILS_URL = "/api/portal/v1/client/config";
    public static final String COUNTRIES_API = "/api/portal/v1/preferences/countries";
    //

    public static final String NPS_URL = "https://delighted.com/t/cXOs4aVP?score=%1$d&locale=%2$s&week=%3$s&year=%4$s&source=%5$s&name=%6$s&%7$s&appVersion=%8$s&appPlatform=Android";

    public static final String NOTIFICATIONS_API = PRINT_BEAT_SERVICES + "notifications/data/{id}";

    public static final String USER_INFO_API = PRINT_BEAT_SERVICES + "Config/userInfo";

    public static final String PROFILE_INFO_API = "api/portal/v1/profile";
    public static final String PROFILE_IMAGE_API = "api/portal/v1/profile/image";

    public static final String INVITES_BASE_API = "api/email-service/v1/invitations";
    public static final String INVITES_BEAT_COINS_API = "api/PrintbeatService/mobile/userLogin?platform=Android";
    public static final String INVITES_SEND_USER_ID_API = "api/PrintbeatService/mobile/invitation";
    public static final String INVITES_LIST_API = INVITES_BASE_API + "?limit=1000&offset=0&type=USER_IN_ORG";
    public static final String INVITES_USER_ROLE_API = INVITES_BASE_API + "/new";
    public static final String INVITES_API = INVITES_BASE_API + "/v2";
    public static final String INVITES_REVOKE_API = INVITES_BASE_API + "/revoke/{id}";
    public static final String INIVTES_RESEND_API = INVITES_BASE_API + "/resend/v2";

    public static final String COINS_BASE_URL = "https://api.cogginsstore.com/";

    public static final String STATE_DISTRIBUTION_API = PRINT_BEAT_SERVICES + "RealtimeTracking/distribution";

    //Knowledge zone APIs
    public static final String KNOWLEDGE_ZONE_API = "api/kcService/";
    public static final String KNOWLEDGE_ZONE_SEARCH = KNOWLEDGE_ZONE_API + "Search/";
    public static final String KNOWLEDGE_ZONE_RECOMMENDED_CONTENT = KNOWLEDGE_ZONE_API + "Search/home";
    public static final String KNOWLEDGE_ZONE_SUGGESTIONS = KNOWLEDGE_ZONE_API + "userdata/suggestions/";
    public static final String KNOWLEDGE_ZONE_BU = KNOWLEDGE_ZONE_API + "bu";
    public static final String KNOWLEDGE_ZONE_SET_FAVORITE = KNOWLEDGE_ZONE_API + "favorites";
    public static final String KNOWLEDGE_ZONE_RATE_ITEM = KNOWLEDGE_ZONE_API + "rating";
    public static final String KNOWLEDGE_ZONE_FAVORITES = KNOWLEDGE_ZONE_API + "favorites";

    public static final String KNOWLEDGE_ZONE_ASSET = KNOWLEDGE_ZONE_API + "assetmanagement/asset";
    public static final String KNOWLEDGE_ZONE_PDF_VIEWER = "/knowledge-zone/lazy.modules/pdf-viewer/pdfjs/web/viewer.html?fileName=%1$s&file=%2$s";

    public static final String SITE_RANKING_API = PRINT_BEAT_SERVICES + "ranking";
    public static final String RANKING_LEADERBOARD_API = PRINT_BEAT_SERVICES + "ranking/leaderboard";
    public static final String ORGANIZATION_SETTINGS_API = PRINT_BEAT_SERVICES + "settings/organization";
    public static final String REQUEST_LEADERBOARD_PERMISSION_API = ORGANIZATION_SETTINGS_API + "/reqLeaderboardPermission";
    public static final String RANK_BREAKDOWN_API = PRINT_BEAT_SERVICES + "ranking/history";

    private ApiConstants() {

    }

}
