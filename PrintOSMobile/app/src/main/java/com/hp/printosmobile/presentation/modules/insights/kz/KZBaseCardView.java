package com.hp.printosmobile.presentation.modules.insights.kz;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.transition.ChangeBounds;
import android.support.transition.TransitionManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.text.Html;
import android.text.Spannable;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hp.printosmobile.AnswersSdk;
import com.hp.printosmobile.R;
import com.hp.printosmobile.data.remote.models.kz.KZItem;
import com.hp.printosmobile.presentation.modules.insights.kz.kzfooter.KZFooter;
import com.hp.printosmobile.presentation.modules.insights.kz.kzfooter.KZItemDetailsCallback;
import com.hp.printosmobile.utils.HPStringUtils;
import com.hp.printosmobile.utils.HPUIUtils;
import com.hp.printosmobile.utils.ShareUtils;
import com.hp.printosmobilelib.ui.widgets.panel.PanelView;
import com.hp.printosmobilelib.ui.widgets.panel.SharableObject;

import butterknife.Bind;


/**
 * Created by Osama Taha
 */
public abstract class KZBaseCardView extends PanelView<KZItem> {

    private static final long EXPAND_COLLAPSE_ANIMATION_DURATION = 125;

    @Bind(R.id.cell_content)
    View cellContent;
    @Bind(R.id.card_background)
    View cardBackground;
    @Bind(R.id.title_tv)
    TextView titleTextView;
    @Bind(R.id.description_tv)
    TextView descriptionTextView;
    @Bind(R.id.expanded_view)
    View expandedView;
    @Bind(R.id.expand_layout)
    View expandLayout;
    @Bind(R.id.expand_icon)
    ImageView expandIcon;
    @Bind(R.id.expand_tv)
    TextView expandLabel;
    @Bind(R.id.kz_footer)
    KZFooter kzFooter;

    protected String selectedBu;
    private static KZItemDetailsCallback callback;
    protected Activity activity;

    public KZBaseCardView(Context context) {
        super(context);
        activity = (Activity) context;
    }

    public KZBaseCardView(Activity context, String selectedBu) {
        super(context);
        this.activity = context;
        this.selectedBu = selectedBu;
    }

    public static void addKZItemDetailsCallback(KZItemDetailsCallback kzItemDetailsCallback) {
        callback = kzItemDetailsCallback;
    }

    @Override
    public Spannable getTitleSpannable() {
        return null;
    }

    @Override
    public int getContentView() {
        return R.layout.kz_item_card;
    }

    @Override
    public void updateViewModel(KZItem viewModel) {
        setViewModel(viewModel);
        setUpCard(viewModel);
    }

    public void setKZFooter(final KZItem kzItem) {

        setViewModel(kzItem);

        kzFooter.setKZProps(kzItem.getId(), selectedBu, kzItem.getName());
        kzFooter.setIsFavorite((kzItem.getUserAssetPref() == null || kzItem.getUserAssetPref().getFavorite() == null) ? false : kzItem.getUserAssetPref().getFavorite());

        if (kzItem.getUserAssetPref() != null && kzItem.getUserAssetPref().getRating() != null && kzItem.getUserAssetPref().getRating() != 0) {
            kzFooter.setIsLiked(kzItem.getUserAssetPref().getRating());
        }

        kzFooter.setEventListener(new KZFooter.UserInteractionsEventListener() {
            @Override
            public void onFavoritePressed(boolean isFavorite) {
                kzItem.getUserAssetPref().setFavorite(isFavorite);
                if (callback != null) {
                    callback.onFavoritePressed(kzItem);
                }
            }

            @Override
            public void onLikePressed(int rating) {
                kzItem.getUserAssetPref().setRating(rating);
                if (callback != null) {
                    callback.onLikePressed(kzItem);
                }
            }

            @Override
            public void onDislikePressed(int rating) {
                kzItem.getUserAssetPref().setRating(rating);
                if (callback != null) {
                    callback.onDislikePressed(kzItem);
                }
            }

            @Override
            public void onSharePressed(String name, String link) {
                ShareUtils.onScreenshotCreated(activity, null,
                        activity.getString(R.string.corrective_actions_share_insights_kz_item, name),
                        link,
                        new SharableObject() {
                            @Override
                            public String getShareAction() {
                                return AnswersSdk.SHARE_CONTENT_TYPE_KNOWLEDGE_ZONE;
                            }
                        });
            }
        });
    }


    protected void setUpTitle() {

        if (getViewModel() == null) {
            return;
        }

        String name = getViewModel().getName();
        if (!TextUtils.isEmpty(name)) {
            titleTextView.setText(name);
        }
    }

    protected void setUpCard(KZItem viewModel) {

        if (getViewModel() == null) {
            return;
        }

        int cardBackgroundColor = ContextCompat.getColor(getContext(),
                KZSupportedFormat.from(viewModel.getFormat()).getCardBackgroundColorResId());
        int cardElementsColor = ContextCompat.getColor(getContext(),
                KZSupportedFormat.from(viewModel.getFormat()).getCardDefaultColorResId());

        titleTextView.setTextColor(cardElementsColor);
        descriptionTextView.setTextColor(cardElementsColor);

        cellContent.setBackgroundColor(cardBackgroundColor);
        expandLabel.setTextColor(cardElementsColor);

        if (this instanceof KZDefaultView) {

            Drawable background = ContextCompat.getDrawable(getContext(), R.drawable.pdf_card);
            DrawableCompat.setTint(background, cardElementsColor);
            cardBackground.setBackground(background);

            expandIcon.setImageResource(R.drawable.more_details);
            DrawableCompat.setTint(expandIcon.getDrawable(), cardElementsColor);

        }

        setKZFooter(viewModel);
        setUpTitle();
        setUpDescription(viewModel, null);

        expandLayout.setOnClickListener(new View.OnClickListener() {

            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View view) {

                boolean shouldExpand = expandedView.getVisibility() == View.GONE;

                if (shouldExpand) {
                    expandedView.setVisibility(View.VISIBLE);
                    expandIcon.setImageResource(R.drawable.less_details);
                    expandLabel.setText(R.string.kpi_explanation_less_details);
                } else {
                    expandedView.setVisibility(View.GONE);
                    expandIcon.setImageResource(R.drawable.more_details);
                    expandLabel.setText(R.string.kpi_explanation_more_details);
                }

                int cardElementsColor = ContextCompat.getColor(getContext(),
                        KZSupportedFormat.from(getViewModel().getFormat()).getCardDefaultColorResId());
                DrawableCompat.setTint(expandIcon.getDrawable(), cardElementsColor);

                final ViewGroup transitionsContainer = findViewById(R.id.expanded_view);
                ChangeBounds transition = new ChangeBounds();
                transition.setDuration(EXPAND_COLLAPSE_ANIMATION_DURATION);
                TransitionManager.beginDelayedTransition(transitionsContainer, transition);
                setActivated(shouldExpand);
            }
        });
    }

    public void setUpDescription(KZItem kzItem, String searchQuery) {

        HPUIUtils.setVisibilityForViews(!TextUtils.isEmpty(kzItem.getHighlight()), expandLayout);

        String highlight = kzItem.getHighlight();

        if (!TextUtils.isEmpty(highlight)) {

            String highlightedText = highlight;

            if (!TextUtils.isEmpty(searchQuery)) {

                highlightedText = HPStringUtils.getHighlightedText(highlight, searchQuery);
            }

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                descriptionTextView.setText(Html.fromHtml(highlightedText, Html.FROM_HTML_MODE_LEGACY));
            } else {
                descriptionTextView.setText(Html.fromHtml(highlightedText));
            }

        } else {
            descriptionTextView.setText("");
        }
    }

    @Override
    protected boolean showPanelHeader() {
        return false;
    }

    @Override
    protected String getEmptyCardText() {
        return null;
    }

    @Override
    protected boolean isPanelRounded() {
        return true;
    }

}
