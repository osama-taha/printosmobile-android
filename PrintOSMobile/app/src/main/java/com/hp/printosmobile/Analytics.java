package com.hp.printosmobile;

import android.app.Application;
import android.os.Bundle;
import android.text.TextUtils;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.data.remote.models.AccountType;
import com.hp.printosmobile.presentation.modules.shared.UserViewModel;
import com.hp.printosmobilelib.core.logging.HPLogger;
import com.hp.printosmobilelib.core.utils.DeviceUtils;

/**
 * Helps with logging custom events and errors to Google Analytics.
 * <p/>
 * Created by Osama on 1/3/2016.
 */
public class Analytics {

    private static final String TAG = Analytics.class.getName();

    //SCREEN NAMES
    public static final String MAIN_SCREEN_NAME = "main";

    private static final String USER_ID_PARAMETER = "&uid";

    //MAIN CATEGORY FOR EVENTS
    public static final String MOBILE_PRINT_BEAT_CATEGORY = "Mobile Print Beat";

    //LOGIN EVENTS:
    public static final String LOGIN_EVENT_LOGIN_ACTION = "login";
    public static final String LOGIN_EVENT_LOGOUT_ACTION = "logout";
    public static final String LOGIN_EVENT_AUTO_LOGIN_ACTION = "Automatic Login";
    public static final String LOGIN_EVENT_AUTO_LOGIN_FAILED_ACTION = "Automatic Login Failed";
    public static final String LOGIN_EVENT_EULA_LABEL = "EULA";
    public static final String LOGIN_EVENT_LOCKED_LABEL = "LOCKED";
    public static final String LOGIN_EVENT_EXPIRED_LABEL = "EXPIRED";
    public static final String LOGIN_EVENT_CREDENTIALS_LABLE = "CREDENTIALS";
    public static final String LOGIN_EVENT_LOGIN_OTHER_LABEL = "OTHER<%s>";

    //CHART CLICKED
    public static final String CHART_CLICKED_ACTION = "chart clicked";

    //SETTINGS EVENTS:
    public static final String ABOUT_CLICKED_ACTION = "about clicked";
    public static final String ASK_THE_EXPERT_CLICKED_ACTION = "ask the expert clicked";
    public static final String OPEN_SOURCE_CLICKED_ACTION = "open sources license clicked";

    //PERSONAL ADVISOR EVENTS:
    public static final String PERSONAL_ADVISOR_ACTION = "personal adviser";
    public static final String PERSONAL_ADVISOR_SWIPE_LEFT_LABEL = "swipe left";
    public static final String PERSONAL_ADVISOR_SWIPE_RIGHT_LABEL = "swipe right";
    public static final String PERSONAL_ADVISOR_DELETE_LABEL = "delete";
    public static final String PERSONAL_ADVISOR_LIKE_LABEL = "like";
    public static final String PERSONAL_ADVISOR_UNLIKE_LABEL = "unlike";
    public static final String PERSONAL_ADVISOR_UNHIDE_ALL = "unhide all";

    public static final String WEEK_VIEW_ACTION = "week view";
    public static final String WEEK_VIEW_SHOW_LABEL = "Week-%d";

    //REPORTS EVENTS:
    public static final String REPORT_ACTION = "report";
    public static final String REPORT_SET_REPORT_TYPE_ACTION = "set report type";
    public static final String REPORT_SHOW_VALUES_LABEL = "show values";
    public static final String REPORT_HIDE_VALUES_LABEL = "hide values";
    public static final String REPORT_ZOOM_IN_LABEL = "zoom in";
    public static final String REPORT_ZOOM_OUT_LABEL = "zoom out";

    //LANGUAGE EVENTS:
    public static final String SETTINGS_CHANGE_LANGUAGE_ACTION = "change language";
    public static final String SETTINGS_CHANGE_UNIT_SYSTEM_ACTION = "change unit system";

    //CONTACT HP
    public static final String CONTACT_HP_SCREENSHOT_LABEL = "Screenshot";

    //ASK THE EXPERT EVENTS:
    public static final String ASK_THE_EXPERT_SEND_ACTION = "send ask the expert";

    //REFRESH EVENTS:
    public static final String AUTO_REFRESH_ACTION = "auto refresh";
    public static final String PULL_TO_REFRESH_ACTION = "pull to refresh";

    // New events
    public static final String SWITCH_EQUIPMENT_ACTION = "switch equipment";
    public static final String VIEW_DEVICE_DETAILS_ACTION = "view device details";
    public static final String REPORT_A_PROBLEM_CLICKED_ACTION =
            "report a problem Clicked";
    public static final String REPORT_A_PROBLEM_SEND_ACTION = "send report a problem";
    public static final String FEEDBACK_TO_HP_CLICKED_ACTION =
            "feedback to HP clicked";
    public static final String FEEDBACK_TO_HP_SEND_ACTION = "send feedback to HP";
    public static final String VIEW_NEXT_10_JOBS_ACTION = "view next 10 jobs";
    public static final String VIEW_SERVICE_CALLS_ACTION = "view service calls";
    public static final String SWITCH_ACCOUNT_ACTION = "switch account";
    public static final String FORGOT_PASS_CLICKED_ACTION =
            "Forgot Password clicked";
    public static final String REGISTER_TO_PRINTOS_CLICKED_ACTION = "register to printos clicked";
    public static final String FORGOT_PASS_REQUEST_SENT_ACTION =
            "Forgot Password request sent";
    public static final String LOGIN_FAILED_ACTION = "Login failed";

    public static final String VIEW_NOTIFICATIONS_LIST_ACTION = "view notifications list";
    public static final String VIEW_NOTIFICATIONS_SETTINGS_ACTION = "view notifications settings";
    public static final String VIEW_CHANGE_ORGANIZATION_ACTION = "view change organization";
    public static final String GOOGLE_PLAY_SERVICES_STATUS_ACTION = "Google Play Services Status";

    public static final String VIEW_LANDSCAPE_HISTOGRAM_ACTION = "view histogram in landscape";


    public static final String SHOW_SEND_INVITES_ACTION = "send invites popup shown";
    public static final String SEND_INVITES_CLICK_CLOSE_ACTION = "send invites close button clicked";
    public static final String SEND_INVITES_CLICK_SEND_ACTION = "send invites button clicked";

    public static final String SHOW_NPS_ACTION = "nps popup shown";
    public static final String SUBMIT_NPS_ACTION = "submit nps";
    public static final String SHOW_BEAT_COINS_ACTION = "beat coins popup shown";
    public static final String COLLECT_COINS_CLICKED_ACTION = "collect coins clicked";

    public static final String APP_OPENED_ACTION = "app opened";

    public static final String SHOW_NOTIFICATION_ACTION = "show notification";
    public static final String CLICK_NOTIFICATION_ACTION = "click notification";
    public static final String UNSUBSCRIBE_NOTIFICATION_ACTION = "unsubscribe notification";
    public static final String SUBSCRIBE_NOTIFICATION_ACTION = "subscribe notification";
    public static final String NOTIFICATION_DELETED_ACTION = "notification deleted";
    public static final String NOTIFICATION_MARKED_READ_ACTION = "notification marked read";

    public static final String ACCEPT_LOCATION_SERVICES = "accept location services";
    public static final String DENY_LOCATION_SERVICES = "deny location services";
    public static final String OPEN_APP_SETTINGS = "click go to settings";

    private static final String FIREBASE_ANALYTICS_PUSH_NOTIFICATION_EVENT = "pb_push_notification";
    private static final String FIREBASE_ANALYTICS_PUSH_NOTIFICATION_ARRIVE_EVENT = "pb_push_notification_arrived";
    private static final String FIREBASE_ANALYTICS_PUSH_NOTIFICATION_TYPE_KEY = "pb_notification_type";
    private static final String FIREBASE_CURRENT_STACK = "current_stack";
    private static final String FIREBASE_USER_ID = "printos_user";

    //Invites
    public static final String SHOW_INVITE_SCREEN_ACTION = "printos invite screen shown";
    public static final String INVITE_VIA_SUGGESTED_ACTION = "printos invite via suggested contact";
    public static final String INVITE_VIA_CONTACT_ACTION = "printos invite via contact";
    public static final String INVITE_VIA_EMAIL_ACTION = "printos invite via email";
    public static final String INVITE_USER_REWARD_ACTION = "printos user got reward";
    public static final String USER_LOGIN_AFTER_INVITE_ACTION = "printos user login after invite";
    public static final String INVITE_FAILED_ONBOARD = "printos invite failed onboard";
    public static final String INVITE_FAILED_OTHER = "printos invite failed other";
    public static final String REVOKE_INVITE_SUCCESS = "printos revoke invite success";
    public static final String REVOKE_INVITE_FAIL = "printos revoke invite fail";
    public static final String RESEND_INVITE_SUCCESS = "printos resend invite success";
    public static final String RESEND_INVITE_FAIL = "printos resend invite fail";
    public static final String INVITE_ALL_VIA_BUTTON = "printos invite all via button";
    public static final String INVITE_ALL_VIA_POPUP = "printos invite all via popup";
    public static final String INVITE_CLICK_SHARE_APP = "printos click share app";
    public static final String INVITE_CLICK_REMIND_ME_LATER = "printos invite all popup click remind me later";
    public static final String INVITE_CLICK_NO_THANKS = "printos invite all popup click no thanks";
    public static final String INVITE_OPEN_TOOLTIP = "printos invite open tooltip";

    public static final String SET_PROFILE_PICTURE_ACTION = "user set profile picture";
    public static final String FAILED_TO_SET_PROFILE_PICTURE_ACTION = "user failed to set profile picture";
    public static final String EDITED_PROFILE_PICTURE_ACTION = "user edited profile picture";
    public static final String FAILED_TO_EDIT_PROFILE_PICTURE_ACTION = "user failed to edit profile picture";
    public static final String DELETE_PROFILE_PICTURE_ACTION = "user delete profile picture";
    public static final String FAILED_TO_DELETE_PROFILE_PICTURE_ACTION = "user failed to delete profile picture";

    public static final String DISMISS_BEAT_COINS_ACTION = "beat coins popup dismissed";
    public static final String DISMISS_NPS_ACTION = "nps popup dismissed";


    public static final String CLICK_CHAT_ACTION = "click chat tab";

    public static final String ACCEPT_EULA_ACTION = "accept eula";
    public static final String REJECT_EULA_ACTION = "reject eula";
    public static final String EULA_SHOWN_ACTION = "eula shown";

    public static final String EVENT_WIZARD_POPUP_SHOWN = "wizard popup shown";
    public static final String EVENT_WIZARD_POPUP_CLICK_START = "wizard popup click start";
    public static final String EVENT_WIZARD_POPUP_CLICK_NEXT_TIME = "wizard popup click next time";
    public static final String EVENT_WIZARD_ABORT = "wizard abort";
    public static final String EVENT_WIZARD_FINISH = "wizard finish";
    public static final String EVENT_WIZARD_COLLECT_COINS = "wizard collect coins";
    public static final String EVENT_WIZARD_ENTER_SCREEN_FROM_EDIT_DETAILS = "wizard edit my details";

    public static final String EVENT_WIZARD_CLICK_NEXT = "wizard click next";
    public static final String EVENT_WIZARD_CLICK_BACK = "wizard click back";
    public static final String EVENT_WIZARD_VIEW_PAGE = "wizard view";

    public static final String HISTOGRAM_BREAK_DOWN_SHOW_EVENT = "daily print volume breakdown view";
    public static final String HISTOGRAM_LANDSCAPE_SHOW_EVENT = "daily print volume landscape view";

    public static final String EVENT_CORRECTIVE_ACTIONS_JAMS_SHOWN = "jams corrective actions shown";
    public static final String EVENT_CORRECTIVE_ACTIONS_FAILURES_SHOWN = "failures corrective actions shown";

    public static final String EVENT_OPEN_FROM_WIDGET = "open app from widget";
    public static final String SCREEN_CORRECTIVE_ACTIONS_POPUP = "Corrective actions popup";
    public static final String EVENT_NAME_DIALOG_SEND_BUTTON_CLICKED = "name dialog send clicked";

    public static final String EVENT_OPEN_APP_FROM_ICON = "open app from icon";
    public static final String EVENT_KPI_EXPLANATION_SHOWN = "kpi explanation shown";
    public static final String EVENT_LOGIN_SUCCESS = "login success";
    public static final String EVENT_CLICK_LOGIN = "click login";
    public static final String EVENT_OPEN_FROM_NOTIFICATION = "open app from notification";
    public static final String EVENT_VIEW_BUSINESS_UNIT = "view bu";
    public static final String EVENT_OPEN_MENU = "open menu";
    public static final String EVENT_CLOSE_MENU = "close menu";
    public static final String EVENT_RATE_DIALOG_REMIND_ME_LATER_CLICKED = "rate dialog remind me later clicked";
    public static final String EVENT_RATE_DIALOG_NO_THANKS_CLICKED = "rate dialog no thanks clicked";
    public static final String EVENT_RATE_DIALOG_SEND_FEEDBACK_CLICKED = "rate dialog send feedback clicked";
    public static final String EVENT_RATE_RATE_DIALOG_SHOWN = "rate dialog shown";
    public static final String EVENT_ACTION_STRING_FORMAT = "SHARE_%s";


    public static final String EVENT_KPI_CLICK = "kpi breakdown %s";
    public static final String EVENT_KPI_LANDSCAPE = "kpi breakdown landscape %s";
    public static final String LABEL_OVERALL_CLICK = "Overall Performance";

    // Demo Mode
    public static final String DEMO_MODE_EVENT = "demo mode login";
    public static final String REAL_USER_LABEL = "real user";
    public static final String DEMO_MODE_LABEL = "demo mode";

    public static final String EVENT_USER_TOUCH_TODAY_VS_LAST_WEEK_GRAPH = "today vs last week tooltip touched";
    public static final String EVENT_USER_TOUCH_TODAY_VS_LAST_WEEK_GRAPH_LABEL_TODAY = "today";
    public static final String EVENT_USER_TOUCH_TODAY_VS_LAST_WEEK_GRAPH_LABEL_LAST_WEEK = "last week";

    public static final String EVENT_PROFILE_IMAGE_POPUP_SHOWN = "profile image popup shown";
    public static final String EVENT_PROFILE_IMAGE_POPUP_SUPPRESSED = "profile image popup suppressed";
    public static final String EVENT_PROFILE_IMAGE_POPUP_IMAGE_ADDED = "profile image popup image set";
    public static final String EVENT_HPID_TOOLTIP_SHOWN = "login HPID open tooltip";

    public static final String STATE_DISTRIBUTION_CLICKED_EVENT = "state distribution clicked";
    public static final String PORTRAIT_STATE_DISTRIBUTION_SHOWN_EVENT = "portrait state distribution screen shown";
    public static final String LANDSCAPE_STATE_DISTRIBUTION_SHOWN_EVENT = "landscape state distribution screen shown";
    public static final String STATE_DISTRIBUTION_SHOWN_DEVICES_LIST_LABEL = "devices list";
    public static final String STATE_DISTRIBUTION_SHOWN_HOME_LABEL = "home";
    public static final String STATE_DISTRIBUTION_SHOWN_DEVICE_DETAILS_LABEL = "device details";

    public static final String EVENT_AVAILABILITY_LEGEND_CLICK = "availability legend clicked";
    public static final String NSLOOKUP_RESULT_EVENT = "nslookup result";
    public static final String LAST_DAYS_TOOLTIP_GRAPH_IS_CLICKED = "last days %s %s tooltip triggered";
    public static final String KPI_TOOLTIP_GRAPH_IS_CLICKED = "kpi %s %s tooltip triggered";

    //state distribution graph
    public static final String STATE_DISTRIBUTION_LEGEND_CLICKED = "state distribution pie legend clicked";
    public static final String STATE_DISTRIBUTION_LANDSCAPE_LEGEND_CLICKED = "state distribution landscape legend clicked";
    public static final String STATE_DISTRIBUTION_LANDSCAPE_ZOOM_SCROLL_CLICKED = "state distribution landscape zoom and scroll";

    //Insights search
    public static final String CLICK_INSIGHTS_SEARCH = "click insights search";
    public static final String CLICK_INSIGHTS_TAB = "click insights tab";
    public static final String SEARCH_INSIGHTS = "A search operation occurred";
    public static final String OPEN_INSIGHTS_ITEM = "open Insights item_%s";
    public static final String OPEN_INSIGHTS_ITEM_FROM_FAVORITES = "open Insights item_%s from favorites";

    public static final String DAILY_SPOTLIGHT_MORE_INFO_EVENT = "daily spotlight clicked";
    public static final String DAILY_SPOTLIGHT_PAGE_CHANGE_ACTION = "daily spotlight changed";
    public static final String DAILY_SPOTLIGHT_PAGE_CHANGE_NEXT_LABEL = "arrow right click";
    public static final String DAILY_SPOTLIGHT_PAGE_CHANGE_PREV_LABEL = "arrow left click";
    public static final String DAILY_SPOTLIGHT_PAGE_CHANGE_SWIPE_LABEL = "swipe";

    public static final String VIEW_SCREEN_EVENT = "view screen";
    public static final String TAB_SCREEN_LABEL = "%s tab";
    public static final String LATEX_DEVICE_DETAIL_SCREEN_LABEL = "latex device details";
    public static final String INDIGO_DEVICE_DETAIL_SCREEN_LABEL = "indigo device details";
    public static final String DEVICE_LIST_SCREEN_LABEL = "devices list";
    public static final String HISTOGRAM_BREAKDOWN_SCREEN_LABEL = "histogram breakdown";
    public static final String KPI_BREAKDOWN_SCREEN_LABEL = "kpi Breakdown";
    public static final String SERVICE_CALL_SCREEN_LABEL = "service calls";
    public static final String STATE_DIS_SCREEN_LABEL = "state distribution";
    public static final String LANDSCAPE_BREAKDOWN_SCREEN_LABEL = "kpi breakdown landscape graph";
    public static final String LANDSCAPE_STATE_DIS_SCREEN_LABEL = "state distribution landscape graph";
    public static final String LANDSCAPE_HISTOGRAM_SCREEN_LABEL = "histogram Landscape";
    public static final String LANDSCAPE_HISTOGRAM_BREAKDOWN_SCREEN_LABEL = "histogram breakdown Landscape";
    public static final String ABOUT_SCREEN_LABEL = "about";
    public static final String SETTINGS_SCREEN_LABEL = "settings";
    public static final String LANGUAGE_SCREEN_LABEL = "language";
    public static final String INVITES_SCREEN_LABEL = "invites";
    public static final String INVITES_SETTINGS_SCREEN_LABEL = "invites settings";
    public static final String INVITES_LANG_SCREEN_LABEL = "invites language";
    public static final String INVITES_ROLL_SCREEN_LABEL = "invites roll";
    public static final String UNIT_SYSTEM_SCREEN_LABEL = "unit system";
    public static final String NOTIFICATION_SCREEN_LABEL = "notifications list";
    public static final String NOTIFICATION_SETTINGS_SCREEN_LABEL = "notification settings";
    public static final String INSIGHTS_SEARCH_SCREEN_LABEL = "insights search";
    public static final String EULA_SCREEN_LABEL = "eula";

    public static final String WEEK_PANEL_SHOWN_EVENT = "weekly print beat panel shown";

    public static final String LOCAL_EVENT = "locale used";

    public static final String RANKING_LEADERBOARD_CLICK_RANK = "Click rank";
    public static final String RANKING_LEADERBOARD_GRAPH_TOUCHED = "Ranking leaderboard graph touched";
    public static final String RANKING_LEADERBOARD_SCROLL_LIST = "Ranking leaderboard scroll list";
    public static final String RANKING_LEADERBOARD_REACH_TOP = "Ranking leaderboard reach top";
    public static final String RANKING_LEADERBOARD_FEATURE_ENABLED = "Ranking leaderboard feature enabled";
    public static final String RANKING_LEADERBOARD_FEATURE_DISABLED = "Ranking leaderboard feature disabled";

    /**
     * The index of the user id dimension.
     */
    private static final int USER_ID_DIMENSION_INDEX = 1;

    /**
     * The index of the organization id dimension.
     */
    private static final int ORGANIZATION_ID_DIMENSION_INDEX = 2;

    private static final int HP_INTERNAL_ORGANIZATION_DIMENSION_INDEX = 3;

    private static final int USER_ROLES_DIMENSION_INDEX = 4;

    private static final int ORGANIZATION_TYPE_INDEX = 5;

    private static final int SELF_PROVISION_ORGANIZATION_INDEX = 6;

    private static final int ENTITY_TYPE_DIMENSION_INDEX = 7;

    private static final int DEVICE_ID_CUSTOM_DIMENSION_INDEX = 11;

    private static final int VISITING_ORG_ID_CUSTOM_DIMENSION_INDEX = 13;

    private static final String ENTITY_TYPE_MOBILE_USER = "Mobile User";

    public static final String TARGET_LAYOUT_SHOW_ACTION = "NPI popup";

    public static final String CREATE_STORE_ACCOUNT_CLICKED = "create store account clicked";
    public static final String CREATE_STORE_CANCEL_CLICKED = "create store cancel clicked";

    public static final String RANKING_LEADERBOARD_POPUP_SHOWN = "Ranking leaderboard popup shown";
    public static final String RANKING_LEADERBOARD_POPUP_YES_CLICKED = "Ranking leaderboard popup yes clicked";
    public static final String RANKING_LEADERBOARD_NO_THANKS_CLICKED = "Ranking leaderboard popup not thanks clicked";
    public static final String RANKING_LEADERBOARD_POPUP_DONT_SHOW_CLICKED = "Ranking leaderboard popup dont show clicked";


    private static Tracker tracker;

    /**
     * An object holds the currently logged in user
     */
    private static PrintOSPreferences preferences;

    /**
     * Enum used to identify the tracker that needs to be used for tracking.
     */
    public enum TrackerName {
        TESTING_TRACKER, // Tracker used only at development time.
        STG_TRACKER, // Tracker used only for Staging server usually at alpha time.
        RELEASE_TRACKER, // Tracker used only at FT and release time.
    }

    /**
     * Init the default {@link Tracker} for this {@link Application}.
     *
     * @return tracker
     */
    public static synchronized void init(TrackerName trackerName, Application application) {

        GoogleAnalytics analytics = GoogleAnalytics.getInstance(application);

        preferences = PrintOSPreferences.getInstance(application);
        UserViewModel signedInUser = PrintOSPreferences.getInstance(application).getUserInfo();

        String trackerId = null;
        if (trackerName == TrackerName.TESTING_TRACKER) {
            trackerId = application.getString(R.string.ga_testing_tracker_id);
        } else if (trackerName == TrackerName.STG_TRACKER) {
            trackerId = application.getString(R.string.ga_staging_tracker_id);
        } else {
            trackerId = application.getString((R.string.ga_release_tracker_id));
        }

        HPLogger.d(TAG, "init google analytics, tracker name: " + trackerName.name());

        tracker = analytics.newTracker(trackerId);
        //By setting the user ID on the tracker, the ID will be sent with all subsequent hits.
        tracker.set(USER_ID_PARAMETER, signedInUser.getUserId());

        if (!TextUtils.isEmpty(signedInUser.getUserId())) {
            FirebaseAnalytics.getInstance(application).setUserId(signedInUser.getUserId());
        }

    }

    /**
     * Logs an event for analytics.
     *
     * @param action - name of the event's action.
     */
    public static void sendEvent(String action) {

        HitBuilders.EventBuilder builder = getEventBuilderWithDimensions();
        if (builder != null) {
            tracker.send(builder
                    .setCategory(MOBILE_PRINT_BEAT_CATEGORY)
                    .setAction(action)
                    .build());
        }

        AppseeSdk.getInstance(PrintOSApplication.getAppContext()).sendEvent(action);
        AmplitudeSDK.getInstance().sendEvent(action, null);

        HPLogger.d(TAG, String.format("event sent: %s", action));
    }

    /**
     * Logs an event for analytics.
     *
     * @param action - name of the event's action.
     * @param label  - name of the event's label.
     */
    public static void sendEvent(String action, String label) {

        HitBuilders.EventBuilder builder = getEventBuilderWithDimensions();
        if (builder != null) {
            tracker.send(builder
                    .setCategory(MOBILE_PRINT_BEAT_CATEGORY)
                    .setAction(action)
                    .setLabel(label)
                    .build());
        }

        AppseeSdk.getInstance(PrintOSApplication.getAppContext()).sendEvent(action, label);
        AmplitudeSDK.getInstance().sendEvent(action, label);

        HPLogger.d(TAG, String.format("event sent: %s|%s", action, TextUtils.isEmpty(label) ? "" : label));

    }

    /**
     * Creates an event builder object and Attaches custom dimensions.
     *
     * @return {@link com.google.android.gms.analytics.HitBuilders.EventBuilder}
     */
    private static HitBuilders.EventBuilder getEventBuilderWithDimensions() {

        String currentOrgId = getCurrentOrganizationId();
        String visitingOrgId = getVisitingOrganizationId();

        UserViewModel signedInUser = preferences.getUserInfo();

        boolean hasOrgId = !TextUtils.isEmpty(currentOrgId);
        boolean hasOrgVisitingId = !TextUtils.isEmpty(visitingOrgId);
        boolean hasUserId = !TextUtils.isEmpty(signedInUser.getUserId());

        if ((!hasOrgId && !hasOrgVisitingId) && hasUserId) {
            return null;
        }

        if ((hasOrgId || hasOrgVisitingId) && !hasUserId) {
            return null;
        }

        HitBuilders.EventBuilder builder = new HitBuilders.EventBuilder()
                .setCustomDimension(USER_ID_DIMENSION_INDEX, signedInUser.getUserId())
                .setCustomDimension(ORGANIZATION_ID_DIMENSION_INDEX, currentOrgId)
                .setCustomDimension(VISITING_ORG_ID_CUSTOM_DIMENSION_INDEX, visitingOrgId);

        String organizationName = signedInUser.getUserOrganization().getOrganizationName();

        if (organizationName != null && !organizationName.isEmpty()) {
            builder.setCustomDimension(HP_INTERNAL_ORGANIZATION_DIMENSION_INDEX, getHPInternalOrganizationDimenValue());
            builder.setCustomDimension(ORGANIZATION_TYPE_INDEX, signedInUser.getUserOrganization().getOrganizationTypeDisplayName());
        }

        builder.setCustomDimension(USER_ROLES_DIMENSION_INDEX, signedInUser.getUserRoles());

        String provision = signedInUser.getSelfProvisionOrg();
        if (provision != null && !provision.isEmpty()) {
            builder.setCustomDimension(SELF_PROVISION_ORGANIZATION_INDEX, signedInUser.getSelfProvisionOrg());
        }

        builder.setCustomDimension(ENTITY_TYPE_DIMENSION_INDEX, ENTITY_TYPE_MOBILE_USER);
        builder.setCustomDimension(DEVICE_ID_CUSTOM_DIMENSION_INDEX, DeviceUtils.getDeviceIdMD5(PrintOSApplication.getAppContext()));

        return builder;
    }

    private static String getCurrentOrganizationId() {

        PrintOSPreferences preferences = PrintOSPreferences.getInstance(PrintOSApplication.getAppContext());

        AccountType userType = preferences.getAccountType();
        switch (userType) {
            case HP:
            case CHANNEL:
                return preferences.getBaseOrganizationId();
            case PSP:
                return preferences.getSelectedOrganization().getOrganizationId();
            default:
                return null;
        }
    }

    private static String getVisitingOrganizationId() {

        PrintOSPreferences preferences = PrintOSPreferences.getInstance(PrintOSApplication.getAppContext());

        AccountType selectedOrgType = preferences.getSelectedOrganization().getAccountType();
        AccountType userType = preferences.getAccountType();

        switch (userType) {
            case HP:
                return selectedOrgType == AccountType.HP ? "" : preferences.getSelectedOrganization().getOrganizationId();
            case CHANNEL:
                return selectedOrgType == AccountType.CHANNEL ? "" : preferences.getSelectedOrganization().getOrganizationId();
            default:
                return null;
        }

    }

    private static String getHPInternalOrganizationDimenValue() {

        AccountType accountType = PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).getAccountType();
        if (accountType == AccountType.HP) {
            return "true";
        } else if (accountType == AccountType.NOT_SET) {
            return null;
        }
        return "false";

    }

    public static void sendNotificationClickedFirebaseEvent(String notificationType) {
        sendNotificationTypeFirebaseAnalytics(FIREBASE_ANALYTICS_PUSH_NOTIFICATION_EVENT, notificationType);
    }


    public static void sendNotificationArriveFirebaseEvent(String notificationType) {
        sendNotificationTypeFirebaseAnalytics(FIREBASE_ANALYTICS_PUSH_NOTIFICATION_ARRIVE_EVENT, notificationType);
    }


    public static void sendNotificationTypeFirebaseAnalytics(String eventName, String notificationType) {

        HPLogger.d(TAG, "Firebase analytics event sent " + eventName);

        Bundle bundle = new Bundle();
        bundle.putString(FIREBASE_ANALYTICS_PUSH_NOTIFICATION_TYPE_KEY, notificationType);

        String serverName = PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).getServerName();
        bundle.putString(FIREBASE_CURRENT_STACK, serverName);

        String userId = preferences.getUserInfo().getUserId();

        if (!TextUtils.isEmpty(userId)) {
            bundle.putString(FIREBASE_USER_ID, userId);
        }

        FirebaseAnalytics.getInstance(PrintOSApplication.getAppContext()).logEvent(eventName, bundle);
    }

}