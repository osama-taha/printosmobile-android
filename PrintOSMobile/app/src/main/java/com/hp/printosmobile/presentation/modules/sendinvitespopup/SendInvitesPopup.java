package com.hp.printosmobile.presentation.modules.sendinvitespopup;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.SpannableStringBuilder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hp.printosmobile.Analytics;
import com.hp.printosmobile.AnswersSdk;
import com.hp.printosmobile.AppseeSdk;
import com.hp.printosmobile.R;
import com.hp.printosmobile.utils.AppUtils;
import com.hp.printosmobile.utils.HPStringUtils;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Osama Taha
 */
public class SendInvitesPopup extends DialogFragment {

    public static final String TAG = SendInvitesPopup.class.getName();

    SendInvitesPopupCallback callback;

    @Bind(R.id.message_text_view)
    TextView messageTextView;

    public static SendInvitesPopup getInstance(SendInvitesPopupCallback sendInvitesPopupCallback) {
        SendInvitesPopup dialog = new SendInvitesPopup();

        dialog.callback = sendInvitesPopupCallback;
        Bundle bundle = new Bundle();
        dialog.setArguments(bundle);

        return dialog;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE,
                R.style.PopupStyle);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        return inflater.inflate(R.layout.send_invites_popup, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        initView();
        getDialog().setCanceledOnTouchOutside(false);
        AppseeSdk.getInstance(getActivity()).startScreen(AppseeSdk.SCREEN_SEND_INVITES);
    }

    private void initView() {

        String appName = getActivity().getString(R.string.printos_app_name);
        String message = getActivity().getString(R.string.send_invites_message, appName);
        SpannableStringBuilder spannableStringBuilder = HPStringUtils.boldPartOfAString(message, appName);
        messageTextView.setText(spannableStringBuilder);

    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        }
    }

    @Override
    public void dismissAllowingStateLoss() {
        super.dismissAllowingStateLoss();
        if (callback != null) {
            callback.onDialogDismissed();
        }
    }

    @OnClick(R.id.send_invites_button)
    public void sendInvitesClicked() {

        AppUtils.sendInvites(getContext(), AnswersSdk.INVITE_METHOD_POPUP);
        dismissAllowingStateLoss();


    }

    @OnClick(R.id.close_button)
    public void closeButtonClicked() {

        Analytics.sendEvent(Analytics.SEND_INVITES_CLICK_CLOSE_ACTION);
        dismissAllowingStateLoss();

    }

    public interface SendInvitesPopupCallback {
        void onDialogDismissed();
    }
}
