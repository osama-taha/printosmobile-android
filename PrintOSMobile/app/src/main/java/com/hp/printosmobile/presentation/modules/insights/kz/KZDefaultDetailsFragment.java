package com.hp.printosmobile.presentation.modules.insights.kz;

import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.hp.printosmobile.AnswersSdk;
import com.hp.printosmobile.Constants;
import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;
import com.hp.printosmobile.data.remote.models.kz.KZItem;
import com.hp.printosmobile.presentation.BaseFragment;
import com.hp.printosmobile.presentation.modules.insights.InsightsDataManager;
import com.hp.printosmobile.presentation.modules.insights.InsightsUtils;
import com.hp.printosmobile.presentation.modules.insights.kz.kzfooter.KZFooter;
import com.hp.printosmobile.presentation.modules.insights.kz.kzfooter.KZItemDetailsCallback;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitEnum;
import com.hp.printosmobile.utils.AppUtils;
import com.hp.printosmobile.utils.ShareUtils;
import com.hp.printosmobilelib.core.communications.remote.APIException;
import com.hp.printosmobilelib.ui.widgets.HPTextView;
import com.hp.printosmobilelib.ui.widgets.panel.SharableObject;

import butterknife.Bind;
import butterknife.OnClick;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Osama Taha on 3/19/18.
 */
public class KZDefaultDetailsFragment extends BaseFragment implements KZItemDetailsView {

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.pdfView)
    WebView webView;
    @Bind(R.id.loading_view)
    View loadingView;
    @Bind(R.id.error_layout)
    LinearLayout errorLayout;
    @Bind(R.id.kz_footer)
    KZFooter kzFooter;
    @Bind(R.id.progress_bar)
    ProgressBar progressBar;
    @Bind(R.id.insights_loading_message)
    HPTextView openDocumentTextView;

    private KZItem kzItem;
    private BusinessUnitEnum businessUnitEnum;
    private KZItemDetailsPresenter presenter;
    private static KZItemDetailsCallback callback;
    private static String kzUrl;

    public static Fragment newInstance(Bundle extras) {
        KZDefaultDetailsFragment fragment = new KZDefaultDetailsFragment();
        fragment.setArguments(extras);
        return fragment;
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_kz_pdf;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (getArguments() != null) {
            businessUnitEnum = (BusinessUnitEnum) getArguments().get(Constants.IntentExtras.BUSINESS_UNIT_EXTRA);
            kzItem = (KZItem) getArguments().get(Constants.IntentExtras.KZ_ITEM_EXTRA);
        }

        if (kzItem != null && KZSupportedFormat.from(kzItem.getFormat()) == KZSupportedFormat.URL) {
            loadingView.setVisibility(View.GONE);
        } else {
            WebSettings settings = webView.getSettings();
            settings.setJavaScriptEnabled(true);
            settings.setLoadWithOverviewMode(true);
            settings.setUseWideViewPort(true);

            if (KZSupportedFormat.from(kzItem.getFormat()) == KZSupportedFormat.HTML || KZSupportedFormat.from(kzItem.getFormat()) == KZSupportedFormat.HTM) {
                webView.getSettings().setBuiltInZoomControls(true);
                webView.getSettings().setDisplayZoomControls(false);
            }

            webView.setWebViewClient(new WebViewClient() {

                private static final String HIDE_PDF_VIEWER_RIGHT_TOOLBAR_JS = "javascript:" +
                        "(function()" +
                        "{ " +
                        "try {" +
                        " elem = document.getElementById('toolbarViewerRight'); " +
                        " if (elem) {      " +
                        "elem.style.display = 'none';  " +
                        "};                " +
                        "} catch (e) {}" +
                        "})();                ";

                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    AppUtils.startApplication(PrintOSApplication.getAppContext(), Uri.parse(url));
                    return true;
                }

                @Override
                public void onPageFinished(WebView view, String url) {

                    hideLoading();

                    //Remove the right toolbar in the pdf viewer.
                    webView.loadUrl(HIDE_PDF_VIEWER_RIGHT_TOOLBAR_JS);
                }


            });
        }

        KZSupportedFormat kzSupportedFormat = KZSupportedFormat.from(kzItem.getFormat());

        if (getActivity() != null) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                getActivity().getWindow().setStatusBarColor(ContextCompat.getColor(getActivity(), kzSupportedFormat.getCardBackgroundColorResId()));
            }

            ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);

            ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
            if (actionBar != null) {
                actionBar.setDisplayHomeAsUpEnabled(true);
                actionBar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(kzSupportedFormat.getCardBackgroundColorResId())));
                actionBar.setTitle(kzItem.getName());
                toolbar.setTitleTextColor(getResources().getColor(kzSupportedFormat.getCardDefaultColorResId()));
                if (toolbar.getNavigationIcon() != null) {
                    toolbar.getNavigationIcon().setColorFilter(getResources().getColor(kzSupportedFormat.getCardDefaultColorResId()), PorterDuff.Mode.SRC_ATOP);
                }
            }
        }

        if (kzItem != null && TextUtils.isEmpty(kzItem.getExternalUrl())) {
            presenter = new KZItemDetailsPresenter();
            presenter.attachView(this);
            presenter.getAssetData(kzItem, businessUnitEnum);
            setKZFooter(kzItem);
        } else {
            hideLoading();
            webView.loadUrl(kzItem.getExternalUrl());
        }
    }

    public static void addKZItemDetailsCallback(KZItemDetailsCallback kzItemDetailsCallback) {
        callback = kzItemDetailsCallback;
    }

    public void setKZFooter(final KZItem kzItem) {

        kzFooter.setKZProps(kzItem.getId(), businessUnitEnum.getKzName(), kzItem.getName());

        kzFooter.setIsFavorite((kzItem.getUserAssetPref() == null || kzItem.getUserAssetPref().getFavorite() == null) ? false : kzItem.getUserAssetPref().getFavorite());

        if (kzItem.getUserAssetPref() != null && kzItem.getUserAssetPref().getRating() != null && kzItem.getUserAssetPref().getRating() != 0) {
            kzFooter.setIsLiked(kzItem.getUserAssetPref().getRating());
        }

        kzFooter.setEventListener(new KZFooter.UserInteractionsEventListener() {
            @Override
            public void onFavoritePressed(boolean isFavorite) {

                if (kzItem.getUserAssetPref() != null) {
                    kzItem.getUserAssetPref().setFavorite(isFavorite);
                }

                if (callback != null) {
                    callback.onFavoritePressed(kzItem);
                }
            }

            @Override
            public void onLikePressed(int rating) {

                if (kzItem.getUserAssetPref() != null) {
                    kzItem.getUserAssetPref().setRating(rating);
                }

                if (callback != null) {
                    callback.onLikePressed(kzItem);
                }
            }

            @Override
            public void onDislikePressed(int rating) {

                if (kzItem.getUserAssetPref() != null) {
                    kzItem.getUserAssetPref().setRating(rating);
                }

                if (callback != null) {
                    callback.onDislikePressed(kzItem);
                }
            }

            @Override
            public void onSharePressed(String name, String link) {
                if (getActivity() != null) {
                    ShareUtils.onScreenshotCreated(getActivity(), null,
                            getActivity().getString(R.string.corrective_actions_share_insights_kz_item, name),
                            link,
                            new SharableObject() {
                                @Override
                                public String getShareAction() {
                                    return AnswersSdk.SHARE_CONTENT_TYPE_KNOWLEDGE_ZONE;
                                }
                            });
                }
            }
        });
    }

    @Override
    public boolean isFragmentNameToBeDisplayed() {
        return false;
    }

    @Override
    public boolean isFilteringAvailable() {
        return false;
    }

    @Override
    public int getToolbarDisplayName() {
        return 0;
    }

    @Override
    public void showLoading() {
        loadingView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        if (KZSupportedFormat.from(kzItem.getFormat()) != KZSupportedFormat.URL) {
            loadingView.setVisibility(View.GONE);
        } else {
            progressBar.setVisibility(View.GONE);
            openDocumentTextView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void showErrorView() {
        webView.setVisibility(View.GONE);
        errorLayout.setVisibility(View.VISIBLE);
        loadingView.setVisibility(View.GONE);
    }

    @Override
    public void hideErrorView() {
        webView.setVisibility(View.VISIBLE);
        errorLayout.setVisibility(View.GONE);
    }

    @Override
    public void onFinishedGettingAssetData(final Object result) {
        if (KZSupportedFormat.from(kzItem.getFormat()) == KZSupportedFormat.URL) {

            InsightsDataManager.getURLContent((String) result).observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(new Subscriber<String>() {
                        @Override
                        public void onCompleted() {
                            hideLoading();
                            SpannableString spannable = new SpannableString(getResources().getString(R.string.insights_url_result_open_document));
                            spannable.setSpan(new UnderlineSpan(), 0, getResources().getString(R.string.insights_url_result_open_document).length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                            openDocumentTextView.setText(spannable);
                            openDocumentTextView.setTextColor(getResources().getColor(R.color.c62));
                        }

                        @Override
                        public void onError(Throwable throwable) {
                            hideLoading();
                            showErrorView();
                        }

                        @Override
                        public void onNext(String stringResponse) {
                            kzUrl = InsightsUtils.getKzUrlFromText(stringResponse);
                        }
                    });

            openDocumentTextView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    progressBar.setVisibility(View.VISIBLE);
                    if (!TextUtils.isEmpty(kzUrl)) {
                        AppUtils.startApplication(getActivity(), Uri.parse(kzUrl));
                    }
                    hideLoading();
                }
            });

        } else {
            if (result != null) {
                webView.loadUrl((String) result);
            }
        }
    }

    @Override
    public void onError(APIException exception, String tag) {

    }

    @OnClick(R.id.retry_btn)
    public void onReloadButtonClicked() {
        presenter.getAssetData(kzItem, businessUnitEnum);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (presenter != null) {
            presenter.detachView();
        }
    }

    @Override
    public void onCancel() {

    }

}
