package com.hp.printosmobile.data.remote.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Anwar Asbah 11/6/2017
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class InviteSendUserIDData {

    @JsonProperty("invitationId")
    private String invitationId;

    @JsonProperty("invitationId")
    public String getInvitationId() {
        return invitationId;
    }

    @JsonProperty("invitationId")
    public void setInvitationId(String invitationId) {
        this.invitationId = invitationId;
    }
}
