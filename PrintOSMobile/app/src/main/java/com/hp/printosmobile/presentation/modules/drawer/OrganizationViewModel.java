package com.hp.printosmobile.presentation.modules.drawer;

import com.hp.printosmobile.data.remote.models.AccountType;
import com.hp.printosmobilelib.core.communications.remote.models.OrganizationJsonBody;

import java.io.Serializable;
import java.util.Comparator;

/**
 * A model representing organizations.
 * Created by Anwar Asbah on 8/29/16.
 */
public class OrganizationViewModel implements Serializable {

    public static final Comparator<OrganizationViewModel> OrganizationComparator = new Comparator<OrganizationViewModel>() {
        @Override
        public int compare(OrganizationViewModel organization, OrganizationViewModel t1) {
            return organization.getOrganizationName().toLowerCase().compareTo(t1.getOrganizationName().toLowerCase());
        }
    };

    private String organizationId;
    private String organizationName;
    private String organizationTypeDisplayName;
    private AccountType accountType;

    public String getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(String organizationId) {
        this.organizationId = organizationId;
    }

    public String getOrganizationName() {
        return organizationName;
    }

    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }

    public String getOrganizationTypeDisplayName() {
        return organizationTypeDisplayName;
    }

    public void setOrganizationTypeDisplayName(String organizationType) {
        this.organizationTypeDisplayName = organizationType;
    }

    public OrganizationJsonBody getOrganizationJsonBody() {
        OrganizationJsonBody body = new OrganizationJsonBody();
        body.setId(organizationId);
        return body;
    }

    public void setAccountType(AccountType accountType) {
        this.accountType = accountType;
    }

    public AccountType getAccountType() {
        return accountType;
    }


    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("OrganizationViewModel{");
        sb.append("organizationId='").append(organizationId).append('\'');
        sb.append(", organizationName='").append(organizationName).append('\'');
        sb.append(", organizationTypeDisplayName='").append(organizationTypeDisplayName).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
