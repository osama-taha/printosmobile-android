package com.hp.printosmobile.presentation.modules.insights.kz.kzfooter;

import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.data.remote.models.kz.KZFavorites;
import com.hp.printosmobile.data.remote.models.kz.KZItem;
import com.hp.printosmobile.presentation.Presenter;
import com.hp.printosmobile.presentation.modules.insights.InsightsDataManager;

import java.util.List;

import retrofit2.Response;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Osama on 3/14/18.
 */
public class FavoritesPresenter extends Presenter<FavoritesView> {

    public static final int PAGE_SIZE = 25;

    private final String businessUnitEnumKey;
    private final List<String> supportedFormats;
    private int totalResults;
    private boolean isLastPage;

    FavoritesPresenter(String businessUnitEnumKey) {
        this.businessUnitEnumKey = businessUnitEnumKey;
        this.supportedFormats = PrintOSPreferences.getInstance(PrintOSApplication.getAppContext())
                .getInsightsConfigurations().getKzSupportedFormats();
    }

    void getFavoritesData(final int pageOffset) {

        if (mView == null) {
            return;
        }

        isLastPage = false;
        mView.hideEmptyView();
        mView.hideErrorView();

        mView.showLoadingView();

        InsightsDataManager.getFavorites(businessUnitEnumKey, PAGE_SIZE, pageOffset, supportedFormats)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).subscribe(new Subscriber<Response<KZFavorites>>() {
            @Override
            public void onCompleted() {
            }

            @Override
            public void onError(Throwable throwable) {

                if (mView == null) {
                    return;
                }

                mView.hideLoadingView();
                mView.showErrorView();
                mView.hideResultsView();
                totalResults = 0;
            }

            @Override
            public void onNext(Response<KZFavorites> kzFavoritesResponse) {

                if (mView == null) {
                    return;
                }

                if (kzFavoritesResponse.isSuccessful()) {

                    mView.hideLoadingView();
                    mView.clearResults();

                    if (kzFavoritesResponse.body() != null) {
                        boolean hasResults = kzFavoritesResponse.body().getFavoriteAssetResponses() != null &&
                                !kzFavoritesResponse.body().getFavoriteAssetResponses().isEmpty();

                        if (hasResults) {
                            mView.hideEmptyView();
                            mView.hideErrorView();
                            totalResults = kzFavoritesResponse.body().getTotalAssetsReturned();
                            mView.addResults(kzFavoritesResponse.body().getFavoriteAssetResponses());
                            mView.showResultsView();
                        } else {
                            totalResults = 0;
                            mView.hideResultsView();
                            mView.setEmptyText();
                            mView.showEmptyView();
                        }
                    }

                } else {
                    totalResults = 0;
                    mView.hideLoadingView();
                    mView.showErrorView();
                }
            }
        });
    }

    public void getNextPage(int pageNumber, final int itemCount) {

        if (mView == null) {
            return;
        }

        mView.addFooterView();
        mView.showLoadingView();

        InsightsDataManager.getFavorites(businessUnitEnumKey, PAGE_SIZE, pageNumber, supportedFormats)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).subscribe(new Subscriber<Response<KZFavorites>>() {
            @Override
            public void onCompleted() {
            }

            @Override
            public void onError(Throwable throwable) {
                if (mView != null) {
                    mView.showErrorFooterView();
                }
            }

            @Override
            public void onNext(Response<KZFavorites> kzFavoritesResponse) {

                if (mView == null) {
                    return;
                }

                if (kzFavoritesResponse.isSuccessful()) {

                    boolean hasResults;
                    if (kzFavoritesResponse.body() != null) {
                        hasResults = kzFavoritesResponse.body().getFavoriteAssetResponses() != null &&
                                !kzFavoritesResponse.body().getFavoriteAssetResponses().isEmpty();

                        isLastPage = itemCount + PAGE_SIZE > totalResults;

                        mView.hideLoadingView();
                        mView.removeFooterView();

                        if (hasResults) {

                            List<KZItem> items = kzFavoritesResponse.body().getFavoriteAssetResponses();

                            mView.hideEmptyView();
                            mView.hideErrorView();

                            mView.addResults(items);
                            mView.showResultsView();
                        }

                        if (isLastPage) {
                            mView.addFooterView();
                            mView.showNoResultsFooterView();
                        }
                    }

                } else {
                    mView.hideLoadingView();
                    mView.showErrorFooterView();
                }
            }
        });

    }

    public boolean isLastPage() {
        return isLastPage;
    }

    @Override
    public void detachView() {
    }

    public void onReloadButtonClicked() {
        getFavoritesData(1);
    }

}