package com.hp.printosmobile.presentation.modules.filters;

import android.text.TextUtils;

import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;
import com.hp.printosmobile.data.remote.models.DeviceData;
import com.hp.printosmobile.presentation.modules.shared.MeasureTypeEnum;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * A model that holds information about division's devices,groups and sites.
 * <p>
 * Created by Osama Taha on 10/9/16.
 */
public class FiltersViewModel implements Serializable {

    public static final String DEFAULT_SITE_ID = "-1";

    public static final Comparator<FilterItem> FILTER_ITEM_COMPARATOR = new Comparator<FilterItem>() {
        @Override
        public int compare(FilterItem lhs, FilterItem rhs) {
            if (lhs == null && rhs == null) return 0;
            if (lhs == null) return 1;
            if (rhs == null) return -1;
            return lhs.compareTo(rhs);
        }
    };

    private boolean sitesOnSameTimeZone;
    private List<FilterItem> sites;
    private List<FilterItem> devices;
    private List<FilterItem> groups;
    private List<FilterItem> selectedDevices;
    private FilterItem selectedSite;
    private Map<String, SiteViewModel> sitesMap;

    public boolean isSitesOnSameTimeZone() {
        return sitesOnSameTimeZone;
    }

    public void setSitesOnSameTimeZone(boolean sitesOnSameTimeZone) {
        this.sitesOnSameTimeZone = sitesOnSameTimeZone;
    }

    public List<FilterItem> getSites() {
        return sites;
    }

    public void setSites(List<FilterItem> sites) {
        this.sites = sites;
    }

    public List<FilterItem> getDevices() {
        return devices;
    }

    public void setDevices(List<FilterItem> devices) {
        this.devices = devices;
    }

    public List<FilterItem> getGroups() {
        return groups;
    }

    public void setGroups(List<FilterItem> groups) {
        this.groups = groups;
    }

    public void initFiltersViewModel(List<DeviceData> devices) {

        List<FilterItem> deviceViewModels = new LinkedList<>();
        if (devices != null) {
            for (DeviceData deviceData : devices) {
                deviceViewModels.add(convertToDeviceModel(deviceData));
            }
        }

        setDevices(deviceViewModels);
        setSites(getSites(deviceViewModels));
        setGroups(getGroups(deviceViewModels));

    }

    public DeviceFilterViewModel convertToDeviceModel(DeviceData deviceData) {

        DeviceFilterViewModel deviceFilterViewModel = new DeviceFilterViewModel();
        deviceFilterViewModel.setDeviceId(deviceData.getDeviceId());
        deviceFilterViewModel.setSerialNumber(deviceData.getSerialNumber());
        deviceFilterViewModel.setBusinessUnit(deviceData.getBusinessUnit());
        deviceFilterViewModel.setDeviceName(deviceData.getDeviceName());
        deviceFilterViewModel.setDeviceNickName(deviceData.getDeviceNickName());
        deviceFilterViewModel.setEndUserId(deviceData.getEndUserId());
        deviceFilterViewModel.setGroups(deviceData.getGroups());
        deviceFilterViewModel.setMeasureTypeEnum(MeasureTypeEnum.from(deviceData.getMeasureType()));

        if (deviceData.getSite() != null) {
            deviceFilterViewModel.setSite(new SiteViewModel(deviceData.getSite().getSiteID(),
                    deviceData.getSite().getSiteName(),
                    deviceData.getSite().getSiteTZ()));
        }

        deviceFilterViewModel.setImpressionType(deviceData.getImpressionType());
        deviceFilterViewModel.setOrganizationID(deviceData.getOrganizationID());
        deviceFilterViewModel.setRtSupported(deviceData.isRtSupported());
        deviceFilterViewModel.setPressModel(deviceData.getPressModel());
        deviceFilterViewModel.setPressDescription(deviceData.getPressDescription());
        deviceFilterViewModel.setSortPosition(deviceData.getSortPosition());

        return deviceFilterViewModel;
    }

    public List<FilterItem> getSites(List<FilterItem> deviceViewModels) {

        Map<String, SiteViewModel> siteViewModelHashMap = new LinkedHashMap<>();

        Map<String, List<FilterItem>> devicesMap = new LinkedHashMap<>();

        Map<String, String> sitesTimeZones = new LinkedHashMap<>();
        Map<String, List<String>> serialNumbers = new HashMap<>();

        for (int i = 0; i < deviceViewModels.size(); i++) {

            DeviceFilterViewModel deviceFilterViewModel = (DeviceFilterViewModel) deviceViewModels.get(i);

            if (deviceFilterViewModel.getSite() != null) {

                if (!devicesMap.containsKey(deviceFilterViewModel.getSite().getId())) {

                    siteViewModelHashMap.put(deviceFilterViewModel.getSite().getId(), deviceFilterViewModel.getSite());
                    sitesTimeZones.put(deviceFilterViewModel.getSite().getTimeZone(), deviceFilterViewModel.getSite().getId());
                    serialNumbers.put(deviceFilterViewModel.getSite().getId(), new ArrayList<String>());
                    devicesMap.put(deviceFilterViewModel.getSite().getId(), new ArrayList<FilterItem>());

                }

                devicesMap.get(deviceFilterViewModel.getSite().getId()).add(deviceFilterViewModel);
                serialNumbers.get(deviceFilterViewModel.getSite().getId()).add(deviceFilterViewModel.getSerialNumber());

            }
        }


        List<FilterItem> filterItems = new ArrayList<>();

        SiteViewModel defaultSite = null;

        for (FilterItem filterItem : siteViewModelHashMap.values()) {

            SiteViewModel siteViewModel = (SiteViewModel) filterItem;

            List<FilterItem> devicesList = devicesMap.get(siteViewModel.getId());
            Collections.sort(devicesList, FILTER_ITEM_COMPARATOR);
            siteViewModel.setDevices(devicesList);
            siteViewModel.setSerialNumbers(serialNumbers.get(siteViewModel.getId()));

            siteViewModel.setGroups(getGroups(devicesList));

            if (siteViewModel.getId().equals(DEFAULT_SITE_ID)) {
                defaultSite = siteViewModel;
                siteViewModel.setName(PrintOSApplication.getAppContext().getString(R.string.filter_unassigned_site));
                siteViewModel.setDefaultSite(true);
                siteViewModel.setSelected(true);
            }

            filterItems.add(siteViewModel);
        }

        if (sitesTimeZones.size() > 1) {
            sitesOnSameTimeZone = false;
        }

        SiteViewModel allSite = null;

        if (sitesOnSameTimeZone) {

            allSite = new SiteViewModel();
            allSite.setName(PrintOSApplication.getAppContext().getString(R.string.filters_all_site));
            allSite.setAllSite(true);
            allSite.setSelected(true);
            allSite.setId(allSite.getName());

            if (defaultSite != null) {
                defaultSite.setSelected(false);
            }

            List<FilterItem> allDevices = new ArrayList<>();
            allDevices.addAll(deviceViewModels);

            Collections.sort(allDevices, FILTER_ITEM_COMPARATOR);

            allSite.setDevices(allDevices);
            allSite.setGroups(getGroups(allDevices));
            filterItems.add(allSite);

        }

        if (allSite == null && defaultSite == null && !filterItems.isEmpty()) {
            //Make the first site selected.
            filterItems.get(0).setSelected(true);
        }

        Collections.sort(filterItems, FilterItem.NAME_COMPARATOR);

        this.sitesMap = siteViewModelHashMap;

        return filterItems;
    }

    private List<FilterItem> getGroups(List<FilterItem> deviceItems) {

        if (deviceItems == null) {
            return new ArrayList<>();
        }

        List<FilterItem> groupsList = new ArrayList<>();
        List<String> groupNames = new ArrayList<>();

        Map<String, List<FilterItem>> deviceMap = new HashMap<>();

        for (FilterItem filterItem : deviceItems) {

            DeviceFilterViewModel deviceFilterViewModel = (DeviceFilterViewModel) filterItem;

            if (deviceFilterViewModel.getGroups() != null) {

                for (String group : deviceFilterViewModel.getGroups()) {

                    if (deviceMap.get(group) == null) {
                        groupNames.add(group);
                        deviceMap.put(group, new ArrayList<FilterItem>());
                    }

                    deviceMap.get(group).add(deviceFilterViewModel);
                }
            }
        }

        for (String name : groupNames) {

            GroupViewModel group = new GroupViewModel();
            group.setName(name);
            group.setDevices(deviceMap.get(name));
            groupsList.add(group);
        }

        return groupsList;

    }

    public void setSelectedDevices(List<FilterItem> selectedDevices) {
        this.selectedDevices = selectedDevices;
    }

    public void setSelectedSite(FilterItem selectedSite) {
        this.selectedSite = selectedSite;
    }

    public SiteViewModel getSelectedSite() {

        if (selectedSite != null) {
            return (SiteViewModel) selectedSite;
        }

        SiteViewModel selectedSiteViewModel = null;

        for (FilterItem filterItem : getSites()) {
            SiteViewModel siteViewModel = (SiteViewModel) filterItem;
            if (siteViewModel.isAllSite()) {
                return siteViewModel;
            } else if (siteViewModel.isDefaultSite()) {
                selectedSiteViewModel = siteViewModel;
            }
        }

        if (selectedSiteViewModel == null && getSites() != null && !getSites().isEmpty()) {
            selectedSiteViewModel = (SiteViewModel) getSites().get(0);
        }

        return selectedSiteViewModel;
    }

    public String getEndUserIdFromCurrentSite() {

        String endUserId = null;
        SiteViewModel selectedSiteViewModel = getSelectedSite();
        if (selectedSiteViewModel != null && selectedSiteViewModel.getDevices() != null) {
            for (FilterItem deviceItem : selectedSiteViewModel.getDevices()) {
                DeviceFilterViewModel deviceViewModel = (DeviceFilterViewModel) deviceItem;
                if (!TextUtils.isEmpty(deviceViewModel.getEndUserId())) {
                    endUserId = deviceViewModel.getEndUserId();
                    break;
                }
            }
        }

        return endUserId;
    }

    public List<FilterItem> getSelectedDevices() {

        if (selectedDevices == null || selectedDevices.isEmpty()) {
            if (getSelectedSite() != null) {
                return getSelectedSite().getDevices();
            }
            return new ArrayList<>();
        }

        return selectedDevices;
    }

    public List<String> getSiteDevicesIds() {
        List<FilterItem> devicesList = getSelectedSite().getDevices();
        return getDevicesIds(devicesList);
    }

    public List<String> getSiteSerialNumbers() {
        if (getSelectedSite() != null) {
            List<FilterItem> devicesList = getSelectedSite().getDevices();
            return getSerialNumbers(devicesList);
        }
        return new ArrayList<>();
    }

    public List<String> getSerialNumbers() {
        if (getSelectedSite() != null) {
            List<FilterItem> devicesList = getSelectedDevices();
            return getSerialNumbers(devicesList);
        }
        return new ArrayList<>();
    }

    public List<String> getSerialNumbers(List<FilterItem> devicesList) {

        if (devicesList == null) {
            return new ArrayList<>();
        }

        List<String> serialNumbers = new ArrayList<>();

        for (FilterItem filterItem : devicesList) {
            DeviceFilterViewModel deviceFilterViewModel = (DeviceFilterViewModel) filterItem;
            serialNumbers.add(deviceFilterViewModel.getSerialNumber());

        }
        return serialNumbers;
    }

    private List<String> getDevicesIds(List<FilterItem> devicesList) {

        if (devicesList == null) {
            return new ArrayList<>();
        }

        List<String> serialNumbers = new ArrayList<>();

        for (FilterItem filterItem : devicesList) {
            DeviceFilterViewModel deviceFilterViewModel = (DeviceFilterViewModel) filterItem;
            serialNumbers.add(deviceFilterViewModel.getDeviceId());

        }

        return serialNumbers;
    }

    public List<String> getSerialNumbers(DeviceFilterViewModel.DeviceFiltering filter) {

        List<FilterItem> devicesList = getSelectedDevices();

        List<String> serialNumbers = new ArrayList<>();

        if (devicesList != null && filter != null) {
            for (FilterItem filterItem : devicesList) {
                DeviceFilterViewModel deviceFilterViewModel = (DeviceFilterViewModel) filterItem;
                if (filter.isIncluded(deviceFilterViewModel)) {
                    serialNumbers.add(deviceFilterViewModel.getSerialNumber());
                }
            }
        }

        return serialNumbers;
    }

    public boolean setSelectedSiteById(String siteId) {

        if (sitesMap != null && sitesMap.keySet().contains(siteId)) {
            for (FilterItem siteViewModel : sites) {
                if (siteViewModel.getId().equals(siteId)) {
                    siteViewModel.setSelected(true);
                    this.selectedSite = siteViewModel;
                    return true;
                } else {
                    siteViewModel.setSelected(false);
                }
            }
        }
        return false;
    }

    public boolean isGroupSelected() {

        boolean devicesBelongToGroup = false;

        List<FilterItem> groups = getGroups();

        if (groups != null) {
            List<String> selectedDevices = getSerialNumbers();

            SiteViewModel selectedSite = getSelectedSite();

            if (selectedSite != null && selectedDevices.size() == selectedSite.getDevices().size()){
                return false;
            }

            for (FilterItem group : groups) {

                devicesBelongToGroup = true;

                List<String> groupSerialNumbers = getSerialNumbers(group.getItems());

                if (groupSerialNumbers.size() == selectedDevices.size()) {
                    for (String serialNumber : selectedDevices) {
                        if (serialNumber != null) {
                            devicesBelongToGroup = devicesBelongToGroup && groupSerialNumbers.contains(serialNumber);
                        }
                    }
                } else {
                    devicesBelongToGroup = false;
                }

                if (devicesBelongToGroup) {
                    break;
                }

            }
        }

        return devicesBelongToGroup;
    }

    @Override
    public String toString() {
        return "FiltersViewModel{" +
                ", sitesOnSameTimeZone=" + sitesOnSameTimeZone +
                ", sites=" + sites +
                ", devices=" + devices +
                ", groups=" + groups +
                '}';
    }

}