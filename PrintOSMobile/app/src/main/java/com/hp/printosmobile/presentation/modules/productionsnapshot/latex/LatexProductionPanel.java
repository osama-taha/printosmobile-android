package com.hp.printosmobile.presentation.modules.productionsnapshot.latex;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

import com.hp.printosmobile.AnswersSdk;
import com.hp.printosmobile.Constants;
import com.hp.printosmobile.R;
import com.hp.printosmobile.presentation.modules.home.Panel;
import com.hp.printosmobile.presentation.modules.productionsnapshot.indigo.ProductionViewModel;
import com.hp.printosmobile.presentation.modules.shared.DeepLinkUtils;
import com.hp.printosmobile.presentation.modules.shared.PanelShareCallbacks;
import com.hp.printosmobile.utils.FileUtils;
import com.hp.printosmobile.utils.HPLocaleUtils;
import com.hp.printosmobilelib.ui.widgets.panel.PanelView;
import com.hp.printosmobilelib.ui.widgets.panel.SharableObject;

import butterknife.Bind;

/**
 * Created by Anwar Asbah on 4/12/2016.
 */
public class LatexProductionPanel extends PanelView<ProductionViewModel> implements SharableObject {

    public static final String TAG = LatexProductionPanel.class.getSimpleName();

    @Bind(R.id.production_snapshot_content)
    View productionSnapshotContent;
    @Bind(R.id.text_queued_value)
    TextView queuedValueTextView;
    @Bind(R.id.text_printing_value)
    TextView printingValueTextView;
    @Bind(R.id.text_printed_value)
    TextView printedValueTextView;
    @Bind(R.id.text_failed_printed_value)
    TextView failedPrintedTextView;

    LatexProductionPanelCallback callback;

    public LatexProductionPanel(Context context) {
        super(context);
    }

    public LatexProductionPanel(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public LatexProductionPanel(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void addCallback(LatexProductionPanelCallback callback) {
        this.callback = callback;
    }

    @Override
    public Spannable getTitleSpannable() {
        return new SpannableStringBuilder(getContext().getString(R.string.production_panel_title));
    }

    @Override
    public int getContentView() {
        return R.layout.production_snapshot_content_latext;
    }

    @Override
    public void updateViewModel(ProductionViewModel viewModel) {

        setViewModel(viewModel);

        if (showEmptyCard()) {
            DeepLinkUtils.respondToIntentForPanel(this, callback, true);
            return;
        }

        int queued = viewModel.getJobsInQueue();
        int printing = viewModel.getNumberOfPrintingJobs();
        int printed = viewModel.getNumberOfTodayPrintedJobs();

        queuedValueTextView.setText(queued == LatexProductionSnapshotViewModel.ERROR_VALUE ?
                getContext().getString(R.string.unknown_value) : getLocalizedValue(viewModel.getJobsInQueue()));

        printingValueTextView.setText(printing == LatexProductionSnapshotViewModel.ERROR_VALUE ?
                getContext().getString(R.string.unknown_value) : getLocalizedValue(viewModel.getNumberOfPrintingJobs()));

        printedValueTextView.setText(printed == LatexProductionSnapshotViewModel.ERROR_VALUE ?
                getContext().getString(R.string.unknown_value) : getLocalizedValue(viewModel.getNumberOfTodayPrintedJobs()));

        failedPrintedTextView.setText(getLocalizedValue(viewModel.getNumberOfTodayFailedPrintedJobs()));
        failedPrintedTextView.setVisibility(viewModel.getNumberOfTodayFailedPrintedJobs() > 0 ? VISIBLE : GONE);

        DeepLinkUtils.respondToIntentForPanel(this, callback, false);
    }

    @Override
    public void bindViews() {

    }

    @Override
    protected boolean isValidViewModel() {
        return getViewModel() != null;
    }

    protected String getEmptyCardText() {
        return getContext().getString(R.string.no_information_to_display);
    }

    private String getLocalizedValue(int value) {
        return HPLocaleUtils.getLocalizedValue(value);
    }

    @Override
    protected boolean hasSharingButton() {
        super.hasSharingButton();
        return true;
    }

    @Override
    protected void onShareClicked() {
        super.onShareClicked();
        if (callback != null) {
            callback.onShareButtonClicked(Panel.LATEX_PRODUCTION);
        }
    }

    @Override
    public void sharePanel() {
        if (callback != null && productionSnapshotContent != null) {
            lockShareButton(true);

            shareButton.setVisibility(GONE);
            final Bitmap panelBitmap = FileUtils.getScreenShot(this);
            shareButton.setVisibility(VISIBLE);

            if (panelBitmap != null) {
                DeepLinkUtils.getShareBody(getContext(),
                        Constants.UNIVERSAL_LINK_SCREEN_HOME,
                        getPanelTag(), null, null, false, new DeepLinkUtils.BranchIOCallback() {
                            @Override
                            public void onLinkCreated(String link) {
                                Uri storageUri = FileUtils.store(getContext(), panelBitmap, PANEL_SCREEN_SHOT_FILE_NAME);
                                callback.onScreenshotCreated(Panel.LATEX_PRODUCTION, storageUri,
                                        getContext().getString(R.string.share_jobs_today_title),
                                        link,
                                        LatexProductionPanel.this);
                                lockShareButton(false);
                            }
                        });
            }
        }
    }

    @Override
    public String getShareAction() {
        return AnswersSdk.SHARE_CONTENT_TYPE_JOBS_TODAY;
    }

    @Override
    public String getPanelTag() {
        return Panel.LATEX_PRODUCTION.getDeepLinkTag();
    }

    public interface LatexProductionPanelCallback extends PanelShareCallbacks, DeepLinkUtils.DeepLinkCallbacks {
    }

}
