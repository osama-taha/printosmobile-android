package com.hp.printosmobile.data.remote.cache;

import android.text.TextUtils;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobilelib.core.logging.HPLogger;

import java.util.List;

import retrofit2.Response;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by Anwar Asbah on 3/28/2018.
 */

public class ApiCacheUtils {

    private static final String TAG = ApiCacheUtils.class.getSimpleName();
    private static PrintOSPreferences preferences;
    private static ObjectMapper mapper;

    private static PrintOSPreferences getPreference() {
        if (preferences == null) {
            preferences = PrintOSPreferences.getInstance(PrintOSApplication.getAppContext());
        }
        return preferences;
    }

    private static ObjectMapper getMapper() {
        if (mapper == null) {
            mapper = new ObjectMapper();
        }
        return mapper;
    }

    public static <T> Observable<Response<T>> getCacheAndUpdate(Observable<Response<T>> observable, final String prefCacheKey, TypeReference<T> type) {

        HPLogger.d(TAG, "getCacheAndUpdate for pref key: " + (prefCacheKey == null ? "null" : prefCacheKey));

        T cachedObject = getCachedResponse(prefCacheKey, type);
        if (cachedObject != null) {
            HPLogger.d(TAG, "cache found for key: " + prefCacheKey);

            requestApiAndAddToCache(observable, prefCacheKey);

            HPLogger.d(TAG, "returning cache for key: " + prefCacheKey);
            Response<T> response = Response.success(cachedObject);
            return Observable.just(response);
        }

        HPLogger.d(TAG, "cache not found for key: " + prefCacheKey);
        return getModifiedRequestForCaching(observable, prefCacheKey);
    }

    private static <T> T getCachedResponse(String cacheKey, final TypeReference<T> type) {
        if (cacheKey == null || type == null) {
            HPLogger.d(TAG, "get Cached Response error: null keys");
            return null;
        }

        HPLogger.d(TAG, "get Cached Response for key: " + cacheKey);
        String cache = getPreference().getPreferenceValue(cacheKey);
        if (TextUtils.isEmpty(cache)) {
            return null;
        }

        try {
            return getMapper().readValue(cache, type);
        } catch (Exception e) {
            HPLogger.d(TAG, "error while getting cache response for key : " + cache + ", " + e.getMessage());
            return null;
        }
    }

    private static <T> void requestApiAndAddToCache(Observable<Response<T>> observable, final String prefCacheKey) {
        HPLogger.d(TAG, "requesting Api And Adding To Cache for key: " + prefCacheKey);
        observable.map(new Func1<Response<T>, T>() {
            @Override
            public T call(Response<T> response) {
                if (response != null && response.isSuccessful()) {
                    return response.body();
                }
                return null;
            }
        }).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<T>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        HPLogger.d(TAG, "failed to update cache for key: " + prefCacheKey);
                    }

                    @Override
                    public void onNext(T t) {
                        try {
                            String cacheString = getMapper().writeValueAsString(t);
                            getPreference().savePreferenceValue(prefCacheKey, cacheString);
                            HPLogger.d(TAG, "Successfully updated cache for key: " + prefCacheKey);
                        } catch (Exception e) {
                            HPLogger.d(TAG, "failed to update cache for key: " + prefCacheKey + ", " + e.getMessage());
                        }
                    }
                });
    }

    private static <T> Observable<Response<T>> getModifiedRequestForCaching(Observable<Response<T>> observable, final String prefCacheKey) {
        HPLogger.d(TAG, "getting Modified Caching Request for key: " + prefCacheKey);
        return observable.map(new Func1<Response<T>, Response<T>>() {
            @Override
            public Response<T> call(Response<T> response) {
                if (response != null && response.isSuccessful() && response.body() != null) {
                    try {
                        String cacheString = getMapper().writeValueAsString(response.body());
                        getPreference().savePreferenceValue(prefCacheKey, cacheString);
                        HPLogger.d(TAG, "Successfully cached Modified Caching Request for key: " + prefCacheKey);
                    } catch (Exception e) {
                        HPLogger.d(TAG, "failed to cache Modified Caching Request for key: " + prefCacheKey);
                    }
                }
                return response;
            }
        });
    }

    public static void clearApiCache(String... keys) {
        if (keys != null) {
            for (String key : keys) {
                if (key != null) {
                    getPreference().savePreferenceValue(key, "");
                }
            }
        }
    }

    public static boolean didCacheIDChange(String prefKey, String updatedCacheID) {
        if (prefKey == null || updatedCacheID == null) {
            return true;
        }

        String cacheID = getPreference().getPreferenceValue(prefKey);
        if (!cacheID.equalsIgnoreCase(updatedCacheID)) {
            getPreference().savePreferenceValue(prefKey, updatedCacheID);
            return true;
        }

        return false;
    }

    public static String getListAsString(List<String> list) {
        if (list == null) {
            return "";
        }

        String asString = "";
        for (String item : list) {
            asString += item;
        }

        return asString;
    }

}
