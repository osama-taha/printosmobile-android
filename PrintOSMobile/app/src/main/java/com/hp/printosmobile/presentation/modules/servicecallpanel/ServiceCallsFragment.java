package com.hp.printosmobile.presentation.modules.servicecallpanel;

import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.hp.printosmobile.Analytics;
import com.hp.printosmobile.AnswersSdk;
import com.hp.printosmobile.Constants;
import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;
import com.hp.printosmobile.presentation.modules.home.Panel;
import com.hp.printosmobile.presentation.modules.main.MainActivity;
import com.hp.printosmobile.presentation.modules.servicecallpanel.ServiceCallViewModel.ServiceCallStateEnum;
import com.hp.printosmobile.presentation.modules.shared.DeepLinkUtils;
import com.hp.printosmobile.utils.FileUtils;
import com.hp.printosmobile.utils.HPUIUtils;
import com.hp.printosmobilelib.ui.common.HPFragment;
import com.hp.printosmobilelib.ui.utils.SpaceItemDecoration;
import com.hp.printosmobilelib.ui.widgets.panel.SharableObject;

import butterknife.Bind;

/**
 * create by anwar asbah 3/12/2017
 */
public class ServiceCallsFragment extends HPFragment implements SharableObject {

    private static final String ARG_VIEW_MODEL = "param1";
    private static final String ARG_SERVICE_CALL_FOCUS_STATE = "param2";
    public static final String SCREEN_SHOT_FILE_NAME = "PrintOS";

    @Bind(R.id.calls_list)
    RecyclerView callsList;
    @Bind(R.id.loading_view)
    View loadingView;

    ServiceCallViewModel serviceCallViewModel;
    ServiceCallsAdapter serviceCallsAdapter;
    ServiceCallStateEnum serviceCallStateEnum = ServiceCallStateEnum.OPEN;

    View toBeShared;
    ServiceCallsFragmentCallback callback;
    private ServiceCallViewModel.CallViewModel modelToBeShared;

    public static ServiceCallsFragment newInstance(ServiceCallViewModel serviceCallViewModel,
                                                   ServiceCallStateEnum serviceCallStateEnum,
                                                   ServiceCallsFragmentCallback callback) {
        ServiceCallsFragment fragment = new ServiceCallsFragment();
        if (serviceCallViewModel != null) {
            Bundle args = new Bundle();
            args.putSerializable(ARG_VIEW_MODEL, serviceCallViewModel);
            args.putSerializable(ARG_SERVICE_CALL_FOCUS_STATE, serviceCallStateEnum);
            fragment.setArguments(args);
        }
        fragment.callback = callback;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getArguments();
        if (bundle != null) {
            if (bundle.containsKey(ARG_VIEW_MODEL)) {
                serviceCallViewModel = (ServiceCallViewModel) bundle.getSerializable(ARG_VIEW_MODEL);
            }

            if (bundle.containsKey(ARG_SERVICE_CALL_FOCUS_STATE)) {
                serviceCallStateEnum = (ServiceCallStateEnum) bundle.getSerializable(ARG_SERVICE_CALL_FOCUS_STATE);
            }
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
        Analytics.sendEvent(Analytics.VIEW_SERVICE_CALLS_ACTION);
    }

    private void init() {
        callsList.addItemDecoration(new SpaceItemDecoration((int) PrintOSApplication.getAppContext()
                .getResources().getDimension(R.dimen.next_10_jobs_middle_separator_width)));
        displayModels();
    }

    public void displayModels(ServiceCallViewModel serviceCallViewModel) {
        this.serviceCallViewModel = serviceCallViewModel;
        displayModels();
    }

    private void displayModels() {
        if (serviceCallViewModel == null || serviceCallViewModel.getCallViewModels() == null ||
                serviceCallViewModel.getCallViewModels().isEmpty()) {
            return;
        }

        loadingView.setVisibility(View.GONE);

        final LinearLayoutManager manager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        callsList.setLayoutManager(manager);
        serviceCallsAdapter = new ServiceCallsAdapter(PrintOSApplication.getAppContext(), serviceCallViewModel,
                new ServiceCallsAdapter.ServiceCallAdapterCallback() {
                    @Override
                    public void onItemExpanded(int viewPosition) {
                        manager.scrollToPositionWithOffset(viewPosition, 0);
                    }

                    @Override
                    public void shareView(View share, ServiceCallViewModel.CallViewModel serviceCallViewModel) {
                        if (getActivity() instanceof MainActivity) {
                            toBeShared = share;
                            modelToBeShared = serviceCallViewModel;
                            ((MainActivity) getActivity()).share(ServiceCallsFragment.this);
                        }
                    }
                });
        callsList.setAdapter(serviceCallsAdapter);

        int firstOccurrence = 0;
        if (serviceCallStateEnum != ServiceCallStateEnum.OPEN) {
            firstOccurrence = serviceCallsAdapter.getFirstClosedOccurrence();
            if (serviceCallsAdapter.getFirstClosedOccurrence() > 0) {
                callsList.scrollToPosition(firstOccurrence);
            }
        }
    }

    public void performSharing() {
        if (toBeShared == null) {
            return;
        }
        final View share = toBeShared.findViewById(R.id.share_button);
        HPUIUtils.setVisibility(false, share);
        Bitmap bitmap = FileUtils.getScreenShot(toBeShared);
        HPUIUtils.setVisibility(true, share);

        if (bitmap == null) {
            return;
        }

        final Uri storageUri = FileUtils.store(getContext(), bitmap, SCREEN_SHOT_FILE_NAME);

        if (callback != null) {
            String title = getContext().getString(R.string.share_service_call_title,
                    modelToBeShared != null ? modelToBeShared.getPressName() : "");
            title = title == null ? "" : title;

            final String finalTitle = title;
            DeepLinkUtils.getShareBody(getActivity(),
                    Constants.UNIVERSAL_LINK_SCREEN_SERVICE_CALLS,
                    null, null, null, false, new DeepLinkUtils.BranchIOCallback() {
                        @Override
                        public void onLinkCreated(String link) {
                            callback.onScreenshotCreated(Panel.PERSONAL_ADVISOR, storageUri,
                                    finalTitle,
                                    link,
                                    ServiceCallsFragment.this);
                            if (share != null) {
                                share.setEnabled(true);
                                share.setClickable(true);
                            }
                        }
                    });
        } else if (share != null) {
            share.setEnabled(true);
            share.setClickable(true);
        }
    }

    public void onFilterSelected() {
        loadingView.setVisibility(View.VISIBLE);
    }

    public void onServiceCallDataRetrived() {
        loadingView.setVisibility(View.GONE);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_service_calls;
    }

    @Override
    public String getToolbarDisplayNameExtra() {
        return "";
    }

    @Override
    public boolean isFragmentNameToBeDisplayed() {
        return true;
    }

    @Override
    public boolean isFilteringAvailable() {
        return true;
    }

    @Override
    public int getToolbarDisplayName() {
        return R.string.service_call_fragment_name;
    }

    @Override
    public boolean onBackPressed() {
        return false;
    }

    @Override
    public boolean isIntercomAccessible() {
        return true;
    }

    @Override
    public boolean isSearchAvailable() {
        return true;
    }

    @Override
    public String getShareAction() {
        return AnswersSdk.SHARE_CONTENT_TYPE_SERVICE_CALLS;
    }

    public interface ServiceCallsFragmentCallback {
        void onScreenshotCreated(Panel personalAdvisor, Uri storageUri, String title, String body,
                                 SharableObject sharableObject);
    }
}
