package com.hp.printosmobile.presentation.modules.shared;

import com.hp.printosmobile.R;

/**
 * An enumeration representing the press states.
 *
 * @Author: Osama Taha
 * <p/>
 * Created on 7/15/2015.
 */
public enum DeviceState {

    ERROR("ERROR", R.string.press_state_error, 0, R.color.c58c, true, 1, "#EA5435"),
    WARNING("WARNING", R.string.press_state_warning, 1, R.color.c58c, true, 2),
    READY("READY", R.string.press_state_ready, 2, R.color.c58c, true, 3, "#7CCB7E"),
    UP("UP", R.string.press_state_up, 0, R.color.c58c, true, 3, "#F08A3C"),
    DOWN("DOWN", R.string.press_state_down, 0, R.color.c58c, true, 3),
    PRINTING("PRINTING", R.string.press_state_printing, 0, R.color.c58c, true, 3, "#53A955"),
    OFF("OFF", R.string.press_state_off, 0, R.color.c58c, true, 3, "#666666"),
    IDLE("IDLE", R.string.press_state_idle, 0, R.color.c58c, true, 3),
    CHECKING_PRINTER("CHECKINGPRINTER", R.string.press_state_checking_printer, 0, R.color.c58c, true, 3),
    JOBS_ON_HOLD("JOBSONHOLD", R.string.press_state_jobs_on_hold, 0, R.color.c58c, true, 3),
    LAMINATING("LAMINATING", R.string.press_state_laminating, 0, R.color.c58c, true, 3),
    STARTUP("STARTUP", R.string.press_state_startup, 0, R.color.c58c, true, 3),
    CALIBRATING("CALIBRATING", R.string.press_state_calibrating, 0, R.color.c58c, true, 3),
    DIAGNOSTIC_MODE("DIAGNOSTICMODE", R.string.press_state_diagnostic_mode, 0, R.color.c58c, true, 3),
    UNW("UNW", R.string.press_state_unknown, 0, R.color.c58c, true, 3),
    MEDIA_LOAD("MEDIALOAD", R.string.press_state_media_load, 0, R.color.c58c, true, 3),
    MAINTENANCE("MAINTENANCE", R.string.press_state_maintenance, 0, R.color.c58c, true, 3),
    PAUSED("PAUSED", R.string.press_state_paused, 0, R.color.c58c, true, 3),
    IN_POWER_SAVE("INPOWERSAVE", R.string.press_state_in_power_save, 0, R.color.c58c, true, 3),
    SLEEPING("SLEEPING", R.string.press_state_sleeping, 0, R.color.c58c, true, 3),
    INITIALIZING("INITIALIZING", R.string.press_state_initializing, 0, R.color.c58c, true, 3),
    STOPPED("STOPPED", R.string.press_state_checking_stopped, 0, R.color.c58c, true, 3),
    PROCESSING("PROCESSING", R.string.press_state_processing, 0, R.color.c58c, true, 3),
    FINISHING("FINISHING", R.string.press_state_finishing, 0, R.color.c58c, true, 3),
    DRYING("DRYING", R.string.press_state_drying, 0, R.color.c58c, true, 3),
    CANCELING("CANCELING", R.string.press_state_canceling, 0, R.color.c58c, true, 3),
    CANCEL_JOB("CANCELJOB", R.string.press_state_cancel_job, 0, R.color.c58c, true, 3),
    EJECTING("EJECTING", R.string.press_state_ejecting, 0, R.color.c58c, true, 3),
    RECEIVING("RECEIVING", R.string.press_state_receiving, 0, R.color.c58c, true, 3),
    CUTTING("CUTTING", R.string.press_state_cutting, 0, R.color.c58c, true, 3),
    PREPARING_TO_PRINT("PREPARINGTOPRINT", R.string.press_state_preparing_to_print, 0, R.color.c58c, true, 3),
    WAITING_TO_PRINT("WAITINGTOPRINT", R.string.press_state_waiting_to_print, 0, R.color.c58c, true, 3),
    DISCONNECTED("DISCONNECTED", R.string.press_state_disconnected, 0, R.color.c58c, true, 3, "#D5D5D5"),
    PRINTER_ERROR("PRINTERERROR", R.string.press_state_printer_error, 0, R.color.c58c, true, 3),
    DEVICE_STATE_AWAKE("AWAKE", R.string.press_state_awake, 0, R.color.c58c, true, 3),
    DEVICE_STATE_BUSY_WITH_ACTIVITIES("BUSYWITHACTIVITIES", R.string.press_state_busy_with_activities, 0, R.color.c58c, true, 3),
    DEVICE_STATE_DEVICE_MAINTENANCE("DEVICEMAINTENANCE", R.string.press_state_maintenance, 0, R.color.c58c, true, 3),
    DEVICE_STATE_DEVICE_OPTIMIZING("DEVICEOPTIMIZING", R.string.press_state_device_optimizing, 0, R.color.c58c, true, 3),
    DEVICE_STATE_DEVICE_RESHIPPING("DEVICERESHIPPING", R.string.press_state_device_reshipping, 0, R.color.c58c, true, 3),
    DEVICE_STATE_INSTALLING_ACCESSORY("INSTALLINGACCESSORY", R.string.press_state_installing_accessory, 0, R.color.c58c, true, 3),
    DEVICE_STATE_LEGACY_USER_REPORTED_TOP_LEVEL_STATUS("LEGACYUSERREPORTEDTOPLEVELSTATUS", R.string.press_state_legacy_user_reported_top_level_status, 0, R.color.c58c, true, 3),
    DEVICE_STATE_NO_ACTIVITY("NOACTIVITY", R.string.press_state_no_activity, 0, R.color.c58c, true, 3),
    DEVICE_STATE_NOT_INITIALIZED("NOTINITIALIZED", R.string.press_state_not_initialized, 0, R.color.c58c, true, 3),
    DEVICE_STATE_OK("OK", R.string.press_state_ok, 0, R.color.c58c, true, 3),
    DEVICE_STATE_PREPARING("PREPARING", R.string.press_state_preparing, 0, R.color.c58c, true, 3),
    DEVICE_STATE_REPLACING_CONSUMABLES("REPLACINGCONSUMABLES", R.string.press_state_replacing_consumables, 0, R.color.c58c, true, 3),
    DEVICE_STATE_SCANNING("SCANNING", R.string.press_state_scanning, 0, R.color.c58c, true, 3),
    DEVICE_STATE_SLEEP("SLEEP", R.string.press_state_sleep, 0, R.color.c58c, true, 3),
    DEVICE_STATE_TURN_OFF("TURNOFF", R.string.press_state_turn_off, 0, R.color.c58c, true, 3),
    DEVICE_STATE_UNREACHABLE("UNREACHABLE", R.string.press_state_unreachable, 0, R.color.c58c, true, 3),
    DEVICE_STATE_UPGRADE("UPGRADE", R.string.press_state_upgrade, 0, R.color.c58c, true, 3),
    DEVICE_STATE_WAITING("WAITING", R.string.press_state_waiting, 0, R.color.c58c, true, 3),
    DEVICE_STATE_WITH_ALERTS("WITHALERTS", R.string.press_state_with_alerts, 0, R.color.c58c, true, 3),
    DEVICE_STATE_WITH_SYSTEM_ERRORS("WITHSYSTEMERRORS", R.string.press_state_with_system_errors, 0, R.color.c58c, true, 3),
    UNKNOWN("UNKNOWN", R.string.press_state_unknown, 0, R.color.c58c, true, 3),
    NULL("NULL", R.string.press_state_null, 0, R.color.c58c, true, 4),
    NO_WARRANTY("NO_WARRANTY", R.string.press_state_printer_error, 0, R.color.c58c, true, 5);

    private static final String DEFAULT_COLOR = "#ffffff";

    private final int displayNameResourceId;
    private String state;
    private int sortOrder;
    private int color;
    private boolean visibility;
    private int latexSortOrder;
    private String stateDistributionColor;

    DeviceState(String state, int displayNameResourceId, int sortOrder, int color, boolean visibility, int latexSortOrder,
                String stateDistributionColor) {
        this.state = state;
        this.displayNameResourceId = displayNameResourceId;
        this.sortOrder = sortOrder;
        this.color = color;
        this.visibility = visibility;
        this.latexSortOrder = latexSortOrder;
        this.stateDistributionColor = stateDistributionColor;
    }

    DeviceState(String state, int displayNameResourceId, int sortOrder, int color, boolean visibility, int latexSortOrder) {
        this.state = state;
        this.displayNameResourceId = displayNameResourceId;
        this.sortOrder = sortOrder;
        this.color = color;
        this.visibility = visibility;
        this.latexSortOrder = latexSortOrder;
        this.stateDistributionColor = DEFAULT_COLOR;
    }

    public String getState() {
        return state;
    }

    private void setColor(int color) {
        this.color = color;
    }

    public int getColor() {
        return color;
    }

    public int getSortOder() {
        return sortOrder;
    }

    public boolean isVisible() {
        return visibility;
    }

    public int getDisplayNameResourceId() {
        return displayNameResourceId;
    }

    public int getLatexSortOrder() {
        return latexSortOrder;
    }

    public String getStateDistributionColor() {
        return stateDistributionColor;
    }

    public static DeviceState from(String state, String color) {

        if (state == null) {
            return DeviceState.UNW;
        }

        for (DeviceState deviceState : DeviceState.values()) {
            if (deviceState.getState().equalsIgnoreCase(state.toLowerCase())) {
                deviceState.setColor(DeviceStateColor.from(color).colorResource);
                return deviceState;
            }
        }

        return DeviceState.UNW;

    }

    public static DeviceState from(String state) {

        if (state == null) {
            return DeviceState.UNW;
        }

        for (DeviceState deviceState : DeviceState.values()) {
            if (deviceState.getState().toLowerCase().equals(state.toLowerCase())) {
                return deviceState;
            }
        }

        return DeviceState.UNW;
    }

    public enum DeviceStateColor {

        GOOD("GOOD", R.color.c58a),
        BAD("BAD", R.color.c58c),
        NEUTRAL("NEUTRAL", R.color.c58b);

        private final String colorType;
        private final int colorResource;

        DeviceStateColor(String colorType, int colorResource) {
            this.colorType = colorType;
            this.colorResource = colorResource;
        }

        public String getColorType() {
            return colorType;
        }

        public static DeviceStateColor from(String colorType) {

            if (colorType == null) {
                return DeviceStateColor.NEUTRAL;
            }

            for (DeviceStateColor deviceStateColor : DeviceStateColor.values()) {
                if (deviceStateColor.getColorType().equalsIgnoreCase(colorType.toLowerCase())) {
                    return deviceStateColor;
                }
            }
            return DeviceStateColor.NEUTRAL;

        }
    }
}