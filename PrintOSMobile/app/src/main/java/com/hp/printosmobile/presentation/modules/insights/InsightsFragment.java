package com.hp.printosmobile.presentation.modules.insights;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TabLayout;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hp.printosmobile.Analytics;
import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.data.remote.models.kz.KZItem;
import com.hp.printosmobile.presentation.BaseFragment;
import com.hp.printosmobile.presentation.Navigator;
import com.hp.printosmobile.presentation.TargetViewManager;
import com.hp.printosmobile.presentation.modules.home.Panel;
import com.hp.printosmobile.presentation.modules.insights.InsightsViewModel.InsightKpiEnum;
import com.hp.printosmobile.presentation.modules.insights.kz.InsightsSearchFragment;
import com.hp.printosmobile.presentation.modules.insights.kz.KZDefaultView;
import com.hp.printosmobile.presentation.modules.insights.kz.KZSupportedFormat;
import com.hp.printosmobile.presentation.modules.insights.kz.KZVideoActivity;
import com.hp.printosmobile.presentation.modules.insights.kz.KZVideoView;
import com.hp.printosmobile.presentation.modules.insights.kz.KZDefaultDetailsFragment;
import com.hp.printosmobile.presentation.modules.insights.kz.kzfooter.FavoritesActivity;
import com.hp.printosmobile.presentation.modules.insights.kz.kzfooter.KZItemDetailsCallback;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitEnum;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitViewModel;
import com.hp.printosmobile.presentation.modules.main.IMainFragment;
import com.hp.printosmobile.presentation.modules.main.MainActivity;
import com.hp.printosmobile.presentation.modules.personaladvisor.AdviceViewModel;
import com.hp.printosmobile.presentation.modules.personaladvisor.PersonalAdvisorPanel;
import com.hp.printosmobile.presentation.modules.personaladvisor.PersonalAdvisorViewModel;
import com.hp.printosmobile.presentation.modules.personaladvisor.presenter.PersonalAdvisorFactory;
import com.hp.printosmobile.presentation.modules.shared.DeviceViewModel;
import com.hp.printosmobile.utils.HPUIUtils;
import com.hp.printosmobilelib.core.communications.remote.APIException;
import com.hp.printosmobilelib.core.utils.HPDateUtils;
import com.hp.printosmobilelib.ui.widgets.HPScrollView;
import com.hp.printosmobilelib.ui.widgets.panel.PanelView;
import com.hp.printosmobilelib.ui.widgets.panel.SharableObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.OnClick;
import rx.Subscriber;

/**
 * Created by Osama Taha on 6/13/2017.
 */

public class InsightsFragment extends BaseFragment implements IMainFragment, InsightsView, KpiInsightPanel.KpiInsightPanelCallback, PersonalAdvisorPanel.PersonalAdvicePanelCallback, KZItemDetailsCallback, InsightsSearchFragment.InsightsView, FavoritesActivity.InsightsView {

    private static final String API_DATE_FORMAT = "yyyy-MM-dd";
    private static final long SCROLL_TO_ADVICE_DELAY = 600;
    private static final long SCROLL_TO_PANEL_DELAY = 200;

    @Bind(R.id.insights_fragment_layout)
    View insightsFragmentLayout;
    @Bind(R.id.panels_container)
    LinearLayout panelContainer;
    @Bind(R.id.scroll_view)
    HPScrollView scrollView;
    @Bind(R.id.no_date_text_view)
    TextView noDataTextView;
    @Bind(R.id.search_card_view)
    View searchCard;
    @Bind(R.id.coordinatorLayout)
    CoordinatorLayout coordinatorLayout;
    @Bind(R.id.loading_indicator)
    View loadingIndicator;
    @Bind(R.id.divisions_selector)
    TabLayout divisionsSelectorView;
    @Bind(R.id.kz_search_label)
    TextView searchLabel;

    private BusinessUnitViewModel businessUnitModel;
    private InsightsPresenter presenter;
    private Panel focusablePanel;
    private boolean reloadPanels;
    private boolean wasDataLoaded;
    private boolean knowledgeZoneSearchEnabled;
    private List<BusinessUnitEnum> businessUnitEnums = new ArrayList<>();
    private InsightsFragmentCallback callback;
    private final List<Object> panels = new ArrayList<>();
    private Map<Panel, PanelView> panelMap = new HashMap<>();
    private BusinessUnitEnum selectedBu;

    public static InsightsFragment newInstance() {
        return new InsightsFragment();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
    }

    private void init() {

        wasDataLoaded = false;
        presenter = new InsightsPresenter();
        presenter.attachView(this);

        enableNestedScrolling(false);

        knowledgeZoneSearchEnabled = PrintOSPreferences.getInstance(getActivity()).isKnowledgeZoneSearchEnabled();

        HPUIUtils.setVisibility(knowledgeZoneSearchEnabled, searchCard);

        scrollView.setOnScrollStoppedListener(new HPScrollView.OnScrollStopListener() {
            @Override
            public void onScrollStopped(int y) {
                displayTargetViewIfAny();
            }

            @Override
            public void onScrollDown() {
            }

            @Override
            public void onScrollUp() {
                enableAdviceVerticalScrolling(false);
            }

            @Override
            public void onTopReached() {
            }

            @Override
            public void onBottomReached() {
                enableAdviceVerticalScrolling(true);
            }
        });

    }

    @Override
    public void displayTargetViewIfAny() {
        super.displayTargetViewIfAny();

        if (!isAdded() || getHostingActivity() == null) {
            return;
        }

        if (isDisplayTargetViews()) {
            setDisplayTargetViews(!TargetViewManager.showTargetView(getActivity(), TargetViewManager.TargetView.INSIGHTS_SEARCH_BAR.getTag(),
                    searchCard, this));

            if (isDisplayTargetViews()) {
                setDisplayTargetViews(!TargetViewManager.showTargetViewForVisible(
                        getHostingActivity(), scrollView, panelMap, this));
            }
        }
    }

    @Override
    public void onTapTargetClick(TargetViewManager.TargetView targetView) {
        super.onTapTargetClick(targetView);
        switch (targetView) {
            case INSIGHTS_CORRECTIVE_ACTION_FAILURE:
                if (panelMap != null && panelMap.containsKey(Panel.FAILURE_CHART)) {
                    PanelView panelView = panelMap.get(Panel.FAILURE_CHART);
                    if (panelView instanceof KpiInsightPanel) {
                        ((KpiInsightPanel) panelView).onFirstCorrectiveActionsButtonClicked();
                    }
                }
                return;
            case INSIGHTS_CORRECTIVE_ACTION_JAMS:
                if (panelMap != null && panelMap.containsKey(Panel.JAM_CHART)) {
                    PanelView panelView = panelMap.get(Panel.JAM_CHART);
                    if (panelView instanceof KpiInsightPanel) {
                        ((KpiInsightPanel) panelView).onFirstCorrectiveActionsButtonClicked();
                    }
                }
                return;
            default:
                break;
        }
    }

    @Override
    public void onCancel() {

    }

    public Activity getHostingActivity() {
        return getActivity();
    }

    @OnClick(R.id.search_card_view)
    public void onSearchCardViewClicked() {

        if (callback != null) {

            BusinessUnitEnum selectedBu = getSelectedBu();

            if (businessUnitEnums.size() > 0) {
                selectedBu = businessUnitEnums.get(divisionsSelectorView.getSelectedTabPosition());
            } else if (businessUnitModel != null) {
                selectedBu = businessUnitModel.getBusinessUnit();
            }

            if (selectedBu != null) {
                callback.onKZBusinessUnitSelected(selectedBu);
                Analytics.sendEvent(Analytics.CLICK_INSIGHTS_SEARCH, selectedBu.getShortName());
            }

        }

    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            callback = (InsightsFragmentCallback) getActivity();
        } catch (ClassCastException e) {
            throw new RuntimeException("parent activity must implement InsightsFragmentCallback");
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_insights;
    }

    @Override
    public boolean isFragmentNameToBeDisplayed() {
        return true;
    }

    @Override
    public boolean isFilteringAvailable() {
        return true;
    }

    @Override
    public int getToolbarDisplayName() {
        return R.string.fragment_insights_name;
    }

    @Override
    public boolean isIntercomAccessible() {
        return true;
    }

    @Override
    public void onBusinessUnitSelected(BusinessUnitViewModel businessUnitModel) {

        if (businessUnitModel == null && (!PrintOSPreferences.getInstance(getHostingActivity()).isHPOrg()
                && !PrintOSPreferences.getInstance(getHostingActivity()).isChannelSupportKz())) {
            return;
        }

        knowledgeZoneSearchEnabled = PrintOSPreferences.getInstance(getActivity()).isKnowledgeZoneSearchEnabled();

        scrollView.setVisibility(View.VISIBLE);
        panelContainer.setVisibility(View.VISIBLE);

        wasDataLoaded = false;
        reloadPanels = true;

        if (businessUnitModel != null) {
            this.businessUnitModel = businessUnitModel;
        }

        if (callback != null) {
            callback.onInsightsBusinessUnitSet();
        }

    }

    public void loadScreen() {

        if (getActivity() == null || wasDataLoaded
                || (businessUnitModel == null && (!PrintOSPreferences.getInstance(getHostingActivity()).isHPOrg() &&
                !PrintOSPreferences.getInstance(getHostingActivity()).isChannelSupportKz()))) {
            return;
        }

        wasDataLoaded = true;

        if (reloadPanels) {
            clearPanels();
        }

        getData();

    }

    private void clearPanels() {

        panelContainer.removeAllViews();
        panels.clear();
        panelMap.clear();
    }

    @Override
    public void onFiltersChanged(BusinessUnitViewModel businessUnitViewModel) {

        onBusinessUnitSelected(businessUnitViewModel);
    }

    @Override
    public void didFinishGettingBusinessUnits(Map<BusinessUnitEnum, BusinessUnitViewModel> businessUnitsMap) {
        //do nothing
    }

    @Override
    public void onMainError(String msg, boolean triggerSignOut, boolean isHasNoDevices) {
        showEmptyDatasetLayout(R.string.error_no_data_msg);
    }

    void showEmptyDatasetLayout(int msgResId) {
        showErrorView();
        noDataTextView.setText(PrintOSApplication.getAppContext().getString(msgResId));
    }

    @Override
    public boolean respondToValidationPeriodExceedance() {
        return false;
    }


    private void getData() {

        if (PrintOSPreferences.getInstance(getActivity()).isHPOrg() || PrintOSPreferences.getInstance(getActivity()).isChannelSupportKz()) {

            presenter.getKzBusinessUnits(getActivity());

        } else {

            getInsightsData(businessUnitModel.getBusinessUnit());

        }

    }

    private void getInsightsData(final BusinessUnitEnum selectedBu) {

        clearPanels();

        final KpiInsightPanel panel = new KpiInsightPanel(getActivity());
        final LinearLayout.LayoutParams layoutParams = HPUIUtils.getPanelLayoutParams(getContext());

        String from = HPDateUtils.formatDate(panel.getDateFrom(), API_DATE_FORMAT);
        String to = HPDateUtils.formatDate(panel.getDateTo(), API_DATE_FORMAT);

        if (PrintOSPreferences.getInstance(getHostingActivity()).isHPOrg()
                || PrintOSPreferences.getInstance(getHostingActivity()).isChannelSupportKz()) {
            businessUnitModel = new BusinessUnitViewModel();
            businessUnitModel.setBusinessUnit(selectedBu);
        }

        showLoadingView();

        presenter.getInsightsData(businessUnitModel, from, to, false)
                .subscribe(new Subscriber<List<Object>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        showEmptyLayout(true);
                    }

                    @Override
                    public void onNext(List<Object> objects) {

                        if (objects == null || objects.size() == 0) {
                            showEmptyLayout(true);
                            return;
                        }

                        showEmptyLayout(false);

                        for (final Object object : objects) {

                            PanelView panelView = null;

                            if (object instanceof InsightsViewModel) {

                                InsightsViewModel viewModel = (InsightsViewModel) object;
                                panelView = new KpiInsightPanel(getActivity());
                                ((KpiInsightPanel) panelView).addCallback(InsightsFragment.this);
                                panelView.updateViewModel(viewModel);

                                if (viewModel.getInsightKpiEnum() == InsightKpiEnum.JAM) {
                                    panelMap.put(Panel.JAM_CHART, panelView);
                                } else {
                                    panelMap.put(Panel.FAILURE_CHART, panelView);
                                }

                            } else if (object instanceof PersonalAdvisorViewModel) {

                                panelView = new PersonalAdvisorPanel(getActivity());
                                ((PersonalAdvisorPanel) panelView).addPersonalAdvisorPanelCallback(InsightsFragment.this);
                                PersonalAdvisorFactory.getInstance().addObserver((PersonalAdvisorPanel) panelView);
                                panelMap.put(Panel.PERSONAL_ADVISOR, panelView);
                                PersonalAdvisorFactory.getInstance().loadData(businessUnitModel);

                            } else if (object instanceof KZItem) {

                                final KZItem kzItem = (KZItem) object;

                                InsightsSearchFragment.addCallback(InsightsFragment.this);
                                FavoritesActivity.addCallback(InsightsFragment.this);

                                if (kzItem.getFormat().equals(KZSupportedFormat.MP4.getValue())) {
                                    panelView = new KZVideoView(getActivity(), selectedBu.getKzName());
                                    KZVideoView.addKZItemDetailsCallback(InsightsFragment.this);
                                    panelView.updateViewModel(kzItem);
                                } else {
                                    panelView = new KZDefaultView(getActivity(), selectedBu.getKzName());
                                    KZDefaultView.addKZItemDetailsCallback(InsightsFragment.this);
                                    panelView.updateViewModel(kzItem);
                                }

                                if (panelView == null) {
                                    continue;
                                }


                                final PanelView finalPanelView1 = panelView;
                                panelView.findViewById(R.id.info_ll).setOnClickListener(new View.OnClickListener() {

                                    @Override
                                    public void onClick(View view) {

                                        if (getActivity() == null) {
                                            return;
                                        }

                                        getBusinessUnitModel();

                                        Analytics.sendEvent(String.format(Analytics.OPEN_INSIGHTS_ITEM,
                                                getBusinessUnitModel().getShortName()), KZSupportedFormat.from(kzItem.getFormat()).getValue());

                                        Navigator.openKZDetailsActivity(getActivity(), ((KZDefaultView) finalPanelView1).getViewModel(), getBusinessUnitModel());

                                        if (KZSupportedFormat.from(kzItem.getFormat()) == KZSupportedFormat.MP4) {
                                            KZVideoActivity.addKZItemDetailsCallback(InsightsFragment.this);
                                        } else {
                                            KZDefaultDetailsFragment.addKZItemDetailsCallback(InsightsFragment.this);
                                        }

                                    }
                                });

                            }

                            if (panelView != null) {
                                panelContainer.addView(panelView, layoutParams);
                                panels.add(panelView);
                            }

                        }

                    }
                });
    }

    public BusinessUnitEnum getBusinessUnitModel() {
        if (businessUnitModel != null) {
            selectedBu = businessUnitModel.getBusinessUnit();
        } else {
            selectedBu = getSelectedBu();
        }

        return selectedBu;
    }


    @Override
    public void onInsightTop5DataRetrieved(InsightsViewModel viewModel, InsightKpiEnum kpiEnum, boolean onDateSelected) {

        KpiInsightPanel insightsPanel = getKPIInsightPanel(kpiEnum);
        if (insightsPanel != null) {
            insightsPanel.updateViewModel(viewModel);
        }

    }

    public void showDivisionSelectorView() {
        divisionsSelectorView.setVisibility(View.VISIBLE);
    }

    @Override
    public void showLoadingView() {
        loadingIndicator.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoadingView() {
        loadingIndicator.setVisibility(View.GONE);
    }

    @Override
    public void showErrorView() {
        noDataTextView.setVisibility(View.VISIBLE);
        panelContainer.setVisibility(View.GONE);
    }

    @Override
    public void hideErrorView() {
        noDataTextView.setVisibility(View.GONE);
        panelContainer.setVisibility(View.VISIBLE);
    }

    @Override
    public void onBusinessUnitDataReceived(final List<BusinessUnitEnum> businessUnitEnums) {

        if (getActivity() == null) {
            return;
        }

        searchCard.setVisibility(View.VISIBLE);

        showDivisionSelectorView();

        this.businessUnitEnums = businessUnitEnums;
        for (BusinessUnitEnum businessUnitEnum : businessUnitEnums) {
            divisionsSelectorView.addTab(divisionsSelectorView.newTab()
                    .setText(businessUnitEnum.getBusinessUnitDisplayName(true)), false);
        }

        BusinessUnitEnum selectedBu = PrintOSPreferences.getInstance(getActivity()).getKZLastSelectedBu();

        if (businessUnitEnums.indexOf(selectedBu) > -1) {
            divisionsSelectorView.getTabAt(businessUnitEnums.indexOf(selectedBu)).select();
        } else {
            selectedBu = businessUnitEnums.get(0);
            divisionsSelectorView.getTabAt(0).select();
        }

        searchLabel.setText(BusinessUnitViewModel.getKZSearchPlaceHolder(selectedBu));

        setSelectedBu(selectedBu);

        divisionsSelectorView.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                if (tab.getPosition() < 0 || businessUnitEnums.isEmpty() || tab.getPosition() >= businessUnitEnums.size()) {
                    return;
                }

                BusinessUnitEnum selectedBu = businessUnitEnums.get(tab.getPosition());

                setSelectedBu(selectedBu);

                searchLabel.setText(BusinessUnitViewModel.getKZSearchPlaceHolder(selectedBu));

                PrintOSPreferences.getInstance(getActivity()).setKZLastSelectedBu(selectedBu);

                getInsightsData(selectedBu);

                enableTabSelection(false);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        getInsightsData(selectedBu);

    }

    @Override
    public void onEmptyBusinessUnits() {
        showEmptyDatasetLayout(R.string.insights_no_insight);
        searchCard.setVisibility(View.GONE);
    }

    private void enableTabSelection(boolean enable) {

        LinearLayout tabStrip = ((LinearLayout) divisionsSelectorView.getChildAt(0));
        tabStrip.setEnabled(false);
        for (int i = 0; i < tabStrip.getChildCount(); i++) {
            tabStrip.getChildAt(i).setClickable(enable);
        }

    }

    private synchronized void showEmptyLayout(boolean showEmptyLayout) {

        if (showEmptyLayout) {

            if (knowledgeZoneSearchEnabled) {

                showEmptyDatasetLayout(R.string.insights_no_recommended_results);

            } else {

                boolean isNull = businessUnitModel == null || businessUnitModel.getFiltersViewModel() == null;
                int totalNumberOfDevices = isNull || businessUnitModel.getFiltersViewModel().getSiteDevicesIds() == null ?
                        0 : businessUnitModel.getFiltersViewModel().getSiteDevicesIds().size();
                int selectedNumberOfDevices = isNull || businessUnitModel.getFiltersViewModel().getSelectedDevices() == null ?
                        0 : businessUnitModel.getFiltersViewModel().getSelectedDevices().size();

                showEmptyDatasetLayout(totalNumberOfDevices != selectedNumberOfDevices ?
                        R.string.insights_no_insight_change_filter : R.string.insights_no_insight);

            }

            showErrorView();

        } else {
            hideErrorView();
        }

        enableNestedScrolling(!showEmptyLayout);

        hideLoadingView();
        enableTabSelection(true);

    }

    private void enableNestedScrolling(final boolean enable) {

        scrollView.setNestedScrollingEnabled(enable);
    }

    @Override
    public void onError(APIException exception, String tag) {

        onInsightTop5DataRetrieved(null, InsightKpiEnum.JAM.getTag().equals(tag) ? InsightKpiEnum.JAM
                : InsightKpiEnum.FAILURE, false);

        if (callback != null) {
            callback.moveToHomeFromTab(getString(R.string.fragment_insights_name_key));
        }
    }

    @Override
    public void onDateSelected(Date from, Date to, InsightKpiEnum insightKpiEnum) {

        KpiInsightPanel insightsPanel = getKPIInsightPanel(insightKpiEnum);
        if (insightsPanel == null) {
            return;
        }

        insightsPanel.showLoading();

        presenter.getTop5Data(businessUnitModel, insightKpiEnum,
                HPDateUtils.formatDate(from, API_DATE_FORMAT),
                HPDateUtils.formatDate(to, API_DATE_FORMAT), true);
    }

    @Override
    public void onShareButtonClicked(Panel panel) {
        performPanelSharing(panel);
    }

    public void performPanelSharing(Panel panel) {

        if (getActivity() instanceof MainActivity) {

            KpiInsightPanel insightsPanel = null;
            if (panel == Panel.JAM_CHART) {
                insightsPanel = getKPIInsightPanel(InsightKpiEnum.JAM);
            } else if (panel == Panel.FAILURE_CHART) {
                insightsPanel = getKPIInsightPanel(InsightKpiEnum.FAILURE);
            }

            if (insightsPanel != null) {
                ((MainActivity) getActivity()).share(insightsPanel);
            }

        }
    }

    public KpiInsightPanel getKPIInsightPanel(InsightKpiEnum insightKpiEnum) {

        for (Object object : panels) {
            if (object instanceof KpiInsightPanel) {
                KpiInsightPanel kpiInsightPanel = (KpiInsightPanel) object;
                if (kpiInsightPanel.getViewModel().getInsightKpiEnum() == insightKpiEnum) {
                    return kpiInsightPanel;
                }
            }
        }

        return null;
    }

    @Override
    public void onScreenshotCreated(Panel panel, Uri uri, String subject, String body, SharableObject sharableObject) {
        shareImage(uri, subject, body, sharableObject);
    }

    @Override
    public void onReadMoreClicked() {
        //Not for now.
    }

    @Override
    public int getFragmentHeight() {
        return insightsFragmentLayout.getHeight();
    }

    @Override
    public void updatePersonalAdvisorPanel(PersonalAdvisorViewModel personalAdvisorViewModel) {

        if (!panelMap.containsKey(Panel.PERSONAL_ADVISOR)) {
            return;
        }

        PersonalAdvisorPanel personalAdvisorPanel = (PersonalAdvisorPanel) panelMap.get(Panel.PERSONAL_ADVISOR);
        scrollFragmentToPanel(focusablePanel);

        boolean showAdvisorPanel = personalAdvisorViewModel != null && personalAdvisorViewModel.getAllAdvices() != null
                && !personalAdvisorViewModel.getAllAdvices().isEmpty();
        HPUIUtils.setVisibility(showAdvisorPanel, personalAdvisorPanel);

        scrollFragmentToPanel(focusablePanel);

        onLoadingDone(panelMap);
    }

    @Override
    public void onPersonalAdvisorError(APIException e, String tag) {

        updatePersonalAdvisorPanel(null);
    }

    public void scrollToNewestAdvice() {
        scrollView.postDelayed(new Runnable() {
            @Override
            public void run() {

                if (panelMap.containsKey(Panel.PERSONAL_ADVISOR)) {
                    PersonalAdvisorPanel personalAdvisorPanel = (PersonalAdvisorPanel) panelMap.get(Panel.PERSONAL_ADVISOR);
                    personalAdvisorPanel.scrollToNewestAdvice();
                }

            }
        }, SCROLL_TO_ADVICE_DELAY);
    }

    private void scrollFragmentToPanel(final Panel panel) {
        if (focusablePanel == null || panelMap == null || !panelMap.containsKey(panel)) {
            return;
        }

        final PanelView panelView = panelMap.get(panel);

        scrollView.postDelayed(new Runnable() {
            @Override
            public void run() {
                scrollView.scrollTo(0, panelView.getTop());
                scrollToNewestAdvice();
                focusablePanel = null;
            }
        }, SCROLL_TO_PANEL_DELAY);

    }

    private void enableAdviceVerticalScrolling(boolean enableScrolling) {
        if (panelMap != null && panelMap.containsKey(Panel.PERSONAL_ADVISOR)) {
            PersonalAdvisorPanel panel = (PersonalAdvisorPanel) panelMap.get(Panel.PERSONAL_ADVISOR);
            panel.enableAdviceScrolling(enableScrolling);
        }
    }

    public void scrollToAdvice(final AdviceViewModel model) {
        onHasAdviceIconClicked(null, model);
    }

    private void onHasAdviceIconClicked(final DeviceViewModel model, final AdviceViewModel adviceViewModel) {

        focusablePanel = Panel.PERSONAL_ADVISOR;
        scrollFragmentToPanel(focusablePanel);

        scrollView.postDelayed(new Runnable() {
            @Override
            public void run() {

                if (panelMap.containsKey(Panel.PERSONAL_ADVISOR)) {
                    PersonalAdvisorPanel personalAdvisorPanel = (PersonalAdvisorPanel) panelMap.get(Panel.PERSONAL_ADVISOR);
                    personalAdvisorPanel.scrollToAdvice(model, adviceViewModel);
                }

            }
        }, SCROLL_TO_ADVICE_DELAY);
    }

    public void setFocusablePanel(Panel panel) {
        this.focusablePanel = panel;
    }

    @Override
    public boolean onBackPressed() {
        if (callback != null) {
            callback.onInsightsFragmentBackPress();
        }
        return true;
    }

    @Override
    public void onFavoritePressed(KZItem kzItem) {
        updatePanel(kzItem);
    }

    @Override
    public void onLikePressed(KZItem kzItem) {
        updatePanel(kzItem);
    }

    @Override
    public void onDislikePressed(KZItem kzItem) {
        updatePanel(kzItem);
    }

    private void updatePanel(KZItem kzItem) {
        updateView(kzItem);
    }

    @Override
    public void updateView(KZItem kzItem) {

        for (Object panel : panels) {
            if (panel instanceof KZDefaultView || panel instanceof KZVideoView) {

                PanelView panelView = ((PanelView) panel);
                KZItem item = (KZItem) (panelView.getViewModel());
                if (item.getId().equals(kzItem.getId())) {
                    panelView.updateViewModel(kzItem);
                    return;
                }


            }
        }
    }

    public interface InsightsFragmentCallback {

        void onInsightsFragmentBackPress();

        void onInsightsBusinessUnitSet();

        void moveToHomeFromTab(String string);

        void onKZBusinessUnitSelected(BusinessUnitEnum businessUnit);

    }

    public void setSelectedBu(BusinessUnitEnum selectedBu) {
        this.selectedBu = selectedBu;
    }

    public BusinessUnitEnum getSelectedBu() {
        return selectedBu;
    }
}
