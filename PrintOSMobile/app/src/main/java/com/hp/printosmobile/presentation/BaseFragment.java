package com.hp.printosmobile.presentation;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.view.View;

import com.hp.printosmobile.Constants;
import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;
import com.hp.printosmobile.utils.AnalyticsUtils;
import com.hp.printosmobile.utils.HPUIUtils;
import com.hp.printosmobilelib.ui.common.HPFragment;
import com.hp.printosmobilelib.ui.widgets.panel.PanelView;
import com.hp.printosmobilelib.ui.widgets.panel.SharableObject;

import java.util.Map;

/**
 * Created by Anwar Asbah on 5/11/17.
 */
public abstract class BaseFragment extends HPFragment implements TargetViewManager.TapTargetCallback {

    private boolean displayTargetViews = true;

    protected void shareImage(Uri storageUri, String subject, String body, SharableObject sharableObject) {

        if (sharableObject != null) {
            AnalyticsUtils.sendShareEvents(getContext(), sharableObject.getShareAction());
        }

        Intent intent;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            intent = new Intent(Intent.ACTION_SEND);
            intent.addFlags(Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION);
        } else {
            intent = new Intent(Intent.ACTION_SEND);
        }

        String subjectExtra = subject == null ? "" : subject;
        String bodyExtra = body == null ? "" : body;
        intent.putExtra(android.content.Intent.EXTRA_SUBJECT, subjectExtra);
        intent.putExtra(android.content.Intent.EXTRA_TEXT, subjectExtra + "\n" + bodyExtra);
        intent.putExtra(Intent.EXTRA_STREAM, storageUri);
        intent.setType("*/*");
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        try {
            startActivityForResult(Intent.createChooser(intent, PrintOSApplication.getAppContext().getString(R.string.share_with)), Constants.IntentExtras.APPS_PICKER_INTENT_REQUEST_CODE);
        } catch (ActivityNotFoundException e) {
            HPUIUtils.displayToast(PrintOSApplication.getAppContext(), PrintOSApplication.getAppContext().getString(R.string.no_app_for_sharing));
        }
    }

    public void setDisplayTargetViews(boolean display) {
        this.displayTargetViews = display;
    }

    public boolean isDisplayTargetViews() {
        return displayTargetViews;
    }

    public boolean isFullyLoaded(Map<?, PanelView> panelViewMap) {
        if (panelViewMap == null) {
            return false;
        }

        boolean isLoading = false;
        for (Object object : panelViewMap.keySet()) {
            isLoading = isLoading || (panelViewMap.get(object).isLoading() && panelViewMap.get(object).getVisibility() == View.VISIBLE);
        }

        return !isLoading;
    }

    public void displayTargetViewIfAny() {
        //To be customized if needed
    }

    public void onLoadingDone(Map<?, PanelView> panelViewMap) {
        if (isFullyLoaded(panelViewMap)) {
            displayTargetViewIfAny();
        }
    }

    public void onTapTargetClick(TargetViewManager.TargetView targetView) {
        //To be customized if needed
    }
}
