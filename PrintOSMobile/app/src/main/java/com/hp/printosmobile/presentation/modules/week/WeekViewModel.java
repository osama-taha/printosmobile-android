package com.hp.printosmobile.presentation.modules.week;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.content.res.ResourcesCompat;

import com.hp.printosmobile.R;
import com.hp.printosmobile.presentation.modules.kpiview.KPIScoreStateEnum;
import com.hp.printosmobile.presentation.modules.kpiview.KPIViewModel;
import com.hp.printosmobile.utils.HPLocaleUtils;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * Created by Anwar Asbah on 4/12/2016.
 */
public class WeekViewModel implements Comparable<WeekViewModel> {

    List<KPIViewModel> kpis;
    Date fromDate;
    Date toDate;
    int relativeIndexToLastWeek;
    int weekScore;
    int weekMaxScore;
    KPIScoreStateEnum performance;
    boolean hasRankingData;
    RankingViewModel rankingViewModel;
    List<WeekPressStatus> weekPressStatuses;
    boolean lowPrintVolume;

    public static final Comparator<WeekViewModel> DATE_COMPARATOR = new Comparator<WeekViewModel>() {
        @Override
        public int compare(WeekViewModel model1, WeekViewModel model2) {
            return model1.compareTo(model2);
        }
    };
    private int weekNumber;

    public Date getFromDate() {
        return fromDate;
    }

    public int getWeekScore() {
        return weekScore;
    }

    public void setWeekScore(int weekScore) {
        this.weekScore = weekScore;
    }

    public int getWeekMaxScore() {
        return weekMaxScore;
    }

    public void setWeekMaxScore(int weekMaxScore) {
        this.weekMaxScore = weekMaxScore;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public int getRelativeIndexToLastWeek() {
        return relativeIndexToLastWeek;
    }

    public void setRelativeIndexToLastWeek(int relativeIndexToLastWeek) {
        this.relativeIndexToLastWeek = relativeIndexToLastWeek;
    }

    public KPIScoreStateEnum getPerformance() {
        return performance;
    }

    public void setPerformance(KPIScoreStateEnum performance) {
        this.performance = performance;
    }

    public List<KPIViewModel> getKpis() {
        return kpis;
    }

    public void setKpis(List<KPIViewModel> kpis) {
        this.kpis = kpis;
    }

    public boolean hasRankingData() {
        return hasRankingData;
    }

    public void setHasRankingData(boolean hasRankingData) {
        this.hasRankingData = hasRankingData;
    }

    public RankingViewModel getRankingViewModel() {
        return rankingViewModel;
    }

    public void setRankingViewModel(RankingViewModel rankingViewModel) {
        this.rankingViewModel = rankingViewModel;
    }

    public List<WeekPressStatus> getWeekPressStatuses() {
        return weekPressStatuses;
    }

    public void setWeekPressStatuses(List<WeekPressStatus> weekPressStatuses) {
        this.weekPressStatuses = weekPressStatuses;
    }

    public boolean isLowPrintVolume() {
        return lowPrintVolume;
    }

    public void setLowPrintVolume(boolean lowPrintVolume) {
        this.lowPrintVolume = lowPrintVolume;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("WeekViewModel{");
        sb.append("kpis=").append(kpis);
        sb.append(", fromDate=").append(fromDate);
        sb.append(", toDate=").append(toDate);
        sb.append(", relativeIndexToLastWeek=").append(relativeIndexToLastWeek);
        sb.append(", weekScore=").append(weekScore);
        sb.append(", weekMaxScore=").append(weekMaxScore);
        sb.append(", performance=").append(performance);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public int compareTo(WeekViewModel another) {
        if (another == null) {
            return 1;
        }
        return this.getToDate().compareTo(another.toDate);
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @Override
    public int hashCode() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            return Objects.hash(kpis, fromDate, toDate, relativeIndexToLastWeek, weekScore, weekMaxScore, performance, hasRankingData, rankingViewModel, weekPressStatuses, lowPrintVolume, weekNumber);
        }
        return 0;
    }

    public void setWeekNumber(int weekNumber) {
        this.weekNumber = weekNumber;
    }

    public int getWeekNumber() {
        return weekNumber;
    }

    public static class WeekPressStatus implements Serializable, Comparable<WeekPressStatus> {

        private static final float[] CORNER_RADII = new float[]{100, 100, 100, 100, 100, 100, 100, 100};

        public static final Comparator<WeekPressStatus> weekPressStatusComparator = new Comparator<WeekPressStatus>() {
            @Override
            public int compare(WeekPressStatus weekPressStatus, WeekPressStatus weekPressStatus2) {
                if (weekPressStatus == null && weekPressStatus2 == null) {
                    return 0;
                } else if (weekPressStatus == null) {
                    return 1;
                } else if (weekPressStatus2 == null) {
                    return -1;
                }
                return weekPressStatus.compareTo(weekPressStatus2);
            }
        };

        @Override
        public int compareTo(@NonNull WeekPressStatus weekPressStatus) {
            WeeklyPressStatusEnum thisEnum = getWeeklyPressStatusEnum();
            WeeklyPressStatusEnum otherEnum = weekPressStatus.getWeeklyPressStatusEnum();
            String thisName = getPressName();
            String otherName = weekPressStatus.getPressName();

            if (thisEnum == null && otherEnum == null) {
                return 0;
            } else if (thisEnum == null) {
                return 1;
            } else if (otherEnum == null) {
                return -1;
            }

            if (thisEnum.sortOrder == otherEnum.sortOrder && thisName != null && otherName != null) {
                return thisName.compareToIgnoreCase(otherName);
            }


            return thisEnum.sortOrder - otherEnum.sortOrder;
        }

        @Override
        public boolean equals(Object obj) {
            return super.equals(obj);
        }

        @Override
        public int hashCode() {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                return Objects.hash(serialNumber, pressName, pressType, weeklyPressStatusEnum, dateReady);
            }
            return 0;
        }

        public enum WeeklyPressStatusEnum {
            READY("READY", R.string.week_press_status_msg_ready, R.color.week_status_ready, 0),
            NOT_READY("NOT_READY", R.string.week_press_status_msg_not_ready, R.color.week_status_not_ready, 1),
            MISSING("MISSING", R.string.week_press_status_msg_missing, R.color.week_status_missing, 2);

            String status;
            int msgID;
            int colorID;
            int sortOrder;

            WeeklyPressStatusEnum(String status, int msgID, int colorID, int sortOrder) {
                this.status = status;
                this.msgID = msgID;
                this.colorID = colorID;
                this.sortOrder = sortOrder;
            }

            public static WeeklyPressStatusEnum from(String status) {
                if (status != null) {
                    for (WeeklyPressStatusEnum statusEnum : WeeklyPressStatusEnum.values()) {
                        if (statusEnum.status.equals(status)) {
                            return statusEnum;
                        }
                    }
                }
                return READY;
            }

            public static String getMsg(Context context, WeeklyPressStatusEnum weekPressStatus, Date date) {
                if (weekPressStatus == null) {
                    return context.getString(READY.msgID);
                }
                switch (weekPressStatus) {
                    case MISSING:
                        return context.getString(MISSING.msgID);
                    case NOT_READY:
                        return context.getString(NOT_READY.msgID, HPLocaleUtils.getTimeAgo(context, date, true));
                    default:
                        return context.getString(READY.msgID, HPLocaleUtils.getTimeAgo(context, date, true));
                }
            }

            public static Drawable getColorDrawable(Context context, WeeklyPressStatusEnum weekPressStatus) {
                GradientDrawable shape = new GradientDrawable();
                shape.setShape(GradientDrawable.RECTANGLE);
                shape.setCornerRadii(CORNER_RADII);
                shape.setColor(ResourcesCompat.getColor(context.getResources(), weekPressStatus.colorID, null));
                return shape;
            }
        }

        String serialNumber;
        String pressName;
        String pressType;
        WeeklyPressStatusEnum weeklyPressStatusEnum;
        Date dateReady;

        public String getSerialNumber() {
            return serialNumber;
        }

        public void setSerialNumber(String serialNumber) {
            this.serialNumber = serialNumber;
        }

        public String getPressName() {
            return pressName;
        }

        public void setPressName(String pressName) {
            this.pressName = pressName;
        }

        public String getPressType() {
            return pressType;
        }

        public void setPressType(String pressType) {
            this.pressType = pressType;
        }

        public WeeklyPressStatusEnum getWeeklyPressStatusEnum() {
            return weeklyPressStatusEnum;
        }

        public void setWeeklyPressStatusEnum(WeeklyPressStatusEnum weeklyPressStatusEnum) {
            this.weeklyPressStatusEnum = weeklyPressStatusEnum;
        }

        public Date getDateReady() {
            return dateReady;
        }

        public void setDateReady(Date dateReady) {
            this.dateReady = dateReady;
        }
    }
}