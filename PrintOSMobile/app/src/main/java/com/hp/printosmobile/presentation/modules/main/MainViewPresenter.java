package com.hp.printosmobile.presentation.modules.main;

import android.content.Context;

import com.hp.printosmobile.Analytics;
import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.data.remote.models.OrganizationUsersData;
import com.hp.printosmobile.data.remote.services.OrganizationService;
import com.hp.printosmobile.presentation.PermissionsManager;
import com.hp.printosmobile.presentation.Presenter;
import com.hp.printosmobile.presentation.indigowidget.PrintOSAppWidgetProvider;
import com.hp.printosmobile.presentation.modules.beatcoins.BeatCoinsViewModel;
import com.hp.printosmobile.presentation.modules.drawer.NavigationPresenter;
import com.hp.printosmobile.presentation.modules.home.HomeDataManager;
import com.hp.printosmobile.presentation.modules.invites.InviteUtils;
import com.hp.printosmobile.presentation.modules.shared.UserViewModel;
import com.hp.printosmobile.utils.ImageLoadingUtils;
import com.hp.printosmobilelib.core.communications.remote.APIException;
import com.hp.printosmobilelib.core.communications.remote.models.UserData;
import com.hp.printosmobilelib.core.logging.HPLogger;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import retrofit2.Response;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.functions.Func2;
import rx.schedulers.Schedulers;

/**
 * Created by Osama Taha on 5/23/16.
 */
public class MainViewPresenter extends Presenter<MainView> {

    private static final String TAG = MainViewPresenter.class.getName();

    private Subscription mSubscription;

    public void initView(final Context context) {
        initView(context, null);
    }

    public void initView(final Context context, final String targetDivision) {
        HPLogger.d(TAG, "initView");
        if (mView != null) {
            mView.showLoading();
        }

        PrintOSPreferences.getInstance(context).setLatexOnly(false);

        final boolean isHPOrg = PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).isHPOrg();
        final boolean isChannelOrg = PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).isChannelOrg();

        mSubscription = HomeDataManager.initBusinessUnits(context)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Action1<Map<BusinessUnitEnum, BusinessUnitViewModel>>() {

                    @Override
                    public void call(Map<BusinessUnitEnum, BusinessUnitViewModel> businessUnitViewModelsMap) {

                        if (businessUnitViewModelsMap != null && businessUnitViewModelsMap.size() > 0) {

                            mView.hideLoading();

                            if (businessUnitViewModelsMap.isEmpty()) {
                                mView.onEmptyBusinessUnits();
                                if (targetDivision != null && mView != null && !isHPOrg && !isChannelOrg) {
                                    mView.onTargetDivisionNotFound();
                                }
                            } else {
                                PrintOSPreferences.getInstance(context).setLatexOnly(
                                        businessUnitViewModelsMap.keySet().size() == 1 &&
                                                businessUnitViewModelsMap.keySet().contains(
                                                        BusinessUnitEnum.LATEX_PRINTER)
                                );

                                mView.onGettingBusinessUnitsCompleted(businessUnitViewModelsMap);
                                BusinessUnitEnum businessUnitEnum = BusinessUnitEnum.from(targetDivision);
                                if (mView != null && targetDivision != null && !isHPOrg && !isChannelOrg &&
                                        !businessUnitViewModelsMap.containsKey(businessUnitEnum)) {
                                    mView.onTargetDivisionNotFound();
                                }
                                if (businessUnitViewModelsMap.containsKey(
                                        BusinessUnitEnum.INDIGO_PRESS)) {
                                    PrintOSPreferences.getInstance(context).setHasIndigoDivision(true);
                                }
                            }

                        } else {

                            PrintOSPreferences.getInstance(context).setHasIndigoDivision(false);

                            mView.hideLoading();
                            mView.onNoDevicesAttached();

                            if (targetDivision != null && mView != null && !isHPOrg && !isChannelOrg) {
                                mView.onTargetDivisionNotFound();
                            }
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if (throwable instanceof APIException && ((APIException) throwable).getKind() == APIException.Kind.UNAUTHORIZED) {
                            throwable = APIException.create(APIException.Kind.UNEXPECTED);
                        }
                        loadError(throwable);
                        if (targetDivision != null && mView != null) {
                            mView.onTargetDivisionNotFound();
                        }
                    }
                });
        addSubscriber(mSubscription);

    }

    public void getBeatCoinData(String id) {

        HPLogger.d(TAG, "getBeatCoinData(" + id + ")");
        mSubscription = HomeDataManager.getBeatCoins(id)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<BeatCoinsViewModel>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        HPLogger.d(TAG, "getBeatCoinData: fail " + e.getMessage());
                        if (mView != null) {
                            mView.onBeatCoinDataRetrieved(null, false);
                        }
                    }

                    @Override
                    public void onNext(BeatCoinsViewModel viewModel) {
                        HPLogger.d(TAG, "getBeatCoinData: success");
                        if (mView != null) {
                            mView.onBeatCoinDataRetrieved(viewModel, false);
                        }
                    }
                });

        addSubscriber(mSubscription);

    }

    public void checkInviteBeatCoin() {
        mSubscription = HomeDataManager.getInviteBeatCoin()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<BeatCoinsViewModel>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        HPLogger.d(TAG, "getBeatCoinData: fail " + e.getMessage());
                        if (mView != null) {
                            mView.onBeatCoinDataRetrieved(null, true);
                        }
                    }

                    @Override
                    public void onNext(BeatCoinsViewModel viewModel) {
                        HPLogger.d(TAG, "getBeatCoinData: success");
                        if (mView != null) {
                            mView.onBeatCoinDataRetrieved(viewModel, true);
                        }
                    }
                });

        addSubscriber(mSubscription);
    }

    private void loadError(Throwable throwable) {
        APIException exception = APIException.create(APIException.Kind.UNEXPECTED);
        if (throwable instanceof APIException) {
            exception = (APIException) throwable;
        }

        if (mView != null) {
            mView.hideLoading();
            mView.onError(exception, MainActivity.TAG);
        }
    }

    public void logout(Context context) {

        UserViewModel viewModel = PrintOSPreferences.getInstance(context).getUserInfo();
        if (viewModel != null) {
            Analytics.sendEvent(Analytics.LOGIN_EVENT_LOGOUT_ACTION,
                    viewModel.getUserId() + "|" + viewModel.getUserOrganization().getOrganizationId());
        } else {
            Analytics.sendEvent(Analytics.LOGIN_EVENT_LOGOUT_ACTION);
        }

        PrintOSPreferences.getInstance(context).onLogoutCompleted();
        PrintOSApplication.initSDKs();
        PrintOSAppWidgetProvider.updateAllWidgets(context);
        NavigationPresenter.reset();
        PermissionsManager.getInstance().clear();

        //Clear memory cache.
        InviteUtils.reset();
        ImageLoadingUtils.clearCache();
        HomeDataManager.clearApiCache();

        if (mView != null) {
            mView.showLoading();

            mView.hideLoading();
            mView.onApplicationLogout();
        }

    }

    public void getUserData() {


        mSubscription = HomeDataManager.getUserData()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<UserData.User>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(UserData.User user) {
                        if (user != null) {
                            PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).saveUserInfo(user);
                            if (mView != null) {
                                mView.onUserEmailRetrieved(user.getPrimaryEmail());
                            }

                        }
                    }
                });

        addSubscriber(mSubscription);
    }

    public static void getOrganizationMembers(final Context context) {

        final OrganizationService organizationService = PrintOSApplication.getApiServicesProvider().getOrganizationService();

        final int page = 1000;
        final int limit = page;

        Observable
                .range(0, Integer.MAX_VALUE - 1)
                .concatMap(new Func1<Integer, Observable<Response<OrganizationUsersData>>>() {
                    @Override
                    public Observable<Response<OrganizationUsersData>> call(Integer integer) {
                        return organizationService.getOrganizationUsers(limit, integer * page);
                    }
                }).map(new Func1<Response<OrganizationUsersData>, List<OrganizationUsersData.OrganizationUser>>() {
            @Override
            public List<OrganizationUsersData.OrganizationUser> call(Response<OrganizationUsersData> organizationUsersDataResponse) {
                return organizationUsersDataResponse.body().getUsers();
            }
        })
                .takeWhile(new Func1<List<OrganizationUsersData.OrganizationUser>, Boolean>() {
                    @Override
                    public Boolean call(List<OrganizationUsersData.OrganizationUser> results) {
                        return !(results == null || results.isEmpty());
                    }
                })
                .scan(new Func2<List<OrganizationUsersData.OrganizationUser>, List<OrganizationUsersData.OrganizationUser>, List<OrganizationUsersData.OrganizationUser>>() {
                    @Override
                    public List<OrganizationUsersData.OrganizationUser> call(List<OrganizationUsersData.OrganizationUser> results, List<OrganizationUsersData.OrganizationUser> results2) {
                        List<OrganizationUsersData.OrganizationUser> list = new ArrayList<>();
                        list.addAll(results);
                        list.addAll(results2);
                        return list;
                    }
                })
                .last()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<List<OrganizationUsersData.OrganizationUser>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onNext(List<OrganizationUsersData.OrganizationUser> organizationUsers) {
                        if (organizationUsers != null) {
                            readOrganizationMembersList(context, organizationUsers);
                        }
                    }
                });

    }

    private static void readOrganizationMembersList(final Context context, List<OrganizationUsersData.OrganizationUser> organizationUsersData) {


        UserViewModel userViewModel = PrintOSPreferences.getInstance(context).getUserInfo();
        String userId = userViewModel.getUserId();

        boolean memberInOrg = false;

        for (OrganizationUsersData.OrganizationUser user : organizationUsersData) {

            if (userId.equals(user.getUserId())) {
                memberInOrg = true;
                break;
            }

        }

        PrintOSPreferences.getInstance(context).setCurrentUserMemberInOrganization(memberInOrg);

    }

    @Override
    public void detachView() {
        stopSubscribers();
        mView = null;
    }
}
