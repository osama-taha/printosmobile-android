package com.hp.printosmobile.data.remote.models;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Anwar Asbah 9/7/2017
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MergedTodayAndLastWeekData {

    @JsonProperty("mergedTodayAndLastWeek")
    List<HourEntryData> hourEntryDatas;
    @JsonProperty("utcStartTime")
    String startDate;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<>();

    @JsonProperty("mergedTodayAndLastWeek")
    public List<HourEntryData> getHourEntryDatas() {
        return hourEntryDatas;
    }

    @JsonProperty("mergedTodayAndLastWeek")
    public void setHourEntryDatas(List<HourEntryData> hourEntryDatas) {
        this.hourEntryDatas = hourEntryDatas;
    }

    @JsonProperty("utcStartTime")
    public String getStartDate() {
        return startDate;
    }

    @JsonProperty("utcStartTime")
    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class HourEntryData {

        @JsonProperty("todayTotalValuePerHour")
        Value todayValue;
        @JsonProperty("lastWeekTotalValuePerHour")
        Value LastWeekValue;
        @JsonIgnore
        private Map<String, Object> additionalProperties = new HashMap<>();

        @JsonProperty("todayTotalValuePerHour")
        public Value getTodayValue() {
            return todayValue;
        }

        @JsonProperty("todayTotalValuePerHour")
        public void setTodayValue(Value todayValue) {
            this.todayValue = todayValue;
        }

        @JsonProperty("lastWeekTotalValuePerHour")
        public Value getLastWeekValue() {
            return LastWeekValue;
        }

        @JsonProperty("lastWeekTotalValuePerHour")
        public void setLastWeekValue(Value lastWeekValue) {
            LastWeekValue = lastWeekValue;
        }

        @JsonAnyGetter
        public Map<String, Object> getAdditionalProperties() {
            return this.additionalProperties;
        }

        @JsonAnySetter
        public void setAdditionalProperty(String name, Object value) {
            this.additionalProperties.put(name, value);
        }

        @JsonInclude(JsonInclude.Include.NON_NULL)
        public static class Value {

            @JsonProperty("value")
            Integer value;
            @JsonIgnore
            private Map<String, Object> additionalProperties = new HashMap<>();

            @JsonProperty("value")
            public Integer getValue() {
                return value;
            }

            @JsonProperty("value")
            public void setValue(Integer value) {
                this.value = value;
            }

            @JsonAnyGetter
            public Map<String, Object> getAdditionalProperties() {
                return this.additionalProperties;
            }

            @JsonAnySetter
            public void setAdditionalProperty(String name, Object value) {
                this.additionalProperties.put(name, value);
            }
        }
    }
}