package com.hp.printosmobile.data.remote.models;

import java.io.Serializable;

/**
 * Created by Osama Taha on 7/24/16.
 */
public class VersionUpdateResult implements Serializable {

    private MessageType messageType;
    private String updateMessage;
    private String linkInPlayStore;

    public enum MessageType {
        FORCE_UPDATE, WARNING, OK
    }

    public MessageType getMessageType() {
        return messageType;
    }

    public void setMessageType(MessageType messageType) {
        this.messageType = messageType;
    }

    public String getUpdateMessage() {
        return updateMessage;
    }

    public void setUpdateMessage(String updateMessage) {
        this.updateMessage = updateMessage;
    }

    public String getLinkInPlayStore() {
        return linkInPlayStore;
    }

    public void setLinkInPlayStore(String linkInPlayStore) {
        this.linkInPlayStore = linkInPlayStore;
    }

    @Override
    public String toString() {
        return "VersionUpdateResult{" +
                "messageType=" + messageType +
                ", updateMessage='" + updateMessage + '\'' +
                ", linkInPlayStore='" + linkInPlayStore + '\'' +
                '}';
    }
}
