package com.hp.printosmobile.utils;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;

import com.hp.printosmobile.Constants;
import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;
import com.hp.printosmobilelib.core.logging.HPLogger;
import com.hp.printosmobilelib.ui.widgets.panel.SharableObject;

/**
 * Created by Anwar Asbah on 1/25/2018.
 */

public class ShareUtils {

    private static final String TAG = ShareUtils.class.getSimpleName();

    private ShareUtils() {
    }

    public static void onScreenshotCreated(Activity context, Uri screenshotUri, String title, String body,
                                           SharableObject sharableObject) {

        if (sharableObject != null) {
            AnalyticsUtils.sendShareEvents(context, sharableObject.getShareAction());
        }

        Intent intent;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            intent = new Intent(Intent.ACTION_SEND);
            intent.addFlags(Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION);
        } else {
            intent = new Intent(Intent.ACTION_SEND);
        }

        if (screenshotUri == null) {
            screenshotUri = getDefaultShareIcon(context);
        }

        intent.putExtra(android.content.Intent.EXTRA_SUBJECT, title == null ? "" : title);
        intent.putExtra(android.content.Intent.EXTRA_TEXT, body == null ? "" : body);
        intent.putExtra(Intent.EXTRA_STREAM, screenshotUri);
        intent.setType("*/*");
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        try {
            context.startActivityForResult(Intent.createChooser(intent, PrintOSApplication.getAppContext().getString(R.string.share_with)), Constants.IntentExtras.APPS_PICKER_INTENT_REQUEST_CODE);
        } catch (ActivityNotFoundException e) {
            HPUIUtils.displayToast(PrintOSApplication.getAppContext(), PrintOSApplication.getAppContext().getString(R.string.no_app_for_sharing));
        }
    }

    private static Uri getDefaultShareIcon(Activity context) {

        Uri imageUri = null;
        try {
            imageUri = Uri.parse(MediaStore.Images.Media.insertImage(context.getContentResolver(),
                    BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_launcher),
                    null, null));
        } catch (Exception e) {
            HPLogger.d(TAG, "failed to get default share image. " + e);
        }

        return imageUri;

    }

}
