package com.hp.printosmobile.presentation.modules.shared;

import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Osama Taha on 3/19/18.
 */
public abstract class PrintOSBaseAdapter<T> extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    protected static final int HEADER = 0;
    protected static final int FOOTER = 1;
    private static final long UPDATE_FOOTER_DELAY = 1000L;

    protected List<T> items;
    protected OnItemClickListener onItemClickListener;
    protected OnReloadClickListener onReloadClickListener;
    protected boolean isFooterAdded = false;
    protected boolean isHeaderAdded = false;

    public PrintOSBaseAdapter() {
        items = new ArrayList<>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        RecyclerView.ViewHolder viewHolder;

        switch (viewType) {
            case HEADER:
                viewHolder = createHeaderViewHolder(parent);
                break;
            case FOOTER:
                viewHolder = createFooterViewHolder(parent);
                break;
            default:
                viewHolder = createItemViewHolder(parent, viewType);
                break;
        }

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        switch (getItemViewType(position)) {
            case HEADER:
                bindHeaderViewHolder(viewHolder);
                break;
            case FOOTER:
                bindFooterViewHolder(viewHolder);
                break;
            default:
                bindItemViewHolder(viewHolder, position);
                break;
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    protected abstract RecyclerView.ViewHolder createHeaderViewHolder(ViewGroup parent);

    protected abstract RecyclerView.ViewHolder createItemViewHolder(ViewGroup parent, int viewType);

    protected abstract RecyclerView.ViewHolder createFooterViewHolder(ViewGroup parent);

    protected abstract void bindHeaderViewHolder(RecyclerView.ViewHolder viewHolder);

    protected abstract void showLoadMoreHeader();

    protected abstract void showErrorHeader();

    protected abstract void showNoResultsHeader();

    protected abstract void bindItemViewHolder(RecyclerView.ViewHolder viewHolder, int position);

    protected abstract void bindFooterViewHolder(RecyclerView.ViewHolder viewHolder);

    protected abstract void showLoadMoreFooter();

    protected abstract void showErrorFooter();

    protected abstract void showNoResultsFooter();

    public abstract void addFooterView();
    public abstract void addHeaderView();

    public T getItem(int position) {
        return items.get(position);
    }

    public void insert(T item, int index){
        items.add(index,item);
        notifyItemInserted(index);
    }
    public void add(T item) {
        items.add(item);
        notifyItemInserted(items.size() - 1);
    }

    public void addAll(List<T> items) {
        for (T item : items) {
            add(item);
        }
    }

    public void remove(T item) {
        int position = items.indexOf(item);
        if (position > -1) {
            items.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void clearItems() {
        isFooterAdded = false;
        while (getItemCount() > 0) {
            remove(getItem(0));
        }
    }

    public boolean isListEmpty() {
        return getItemCount() == 0;
    }

    public boolean isLastPosition(int position) {
        return (position == items.size() - 1);
    }


    public boolean isFirstPosition(int position) {
        return (position == 0);
    }

    public void removeFooterView() {

        if (isFooterAdded){
            isFooterAdded = false;

            int position = items.size() - 1;
            T item = getItem(position);

            if (item != null) {
                items.remove(position);
                notifyItemRemoved(position);
            }

        }

    }

    public void removeHeaderView() {
        isHeaderAdded = false;

        int position = 0;
        T item = getItem(position);

        if (item != null) {
            items.remove(position);
            notifyItemRemoved(position);
        }
    }


    public void updateFooterView(final FooterTypeEnum footerTypeEnum) {

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                switch (footerTypeEnum) {
                    case LOAD_MORE:
                        showLoadMoreFooter();
                        break;
                    case ERROR:
                        showErrorFooter();
                        break;
                    case NO_RESULTS:
                        showNoResultsFooter();
                        break;
                    default:
                        break;
                }
            }
        }, UPDATE_FOOTER_DELAY);

    }


    public void updateHeaderView(FooterTypeEnum footerTypeEnum) {
        switch (footerTypeEnum) {
            case LOAD_MORE:
                showLoadMoreHeader();
                break;
            case ERROR:
                showErrorHeader();
                break;
            case NO_RESULTS:
                showNoResultsHeader();
                break;
            default:
                break;
        }
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public void setOnReloadClickListener(OnReloadClickListener onReloadClickListener) {
        this.onReloadClickListener = onReloadClickListener;
    }

    public enum FooterTypeEnum {
        LOAD_MORE,
        ERROR,
        NO_RESULTS
    }


    public interface OnItemClickListener {
        void onItemClick(int position, View view);
    }

    public interface OnReloadClickListener {
        void onReloadClick();
    }
}