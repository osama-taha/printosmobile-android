package com.hp.printosmobile.presentation.modules.npspopup;

import android.text.TextUtils;

import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.aaa.AAAManager;
import com.hp.printosmobile.data.remote.ApiServicesProvider;
import com.hp.printosmobile.data.remote.models.UserInfoData;
import com.hp.printosmobile.data.remote.services.UserDataService;
import com.hp.printosmobile.presentation.Presenter;
import com.hp.printosmobile.presentation.modules.filters.FiltersViewModel;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitEnum;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitViewModel;

import retrofit2.Response;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by Anwar Asbah on 9/5/2017.
 */
public class NPSPresenter extends Presenter<NPSView> {

    public void requestNPS(final BusinessUnitViewModel businessUnitViewModel, boolean isFromMenu,
                           boolean showDialog) {

        String endUserId = getEndUserId(businessUnitViewModel);

        if (TextUtils.isEmpty(endUserId)) {
            onRequestError(isFromMenu);
            return;
        }

        getUserInfo(endUserId, isFromMenu, showDialog);

    }

    private void getUserInfo(String endUserId, final boolean isFromMenu, final boolean showDialog) {
        Subscription subscription = getUserInfo(endUserId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<UserInfoData>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        onRequestError(isFromMenu);
                    }

                    @Override
                    public void onNext(UserInfoData userInfoData) {
                        if (mView != null && userInfoData != null && !userInfoData.hasNullValue()) {
                            mView.onNPSUserInfoRetrieved(userInfoData, showDialog);
                        }
                    }
                });
        addSubscriber(subscription);
    }

    public String getEndUserId(BusinessUnitViewModel businessUnitViewModel) {

        FiltersViewModel filtersViewModel = null;
        if (businessUnitViewModel != null && businessUnitViewModel.getBusinessUnit() == BusinessUnitEnum.INDIGO_PRESS) {
            filtersViewModel = businessUnitViewModel.getFiltersViewModel();
        }

        String endUserId = null;
        if (filtersViewModel != null) {
            endUserId = filtersViewModel.getEndUserIdFromCurrentSite();
        }

        return endUserId;

    }


    public static Observable<UserInfoData> getUserInfo(String id) {
        ApiServicesProvider serviceProvider = PrintOSApplication.getApiServicesProvider();
        UserDataService userDataService = serviceProvider.getUserDataService();

        return userDataService.getUserInfo(id).map(new Func1<Response<UserInfoData>, UserInfoData>() {
            @Override
            public UserInfoData call(Response<UserInfoData> userInfoDataResponse) {
                if (userInfoDataResponse != null && userInfoDataResponse.isSuccessful()) {
                    return userInfoDataResponse.body();
                }
                return null;
            }
        });
    }

    private void onRequestError(boolean isFromMenu) {
        if (mView != null) {
            mView.onNPSDataRetrievalFailure(isFromMenu);
        }
    }

    public void refresh() {
        stopSubscribers();
    }

    @Override
    public void detachView() {
        stopSubscribers();
        mView = null;
    }

}