package com.hp.printosmobile.data.remote.services;

import com.hp.printosmobile.data.remote.ApiConstants;
import com.hp.printosmobile.data.remote.models.CurrentOrganizationData;
import com.hp.printosmobile.data.remote.models.OrganizationBody;
import com.hp.printosmobile.data.remote.models.OrganizationUsersData;
import com.hp.printosmobilelib.core.communications.remote.models.OrganizationJsonBody;
import com.hp.printosmobilelib.core.communications.remote.models.UserData;

import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.PUT;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Retrofit REST interface definition for Organization Endpoints
 *
 * @author Anwar Asbah
 */
public interface OrganizationService {

    /**
     * Returns list of accounts.
     */
    @GET(ApiConstants.GET_ORGANIZATION_URL)
    Observable<Response<OrganizationBody>> getOrganizations();

    /**
     * change context.
     */
    @PUT(ApiConstants.CHANGE_ORGANIZATION_CONTEXT_URL)
    Observable<Response<UserData>> changeOrganizationContext(@Body OrganizationJsonBody organization);

    /**
     * get organization data
     */
    @GET(ApiConstants.ORGANIZATION_DATA_API)
    Observable<Response<CurrentOrganizationData>> getOrganizationData();

    /**
     * get organization members
     */
    @GET(ApiConstants.ORGANIZATION_MEMBERS_API)
    Observable<Response<OrganizationUsersData>> getOrganizationUsers(@Query("limit") int limit, @Query("offset") int offset);

}
