package com.hp.printosmobile.data.remote.models;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class NotificationSubscription {

    @JsonProperty("userSubscriptionList")
    private List<Subscription> subscriptions;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<>();


    @JsonProperty("userSubscriptionList")
    public List<Subscription> getSubscriptions() {
        return subscriptions;
    }

    @JsonProperty("userSubscriptionList")
    public void setSubscriptions(List<Subscription> subscriptions) {
        this.subscriptions = subscriptions;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public static class Subscription {
        @JsonProperty("eventDescriptionId")
        private String eventDescriptionId;
        @JsonProperty("userSubscriptionId")
        private String userSubscriptionId;
        @JsonProperty("browser")
        private boolean browser;
        @JsonProperty("email")
        private boolean email;
        @JsonProperty("mobile")
        private boolean mobile;
        @JsonIgnore
        private Map<String, Object> additionalProperties = new HashMap<>();

        @JsonProperty("eventDescriptionId")
        public String getEventDescriptionId() {
            return eventDescriptionId;
        }

        @JsonProperty("eventDescriptionId")
        public void setEventDescriptionId(String eventDescriptionId) {
            this.eventDescriptionId = eventDescriptionId;
        }

        @JsonProperty("userSubscriptionId")
        public String getUserSubscriptionId() {
            return userSubscriptionId;
        }

        @JsonProperty("userSubscriptionId")
        public void setUserSubscriptionId(String userSubscriptionId) {
            this.userSubscriptionId = userSubscriptionId;
        }

        @JsonProperty("browser")
        public boolean isBrowser() {
            return browser;
        }

        @JsonProperty("browser")
        public void setBrowser(boolean browser) {
            this.browser = browser;
        }

        @JsonProperty("email")
        public boolean isEmail() {
            return email;
        }

        @JsonProperty("email")
        public void setEmail(boolean email) {
            this.email = email;
        }

        @JsonProperty("mobile")
        public boolean isMobile() {
            return mobile;
        }

        @JsonProperty("mobile")
        public void setMobile(boolean mobile) {
            this.mobile = mobile;
        }

        @JsonAnyGetter
        public Map<String, Object> getAdditionalProperties() {
            return this.additionalProperties;
        }

        @JsonAnySetter
        public void setAdditionalProperty(String name, Object value) {
            this.additionalProperties.put(name, value);
        }
    }

}
