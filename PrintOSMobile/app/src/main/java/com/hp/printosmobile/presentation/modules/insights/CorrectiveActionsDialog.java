package com.hp.printosmobile.presentation.modules.insights;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.hp.printosmobile.Analytics;
import com.hp.printosmobile.AnswersSdk;
import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;
import com.hp.printosmobile.data.remote.models.InsightData;
import com.hp.printosmobile.presentation.modules.main.MainActivity;
import com.hp.printosmobile.utils.FileUtils;
import com.hp.printosmobile.utils.HPLocaleUtils;
import com.hp.printosmobile.utils.HPUIUtils;
import com.hp.printosmobilelib.core.logging.HPLogger;
import com.hp.printosmobilelib.ui.widgets.panel.SharableObject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Osama Taha on 1/23/2018.
 */
public class CorrectiveActionsDialog extends DialogFragment implements SharableObject {

    private static final String TAG = CorrectiveActionsDialog.class.getName();

    private static final String INSIGHT_MODEL_ARG = "INSIGHT_MODEL_ARG";
    private static final String CORRECTIVE_ACTION_TEXT_FORMAT = "* %s - %s";
    private static final String CORRECTIVE_ACTION_SCREEN_SHOT_FILE_NAME = "corrective_actions_screenshot";

    @Bind(R.id.dialog_layout)
    View dialogLayout;
    @Bind(R.id.content_layout)
    View contentLayout;
    @Bind(R.id.corrective_actions_list)
    RecyclerView correctiveActionsList;
    @Bind(R.id.event_name_text_view)
    TextView eventNameTextView;
    @Bind(R.id.event_description_text_view)
    TextView eventDescriptionTextView;
    @Bind(R.id.kpi_image_view)
    ImageView kpiImageView;
    @Bind(R.id.share_button)
    ImageView shareButton;
    @Bind(R.id.close_button)
    ImageView closeButton;
    @Bind(R.id.top_layout)
    View screenShotView;

    CorrectiveActionsAdapter correctiveActionsAdapter;
    InsightsViewModel.InsightModel insightModel;
    CorrectiveActionsDialogCallback callback;

    public static CorrectiveActionsDialog newInstance(InsightsViewModel.InsightModel insightModel, CorrectiveActionsDialogCallback callback) {
        CorrectiveActionsDialog dialog = new CorrectiveActionsDialog();
        dialog.callback = callback;
        Bundle bundle = new Bundle();
        bundle.putSerializable(INSIGHT_MODEL_ARG, insightModel);
        dialog.setArguments(bundle);
        return dialog;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Analytics.sendEvent(Analytics.SCREEN_CORRECTIVE_ACTIONS_POPUP);

        Bundle bundle = getArguments();
        if (bundle != null && bundle.containsKey(INSIGHT_MODEL_ARG)) {
            insightModel = (InsightsViewModel.InsightModel) bundle.getSerializable(INSIGHT_MODEL_ARG);
            if (insightModel.getKpi() == InsightsViewModel.InsightKpiEnum.JAM) {
                Analytics.sendEvent(Analytics.EVENT_CORRECTIVE_ACTIONS_JAMS_SHOWN);
            } else {
                Analytics.sendEvent(Analytics.EVENT_CORRECTIVE_ACTIONS_FAILURES_SHOWN);
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.dialog_corrective_actions, container, false);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        init();
    }

    private void init() {

        Context context = PrintOSApplication.getAppContext();

        shareButton.setColorFilter(ContextCompat.getColor(context, android.R.color.white));

        eventNameTextView.setText(insightModel.getLabel());
        kpiImageView.setImageResource(insightModel.getKpi().getCorrectiveActionDrawableId());

        int eventDescriptionTextResId = insightModel.getKpi() == InsightsViewModel.InsightKpiEnum.JAM ? R.string.corrective_actions_event_summary_jam : R.string.corrective_actions_event_summary_failure;
        String dateRangeString = HPLocaleUtils.getDateRangeString(context, insightModel.getDateFrom(), insightModel.getDateTo());
        String percentageString = HPLocaleUtils.getPercentageString(context, insightModel.getPercentage());
        String descriptionString = context.getString(eventDescriptionTextResId, percentageString, dateRangeString);

        int start = descriptionString.indexOf(percentageString);
        int end = start + percentageString.length();
        SpannableString descriptionSpannable = new SpannableString(descriptionString);
        descriptionSpannable.setSpan(new ForegroundColorSpan(ContextCompat.getColor(context, R.color.hp_blue)), start, end, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        eventDescriptionTextView.setText(descriptionSpannable);


        correctiveActionsAdapter = new CorrectiveActionsAdapter();
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        correctiveActionsList.setLayoutManager(layoutManager);
        correctiveActionsList.setAdapter(correctiveActionsAdapter);

        contentLayout.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                contentLayout.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                contentLayout.setVisibility(View.VISIBLE);
                contentLayout.setTranslationY(contentLayout.getMeasuredHeight());
                contentLayout.animate().translationY(0);
            }
        });


        dialogLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
    }

    @OnClick(R.id.close_button)
    public void close() {
        dismiss();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (callback != null) {
            callback.onDialogDismiss();
        }
    }

    @OnClick(R.id.share_button)
    public void share() {
        HPLogger.d(TAG, "onShare Clicked");
        if (callback != null && correctiveActionsAdapter != null && correctiveActionsAdapter.getItemCount() > 0) {

            lockShareButton(true);
            if (getActivity() instanceof MainActivity) {
                ((MainActivity) getActivity()).share(CorrectiveActionsDialog.this);
            }

        }
    }

    public void performSharing() {

        if (callback == null) {
            return;
        }

        HPUIUtils.setVisibility(false, closeButton, shareButton);
        Bitmap topPartBitmap = FileUtils.getScreenShot(screenShotView);
        Bitmap correctiveActionsBitmap = FileUtils.getScreenshotFromRecyclerView(correctiveActionsList);
        HPUIUtils.setVisibility(true, closeButton, shareButton);

        if (topPartBitmap == null || correctiveActionsBitmap == null) {
            return;
        }

        Paint paint = new Paint();
        int height = 0;

        Bitmap bigBitmap = Bitmap.createBitmap(correctiveActionsList.getMeasuredWidth(),
                topPartBitmap.getHeight() + correctiveActionsBitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas bigCanvas = new Canvas(bigBitmap);
        bigCanvas.drawColor(Color.WHITE);

        bigCanvas.drawBitmap(topPartBitmap, 0f, height, paint);
        height += topPartBitmap.getHeight();
        topPartBitmap.recycle();

        bigCanvas.drawBitmap(correctiveActionsBitmap, 0f, height, paint);
        correctiveActionsBitmap.recycle();

        Uri imageUri = FileUtils.store(getContext(), bigBitmap, CORRECTIVE_ACTION_SCREEN_SHOT_FILE_NAME);

        callback.onScreenshotCreated(insightModel, imageUri, this);
        lockShareButton(false);
    }

    public void lockShareButton(boolean lock) {
        shareButton.setEnabled(!lock);
        shareButton.setClickable(!lock);
    }

    @Override
    public String getShareAction() {
        boolean isJam = insightModel.getKpi() == InsightsViewModel.InsightKpiEnum.JAM;
        return isJam ? AnswersSdk.SHARE_CORRECTIVE_ACTIONS_JAMS : AnswersSdk.SHARE_CORRECTIVE_ACTIONS_FAILURES;
    }

    public class CorrectiveActionsAdapter extends RecyclerView.Adapter<CorrectiveActionsAdapter.CorrectiveActionViewHolder> {

        @Override
        public CorrectiveActionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            ViewGroup deviceView = (ViewGroup) inflater.inflate(R.layout.corrective_action_item, parent, false);
            return new CorrectiveActionViewHolder(deviceView);
        }

        @Override
        public void onBindViewHolder(CorrectiveActionViewHolder holder, int position) {

            InsightData.Insight.Hint hint = insightModel.getCorrectiveActions().get(position);

            //Create a string from html string.
            Spanned correctiveAction;
            String text = String.format(CORRECTIVE_ACTION_TEXT_FORMAT, hint.getName(), hint.getCorrectiveAction());
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                correctiveAction = Html.fromHtml(text, Html.FROM_HTML_MODE_LEGACY);
            } else {
                correctiveAction = Html.fromHtml(text);
            }

            //Making the hint name big and bold.
            int start = 0;
            int end = start + text.indexOf("-");
            SpannableString hintSpannable = new SpannableString(correctiveAction);
            hintSpannable.setSpan(new RelativeSizeSpan(1.1f), start, end, 0);
            hintSpannable.setSpan(new StyleSpan(Typeface.BOLD), start, end, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
            holder.correctiveActionText.setText(hintSpannable);
        }

        @Override
        public int getItemCount() {
            return insightModel.getCorrectiveActions() == null ? 0 : insightModel.getCorrectiveActions().size();
        }

        public class CorrectiveActionViewHolder extends RecyclerView.ViewHolder {

            @Bind(R.id.corrective_action_text)
            TextView correctiveActionText;

            CorrectiveActionViewHolder(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }
        }
    }

    public interface CorrectiveActionsDialogCallback {
        void onDialogDismiss();

        void onScreenshotCreated(InsightsViewModel.InsightModel panel, Uri storageUri, SharableObject sharableObject);
    }
}
