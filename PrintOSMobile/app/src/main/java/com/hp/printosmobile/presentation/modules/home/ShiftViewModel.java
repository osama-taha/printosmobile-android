package com.hp.printosmobile.presentation.modules.home;

import com.hp.printosmobile.R;

import java.io.Serializable;

/**
 * Created by Osama Taha on 10/5/16.
 */
public class ShiftViewModel implements Serializable {

    private String shiftId;
    private String shiftName;
    private ShiftType shiftType;
    private int startDay;
    private String startTime;
    private int startHour;
    private int endDay;
    private int endHour;
    private int presentedDay;

    public String getShiftId() {
        return shiftId;
    }

    public void setShiftId(String shiftId) {
        this.shiftId = shiftId;
    }

    public String getShiftName() {
        return shiftName;
    }

    public void setShiftName(String shiftName) {
        this.shiftName = shiftName;
    }

    public ShiftType getShiftType() {
        return shiftType;
    }

    public void setShiftType(ShiftType shiftType) {
        this.shiftType = shiftType;
    }

    public int getStartDay() {
        return startDay;
    }

    public void setStartDay(int startDay) {
        this.startDay = startDay;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public int getStartHour() {
        return startHour;
    }

    public void setStartHour(int startHour) {
        this.startHour = startHour;
    }

    public int getEndDay() {
        return endDay;
    }

    public void setEndDay(int endDay) {
        this.endDay = endDay;
    }

    public int getEndHour() {
        return endHour;
    }

    public void setEndHour(int endHour) {
        this.endHour = endHour;
    }

    public int getPresentedDay() {
        return presentedDay;
    }

    public void setPresentedDay(int presentedDay) {
        this.presentedDay = presentedDay;
    }

    public enum ShiftType {

        TODAY("DAY_DEFAULT_SHIFT", R.string.day_default_shift_display_name), CURRENT("SHIFTS", R.string.shift_display_name);

        private final String shiftType;
        private final int displayNameResource;

        ShiftType(String shiftType, int displayNameResource) {
            this.shiftType = shiftType;
            this.displayNameResource = displayNameResource;
        }

        public String getShiftType() {
            return shiftType;
        }

        public int getDisplayNameResource() {
            return displayNameResource;
        }

        public static ShiftType fromString(String type) {

            for (ShiftType shiftType : ShiftType.values()) {
                if (shiftType.getShiftType().equals(type)) {
                    return shiftType;
                }
            }

            return ShiftType.CURRENT;
        }
    }

    @Override
    public String toString() {
        return "ShiftViewModel{" +
                "shiftId='" + shiftId + '\'' +
                ", shiftName='" + shiftName + '\'' +
                ", shiftType=" + shiftType +
                ", startDay=" + startDay +
                ", startTime='" + startTime + '\'' +
                ", startHour=" + startHour +
                ", endDay=" + endDay +
                ", endHour=" + endHour +
                ", presentedDay=" + presentedDay +
                '}';
    }
}
