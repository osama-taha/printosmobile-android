package com.hp.printosmobile.data.remote.services;

import com.hp.printosmobile.data.remote.ApiConstants;
import com.hp.printosmobile.data.remote.models.BoxModel;

import retrofit2.Response;
import retrofit2.http.GET;
import rx.Observable;

/**
 * Created by Anwar Asbah 6/13/2016
 */
public interface BoxService {

    /**
     * Returns list of devices for Indigo.
     */
    @GET(ApiConstants.BOX_SERVICE)
    Observable<Response<BoxModel>> getBoxData();
}
