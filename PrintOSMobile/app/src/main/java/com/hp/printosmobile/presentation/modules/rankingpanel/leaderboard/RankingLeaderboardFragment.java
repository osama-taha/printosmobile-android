package com.hp.printosmobile.presentation.modules.rankingpanel.leaderboard;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.hp.printosmobile.Analytics;
import com.hp.printosmobile.Constants;
import com.hp.printosmobile.R;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.data.remote.models.PermissionsData;
import com.hp.printosmobile.data.remote.models.RankingLeaderboardStatus;
import com.hp.printosmobile.presentation.BaseFragment;
import com.hp.printosmobile.presentation.PermissionsManager;
import com.hp.printosmobile.presentation.modules.coins.BeatCoinsPresenter;
import com.hp.printosmobile.presentation.modules.coins.BeatCoinsView;
import com.hp.printosmobile.presentation.modules.filters.SiteViewModel;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitEnum;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitViewModel;
import com.hp.printosmobile.presentation.modules.main.IMainFragment;
import com.hp.printosmobile.presentation.modules.week.RankingViewModel;
import com.hp.printosmobile.utils.AppUtils;
import com.hp.printosmobile.utils.HPUIUtils;
import com.hp.printosmobilelib.core.communications.remote.APIException;
import com.hp.printosmobilelib.ui.widgets.HPTabLayout;
import com.hp.printosmobilelib.ui.widgets.HPTextView;
import com.hp.printosmobilelib.ui.widgets.HPViewPager;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.OnClick;
import retrofit2.Response;
import rx.Subscriber;

/**
 * Ranking leaderboard tab.
 * Created by Osama Taha
 */
public class RankingLeaderboardFragment extends BaseFragment implements IMainFragment, RankingLeaderboardView, BeatCoinsView, TabLayout.OnTabSelectedListener {

    @Bind(R.id.request_leaderboard_permission_layout)
    View requestLeaderboardPermissionLayout;
    @Bind(R.id.ranking_leaderboard_error_layout)
    View errorLayout;
    @Bind(R.id.ranking_leaderboard_error_message)
    TextView errorMessageTextView;
    @Bind(R.id.ranking_leaderboard_not_enabled_layout)
    View rankingLeaderboardNotEnabledLayout;
    @Bind(R.id.main_layout)
    View mainLayout;
    @Bind(R.id.ranking_tabs)
    HPTabLayout rankingTabLayout;
    @Bind(R.id.ranking_pager)
    HPViewPager rankingPager;
    @Bind(R.id.empty_view)
    TextView emptyView;
    @Bind(R.id.loading_indicator)
    View loadingIndicator;
    @Bind(R.id.ranking_beat_coins_title)
    TextView rankingBeatCoinsTitle;
    @Bind(R.id.ranking_leaderboard_request_permission)
    Button requestRankingLeaderboardButton;
    @Bind(R.id.request_permission_loading_progress_bar)
    ProgressBar requestPermissionLoadingProgressBar;

    @Bind(R.id.ranking_leaderboard_cell_container)
    View rankingCellContainer;
    @Bind(R.id.ranking_cell_position)
    TextView rankingTextView;
    @Bind(R.id.ranking_cell_site_name)
    TextView siteNameTextView;
    @Bind(R.id.ranking_cell_score)
    TextView scoreTextView;
    @Bind(R.id.ranking_cell_country_flag)
    ImageView flagImageView;
    @Bind(R.id.ranking_cell_incognito_icon)
    ImageView incognitoImageView;
    @Bind(R.id.ranking_cell_offset_layout)
    View trendLayout;
    @Bind(R.id.ranking_cell_trend_icon)
    ImageView trendIcon;
    @Bind(R.id.ranking_cell_offset)
    TextView trendOffset;
    @Bind(R.id.ranking_cell_trophy_icon)
    ImageView trophyImageView;

    @Bind(R.id.button_refresh)
    HPTextView refreshButton;

    private RankingLeaderboardFragmentCallbacks callbacks;
    private RankingLeaderboardPresenter presenter;
    private BeatCoinsPresenter beatCoinsPresenter;
    private RankingPagerAdapter adapter;
    private String focusableTab;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        HPUIUtils.setVisibility(false, rankingCellContainer);

        presenter = new RankingLeaderboardPresenter();
        presenter.attachView(this);

        beatCoinsPresenter = new BeatCoinsPresenter();
        beatCoinsPresenter.attachView(this);

    }

    private void initView() {

        RankingLeaderboardStatus rankingLeaderboardStatus = PermissionsManager.getInstance().getRankingLeaderboardStatus();
        boolean isRankingLeaderboardEnabled = rankingLeaderboardStatus == RankingLeaderboardStatus.ENABLED;

        HPUIUtils.setVisibility(false,emptyView, errorLayout, refreshButton);
        HPUIUtils.setVisibility(isRankingLeaderboardEnabled, mainLayout);
        HPUIUtils.setVisibility(!isRankingLeaderboardEnabled, rankingLeaderboardNotEnabledLayout);

        if (!isRankingLeaderboardEnabled) {

            if (rankingLeaderboardStatus == RankingLeaderboardStatus.REQUESTED) {

                showRankingLeaderboardPermissionRequestedLayout();

            } else {

                boolean hasUpdatePermission = PermissionsManager.getInstance().hasPermission(PermissionsManager.Permission.UPDATE);

                if (hasUpdatePermission) {

                    boolean beatCoinsClaimed = PrintOSPreferences.getInstance().
                            isBeatCoinsClaimed(BeatCoinsPresenter.BeatCoinsMode.RANKING_LEADERBOARD);

                    HPUIUtils.setVisibility(!beatCoinsClaimed, rankingBeatCoinsTitle);

                    requestRankingLeaderboardButton.setText(getActivity().getString(beatCoinsClaimed ?
                            R.string.ranking_leaderboard_agree : R.string.ranking_leaderboard_agree_and_collect_coins));


                } else {

                    errorMessageTextView.setText(getString(R.string.ranking_leaderboard_contact_admin));

                }

                HPUIUtils.setVisibility(hasUpdatePermission, requestLeaderboardPermissionLayout);
                HPUIUtils.setVisibility(!hasUpdatePermission, errorLayout);
            }

        } else {

            presenter.init(getContext());
        }
    }

    private void setMySiteDemoCell(RankingViewModel rankingViewModel) {

        rankingCellContainer.setBackgroundResource(R.drawable.ranking_leaderboard_cell_border);

        rankingTextView.setText("1"); //Dummy data
        scoreTextView.setText("100"); //Dummy score
        incognitoImageView.setVisibility(View.INVISIBLE);
        trophyImageView.setVisibility(View.VISIBLE);

        trendLayout.setBackgroundResource(RankingViewModel.Trend.UP.getBackgroundIconResId());
        trendOffset.setText("10"); // dummy offset and trend.
        trendIcon.setImageResource(RankingViewModel.Trend.UP.getDirectionIconResId());

        if (rankingViewModel != null) {

            RankingViewModel.SiteRankViewModel siteRankViewModel;
            if (rankingViewModel.hasSubRegionSection()) {
                siteRankViewModel = rankingViewModel.getSubRegionViewModel();
            } else if (rankingViewModel.hasRegionSection()) {
                siteRankViewModel = rankingViewModel.getRegionViewModel();
            } else {
                siteRankViewModel = rankingViewModel.getWorldWideViewModel();
            }

            if (siteRankViewModel != null) {

                if (!TextUtils.isEmpty(siteRankViewModel.getCountryFlagUrl())) {
                    Picasso.with(getActivity())
                            .load(siteRankViewModel.getCountryFlagUrl())
                            .fit().centerCrop()
                            .into(flagImageView);
                }
            } else {
                flagImageView.setVisibility(View.INVISIBLE);
            }
        } else {
            flagImageView.setVisibility(View.INVISIBLE);
        }

        SiteViewModel selectedSite = presenter.getBusinessUnitViewModel().getFiltersViewModel().getSelectedSite();
        String siteName = null;
        if (selectedSite != null) {
            siteName = selectedSite.getName();
        }

        String selectedOrgName = PrintOSPreferences.getInstance().getSelectedOrganization().getOrganizationName();

        siteNameTextView.setText(TextUtils.isEmpty(siteName) ? selectedOrgName : String.format("%s - %s", selectedOrgName, siteName));

    }

    private void showRankingLeaderboardPermissionRequestedLayout() {

        HPUIUtils.setVisibility(false, requestLeaderboardPermissionLayout);
        HPUIUtils.setVisibility(true, errorLayout, refreshButton);

        errorMessageTextView.setText(R.string.ranking_leaderboard_permission_requested);
    }

    public void loadScreen(BusinessUnitViewModel businessUnitViewModel) {

        if (presenter == null) {
            presenter = new RankingLeaderboardPresenter();
            presenter.attachView(this);
        }

        presenter.setBusinessUnitViewModel(businessUnitViewModel);

        if (presenter.getRankingViewModel() == null || !PermissionsManager.getInstance().isPermissionLoaded()) {

            if (!PermissionsManager.getInstance().isPermissionLoaded()) {

                requestPermissionAnUpdate();

            } else {

                initView();
            }

        } else {

            scrollToTab(focusableTab);

        }
    }

    private void requestPermissionAnUpdate() {

        PermissionsManager.getInstance()
                .getPermissionsData(true)
                .subscribe(new Subscriber<Response<PermissionsData>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        if (refreshButton != null){
                            refreshButton.setPressed(false);
                        }
                    }

                    @Override
                    public void onNext(Response<PermissionsData> permissionsDataResponse) {

                        if (refreshButton == null){
                            return;
                        }

                        refreshButton.setPressed(false);
                        initView();
                    }
                });
    }

    private void scrollToTab(String tabKey) {

        if (tabKey == null) {
            return;
        }

        int index = adapter.fragmentKeys.indexOf(tabKey.toLowerCase());
        if (index >= 0) {
            rankingTabLayout.getTabAt(index).select();
            LeaderboardListFragment fragment = (LeaderboardListFragment) adapter.fragmentHashMap.get(tabKey.toLowerCase());
            fragment.setFocusOnMySiteCell();
        }

        setFocusableTab(null);
    }

    private void setupViewPager() {

        RankingViewModel rankingViewModel = presenter.getRankingViewModel();

        showEmptyView(false);

        adapter = new RankingPagerAdapter(getActivity().getSupportFragmentManager(), getActivity().getApplicationContext());

        if (rankingViewModel.hasSubRegionSection()) {
            adapter.addFragment(LeaderboardListFragment.newInstance(rankingViewModel.getSubRegionViewModel()),
                    rankingViewModel.getSubRegionViewModel().getRankAreaType().getKey(),
                    getString(rankingViewModel.getSubRegionViewModel().getRankAreaType().getTitleResourceId()));
        }

        if (rankingViewModel.hasRegionSection()) {
            adapter.addFragment(LeaderboardListFragment.newInstance(rankingViewModel.getRegionViewModel()),
                    RankingViewModel.RankAreaType.REGION.getKey(),
                    getString(rankingViewModel.getRegionViewModel().getRankAreaType().getTitleResourceId()));
        }

        if (rankingViewModel.hasWorldWideSection()) {
            adapter.addFragment(LeaderboardListFragment.newInstance(rankingViewModel.getWorldWideViewModel()),
                    RankingViewModel.RankAreaType.WORLD_WIDE.getKey(),
                    getString(rankingViewModel.getWorldWideViewModel().getRankAreaType().getTitleResourceId()));
        }

        HPUIUtils.setVisibility(adapter.getCount() > 1, rankingTabLayout);

        rankingPager.setOffscreenPageLimit(adapter.getCount());
        rankingPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(rankingTabLayout));
        rankingPager.setScrollingEnabled(false);

        rankingPager.setAdapter(adapter);

        rankingTabLayout.setupWithViewPager(rankingPager);
        rankingTabLayout.addOnTabSelectedListener(this);

        adapter.customizeTabs(rankingTabLayout);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                scrollToTab(focusableTab);
            }
        }, 500);

    }

    private void showEmptyView(boolean showEmptyLayout) {
        HPUIUtils.setVisibility(!showEmptyLayout, mainLayout);
        HPUIUtils.setVisibility(showEmptyLayout, emptyView);
    }

    @OnClick(R.id.ranking_leaderboard_request_permission)
    void onRequestRankingLeaderboardPermissionClicked() {

        Analytics.sendEvent(Analytics.RANKING_LEADERBOARD_FEATURE_ENABLED);

        if (PrintOSPreferences.getInstance().isBeatCoinsClaimed(BeatCoinsPresenter.BeatCoinsMode.RANKING_LEADERBOARD)) {

            setPermissionRequestProgressBarVisibility(true);

            requestLeaderboardPermission();

        } else {

            if (beatCoinsPresenter != null) {

                setPermissionRequestProgressBarVisibility(true);

                beatCoinsPresenter.claimCoins(BeatCoinsPresenter.BeatCoinsMode.RANKING_LEADERBOARD,
                        RankingLeaderboardPresenter.RANKING_LEADERBOARD_BEAT_COINS);
            }
        }

    }

    @OnClick(R.id.button_refresh)
    void onRefreshClicked() {

        refreshButton.setSelected(true);

        requestPermissionAnUpdate();
    }

    private void setPermissionRequestProgressBarVisibility(boolean visible) {
        HPUIUtils.setVisibilityForViews(visible, requestPermissionLoadingProgressBar);

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_ranking_leaderboard;
    }

    @Override
    public boolean isFragmentNameToBeDisplayed() {
        return true;
    }

    @Override
    public boolean onBackPressed() {
        if (callbacks != null) {
            callbacks.onRankingLeaderboardFragmentBackPressed();
        }
        return true;
    }

    @Override
    public boolean isFilteringAvailable() {
        return false;
    }

    @Override
    public boolean isBeta() {
        return true;
    }

    @Override
    public boolean isMenuIconVisible() {
        return false;
    }

    @Override
    public boolean isNotificationIconVisible() {
        return false;
    }

    @Override
    public boolean isSearchAvailable() {
        return false;
    }

    @Override
    public int getToolbarDisplayName() {
        return R.string.fragment_ranking_leaderboard_screen_name;
    }

    @Override
    public void onCancel() {

    }

    public void attachListener(RankingLeaderboardFragmentCallbacks callbacks) {
        this.callbacks = callbacks;
    }

    @Override
    public void showLoading() {
        loadingIndicator.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        loadingIndicator.setVisibility(View.GONE);
    }

    @Override
    public void onRequestLeaderboardPermissionCompleted() {
        setPermissionRequestProgressBarVisibility(false);
        showRankingLeaderboardPermissionRequestedLayout();

    }

    @Override
    public void onRequestLeaderboardPermissionFailed() {
        setPermissionRequestProgressBarVisibility(false);
        HPUIUtils.displayToastWithCenteredText(getContext(), "Failed to request ranking leaderboard permission.");
    }

    @Override
    public void onGettingSiteRankingViewModelCompleted(RankingViewModel rankingViewModel, boolean isGroupSelected) {

        hideLoading();

        //If site doesn't have ranking data.
        if (rankingViewModel == null) {
            showEmptyView(true);
            emptyView.setText(getString(isGroupSelected ? R.string.ranking_leaderboard_group_selected : R.string.ranking_leaderboard_no_ranking_this_week));
            clearAdapter();
        } else {
            setupViewPager();
        }

    }

    private void clearAdapter() {
        if (adapter != null) {
            adapter.clear();
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onGettingSiteRankingViewModelFailed() {
        hideLoading();
        showEmptyView(true);
    }

    @Override
    public void onError(APIException exception, String tag) {

    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        if (tab != null) {
            //adapter.setSelected(true, tab);
        }
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {
        if (tab != null) {
            //adapter.setSelected(false, tab);
        }
    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {
        if (tab != null) {
            //adapter.setSelected(true, tab);
        }
    }

    @Override
    public void onBusinessUnitSelected(BusinessUnitViewModel businessUnitViewModel) {
        if (presenter != null) {
            presenter.setBusinessUnitViewModel(businessUnitViewModel);
        }
    }

    @Override
    public void onFiltersChanged(BusinessUnitViewModel businessUnitViewModel) {
        if (presenter != null) {
            presenter.setRankingViewModel(null);
            presenter.setBusinessUnitViewModel(businessUnitViewModel);
        }
    }

    @Override
    public void didFinishGettingBusinessUnits(Map<BusinessUnitEnum, BusinessUnitViewModel> businessUnitsMap) {

    }

    @Override
    public void onMainError(String msg, boolean triggerSignOut, boolean isHasNoDevices) {

    }

    @Override
    public boolean respondToValidationPeriodExceedance() {
        return false;
    }

    @Override
    public void onClaimCoinsSucceeded() {
        setPermissionRequestProgressBarVisibility(false);
        requestLeaderboardPermission();
    }

    @Override
    public void onClaimCoinsFailed() {
        setPermissionRequestProgressBarVisibility(false);
        requestLeaderboardPermission();
    }

    private void requestLeaderboardPermission() {
        if (presenter != null) {
            presenter.requestRankingLeaderboard();
        }
    }

    @Override
    public void onStoreAccountNotFound() {

        setPermissionRequestProgressBarVisibility(false);

        if (beatCoinsPresenter != null) {
            beatCoinsPresenter.showCreateStoreAccountPopup(getActivity());
        }
    }

    @Override
    public void onCreateAccountSelected() {
        AppUtils.startApplication(getActivity(), Uri.parse(Constants.BeatCoinsStore.PRINT_BEAT_STORE_LOGIN_URL));
    }

    @Override
    public void onCancelCreateAccountSelected() {
        if (presenter != null) {
            presenter.requestRankingLeaderboard();
        }
    }

    public void setRankingViewModel(RankingViewModel rankingViewModel) {
        HPUIUtils.setVisibility(true, rankingCellContainer);
        setMySiteDemoCell(rankingViewModel);
    }

    public void setFocusableTab(String focusableTab) {
        this.focusableTab = focusableTab;
    }

    public interface RankingLeaderboardFragmentCallbacks {
        void onRankingLeaderboardFragmentBackPressed();
    }

    private static class RankingPagerAdapter extends FragmentStatePagerAdapter {

        private final Map<String, Fragment> fragmentHashMap = new HashMap<>();
        private final List<String> fragmentKeys = new ArrayList<>();
        private final List<String> fragmentNames = new ArrayList<>();
        private final Context context;

        public RankingPagerAdapter(FragmentManager fm, Context context) {
            super(fm);
            this.context = context;
        }

        public void addFragment(Fragment fragment, String key, String name) {
            fragmentHashMap.put(key.toLowerCase(), fragment);
            fragmentKeys.add(key.toLowerCase());
            fragmentNames.add(name);
        }

        @Override
        public Fragment getItem(int position) {
            return fragmentHashMap.get(fragmentKeys.get(position));
        }

        @Override
        public boolean isViewFromObject(View view, Object fragment) {
            return ((Fragment) fragment).getView() == view;
        }

        /**
         * Here we can safely save a reference to the created
         * Fragment, no matter where it came from (either getItem() or
         * FragmentManager).
         */
        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            Fragment fragment = (Fragment) super.instantiateItem(container, position);

            String name = fragmentKeys.get(position);
            if (fragmentHashMap.containsKey(name)) {
                fragmentHashMap.remove(name);
            }
            fragmentHashMap.put(name, fragment);
            // save the appropriate reference depending on position
            return fragment;
        }

        public void customizeTabs(TabLayout tabLayout) {
            for (int i = 0; i < tabLayout.getTabCount(); i++) {
                TabLayout.Tab tab = tabLayout.getTabAt(i);
                tab.setCustomView(getTabView(i));
            }
        }

        public View getTabView(int position) {
            View view = LayoutInflater.from(context).inflate(R.layout.leaderboard_custom_tab, null);
            TextView title = view.findViewById(R.id.tab_title);
            title.setText(fragmentNames.get(position));
            return view;
        }

        public void setSelected(boolean selected, TabLayout.Tab tab) {
            View view = tab.getCustomView();
            TextView title = view.findViewById(R.id.tab_title);
            title.setTextColor(ContextCompat.getColor(context, selected ? R.color.c2 : R.color.hp_black));
        }

        @Override
        public int getCount() {
            return fragmentKeys.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return fragmentNames.get(position);
        }

        private List<Fragment> getFragments() {
            return new ArrayList<>(fragmentHashMap.values());
        }

        public void clear() {
            fragmentHashMap.clear();
            fragmentKeys.clear();
            fragmentNames.clear();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if (presenter != null) {
            presenter.detachView();
        }

        if (beatCoinsPresenter != null) {
            beatCoinsPresenter.detachView();
        }

    }

}
