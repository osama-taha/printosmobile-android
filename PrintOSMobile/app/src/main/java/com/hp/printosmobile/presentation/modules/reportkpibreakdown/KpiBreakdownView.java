package com.hp.printosmobile.presentation.modules.reportkpibreakdown;

import com.hp.printosmobile.presentation.MVPView;

import java.util.List;

/**
 * Created by Anwar Asbah on 1/30/2018.
 */
public interface KpiBreakdownView extends MVPView {

    void displayKpiBreakdown(List<KpiBreakdownViewModel> kpiBreakdownViewModels);

}
