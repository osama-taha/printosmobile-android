package com.hp.printosmobile.presentation.modules.insights.kz.kzfooter;

import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.hp.printosmobile.Analytics;
import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;
import com.hp.printosmobile.data.remote.models.kz.KZItem;
import com.hp.printosmobile.presentation.BaseActivity;
import com.hp.printosmobile.presentation.Navigator;
import com.hp.printosmobile.presentation.modules.insights.kz.KZDefaultDetailsFragment;
import com.hp.printosmobile.presentation.modules.insights.kz.KZDefaultView;
import com.hp.printosmobile.presentation.modules.insights.kz.KZSupportedFormat;
import com.hp.printosmobile.presentation.modules.insights.kz.KZVideoActivity;
import com.hp.printosmobile.presentation.modules.insights.kz.KZVideoView;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitEnum;
import com.hp.printosmobile.presentation.modules.shared.PrintOSBaseAdapter;

import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;

public class FavoritesActivity extends BaseActivity implements FavoritesView, KZItemDetailsCallback {

    private static String selectedBu;
    private static BusinessUnitEnum businessUnitEnum;
    private static InsightsView callback;
    private FavoritesPresenter presenter;

    @Bind(R.id.appbar)
    AppBarLayout appbar;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.toolbar_underline)
    View toolbarUnderline;
    @Bind(R.id.empty_tv)
    TextView emptyTextView;
    @Bind(R.id.error_icon)
    ImageView errorIcon;
    @Bind(R.id.favorites_progress_bar)
    ProgressBar favoritesProgressBar;
    @Bind(android.R.id.empty)
    LinearLayout emptyLayout;
    @Bind(R.id.error_layout)
    LinearLayout errorLayout;
    @Bind(R.id.main_content)
    View containerView;
    @Bind(R.id.favorites_results_rv)
    RecyclerView favoritesRecyclerView;
    private LinearLayoutManager layoutManager;
    private FavoritesAdapter favoritesInsightsAdapter;
    private boolean isLoading;
    private int currentPage = 1;
    private List<KZItem> kzItemsList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(getString(R.string.favorites));
            toolbar.setTitleTextColor(getResources().getColor(R.color.hp_default));
            toolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.hp_default), PorterDuff.Mode.SRC_ATOP);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onBackPressed();
                }
            });
        }

        presenter = new FavoritesPresenter(selectedBu);
        presenter.attachView(this);
        presenter.getFavoritesData(currentPage);
        setUpFavoritesResultsList();
        KZDefaultView.addKZItemDetailsCallback(this);
        KZVideoView.addKZItemDetailsCallback(this);
        KZDefaultDetailsFragment.addKZItemDetailsCallback(this);
        KZVideoActivity.addKZItemDetailsCallback(this);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_favorites;
    }

    public static Intent createIntent(Context context, BusinessUnitEnum buEnum) {
        Intent intent = new Intent(context, FavoritesActivity.class);
        selectedBu = buEnum.getKzName();
        businessUnitEnum = buEnum;
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        return intent;
    }

    @Override
    public void onBackPressed() {
        hideLoadingView();
        super.onBackPressed();
    }

    @Override
    public void openKnowledgeZoneItem(KZItem kzItem) {

        Analytics.sendEvent(String.format(Analytics.OPEN_INSIGHTS_ITEM_FROM_FAVORITES, businessUnitEnum.getShortName()), KZSupportedFormat.from(kzItem.getFormat()).getValue());

        Navigator.openKZDetailsActivity(this, kzItem, businessUnitEnum);

    }

    @OnClick(R.id.retry_btn)
    public void onReloadButtonClicked() {
        presenter.onReloadButtonClicked();
    }

    @Override
    public void showEmptyView() {
        errorIcon.setVisibility(View.GONE);
        emptyLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideEmptyView() {
        emptyLayout.setVisibility(View.GONE);
    }

    @Override
    public void showLoadingView() {
        favoritesProgressBar.setVisibility(View.VISIBLE);
        toolbarUnderline.setVisibility(View.GONE);
        isLoading = true;
    }

    @Override
    public void hideLoadingView() {
        favoritesProgressBar.setVisibility(View.GONE);
        toolbarUnderline.setVisibility(View.VISIBLE);
        isLoading = false;
    }

    @Override
    public void addResults(List<KZItem> knowledgeZoneItems) {
        this.kzItemsList = knowledgeZoneItems;
        favoritesInsightsAdapter.addAll(kzItemsList);
    }

    private void setUpFavoritesResultsList() {

        layoutManager = new LinearLayoutManager(PrintOSApplication.getAppContext(), LinearLayoutManager.VERTICAL, false);
        favoritesRecyclerView.setLayoutManager(layoutManager);
        favoritesInsightsAdapter = new FavoritesAdapter(selectedBu, this);
        favoritesInsightsAdapter.setOnReloadClickListener(onReloadClickListener);
        favoritesRecyclerView.setAdapter(favoritesInsightsAdapter);
        favoritesRecyclerView.addOnScrollListener(recyclerViewOnScrollListener);
        favoritesInsightsAdapter.setOnItemClickListener(onItemClickListener);
        favoritesRecyclerView.addItemDecoration(new VerticalSpaceItemDecoration(VerticalSpaceItemDecoration.VERTICAL_ITEM_SPACE));
    }

    private PrintOSBaseAdapter.OnReloadClickListener onReloadClickListener = new PrintOSBaseAdapter.OnReloadClickListener() {
        @Override
        public void onReloadClick() {
            presenter.getNextPage(1, favoritesInsightsAdapter.getItemCount());
        }
    };

    private PrintOSBaseAdapter.OnItemClickListener onItemClickListener = new PrintOSBaseAdapter.OnItemClickListener() {
        @Override
        public void onItemClick(int position, View view) {
            openKnowledgeZoneItem(favoritesInsightsAdapter.getItem(position));
        }
    };

    private RecyclerView.OnScrollListener recyclerViewOnScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);

            if (dy <= 0) {
                return;
            }

            int visibleItemCount = layoutManager.getChildCount();
            int totalItemCount = layoutManager.getItemCount();
            int firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();

            if (!isLoading && !presenter.isLastPage()) {
                if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                        && firstVisibleItemPosition >= 0
                        && totalItemCount >= FavoritesPresenter.PAGE_SIZE) {
                    presenter.getNextPage(currentPage += 1, favoritesInsightsAdapter.getItemCount());
                }
            }

        }
    };

    @Override
    public void clearResults() {
        favoritesInsightsAdapter.clearItems();
    }

    @Override
    public void hideResultsView() {
        favoritesRecyclerView.setVisibility(View.GONE);
    }

    @Override
    public void showResultsView() {
        favoritesRecyclerView.setVisibility(View.VISIBLE);
    }

    @Override
    public void setEmptyText() {
        emptyTextView.setText(getString(R.string.no_favorites_found));
    }

    @Override
    public void showErrorView() {
        errorLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideErrorView() {
        errorLayout.setVisibility(View.GONE);
    }

    @Override
    public void showLoadingFooterView() {
        isLoading = true;
        favoritesInsightsAdapter.updateFooterView(PrintOSBaseAdapter.FooterTypeEnum.LOAD_MORE);
    }

    @Override
    public void addFooterView() {
        favoritesInsightsAdapter.addFooterView();
    }

    @Override
    public void removeFooterView() {
        favoritesInsightsAdapter.removeFooterView();
        isLoading = false;
    }

    @Override
    public void showErrorFooterView() {
        favoritesInsightsAdapter.updateFooterView(PrintOSBaseAdapter.FooterTypeEnum.ERROR);
    }

    @Override
    public void showNoResultsFooterView() {
        favoritesInsightsAdapter.updateFooterView(PrintOSBaseAdapter.FooterTypeEnum.NO_RESULTS);
    }

    @Override
    public void onDestroy() {
        if (presenter != null) {
            presenter.detachView();
        }
        removeListeners();

        super.onDestroy();
    }

    private void removeListeners() {
        favoritesInsightsAdapter.setOnItemClickListener(null);
        favoritesRecyclerView.removeOnScrollListener(recyclerViewOnScrollListener);
    }

    @Override
    public void onFavoritePressed(KZItem kzItem) {
        updatePanel(kzItem);
    }

    @Override
    public void onLikePressed(KZItem kzItem) {
        updatePanel(kzItem);
    }

    @Override
    public void onDislikePressed(KZItem kzItem) {
        updatePanel(kzItem);
    }

    @Override
    public void onResume() {
        super.onResume();
        favoritesInsightsAdapter.notifyDataSetChanged();
    }

    private void updatePanel(KZItem kzItem) {
        if (callback != null) {
            callback.updateView(kzItem);
        }

        if (kzItemsList == null || kzItemsList.size() == 0) {
            return;
        }

        for (KZItem kzListItem : kzItemsList) {
            if (kzListItem.getId().equals(kzItem.getId())) {
                kzItemsList.set(kzItemsList.indexOf(kzListItem), kzItem);
                favoritesInsightsAdapter.clearItems();
                favoritesInsightsAdapter.addAll(kzItemsList);
                favoritesInsightsAdapter.notifyDataSetChanged();
            }
        }
    }

    public static void addCallback(InsightsView insightsView) {
        callback = insightsView;
    }

    public interface InsightsView {
        void updateView(KZItem kzItem);
    }
}
