package com.hp.printosmobile.presentation.modules.filters;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Osama Taha on 10/9/16.
 */
public class GroupViewModel implements FilterItem, Serializable {

    private String id;
    private String name;
    private boolean isSelected;
    private List<FilterItem> devices;

    public GroupViewModel() {
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String getId() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setDevices(List<FilterItem> devices) {
        this.devices = devices;
    }

    public List<FilterItem> getDevices() {
        return devices;
    }

    @Override
    public List<FilterItem> getItems() {
        List<FilterItem> items = new ArrayList<>();
        items.addAll(devices);
        return items;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    @Override
    public boolean isSelected() {
        return false;
    }

    @Override
    public int compareTo(FilterItem filterItem) {
        return 0;
    }

    @Override
    public String toString() {
        return "GroupViewModel{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}