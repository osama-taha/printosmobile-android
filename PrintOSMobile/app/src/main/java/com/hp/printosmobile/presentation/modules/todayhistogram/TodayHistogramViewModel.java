package com.hp.printosmobile.presentation.modules.todayhistogram;

import android.os.Build;

import com.hp.printosmobile.R;
import com.hp.printosmobile.presentation.modules.home.ShiftViewModel;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitEnum;
import com.hp.printosmobile.presentation.modules.shared.MeasureTypeEnum;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * Created by Anwar Asbah on 9/18/2016.
 */
public class TodayHistogramViewModel implements Serializable {

    private static final String PRIMARY_COLOR = "#303F9F";
    private static final String SECONDARY_COLOR = "#0693d2";

    public enum DetailTypeEnum {
        MAIN("main", PRIMARY_COLOR, SECONDARY_COLOR),
        SQM("sqm", PRIMARY_COLOR, SECONDARY_COLOR),
        LM("lm", PRIMARY_COLOR, SECONDARY_COLOR),
        SHEETS("sheets", PRIMARY_COLOR, SECONDARY_COLOR);

        String primaryColor;
        String secondaryColor;
        String key;

        DetailTypeEnum(String key, String primaryColor, String secondaryColor) {
            this.primaryColor = primaryColor;
            this.secondaryColor = secondaryColor;
            this.key = key;
        }

        public String getPrimaryColor() {
            return primaryColor;
        }

        public String getSecondaryColor() {
            return secondaryColor;
        }

        public String getKey() {
            return key;
        }

        public static DetailTypeEnum from(String key) {
            if (key != null) {
                for (DetailTypeEnum detailTypeEnum : DetailTypeEnum.values()) {
                    if (detailTypeEnum.key.equals(key)) {
                        return detailTypeEnum;
                    }
                }
            }
            return MAIN;
        }
    }

    private boolean isShiftSupport;
    private List<TodayHistogramItem> data;
    private Map<String, HistogramShiftLegend> shiftLegends;
    private int lowerThreshold;
    private int higherThreshold;
    private BusinessUnitEnum businessUnit;
    private String impressionType;
    private MeasureTypeEnum measureTypeEnum = MeasureTypeEnum.MIXED;

    public String getImpressionType() {
        return impressionType;
    }

    public void setImpressionType(String impressionType) {
        this.impressionType = impressionType;
    }

    public MeasureTypeEnum getMeasureTypeEnum() {
        return measureTypeEnum;
    }

    public void setMeasureTypeEnum(MeasureTypeEnum measureTypeEnum) {
        this.measureTypeEnum = measureTypeEnum;
    }

    public void setShiftSupport(boolean shiftSupport) {
        isShiftSupport = shiftSupport;
    }

    public boolean isShiftSupport() {
        return isShiftSupport;
    }

    public List<TodayHistogramItem> getData() {
        return data;
    }

    public void setData(List<TodayHistogramItem> data) {
        this.data = data;
    }

    public void setShiftLegends(Map<String, HistogramShiftLegend> shiftLegends) {
        this.shiftLegends = shiftLegends;
    }

    public Map<String, HistogramShiftLegend> getShiftLegends() {
        return shiftLegends;
    }

    public int getMaxValue(int daysSpan) {

        if (data == null) {
            return 0;
        }

        int maxValue = 0;

        for (int i = 1; i <= daysSpan + 1 && i <= data.size(); i++) {
            maxValue = Math.max(maxValue, data.get(data.size() - i).getActual());
            if (i == 1) {
                maxValue = Math.max(maxValue, data.get(data.size() - 1).getTarget());
            }
        }

        return maxValue;
    }

    public void setBusinessUnit(BusinessUnitEnum businessUnit) {
        this.businessUnit = businessUnit;
    }

    public BusinessUnitEnum getBusinessUnit() {
        return businessUnit;
    }

    public int getLowerThreshold() {
        return lowerThreshold;
    }

    public void setLowerThreshold(int lowerThreshold) {
        this.lowerThreshold = lowerThreshold;
    }

    public int getHigherThreshold() {
        return higherThreshold;
    }

    public void setHigherThreshold(int higherThreshold) {
        this.higherThreshold = higherThreshold;
    }

    public static class TodayHistogramItem implements Comparable<TodayHistogramItem>, Serializable {

        public static final float ERROR_VALUE = -1;

        private int actual;
        private int target;
        private TodayHistogramItemStatus status;
        private Date date;
        private float sheets = ERROR_VALUE;
        private float meters = ERROR_VALUE;
        private float metersSquare = ERROR_VALUE;
        private float lastWeekValue;
        private float lastWeekSheets;
        private float lastWeekMeters;
        private float lastWeekMetersSquare;
        private int dayOrdinal;
        private ShiftViewModel shift;

        public float getLastWeekValue() {
            return lastWeekValue;
        }

        public void setLastWeekValue(float lastWeekValue) {
            this.lastWeekValue = lastWeekValue;
        }

        public float getLastWeekSheets() {
            return lastWeekSheets;
        }

        public void setLastWeekSheets(float lastWeekSheets) {
            this.lastWeekSheets = lastWeekSheets;
        }

        public float getLastWeekMeters() {
            return lastWeekMeters;
        }

        public void setLastWeekMeters(float lastWeekMeters) {
            this.lastWeekMeters = lastWeekMeters;
        }

        public float getLastWeekMetersSquare() {
            return lastWeekMetersSquare;
        }

        public void setLastWeekMetersSquare(float lastWeekMetersSquare) {
            this.lastWeekMetersSquare = lastWeekMetersSquare;
        }

        public int getActual() {
            return actual;
        }

        public void setActual(int actual) {
            this.actual = actual;
        }

        public int getTarget() {
            return target;
        }

        public void setTarget(int target) {
            this.target = target;
        }

        public TodayHistogramItemStatus getStatus() {
            return status;
        }

        public void setStatus(TodayHistogramItemStatus status) {
            this.status = status;
        }

        public void setDate(Date date) {
            this.date = date;
        }

        public Date getDate() {
            return date;
        }

        public int getDayOrdinal() {
            return dayOrdinal;
        }

        public void setDayOrdinal(int dayOrdinal) {
            this.dayOrdinal = dayOrdinal;
        }

        public float getSheets() {
            return sheets;
        }

        public void setSheets(float sheets) {
            this.sheets = sheets;
        }

        public float getMeters() {
            return meters;
        }

        public void setMeters(float meters) {
            this.meters = meters;
        }

        public float getMetersSquare() {
            return metersSquare;
        }

        public void setMetersSquare(float metersSquare) {
            this.metersSquare = metersSquare;
        }

        public void setShift(ShiftViewModel shift) {
            this.shift = shift;
        }

        public ShiftViewModel getShift() {
            return shift;
        }

        public float getDayData(TodayHistogramViewModel.DetailTypeEnum detailTypeEnum) {
            switch (detailTypeEnum) {
                case SQM:
                    return getMetersSquare();
                case LM:
                    return getMeters();
                case SHEETS:
                    return getSheets();
                default:
                    return getActual();
            }
        }

        public float getLastWeekData(TodayHistogramViewModel.DetailTypeEnum detailTypeEnum) {
            switch (detailTypeEnum) {
                case SQM:
                    return getLastWeekMetersSquare();
                case LM:
                    return getLastWeekMeters();
                case SHEETS:
                    return getLastWeekSheets();
                default:
                    return getLastWeekValue();
            }
        }

        public static final Comparator<TodayHistogramItem> COMPARATOR = new Comparator<TodayHistogramItem>() {
            @Override
            public int compare(TodayHistogramItem lhs, TodayHistogramItem rhs) {
                if (lhs == null && rhs == null) return 0;
                if (lhs == null) return -1;
                if (rhs == null) return 1;
                return lhs.compareTo(rhs);
            }
        };

        @Override
        public int compareTo(TodayHistogramItem another) {
            Date leftDate = this.getDate();
            Date rightDate = another.getDate();

            if (leftDate == null && rightDate == null) return 0;
            if (leftDate == null) return -1;
            if (rightDate == null) return 1;

            return leftDate.compareTo(rightDate);
        }

        @Override
        public boolean equals(Object obj) {
            return super.equals(obj);
        }

        @Override
        public int hashCode() {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                return Objects.hash(actual, target, status, date, sheets, meters, metersSquare, lastWeekValue, lastWeekSheets, lastWeekMeters, lastWeekMetersSquare, dayOrdinal, shift);
            }
            return 0;
        }

        public enum TodayHistogramItemStatus {

            GOOD(R.color.bar_green),
            AVERAGE(R.color.bar_orange),
            BELOW_AVERAGE(R.color.bar_red);

            private int colorId;

            TodayHistogramItemStatus(int colorId) {
                this.colorId = colorId;
            }

            public int getColorId() {
                return colorId;
            }

            public static TodayHistogramItemStatus getStatus(int actual, int target, int lowerThreshold, int higherThreshold) {
                float percentage = target == 0 ? 100 : (((float) actual / (float) target) * 100);
                if (percentage <= lowerThreshold) {
                    return BELOW_AVERAGE;
                } else if (percentage <= higherThreshold) {
                    return AVERAGE;
                } else {
                    return GOOD;
                }
            }
        }

    }

    public static class HistogramShiftLegend implements Serializable {

        private String legendName;
        private int legendColor;

        public String getLegendName() {
            return legendName;
        }

        public void setLegendName(String legendName) {
            this.legendName = legendName;
        }

        public int getLegendColor() {
            return legendColor;
        }

        public void setLegendColor(int legendColor) {
            this.legendColor = legendColor;
        }

        @Override
        public String toString() {
            final StringBuffer sb = new StringBuffer("HistogramShiftLegend{");
            sb.append("legendName='").append(legendName).append('\'');
            sb.append(", legendColor='").append(legendColor).append('\'');
            sb.append('}');
            return sb.toString();
        }

    }
}
