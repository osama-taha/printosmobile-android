package com.hp.printosmobile.presentation.modules.kpiview;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.content.res.ResourcesCompat;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.hp.printosmobile.R;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.presentation.modules.shared.Unit;
import com.hp.printosmobile.utils.HPLocaleUtils;
import com.hp.printosmobile.utils.HPUIUtils;
import com.hp.printosmobilelib.ui.common.HPView;
import com.hp.printosmobilelib.ui.widgets.HPProgressBar;
import com.squareup.picasso.Picasso;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Anwar Asbah on 4/27/2016.
 */
public class KPIView extends FrameLayout implements HPView<KPIViewModel> {

    @Bind(R.id.image_kpi_drawable)
    ImageView kpiImage;
    @Bind(R.id.text_kpi_name)
    TextView kpiNameText;
    @Bind(R.id.text_kpi_value)
    TextView kpiValueText;
    @Bind(R.id.score_layout)
    View scoreLayout;
    @Bind(R.id.info_image_view)
    View infoImageView;
    @Bind(R.id.extra_margin)
    View extraMargin;
    @Bind(R.id.text_kpi_score)
    TextView kpiScoreText;
    @Bind(R.id.kpi_progress_bar)
    HPProgressBar kpiProgressBar;
    @Bind(R.id.image_trend)
    ImageView kpiTrendImage;
    @Bind(R.id.view_separator)
    View separatorView;

    public KPIView(Context context) {
        super(context);
        init();
    }

    public KPIView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public KPIView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        inflate(getContext(), R.layout.view_kpi, this);
        ButterKnife.bind(this, this);
    }

    public void hidePoints(boolean hidePoints) {
        kpiValueText.setVisibility(hidePoints ? GONE : VISIBLE);
    }

    public void hideSeparatorView(boolean hideSeparatorView) {
        separatorView.setVisibility(hideSeparatorView ? INVISIBLE : VISIBLE);
    }

    @Override
    public void setViewModel(KPIViewModel viewModel) {
        Picasso.with(getContext()).load(viewModel.getKpiDrawable()).into(kpiImage);
        kpiNameText.setText(viewModel.getLocalizedName());

        String valueString = getValue(viewModel.getValue(), viewModel.getUnit());
        valueString = viewModel.getCustomText() == null ? valueString :
                valueString + viewModel.getCustomText();
        kpiValueText.setText(valueString);

        setKpiProgressBarColor(kpiProgressBar, viewModel.getScoreState()).setValues(viewModel.getScore(), viewModel.getMaxScore());
        Picasso.with(getContext()).load(getTrendDrawable(viewModel.getScoreTrend())).into(kpiTrendImage);
        String kpiScoreFormatted = String.format(getResources().getString(R.string.week_panel_kpi_score_notation),
                viewModel.getScore(), viewModel.getMaxScore());
        SpannableString spannableStringText = getTextStyleSpan(kpiScoreFormatted, String.valueOf(viewModel.getScore()));
        kpiScoreText.setText(spannableStringText);

    }

    public static SpannableString getTextStyleSpan(String kpiScoreFormatted, String boldString) {

        SpannableString spannableString = new SpannableString(kpiScoreFormatted);

        int start = 0;
        int end = boldString.length();

        spannableString.setSpan(new StyleSpan(Typeface.BOLD),
                start, end, Spannable.SPAN_INCLUSIVE_INCLUSIVE);

        return spannableString;
    }

    @Override
    public void showLoading() {
    }

    @Override
    public void hideLoading() {
    }

    public int getKpiScoreTextViewWidth() {
        return HPUIUtils.getTextWidth(kpiScoreText, kpiScoreText.getText().toString());
    }

    public void setKpiScoreTextViewWidth(int width) {
        ViewGroup.LayoutParams params = kpiScoreText.getLayoutParams();
        params.width = width;
        kpiScoreText.setLayoutParams(params);
    }

    private int getTrendDrawable(KPIScoreTrendEnum trend) {
        switch (trend) {
            case DECREASE:
                return R.drawable.kpi_trend_dw;
            case INCREASE:
                return R.drawable.kpi_trend_up;
            default:
                return R.drawable.kpi_trend_equal;
        }
    }

    private String getValue(int value, KPIValueHandleEnum valueHandle) {

        if (valueHandle == null) {
            return String.valueOf(value);
        }

        switch (valueHandle) {
            case PERCENT:
                return HPLocaleUtils.getPercentageString(getContext(), value);
            case SQM:
                return Unit.SQM.getUnitTextWithValue(getContext(), PrintOSPreferences.getInstance(getContext()).getUnitSystem(),
                        Unit.UnitStyle.VALUE, HPLocaleUtils.getLocalizedValue(value));
            case HOURS:
                return getContext().getString(R.string.hours, value);
            case KILO:
            case MEGA:
            case TECHNICAL_ISSUES:
            default:
                return HPLocaleUtils.getLocalizedValue(value);
        }

    }

    private HPProgressBar setKpiProgressBarColor(HPProgressBar bar, KPIScoreStateEnum state) {
        int trackColor;
        int progressColor;

        switch (state) {
            case GREAT:
                trackColor = ResourcesCompat.getColor(getResources(), R.color.kpi_score_state_great_track_color, null);
                progressColor = ResourcesCompat.getColor(getResources(), R.color.kpi_score_state_great_progress_color, null);
                break;
            case GOOD:
                trackColor = ResourcesCompat.getColor(getResources(), R.color.kpi_score_state_great_track_color, null);
                progressColor = ResourcesCompat.getColor(getResources(), R.color.kpi_score_state_great_progress_color, null);
                break;
            case AVERAGE:
                trackColor = ResourcesCompat.getColor(getResources(), R.color.kpi_score_state_good_track_color, null);
                progressColor = ResourcesCompat.getColor(getResources(), R.color.kpi_score_state_good_progress_color, null);
                break;
            default:
                trackColor = ResourcesCompat.getColor(getResources(), R.color.kpi_score_state_below_track_color, null);
                progressColor = ResourcesCompat.getColor(getResources(), R.color.kpi_score_state_below_progress_color, null);
                break;
        }
        return bar.setColors(progressColor, trackColor);
    }

    public void addOnScoreClickListener(OnClickListener clickListener) {
        infoImageView.setVisibility(clickListener == null ? GONE : VISIBLE);
        extraMargin.setVisibility(clickListener == null ? GONE : VISIBLE);
        kpiScoreText.setTextColor(ResourcesCompat.getColor(getResources(),
                clickListener == null ? R.color.hp_black : R.color.c62, null));
        scoreLayout.setOnClickListener(clickListener);
    }
}