package com.hp.printosmobile.presentation.modules.contacthp;

import com.hp.printosmobile.presentation.MVPView;
import com.hp.printosmobilelib.core.communications.remote.APIException;
import com.hp.printosmobilelib.core.communications.remote.models.UserData;

import java.util.Map;

/**
 * Created by anwar asbah on 10/12/2016.
 */
public interface ContactHpView extends MVPView {
    void onSuccess();

    void onUserRetrieved(UserData.User user, APIException apiException);

    void onCountriesRetrieved(Map<String, String> countryDataList, APIException apiException);

    void onUserUpdated();
}
