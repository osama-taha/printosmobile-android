package com.hp.printosmobile.presentation.modules.today;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.TextView;

import com.hp.printosmobile.R;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.data.remote.models.MergedTodayAndLastWeekData;
import com.hp.printosmobile.data.remote.models.PreferencesData;
import com.hp.printosmobile.presentation.modules.home.ShiftViewModel;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitEnum;
import com.hp.printosmobile.presentation.modules.shared.Unit;
import com.hp.printosmobile.utils.FileUtils;
import com.hp.printosmobilelib.core.utils.HPDateUtils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by Anwar Asbah on 9/6/2017.
 */

public class TodayGraphUtils {

    public static final String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";

    private static final String GRAPH_HTML_FILE_NAME = "today_graph_blueprint.html";
    private static final String LOCALE_KEY = "@LOCALE@";
    private static final String DAILY_TARGET_KEY = "@DAILYTARGET@";
    private static final String TODAY_SERIES_KEY = "@TODAYSERIES@";
    private static final String LAST_WEEK_SERIES_KEY = "@LASTWEEKSERIES@";
    private static final String DATE_START_KEY = "@DATESTART@";
    private static final String MAX_Y_VAL_KEY = "@MAXYAXIS@";
    private static final String GRAPH_TITLE_KEY = "@TITLE@";
    private static final String DATE_KEY = "@DATE@";
    private static final String VAL_KEY = "@VALUE@";
    private static final String LOCALIZED_DAILY_TARGET_STRING_KEY = "@LOCALIZED_DAILY_TARGET_STRING@";
    private static final String TODAY_SHIFT_SERIES_NAME = "@TODAY_SHIFT_SERIES_NAME@";
    private static final String LAST_WEEK_SERIES_NAME = "@LAST_WEEK_SERIES_NAME@";
    private static final String UNIT_STRING_KEY = "@UNIT@";
    private static final String FOOT_NOTE_KEY = "@FOOT_NOTE@";
    private static final String TODAY_SHIFT_LINE_COLOR = "@TODAY_SHIFT_LINE_COLOR@";
    private static final String TODAY_SHIFT_GRADIENT = "@TODAY_SHIFT_GRADIENT@";
    private static final String GIF_URL = "@GIF_URL@";

    private static final String COLOR_HIGH = "#52d925";
    private static final String COLOR_LOW = "#f86b57";
    private static final String GRADIENT_HIGH = "linearGradient: {x1: 0,y1: 0,x2: 0,y2: 1},\n" +
            "        stops: [\n" +
            "          [0, Highcharts.Color('#52d925').setOpacity(0.5).get('rgba')],\n" +
            "          [1, Highcharts.Color('#FFFFFF').setOpacity(0).get('rgba')],\n" +
            "        ]";
    private static final String GRADIENT_LOW = "linearGradient: {x1: 0,y1: 0,x2: 0,y2: 1},\n" +
            "        stops: [\n" +
            "          [0, Highcharts.Color('#f86b57').setOpacity(0.5).get('rgba')],\n" +
            "          [1, Highcharts.Color('#FFFFFF').setOpacity(0).get('rgba')],\n" +
            "        ]";
    private static final String GIF_URL_HIGH = "https://media.giphy.com/media/l1J9QSlZC1uBdO29G/giphy.gif";
    private static final String GIF_URL_LOW = "https://media.giphy.com/media/3ohhwpir0bvYZbwOaY/giphy.gif";

    private static final String DATE_UTC_STRING_FORMAT = "Date.UTC(%1$d, %2$d, %3$d, %4$d, %5$d, 0)";

    private static final String SERIES_NOW_ITEM = "{" +
            "        x: " + DATE_KEY + ",\n" +
            "        y: " + VAL_KEY + ",\n" +
            "        marker: {\n" +
            "          symbol: 'url(@GIF_URL@)',\n" +
            "          height: 15, width: 15\n" +
            "        }}";
    private static final String SERIES_LAST_NOW_ITEM = "{" +
            "        x: " + DATE_KEY + ",\n" +
            "        y: " + VAL_KEY + "}";

    private static int maxVal;
    private static int weightedAverage;

    private TodayGraphUtils() {
    }

    public static void displayGraph(Context context, WebView progressWebView, TextView chartFooterTodayTextView,
                                    View chartFooterTodayIcon, ShiftViewModel.ShiftType shiftType, TodayViewModel todayViewModel) {

        if (todayViewModel == null) {
            return;
        }

        WebSettings webSettings = progressWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setDomStorageEnabled(true);
        progressWebView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);

        PreferencesData.UnitSystem unitSystem = PrintOSPreferences.getInstance(context).getUnitSystem();
        BusinessUnitEnum businessUnitEnum = todayViewModel.getBusinessUnitViewModel().getBusinessUnit();
        String unit = context.getString(businessUnitEnum == BusinessUnitEnum.INDIGO_PRESS ?
                R.string.impressions : Unit.SQM.getUnitTextResourceId(unitSystem, Unit.UnitStyle.DEFAULT));

        boolean isShift = shiftType != ShiftViewModel.ShiftType.TODAY;

        try {
            String detail = FileUtils.loadAssestFile(context, GRAPH_HTML_FILE_NAME);

            TodayViewModel.TodayVsLastWeekViewModel todayVsLastWeekViewModel = todayViewModel.getTodayVsLastWeekViewModel();

            maxVal = (int) todayViewModel.getPrintVolumeShiftTargetValue();
            List<Integer> todayEntries = todayVsLastWeekViewModel.getTodaySeries();
            List<Integer> lastWeekEntries = todayVsLastWeekViewModel.getLastWeekSeries();

            String series = todayEntries == null ? "" : getSeries(todayViewModel, todayEntries, todayVsLastWeekViewModel.getStartDate(), true);
            String seriesLastWeek = lastWeekEntries == null ? "" : getSeries(todayViewModel, lastWeekEntries, todayVsLastWeekViewModel.getStartDate(), false);

            Calendar today = Calendar.getInstance();
            today.setTime(todayVsLastWeekViewModel.getStartDate());
            String date = String.format(DATE_UTC_STRING_FORMAT,
                    today.get(Calendar.YEAR),
                    today.get(Calendar.MONTH),
                    today.get(Calendar.DAY_OF_MONTH),
                    today.get(Calendar.HOUR_OF_DAY), 0);

            detail = detail.replace(DAILY_TARGET_KEY, String.valueOf(todayViewModel.getPrintVolumeShiftTargetValue()));
            detail = detail.replace(TODAY_SERIES_KEY, series);
            detail = detail.replace(LAST_WEEK_SERIES_KEY, seriesLastWeek);
            detail = detail.replace(DATE_START_KEY, date);
            detail = detail.replace(MAX_Y_VAL_KEY, String.valueOf(maxVal));
            detail = detail.replace(UNIT_STRING_KEY, unit);

            todayViewModel.getTodayVsLastWeekViewModel().setWeightedAverage(weightedAverage);

            boolean isRealtimeHigher = weightedAverage <= todayViewModel.getPrintVolumeValue();
            detail = detail.replace(TODAY_SHIFT_LINE_COLOR, isRealtimeHigher ? COLOR_HIGH : COLOR_LOW);
            detail = detail.replace(TODAY_SHIFT_GRADIENT, isRealtimeHigher ? GRADIENT_HIGH : GRADIENT_LOW);
            detail = detail.replace(GIF_URL, isRealtimeHigher ? GIF_URL_HIGH : GIF_URL_LOW);


            String locale = PrintOSPreferences.getInstance(context).getLanguage(context.getString(R.string.default_language));
            detail = detail.replace(LOCALE_KEY, locale);

            String todayShiftSeriesName;
            if (businessUnitEnum == BusinessUnitEnum.INDIGO_PRESS) {
                todayShiftSeriesName = context.getString(isShift ? R.string.today_chart_impression_this_shift
                        : R.string.today_chart_impression_today);
            } else {
                int stringId;
                if (unitSystem == PreferencesData.UnitSystem.Metric) {
                    stringId = isShift ? R.string.today_chart_sqm_this_shift : R.string.today_chart_sqm_today;
                } else {
                    stringId = isShift ? R.string.today_chart_ft_square_this_shift : R.string.today_chart_ft_square_today;
                }
                todayShiftSeriesName = context.getString(stringId);
            }
            todayShiftSeriesName = todayShiftSeriesName.replace("\'", "\\\'");

            String lastWeekSeriesName;
            if (businessUnitEnum == BusinessUnitEnum.INDIGO_PRESS) {
                lastWeekSeriesName = context.getString(R.string.today_chart_impression_last_week);
            } else {
                lastWeekSeriesName = context.getString(unitSystem == PreferencesData.UnitSystem.Metric ?
                        R.string.today_chart_sqm_last_week : R.string.today_chart_ft_square_last_week);
            }
            lastWeekSeriesName = lastWeekSeriesName.replace("\'", "\\\'");

            detail = detail.replace(TODAY_SHIFT_SERIES_NAME, todayShiftSeriesName);
            detail = detail.replace(LAST_WEEK_SERIES_NAME, lastWeekSeriesName);

            detail = detail.replace(GRAPH_TITLE_KEY, "");

            String dailyTargetString = context.getString(R.string.today_chart_daily_target);
            detail = detail.replace(LOCALIZED_DAILY_TARGET_STRING_KEY, dailyTargetString);

            String footer = context.getString(isShift ? R.string.today_chart_this_shift
                    : R.string.today_chart_today);
            detail = detail.replace(FOOT_NOTE_KEY, "");
            chartFooterTodayTextView.setText(footer);
            chartFooterTodayIcon.setBackgroundResource(isRealtimeHigher
                    ? R.drawable.today_graph_legend_shape_green
                    : R.drawable.today_graph_legend_shape_red);

            progressWebView.loadDataWithBaseURL("", detail, "text/html", "utf-8", "");
        } catch (Exception e) {
            Log.e("Exception ", "======>" + e.toString());
        }
    }

    public static int getWeightedAverage() {
        return weightedAverage;
    }

    private static String getSeries(TodayViewModel todayViewModel, List<Integer> series, Date startDate, boolean isToday) {
        String seriesString = "";

        Calendar nowDate = Calendar.getInstance();
        String date = String.format(DATE_UTC_STRING_FORMAT,
                nowDate.get(Calendar.YEAR),
                nowDate.get(Calendar.MONTH),
                nowDate.get(Calendar.DAY_OF_MONTH),
                nowDate.get(Calendar.HOUR_OF_DAY),
                nowDate.get(Calendar.MINUTE), 0);
        boolean isNowAdded = false;

        for (int i = 0; i < series.size(); i++) {

            Calendar pointDate = Calendar.getInstance();
            pointDate.setTime(startDate);
            pointDate.add(Calendar.HOUR_OF_DAY, i + 1);

            Integer integer = series.get(i);
            int value = integer == null ? 0 : integer;
            maxVal = Math.max(maxVal, value);

            seriesString += value;
            seriesString += (i == series.size() - 1 && !isToday ? "" : ",");

            if (nowDate.getTimeInMillis() < pointDate.getTimeInMillis() && !isNowAdded) {
                isNowAdded = true;

                if (isToday) {
                    value = todayViewModel == null ? 0 : (int) todayViewModel.getPrintVolumeValue();
                    maxVal = Math.max(maxVal, value);

                    String nowItem = SERIES_NOW_ITEM;
                    nowItem = nowItem.replace(DATE_KEY, date);
                    nowItem = nowItem.replace(VAL_KEY, String.valueOf(value));

                    seriesString += nowItem;
                    //stop looping to discard corrupted data in future
                    return seriesString;
                } else {
                    int val1 = value;
                    int val2;
                    if (i < series.size() - 1) {
                        if (series.get(i + 1) == null) {
                            val2 = val1;
                        } else {
                            val2 = series.get(i + 1);
                        }
                    } else {
                        val2 = val1;
                    }

                    weightedAverage = val1 + (int) ((nowDate.get(Calendar.MINUTE) / 60.0f) * (val2 - val1));

                    String lastWeekNowEqivItem = SERIES_LAST_NOW_ITEM;
                    lastWeekNowEqivItem = lastWeekNowEqivItem.replace(DATE_KEY, date);
                    lastWeekNowEqivItem = lastWeekNowEqivItem.replace(VAL_KEY, String.valueOf(weightedAverage));

                    seriesString += lastWeekNowEqivItem;
                    seriesString += (i == series.size() - 1 ? "" : ",");
                }
            }
        }

        //to account to list with partial data
        if (isToday) {
            String nowItem = SERIES_NOW_ITEM;
            nowItem = nowItem.replace(DATE_KEY, date);
            nowItem = nowItem.replace(VAL_KEY, String.valueOf(todayViewModel.getPrintVolumeValue()));
            maxVal = (int) Math.max(maxVal, todayViewModel.getPrintVolumeValue());

            seriesString += nowItem;
        }

        return seriesString;
    }

    public static TodayViewModel parseTodayLastWeekData(TodayViewModel todayViewModel, MergedTodayAndLastWeekData mergedTodayAndLastWeekData) {
        if (todayViewModel == null || mergedTodayAndLastWeekData == null || mergedTodayAndLastWeekData.getHourEntryDatas() == null) {
            return null;
        }

        List<Integer> todaySeries = new ArrayList<>();
        List<Integer> lastWeekSeries = new ArrayList<>();

        for (MergedTodayAndLastWeekData.HourEntryData hourEntryData : mergedTodayAndLastWeekData.getHourEntryDatas()) {
            if (hourEntryData != null) {
                MergedTodayAndLastWeekData.HourEntryData.Value todayValue = hourEntryData.getTodayValue();
                MergedTodayAndLastWeekData.HourEntryData.Value lastWeekValue = hourEntryData.getLastWeekValue();

                if (todayValue != null && todayValue.getValue() != null) {
                    todaySeries.add(todayValue.getValue());
                }
                if (lastWeekValue != null && lastWeekValue.getValue() != null) {
                    lastWeekSeries.add(lastWeekValue.getValue());
                }
            }
        }

        TodayViewModel.TodayVsLastWeekViewModel todayVsLastWeekViewModel = new TodayViewModel.TodayVsLastWeekViewModel();
        todayVsLastWeekViewModel.setTodaySeries(todaySeries);
        todayVsLastWeekViewModel.setLastWeekSeries(lastWeekSeries);

        Date startDate = mergedTodayAndLastWeekData.getStartDate() == null ? Calendar.getInstance().getTime() :
                HPDateUtils.parseDate(mergedTodayAndLastWeekData.getStartDate(), DATE_FORMAT);


        todayVsLastWeekViewModel.setStartDate(startDate);
        todayViewModel.setTodayVsLastWeekViewModel(todayVsLastWeekViewModel);

        return todayViewModel;
    }
}
