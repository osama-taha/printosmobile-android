package com.hp.printosmobile;

/**
 * A class holds constant values.
 *
 * @author Osama Taha
 */
public final class Constants {

    public static final String GOOGLE_PLAY_URL = "market://details?id=";
    public static final String REGISTRATION_LINK = "https://h71044.www7.hp.com/ga/us/en/contactus.php?PRINTOS";
    public static final String NOTIFICATION_BODY_KEY = "body";
    public static final String NOTIFICATION_ENTITY_ID_KEY = "entityId";
    public static final String NOTIFICATION_EVENT_KEY = "event";
    public static final String NOTIFICATION_USER_EVENT_ID_KEY = "userEventId";
    public static final String NOTIFICATION_ID_KEY = "notificationId";
    public static final String NOTIFICATION_LINK_KEY = "link";
    public static final String NAME_ALLOWED_CHARS_REGEX_FORMAT = "^[a-zA-Z0-9- &'.,_]+$";
    public static final String ACCEPT_INVITE_URL = "/start/#/accept/user/";
    public static final String PRINTOS_NS_LOOKUP_TEXT = "locate.printos.com";

    public static final String UNIVERSAL_LINK_SCREEN_KEY = "screen";
    public static final String UNIVERSAL_LINK_BUSINESS_UNIT_KEY = "bu";
    public static final String UNIVERSAL_LINK_SITE_ID_KEY = "siteId";
    public static final String UNIVERSAL_LINK_FILTER_DEVICE_EXTRA = "devices";
    public static final String UNIVERSAL_LINK_ORGANIZATION_ID_KEY = "orgId";
    public static final String UNIVERSAL_LINK_IS_SHIFT = "isShift";
    public static final String UNIVERSAL_LINK_PANEL_KEY = "panelTag";
    public static final String UNIVERSAL_LINK_TARGET_OBJECT_KEY = "targetObject";
    public static final String UNIVERSAL_LINK_DESKTOP_URL = "$desktop_url";
    public static final int UNIVERSAL_LINK_SCREEN_HOME = 1;
    public static final int UNIVERSAL_LINK_SCREEN_INSIGHTS = 2;
    public static final int UNIVERSAL_LINK_SCREEN_STATE_DISTRIBUTION = 3;
    public static final int UNIVERSAL_LINK_SCREEN_SERVICE_CALLS = 4;
    public static final int UNIVERSAL_LINK_SCREEN_KPI_BREAKDOWN = 5;
    public static final int UNIVERSAL_LINK_SCREEN_HISTOGRAM_BREAKDOWN = 6;
    public static final int UNIVERSAL_LINK_SCREEN_PAPOPUP = 7;
    public static final int UNIVERSAL_LINK_SCREEN_LEADERBOARD = 8;
    public static final int UNIVERSAL_LINK_SCREEN_INSIGHTS_ITEM_DETAILS = 9;
    public static final int UNIVERSAL_LINK_SCREEN_INSIGHTS_SEARCH = 10;
    public static final int SWITCH_SERVER_POPUP_MAX_SESSION_COUNT = 25;

    public static final String DEEP_LINK_APP_NOT_INSTALLED_LINK = "/knowledge-zone/#/view/asset/";

    public static final String UNIVERSAL_LINK_SCREEN_INSIGHTS_TAB = "INSIGHTS";
    public static final String UNIVERSAL_LINK_SCREEN_NOTIFICATIONS = "NOTIFICATIONS";
    public static final String UNIVERSAL_LINK_SCREEN_INVITE_SCREEN = "InvitePrintOS";
    public static final String DEFAULT_LOCATION = "Default";
    public static final String CHINA = "China";
    public static final String PDF_FILE_EXTENSION = ".pdf";

    public static final String COOKIE_TAG = "Cookie";
    public static final String ACCEPT_TAG = "Accept";
    public static final String ACCEPT_VAL = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8";
    public static final String ACCEPT_ENCODING_TAG = "Accept-Encoding";
    public static final String ACCEPT_ENCODING_VAL = "gzip, deflate, br";

    public static final String TERMS_OF_SERVICES_HTML_START_TAG = "<html><head><style type=\"text/css\">@font-face {font-family: MyFont;src: url(\"file:///android_asset/HPRegularSimplifiedW01.ttf\")}body {font-family: MyFont;text-align: justify; word-wrap: break-word;}</style></head><body>";
    public static final String TERMS_OF_SERVICES_HTML_END_TAG = "</body></html>";

    public static final long SENDING_ANALYTICS_EVENT_DELAY = 5000;

    //hpid getting edit my details url api constants
    public static final String API_CONSTANT_REGISTRY = "registry";
    public static final String API_CONSTANT_SERVICES = "services";
    public static final String API_CONSTANT_HPID_ACCOUNT = "hpid_account";
    public static final String API_CONSTANT_SV_URL = "sv_url";

    public static final int NONE = -1;
    public static final int TARGET_VIEW_DISPLAY_PERIOD = 90;

    private Constants() {
    }

    public final class IntentExtras {

        public static final int FIRST_INSTALL_PERMISSIONS_REQUEST_CODE = 1076;
        public static final int ACCESS_LOCATION_PERMISSION_REQUEST_CODE = 1077;

        public static final String NOTIFICATION_REGISTRATION_COMPLETED_INTENT_ACTION = "gcm_registration_completed";
        public static final String NOTIFICATION_UNREGISTER_COMPLETED_INTENT_ACTION = "gcm_un_register_completed";

        public static final String NOTIFICATION_UNREGISTER_INTENT_ACTION = "UNREGISTER";
        public static final String NOTIFICATION_REGISTER_INTENT_ACTION = "REGISTER";
        public static final String NOTIFICATION_UNREGISTER_AND_CLEAR_CACHE_INTENT_ACTION = "UNREGISTER_BEFORE_LOGOUT";
        public static final int EULA_ACTIVITY_REQUEST_CODE = 1001;
        public static final int EULA_ACTIVITY_RESULT_CODE = 1002;
        public static final int APPS_PICKER_INTENT_REQUEST_CODE = 1009;
        public static final String EULA_ACTIVITY_TO_ACCEPT_TERMS = "ACCEPT_EULA_TERMS";
        public static final String EULA_ACTIVITY_ACCEPTED_RESULT = "IS_EULA_ACCEPTED";
        public static final String EULA_ACTIVITY_RESULT_OBJECT = "EULA_RESULT";

        public static final String MAIN_ACTIVITY_ORGANIZATION_EXTRA = "organization_extra";
        public static final String MAIN_ACTIVITY_DIVISION_EXTRA = "division_extra";
        public static final String MAIN_ACTIVITY_FILTER_EXTRA = "filter_extra";
        public static final String MAIN_ACTIVITY_FILTER_DEVICE_EXTRA = "filter_device_extra";
        public static final String MAIN_ACTIVITY_TODAY_EXTRA = "today_extra";
        public static final String MAIN_ACTIVITY_IS_SHIFT_EXTRA = "is_shift_extra";
        public static final String MAIN_ACTIVITY_PANEL_EXTRA = "PANEL_EXTRA";
        public static final String MAIN_ACTIVITY_OBJECT_EXTRA = "PRESS_EXTRA";
        public static final String MAIN_ACTIVITY_NOTIFICATION_EXTRA = "notification_extra";
        public static final String MAIN_ACTIVITY_NOTIFICATION_ID_EXTRA = "notification_id_extra";
        public static final String MAIN_ACTIVITY_USER_ID_EXTRA = "user_id_extra";
        public static final String MAIN_ACTIVITY_LINK_EXTRA = "link_extra";
        public static final String MAIN_ACTIVITY_BEAT_COIN_EXTRA = "mainActivityBeatCoinExtra";
        public static final String MAIN_ACTIVITY_IS_PUSH_NOTIFICATION = "is_push_notification";
        public static final String MAIN_ACTIVITY_PB_NOTIFICATION_EXTRA = "mainActivityPBNotificationExtra";

        public static final String VERSION_UPDATE_INTENT_ACTION = "com.hp.printosforpsp.broadcast.version_update";
        public static final String VERSION_UPDATE_INTENT_EXTRA_KEY = "version_update_extra";
        public static final String NOTIFICATION_RECEIVED_ACTION = "NOTIFICATION_RECEIVED_ACTION";
        public static final String BUSINESS_UNIT_EXTRA = "BUSINESS_UNIT";

        public static final String NPS_EXTRA = "NPS";

        public static final String COMING_FROM_LOGIN = "COMING_FROM_LOGIN";
        public static final String SIGN_OUT_REQUESTED = "SIGN_OUT_REQUESTED";
        public static final String MAIN_ACTIVITY_IS_HANDLING_LINK = "main_activity_is_handling_link";
        public static final String KZ_ITEM_EXTRA = "kz_item_extra";
        public static final String ENABLE_RANKING_LEADERBOARD_REQUESTED = "ENABLE_RANKING_LEADERBOARD_REQUESTED";
    }

    public class MaintenanceDialog {

        public static final String FIRST_NAME_PARAMETER = "@FIRSTNAME@";
        public static final String DATE_AND_TIME_PARAMETER = "@DATEANDTIME@";

        private MaintenanceDialog() {
        }
    }

    public final class Notification {

        public static final String PRINTOS_NOTIFICATION_CHANNEL_ID = "printos-notifications-cid";
        public static final String PRINTOS_NOTIFICATION_CHANNEL_NAME = "PrintOS notifications";

        public static final String REG_SERVICE_NOTIFICATION_CHANNEL_ID = "printos-reg-service-cid";
        public static final String REG_SERVICE_NOTIFICATION_CHANNEL_NAME = "Notification Registration";

        private Notification() {
        }
    }

    public final class BeatCoinsStore {
        public static final String PRINT_BEAT_STORE_LOGIN_URL = "https://www.printbeat.store/Account/Login.aspx";
        public static final String PRINT_BEAT_STORE_INSTANT_API = "https://api.cogginsstore.com/api/v1/points/a05033dfa3354586ba02f7e47443a51c/Instant";

    }
}
