package com.hp.printosmobile.presentation.modules.reports.presenter;

import com.hp.printosmobile.presentation.MVPView;
import com.hp.printosmobile.presentation.modules.reports.ReportKpiViewModel;

/**
 * Created by Anwar Asbah on 5/23/16.
 */
public interface ReportView extends MVPView {

    void displayKpiReport(ReportKpiViewModel kpi, boolean isRoot, int pointTotal);

    void onRequestCompleted();

    void onOverallError();
}
