package com.hp.printosmobile.presentation.modules.reportkpibreakdown;

import android.text.TextUtils;

import com.hp.printosmobile.R;
import com.hp.printosmobile.presentation.modules.kpiview.KPIValueHandleEnum;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitEnum;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Anwar Asbah on 1/29/2018.
 */
public enum KpiBreakdownEnum implements Serializable {

    INDIGO_OVERALL(Constants.OVERALL_PERFORMANCE,
            BusinessUnitEnum.INDIGO_PRESS,
            R.string.reports_performance_chart_title,
            R.string.reports_performance_chart_title,
            -1,
            KPIValueHandleEnum.IMPRESSIONS,
            Type.COLUMN,
            Constants.PRIMARY_COLOR,
            Constants.SECONDARY_COLOR, 0, "", "",
            "overall performance"),

    INDIGO_PRINT_VOLUME(Constants.PRINT_VOLUME,
            BusinessUnitEnum.INDIGO_PRESS,
            R.string.indigo_kpi_week_parameter_volume,
            R.string.indigo_kpi_week_parameter_volume,
            -1,
            KPIValueHandleEnum.IMPRESSIONS,
            Type.COLUMN,
            Constants.PRIMARY_COLOR,
            Constants.SECONDARY_COLOR, 1, "", "",
            "print volume"),

    INDIGO_IMPRESSIONS("impressions",
            BusinessUnitEnum.INDIGO_PRESS,
            R.string.indigo_kpi_week_parameter_impression,
            R.string.indigo_kpi_week_parameter_impression,
            -1,
            KPIValueHandleEnum.IMPRESSIONS,
            Type.COLUMN,
            Constants.PRIMARY_COLOR,
            Constants.SECONDARY_COLOR, 0, "", "", "impressions"),

    INDIGO_EPM("epm",
            BusinessUnitEnum.INDIGO_PRESS,
            R.string.indigo_kpi_week_parameter_epm,
            R.string.indigo_kpi_week_parameter_epm,
            -1,
            KPIValueHandleEnum.EPM,
            Type.COLUMN,
            Constants.PRIMARY_COLOR,
            Constants.SECONDARY_COLOR, 1, "", "",
            "epm"),

    INDIGO_SHEETS("sheets",
            BusinessUnitEnum.INDIGO_PRESS,
            R.string.indigo_kpi_week_parameter_sheets,
            R.string.indigo_kpi_week_parameter_sheets,
            -1,
            KPIValueHandleEnum.SHEET,
            Type.COLUMN,
            Constants.PRIMARY_COLOR,
            Constants.SECONDARY_COLOR, 2, "", "",
            "sheets"),

    INDIGO_LM("LM",
            BusinessUnitEnum.INDIGO_PRESS,
            R.string.indigo_kpi_week_parameter_lm,
            R.string.indigo_kpi_week_parameter_lm,
            -1,
            KPIValueHandleEnum.METER,
            Type.COLUMN,
            Constants.PRIMARY_COLOR,
            Constants.SECONDARY_COLOR, 3, "", "",
            "lm"),

    INDIGO_AVAILABILITY("Availability",
            BusinessUnitEnum.INDIGO_PRESS,
            R.string.indigo_kpi_week_parameter_availability,
            R.string.indigo_kpi_week_parameter_availability,
            R.string.kpi_credit_out_of_up_time,
            KPIValueHandleEnum.PERCENT,
            Type.SPLINE,
            Constants.PRIMARY_COLOR,
            Constants.SECONDARY_COLOR, 2, "", "",
            "availability"),

    INDIGO_AVAILABILITY_AGGREGATE("Distribution of up time",
            BusinessUnitEnum.INDIGO_PRESS,
            R.string.indigo_kpi_week_parameter_distribution_of_up_time,
            R.string.indigo_kpi_week_parameter_distribution_of_up_time,
            R.string.kpi_credit_out_of_up_time,
            KPIValueHandleEnum.PERCENT,
            Type.SPLINE,
            Constants.PRIMARY_COLOR,
            Constants.SECONDARY_COLOR, 3, "", "",
            "distribution of up time"),

    INDIGO_PRINTING_TIME("Print time",
            BusinessUnitEnum.INDIGO_PRESS,
            R.string.indigo_kpi_week_parameter_print_time,
            R.string.indigo_kpi_week_parameter_print,
            R.string.kpi_credit_out_of_up_time,
            KPIValueHandleEnum.PERCENT,
            Type.SPLINE,
            "#5bb254",
            "#5bb254", 4, "", "",
            "printing time"),
    INDIGO_NON_PRINTING_TIME("Not-printing time",
            BusinessUnitEnum.INDIGO_PRESS,
            R.string.indigo_kpi_week_parameter_non_printing_time,
            R.string.indigo_kpi_week_parameter_non_printing,
            R.string.kpi_credit_out_of_up_time,
            KPIValueHandleEnum.PERCENT,
            Type.SPLINE,
            "#87c782",
            "#87c782", 5, "", "",
            "non printing time"),
    INDIGO_FAILURE_RECOVERY_TIME("Failures recovery time",
            BusinessUnitEnum.INDIGO_PRESS,
            R.string.indigo_kpi_week_parameter_failures_recovery_time,
            R.string.indigo_kpi_week_parameter_failures_recovery,
            R.string.kpi_credit_out_of_up_time,
            KPIValueHandleEnum.PERCENT,
            Type.SPLINE,
            "#f7844f",
            "#f7844f", 6, "", "",
            "failures recovery time"),
    INDIGO_JAMS_RECOVERY_TIME("Jams recovery time",
            BusinessUnitEnum.INDIGO_PRESS,
            R.string.indigo_kpi_week_parameter_jams_recovery_time,
            R.string.indigo_kpi_week_parameter_jams_recovery,
            R.string.kpi_credit_out_of_up_time,
            KPIValueHandleEnum.PERCENT,
            Type.SPLINE,
            "#911e5a",
            "#911e5a", 7, "", "",
            "jams recovery time"),
    INDIGO_RESTARTS_TIME("Restarts time",
            BusinessUnitEnum.INDIGO_PRESS,
            R.string.indigo_kpi_week_parameter_restarts_time,
            R.string.indigo_kpi_week_parameter_restarts,
            R.string.kpi_credit_out_of_up_time,
            KPIValueHandleEnum.PERCENT,
            Type.SPLINE,
            "#f15c80",
            "#f15c80", 8, "", "",
            "restarts time"),
    INDIGO_SUPPLIES_TIME("Supplies time",
            BusinessUnitEnum.INDIGO_PRESS,
            R.string.indigo_kpi_week_parameter_supplies_time,
            R.string.indigo_kpi_week_parameter_availability_supplies,
            R.string.kpi_credit_out_of_up_time,
            KPIValueHandleEnum.PERCENT,
            Type.SPLINE,
            "#8085e9",
            "#8085e9", 9, "", "",
            "supplies time"),
    INDIGO_UP_TIME("up time",
            BusinessUnitEnum.INDIGO_PRESS,
            R.string.unknown_value,
            R.string.unknown_value,
            R.string.kpi_credit_out_of_up_time,
            KPIValueHandleEnum.PERCENT,
            Type.SPLINE,
            Constants.PRIMARY_COLOR,
            Constants.SECONDARY_COLOR, 10, "", "",
            "up time"),

    INDIGO_TECHNICAL_ISSUES("Technical issues",
            BusinessUnitEnum.INDIGO_PRESS,
            R.string.indigo_kpi_week_parameter_technical,
            R.string.indigo_kpi_week_parameter_technical,
            R.string.kpi_credit_out_of_guidelines,
            KPIValueHandleEnum.PERCENT,
            Type.SPLINE,
            Constants.PRIMARY_COLOR,
            Constants.SECONDARY_COLOR, 3, "", "",
            "technical issues"),

    INDIGO_FAILURES(KpiBreakdownDataManager.SUB_TAG + "Failures",
            BusinessUnitEnum.INDIGO_PRESS,
            R.string.indigo_kpi_week_failures,
            R.string.indigo_kpi_week_failures,
            R.string.kpi_credit_rate_per_million_imp,
            KPIValueHandleEnum.FAILURE,
            Type.COLUMN,
            Constants.PRIMARY_COLOR,
            Constants.SECONDARY_COLOR, 4, "failures_rate",
            "failures_count",
            "failures"),

    INDIGO_JAMS(KpiBreakdownDataManager.SUB_TAG + "Jams",
            BusinessUnitEnum.INDIGO_PRESS,
            R.string.indigo_kpi_week_jams,
            R.string.indigo_kpi_week_jams,
            R.string.kpi_credit_rate_per_million_imp,
            KPIValueHandleEnum.JAM,
            Type.COLUMN,
            Constants.PRIMARY_COLOR,
            Constants.SECONDARY_COLOR, 5, "jams_rate",
            "jams_count",
            "jams"),

    INDIGO_RESTART("Restarts",
            BusinessUnitEnum.INDIGO_PRESS,
            R.string.indigo_kpi_week_restarts,
            R.string.indigo_kpi_week_restarts,
            R.string.kpi_credit_out_of_guidelines,
            KPIValueHandleEnum.PERCENT,
            Type.SPLINE,
            Constants.PRIMARY_COLOR,
            Constants.SECONDARY_COLOR, 4, "", "restarts_count",
            "restarts"),

    INDIGO_RESTART_OCCURRENCES(KpiBreakdownDataManager.SUB_TAG + "Restarts",
            BusinessUnitEnum.INDIGO_PRESS,
            R.string.indigo_kpi_week_restarts_occurrences,
            R.string.indigo_kpi_week_restarts_occurrences,
            R.string.kpi_credit_rate_per_million_imp,
            KPIValueHandleEnum.RESTARTS,
            Type.COLUMN,
            Constants.PRIMARY_COLOR,
            Constants.SECONDARY_COLOR, 5, "restarts_rate",
            "restarts_count",
            "restarts (occurrences)"),

    INDIGO_SUPPLIES_LIFESPAN("Supplies lifespan",
            BusinessUnitEnum.INDIGO_PRESS,
            R.string.indigo_kpi_week_parameter_supplies,
            R.string.indigo_kpi_week_parameter_supplies,
            R.string.kpi_credit_out_of_guidelines,
            KPIValueHandleEnum.PERCENT,
            Type.SPLINE,
            Constants.PRIMARY_COLOR,
            Constants.SECONDARY_COLOR, 5, "", "",
            "supplies lifespan"),

    INDIGO_PIP_REPLACEMENTS(KpiBreakdownDataManager.SUB_TAG + "Pip",
            BusinessUnitEnum.INDIGO_PRESS,
            R.string.indigo_kpi_week_pip,
            R.string.indigo_kpi_week_pip,
            R.string.kpi_credit_average_lifespan,
            KPIValueHandleEnum.IMPRESSIONS,
            Type.COLUMN,
            Constants.PRIMARY_COLOR,
            Constants.SECONDARY_COLOR, 6, "pip_average_lifespan", "pip_average_lifespan",
            "pip"),

    INDIGO_BLANKET_REPLACEMENTS(KpiBreakdownDataManager.SUB_TAG + "Blanket",
            BusinessUnitEnum.INDIGO_PRESS,
            R.string.indigo_kpi_week_blanket,
            R.string.indigo_kpi_week_blanket,
            R.string.kpi_credit_average_lifespan,
            KPIValueHandleEnum.IMPRESSIONS,
            Type.COLUMN,
            Constants.PRIMARY_COLOR,
            Constants.SECONDARY_COLOR, 7, "blanket_average_lifespan", "blanket_average_lifespan",
            "blanket"),

    LATEX_OVERALL(Constants.OVERALL_PERFORMANCE,
            BusinessUnitEnum.LATEX_PRINTER,
            R.string.reports_performance_chart_title,
            R.string.reports_performance_chart_title,
            -1,
            KPIValueHandleEnum.IMPRESSIONS,
            Type.COLUMN,
            Constants.PRIMARY_COLOR,
            Constants.SECONDARY_COLOR, 0, "", "", "overall performance"),

    LATEX_PRINT_VOLUME(Constants.PRINT_VOLUME,
            BusinessUnitEnum.LATEX_PRINTER,
            R.string.indigo_kpi_week_parameter_volume,
            R.string.indigo_kpi_week_parameter_volume,
            -1,
            KPIValueHandleEnum.SQM,
            Type.COLUMN,
            Constants.PRIMARY_COLOR,
            Constants.SECONDARY_COLOR, 1, "", "",
            "print volume"),

    LATEX_UTILIZATION("Utilization",
            BusinessUnitEnum.LATEX_PRINTER,
            R.string.latex_kpi_week_utilization,
            R.string.latex_kpi_week_utilization,
            R.string.kpi_credit_usage_vs_idle,
            KPIValueHandleEnum.PERCENT,
            Type.SPLINE,
            Constants.PRIMARY_COLOR,
            Constants.SECONDARY_COLOR, 2, "", "",
            "utilization"),

    LATEX_MAINTENANCE("Maintenance",
            BusinessUnitEnum.LATEX_PRINTER,
            R.string.pwp_kpi_week_parameter_maintenance,
            R.string.pwp_kpi_week_parameter_maintenance,
            -1,
            KPIValueHandleEnum.OVER_DUE_TASK,
            Type.COLUMN,
            Constants.PRIMARY_COLOR,
            Constants.SECONDARY_COLOR, 3, "", "",
            "maintenace"),

    SCITEX_OVERALL(Constants.OVERALL_PERFORMANCE,
            BusinessUnitEnum.SCITEX_PRESS,
            R.string.reports_performance_chart_title,
            R.string.reports_performance_chart_title,
            -1,
            KPIValueHandleEnum.IMPRESSIONS,
            Type.COLUMN,
            Constants.PRIMARY_COLOR,
            Constants.SECONDARY_COLOR, 0, "", "",
            "overall performance"),

    SCITEX_AVAILABILITY("Availability",
            BusinessUnitEnum.SCITEX_PRESS,
            R.string.indigo_kpi_week_parameter_availability,
            R.string.indigo_kpi_week_parameter_availability,
            -1,
            KPIValueHandleEnum.HOURS,
            Type.COLUMN,
            Constants.PRIMARY_COLOR,
            Constants.SECONDARY_COLOR, 1, "", "", "availability"),

    SCITEX_PRINT_VOLUME(Constants.PRINT_VOLUME,
            BusinessUnitEnum.SCITEX_PRESS,
            R.string.indigo_kpi_week_parameter_volume,
            R.string.indigo_kpi_week_parameter_volume,
            -1,
            KPIValueHandleEnum.SQM,
            Type.COLUMN,
            Constants.PRIMARY_COLOR,
            Constants.SECONDARY_COLOR, 2, "", "",
            "print volume"),

    SCITEX_PAPER_JAMS("Paper jams",
            BusinessUnitEnum.SCITEX_PRESS,
            R.string.scitex_kpi_week_paper_jams,
            R.string.scitex_kpi_week_paper_jams,
            -1,
            KPIValueHandleEnum.PAPER_JAMS,
            Type.COLUMN,
            Constants.PRIMARY_COLOR,
            Constants.SECONDARY_COLOR, 3, "", "",
            "paper jams"),

    PWP_OVERALL(Constants.OVERALL_PERFORMANCE,
            BusinessUnitEnum.IHPS_PRESS,
            R.string.reports_performance_chart_title,
            R.string.reports_performance_chart_title,
            -1,
            KPIValueHandleEnum.IMPRESSIONS,
            Type.COLUMN,
            Constants.PRIMARY_COLOR,
            Constants.SECONDARY_COLOR, 0, "", "", "overall performance"),

    PWP_PRINT_VOLUME(Constants.PRINT_VOLUME,
            BusinessUnitEnum.IHPS_PRESS,
            R.string.pwp_kpi_week_parameter_print_volume,
            R.string.pwp_kpi_week_parameter_print_volume,
            -1,
            KPIValueHandleEnum.PAGES,
            Type.COLUMN,
            Constants.PRIMARY_COLOR,
            Constants.SECONDARY_COLOR, 1, "", "",
            "print volume"),

    PWP_PAGES_PER_RESTART("Pages perrestart",
            BusinessUnitEnum.IHPS_PRESS,
            R.string.pwp_kpi_week_pages_perrestart,
            R.string.pwp_kpi_week_pages_perrestart,
            -1,
            KPIValueHandleEnum.PAGES,
            Type.COLUMN,
            Constants.PRIMARY_COLOR,
            Constants.SECONDARY_COLOR, 2, "", "",
            "pages per restart"),

    PWP_OPERATION_EFFICIENCY("Operating efficiency",
            BusinessUnitEnum.IHPS_PRESS,
            R.string.pwp_kpi_week_operating_efficiency,
            R.string.pwp_kpi_week_operating_efficiency,
            R.string.kpi_credit_out_of_guidelines,
            KPIValueHandleEnum.PERCENT,
            Type.SPLINE,
            Constants.PRIMARY_COLOR,
            Constants.SECONDARY_COLOR, 3, "", "",
            "operating efficienct"),

    PWP_MAINTENANCE("Maintenance",
            BusinessUnitEnum.IHPS_PRESS,
            R.string.pwp_kpi_week_parameter_maintenance,
            R.string.pwp_kpi_week_parameter_maintenance,
            R.string.kpi_credit_out_of_guidelines,
            KPIValueHandleEnum.PERCENT,
            Type.SPLINE,
            Constants.PRIMARY_COLOR,
            Constants.SECONDARY_COLOR, 4, "", "",
            "maintenance"),

    PWP_SYSTEM_HEALTH("System health",
            BusinessUnitEnum.IHPS_PRESS,
            R.string.pwp_kpi_week_system_health,
            R.string.pwp_kpi_week_system_health,
            R.string.kpi_credit_out_of_guidelines,
            KPIValueHandleEnum.PERCENT,
            Type.SPLINE,
            Constants.PRIMARY_COLOR,
            Constants.SECONDARY_COLOR, 5, "", "", "system health"),

    UNKNOWN("",
            BusinessUnitEnum.UNKNOWN,
            R.string.unknown_value,
            R.string.unknown_value,
            -1,
            KPIValueHandleEnum.EMPTY,
            Type.SPLINE,
            Constants.PRIMARY_COLOR,
            Constants.SECONDARY_COLOR, 0, "", "", "unknown");

    static {
        INDIGO_PRINT_VOLUME.breakdownEnumList.add(INDIGO_IMPRESSIONS);
        INDIGO_PRINT_VOLUME.breakdownEnumList.add(INDIGO_EPM);

        INDIGO_AVAILABILITY.breakdownEnumList.add(INDIGO_AVAILABILITY);
        INDIGO_AVAILABILITY.breakdownEnumList.add(INDIGO_PRINTING_TIME);
        INDIGO_AVAILABILITY.breakdownEnumList.add(INDIGO_NON_PRINTING_TIME);
        INDIGO_AVAILABILITY.breakdownEnumList.add(INDIGO_RESTARTS_TIME);
        INDIGO_AVAILABILITY.breakdownEnumList.add(INDIGO_FAILURE_RECOVERY_TIME);
        INDIGO_AVAILABILITY.breakdownEnumList.add(INDIGO_JAMS_RECOVERY_TIME);
        INDIGO_AVAILABILITY.breakdownEnumList.add(INDIGO_SUPPLIES_TIME);

        INDIGO_RESTART.breakdownEnumList.add(INDIGO_RESTART);
        INDIGO_RESTART.breakdownEnumList.add(INDIGO_RESTART_OCCURRENCES);

        INDIGO_TECHNICAL_ISSUES.breakdownEnumList.add(INDIGO_TECHNICAL_ISSUES);
        INDIGO_TECHNICAL_ISSUES.breakdownEnumList.add(INDIGO_FAILURES);
        INDIGO_TECHNICAL_ISSUES.breakdownEnumList.add(INDIGO_JAMS);

        INDIGO_SUPPLIES_LIFESPAN.breakdownEnumList.add(INDIGO_SUPPLIES_LIFESPAN);
        INDIGO_SUPPLIES_LIFESPAN.breakdownEnumList.add(INDIGO_BLANKET_REPLACEMENTS);
        INDIGO_SUPPLIES_LIFESPAN.breakdownEnumList.add(INDIGO_PIP_REPLACEMENTS);

        INDIGO_OVERALL.breakdownEnumList.add(INDIGO_AVAILABILITY);
        INDIGO_OVERALL.breakdownEnumList.add(INDIGO_PRINT_VOLUME);
        INDIGO_OVERALL.breakdownEnumList.add(INDIGO_TECHNICAL_ISSUES);
        INDIGO_OVERALL.breakdownEnumList.add(INDIGO_RESTART);
        INDIGO_OVERALL.breakdownEnumList.add(INDIGO_SUPPLIES_LIFESPAN);

        LATEX_OVERALL.breakdownEnumList.add(LATEX_PRINT_VOLUME);
        LATEX_OVERALL.breakdownEnumList.add(LATEX_MAINTENANCE);
        LATEX_OVERALL.breakdownEnumList.add(LATEX_UTILIZATION);

        SCITEX_OVERALL.breakdownEnumList.add(SCITEX_AVAILABILITY);
        SCITEX_OVERALL.breakdownEnumList.add(SCITEX_PAPER_JAMS);
        SCITEX_OVERALL.breakdownEnumList.add(SCITEX_PRINT_VOLUME);

        PWP_OVERALL.breakdownEnumList.add(PWP_MAINTENANCE);
        PWP_OVERALL.breakdownEnumList.add(PWP_OPERATION_EFFICIENCY);
        PWP_OVERALL.breakdownEnumList.add(PWP_PAGES_PER_RESTART);
        PWP_OVERALL.breakdownEnumList.add(PWP_PRINT_VOLUME);
        PWP_OVERALL.breakdownEnumList.add(PWP_SYSTEM_HEALTH);

        INDIGO_OVERALL.parentKpi = INDIGO_OVERALL;

        INDIGO_PRINT_VOLUME.parentKpi = INDIGO_PRINT_VOLUME;
        INDIGO_IMPRESSIONS.parentKpi = INDIGO_PRINT_VOLUME;
        INDIGO_EPM.parentKpi = INDIGO_PRINT_VOLUME;
        INDIGO_SHEETS.parentKpi = INDIGO_PRINT_VOLUME;
        INDIGO_LM.parentKpi = INDIGO_PRINT_VOLUME;

        INDIGO_AVAILABILITY.parentKpi = INDIGO_AVAILABILITY;
        INDIGO_AVAILABILITY_AGGREGATE.parentKpi = INDIGO_AVAILABILITY;
        INDIGO_PRINTING_TIME.parentKpi = INDIGO_AVAILABILITY;
        INDIGO_NON_PRINTING_TIME.parentKpi = INDIGO_AVAILABILITY;
        INDIGO_FAILURE_RECOVERY_TIME.parentKpi = INDIGO_AVAILABILITY;
        INDIGO_JAMS_RECOVERY_TIME.parentKpi = INDIGO_AVAILABILITY;
        INDIGO_RESTARTS_TIME.parentKpi = INDIGO_AVAILABILITY;
        INDIGO_SUPPLIES_TIME.parentKpi = INDIGO_AVAILABILITY;
        INDIGO_UP_TIME.parentKpi = INDIGO_AVAILABILITY;

        INDIGO_TECHNICAL_ISSUES.parentKpi = INDIGO_TECHNICAL_ISSUES;
        INDIGO_FAILURES.parentKpi = INDIGO_TECHNICAL_ISSUES;
        INDIGO_JAMS.parentKpi = INDIGO_TECHNICAL_ISSUES;

        INDIGO_RESTART.parentKpi = INDIGO_RESTART;
        INDIGO_RESTART_OCCURRENCES.parentKpi = INDIGO_RESTART;

        INDIGO_SUPPLIES_LIFESPAN.parentKpi = INDIGO_SUPPLIES_LIFESPAN;
        INDIGO_PIP_REPLACEMENTS.parentKpi = INDIGO_SUPPLIES_LIFESPAN;
        INDIGO_BLANKET_REPLACEMENTS.parentKpi = INDIGO_SUPPLIES_LIFESPAN;

        LATEX_OVERALL.parentKpi = LATEX_OVERALL;
        LATEX_PRINT_VOLUME.parentKpi = LATEX_PRINT_VOLUME;
        LATEX_UTILIZATION.parentKpi = LATEX_UTILIZATION;
        LATEX_MAINTENANCE.parentKpi = LATEX_MAINTENANCE;

        SCITEX_OVERALL.parentKpi = SCITEX_OVERALL;
        SCITEX_AVAILABILITY.parentKpi = SCITEX_AVAILABILITY;
        SCITEX_PRINT_VOLUME.parentKpi = SCITEX_PRINT_VOLUME;
        SCITEX_PAPER_JAMS.parentKpi = SCITEX_PAPER_JAMS;

        PWP_OVERALL.parentKpi = PWP_OVERALL;
        PWP_PRINT_VOLUME.parentKpi = PWP_PRINT_VOLUME;
        PWP_PAGES_PER_RESTART.parentKpi = PWP_PAGES_PER_RESTART;
        PWP_OPERATION_EFFICIENCY.parentKpi = PWP_OPERATION_EFFICIENCY;
        PWP_MAINTENANCE.parentKpi = PWP_MAINTENANCE;
        PWP_SYSTEM_HEALTH.parentKpi = PWP_SYSTEM_HEALTH;

        UNKNOWN.parentKpi = UNKNOWN;
    }

    private List<KpiBreakdownEnum> breakdownEnumList;
    private KpiBreakdownEnum parentKpi;
    private Type type;
    private String key;
    private BusinessUnitEnum businessUnitEnum;
    private int nameResource;
    private int shortNameResource;
    private int creditResource;
    private KPIValueHandleEnum valueHandleEnum;
    private String primaryColor;
    private String secondaryColor;
    private int sortOrder;
    private String propertyTag;
    private String dailySpotPropertyTag;
    private String deepLinkTag;

    KpiBreakdownEnum(String key, BusinessUnitEnum businessUnitEnum, int nameResource, int shortNameResource,
                     int creditResource, KPIValueHandleEnum valueHandleEnum, Type type, String primaryColor,
                     String secondaryColor, int sortOrder, String propertyTag, String dailySpotPropertyTag,
                     String deepLinkTag) {
        breakdownEnumList = new ArrayList<>();
        this.type = type;
        this.businessUnitEnum = businessUnitEnum;
        this.key = key;
        this.nameResource = nameResource;
        this.shortNameResource = shortNameResource;
        this.creditResource = creditResource;
        this.valueHandleEnum = valueHandleEnum;
        this.primaryColor = primaryColor;
        this.secondaryColor = secondaryColor;
        this.sortOrder = sortOrder;
        this.propertyTag = propertyTag;
        this.deepLinkTag = deepLinkTag;
        this.dailySpotPropertyTag = dailySpotPropertyTag;
    }

    public List<KpiBreakdownEnum> getBreakdownEnumList() {
        return breakdownEnumList;
    }

    public Type getType() {
        return type;
    }

    public String getKey() {
        return key;
    }

    public BusinessUnitEnum getBusinessUnitEnum() {
        return businessUnitEnum;
    }

    public int getNameResource() {
        return nameResource;
    }

    public int getCreditResource() {
        return creditResource;
    }

    public KPIValueHandleEnum getValueHandleEnum() {
        return valueHandleEnum;
    }

    public String getPrimaryColor() {
        return primaryColor;
    }

    public String getSecondaryColor() {
        return secondaryColor;
    }

    public int getSortOrder() {
        return sortOrder;
    }

    public String getPropertyTag() {
        return propertyTag;
    }

    public String getDailySpotPropertyTag() {
        return dailySpotPropertyTag;
    }

    public int getShortNameResource() {
        return shortNameResource;
    }

    public KpiBreakdownEnum getParentKpi() {
        return parentKpi;
    }

    private void setParentKpi(KpiBreakdownEnum parentKpi) {
        this.parentKpi = parentKpi;
    }

    public boolean isOverAll() {
        return this == INDIGO_OVERALL || this == PWP_OVERALL || this == LATEX_OVERALL || this == SCITEX_OVERALL;
    }

    public enum Type {
        COLUMN("column"), SPLINE("areaspline");

        private String htmlTag;

        Type(String htmlTag) {
            this.htmlTag = htmlTag;
        }

        public String getHtmlTag() {
            return htmlTag;
        }
    }

    public static KpiBreakdownEnum fromDeepLink(String key, BusinessUnitEnum businessUnitEnum) {
        return from(key, businessUnitEnum, true);
    }

    public static KpiBreakdownEnum from(String key, BusinessUnitEnum businessUnitEnum) {
        return from(key, businessUnitEnum, false);
    }

    public String getDeepLinkTag() {
        return deepLinkTag;
    }

    private static KpiBreakdownEnum from(String key, BusinessUnitEnum businessUnitEnum, boolean fromDeepLink) {
        if (businessUnitEnum != null) {
            if (TextUtils.isEmpty(key)) {
                switch (businessUnitEnum) {
                    case INDIGO_PRESS:
                        return INDIGO_OVERALL;
                    case SCITEX_PRESS:
                        return SCITEX_OVERALL;
                    case LATEX_PRINTER:
                        return LATEX_OVERALL;
                    case IHPS_PRESS:
                        return PWP_OVERALL;
                    default:
                        break;
                }
            } else {
                for (KpiBreakdownEnum kpiBreakdownEnum : KpiBreakdownEnum.values()) {
                    if (kpiBreakdownEnum.getBusinessUnitEnum() == businessUnitEnum &&
                            ((!fromDeepLink && kpiBreakdownEnum.getKey().equalsIgnoreCase(key)) ||
                                    (fromDeepLink && kpiBreakdownEnum.getDeepLinkTag().equalsIgnoreCase(key)))) {
                        return kpiBreakdownEnum;
                    }
                }
            }
        }
        return UNKNOWN;
    }

    public static KpiBreakdownEnum getFirst(BusinessUnitEnum businessUnitEnum) {
        if (businessUnitEnum != null) {
            List<KpiBreakdownEnum> children = from(null, businessUnitEnum).breakdownEnumList;

            if (children != null && children.size() > 0) {
                KpiBreakdownEnum firstChild = children.get(0);
                for (KpiBreakdownEnum breakdownEnum : children) {
                    if (firstChild.sortOrder > breakdownEnum.sortOrder) {
                        firstChild = breakdownEnum;
                    }
                }
                return firstChild;
            }
        }

        return UNKNOWN;
    }

    private static class Constants {
        private static final String PRINT_VOLUME = "Print volume";
        private static final String OVERALL_PERFORMANCE = "Overall Performance";
        private static final String PRIMARY_COLOR = "#303F9F";
        private static final String SECONDARY_COLOR = "#0693d2";
    }
}
