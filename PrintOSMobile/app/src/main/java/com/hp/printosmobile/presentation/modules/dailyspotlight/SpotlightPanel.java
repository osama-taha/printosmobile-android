package com.hp.printosmobile.presentation.modules.dailyspotlight;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Interpolator;

import com.gigamole.infinitecycleviewpager.HorizontalInfiniteCycleViewPager;
import com.hp.printosmobile.Analytics;
import com.hp.printosmobile.AnswersSdk;
import com.hp.printosmobile.Constants;
import com.hp.printosmobile.R;
import com.hp.printosmobile.presentation.modules.home.Panel;
import com.hp.printosmobile.presentation.modules.reportkpibreakdown.KpiBreakdownEnum;
import com.hp.printosmobile.presentation.modules.shared.DeepLinkUtils;
import com.hp.printosmobile.presentation.modules.shared.PanelShareCallbacks;
import com.hp.printosmobile.utils.FileUtils;
import com.hp.printosmobilelib.ui.widgets.panel.PanelView;
import com.hp.printosmobilelib.ui.widgets.panel.SharableObject;

import butterknife.Bind;

/**
 * Created by Anwar Asbah
 */
public class SpotlightPanel extends PanelView<Object> implements SharableObject, SpotlightPagerAdapter.DailySpotlightPagerAdapterCallback {

    public static final String TAG = SpotlightPanel.class.getSimpleName();
    private static final long LOOPING_PERIOD = 5000;
    private static final long FIRST_REQUEST_DELAY = 300;
    private static final int SCROLL_DURATION = 200;
    private static final int PAGE_LAYER_TYPE = 2;

    @Bind(R.id.spotlight_pager)
    HorizontalInfiniteCycleViewPager spotlightPager;
    @Bind(R.id.button_previous)
    View prevButton;
    @Bind(R.id.button_next)
    View nextButton;

    SpotlightPagerAdapter spotlightPagerAdapter;
    DailySpotlightPanelCallback callback;
    ViewPager.OnPageChangeListener onPageChangeListener;

    Handler handler;
    Runnable runnable;

    public SpotlightPanel(Context context) {
        super(context);
    }

    public SpotlightPanel(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SpotlightPanel(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void addCallback(DailySpotlightPanelCallback callback) {
        this.callback = callback;
    }

    @Override
    public Spannable getTitleSpannable() {
        return new SpannableString(getContext().getString(R.string.spotlight_panel_title));
    }

    @Override
    public int getContentView() {
        return R.layout.panel_daily_spotlight;
    }

    @Override
    public void bindViews() {
        if (onPageChangeListener != null) {
            spotlightPager.removeOnPageChangeListener(onPageChangeListener);
        }

        onPageChangeListener = new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageScrolled(int i, float v, int i1) {
            }

            @Override
            public void onPageSelected(int i) {
                if (spotlightPagerAdapter != null) {
                    SpotlightItemFragment fragment = getCurrentFragment();
                    if (fragment != null) {
                        boolean hasData = fragment.hasData();
                        if (hasData) {
                            fragment.displayView();
                        } else {
                            fragment.loadData();
                        }
                    }
                }
                setLooping();
            }

            @Override
            public void onPageScrollStateChanged(int i) {
                if (i == ViewPager.SCROLL_STATE_DRAGGING) {
                    Analytics.sendEvent(Analytics.DAILY_SPOTLIGHT_PAGE_CHANGE_ACTION,
                            Analytics.DAILY_SPOTLIGHT_PAGE_CHANGE_SWIPE_LABEL);

                    if (spotlightPager.getCurrentItem() == spotlightPagerAdapter.getCount() - 1) {
                        onNextClicked();
                    }
                }
            }
        };

        prevButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Analytics.sendEvent(Analytics.DAILY_SPOTLIGHT_PAGE_CHANGE_ACTION,
                        Analytics.DAILY_SPOTLIGHT_PAGE_CHANGE_PREV_LABEL);
                onPreviousClicked();
            }
        });

        nextButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Analytics.sendEvent(Analytics.DAILY_SPOTLIGHT_PAGE_CHANGE_ACTION,
                        Analytics.DAILY_SPOTLIGHT_PAGE_CHANGE_NEXT_LABEL);
                onNextClicked();
            }
        });

        spotlightPager.addOnPageChangeListener(onPageChangeListener);
        spotlightPager.setMaxPageScale(1);
        spotlightPager.setMinPageScale(1);

        spotlightPager.setInterpolator(new Interpolator() {
            @Override
            public float getInterpolation(float v) {
                return v;
            }
        });

        spotlightPager.setPageTransformer(true, new ViewPager.PageTransformer() {
            @Override
            public void transformPage(@NonNull View view, float v) {
                //DO NOTHING
            }
        }, PAGE_LAYER_TYPE);

        spotlightPager.setScrollDuration(SCROLL_DURATION);

        shareButton.setVisibility(VISIBLE);

        setLooping();
    }

    private void setLooping() {
        if (handler == null) {
            handler = new Handler(Looper.getMainLooper());
        }
        if (runnable == null) {
            runnable = new Runnable() {
                @Override
                public void run() {
                    onNextClicked();
                }
            };
        }
        handler.removeCallbacks(runnable);
        handler.postDelayed(runnable, LOOPING_PERIOD);
    }

    @Override
    public void updateViewModel(Object viewModels) {
        //do nothing
    }

    @Override
    protected boolean isValidViewModel() {
        return true;
    }

    protected String getEmptyCardText() {
        return getContext().getString(R.string.no_information_to_display);
    }

    @Override
    protected boolean hasSharingButton() {
        super.hasSharingButton();
        return true;
    }

    @Override
    protected void onShareClicked() {
        super.onShareClicked();
        if (callback != null) {
            callback.onShareButtonClicked(Panel.DAILY_SPOTLIGHT);
        }
    }

    @Override
    public void showLoading() {
        //do nothing
    }

    @Override
    public void sharePanel() {

        SpotlightItemFragment fragment = getCurrentFragment();
        if (callback != null && fragment != null && fragment.hasData()) {

            lockShareButton(true);

            shareButton.setVisibility(GONE);
            prevButton.setVisibility(GONE);
            nextButton.setVisibility(GONE);
            Bitmap panelBitmap = FileUtils.getScreenShot(this);
            shareButton.setVisibility(VISIBLE);
            prevButton.setVisibility(VISIBLE);
            nextButton.setVisibility(VISIBLE);

            if (panelBitmap != null) {
                final Uri storageUri = FileUtils.store(getContext(), panelBitmap, PANEL_SCREEN_SHOT_FILE_NAME);

                final String shareTitle = fragment.getShareSubject();
                DailySpotlightEnum dailySpotlightEnum = fragment.getDailySpotlightEnum();

                DeepLinkUtils.getShareBody(getContext(),
                        Constants.UNIVERSAL_LINK_SCREEN_KPI_BREAKDOWN,
                        dailySpotlightEnum.getRootBreakdownEnum().getDeepLinkTag().toLowerCase(),
                        null,
                        dailySpotlightEnum.getRootBreakdownEnum().getParentKpi().getDeepLinkTag().toLowerCase(), false,
                        new DeepLinkUtils.BranchIOCallback() {
                            @Override
                            public void onLinkCreated(String link) {
                                callback.onScreenshotCreated(Panel.HISTOGRAM, storageUri,
                                        shareTitle,
                                        link,
                                        SpotlightPanel.this);
                                lockShareButton(false);
                            }
                        });
            }
        }
    }

    @Override
    public String getShareAction() {
        return AnswersSdk.SHARE_CONTENT_TYPE_DAILY_SPOTLIGHT;
    }

    @Override
    public String getPanelTag() {
        return Panel.DAILY_SPOTLIGHT.getDeepLinkTag();
    }

    public void onPreviousClicked() {
        if (spotlightPager != null && spotlightPagerAdapter != null) {
            int prev = spotlightPager.getRealItem() - 1;
            spotlightPager.setCurrentItem(prev);
        }
    }

    public void onNextClicked() {
        if (spotlightPager != null && spotlightPagerAdapter != null) {
            spotlightPager.setCurrentItem((spotlightPager.getRealItem() + 1));
        }
    }

    public void requestData() {
        if (callback != null) {
            if (spotlightPagerAdapter == null) {
                spotlightPagerAdapter = new SpotlightPagerAdapter(callback.getFragmentManager(), this);
                spotlightPager.setAdapter(spotlightPagerAdapter);
            } else {
                spotlightPagerAdapter.clear();
                spotlightPager.notifyDataSetChanged();
            }

            spotlightPager.setOffscreenPageLimit(spotlightPagerAdapter.getCount());

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (onPageChangeListener != null) {
                        onPageChangeListener.onPageSelected(spotlightPager.getRealItem() + 1);
                    }
                }
            }, FIRST_REQUEST_DELAY);
        }
    }

    public void onItemClicked() {
        SpotlightItemFragment fragment = getCurrentFragment();
        if (fragment != null) {
            DailySpotlightEnum dailySpotlightEnum = fragment.getDailySpotlightEnum();
            callback.onMoreInfoClicked(dailySpotlightEnum.getRootBreakdownEnum());
            Analytics.sendEvent(Analytics.DAILY_SPOTLIGHT_MORE_INFO_EVENT, dailySpotlightEnum.name());
        }
    }

    @Override
    public void onItemClicked(DailySpotlightEnum dailySpotlightEnum) {
        callback.onMoreInfoClicked(dailySpotlightEnum.getRootBreakdownEnum());
        Analytics.sendEvent(Analytics.DAILY_SPOTLIGHT_MORE_INFO_EVENT, dailySpotlightEnum.name());
    }

    @Override
    public void onDataRetrieved() {
        //Do nothing
    }

    private SpotlightItemFragment getCurrentFragment() {
        if (spotlightPager != null && spotlightPagerAdapter != null) {
            int currentIndex = spotlightPager.getRealItem();
            if (currentIndex < 0 || currentIndex >= spotlightPagerAdapter.getCount()) {
                return null;
            }
            Fragment fragment = spotlightPagerAdapter.getItem(currentIndex);
            if (fragment instanceof SpotlightItemFragment) {
                return (SpotlightItemFragment) fragment;
            }
            return null;
        }
        return null;
    }

    @Override
    protected void onWindowVisibilityChanged(int visibility) {
        super.onWindowVisibilityChanged(visibility);
        if (visibility == VISIBLE) {
            setLooping();
        } else {
            if (handler != null && runnable != null) {
                handler.removeCallbacks(runnable);
            }
        }
    }

    @Override
    public View getCurrentView() {
        SpotlightItemFragment fragment = getCurrentFragment();
        if (fragment == null) {
            return this;
        }
        return fragment.getView();
    }

    public interface DailySpotlightPanelCallback extends PanelShareCallbacks, DeepLinkUtils.DeepLinkCallbacks {

        void onMoreInfoClicked(KpiBreakdownEnum kpi);

        FragmentManager getFragmentManager();
    }
}
