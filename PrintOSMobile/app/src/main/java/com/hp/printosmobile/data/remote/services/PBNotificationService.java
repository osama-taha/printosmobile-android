package com.hp.printosmobile.data.remote.services;

import com.hp.printosmobile.data.remote.ApiConstants;
import com.hp.printosmobile.data.remote.models.PBNotificationData;

import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Path;
import rx.Observable;

public interface PBNotificationService {

    @GET(ApiConstants.NOTIFICATIONS_API)
    Observable<Response<PBNotificationData>> getPBNotificationData(@Path("id") String id);

}
