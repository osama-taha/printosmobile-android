package com.hp.printosmobile.presentation.modules.insights.kz.kzfooter;

import com.hp.printosmobile.data.remote.models.kz.KZItem;

import java.util.List;

interface FavoritesView {

    void showEmptyView();

    void hideEmptyView();

    void showLoadingView();

    void hideLoadingView();

    void addResults(List<KZItem> knowledgeZoneItems);

    void clearResults();

    void hideResultsView();

    void showResultsView();

    void setEmptyText();

    void showErrorView();

    void hideErrorView();

    void showLoadingFooterView();

    void addFooterView();

    void removeFooterView();

    void showErrorFooterView();

    void showNoResultsFooterView();

    void openKnowledgeZoneItem(KZItem knowledgeZoneItem);

}
