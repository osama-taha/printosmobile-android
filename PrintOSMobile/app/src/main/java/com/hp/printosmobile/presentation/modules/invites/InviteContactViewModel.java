package com.hp.printosmobile.presentation.modules.invites;

import android.graphics.Bitmap;

import com.hp.printosmobile.R;

import java.io.Serializable;

/**
 * Created by Anwar Asbah on 11/2/2017.
 */
public class InviteContactViewModel implements Serializable {

    public InviteContactViewModel() {
    }

    public enum InviteStatus implements Serializable {
        TO_BE_INVITED("Revoked",
                R.drawable.blue_rounded_rect_frame,
                R.string.invites_fragment_invite,
                R.color.c62,
                -1),
        ON_BOARD("Accepted",
                R.drawable.rounded_green_bg,
                R.string.invites_fragment_on_board,
                android.R.color.white,
                R.drawable.btn_share_app),
        INVITED("pending",
                R.drawable.blue_rounded_rect_frame,
                R.string.invites_fragment_invited,
                R.color.c62,
                R.drawable.btn_invited),
        LOADING("",
                R.drawable.blue_rounded_rect_frame,
                R.string.invites_fragment_invited,
                android.R.color.white,
                -1),
        UNKNOWN("",
                R.drawable.rounded_green_bg,
                R.string.unknown_value,
                android.R.color.white,
                -1);

        String key;
        int drawable;
        int label;
        int textColor;
        int drawableLeft;

        InviteStatus(String key, int drawable, int label, int color, int drawableLeft) {
            this.drawable = drawable;
            this.label = label;
            this.textColor = color;
            this.key = key;
            this.drawableLeft = drawableLeft;
        }

        public static InviteStatus from(String inviteKey) {
            if (inviteKey == null) {
                return UNKNOWN;
            }
            for (InviteStatus inviteStatus : InviteStatus.values()) {
                if (inviteStatus.key.equalsIgnoreCase(inviteKey)) {
                    return inviteStatus;
                }
            }

            return UNKNOWN;
        }
    }

    private String name;
    private String id;
    private String email;
    private Bitmap photo;
    private InviteStatus inviteStatus = InviteStatus.LOADING;
    private int color;
    private String initials;
    private String inviteId;
    private boolean revokeEnabled;
    private boolean resendEnabled;
    private String roleId;
    private String roleType;
    private String language;
    private String phone;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Bitmap getPhoto() {
        return photo;
    }

    public void setPhoto(Bitmap photo) {
        this.photo = photo;
    }

    public InviteStatus getInviteStatus() {
        return inviteStatus;
    }

    public void setInviteStatus(InviteStatus inviteStatus) {
        this.inviteStatus = inviteStatus;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public String getInitials() {
        return initials;
    }

    public void setInitials(String initials) {
        this.initials = initials;
    }

    public String getInviteId() {
        return inviteId;
    }

    public void setInviteId(String inviteId) {
        this.inviteId = inviteId;
    }

    public boolean isRevokeEnabled() {
        return revokeEnabled;
    }

    public void setRevokeEnabled(boolean revokeEnabled) {
        this.revokeEnabled = revokeEnabled;
    }

    public boolean isResendEnabled() {
        return resendEnabled;
    }

    public void setResendEnabled(boolean resendEnabled) {
        this.resendEnabled = resendEnabled;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getRoleType() {
        return roleType;
    }

    public void setRoleType(String roleType) {
        this.roleType = roleType;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
