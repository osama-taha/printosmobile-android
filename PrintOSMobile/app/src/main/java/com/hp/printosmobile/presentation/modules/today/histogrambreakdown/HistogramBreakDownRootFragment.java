package com.hp.printosmobile.presentation.modules.today.histogrambreakdown;

import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;

import com.hp.printosmobile.R;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitEnum;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitViewModel;
import com.hp.printosmobile.presentation.modules.main.IMainFragment;
import com.hp.printosmobile.presentation.modules.todayhistogram.TodayHistogramViewModel;
import com.hp.printosmobile.utils.HPUIUtils;
import com.hp.printosmobilelib.ui.common.HPFragment;
import com.hp.printosmobilelib.ui.widgets.HPViewPager;
import com.hp.printosmobilelib.ui.widgets.panel.PanelView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;

/**
 * create by anwar asbah 4/24/2018
 */
public class HistogramBreakDownRootFragment extends HPFragment implements IMainFragment, TabLayout.OnTabSelectedListener, HistogramBreakdownFragment.HistogramBreakdownFragmentCallback {

    public enum HistogramResolution {
        DAILY(R.string.histogram_res_daily, "Day", -31),
        WEEKLY(R.string.histogram_res_weekly, "Week", -53),
        MONTHLY(R.string.histogram_res_monthly, "Month", -12);

        private int nameID;
        private String apiTag;
        private int apiOffset;

        HistogramResolution(int nameID, String apiTag, int apiOffset) {
            this.nameID = nameID;
            this.apiTag = apiTag;
            this.apiOffset = apiOffset;
        }

        public int getNameID() {
            return nameID;
        }

        public String getApiTag() {
            return apiTag;
        }

        public int getApiOffset() {
            return apiOffset;
        }

        public static HistogramResolution from(String name) {
            if (TextUtils.isEmpty(name)) {
                return DAILY;
            }

            for (HistogramResolution resolution : HistogramResolution.values()) {
                if (resolution.name().equals(name)) {
                    return resolution;
                }
            }

            return DAILY;
        }
    }

    private static final String HISTOGRAM_VIEW_MODEL_PARAM = "view_model_param";
    private static final String FOCUSABLE_PANEL_PARAM = "focusable_panel_param";
    private static final String RESOLUTION_PARAM = "resolution_param";

    private static final long DELAY = 200;

    @Bind(R.id.viewpager)
    HPViewPager viewPager;
    @Bind(R.id.sliding_tabs)
    TabLayout tabLayout;

    SimplePagerAdapter simplePagerAdapter;
    private TodayHistogramViewModel histogramViewModel;
    private String focusablePanel;
    private HistogramResolution resolution = HistogramResolution.DAILY;

    private HistogramBreakdownRootFragmentCallback callback;

    public static HistogramBreakDownRootFragment newInstance(TodayHistogramViewModel viewModel, String focusablePanel,
                                                             String resolution,
                                                             HistogramBreakdownRootFragmentCallback callback) {

        Bundle bundle = new Bundle();
        if (viewModel != null) {
            bundle.putSerializable(HISTOGRAM_VIEW_MODEL_PARAM, viewModel);
        }
        if (focusablePanel != null) {
            bundle.putString(FOCUSABLE_PANEL_PARAM, focusablePanel);
        }
        if (resolution != null) {
            bundle.putString(RESOLUTION_PARAM, resolution);
        }

        HistogramBreakDownRootFragment fragment = new HistogramBreakDownRootFragment();
        fragment.callback = callback;
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Bundle args = getArguments();
        if (args != null) {
            if (args.containsKey(HISTOGRAM_VIEW_MODEL_PARAM)) {
                histogramViewModel = (TodayHistogramViewModel) args.get(HISTOGRAM_VIEW_MODEL_PARAM);
            }
            if (args.containsKey(FOCUSABLE_PANEL_PARAM)) {
                focusablePanel = args.getString(FOCUSABLE_PANEL_PARAM);
            }
            if (args.containsKey(RESOLUTION_PARAM)) {
                String resolutionS = args.getString(RESOLUTION_PARAM);
                resolution = HistogramResolution.from(resolutionS);
            }
        }

        setupViewPager();
        initView();
    }

    private void initView() {
        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            View tab = ((ViewGroup) tabLayout.getChildAt(0)).getChildAt(i);
            ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) tab.getLayoutParams();
            p.setMargins(0, 0, HPUIUtils.dpToPx(getActivity(), 10), 0);
            tab.requestLayout();
        }
    }

    private void setupViewPager() {

        tabLayout.setVisibility(View.VISIBLE);
        simplePagerAdapter = new SimplePagerAdapter(getChildFragmentManager());

        simplePagerAdapter.addFragment(HistogramResolution.DAILY, HistogramBreakdownFragment.newInstance(histogramViewModel, focusablePanel, HistogramResolution.DAILY, this));
        simplePagerAdapter.addFragment(HistogramResolution.WEEKLY, HistogramBreakdownFragment.newInstance(null, focusablePanel, HistogramResolution.WEEKLY, this));
        simplePagerAdapter.addFragment(HistogramResolution.MONTHLY, HistogramBreakdownFragment.newInstance(null, focusablePanel, HistogramResolution.MONTHLY, this));

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        viewPager.setScrollingEnabled(false);
        viewPager.setAdapter(simplePagerAdapter);
        viewPager.setOffscreenPageLimit(simplePagerAdapter.getCount());

        tabLayout.setupWithViewPager(viewPager);
        tabLayout.addOnTabSelectedListener(this);

        viewPager.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                viewPager.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        int selection = simplePagerAdapter.fragmentType.indexOf(resolution);
                        tabLayout.getTabAt(selection).select();
                    }
                }, DELAY);
            }
        });
    }

    @Override
    public boolean isIntercomAccessible() {
        return false;
    }


    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_histogram_break_down_root;
    }

    @Override
    public String getToolbarDisplayNameExtra() {
        return "";
    }

    @Override
    public boolean isFragmentNameToBeDisplayed() {
        return false;
    }

    @Override
    public boolean isFilteringAvailable() {
        return false;
    }

    @Override
    public int getToolbarDisplayName() {
        return R.string.unknown_value;
    }

    @Override
    public boolean onBackPressed() {
        return true;
    }

    @Override
    public void onBusinessUnitSelected(BusinessUnitViewModel businessUnitViewModel) {

    }

    @Override
    public void onFiltersChanged(BusinessUnitViewModel businessUnitViewModel) {

    }

    @Override
    public void didFinishGettingBusinessUnits(Map<BusinessUnitEnum, BusinessUnitViewModel> businessUnitsMap) {

    }

    @Override
    public void onMainError(String msg, boolean triggerSignOut, boolean isHasNoDevices) {

    }

    @Override
    public boolean respondToValidationPeriodExceedance() {
        return true;
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        if (viewPager != null && simplePagerAdapter != null) {
            Fragment fragment = simplePagerAdapter.getFragment(tab.getPosition());
            if (fragment instanceof HistogramBreakdownFragment) {
                ((HistogramBreakdownFragment) fragment).onSelection();
            }
        }
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    private class SimplePagerAdapter extends FragmentStatePagerAdapter {

        private final Map<HistogramResolution, Fragment> fragmentHashMap = new HashMap<>();
        private final List<HistogramResolution> fragmentType = new ArrayList<>();

        private SimplePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        private void addFragment(HistogramResolution type, Fragment fragment) {
            fragmentHashMap.put(type, fragment);
            fragmentType.add(type);
        }

        @Override
        public Fragment getItem(int position) {
            return fragmentHashMap.get(fragmentType.get(position));
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            Fragment fragment = (Fragment) super.instantiateItem(container, position);

            HistogramResolution name = fragmentType.get(position);
            if (fragmentHashMap.containsKey(name)) {
                fragmentHashMap.remove(name);
            }
            fragmentHashMap.put(name, fragment);
            // save the appropriate reference depending on position
            return fragment;
        }

        @Override
        public boolean isViewFromObject(View view, Object fragment) {
            return ((Fragment) fragment).getView() == view;
        }

        @Override
        public int getCount() {
            return fragmentType.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return getString(fragmentType.get(position).getNameID());
        }

        public Fragment getFragment(int position) {
            if (position < 0 || position >= fragmentType.size()) {
                return null;
            }
            HistogramResolution type = fragmentType.get(position);
            if (type != null && fragmentHashMap.containsKey(type)) {
                return fragmentHashMap.get(type);
            }
            return null;
        }

    }

    public View getMostFocusablePanel() {
        if (simplePagerAdapter != null && tabLayout != null && viewPager != null) {
            Fragment fragment = simplePagerAdapter.getFragment(tabLayout.getSelectedTabPosition());
            if (fragment instanceof HistogramBreakdownFragment) {
                return ((HistogramBreakdownFragment) fragment).getMostFocusablePanel();
            }
        }
        return null;
    }

    @Override
    public boolean isSearchAvailable() {
        return true;
    }

    @Override
    public void onShareButtonClicked(PanelView panelView) {
        if (panelView != null && callback != null) {
            callback.onShareButtonClicked(panelView);
        }
    }

    public interface HistogramBreakdownRootFragmentCallback {
        void onShareButtonClicked(PanelView panelView);
    }
}
